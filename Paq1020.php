<?php
   //BANDEJA TRAMITES PENDIENTES
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CTramites.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Detalle') {
      fxDetalle();
   } elseif (@$_REQUEST['Boton'] == 'Imprimir') {
      fxImprimir();
   } else {
      fxInit();
   }

   function fxInit() {
      //$lcCodAlu = $_SESSION['GADATA']['CCODALU'];
      //$laData = ['CCODALU' => $lcCodAlu, 'CCODUSU' => 'U666'];   // $_SESSION['GCCODUSU']
      //Cargar Deudas Generadas
      $lo = new CPaquetes();
      $lo->paData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omInitEstadoDeuda();
      if (!$llOk) {
         $lo->pcError;
      } 
      $_SESSION['paDatos'] = $lo->paDatos;
      $laData['CNOMBRE'] = $_SESSION['GADATA']['CNOMBRE'];
      $_SESSION['paData'] = $laData ; 
      fxScreen(0);
   }

   function fxDetalle() {
      if (!isset($_POST['pcIdDeud'])) {
         fxInit();
         return;
      }
      $lo = new CTramites();
      $lo->paData = ['CIDDEUD' =>$_POST['pcIdDeud']];
      
      $llOk = $lo->omInitDetalleDeuda();      
      if (!$llOk) {
         fxAlert($lo->pcError);
      }      
      $laData['CIDDEUD'] = $_POST['pcIdDeud'];
      $laData['CNOMBRE'] = $_SESSION['GADATA']['CNOMBRE'];
      $laData['NMONTO']  = $lo->pnMonto;
      $_SESSION['paData'] = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxImprimir() {
      $lo = new CTramites();
      $lo->paData = ['CIDDEUD' =>$_SESSION['paData']['CIDDEUD'],'CNOMBRE' =>$_SESSION['paData']['CNOMBRE']];
      $llOk = $lo->omImprimeDetalleDeuda();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }     
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1020.tpl');
   }
?>