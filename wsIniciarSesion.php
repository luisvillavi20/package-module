<?php
   // ----------------------------------------------------------------------------
   // WS confirmacion de firmas completas de certificados
   // 2019-02-05 LVA Creacion
   // ----------------------------------------------------------------------------
   session_start();
   require_once 'Clases/CLogin.php';
   try {
      $REQUEST = json_decode(file_get_contents("php://input"), true);
      if (is_null($REQUEST)) {
         echo '{"ERROR":"PARAMETROS INCORRECTOS"}';
         return;
      }
   } catch (Exception $e) {
      echo json_encode(["ERROR" => $e->getMessage()]);
   }
   fxIniciarSesionPendientes();
   function fxIniciarSesionPendientes() {
      global $REQUEST;
      $lo = new CLogin();
      $lo->paDataTra = $REQUEST;
      $lo->paDataErp = $REQUEST;
      //$laData = $lo->paData; 
      $llOk = $lo->omIniciarSesionPendientesTramites();
      if ($llOk == false) {
         $llOk1 = $lo->omIniciarSesionPendientesErp();
         echo json_encode($lo->paData);
      } else {
         //$llOk1 = 1;
         echo json_encode($lo->paData);
      }
      if (!$llOk && !$llOk1) {
         echo json_encode(["ERROR" => $lo->pcError]);
      }
   }
?>

