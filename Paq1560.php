<?php
   //BANDEJA TRAMITES PENDIENTES - CONSTANCIAS DE CONDUCTA
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CWebService.php';
   require_once 'Styles/phpqrcode/qrlib.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'Subir') {
      fxSubirArchivo();
   } elseif (@$_REQUEST['Boton'] == 'Salir') {     
      fxScreen3();
   } else {
      fxInit();
   }
   
   function fxInit() {        
      $lo = new CPaquetes();
      $lo->paData = ['CIDCATE' => 'CCCSUC'];
      $llOk = $lo->omRecuperarCertificadosConductaPorSubir();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;            
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

    function fxSubirArchivo() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paFile = $_FILES['pfDocBac'];
      $llOk = $lo->omSubirArchivoORAA();
      if (!$llOk) {
         fxScreen(0);
         fxAlert($lo->pcError);
         return;
      }
      fxHeader('Paq1560.php', "ACTUALIZACION DE ARCHIVO REALIZADA CORRECTAMENTE");
      fxScreen(0);
   }
   
   function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1560.tpl');
   }
   function fxScreen3() {
      fxHeader("Mnu1000.php"); 
   } 
?>