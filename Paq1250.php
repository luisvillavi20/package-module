<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif(@$_REQUEST['Boton'] == 'Imprimir') {
      fxImprimir();
   } else {
      fxInit();
   }

   function fxImprimir() {
      $lo = new CPaquetes();
      $lo->paData = ['CCOLUAC' => $_REQUEST['paData']['CCOLUAC']];
      $llOk = $lo->omReporteJuradosBachiller();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
      }      
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omRecuperarColacionesEscuela();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1250.tpl');
   }
?>