<?php
   //BANDEJA TRAMITES PENDIENTES
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Seguimiento') {
      fxSeguir();
   } elseif (@$_REQUEST['Boton'] == 'SeguimientoDocumento') {
      fxSeguimientoTerminadoPaquetes();
   } elseif (@$_REQUEST['Boton'] == 'RevisarEscuela') {
      fxRevisarEscuela();
   } elseif (@$_REQUEST['Boton'] == 'RevisarDocente') {
      fxRevisarDocente();
   } elseif (@$_REQUEST['Boton'] == 'RevisarORAA') {
      fxRevisarOraa();
   } elseif (@$_REQUEST['Boton'] == 'Observacion') {
      fxObservacion();
   } elseif (@$_REQUEST['Boton'] == 'Observar') {
      fxObservar();
   } elseif (@$_REQUEST['Boton'] == 'RevisarOraaTitulacion') {
      fxRevisarOraaTitulacion();
   } elseif (@$_REQUEST['Boton'] == 'RevisarOraaMaestria') {
      fxRevisarOraaMaestria();
   }  elseif (@$_REQUEST['Boton'] == 'RevisarOraaDoctorado') {
      fxRevisarOraaDoctorado();
   } else {
      fxInit();
   }

	function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omBandejaPaquetesCompletos(); // Init
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paArrays'] = [];
      fxScreen(0);
	}

   function fxSeguir() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omInitEstadoTramitesPaquetes(); // cambiar om
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = $_SESSION['GADATA'] + $lo->paData + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      //print_r($lo->paDatos[0]['CCODALU']);
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxSeguimientoTerminadoPaquetes() {
      if (!isset($_REQUEST['pcCodtre'])) {
         fxAlert("SELECCIONE UN PAQUETE PARA HACER SEGUIMIENTO");
         fxInit();
         return;
      }
      $lo = new CPaquetes();
      $lo->paData = ['CCODTRE' =>$_REQUEST['pcCodtre']];
      $llOk = $lo->omInitDetalleSeguimiento(); // cambiar om
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paData']  = $_SESSION['GADATA']  + ['CCODTRE' => $_REQUEST['pcCodtre']];
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['GADATA']['CNRODNI'];
      return;
   }

   function fxRevisarEscuela() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omRevisarPaqueteEscuela();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $lcNroDni = $lo->paData['CNRODNI'];
      if($_SESSION['paqDat']['CCODIGO'] == 'B') { 
         $llOk = $lo->omGenerarConstancia();
         if (!$llOk) {
            fxAlert($lo->pcError);
         }
      } elseif($_SESSION['paqDat']['CCODIGO'] == 'T') { 
         $llOk = $lo->omGenerarConstanciaTitulacion();
         if (!$llOk) {
            fxAlert($lo->pcError);  
         }
      }
      $laData = $_SESSION['GADATA']+$_REQUEST['paData'];
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxRevisarDocente() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRevisarPaqueteDocente();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxRevisarOraa() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRevisarPaqueteORAA();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $llOk = $lo->omGenerarConstanciaORAA();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paData']  = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxDocumento($lo->pcFile);
      fxInit();
   }

   function fxRevisarOraaTitulacion() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRevisarPaqueteORAATitulacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      /*$llOk = $lo->omGenerarConstanciaORAATitulacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }*/
      $_SESSION['paData']  = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      //fxDocumento($lo->pcFile);
      fxInit();
   }

   function fxRevisarOraaMaestria() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRevisarPaqueteORAAMaestria();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      /*$llOk = $lo->omGenerarConstanciaORAATitulacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }*/
      $_SESSION['paData']  = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      //fxDocumento($lo->pcFile);
      fxInit();
   }

   function fxRevisarOraaDoctorado() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRevisarPaqueteORAADoctorado();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      /*$llOk = $lo->omGenerarConstanciaORAATitulacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }*/
      $_SESSION['paData']  = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      //fxDocumento($lo->pcFile);
      fxInit();
   }
   

   function fxObservacion() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omObservacionDocumento ();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      //$laData = $_SESSION['GADATA'];
      print_r($lo->paData);
      $_SESSION['paData']  = $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(2);
   }

   function fxObservar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omObservarDocumento();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

	function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('saArrays',   $_SESSION['paArrays']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1110.tpl');
   }
?>
