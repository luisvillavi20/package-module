<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) {
      return;
   } elseif (@$_REQUEST['Boton'] == 'Inscribirse') {
      fxInscribirse();
   } else {
      fxInit();
   }
   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $llOk = $lo->omInitCursoLiderazgo();
      if (!$llOk) {
         fxHeader('Mnu0000.php', $lo->pcError);
         return;
      }
      // VERIFICA SI YA ESTA INSCRITO O YA APROBO EL CURSO
      if (isset($lo->paData['CESTTUT'])) {
         $_SESSION['paData'] = $lo->paData + ['CNOMBRE' => $_SESSION['GADATA']['CNOMBRE'], 'CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
         fxScreen(2);
      }
      else {
         $_SESSION['paData'] = ['CNOMBRE' => $_SESSION['GADATA']['CNOMBRE'], 'CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
         $_SESSION['paDatos'] = $lo->paData;
         fxScreen(0);
      }  
   }

   function fxInscribirse() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => 'U666', 'CCODALU' => $_SESSION['GADATA']['CCODALU']];
      $llOk = $lo->omInscripcionLiderazgo();
      if (!$llOk) {
         fxHeader('Paq1230.php', $lo->pcError);
         return;
      }
      fxScreen(1);
      //fxInit();
   }

   function fxScreen($p_nBehavior) {
      global $loSmarty;
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq1230.tpl');
   }
?>