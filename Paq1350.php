<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } 
     elseif (@$_REQUEST['Boton'] == 'Editar') {
      fxCargaDetalleAModif();
   } elseif (@$_REQUEST['Boton'] == 'Cambiar') {
      fxActualizaDatosAluITL();
   } elseif (@$_REQUEST['Boton'] == 'Agregar') {
      fxAgregarRegistro();
   } elseif (@$_GET['Id']=='BuscarDNI'){
      fxBuscarAluITL();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omCargaDeAlumnoITL(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxCargaDetalleAModif() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'];
      $lo->paData = $laData;
      if (($laData['NSERIAL']) == null)
         echo "<script>
                   alert('Seleccione un Alumno');
                   window.location= 'Paq1350.php'
               </script>";
      else {
         $llOk = $lo->omDetalleAlumnoITLMod();
         if (!$llOk) {
            fxHeader('Paq1350.php', $lo->pcError);
         }
         $_SESSION['paDatos'] = $lo->paDatos;
         fxScreen(1);
      }
   }

   function fxActualizaDatosAluITL(){
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'];      
      $lo->paData = $laData + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omDetalleAlumnoITLUpd(); 
      if (!$llOk) {
         echo "'<script>alert('$lo->pcError');</script>'";
         fxScreen(1);
      }
      else {
         fxInit();
      }
   }

   function fxBuscarAluITL(){
      $lcNroDNI = $_GET['DNI'];
      $lo = new CPaquetes();
      $lo->paData = ['CNRODNI' => $lcNroDNI, 'CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAlumnoITLBusqueda();
      if (!$llOk) {
         echo($lo->pcError); 
         return;
      }
   }

   function fxAgregarRegistro(){
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + ['CCODUSU'=> $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omAgregarNuevoRegTOELF();
      if (!$llOk) {
         fxHeader('Paq1350.php', $lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();

   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1350.tpl');
   }
?>