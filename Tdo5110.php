<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CConstancias.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Levantar') {
      fxLevantarObs();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CConstancias();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omInitBandejaConductaObservados(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxLevantarObs() {
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $lo = new CConstancias();
      $lo->paData = $laData;
      $llOk = $lo->omBandejaLevConstaciaEspecialObservada();
      if (!$llOk) {
         fxHeader('Tdo5110.php', $lo->pcError);  
      }
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5110.tpl');
   }
?>