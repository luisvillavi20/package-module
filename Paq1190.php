<?php
   // ----------------------------------------------------------------------------
   // Registro y mantenimiento de alumnos deudores de Coordinacion de Laboratorio
   // 2019-02-05 LVA Creacion
   // ----------------------------------------------------------------------------
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Detalle') {
      fxDetalle();
   } elseif (@$_REQUEST['Boton'] == 'Agregar') {
      fxAgregar();
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Remover') {
      fxRemover();
   } elseif(@$_GET['Id'] == 'Verificar') {
      fxVerificar();
   } else {
      fxInit();
   } 

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omInitBandejaRevisionMaterialDidactico();
      if (!$llOk) {
         fxHeader('Mnu1000.php', 'SIN PAQUETES PENDIENTES');
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxDetalle() {
      if (!isset($_REQUEST['pnNserial'])) {
         fxAlert("SELECCIONE A UN ALUMNO PARA VER EL DETALLE");
         fxInit();
         return;
      }
      
      $lo = new CPaquetes();
      $lo->paData = ['NSERIAL' => $_REQUEST['pnNserial']] + $_SESSION['GADATA'];
      //$lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omDetalleAlumnoMaterialDidactico();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
         return;
      }
      $_SESSION['paData'] = $lo->paData;
      fxScreen(1);
   }

   function fxRemover() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $lo->paData = $laData;
      $llOk = $lo->omRemoverDeudaMaterialDidactico();
      if (!$llOk) {
         fxHeader('Paq1190.php', $lo->pcError);
      }
      fxInit();
   }

   function fxAgregar() {
      $laData = $_SESSION['GADATA'];
      $lo = new CPaquetes();
      $lo->paData = $laData;
      $llOk = $lo->omCargarDatos();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paCenCos'] = $lo->paCenCos;
      fxScreen(2);
   }

   function fxGrabar() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $lo->paData = $laData;
      $llOk = $lo->omGrabarMaterialDidactico();
      if (!$llOk) {
         fxHeader('Paq1190.php', $lo->pcError);
      }
      fxInit();
   } 

   function fxVerificar() {
      $lo = new CPaquetes();
      $lo->paData = ['CNRODNI' => $_REQUEST['CNRODNI']] + $_SESSION['GADATA'];
      $llOk = $lo->omVerificarAlumnoxDni();
      if (!$llOk) {
         echo json_encode(["ERROR" => $lo->pcError]);
      } else {
         echo json_encode($lo->paData);
      }
   }
   
   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('saCencos', $_SESSION['paCenCos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1190.tpl');
   }
?>