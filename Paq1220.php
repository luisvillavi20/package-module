<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) {
      return;
   } elseif (@$_GET['Id'] == 'Verificar') {
      fxVerificar();
   } elseif(@$_GET['Id'] == 'CambioUnidadAcademica') {
      fxCambioUnidadAcademica();
   } elseif (@$_REQUEST['Boton'] == 'Agregar') {
      fxAgregar();
   } elseif (@$_REQUEST['Boton'] == 'Editar') {
      fxEditar();
   } elseif (@$_REQUEST['Boton'] == 'Aprobar') {
      fxAprobar();
   } elseif (@$_REQUEST['Boton'] == 'Desaprobar') {
      fxDesaprobar();
   } else {
      fxInit();
   }
   function fxInit() {
      $lo = new CPaquetes();
      $llOk = $lo->omInitTutoria();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'];
      fxScreen(0);
   }
   function fxAgregar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAgregarLiderazgo();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxInit();
   }
   function fxEditar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRecuperarInfoTutoria();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }
   function fxAprobar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CESTADO' => 'A'];
      $llOk = $lo->omActualizarEstadoLiderazgo();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxEditar();
   }
   function fxDesaprobar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CESTADO' => 'D'];
      $llOk = $lo->omActualizarEstadoLiderazgo();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxEditar();
   }
   function fxVerificar() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODALU' => $_REQUEST['CCODALU']] + $_SESSION['GADATA'];
      $llOk = $lo->omVerificarAlumnoSolicitud();
      echo json_encode([
         'success' => $llOk,
         'data' => $llOk ? $lo->paData: $lo->pcError
      ]);
   }
   function fxCambioUnidadAcademica() {
      global $loSmarty;
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRecuperarBandejaTutoria();      
      if (!$llOk) {
         fxAlert($lo->pcError);
         return;
      }
      $loSmarty->assign('saDatos', $lo->paDatos);
      $loSmarty->display('Plantillas/Paq1221.tpl');
   }
   function fxScreen($p_nBehavior) {
      global $loSmarty;
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq1220.tpl');
   }
?>