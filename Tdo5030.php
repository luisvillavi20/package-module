<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   }
   elseif (@$_REQUEST['Boton'] == 'Revisar') {
      fxRevisar();
   } elseif (@$_REQUEST['Boton'] == 'Aprobar') {
      fxGrabarAprobar();
   } elseif (@$_REQUEST['Boton'] == 'Denegar') {
      fxDenegar();
   } elseif (@$_REQUEST['Boton'] == 'Enviar') {
      fxEnviarCursosPorJurado();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CMatricula();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omInitBandejaSolicitudesCursosJurado(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxRevisar() {
      $lo = new CMatricula();
      $lo->paData = ['CIDENTI' => $_REQUEST['pcCidenti'],'CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omRevisarSolicitud();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData;
      fxScreen(1);  
   }

   function fxGrabarAprobar() {
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['paData'];
      $llOk = $lo->omAprobarSolicitudCursoJurado(); 
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(1);
         return;
      }
      $lo->paData = $_REQUEST['paData'] + $_SESSION['paData'];
      $llOk = $lo->omRevisarSolicitud();
      if (!$llOk) {
         fxHeader('Tdo5030.php', $lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData; 
      fxScreen(1);
   }
   
   function fxDenegar(){
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['paData'];
      $llOk = $lo->omDenegarSolicitudCursoJurado(); 
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(1);
         return;
      }
      $llOk = $lo->omRevisarSolicitud();
      if (!$llOk) {
         fxHeader('Tdo5030.php', $lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData; 
      fxScreen(1);
   }

   function fxEnviarCursosPorJurado(){
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['paData'];
      $llOk = $lo->omEnviarCursosPorJurado(); 
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(1);
         return;
      }
      $llOk = $lo->omRevisarSolicitud();
      if (!$llOk) {
         fxHeader('Tdo5030.php', $lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData; 
      fxScreen(1);
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5030.tpl');
   }
?>