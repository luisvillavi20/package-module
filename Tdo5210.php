<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CTramites.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (@$_REQUEST['Boton'] == 'Confirmar') {
      fxConfirmar();
   } elseif (@$_REQUEST['Boton'] == 'SubirDocumento') {
      fxSubirDocumento();
   } else {
      fxInit();
   }

   function fxInit() {
      fxScreen(0);
   }

   function fxSubirDocumento(){
      $lo = new CPaquetes();
      $lcCodTre = $_REQUEST['paData']['CCODTRE'];
      $lo->paFile = $_FILES['fAutenticacion'];
      $laData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']] + ['CCODALU' => $_SESSION['GADATA']['CCODALU']] +
                ['CCODTRE' => $lcCodTre];
      $lo->paData = $laData;
      $llOk = $lo->omSubirArchivo();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
      }
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omActualizarTramiteMesaDePartes();
      if (!$llOk) {
        fxAlert($lo->pcError);
        fxHeader('Mnu0000.php');
      } else {
        fxHeader('Mnu0000.php','DOCUMENTO SUBIDO CORRECTAMENTE');
      }
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5210.tpl');
   }
?>