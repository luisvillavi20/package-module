<?php
   // BANDEJA PARA REGISTRAR DATOS DE CONSTANCIA DE INTERNADO (OBSTETRICIA)
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   
   if (!fxSoloAdministrativo()) {  
      return;
   } elseif (@$_REQUEST['Boton'] === '') {
      
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $llOk = $lo->omRecuperarPendientesInternado();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      fxScreen(0);
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1160.tpl');
   }
?>