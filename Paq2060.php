<?php
   //BANDEJA TRAMITES PENDIENTES
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Seguimiento') {
      fxSeguir();
   } elseif (@$_REQUEST['Boton'] == 'SeguimientoDocumento') {
      fxSeguimientoTerminadoPaquetes();
   } elseif (@$_REQUEST['Boton'] == 'RevisarDocente') {
      fxRevisarDocente();
   } elseif (@$_REQUEST['Boton'] == 'Observacion') {
      fxObservacion();
   } elseif (@$_REQUEST['Boton'] == 'Observar') {
      fxObservar();
   } else {
      fxInit();
	}

	function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'];
      $llOk = $lo->omBandejaPaquetesCompletos();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paArrays'] = [];
      fxScreen(0);
	}

   function fxSeguir() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omInitEstadoTramitesPaquetes();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = $_SESSION['GADATA'] + $lo->paData;
      //print_r($lo->paDatos[0]['CCODALU']);
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paDato'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxSeguimientoTerminadoPaquetes() {
      if (!isset($_REQUEST['pcCodtre'])) {
         fxAlert("SELECCIONE UN PAQUETE PARA HACER SEGUIMIENTO");
         fxInit();
         return;
      }
      $lo = new CPaquetes();
      $lo->paData = ['CCODTRE' =>$_REQUEST['pcCodtre']];
      $llOk = $lo->omInitDetalleSeguimiento();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paData']  = $_SESSION['GADATA'] + ['CCODTRE' => $_REQUEST['pcCodtre']];
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['GADATA']['CNRODNI'];
      return;
   }

   function fxRevisarDocente() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omRevisarPaqueteDocente();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxObservacion() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omObservacionDocumento();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(2);
   }

   function fxObservar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $i = 0;
      foreach ($_SESSION['paDato'] as $laTmp) {
         if($laTmp['CCODTRE'] == $lo->paData['CCODTRE']){
            break;
         }
         $i++;
      }
      $_SESSION['paDato'][$i]['CESTMTR'] = 'E';
      $llOk = $lo->omObservarDocumento();
      if (!$llOk) {
         //fxAlert($lo->pcError);
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

	function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('saDato', $_SESSION['paDato']);
      $loSmarty->assign('saArrays',   $_SESSION['paArrays']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq2060.tpl');
   }
?>
