<?php
session_start();
require_once 'Libs/Smarty.class.php';
require_once 'Clases/CFormatos.php';
require_once 'Clases/CPersona.php';
$loSmarty = new Smarty;   
if (!fxSoloAlumnos()) { 
   return;
} elseif (@$_REQUEST['Boton'] == 'Cerrar Sesion') {
   fxCerrarSesion();
} elseif (@$_REQUEST['Id'] == 'CambiarCodigo') {
   fxCambiar();
} elseif (@$_REQUEST['Id']=='Agregar') {
   fxAgregarPaqSesion();
} else {      
   fxInit();
}

function fxCerrarSesion() {      
   session_destroy();
   fxheader("index.php");
}

function fxInit() {
   $_SESSION['GADATA']['CALUCOD'] = null;
   $_SESSION['GADATA']['CNIVUAC'] = null;
   $_SESSION['GADATA']['CDESNIV'] = null;
   $_SESSION['paSesCod'] = null;
   $lo = new CFormatos();      
   $lo->paData = ['CNRODNI' =>$_SESSION['GADATA']['CNRODNI']];   
   $llOk = $lo->omGenerarFormato();
   if (!$llOk) {
      fxHeader('Mnu0000.php', $lo->pcError);
   }
   //print_r($_SESSION['paqDat']['CCODIGO']);
   fxScreen();
}

function fxCambiar(){
   $lo = new CPersona();      
   $lo->paData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI'], 'CALUCOD' => $_REQUEST['p_cCodAlu']];
   $_SESSION['paSesCod'] = $_REQUEST['p_cCodAlu'];
   $llOk = $lo->omCambiarCodigo();
   if (!$llOk) {
      echo json_encode(['ERROR'=>$lo->pcError]);
      return;
   }
   $_SESSION['GADATA']['CCODALU'] = $lo->paData['CCODALU'];
   $_SESSION['GADATA']['CUNIACA'] = $lo->paData['CUNIACA'];
   $_SESSION['GADATA']['CNOMUNI'] = $lo->paData['CNOMUNI'];
   $_SESSION['GADATA']['CNIVUAC'] = $lo->paData['CNIVUAC'];
   $_SESSION['GADATA']['CDESNIV'] = $lo->paData['CDESNIV'];
   $_SESSION['GADATA']['CALUCOD'] = $lo->paData['CCODALU'];
   echo json_encode(['OK'=>'CODIGO DE ALUMNO CAMBIADO CORRECTAMENTE']);
}

function fxAgregarPaqSesion(){
   $lcModPaq = $_REQUEST['p_cCodigo'];
   $_SESSION['paqDat'] = ['CCODIGO' => $lcModPaq];
}

function fxScreen() {
   global $loSmarty;      
   $loSmarty->assign('saqDat', $_SESSION['paqDat']);
   $loSmarty->assign('saData', $_SESSION['GADATA']);
   $loSmarty->assign('saSesCod', $_SESSION['paSesCod']);
   $loSmarty->display('Plantillas/Mnu0000.tpl');
}

?>
