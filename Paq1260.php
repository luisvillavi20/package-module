<?php
   //BANDEJA TRAMITES PENDIENTES
   /*error_reporting(E_ALL);*/
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Seguimiento') {
      fxSeguir();
   } elseif (@$_REQUEST['Boton'] == 'SeguimientoDocumento') {
      fxSeguimientoTerminadoPaquetes();
   } elseif (@$_REQUEST['Boton'] == 'Imprimir') {
      fxImprimir();
   } elseif (@$_REQUEST['Boton'] == 'Actas') {
      fxActas();
   } elseif (@$_GET['Id'] == 'CargarExpedientes') {
      axfxCargarExpedientes();
   } elseif (@$_REQUEST['Id'] == 'CargaUniAca') {
      axfxCargarUnidadesAca();
   } else {
      fxInit();
	}

	function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omCargarListaColaciones();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $llOk = $lo->omRecuperarHistorialExpedientes();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['pcUniAca'] = [];
      $_SESSION['paHisCol'] = $lo->paHisCol;
      $_SESSION['paLisFac'] = $lo->paLisFac;
      fxScreen(0);
   }
   
   function fxSeguir() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omInitEstadoTramitesPaquetes();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = ['CNOMBRE' => $_SESSION['GADATA']['CNOMBRE'], 'CNRODNI' => $lo->paData['CNRODNI'], 'CNOMALU' => $lo->paData['CNOMBRE'], 'CNOMUNI' => $lo->paData['CNOMUNI']];
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxSeguimientoTerminadoPaquetes() {
         if (!isset($_REQUEST['pcCodtre'])) {
         fxAlert("SELECCIONE UN TRÁMITE PARA HACER SEGUIMIENTO");
         fxInit();
         return;
      }
      $lo = new CPaquetes();
      $lo->paData = ['CCODTRE' =>$_REQUEST['pcCodtre'], 'CNRODNA ' => $_REQUEST[ 'pcNroDni']];
      $llOk = $lo->omInitDetalleSeguimiento();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paData']  = ['CCODTRE' => $_REQUEST['pcCodtre']] + ['CNRODNA' => $_REQUEST[ 'pcNroDni']];
      $_SESSION['paDatos'] = $lo->paDatos;
      if ($lo->paDatos[0]['CESTDTR'] != null and ($lo->paDatos[0]['CESTDTR'] == 'C' and  $lo->paDatos[0]['CESTMTR'] == 'M')) {
         fxScreen(2); 
      } elseif ($lo->paDatos[0]['CESTMTR'] == 'B' and $lo->paDatos[0]['CESTDTR'] == null and $lo->paDatos[0]['CIDCATE'] != 'CCCONB') {     // CCCSUC CERTIFICADO DE INFORMATICA
         fxScreen(3);
      } elseif ($lo->paDatos[0]['CIDCATE'] == 'CCCONB' and $lo->paDatos[0]['CESTMTR'] == 'B') { //biblioteca material didactivco laboratorio
         fxScreen(4);
      } else {
         fxScreen(2);
      }
   }

   function fxImprimir() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omReporteCargoColacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
         return;
      }
      fxDocumento($lo->pcFile);
      fxInit();
   }

   function fxActas() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU'],'CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omReporteActasColacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
         return;
      }
      fxDocumento("EXP/ACTAS/{$lo->paDatos['CCOLUAC']}.pdf");
      $lo->paDatos = '';
      fxInit();
   }

   function axfxCargarExpedientes() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']] + $_REQUEST['paData'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omRecuperarHistorialExpedientes();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;

      global $loSmarty;
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->display('Plantillas/Paq1261.tpl');
   }

   function axfxCargarUnidadesAca() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omRecuperarUnidadesAcademicasActas();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['pcUniAca'] = $lo->paDatos;
      echo json_encode($_SESSION['pcUniAca']);
   }

	function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('saUniAca', $_SESSION['pcUniAca']);
      $loSmarty->assign('saHisCol', $_SESSION['paHisCol']);
      $loSmarty->assign('saLisFac', $_SESSION['paLisFac']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1260.tpl');
   }
?>
