<?php
   //BANDEJA TRAMITES PENDIENTES
   /*error_reporting(E_ALL);
   ini_set('display_errors',1);*/
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } else if (@$_REQUEST['Boton'] == 'Seguimiento') {
      fxSeguir();
   } else if (@$_REQUEST['Boton'] == 'SeguimientoDocumento') {
      fxSeguimientoTerminadoPaquetes();
   } else {
      fxInit();
	}

	function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omIniBandejaPaquetes();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }    
      $_SESSION['paData'] = $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }
   
   function fxSeguir() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omInitEstadoTramitesPaquetes();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = ['CNOMBRE' => $_SESSION['GADATA']['CNOMBRE'], 'CNRODNI' => $lo->paData['CNRODNI'], 'CNOMALU' => $lo->paData['CNOMBRE'],
                 'CNOMUNI' => $lo->paData['CNOMUNI'], 'CNROCEL' => $lo->paData['CNROCEL'], 'CEMAIL' => $lo->paData['CEMAIL'], 'CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxSeguimientoTerminadoPaquetes() {
         if (!isset($_REQUEST['pcCodtre'])) {
         fxAlert("SELECCIONE UN TRÁMITE PARA HACER SEGUIMIENTO");
         fxInit();
         return;
      }
      $lo = new CPaquetes();
      $lo->paData = ['CCODTRE' =>$_REQUEST['pcCodtre'], 'CNRODNA ' => $_REQUEST[ 'pcNroDni']];
      $llOk = $lo->omInitDetalleSeguimiento();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paData']  = ['CCODTRE' => $_REQUEST['pcCodtre']] + ['CNRODNA' => $_REQUEST[ 'pcNroDni']];
      $_SESSION['paDatos'] = $lo->paDatos;
      if ($lo->paDatos[0]['CESTDTR'] != null and ($lo->paDatos[0]['CESTDTR'] == 'C' and  $lo->paDatos[0]['CESTMTR'] == 'M')) {
         fxScreen(2); 
      } elseif ($lo->paDatos[0]['CESTMTR'] == 'B' and $lo->paDatos[0]['CESTDTR'] == null and $lo->paDatos[0]['CIDCATE'] != 'CCCONB') {     // CCCSUC CERTIFICADO DE INFORMATICA
         fxScreen(3);
      } elseif ($lo->paDatos[0]['CIDCATE'] == 'CCCONB' and $lo->paDatos[0]['CESTMTR'] == 'B') { //biblioteca material didactivco laboratorio
         fxScreen(4);
      } else {
         fxScreen(2);
      }
   }

	function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1200.tpl');
   }
?>
