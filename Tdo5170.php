<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   }
   elseif (@$_REQUEST['Boton'] == 'Revisar') {
      fxRevisar();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CMatricula();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omInitBandejaSeguimientoCPJSecretarias(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxRevisar() {
      $lcIdenti = $_REQUEST['pcCidenti'];
      foreach ($_SESSION['paDatos'] as $laFila) {
			if ($laFila['CIDENTI'] === $lcIdenti) {
            $laData = $laFila;
				break;
         }
      }
      $_SESSION['paData'] = $laData;
      fxScreen(1);  
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5090.tpl');
   }
?>