<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) {
      return;
   } else {
      fxInit();
   }
   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'];
      $llOk = $lo->omRevisarEstadoSolicitud();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;            
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'];
      fxScreen(0);
   }

   function fxScreen($p_nBehavior) {
      global $loSmarty;      
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq1150.tpl');
   }
?>