<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita365e7154edbdd9c411345d9ee1b509f
{
    public static $prefixLengthsPsr4 = array (
        'Z' => 
        array (
            'Zend\\Escaper\\' => 13,
        ),
        'P' => 
        array (
            'PhpOffice\\PhpWord\\' => 18,
            'PhpOffice\\Common\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Zend\\Escaper\\' => 
        array (
            0 => __DIR__ . '/..' . '/zendframework/zend-escaper/src',
        ),
        'PhpOffice\\PhpWord\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpoffice/phpword/src/PhpWord',
        ),
        'PhpOffice\\Common\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpoffice/common/src/Common',
        ),
    );

    public static $classMap = array (
        'PclZip' => __DIR__ . '/..' . '/pclzip/pclzip/pclzip.lib.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita365e7154edbdd9c411345d9ee1b509f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita365e7154edbdd9c411345d9ee1b509f::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInita365e7154edbdd9c411345d9ee1b509f::$classMap;

        }, null, ClassLoader::class);
    }
}
