<?php
require_once 'PHPMailer.php';
require_once 'Exception.php';
require_once 'OAuth.php';
require_once 'POP3.php';
require_once 'SMTP.php';
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
class CEmail {
    protected $php_mailer;
    public $paData, $pcError;

    public function __construct() {
        $this->php_mailer = null;
        $this->paData     = null;
        $this->pcError    = null;
    }

    public function omConnect() {
        $this->php_mailer = new PHPMailer();
        $this->php_mailer->IsSMTP(); // enable SMTP
        //$this->php_mailer->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
        $this->php_mailer->SMTPAuth = true; // authentication enabled
        $this->php_mailer->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        //$this->php_mailer->Host = "smtp.gmail.com";
        $this->php_mailer->Host = 'smtp.office365.com';
        $this->php_mailer->Port = 465; // or 587
        $this->php_mailer->IsHTML(true);
        /*$this->php_mailer->Username = "efb.devs@gmail.com";
        $this->php_mailer->Password = "SistemasFPM";*/
        $this->php_mailer->Username = '72539751@ucsm.edu.pe';
        $this->php_mailer->Password = 'Ucsm9751';
        $this->php_mailer->SMTPSecure = 'tls';
        $this->php_mailer->Port = 25;
        $this->php_mailer->SetFrom("72539751@ucsm.edu.pe");
        return true;
    }

    public function omSend() {
        $llOk = $this->mxValParamSend();
        if (!$llOk) {
            return false;
        }
        $llOk = $this->mxAñadirDestinos();
        if (!$llOk) {
            return false;
        }
        $this->php_mailer->Subject = $this->paData['CSUBJEC'];
        $this->php_mailer->Body = $this->paData['CBODY'];
        if (!$this->php_mailer->Send()) {
            $this->pcError = $this->php_mailer->ErrorInfo;
            return false;
        } else {
            return true;
        }
    }

    protected function mxValParamSend() {
        if (!isset($this->paData['CSUBJEC']) || empty($this->paData['CSUBJEC'])) {
            $this->pcError = "ASUNTO DEL EMAIL NO DEFINIDO";
            return false;
        } elseif (!isset($this->paData['AEMAILS']) || count($this->paData['AEMAILS']) == 0) {
            $this->pcError = "EMAIL DESTINO NO DEFINIDO";
            return false;
        } elseif (!isset($this->paData['CBODY']) || empty($this->paData['CBODY'])) {
            $this->pcError = "CUERPO DEL EMAIL NO DEFINIDO";
            return false;
        }
        return true;
    }

    protected function mxAñadirDestinos() {
        foreach ($this->paData['AEMAILS'] as $value) {
            $this->php_mailer->AddAddress($value);
        }
        return true;
    }
}
?>