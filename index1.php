<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CLogin.php';
   require_once 'Clases/CUsuarios.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;   
   if (@$_REQUEST['Boton'] == 'Iniciar') {
      fxIniciar(); 
   } elseif (@$_GET['Id'] == 'BuscarCargos'){
      fxBuscarCargos();
   } else {
      fxInit();
   }
   //Inicio de Mantenimiento Usuarios
   function fxInit() {   
      if (isset($_SESSION["URL"])) {
         $lcUrl = $_SESSION["URL"]; 
         $_SESSION = []; 
         $_SESSION["URL"] = $lcUrl; 
      }
      fxScreen();
   } 
   function fxBuscarCargos(){
      $lcNroDNI = $_GET['DNI'];
      $lo = new CUsuarios();    
      $lo->paData = ['CNRODNI' => $lcNroDNI];
      $llOk = $lo->omBuscarCargosDisponibles();
      if (!$llOk) {       
         echo($lo->pcError); 
         return;
      } 
   }    

   function fxIniciar() {  
      $lo = new CLogin();
      $lo->paData = $_REQUEST['paData'];      
      $llOk = $lo->omIniciarSesion2();
      if (!$llOk) {
         fxHeader('index1.php', $lo->pcError);
         return;
      }
      if (isset($_SESSION["URL"])) { 
         $lcUrl = $_SESSION["URL"].'.php'; 
         $_SESSION["URL"] = null; 
         fxHeader($lcUrl); 
      } 
      $_SESSION['GADATA'] = $lo->paData;
      if ($_REQUEST['paData']['CNRODNI'] == $_REQUEST['paData']['CCLAVE']) {
         fxAlert('SU CLAVE ES ALTAMENTE INSEGURA, CÁMBIELA POR FAVOR EN EL SISTEMA ACADÉMICO DE LA UNIVERSIDAD');
      }
      fxHeader('Mnu1000.php');
   }
   function fxScreen() {
      global $loSmarty;
      $loSmarty->display('Plantillas/Index1.tpl');
   } 
   
?>