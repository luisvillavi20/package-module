<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>
   <script>
      
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1330.php" method="post" enctype="multipart/form-data">
<!--<input type="hidden" id="Id" name="Id">-->
<!--<form action="Paq1330.php" method="post">-->
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>REGISTRO DE NOTAS TOEFL ITP</b>
         <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="input-group input-group-sm mb-1"> 
            <div class="panel-heading"><h3 class="panel-title"><b>ARCHIVO DE NOTAS</b></div>
            <!--<input type="file" id="poFile" name="poFile" class="form-control" accept="text/csv">-->
            <!--<button type="button" class="btn btn-sm btn-primary" onclick="f_cargarArchivo();">Cargar Detalle</button>-->
            <input name="pfDocBac" accept=".csv" type="file" class="form-control-file" required>
            <button type="submit" name="Boton" value="Subir" onclick="confirm('¿Está seguro que desea subir éste archivo? Esta operación no podrá ser revertida.')" class="center-block btn"><i class="glyphicon glyphicon-upload"></i> Subir</button>
         </div>
            <div class="table-responsive mh-40">
               <table class="table table-sm table-hover table-bordered text-13">
                  <thead class="warning2">
                     <tr class="text-center">
                        <th scope="col">#</th>
                        <th scope="col">Periodo</th>
                        <th scope="col">Cod. Alumno</th>
                        <th scope="col">Origen</th>
                        <th scope="col">Nota</th>
                        <th scope="col">Aprobado</th>
                        <th scope="col">Nivel</th>
                        <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                     </tr>
                  </thead>
                  <tbody id="detallesRequerimiento">
                     {$k = 0}
                     {$nTotal = 0}
                     {foreach from = $saDatos['paEgresos'] item = i}
                        <tr onclick="f_seleccionarFila(this);">
                           <th scope="row">{$k+1}</th>
                           <td class="text-right">{$i['CCODALU']}</td>
                           <td class="text-right">{$i['CPERIOD']}</td>
                           <td class="text-right">{$i['CORIGEN']}</td>
                           <td class="text-right">{$i['CNOTA']}</td>
                           <td class="text-right">{$i['NAPROBA']}</td>
                           <td class="text-right">{$i['CNIVEL']}</td>
                           <td align="center"><input id="pcIndice" type="radio" name="pnIndice" value="{$i['NSERIAL']}"></td>
                        </tr>
                        {$k = $k + 1}
                        {$nTotal = $nTotal + $i['NSTOTAL']}
                     {/foreach}
                  </tbody>
               </table>
            </div>
            <div class="input-group input-group-sm mb-1 justify-content-center">
               <button type="button" class="btn btn-success">Agregar</button>
               <button type="button" class="btn btn-warning">Editar</button>
               <button type="button" class="btn btn-danger">Eliminar</button>
               <!--<div class="btn-group btn-group-sm" role="group">
                  <button id="add_button" type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#nuevoDetalle">Agregar</button>
                  <button type="button" class="btn btn-outline-primary" onclick="f_editarDetalle();">Editar</button>
                  <button type="button" class="btn btn-outline-primary" onclick="f_eliminarDetalle();">Eliminar</button>
               </div>-->
            </div>
      </div>
      </div>                                         
      <div class="row">
         <div class="col-xs-4">
            <button type="button" data-toggle="modal" href="#editarSolucitud" class="btn btn-block btn-success btn-lg">Generar Solicitud</button>
         </div>
         <div class="col-xs-4">
            <!--<button type="submit" onclick="return confirm('¿Esta seguro que desea Denegar la Solicitud?')" name="Boton" value="Denegar" class="center-block btn btn-warning btn-lg btn-block"> Denegar <i class="glyphicon glyphicon-remove"></i></button>-->
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
<form action="Paq1300.php" method="POST">
<div class="modal fade" id="editarSolucitud" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">REALIZAR SOLICITUD</h4>
   </div>
   <div class="modal-body">
      <table class="table" id="table_edit_documents">
         <th>Codigo de Alumno</th>
         <td><input type="text" id="text" class="form-control" name="paData[CCODALU]" maxlength="10" placeholder="Ingrese Código de alumno..." required onkeydown="f_ValidaAlumno(event);" onkeypress="return f_ValidaNumericos(event);"></td>
         </tr><tr>
         <th>Nombre de Alumno</th>
         <td><label><b id="cNomAlu" style="width: 250px"></b></label></td>
         </tr>
      </table>
   </div>
   <div class="modal-footer">
      <div class="row">
         <div class="col-sm-6">
            <input type="submit" name="Boton" value="Solicitar" onclick="return confirm('¿Está seguro que desea aprobar esta solicitud?')" class="center-block btn btn-success btn-lg btn-block" />
         </div>
         <div class="col-sm-6">
            <button type="button" class="center-block btn btn-danger btn-lg btn-block"data-dismiss="modal">Cancelar</button>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</form>
</div>
</body>
</html>