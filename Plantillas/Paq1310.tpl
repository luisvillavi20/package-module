<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>

</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1310.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
         <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed">
               <thead>
                  <tr>
                     <th class="col-xs-2" style='text-align: center'>Codigo de Tramite</th>
                     <th class="col-xs-3" style='text-align: left'>Descripcion de Tramite</th>
                     <th class="col-xs-3" style='text-align: left'>Fecha de Modificacion</th>
                     <th class="col-xs-3" style='text-align: left'>Estado</th>
                     <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                  </tr>
               </thead>
                  {foreach from = $saDatos item = i}
                  <tr>
                    <td style='text-align: center'>{$i['CCODTRE']}</td>
                    <td style='text-align: left'>{$i['CDESCRI']}</td>
                    <td style='text-align: left'>{$i['TMODIFI']}</td>
                    <td style='text-align: left'>{$i['CESTADO']}</td>
                    <td style='text-align: center' class="col-xs-1">            
                        <input type="radio" name="paData[CCODTRE]"  value="{$i['CCODTRE']}"/>
                     </td>
                  </tr>
                  {/foreach}
            </table>
         </div>
      </div>
      </div>                                         
      <div class="row">
         <div class="col-xs-4">
            <button type="submit" onclick="return confirm('¿Esta seguro que desea De Aprobar la Solicitud?')" name="Boton" value="Aprobar" class="center-block btn btn-success btn-lg btn-block"> Aprobar <i class="glyphicon glyphicon-remove"></i></button>
         </div>
         <div class="col-xs-4">
            <button type="submit" onclick="return confirm('¿Esta seguro que desea Observar la Solicitud?')" name="Boton" value="Observar" class="center-block btn btn-warning btn-lg btn-block"> Observar <i class="glyphicon glyphicon-remove"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
</body>
</html>