{foreach from = $saDatos item = i}
   <tr>         
      <td style='text-align: left' class="col-xs-2">{$i['CCODALU']}</td>
      <td style='text-align: left' class="col-xs-2">{$i['CNOMBRE']}</td>
      <td style='text-align: left' class="col-xs-2">{$i['CUNIACA']}</td>
      <td style='text-align: center' class="col-xs-2">{$i['DGENERA']}</td>
      <td style='text-align: center' class="col-xs-1">
         {if $i['CESTADO'] == 'A'}
            <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
         {elseif $i['CESTADO'] == 'D'}  
            <img src="Images/denegar.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Denegado">
         {elseif $i['CESTADO'] == 'P'}  
            <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
         {else}
            <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
         {/if}
      </td>
      <td style='text-align: center' class="col-xs-1">         
      <input type="radio" name="pcCidenti" value="{$i['CIDENTI']}" required></td>
   </tr>
{/foreach}