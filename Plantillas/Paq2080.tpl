<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody">
<form action="Paq2080.php" method="post">
   <div class="row">
      <div class="col-sm-10 col-sm-push-1">
         <div class="panel">
            <div class="panel-heading"><h3 class="panel-title"><b>CONSTANCIAS BACHILLER</b>
               <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
            </div>
            <div class="panel-body">
               <div class="col-xs-10">
                  <input type="text" name="paData[CCODALU]" placeholder="CODIGO DEL ALUMNO" class="form-control">
               </div>
               <div class="col-xs-2">
                  <button name="Boton" value="Imprimir" class="form-control btn-success">Imprimir</button>
               </div>
            </div>
            <br><br>
            <div class="col-sm-4"></div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <a href="Mnu1000.php" class="btn btn-lg btn-block btn-danger">Salir</a>
            </div>
            <br><br>
            <br><br>
         </div>
      </div>
   </div>
</form>
</div>