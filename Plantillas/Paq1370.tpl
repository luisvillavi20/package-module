
<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
   function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
      }

   function myFunctionObs(e){
      if( $("input[type='radio']").is(':checked')){
         $('#modalNew').modal('show');
      }
      else{
         window.alert('Seleccione un alumno');
      }
   }
   </script>

   <style>
   textarea{
      resize: none;
   }
   </style>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1370.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <select id="pcOpcion" name="paData[CTIPCON]" class="selectpicker col-sm-2" onchange="f_CargarSolitudes();">
            <option value="CCCONC">CONSTANCIAS BACHILLER - TITULACIÓN</option>
            <option value="VECLOD">SOLICITUDES DE DEUDA</option>
				 </select>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed table-hover">
               <thead>
                  <tr>
                     <th class="col-xs-1" style='text-align: left'>Nro Expediente</th>
                     <th class="col-xs-2" style='text-align: left'>Tipo Documento</th>
                     <th class="col-xs-1" style='text-align: left'>DNI</th>
                     <th class="col-xs-1" style='text-align: left'>Cod.Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre</th>
                     <th class="col-xs-1" style='text-align: center'>Activar</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                  <tr onclick="f_seleccionarFila(this)">
                    <td style='text-align: left'>E-{$i['CCODTRE']}</td>
                    <td style='text-align: left'>{$i['CDESCRI']}</td>
                    <td style='text-align: left'>{$i['CNRODNI']}</td>
                    <td style='text-align: left'>{$i['CCODALU']}</td>
                    <td style='text-align: left'>{$i['CNOMBRE']}</td>
                    <td style='text-align: center'><input type="radio" name="paData[CCODTRE]" value="{$i['CCODTRE']}" required/></td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type= "submit" onclick="return confirm('¿Esta seguro que desea aprobar toda la lista de documentos?')" id = "aprobSol" name="Boton" value="AprobarSol" class="center-block btn btn-success btn-lg btn-block"> Aprobar <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4">
            <button type = "button" class="center-block btn btn-warning btn-lg btn-block" onclick ="myFunctionObs()">Observar&nbsp;&nbsp;<i class="glyphicon glyphicon-eye-open"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      
      {/if}

      </div>
      </div>
    </div>
  </div>
</form>
<div class="modal fade" id="modalNew" role="dialog">
      <div class="modal-dialog">
      <form action="Paq1370.php" method="POST">
      <input type = "hidden" name = paData[CCODTRE] value = "{$saDatos[0]['CCODTRE']}"> 
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">AGREGAR OBSERVACIÓN </h4>
            </div>
         <div class="modal-body">
         <table class="table table-condesed">
            <tr>
               <th>OBSERVACIÓN</th>
               <td><textarea style="text-transform:uppercase" required ="" id = "txtObser" class="form-control" rows = "4" name="paData[MOBSERV]" pattern="\d*"></textarea></td>
            </tr>
         </table>
         </div>
         <div class="modal-footer">
            <button class="btn btn-warning" name="Boton" value="ObservSol">Observar</button>
            <a class="btn btn-danger" data-dismiss="modal">Cerrar</a>
         </div>
         </div>
      </form>
      </div>
      </div>
</div>
</body>
</html>