<html>
<head>
   <title>Bandeja de Entrada Bachillerato</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <style>
      .comment_point {
         position: absolute;
         font-size: large;
         cursor: pointer;
         color: red;
         background-blend-mode: multiply;
      }
      .comment_point:hover {
         color: rebeccapurple;
      }
      .span-button {
         cursor: pointer;
      }
      .span-button:hover {
         color: linen;
      }
   </style>
   <script>
      function getPosition(e) {
         let imageTop = $("img").offset().top;
         let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
         let relativeTop = e.clientY + scrollTop;
         let realTop = relativeTop - imageTop;
         let imageLeft = $("img").offset().left;
         let scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
         let relativeLeft = scrollLeft + e.clientX;
         let realLeft = relativeLeft - imageLeft;
         $("#commentPanel").css('top', relativeTop + "px");
         $("#commentPanel").css('left', relativeLeft + "px");
         $("#commentPanel").css('display', "inline-block");
         $("#commentPanel").attr('nPosTop', relativeTop);
         $("#commentPanel").attr('nPosLef', relativeLeft);
         $("#commentPanel").attr('nRealTop', realTop);
         $("#commentPanel").attr('nRealLeft', realLeft);
      }
      function f_comentar() {
         let cComent =  $("#cComent").val();
         let lnPagina = $('input[name="paData[NPAGINA]"]').val();
         let lcIdTesi = $('input[name="paData[CIDTESI]"]').val();
         let lnIdLog = $('input[name="paData[NIDLOG]"]').val();
         $.post('Tes1010.php', {
            Id: 'AñadirComentario',
            paData: {
               MDETALL: {
                  nX: $("#commentPanel").attr('nRealLeft'),
                  nY: $("#commentPanel").attr('nRealTop'),
               },
               MOBSERV: cComent,
               NPAGINA: lnPagina,
               CIDTESI: lcIdTesi,
               NIDLOG:  lnIdLog
            }
         }, function (data) {
            let loJson = JSON.parse(data);
            if (!loJson.success) {
               alert(loJson.msg);
            }
         });
         $("#commentPanel").css('display', "none");
         let loPoint = document.createElement("span");
         loPoint.innerHTML = "◉";
         loPoint.className = "comment_point";
         loPoint.style.top = ($("#commentPanel").attr('nPosTop') - 15);
         loPoint.style.left = $("#commentPanel").attr('nPosLef') - 8;
         
         $("#cComent").val('');
         loPoint.onclick = function() {
            $('#cDetCom').html(cComent);
         };         
         $("#main").append(loPoint);
      }
      $(document).ready(function() {
         let lnPagina = $('input[name="paData[NPAGINA]"]').val();
         let lnIdLog = $('input[name="paData[NIDLOG]"]').val();
         $.post('Tes1010.php', {
            Id: 'RecuperarComentarios',
            paData: {
               NPAGINA: lnPagina,
               NIDLOG: lnIdLog
            }
         }, function(p_cData) {
            let loJson = JSON.parse(p_cData);
            if (!loJson.success) {
               console.log(loJson.msg);
               return;
            }
            loJson.data.forEach(element => {
               let imageTop = $("img").offset().top;
               let imageLeft = $("img").offset().left;
               let loPoint = document.createElement("span");
               loPoint.innerHTML = "◉";
               loPoint.className = "comment_point";
               let relativeTop = parseFloat(imageTop) + parseFloat(element.mDetall.nY) - 15;
               console.log(relativeTop);
               let relativeLeft = parseFloat(imageLeft) + parseFloat(element.mDetall.nX) - 8;
               loPoint.style.top = relativeTop + "px";
               loPoint.style.left = relativeLeft + "px";
               
               loPoint.onclick = function() {
                  $('#cDetCom').html(element.mObserv);
               };         
               $("#main").append(loPoint);
            });
         });
      });
   </script>
</head>
<body class="divBody">   
   <div id="main" class="container-fluid">
      <div class="row">
         <div id="panel-general" class="col-sm-9">
            <form action="Tes1010.php" method="POST">
               <input type="hidden" name="paData[  ]" value="{$saDatos['NIDLOG']}">
               <input type="hidden" name="paData[NPAGINA]" value="{$saDatos['NPAGINA']}">
               <input type="hidden" name="paData[CIDTESI]" value="{$saDatos['CIDTESI']}">
               <div class="panel panel-success">
                  <div class="panel-heading"><h4>DOCUMENTO - {$saDatos['MTITULO']}</h4></div>
                  <div class="panel-body">
                     <img class="center-block" onclick="getPosition(event)" style="border: 1px solid black" src="data:image/png;base64, {$saDatos['CIMAGE']}">               
                     <div class="col-xs-10 col-xs-push-1" style="margin-top: 10px">
                        <button {if $saDatos['NPAGINA'] == 1} disabled {/if} type="submit" name="Boton" value="Anterior" class="btn btn-primary col-xs-4"><i class="glyphicon glyphicon-chevron-left"></i>Anterior</button>
                        <span class="col-xs-4 text-center">{$saDatos['NPAGINA']}/{$saDatos['NCANPAG']}</span>
                        <button {if $saDatos['NPAGINA'] == $saDatos['NCANPAG']} disabled {/if} type="submit" name="Boton" value="Siguiente" class="btn btn-primary col-xs-4">Siguiente<i class="glyphicon glyphicon-chevron-right"></i></button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="panel-extend col-sm-3">
            <div class="panel panel-success">
               <div class="panel-heading">Previsualizacion</div>
               <div class="panel-body">
                  <form action="Tes1010.php" method="POST">
                     <input type="hidden" name="paData[NIDLOG]" value="{$saDatos['NIDLOG']}">
                     <input type="hidden" name="paData[NPAGINA]" value="{$saDatos['NPAGINA']}">
                     <input type="hidden" name="paData[CIDTESI]" value="{$saDatos['CIDTESI']}">
                     <button {if $saDatos['NPAGINA'] == 1} disabled {/if} type="submit" name="Boton" value="Anterior" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i></button>                     
                     <button {if $saDatos['NPAGINA'] == $saDatos['NCANPAG']} disabled {/if} type="submit" name="Boton" value="Siguiente" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-right"></i></button>
                     <button type="button" disabled class="btn btn-primary"><i class="glyphicon glyphicon-zoom-in"></i></button>
                     <button type="button" disabled class="btn btn-primary"><i class="glyphicon glyphicon-zoom-out"></i></button>
                  </form>
               </div>
            </div>
            <div class="panel panel-success">
               <div class="panel-heading">Detalle</div>
               <div id="cDetCom" class="panel-body">-</div>
            </div>
         </div>
      </div>
   </div>
   <div id="commentsContainer" style="position: absolute; top: 0; left: 0;"></div>
   <div id="commentPanel" class="panel panel-success" style="display: none; position: absolute;">
      <div class="panel-heading">
         Comentario
      </div>
      <div class="panel-body">
         <textarea id="cComent" class="form-control" autofocus placeholder="Escribir comentario"></textarea>
         <br>
         <button onclick="f_comentar()" class="btn btn-success" style="float: right;">Comentar</button>
      </div>
   </div>
</body>
</html>