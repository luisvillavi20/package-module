<html>
  <head>
    <title>Mantenimiento de Colaciones</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Styles/css/bootstrap.css">
    <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
    <script src="Styles/js/jquery-3.2.0.js"></script>
    <script src="Styles/js/bootstrap.js"></script>
    <script type="text/javascript" src="CSS/jquery.modal.js"></script>
    <script >
      function valSoloNumericos(e){
        if (e.which == 8) {
           return true;
        }
        if (e.key < '0' || e.key > '9') {
           return false;
        }
        return true;
      }
    </script>
  </head>
  <body>
    <div class="panel-heading" style="background-color: #245433; color: #ffffff;"><h3><b>Trámite administrativo</b></h3></div>
      <form action="Paq2040.php" method="post"> 
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="panel with-nav-tabs panel-success">
                <div class="panel-heading"><h4>Mantenimiento de Colaciones</h4></div>
                  <div class="panel-body">   
                    {if $snBehavior == 0}
                      <p class="text-muted"><b>{$saData['CNOMBRE']}</b></p>   
                      <div class="table-responsive">
                        <table class="table table-hover"> 
                          <th style='text-align: center'>Cod. Colación</th>
                          <th style='text-align: center'>Periodo</th>
                          <th style='text-align: center'>Descripción</th>
                          <th style='text-align: center'>Estado</th>
                          <th style='text-align: center'>Fecha</th>
                          <th></th>
                          {$j = 1}
                          {foreach from = $saDatos item = i}
                            <tr>
                              <td style='text-align: center'>{$i['CIDCOLA']}</td>
                              <td style='text-align: center'>{$i['CPERIOD']}</td>
                              <td style='text-align: left'>{$i['CDESCRI']}</td>
                              {if $i['CESTADO']=='A'}
                                <td style='text-align: center; color: #6fe300'>ACTIVO</td>
                              {else}
                                <td style='text-align: center; color: #e60000'>INACTIVO</td>
                              {/if}
                              <td style='text-align: center'>{$i['DFECHA']}</td>
                              <td style='text-align: center'><input type="radio" name="paData[CIDCOED]" value="{$i['CIDCOLA']}"/></td>
                            </tr>
                            {$j = $j + 1}
                          {/foreach}
                        </table> 
                      </div>
                    </div>
                  </div>
                <div class="row"> 
                  <div class="col-sm-4">
                    <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#myModal" role="button"> Nuevo <i class="glyphicon glyphicon-plus-sign"></i></a> 
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header modal-header-success">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title"><b>Crear colación </b></h3>
                          </div>
                          <div class="modal-body">
                            <table class="table table-hover">
                              <tr>
                                 <td><b>CÓDIGO COLACIÓN </b></td>
                                 <td><input class="form-control" type='text' placeholder="Ingrese codigo de colación..." maxlength="5" minlength="5" name="paData[CIDCOLA]"></td>
                              </tr>
                              <tr>
                                 <td><b>PERIODO </b></td>
                                 <td><input class="form-control" type='text' placeholder="Ingrese periodo..." maxlength="7" minlength="7" name="paData[CPERIOD]" onkeypress="return valSoloNumericos(event);"></td>
                              </tr>
                              <tr>
                                 <td><b>DESCRIPCIÓN </b></td>
                                 <td><input class="form-control" type='text' placeholder="Ingrese descripción..." maxlength="100" minlength="0" name="paData[CDESCRI]"></td>
                              </tr>
                              <tr>
                                 <td><b>FECHA DE COLACIÓN </b></td>
                                 <td><input type="date" class="form-control" name="paData[DFECHA]"/></td>
                              </tr>
                            </table>     
                            <div class="modal-footer">
                              <div class="row">
                                <div class="col-sm-6"><input type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"/></div>
                                <div class="col-sm-6"><button type="button" class="center-block btn btn-danger btn-lg btn-block" data-dismiss="modal">Cancelar</button></div>
                              </div>  
                            </div>
                            </div>
                            </div>      
                         </div>
                         </div>
                      </div>     
                      <div class="col-sm-4">
                        <button type="submit" name="Boton" value="Editar" class="center-block btn btn-info btn-lg btn-block"/> Editar <i class="glyphicon glyphicon-edit"></i>
                      </div>
                      <div class="col-sm-4">
                        <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
                      </div>
                    </div>      
                    </div>
           {elseif $snBehavior == 1}
            <h4 class="modal-title"><b>Editar Colación </b></h4>      
            <div class="modal-body">
               <table class="table table-hover">
                  <tr>
                     <td><b>CÓDIGO COLACIÓN </b></td>         
                     <td>
                        <input class="form-control" type='text' name="paData[CIDCOLA]" value="{$saDatos[0]['CIDCOLA']}" readonly>
                     </td>
                  </tr>
                  <tr>
                     <td><b>PERIODO </b></td>         
                     <td>
                        <input class="form-control" type='text' name="paData[CPERIOD]" value="{$saDatos[0]['CPERIOD']}">
                     </td>
                  </tr>
                  <tr>
                     <td><b>DESCRIPCIÓN </b></td>         
                     <td>
                        <input class="form-control" type='text' name="paData[CDESCRI]" value="{$saDatos[0]['CDESCRI']}">
                     </td>
                  </tr>
                  <tr>
                     <td><b>FECHA DE COLACIÓN </b></td>         
                     <td>
                        <input class="form-control" type='text' name="paData[DFECHA]" value="{$saDatos[0]['DFECHA']}">
                     </td>
                  </tr>
                  <tr>
                     <td><b>CODIGO DE USUARIO </b></td>
                     <td><input class="form-control" type='text' name="paData[CCODUSU]" value="{$saDatos[0]['CCODUSU']}" style="text-transform:uppercase"  ></td>
                  </tr>
                  <tr>
                     <td><b>ESTADO </b></td>
                     <td>
                        <select class="form-control" name="paData[CESTADO]">
                           <option value="A" {if $saDatos[0]['CESTADO'] == 'A'} selected="true"{/if}>ACTIVO</option>
                           <option value="I" {if $saDatos[0]['CESTADO'] == 'I'} selected="true"{/if}>INACTIVO</option>
                        </select>          
                     </td>         
                  </tr>
               </table>
            </div>
         </div>
      </div>
            <div class="row col-md-4 col-md-push-8">
                <button type="submit" name="Boton" value="Actualizar" class="center-block btn btn-success btn-lg btn-block"> Actualizar <i class="glyphicon glyphicon-refresh"></i></button>    
               <a href="Paq2040.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a> 
            </div>  
            </div>
         {/if}   
         </div>
         </div>
      </div>
      </div>
    </form>
</html>