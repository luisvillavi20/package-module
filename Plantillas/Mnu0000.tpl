<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
	<script>
		function f_cambiarCodigo(p_cCodAlu) {
			var lcSend = "Id=CambiarCodigo&p_cCodAlu="+p_cCodAlu;
         $.post("Mnu0000.php",lcSend).done(function(p_cResult) {
            var laJson = JSON.parse(p_cResult);
            if (laJson.ERROR){
               alert(laJson.ERROR);
            } /*else {
               alert(laJson.OK);
            }*/
         });
      }

      function f_sendData(e) {
         var lcModPaq = e;
         var lcSend = "Id=Agregar&p_cCodigo="+lcModPaq;
		   $.post("Mnu0000.php",lcSend).done(function(p_cResult) {
            window.location = 'Mnu2000.php';
         });    
      }
	</script>
</head>
<body onload="f_Init()" class="divBody">
<!-- Load Facebook SDK for JavaScript -->
<!-- <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script> -->

<!-- Your customer chat code -->
<!-- <div class="fb-customerchat"
  attribution=setup_tool
  page_id="138087020200132"
  theme_color="#67b868"
  logged_in_greeting="¡Hola! ¿Cómo podemos ayudarte?"
  logged_out_greeting="¡Hola! ¿Cómo podemos ayudarte?">
</div> -->
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody">
   <form action="Mnu0000.php" id="poForm" method="post"> 
   <div class="row">
   <div class="col-sm-3"></div>  
   <div class="col-sm-6">
      <div class="panel-default">
         <div class="panel-group">
            <div class="panel">
               <div class="panel-heading" >
                  {if $saData['CESTENT'] == 0}
                  <h4 class="panel-title" title="{$saData['CNRODNI']}">
                     {$saData['CNRODNI']} - {$saData['CNOMBRE']}<br>
                     SELECCIONE SU UNIDAD ACADÉMICA:<br>
                     <div class="row">
                        <div class="col-sm-12 col-md-6">
                           CÓDIGO DE ALUMNO:
                           <select class="selectpicker form-control" data-live-search="true" name="pcCodAlu" onchange="f_cambiarCodigo(this.value)">
                              <option value="*" disabled selected>--------- CLIC PARA SELECCIONAR ---------</option>
                              {foreach from = $saData['MDATOS'] item = i}  
                                 <option value="{$i['CCODALU']}">{$i['CCODALU']} - {$i['CNOMUNI']}</option>  
                              {/foreach}                 
                           </select>
                        </div>
                        <div class="col-sm-12 col-md-6">
                           <p></p>
                           <p style="color:red; font-size: 18px"><img src="Images/flecha.png" width="15" height="15"> El trámite que genere será con el código seleccionado.</p>
                        </div>
                     </div>
                     <b><a data-toggle="collapse" href="#collapse1"><br><br><span class="glyphicon glyphicon-folder-open" disabled></span> TRÁMITE ADMINISTRATIVO</a></b>
                  </h4>
                  {elseif $saData['CESTENT'] == 1}
                  <h4 class="panel-title" title="">{$saData['CNRORUC']}<br>{$saData['CRAZSOC']}<br>{$saData['CNRODNI']}-{$saData['CNOMBRE']}
                     <b><a data-toggle="collapse" href="#collapse1"><br><br><span class="glyphicon glyphicon-folder-open"></span> AUSPICIO ACADEMICO</a></b>
                  </h4>
                  {/if}
               </div>
               <!--<div class="panel-body panel-default">-->
               <div id="collapse1" class="collapse in">
                  <div class="panel-body panel-default">
                     <ul class="list-group  panel-default">	
                        <li class="list-group-item"><span class="glyphicon glyphicon-credit-card text-success"></span>
                           <a href="Tdo2750.php">SUBIR SOLICITUD (MESA DE PARTES VIRTUAL)</a>	
                        </li>
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                           <a href="Tdo1210.php">GENERAR DEUDA PROVISIONAL - CERTIFICADOS Y PAGOS VARIOS</a>   
                        </li>
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                           <a href="Tdo4110.php">GENERAR DEUDA PROVISIONAL - CONSTANCIAS</a>	
                        </li>
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                           <a role="button"  value="B" onclick="f_sendData('B');">BACHILLERATO ONLINE</a>
                        </li>
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                           <a role="button"  value="T" onclick="f_sendData('T');">TITULACION ONLINE</a>
                        </li>
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                           <a role="button"  value="M" onclick="f_sendData('M');">MAESTRIAS ONLINE</a>
                        </li>
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                           <a role="button"  value="D" onclick="f_sendData('D');">DOCTORADO ONLINE</a>
                        </li>
                        <li class="list-group-item  panel-default"><span class="glyphicon glyphicon-search text-success"></span>
                           <a href="Tdo1120.php">ESTADO DE DEUDA PROVISIONAL</a>	
                        </li>
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-comment text-success"></span>
                           <a href="Tdo1180.php">LLENADO DE FORMULARIO</a>	
                        </li>
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                           <a href="Tdo1130.php">ESTADO DE TRÁMITES</a>	
                        </li>
                        <li class="list-group-item"><span class="glyphicon glyphicon-check text-success"></span>
                           <a href="Tdo1220.php">DATOS PERSONALES</a>	
                        </li>
                        <li class="list-group-item"><span class="glyphicon glyphicon-credit-card text-success"></span>
                           <a href="Tdo1230.php">ESTADO DE CUENTA</a>
                        </li>
                        <li class="list-group-item"><span class="glyphicon glyphicon-education text-success"></span>
                           <a href="Tdo5040.php">SOLICITUD PARA CURSOS POR JURADO</a>
                        </li>
                        <li class="list-group-item"><span class="glyphicon glyphicon-education text-success"></span>
                           <a href="Cnv1110.php">SOLICITUD PARA CONVALIDACION DE CURSOS</a>
                        </li>
                        <li class="list-group-item"><span class="glyphicon glyphicon-education text-success"></span>
                           <a href="Tdo4190.php">SOLICITUD SILABOS CERTIFICADOS</a>
                        </li>
                        <li class="list-group-item"><span class="glyphicon glyphicon-credit-card text-success"></span>
                           <a href="Tdo5010.php">SOLICITUD DE PENSIÓN POR CRÉDITOS</a>	
                        </li>
                     </ul> 
                  </div>
               </div>
               <div class="panel-heading" style="display: none">
                  <h4 class="panel-title">
                     <b><a data-toggle="collapse" href="#collapse2"><span class="glyphicon glyphicon-folder-open" disabled></span> INSCRIPCIÓN CURSOS</a></b>
                  </h4>
               </div>
               <div id="collapse2" class="collapse">
                  <div class="panel-body panel-default">
                     <ul class="list-group  panel-default">	
                        <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                           <a href="Paq1230.php">INSCRIPCIÓN CURSO LIDERAZGO MORAL Y EMPRESARIAL</a>	
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <button type="submit" name="Boton" value="Cerrar Sesion" class="center-block btn btn-danger btn-lg btn-block"> <i class="glyphicon glyphicon-log-out"></i> Cerrar Sesión </button>
         </div>
      </div>
   </div>
   </div>
   <div class="row">
      <div class="col-sm-3"></div>
      <div class="col-sm-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title"><p class="text-justify"><b>MANUAL DE USUARIO</b></p></h3>
            </div>   
            <div class="panel-body "> 
               <p class="text-center"><a class ="center-block btn btn-success btn-lg btn-block" href="./Certificados.pdf" download>DESCARGAR MANUAL - CERTIFICADOS</a>
               <p class="text-center"><a class ="center-block btn btn-success btn-lg btn-block" href="./Constancias.pdf" download>DESCARGAR MANUAL - CONSTANCIAS</a>
               <p class="text-center"><a class ="center-block btn btn-success btn-lg btn-block" href="./CursosJuradoAlumno.pdf" download>DESCARGAR MANUAL - CURSOS POR JURADO</a>
               <p class="text-center"><a class ="center-block btn btn-success btn-lg btn-block" href="./MatriculasCreditos.pdf" download>DESCARGAR MANUAL - MATRÍCULAS POR CRÉDITOS</a>
            </div>
         </div>
      </div> 
      <div class="col-sm-3"></div>
   </div> 
</form>
</div>
<div id="footer"></div>
</body>
</html>