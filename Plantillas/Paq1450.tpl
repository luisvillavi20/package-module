<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
   
</head>
<script>
   function showMod(e){
      if( $("input[type='radio']").is(':checked')){
         let cCodTre = $('input[name="pcCodTre"]:checked').val();
         if(e == 'A'){
            $('#mrCodTre').val(cCodTre);
            var fecha = new Date();
            var ano = fecha.getFullYear();
            $('#modalRev').modal('show');
            $('input[name="paData[CIDANOR]"]').val(ano);
         } else {
            $('#moCodTre').val(cCodTre);
            $('#modalObs').modal('show');
         }
      }
      else{
         window.alert('Seleccione un alumno');
      }
   }

   $(document).ready(function() {
      $('#table').dataTable();
   });
</script>
<body class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Solicitudes de Autenticacion</b></h3></div>
<div class="container-fluid divBody">
<form action="Paq1450.php" method="post" enctype="multipart/form-data">
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title"><b>Bandeja de Solicitudes de Autenticación</b></h3></div>
<div class="panel-body">
   <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>     
   <div class="table-responsive">
   <table class="table table-condensed" id="table"> 
      <thead>
         <tr> 
            <th class="col-xs-1">Codigo</th>  
            <th class="col-xs-2">Fecha de Solicitud</th>  
            <th class="col-xs-1">DNI</th>
            <th class="col-xs-1">Cod. Alumno</th>  
            <th class="col-xs-3">Nombre Alumno</th>
            <th class="col-xs-3">Unidad Academica</th>
            <th class="col-xs-1">Documento</th>
            <th style="text-align: center" class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
         </tr>
      </thead>
      <tbody>
      {foreach from = $saDatos item = i}
         <tr>
            <td class="col-xs-1">E-{$i['CCODTRE']}</td>  
            <td class="col-xs-2">{$i['TFECHA']}</td>  
            <td class="col-xs-1">{$i['CNRODNI']}</td>
            <td class="col-xs-1">{$i['CCODALU']}</td>  
            <td class="col-xs-3">{$i['CNOMBRE']}</td>
            <td class="col-xs-3">{$i['CNOMUNI']}</td>
            <td class="col-xs-1">
            {if $i['CESTADO'] == 'F'}
            DICTAMEN SIN SUBIR
            {else}
            <button type="button" class="btn btn-info btn-block btn-xs" name="pcCodtre" onClick="window.open('DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE={$i['CCODTRA']}', 'Algo', 'height=850, width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>
            {/if}
            </td>
            <td class="col-xs-1">
            <input type="radio" name="pcCodTre" value="{$i['CCODTRE']}">
            </td>
         </tr>
      {/foreach}
      </tbody>      
   </table>
   </div>  
</div>
</div>
<div>
   <div class="col-xs-4">
   <button type="button" onclick="showMod('A')" class="center-block btn btn-success btn-lg btn-block"> Aprobar <i class="glyphicon glyphicon-ok"></i></button>
   </div>
   <div class="col-xs-4">
   <button type="button" onclick="showMod('O')" class="center-block btn btn-warning btn-lg btn-block"> Observar <i class="glyphicon glyphicon-eye"></i></button>
   </div>
   <div class="col-xs-4"><a href="Mnu1000.php" role="button" class="btn btn-danger btn-block btn-lg">Salir</a></div>
</div>
</div>
<div class="modal fade" id="modalRev" role="dialog">
   <div class="modal-dialog">
   <form action="Paq1450.php" method="POST">
      <input type = "hidden" name = "paData[CCODTRE]" id = "mrCodTre"> 
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">TURNITING</h4>
         </div>
      <div class="modal-body">
      <table class="table table-condesed">
         <tr>
            <th class="col-xs-3">Id de Revision</th>
            <td class="col-xs-2"><input class="form-control" style="text-align: center" name="paData[CIDANOR]" type="text" maxlength = "4"></td>
            <td class="col-xs-1" align="center">-</td>
            <td class="col-xs-2"><input class="form-control" style="text-align: center" name="paData[CIDREVI]" type="text" maxlength = "4"></td>
            <td></td>
         </tr>
      </table>
      </div>
      <div class="modal-footer">
         <button class="btn btn-success" name="Boton" value="Confirmar">Aprobar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
      </div>
   </form>
   </div>
</div>
<div class="modal fade" id="modalObs" role="dialog">
   <div class="modal-dialog">
   <form action="Paq1450.php" method="POST">
      <input type = "hidden" name = "paData[CCODTRE]" id = "moCodTre"> 
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">TURNITING OBSERVACION</h4>
         </div>
      <div class="modal-body">
      <table class="table table-condesed">
         <tr>
            <th>Observación</th>
            <td><textarea  style="resize: none; text-transform:uppercase" rows = "4" class="form-control" name="paData[MOBSERV]"></textarea></td>
         </tr>
      </table>
      </div>
      <div class="modal-footer">
         <button class="btn btn-warning" name="Boton" value="Observar">Observar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
      </div>
   </form>
   </div>
</div>
</form>
<div id="footer">
</div>
</div>
</body>
</html>