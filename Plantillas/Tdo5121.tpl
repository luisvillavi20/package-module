<thead>
   <tr>
      <th class="col-xs-2" style='text-align: center'>Codigo de Tramite</th>
      <th class="col-xs-3" style='text-align: left'>Descripción de Trámite</th>
      <th class="col-xs-2" style='text-align: left'>Código Alumno</th>
      <th class="col-xs-3" style='text-align: left'>Nombre</th>
      <th class="col-xs-2" style='text-align: left'>Fecha de Modificacion</th>
      <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
   </tr>
</thead>
<tbody>
{foreach from = $saDatos item = i} 
   <tr class ="Datos">
     <td style='text-align: center'>{$i['CCODTRE']}</td>
     <td style='text-align: left'>{$i['CDESCRI']}</td>
     <td style='text-align: left'>{$i['CCODALU']}</td>
     <td style='text-align: left'>{$i['CNOMBRE']}</td>
     <td style='text-align: left'>{$i['TMODIFI']}</td>
     <td style='text-align: center' class="col-xs-1">            
         <input class="obsrv" type="radio" name="pcCodTre2"  value="{$i['CCODTRE']}"/>
         <input  class = "mobsrv" type = "hidden" value="{$i['MOBSERV']}" />
      </td>
   </tr>
{/foreach}

      