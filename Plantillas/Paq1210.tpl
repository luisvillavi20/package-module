<html>
<head>
   <title>Trámites Administrativos</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      $(document).ready(function() {
         $('#editDocumentDialog').on('shown.bs.modal', function (e) {
            let nSerial = $('input[name="nSerial"]:checked').val();
            if (nSerial == undefined) {
               $('#editDocumentDialog').modal('hide');
               alert('Seleccione documento a editar');
            } else {
               let lcIdCate = $('[param="' + nSerial + '"]')[0].textContent;
               let lcDescri = $('[param="' + nSerial + '"]')[1].textContent;
               $('#editDocumentDialog input[name="paData[CIDCATE]"]').val(lcIdCate);
               $('#editDocumentDialog input[name="paData[CDESCRI]"]').val(lcDescri);
               $('#editDocumentDialog input[name="paData[NSERIAL]"]').val(nSerial);

               if (lcIdCate == 'CCESTU') {
                  $('#editDocumentDialog .CCESTU').css('display', 'table-row');
               }
               else {
                  $('#editDocumentDialog .CCESTU').css('display', 'none');
               }
            }
         });
         $('table tr').each(function(a,b){
            $(b).click(function(){
               $('table tr').css('background','#ffffff');
               $(this).css('background','#e6e6e6');   
            });
         });
      });

      function f_verificarDocumento(self) {
         if (self.value == 'CCESTU') {
            $('.form_ccestu').css('display', 'table-row');
            $('input[param="CCURCOM"]').attr('name', 'paData[CCURCOM]');
            $('input[param="NCANSEM"]').attr('name', 'paData[NCANSEM]');
            $('input[name="paData[NCANSEM]"]').attr('', '');
         } else {
            $('.form_ccestu').css('display', 'none');
            $('input[name="paData[NCANSEM]"]').removeAttr('');
            $('input[name="paData[NCANSEM]"]').removeAttr('name');
            $('input[name="paData[CCURCOM]"]').removeAttr('name');
         }
      }

      function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
      }
   </script>
</head>
<body class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80"> Trámites Administrativos</b></h3></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-md-push-2">
<form action="Paq1210.php" method="POST">
   {if $snBehavior == 0}
      <div class="panel panel-success">
         <div class="panel-heading">
            <h3 class="panel-title"><b>REGISTRO Y MANTENIMIENTO DE PAQUETES</b><span style="float: right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
         </div>
         <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-condesed table-hover">
               <thead>
                  <tr>
                  <th>Código</th>
                  <th>Unidad Académica</th>
                  <th>Paquete</th>
                  <th>Estado</th>
                  <th style="text-align: center">Activar</th>
                  </tr>   
               </thead>
               <tbody>
                  {foreach from = $saDatos item = i}
                     <tr onclick="f_seleccionarFila(this)">
                     <td>{$i['CIDPAQU']}</td>
                     <td>{$i['CUNIACA']} - {$i['CNOMUNI']}</td>
                     <td>{$i['CDESCRI']}</td>
                     <td>
                        {if $i['CESTADO'] == 'A'}
                           ACTIVO
                        {else}
                           INACTIVO
                        {/if}
                     </td>                                 
                     <td style="text-align: center"><input type="radio" name="paData[CIDPAQU]" value="{$i['CIDPAQU']}" ></td>
                     </tr>
                  {/foreach}
               </tbody>
            </table>
            </div>
         </div>
      </div>
      <div class="row">
         {if $saData['CNIVEL'] != 'V'}
         <div class="col-md-4">
            <button name="Boton" value="NuevoPaquete" class="btn btn-block btn-success btn-lg">Nuevo</button>
         </div>
         <div class="col-md-4">
            <button name="Boton" value="InfoPaquete" class="btn btn-block btn-primary btn-lg">Editar</button>
         </div>
         {else}
         <div class="col-md-4">
            <button name="Boton" value="InfoPaquete" class="btn btn-block btn-primary btn-lg">Ver Detalle</button>
         </div>
         {/if}
        
         <div class="col-md-4">
            <a href="Mnu1000.php" class="btn btn-block btn-danger btn-lg">Salir</a>
         </div>
      </div>
   {elseif $snBehavior == 1}
      <div class="panel panel-success">
         <div class="panel-heading">
            <h3 class="panel-title"><b>REGISTRO Y MANTENIMIENTO DE PAQUETES</b><span style="float: right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
         </div>
         <div class="panel-body">
            <div class="table-responsive">
               <table class="table table-condesed">
                  <tr>
                  <th class="col-sm-1">Código</th>
                  <td class="col-sm-3"><input type="text" name="paData[CIDPAQU]" value="{$saDatos['paPaquet']['CIDPAQU']}" class="form-control" readonly></td>
                  <th class="col-sm-3">Unidad Academica</th>
                  <td class="col-sm-5"><input type="text" value="{$saDatos['paPaquet']['CUNIACA']} - {$saDatos['paPaquet']['CNOMUNI']}" class="form-control" readonly></td>
                  </tr><tr>
                  <th>Estado</th>
                  <th>
                     <select class="selectpicker form-control" name="paData[CESTADO]" >
                        <option value="A" {if $saDatos['paPaquet']['CESTADO'] == 'A'} selected {/if}>ACTIVO</option>
                        <option value="I" {if $saDatos['paPaquet']['CESTADO'] == 'I'} selected {/if}>INACTIVO</option>
                     </select>
                  </th>
                  <th>Descripción</th>
                  <th><input type="text" class="form-control" name="paData[CDESCRI]" value="{$saDatos['paPaquet']['CDESCRI']}"></th>
                  </tr>
               </table>
            </div>
            <div class="table-responsive">
               <table class="table table-condesed table-hover">
                  <thead>
                     <tr>
                     <th>Código</th>
                     <th>Descripción</th>
                     <th>Monto</th>
                     <th>Derecho de Trámite</th>
                     <th>Estado</th>
                     <th style="text-align: center">Activar</th>
                     </tr>
                  </thead>
                  <tbody>
                     {foreach from = $saDatos['paDocPaq'] item = i}
                        <tr onclick="f_seleccionarFila(this)">
                        <td param="{$i['NSERIAL']}">{$i['CIDCATE']}</td>
                        <td param="{$i['NSERIAL']}">{$i['CDESCRI']}</td>
                        <td>{$i['NMONTO']}</td>
                        <td>{$i['NCOSFOR']}</td>
                        <td>
                           {if $i['CESTADO'] == 'A'}
                              ACTIVO
                           {else}
                              INACTIVO
                           {/if}
                        </td>
                        <td style="text-align: center"><input name="nSerial" value="{$i['NSERIAL']}" type="radio"></td>
                        </tr>
                     {/foreach}
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="row">
         {if $saData['CNIVEL'] != 'V'}
         <div class="col-sm-3">
            <button class="btn btn-block btn-success btn-lg" name="Boton" value="EditarPaquete">Grabar</button>
         </div>
         <div class="col-sm-3">
            <button type="button" data-toggle="modal" href="#addDocumentDialog" class="btn btn-block btn-primary btn-lg">Agregar Documento</button>
         </div>
         <div class="col-sm-3">
            <button type="button" data-toggle="modal" href="#editDocumentDialog" class="btn btn-block btn-info btn-lg">Editar Documento</button>
         </div>
         {/if}
         <div class="col-sm-3">
            <a href="Paq1210.php" class="btn btn-block btn-danger btn-lg">Salir</a>
         </div>
      </div>
      <div class="row"></div>
   {elseif $snBehavior == 3}
      <div class="panel panel-success">
         <div class="panel-heading">
            <h3 class="panel-title"><b>REGISTRO Y MANTENIMIENTO DE PAQUETES</b><span style="float: right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
         </div>
         <div class="panel-body">
            <table class="table table-condesed">
               <tr>
               <th>Descripción</th>
               <th><input type="text" name="paData[CDESCRI]" class="form-control" ></th>
               </tr><tr>
               <th>Unidad Académica</th>
               <td>
                  <select class="selectpicker form-control" data-live-search="true" name="paData[CUNIACA]" required>
                     <option selected="true" disabled value="">Seleccione Unidad Academica</option>
                     {foreach from = $saDatos['paUniAca'] item = i}
                        <option value="{$i['CUNIACA']}">{$i['CUNIACA']} - {$i['CNOMUNI']}</option>
                     {/foreach}
                  </select>
               </td>
               </tr><tr>
               <th>Tipo de Colación</th>
               <td>
                  <select class="selectpicker form-control" data-live-search="true" name="paData[CCODIGO]" required>
                     <option selected="true" disabled value="">Seleccione Tipo de Colación</option>
                     {foreach from = $saDatos['paTipCol'] item = i}
                        <option value="{$i['CCODIGO']}">{$i['CCODIGO']} - {$i['CDESCRI']}</option>
                     {/foreach}
                  </select>
               </td>
               </tr>
            </table>
         </div>
      </div>
      <div class="row">
         <div class="col-md-4"><button class="btn btn-success btn-block btn-lg" name="Boton" value="AgregarPaquete">Agregar</button></div>
         <div class="col-md-4"></div>
         <div class="col-md-4"><button class="btn btn-danger btn-block btn-lg"  name="Boton" formnovalidate>Cerrar</button></div>            
      </div>
   {/if}   
</form>   
<form method="POST" action="Paq1210.php">
<div class="modal fade" id="addDocumentDialog" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">AGREGAR DOCUMENTO</h4>
   </div>
   <div class="modal-body">
      <input type="hidden" name="paData[CIDPAQU]" value="{$saDatos['paPaquet']['CIDPAQU']}">
      <table class="table table-condesed">
         <tr>
         <th>Documentos</th>
         <td>
            <select class="selectpicker form-control" data-live-search="true" name="paData[CIDCATE]" onchange="f_verificarDocumento(this)" >
               <option selected="true" disabled value="">Seleccione Documento</option>
               {foreach from = $saDatos['paLisDoc'] item = i}
                  <option value="{$i['CIDCATE']}">{$i['CIDCATE']} - {$i['CDESCRI']}</option>
               {/foreach}
            </select>
         </td>
         </tr>
         <tr class="form_ccestu" style="display: none">
         <td>Número de semestres</td>
         <td>
            <input param="NCANSEM" name="paData[NCANSEM]" type="number" class="form-control">
         </td>
         </tr>
         <tr class="form_ccestu" style="display: none">
         <td>Complementación Curricular</td>
         <td>
            <select param="CCURCOM" name="paData[CCURCOM]" class="form-control">
               <option value="N">NO</option>
               <option value="S">SI</option>
            </select>
         </td>
         </tr><tr>
         <td></td>
         <td style="text-align: right">
            <a data-toggle="modal" href="#addMissingDocumentDialog">¿Documento no encontrado?</a>
         </td>
         </tr>
      </table>
   </div>
   <div class="modal-footer">
      <button onsubmit="return false" class="btn btn-success" name="Boton" value="AgregarDocumento">Agregar</button>
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>
</div>
</div>
</form>
<form action="Paq1210.php" method="POST">
<div class="modal fade" id="editDocumentDialog" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">EDITAR DOCUMENTO</h4>
   </div>
   <div class="modal-body">
      <input type="hidden" name="paData[NSERIAL]">
      <input type="hidden" name="paData[CIDPAQU]" value="{$saDatos['paPaquet']['CIDPAQU']}">
      <table class="table table-condesed" id="table_edit_documents">
         <tr>
         <th>Código</th>
         <td><input name="paData[CIDCATE]" class="form-control" readonly></td>
         </tr><tr>
         <th>Descripción</th>
         <td><input name="paData[CDESCRI]" class="form-control" readonly></td>
         </tr><tr class="CCESTU" style="display: none">
         <th>Número de semestres</th>
         <td><input type="number" name="paData[NCANSEM]" class="form-control"></td>
         </tr><tr class="CCESTU" style="display: none">
         <th>Complementación curricular</th>
         <td>
         <select name="paData[CCURCOM]" class="form-control">
            <option value="N">SI</option>
            <option value="S">NO</option>
         </select>
         </td>
         </tr>
         <th>Estado</th>
         <td>
            <select class="form-control" name="paData[CESTADO]">
               <option value="A">ACTIVO</option>
               <option value="I">INACTIVO</option>
            </select>
         </td>
         </tr>
      </table>
   </div>
   <div class="modal-footer">
      <button class="btn btn-success" name="Boton" value="EditarDocumento">Grabar</button>
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>
</div>
</div>
</form>
<form action="Paq1210.php" method="POST">
<div class="modal fade" id="addMissingDocumentDialog" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">AÑADIR DOCUMENTO</h4>
   </div>
   <div class="modal-body">
      <input type="hidden" name="paData[CIDPAQU]" value="{$saDatos['paPaquet']['CIDPAQU']}">
      <table class="table table-condesed">
         <tr>
         <th>Nombre del documento</th>
         <td><input type="text" placeholder="Nombre del documento" name="paData[CDESCRI]" class="form-control"></td>
         </tr><tr>
         <th>Formato</th>
         <td>
            <select class="form-control" name="paData[CTIPFOR]">
               {foreach from = $saDatos['paLisFor'] item = i}
                  <option value="{$i['CTIPFOR']}">{$i['CTIPFOR']} - {$i['CDESCRI']}</option>
               {/foreach}
            </select>
         </td>
         </tr><tr>
         <th>Periodo</th>
         <td>
            <select class="form-control" name="paData[CPERIOD]">
               {foreach from = $saDatos['paLisPry'] item = i}
                  <option value="{$i['CPROYEC']}">{$i['CPROYEC']} - {$i['CDESCRI']}</option>
               {/foreach}
            </select>
         </td>
         </tr><tr>
         <th>Monto</th>
         <td><input type="number" class="form-control" name="paData[NMONTO]" value="0.0"></td>
         </tr><tr>
         <th>Fecha Inicio</th>
         <td><input type="date" class="form-control" name="paData[DINICIO]"></td>
         </tr><tr>
         <th>Fecha Finalización</th>
         <td><input type="date" class="form-control" name="paData[DFINALI]"></td>
         </tr>
      </table>
   </div>
   <div class="modal-footer">
      <button class="btn btn-success" name="Boton" value="AgregarDocumentoNuevo">Grabar</button>
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</body>
</html>