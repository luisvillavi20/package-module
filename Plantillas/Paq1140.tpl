<html>
  <head>
    <title>Solicitud de Bachiller para Administrativos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Styles/css/bootstrap.css">
    <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
    <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
    <link rel="stylesheet" href="CSS/style.css">
    <script src="js/java.js"></script>
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    <script src="Styles/js/jquery-3.2.0.js"></script>
    <script src="Styles/js/bootstrap.js"></script>
    <script>
      function valSoloNumericos(e){
        if (e.which == 8) {
          return true;
        }
        if (e.key < '0' || e.key > '9') {
          return false;
        }
        return true;
      }
      function valCodigoAlumno(e){
        var inputtxt = document.getElementById('text'); 
        var valor = inputtxt.value;
        if (valor.length == 9)
        {
          $.get("Paq1140.php?Id=Verificar&CCODALU=" + valor + e.key, (data) => {
            let json = JSON.parse(data);
            if (!json.success) {
              $('#cNomAlu').css("color", "#ce4646");
            }
            else {
              $('#cNomAlu').css("color", "#333333");
            }
            $('#cNomAlu').text(json.data['CNOMBRE']);
          });
        }
      }
    </script>
  </head>
  <body onload="f_Init()" class="divBody">
    <div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
    <div class="container-fluid divBody table-responsive">
      <form action="Paq1140.php" method="post" enctype="multipart/form-data">
        <div class="container divBody">
          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-success">
                {if $snBehavior == 0}
                  <div class="panel-heading">
                    <h3 class="panel-title"><b>SOLICITUD DE BACHILLER</b><span style="float: right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
                  </div>
                  <div class="panel-body">
                    <div>
                      <table class="table table-condesed">
                        <tr>
                          <th class="col-sm-3">Solicitud de autorización: </th>
                          <th class="col-sm-4">
                            <select class="selectpicker form-control" data-live-search="true" name="paData[CTIPAUT]">
                              <option selected="true" disabled="disabled">Seleccione solicitud...</option>
                              {foreach from = $saDatos item = i}
                                <option value="{$i['CTIPAUT']}">{$i['CDESCRI']}</option>
                              {/foreach}            
                            </select>
                          </th>
                          <th></th>
                        </tr>
                        <tr>
                          <th>Codigo de Alumno: </th>
                          <th>
                            <input type="text" id="text" class="form-control" name="paData[CCODALU]" maxlength="10" placeholder="Ingrese codigo de alumno..." required onkeydown="valCodigoAlumno(event);" onkeypress="return valSoloNumericos(event);">
                          </th>
                          <th>
                            <b id="cNomAlu" style="width: 250px"></b>
                          </th>
                        </tr>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <button type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-check"></i></button><br>
                      </div>
                      <div class="col-sm-4">
                      </div>
                      <div class="col-sm-4">
                        <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
                      </div>
                    </div>   
                  </div>
                  <div class="row"></div>
                </div>
              {/if}
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div id="footer">
    </div>
  </body>
</html>