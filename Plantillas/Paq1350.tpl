<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>
   
   <script>
      $(document).ready(function(){
		   $('.Ocultar').hide();
         $('#Buscar').hide();
      });

      function fxBuscarDNI(){         
         var lcNroDni = $('#pcNroDni').val();      
         $.get( "Paq1350.php?DNI="+lcNroDni+"&Id=BuscarDNI", 
         function( data ) {
            $('#selCodigo').empty();
            var content  = jQuery.parseJSON(data);
            document.getElementById("NombreAlu").innerHTML = content[0].Nombre;
            $.each(content,function(){
               $('#selCodigo').append('<option value="'+this.Codigo+'">'+this.Codigo+' '+this.CNomUni+'</option>');
               $('#selCodigo').selectpicker('refresh');
            });
         });
         $('.Ocultar').show();
         $('#Buscar').show();
      }

   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1350.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
         <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed">
               <thead>
                  <tr>
                     <th class="col-xs-2" style='text-align: center'>Codigo de Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre de Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nota de Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Aprobado</th>
                     <th class="col-xs-3" style='text-align: left'>Nivel</th>
                     <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                  </tr>
               </thead>
                  {foreach from = $saDatos item = i}
                  <tr>
                     <td style='text-align: center'>{$i['CCODALU']}</td>
                     <td style='text-align: left'>{$i['CNOMBRE']}</td>
                     <td style='text-align: left'>{$i['NNOTA']}
                     <td style='text-align: left'>{$i['CAPROBA']}
                     <td style='text-align: left'>{$i['CNIVEL']}
                     <td style='text-align: center' class="col-xs-1">            
                        <input type="radio" name="paData[NSERIAL]"  value="{$i['NSERIAL']}"/>
                     </td>
                  </tr>
                  {/foreach}
            </table>
            
         </div>
      </div>                               
      <div class="row">
         <div class="col-xs-4">
            <button type="button" data-toggle="modal" href="#addModal" class="center-block btn btn-success btn-lg btn-block"> Agregar <i class="glyphicon glyphicon-plus"></i></button>
         </div>
         <div class="col-xs-4">
            <button  class="center-block btn btn-warning btn-lg btn-block"  name="Boton" value = "Editar"> Editar &nbsp;&nbsp;<i class="glyphicon glyphicon-th-list"></i></a>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      </div>
      </div>
    </div>
  </div>
   {elseif $snBehavior == 1}
   <div class="panel-heading"><h3 class="panel-title"><b>DETALLE DE ALUMNO</b></div>
      <div class="panel-body">

            <div class="table-responsive mh-50">
            <table class="table table-condesed">
               <input class="hidden" name = "paData[NSERIAL]" value = "{$saDatos[0]['NSERIAL']}" type = "text">
               <tr>
               <th>Nota Alumno</th>
               <td><input type = "number" class="form-control" name="paData[NNOTA]" value = "{$saDatos[0]['NNOTA']}" type="text" required></td>
               </tr>
               <tr>
               <th>Aprobado</th>
               <td><input class="form-control" name="paData[CAPROBA]" value = "{$saDatos[0]['CAPROBA']}"  type="text" required></td>
               </tr>
               <tr>
               <th>Nivel</th>
               <td><input class="form-control" name="paData[CNIVEL]" value = "{$saDatos[0]['CNIVEL']}" type="text" required></td>
               </tr>
            </table>
            </div>
            <div class="row">
               <div class = "col-xs-4">
                  <button class="center-block btn btn-success btn-lg btn-block"  name="Boton" value = "Cambiar"> Guardar &nbsp;&nbsp;<i class="glyphicon glyphicon-save"></i></a>
               </div>   
               <div class="col-sm-4">
                  <a href="Paq1350.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
               </div>
            </div>
      </div>
   </div>
   {/if}        
</form>
</div>
<form action="Paq1350.php" method="POST">
<div id="addModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Creación de Nuevo Registro </h4>
            </div>
            <div class="modal-body">
               <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>DNI: </h5>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <input onblur="fxBuscarDNI()" class="form-control col-xs-12" type="text" id="pcNroDni">
                        <label id = "NombreAlu"></label> 
                    </div>
                </div>
               <div class="Ocultar">
                <hr>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>Código de Alumno</h5>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <select id = "selCodigo" class="selectpicker form-control" data-live-search="true" name="paData[CCODALU]"></select>
                    </div>
                </div>
            <hr>
               <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>Aprobado: </h5>
                    </div>
                     <div class="form-check col-lg-3 col-md-3 col-sm-3 col-xs-3">
				         	<label>
				         		<input type="radio" name="paData[CAPROBA]" value="S" required> <span class="label-text">SI</span>
				         	</label>
				         </div>
				         <div class="form-check col-lg-3 col-md-3 col-sm-3 col-xs-3">
				         	<label>
				         		<input type="radio" name="paData[CAPROBA]" value="N"required> <span class="label-text">NO</span>
				         	</label>
				         </div>
                </div>
            <hr>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>NOTA: </h5>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <input type="number" class="form-control col-xs-12"  name="paData[CNOTA]" required> 
                    </div>
                </div>
            <hr>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>NIVEL: </h5>
                    </div>
                    <div class="form-check col-lg-2 col-md-2 col-sm-2 col-xs-2">
				         	<label>
				         		<input type="radio" name="paData[CNIVEL]" value="A1" required> <span class="label-text">A1</span>
				         	</label>
				         </div>
				         <div class="form-check col-lg-2 col-md-2 col-sm-2 col-xs-2">
				         	<label>
				         		<input type="radio" name="paData[CNIVEL]" value="A2" required> <span class="label-text">A2</span>
				         	</label>
				         </div>
                     <div class="form-check col-lg-2 col-md-2 col-sm-2 col-xs-2">
				         	<label>
				         		<input type="radio" name="paData[CNIVEL]" value="B1" required> <span class="label-text">B1</span>
				         	</label>
				         </div>
				         <div class="form-check col-lg-2 col-md-2 col-sm-2 col-xs-2">
				         	<label>
				         		<input type="radio" name="paData[CNIVEL]" value="B2" required> <span class="label-text">B2</span>
				         	</label>
				         </div>
                     <div class="form-check col-lg-2 col-md-1 col-sm-2 col-xs-2">
				         	<label>
				         		<input type="radio" name="paData[CNIVEL]" value="C1" required> <span class="label-text">C1</span>
				         	</label>
				         </div>
                </div>
            <hr>
               <div class="row">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>PERIODO: </h5>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <input  name="paData[CPERIOD]" type="date" class="form-control" placeholder="dd.mm.yyyy" required>
                  </div>        
               </div>
            <hr>
               <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <h5>ORIGEN</h5>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <select class="selectpicker form-control" data-live-search="true" name='paData[CORIGEN]'>
                        <option value="01">OCRRII</option>
                        <option value='02'>ICPNA</option>
                        <option value='03'>CULTURAL</option>
                        </select>
                    </div>
               </div>
            <hr>
            </div>
            </div>
            <div class="modal-footer">
               <div class="row">
                  <div class="col-sm-4">
                  </div>
                  <div class="col-sm-4">
                  <button type="submit" id="Buscar" class="center-block btn btn-success btn-lg btn-block" name="Boton" value="Agregar">Agregar.<i class="glyphicon glyphicon-folder-open"></i></button>   
                  </div>
                  <div class="col-sm-4">
                     <button type="button" class="center-block btn btn-danger btn-lg btn-block" data-dismiss="modal">Cancelar</button>
                  </div>
               </div>
            </div>
         </div>
   </div>
   </div>
</form>                                              
</body>
</html>