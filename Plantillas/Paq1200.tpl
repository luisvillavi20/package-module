<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
   <script>
      $(document).ready(function () {
      $('table tr').each(function(a,b){
                  $(b).click(function(){
                        $('table tr').css('background','#ffffff');
                        $(this).css('background','#e6e6e6');   
                  });
               });
      });

      function f_seleccionarFila(e) {
               var a = e.cells[e.cells.length-1].children.item(0);
               a.checked = true;
      }

      $(document).ready(function() {
         $('#table').dataTable();
      });

   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Seguimiento </b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1200.php" method="post">
   {if $snBehavior == 0}
   <div class="container divBody">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-success">
               <div class="panel-heading"><h3 class="panel-title"><b>Seguimiento </b></h3></div>   
               <div class="panel-body">
                  <div class="table-responsive mh-50">
                     {if $saData['CCODUSU'] == '3070' or $saData['CCODUSU'] == '2052'}
                     <table id ="table" class="table table-condensed display">
                     {else}
                     <table class="table table-condensed table-hover"> 
                     {/if}
                        <thead> 
                           <th style='text-align: center' class="col-xs-2">Fecha Recepción</th>
                           <th style='text-align: center' class="col-xs-1">Cod. Alumno</th>
                           <th style='text-align: center' class="col-xs-1">DNI</th>
                           <th style='text-align: left' class="col-xs-3">Nombre</th>
                           <th style='text-align: left' class="col-xs-4">Unidad Académica</th>
                           {if $saData['CCODUSU'] == '3070' or $saData['CCODUSU'] == '2052'}
                              <th style='text-align: left' class="col-xs-2">Exp. Completo</th>    
                           {/if}
                           <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                        </thead>
                        {foreach from = $saDatos item = i}
                        <tr onclick="f_seleccionarFila(this)">
                           <td style='text-align: center'>{$i['DRECEPC']}</td>
                           <td style='text-align: center'>{$i['CCODALU']}</td>
                           <td style='text-align: center'>{$i['CNRODNI']}</td>
                           <td style='text-align: left'>{$i['CNOMBRE']}</td>
                           <td style='text-align: left'>{$i['CNOMUNI']}</td>
                           {if $saData['CCODUSU'] == '3070' or $saData['CCODUSU'] == '2052'}
                              {if $i['CETAPA'] == 'B' OR $i['CETAPA'] == 'C'}
                                 <td style='text-align: left'><span class="label label-success">COMPLETO</span></td>
                              {else}
                                 <td style='text-align: left'><span class="label label-default">PENDIENTE</span></td>   
                              {/if}
                           {/if}
                           <td style="text-align: center"><input type="radio" name="paData[CCODALU]" value="{$i['CCODALU']}" required></td>
                        </tr>
                        {/foreach}
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>                
      <div class="row">
        <div class="col-sm-4">
          <button type="submit" name="Boton" value="Seguimiento" class="center-block btn btn-primary btn-lg btn-block"> Seguimiento <i class="glyphicon glyphicon-ok"></i></button>
        </div>
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
          <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
        </div>
      </div>
      {elseif $snBehavior == 1}
      <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITES</b>
              <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
           </div>
         <div class="panel-body">
            <p class="text-muted"><b>ALUMNO: {$saData['CNRODNI']} - {$saData['CNOMALU']} - {$saData['CNOMUNI']}  DATOS DE ALUMNO: {$saData['CNROCEL']} - {$saData['CEMAIL']} </b></p>       
            <div class="table-responsive mh-50">
            <table class="table table-condensed"> 
               <thead> 
                  <th style='text-align: center' class="col-xs-4">Tipo Documento</th>
                  <th style='text-align: center' class="col-xs-2">Nro Expediente</th>
                  <th style='text-align: center' class="col-xs-2">Fecha de Envio</th>  
                  <th style='text-align: center' class="col-xs-1">Estado Escuela</th>
                  {if $saData['CCODUSU'] == '3070'}       
                  <th style='text-align: center' class="col-xs-1">Docentes Re</th>       
                  {/if}
                  <th style='text-align: center' class="col-xs-1">Estado ORAA</th>        
                  <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
               </thead>
               {$j = 0}
               {foreach from = $saDatos item = i}  
                  <tr onclick="f_seleccionarFila(this)">
                  <td style='text-align: left' class="col-xs-4">{$i['CDESDOC']}</td>
                  <td style='text-align: center' class="col-xs-2">E-{$i['CCODTRE']}</td>
                  <td style='text-align: center' class="col-xs-2">{$i['TFECREC']}</td>
                  <td style='text-align: center' class="col-xs-1"> 
                  {if ($i['CESTDTR'] !=  null and ($i['CESTMTR'] == 'B' or $i['CESTMTR'] != 'S') and ($i['CETAPA'] == 'A'))}
                     {if $i['CESTDTR'] == 'A'}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTDTR'] == 'B' or $i['CESTMTR'] == 'E'}  
                        <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {elseif $i['CESTDTR'] == 'F' or $i['CESTMTR'] == 'F'}  
                        <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M'}  
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Mesa de partes">
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S'}  
                      <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                     {else}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  {elseif $i['CETAPA'] == 'A' }
                     {if $i['CESTMTR'] == 'E'}   
                        <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDAOTR' or $i['CIDCATE'] == 'PDADOC')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">         
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'CCCONB' OR ($i['CIDCATE'] != 'CCCSID' OR $i['CIDCATE'] != 'CCCOND'))}  
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
                     {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 2) == 'CC' and ($i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCESTU')}
                        <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                     {elseif $i['CESTMTR'] == 'M' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Mesa de partes">
                     {elseif $i['CESTMTR'] == 'S' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == '000084' or $i['CIDCATE'] == '000017') or (substr($i['CIDCATE'], 0, 3) == 'PDA')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                     {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 1) == 'P'}
                        <img src="Images/subir.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Subir Archivo">      
                     {elseif $i['CESTMTR'] == 'B' and $i['CIDCATE'] == 'PQ0033'}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">      
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S'}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                     {else}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  {elseif $i['CETAPA'] == 'B' or $i['CETAPA'] == 'C' or $i['CETAPA'] == 'D' }
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
                  {/if}
                  </td>
                  {if $saData['CCODUSU'] == '3070'}
                  <td style='text-align: center' class="col-xs-1">
                  {if ($i['CESTDTR'] !=  null and ($i['CESTMTR'] == 'B' or $i['CESTMTR'] != 'S') and $i['CETAPA'] == 'B')}
                     {if $i['CESTDTR'] == 'A' }
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDAOTR' or $i['CIDCATE'] == 'PDADOC')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">      
                     {elseif $i['CESTDTR'] == 'B' or $i['CESTMTR'] == 'E'}  
                        <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {elseif $i['CESTDTR'] == 'F' or $i['CESTMTR'] == 'F' }  
                        <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M' }  
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S' }  
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDAOTR' or $i['CIDCATE'] == 'PDADOC')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">      
                     {else}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  {elseif $i['CETAPA'] == 'B'}
                     {if $i['CESTMTR'] == 'E'}   
                        <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDAOTR' or $i['CIDCATE'] == 'PDADOC')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">      
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'CCCONB' OR ($i['CIDCATE'] != 'CCCSID' OR $i['CIDCATE'] != 'CCCOND'))}  
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 2) == 'CC' and ($i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCESTU')}
                        <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                     {elseif $i['CESTMTR'] == 'M' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND')}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'S' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == '000084' or $i['CIDCATE'] == '000017')}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 1) == 'P'}
                        <img src="Images/subir.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Subir Archivo">      
                     {elseif $i['CESTMTR'] == 'R' and $i['CIDCATE'] == 'PQ0033'}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">      
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S'}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                     {else}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  {elseif $i['CETAPA'] == 'C' or $i['CETAPA'] == 'D' }
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">  
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
                  </td>
                  {/if}
                  <td style='text-align: center' class="col-xs-1">
                  {if ($i['CESTDTR'] !=  null and ($i['CESTMTR'] == 'B' or $i['CESTMTR'] != 'S') and $i['CETAPA'] == 'D')}
                     {if $i['CESTDTR'] == 'C' }
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDAOTR' or $i['CIDCATE'] == 'PDADOC')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">         
                     {elseif $i['CESTDTR'] == 'B' or $i['CESTMTR'] == 'E'}  
                        <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {elseif $i['CESTDTR'] == 'F' or $i['CESTMTR'] == 'F' }  
                        <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M' }  
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S' }  
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {else}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  {elseif $i['CETAPA'] == 'C'}
                     {if $saData['CCODIGO'] == 'T'}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">         
                     {elseif $i['CESTMTR'] == 'E'}   
                        <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDAOTR' or $i['CIDCATE'] == 'PDADOC')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">         
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'CCCONB' OR ($i['CIDCATE'] != 'CCCSID' OR $i['CIDCATE'] != 'CCCOND'))}  
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 2) == 'CC' and ($i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCESTU')}
                        <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                     {elseif $i['CESTMTR'] == 'M' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND')}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'S' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == '000084' or $i['CIDCATE'] == '000017')}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 1) == 'P'}
                        <img src="Images/subir.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Subir Archivo">      
                     {elseif $i['CESTMTR'] == 'R' and $i['CIDCATE'] == 'PQ0033'}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">      
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDAOTR' or $i['CIDCATE'] == 'PDADOC')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">         
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S'}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                     {else}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  {elseif $i['CETAPA'] == 'D' }
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
                  {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDAOTR' or $i['CIDCATE'] == 'PDADOC')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">      
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
                  </td>
                  <td style='text-align: center' class="col-xs-1">            
                  <input type="radio" name="pcCodtre"  value="{$i['CCODTRE']}"/>
                  <input type="hidden" name="pcNroDni" value="{$i['CNRODNI']}">
                  </td>
                  </tr>
                  {$j = $j + 1}
               {/foreach}
            </table>
            </div>
            </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-sm-4">
               <button type="submit" name="Boton" value="SeguimientoDocumento" class="center-block btn btn-primary btn-lg btn-block"> Seguimiento <i class="glyphicon glyphicon-forward"></i></button><br>
            </div>  
         <div class="col-sm-4">
         </div>
         <div class="col-sm-4">
            <a href="Paq1200.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div> 
         </div> 
         </div> 
         </div>
         {elseif $snBehavior == 2}
         <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL TRÁMITE</b></h3></div>
            <div class="panel-body">    
            <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
            <div class="form-group">
            <table class="table table-striped">   
               <tr class="info">
                  <th colspan="2">Datos del Trámite:</th>
               </tr><tr>
                  <th><label>Tipo de Trámite: </label></th>
                  <th><label>{$saDatos[0]['CDESCRI']}</label></th>
               </tr><tr>
                  <th><label>Observaciones: </label></th>
                  {if $saDatos[0]['MOBSERM'] != null}
                     <th><label>{$saDatos[0]['MOBSERM']}</label></th>
                  {elseif isset($saDatos[0]['MOBSERD']['MOBSERV']) != null}
                     <th><label>{$saDatos[0]['MOBSERD']['MOBSERV']}</label></th>
                  {elseif $saDatos[0]['MOBSERM'] == null or $saDatos[0]['MOBSERD']['MOBSERV']== null }
                     <th><label> </label></th>
                  {/if}
               </tr><tr>   
            </table>
            </div>      
            <br>
            <div class="table-responsive">
            <table class="table table-hover">
               <tr class="info">
                     <th colspan="5">Ubicación del Documento:</th>
                  </tr>
               <th class="col-xs-2" style='text-align: center'>Recepción</th>  
               <th class="col-xs-2" style='text-align: center'>Envio/Aprobación</th> 
               <th class="col-xs-4" style='text-align: center'>Unidad</th>     
               <th class="col-xs-2" style='text-align: center'>Estado</th>            
                  {foreach from = $saDatos item = i} 
                  <tr>
                     <td style='text-align: center'>{$i['TRECIBI']}</td>
                     <td style='text-align: center'>
                     {if $i['TULTIMA'] == null }
                        <span class="label label-default">-----</span>
                     {else}
                        {$i['TULTIMA']}
                     {/if}
                     </td>
                     <td style='text-align: center'>{$i['CDESCRI']}</td> 
                     <td style='text-align: center'><h4>
                     {if $i['CESTDTR'] != null}
                        {if $i['CESTDTR'] == 'A' }
                           <span class="label label-default">PENDIENTE</span>                    
                        {elseif $i['CESTDTR'] == 'C' and ($i['CESTMTR'] != 'S' or $i['CESTMTR'] != 'M') }
                           <span class="label label-success">REVISADO</span>
                        {elseif $i['CESTDTR'] == 'C' and ($i['CESTMTR'] == 'S' or $i['CESTMTR'] == 'M') }
                           <span class="label label-success">VISTO - APROBADO</span>
                        {elseif $i['CESTDTR'] == 'B' }  
                           <span class="label label-warning">VISTO - OBSERVADO</span>
                        {elseif $i['CESTDTR'] == 'F' }  
                           <span class="label label-danger">LLENAR FORMULARIO</span>
                        {elseif $i['CESTMTR'] == 'S' or $i['CESTPRO'] == 'R'}  
                           <span class="label label-success">VISTO - APROBADO</span> 
                        {else}
                           <span class="label label-default">PENDIENTE</span>
                        {/if}
                     {else}
                        {if $i['CESTMTR'] == 'A' and ($i['CIDCATE'] != 'CCCOND' or $i['CIDCATE'] == 'CCCONB')}
                           <span class="label label-default">PENDIENTE</span> 
                        {elseif $i['CESTMTR'] == 'F' and ($i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == 'CCCONB')}
                           <span class="label label-default">PENDIENTE</span> 
                        {elseif $i['CESTMTR'] == 'B' }
                           <span class="label label-success">EN REVISIÓN</span>
                        {elseif $i['CESTMTR'] == 'E' }  
                           <span class="label label-warning">VISTO - OBSERVADO</span>
                        {elseif  $i['CESTMTR'] == 'F' }  
                           <span class="label label-danger">SUBIR ARCHIVO</span>
                        {elseif $i['CESTMTR'] == 'S' or $i['CESTMTR'] == 'M' }
                           <span class="label label-success">VISTO - APROBADO</span> 
                        {else}
                           <span class="label label-default">PENDIENTE</span>
                        {/if}
                     {/if}
                     </h4></td>
                  </tr>  
               {/foreach}
            </table> 
                <br>
                {if $i['CESTMTR'] == 'S' and $i['CESTPRO'] == 'R' and ($i['CIDCATE'] == 'CCESTU' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCSUC' or $i['CIDCATE'] == '000017')}
                  <div class="row col-md-10 col-md-push-1">
                       <div class="col-sm-4"></div>
                       <div class="col-sm-4">
                          <a class="btn btn-info btn-block btn-lg" onClick="window.open('{$i['CDOCDIG']}', 'Algo', 'height=850 ,width=850');">Ver Documento</a>   
                       </div>
                  </div>
                {elseif ($i['CESTMTR'] == 'S' or $i['CESTMTR'] == 'M' and $i['CESTPRO'] == 'P') and ($i['CIDCATE'] == 'CCESTU' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCSUC' or $i['CIDCATE'] == '000084' or $i['CIDCATE'] == '000017')}
                     <div class="row col-md-10 col-md-push-1">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                           <button type="button" class="btn btn-info btn-block btn-lg" name="pcCodtre" onClick="window.open('DocumentoPaquete.php?CNRODNI={$saData['CNRODNA']}&CCODTRE={$saData['CCODTRE']}', 'Algo', 'height=850, width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>
                        </div>
                     </div>
                     <div class="row col-md-10 col-md-push-1">
                        <iframe id="frameDocumento" style="width: 100%; height:80vh; display: none" src="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}"></iframe>
                     </div>
                 {elseif ($i['CESTMTR'] == 'R' or $i['CESTMTR'] == 'B') and $i['CIDCATE'] == 'PQ0033'}
                     <div class="row col-md-10 col-md-push-1">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                           <button type="button" class="btn btn-info btn-block btn-lg" name="pcCodtre" onClick="window.open('DocumentoPaquete.php?CNRODNI={$saData['CNRODNA']}&CCODTRE={$saData['CCODTRE']}', 'Algo', 'height=850, width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>
                        </div>
                     </div>
                     <div class="row col-md-10 col-md-push-1">
                        <iframe id="frameDocumento" style="width: 100%; height:80vh; display: none" src="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}"></iframe>
                     </div>
               {/if}
            </div>         
         </div>
         </div>
         <div class="row col-md-4 col-md-push-8">     
         <a href="Paq1200.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
         </div>
         </div>
         </div> 
         </div>
         {elseif $snBehavior == 3}
         <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DE TRÁMITE</b></h3></div>
            <div class="panel-body">  
            <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
               {foreach from = $saDatos item = i} 
                  <div class="form-group">
                     <table class="table table-striped">   
                     <tr class="info">
                        <th colspan="2">Datos del Trámite</th>
                     </tr><tr>
                        <th class="col-xs-3"><label>Tipo de Trámite: </label></th>
                        <th><label>{$saDatos[0]['CDESCRI']}</label></th>
                     </tr><tr>
                        <th class="col-xs-3"><label>Codigo Usuario: </label></th>
                        <th><label>{$i['CCODUSU']}</label></th>
                     </tr><tr>
                        <th class="col-xs-3"><label>Fecha de termino: </label></th>
                        <th><label>{$i['TRECIBI']}</label></th>
                     </tr>
                     </table>
                  </div>  
               {/foreach}     
            </div>
         </div>   
         <div class="row">
            <div class="col-sm-4">
               {if $saDatos[0]['CIDCATE'] == 'PQ0034'}
                  <button type="button" class="btn btn-info btn-block btn-lg" name="pcCodtre" onClick="window.open('{$i['MDETALL']['CURLPDF']}', 'Algo', 'height=850 ,width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>
               {else}
                  <button type="button" class="btn btn-info btn-block btn-lg" name="pcCodtre" onClick="window.open('DocumentoPaquete.php?CNRODNI={$saData['CNRODNA']}&CCODTRE={$saData['CCODTRE']}', 'Algo', 'height=850, width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>
               {/if}
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <a href="Paq1200.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
            </div>
         </div> 
         {elseif $snBehavior == 4}
         <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL TRÁMITE</b></h3></div>
            <div class="panel-body">  
            <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
               {foreach from = $saDatos item = i} 
                  <div class="form-group">
                     <table class="table table-striped">   
                     <tr class="info">
                        <th colspan="2">Datos del Trámite</th>
                     </tr><tr>
                        <th class="col-xs-3"><label>Tipo de Trámite: </label></th>
                        <th ><label>{$saDatos[0]['CDESCRI']}</label></th>
                     </tr><tr>
                        <th><label>Codigo Usuario: </label></th>
                        <th><label>{$i['MDETALL']['CCODUSU']}</label></th>
                     </tr><tr>
                        <th><label>Fecha de termino: </label></th>
                        <th><label>{$i['MDETALL']['DVENCIM']}</label></th>
                     </table>
                  </div>  
               {/foreach}    
            </div>
         </div>   
         <div class="row">
            <div class="col-sm-4">
               <a class="btn btn-info btn-block btn-lg" onClick="window.open('{$i['MDETALL']['CURLPDF']}', 'Algo', 'height=850 ,width=850');">Ver Documento</a>
            </div>
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
               <a href="Paq1200.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
            </div>
          </div> 
         {elseif $snBehavior == 4}
         <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL ARCHIVO</b></h3></div>
            <div class="panel-body">  
               <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>     
               <div class="form-group">
                  <table class="table table-striped">   
                     <tr class="info">
                        <th colspan="2">Datos del Trámite:</th>
                     </tr><tr>
                        <th><label>Tipo de Trámite: </label></th>
                        <th><label>{$saDatos[0]['CDESDOC']}</label></th>
                     </tr><tr>
                        <th><label>Observaciones: </label></th>
                        <th><label>{$saDatos[0]['MOBSERV']}</label></th>
                     </tr><tr>   
                  </table>
               </div>      
               <br>
               <div class="table-responsive">
               <table class="table table-hover">
                  <tr class="info">
                        <th colspan="5">Ubicación del Documento:</th>
                  </tr>
                  <th class="col-xs-2" style='text-align: center'>Recepción</th>  
                  <th class="col-xs-2" style='text-align: center'>Envio/Aprobación</th> 
                  <th class="col-xs-4" style='text-align: center'>Unidad</th>     
                  <th class="col-xs-2" style='text-align: center'>Estado</th>            
                     {foreach from = $saDatos item = i} 
                     <tr>
                        <td style='text-align: center'>{$i['TFECREC']}</td>
                        <td style='text-align: center'>
                        {if $i['TFECFIN'] == '' }
                           <span class="label label-default">-----</span>
                        {else}
                           {$i['TFECFIN']}
                        {/if}
                        </td>
                        <td style='text-align: center'>{$i['CDESCCO']}</td> 
                        <td style='text-align: center'><h4>
                        {if $i['CESTDTR'] == 'A' }
                        <span class="label label-default">PENDIENTE</span> 
                        {elseif $i['CESTDTR'] == 'C' }
                           <span class="label label-success">VISTO - APROBADO</span>
                        {elseif $i['CESTDTR'] == 'B' }  
                           <span class="label label-warning">VISTO - OBSERVADO</span>
                        {elseif $i['CESTDTR'] == 'X' }  
                           <span class="label label-danger">VISTO - ANULADO</span>
                        {/if}
                        </h4></td>
                     </tr>  
                  {/foreach}
               </table> 
               </div>     
            </div>
            </div>
         <div class="row col-md-4 col-md-push-8">     
         <a href="Paq1200.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
         </div>
         </div>
         </div> 
         </div>
         {/if} 
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer"></div>
</body>
</html>