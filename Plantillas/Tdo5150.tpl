<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>
   <script>
   function funcApro(e){
      var pcFileName = $('#file').val();
      pcFileName = pcFileName.substr(-10);
      return confirm('¿Está seguro que desea subir '+pcFileName+'? Esta operación no podrá ser revertida.');
   }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Tdo5150.php" method="POST" id="poForm" enctype="multipart/form-data">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      <input type="hidden" name="p_cCodTre" id="p_cCodTre">
      <input type="hidden" name="Id" id="Id">
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
      </div>
      <div class="panel-body">
         <div class="table-responsive mh-50">
            <table class="table table-condensed" id="TBody">
               <thead>
                  <tr>
                     <th class="col-xs-2" style='text-align: center'>Codigo de Tramite</th>
                     <th class="col-xs-3" style='text-align: left'>Descripción de Trámite</th>
                     <th class="col-xs-2" style='text-align: left'>Código Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre</th>
                     <th class="col-xs-2" style='text-align: center'>Fecha de Solicitud</th>
                     <th class="col-xs-2" style='text-align: left'>Subir</th>
                     <th class="col-xs-2" style='text-align: left'>Aprobar</th>
                  </tr>
               </thead>
               <tbody>
                  {foreach from = $saDatos item = i}
                  <form action="Tdo5150.php" method="POST" enctype="multipart/form-data">
                     <tr class ="Datos">
                     <td style='text-align: center'>E-{$i['CCODTRE']}</td>
                     <td style='text-align: left'>{$i['CDESCRI']}</td>
                     <td style='text-align: left'>{$i['CCODALU']}</td>
                     <td style='text-align: left'>{$i['CNOMBRE']}</td>
                     <td style='text-align: center'>{$i['TFECHA']}</td>
                     <td>
                        <input name="fTraduccion" id = "file" type="file" accept="application/pdf" class="file" >
                     </td>
                     <td>
                        <button name="Boton" value="Aprobar" onclick="return funcApro(this)" class="center-block btn-success"/><i class="glyphicon glyphicon-ok"></i></button>
                     </td> 
                     <input type="hidden" class="pcCodTr" name = "pcCodTre" value="{$i['CCODTRE']}">
                  </tr>
                  </form>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row" id="btnBody1">
         <div class="col-xs-4"></div>
         <div class="col-xs-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
         <div class="col-sm-4"></div>
      </div>                                         
      </div>
      </div>
    </div>
  </div>
</form>
</div>
</body>
</html>