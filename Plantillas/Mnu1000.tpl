<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<script>
   function funcion(lcModPaq){
      var lcSend = "Id=Agregar&p_cCodigo="+lcModPaq;
		$.post("Mnu1000.php",lcSend).done(function(p_cResult) {
      });  
      if(lcModPaq == 'B'){ 
         $('#collapse3').collapse('hide');
      } else if(lcModPaq == 'T'){
         $('#collapse2').collapse('hide');
      }    
   }
      function f_CambiarCentroCosto(p_cNivel) {
         $("#p_cOption").val('OPTION');
         $('form').submit();
      }
</script>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody">
<form action="Mnu1000.php" method="post"> 
   <div class="container">
   <div class="row">
   <div class="col-sm-3"></div>
   <div class="col-sm-6">
   <div class="panel-group">
   <div class="panel panel-success">
   {if  $saOption neq 0}
   <div class="panel-heading">
      <h4 class="panel-title">Cargo 
         <select name="paData[CNIVEL]" class="form-control form-control-sm" onchange="f_CambiarCentroCosto(this.value);">
               {foreach from=$saOption item=i}
                  <option value="{$i['CNIVEL']}" {if $saData['CNIVEL'] eq $i['CNIVEL']} selected {/if}>{$i['CDESCRI']}</option>
               {/foreach}
         </select>
         <input type="hidden" name="OPTION" id="p_cOption">
      </h4>
   </div> 
   {/if}
   {if  $saData['CNIVEL']=='0'}     <!--ADMINISTRADOR-->
   <div class="panel-heading">
      <h4 class="panel-title">ADMINISTRADOR 
         <br>{$saData['CNOMBRE']} <a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-wrench"></span><b> MANTENIMIENTO GENERAL </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="collapse in">          
      <ul class="list-group">     
         <li class="list-group-item"><span class="glyphicon glyphicon-file text-primary"></span>
            <a href="Paq1150.php">Estado Solicitudes</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq1140.php">Beneficio de Bachiller</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-duplicate text-primary"></span>
            <a href="Paq2010.php">Mantenimiento Autorizaciones</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq2040.php">Mantenimiento Colaciones</a>
         </li> 
         <!--li class="list-group-item"><span class="glyphicon glyphicon-file text-primary"></span>
            <a href="Paq2600.php">Reportes Trámites Finalizados</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-duplicate text-primary"></span>
            <a href="Tdo2210.php">Mantenimiento de Paquetes</a>
         </li>-->
            <li class="list-group-item"><span class="glyphicon glyphicon-file text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-file text-primary"></span>
            <a href="Paq1170.php">Mantenimiento de Usuarios - Paquetes</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-cloud text-primary"></span>
            <a href="Paq1210.php">Mantenimiento de Paquetes</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a onclick ="funcion('B');" href="Paq1200.php">Seguimiento de Paquetes - Bachiller</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a onclick ="funcion('T');" href="Paq1200.php">Seguimiento de Paquetes - Titulación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-duplicate text-primary"></span>
            <a href="Paq2030.php">Mantenimiento Unidades Académicas</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-duplicate text-primary"></span>
            <a href="Paq2020.php">Mantenimiento Centros de Costo</a>
         </li>
         <!--<li class="list-group-item"><span class="glyphicon glyphicon-file text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
         </li> -->
         <li class="list-group-item"><span class="glyphicon glyphicon-user text-primary"></span>
            <a href="Tdo2230.php">Mantenimiento de Usuarios</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo2250.php">Mantenimiento de Alumnos</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-cloud text-primary"></span>
            <a href="Tdo2290.php">Reportes por Codigo</a>
         </li>
         <!--<li class="list-group-item"><span class="glyphicon glyphicon-file text-primary"></span>
            <a href="Paq3200.php">Bandeja para Turnitin</a>
         </li>-->
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo2720.php">Busqueda de Estado de Trámite</a>
         </li>
      </ul>  
   </div>
   {elseif $saData['CNIVEL'] == '1'}    <!--SECRETARIAS-->	
   <div class="panel-heading">
      <h4 class="panel-title"><!--SECRETARIA DE {$saData['CNOMUNI']} -->
         <br>{$saData['CNOMBRE']}<br><br>
         <a data-toggle="collapse" href="#collapse1"><span class="glyphicon glyphicon-folder-open"></span><b> CURSOS COMPLEMENTARIOS</b></a><br>
      </h4>
   </div>
   <div id="collapse1" class="collapse in">	  
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-file text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
       	</li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo2250.php"> Mantenimiento de Alumnos</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-cloud text-primary"></span>
            <a href="Tdo2290.php">Reportes por Codigo</a>
         </li>
         {if $saData['CCODUSU']|in_array:['1869','1836']}
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo5020.php"> Asignación de Docentes Cursos por Jurado</a>
         </li>
         {/if}
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5030.php"> Solicitudes de Cursos por Jurado</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5070.php"> Docentes Asignados para Cursos por Jurado</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5080.php"> Solicitudes Denegadas de Cursos por Jurado</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5100.php"> Constacias Especiales</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5110.php"> Constacias Especiales Observadas</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Cnv2110.php"> Bandeja de Solicitudes Convalidación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Cnv2130.php"> Registro de Trámite de Convalidación Manual</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Tdo4210.php"> Bandeja de Solicitudes de Silabos Certificados</a>
         </li>
      </ul>	
   </div>
   <div class="panel-heading">
      <h4 class="panel-title"><!--SECRETARIA DE {$saData['CNOMUNI']} -->
         <a onclick ="funcion('B');" data-toggle="collapse" href="#collapse2" data-parent = "#accordion"><span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE DE BACHILLER</b></a>
      </h4>
   </div>
   <div id="collapse2" class="collapse" >	  
      <ul class="list-group">
         <li style="background-color: #F2DC90" class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="https://forms.gle/ijTrs6KMtmnpWRZm9"> Encuesta </a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Paq1060.php"> Pendientes</a><span class="badge">{$saData['NNROPAQ']}</span>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a onclick ="funcion('B');" href="Paq1110.php"> Expedientes Completos de Bachiller</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq1200.php"> Expedientes de Bachiller - Seguimiento</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
          <a href="Paq2050.php">Grupos de colación - Asignar revisores/alumnos</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
          <a href="Paq2070.php">Constancia de Expedientes Completos de Bachiller</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-check text-primary"></span>
          <a href="Paq1250.php">Reporte docentes revisores</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-check text-primary"></span>
            <a href="Paq1270.php">Estado docentes revisores</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-check text-primary"></span>
            <a href="Paq2610.php">Historial de Actas de Colación</a>
         </li>
          <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1190.php">Alumnos deudores de Material Didactico</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1380.php">Alumnos deudores de Material de Clínica</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-cloud text-primary"></span>
            <a href="Paq1210.php">Mantenimiento de Paquetes</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1310.php">Certificado de Conducta</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1380.php">Constancias de Clinica Aprobadas</a>
         </li>
         {if $saData['CCODUSU'] == '2445'}
            <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
               <a href="Paq1420.php">Constancias de Internado</a>
            </li>
         {/if}
      </ul>	
   </div>
    <div class="panel-heading">
      <h4 class="panel-title"><!--SECRETARIA DE {$saData['CNOMUNI']} -->
         <a onclick ="funcion('T');" data-toggle="collapse" href="#collapse3" data-parent="#accordion"><span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE DE TITULACIÓN</b></a>
      </h4>
   </div>
   <div id="collapse3" class="collapse" >	  
      <ul class="list-group">
         <li style="background-color: #F2DC90" class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="https://forms.gle/ijTrs6KMtmnpWRZm9"> Encuesta </a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Paq1060.php"> Pendientes</a><span class="badge">{$saData['NNROPAT']}</span>
         </li>
          <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Paq1480.php">Recepción de Autenticaciones</a>
         </li>
          <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Paq1530.php"> Expedientes Completos para Sustentación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a onclick ="funcion('T');" href="Paq1110.php"> Expedientes Completos de Titulación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq1200.php"> Expedientes de Titulación - Seguimiento</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq2050.php">Grupos de colación - Asignar revisores/alumnos</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq2070.php">Constancia de Expedientes Completos de Titulación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-cloud text-primary"></span>
            <a href="Paq1210.php">Mantenimiento de Paquetes</a>
         </li>
      </ul>	
   </div>
   {elseif $saData['CNIVEL'] == 'A'}      <!--DIRECTOR-->	
   <div class="panel-heading">
      <h4 class="panel-title">ADMINISTRADOR {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
            <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE PARA FIRMA </b></a>
      </h4>
   </div> 
   <div id="collapse1" class="collapse in">
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-bookmark text-success"></span>
            <a href="Paq1140.php">Beneficio de Bachiller</a>
         </li>
      </ul>
   </div>
   {elseif $saData['CNIVEL'] == '2'}      <!--DIRECTOR-->	
   <div class="panel-heading">
      <h4 class="panel-title">DIRECTOR {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
            <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE PARA FIRMA </b></a>
      </h4>
   </div> 
   <div id="collapse1" class="collapse in">
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-bookmark text-success"></span>
            <a href="Tdo2170.php"> Pendientes Auspicio</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq1200.php"> Expedientes de Bachiller - Seguimiento</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo5020.php"> Asignacion de Docentes para cursos por jurado </a>
         </li>
      </ul>
   </div>
   {elseif  $saData['CNIVEL']=='G'}      <!--ADMISION-->
   <div class="panel-heading">
      <h4 class="panel-title">ADMISIÓN {$saData['CNOMBRE']} <a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> CONSTANCIAS ESPECIALES</b></a>
      </h4>
   </div>  
   <div id="collapse1" class="collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-bookmark text-success"></span>
            <a href="Tdo5130.php"> Constacias Especiales de ingreso</a>
         </li> 
      </ul>
   </div>
   {elseif  $saData['CNIVEL']=='3'}      <!--DECANO-->
   <div class="panel-heading">
      <h4 class="panel-title">DECANO {$saData['CNOMBRE']} <a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE PARA FIRMA </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-bookmark text-success"></span>
            <a href="Tdo2170.php"> Pendientes Auspicio</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq1200.php"> Expedientes de Bachiller - Seguimiento</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo5020.php"> Asignación de Docentes Cursos por Jurado</a>
         </li>
      </ul>
   </div>
   {elseif  $saData['CNIVEL']=='4'}       <!--COORDINADOR DE LABORATORIOS-->
   <div class="panel-heading">
      <h4 class="panel-title">COORDINADOR DE LABORATORIOS {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> MANTENIMIENTOS </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2250.php">Mantenimiento de Alumnos</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2290.php">Reportes por Codigo</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1190.php">Alumnos deudores de Material Didactico</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1180.php">Alumnos deudores de Cordinación de Laboratorios</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1370.php">Solicitudes de Constancia de Clínica Pendientes</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1410.php">Levantamiento de Constancias de Clínica Observados</a>
         </li>
      </ul>
   </div>
   {elseif  $saData['CNIVEL']=='6'}            <!--JEFE DE TESORERIA-->
   <div class="panel-heading">
      <h4 class="panel-title">JEFE DE TESORERIA <br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> MANTENIMIENTO DE PAQUETES Y DOCUMENTOS </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-duplicate text-primary"></span>
            <a href="Tdo2210.php">Paquetes x Unidad Académica</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-file text-primary"></span>
            <a href="Tdo2220.php"> Documentos x Categoria</a>
         </li>     
      </ul>
   </div>
   {elseif  $saData['CNIVEL']=='7'}            <!--CONSTANCIAS - TRAMITE ACADEMICO-->
   <div class="panel-heading">
      <h4 class="panel-title">TRÁMITE ACADÉMICO <br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRAMITE ACADEMICO - CONSTANCIAS </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-duplicate text-primary"></span>
            <a href="Tdo4120.php">Trámites Pendiendes de Constancias</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo4130.php"> Busqueda de Estado de Trámite - Constancias</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-upload text-primary"></span>
            <a href="Paq1560.php"> Subir Constancia de Conducta</a>
         </li>
      </ul>
   </div>
   {elseif  $saData['CNIVEL']=='8'}      <!--JEFE DE CONTABILIDAD-->
   <div class="panel-heading">
      <h4 class="panel-title">CONTABILIDAD <br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRAMITE ACADEMICO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <!--<li class="list-group-item"><span class="glyphicon glyphicon glyphicon-search"></span>
         <a href="Tdo2320.php">Consulta de Deudas Provisionales</a>
         </li>-->
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
         </li>     
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2290.php">Reportes por Codigo</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo2720.php">Busqueda de Estado de Trámite - Certificados</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo4130.php">Busqueda de Estado de Trámite - Constancias</a>
         </li>
      </ul>
   </div> 
   {elseif $saData['CNIVEL']=='9'}      <!--COOP Y RELACIONES INTERNACIONALES-->
   <div class="panel-heading">
      <h4 class="panel-title">OFICINA DE COOP Y RELACIONES INTERNACIONALES <br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRAMITE ACADEMICO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-arrow-right text-primary"></span>
            <a href="Tdo2290.php">Relación Postulantes</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo2250.php">Mantenimiento de Alumnos</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq1350.php">Registro de Notas TOELF-ITP</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Conceptos de Pago</a>
         </li>
      </ul>
   </div> 
   {elseif $saData['CNIVEL']=='M'}        <!--MESA DE PARTES-->
   <div class="panel-heading">
      <h4 class="panel-title">MESA DE PARTES<br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE ACADEMICO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Tdo2710.php"> Certificados Pendientes por dia</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-send text-primary"></span>
            <a href="Tdo2730.php"> Certificados y Constancias Pendientes entre Fechas</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo2720.php"> Busqueda de Estado de Trámite - Certificados</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo4130.php"> Busqueda de Estado de Trámite - Constancias</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo2740.php"> Solicitudes Pendientes de Formato 4</a>
         </li>
      </ul>
   </div>
   {elseif $saData['CNIVEL'] == 'B'}        <!--POSTGRADO-->
   <div class="panel-heading">
      <h4 class="panel-title">POSTGRADO <br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE ACADEMICO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Tdo5140.php"> Constancias Especiales</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5030.php"> Solicitudes de Cursos por Jurado</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5070.php"> Docentes Asignados para Cursos por Jurado</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5080.php"> Solicitudes Denegadas de Cursos por Jurado</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2250.php">Mantenimiento de Alumnos</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2290.php">Reportes por Codigo</a>
         </li>
      </ul>   
   </div>
   {elseif $saData['CNIVEL'] == 'N'}        <!--DOCENTES REVISORES-->
   <div class="panel-heading">
      <h4 class="panel-title">DOCENTES REVISORES<br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE ACADEMICO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Paq2060.php"> Revisión de expedientes Bachiller</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Cnv2120.php"> Bandeja de Aprobación de Cursos para Convalidación</a>
         </li>
      </ul>   
   </div>
   {elseif $saData['CNIVEL']=='R'}        <!--OFICINA DE REGISTRO Y ARCHIVO ACADEMICO-->
   <div class="panel-heading">
      <h4 class="panel-title">PERSONAL TÉCNICO 
         <br>{$saData['CNOMBRE']}<br><br>
         <a data-toggle="collapse" href="#collapse1"><span class="glyphicon glyphicon-folder-open"></span><b> BANDEJA DE TRÁMITE</b></a>
      </h4>
   </div>
   <div id="collapse1" class="collapse in">	  
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2110.php"> Pendientes</a><span class="badge">{$saData['NNROPEN']}</span>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-eye-open text-success"></span>
            <a href="Tdo2120.php"> Observados</a><span class="badge">{$saData['NNROOBS']}
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-upload text-primary"></span>
            <a href="Paq1080.php"> Subir certificado de estudios</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-upload text-primary"></span>
            <a href="Paq1090.php"> Subir certificado de idioma extranjero</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-upload text-primary"></span>
            <a href="Paq1100.php"> Subir certificado de informatica</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-upload text-primary"></span>
            <a href="Tdo5150.php"> Subir certificados para firma digital</a>
         </li>    
         {if $saData['CCODUSU'] == '3070' || $saData['CCODUSU'] == '2052'}
         <li class="list-group-item"><span class="glyphicon glyphicon-upload text-primary"></span>
            <a onclick ="funcion('B');" href="Paq1200.php"> Seguimiento de bachiller</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a onclick ="funcion('B');" href="Paq1110.php"> Expedientes de Bachiller</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a onclick ="funcion('B');" href="Paq1260.php">Historial Colación de Bachiller</a>
         </li>
         {/if}
         {if $saData['CCODUSU'] == '2052'}
         <li class="list-group-item"><span class="glyphicon glyphicon-upload text-primary"></span>
            <a onclick ="funcion('T');" href="Paq1200.php"> Seguimiento de Titulacion</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a onclick ="funcion('T');" href="Paq1110.php"> Expedientes de Titulación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a onclick ="funcion('T');" href="Paq1260.php">Historial Colación de Titulación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a onclick ="funcion('M');" href="Paq1110.php"> Expedientes de Maestria</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a onclick ="funcion('D');" href="Paq1110.php"> Expedientes de Doctorado</a>
         </li>
         {/if}
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq2300.php">Generación Colación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo2250.php"> Mantenimiento de Alumnos</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Tdo2650.php"> Historial de Trámites </a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo2720.php"> Busqueda de Estado de Trámite - Certificados</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo4130.php"> Busqueda de Estado de Trámite - Constancias</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Paq1480.php">Recepcionar Trámites</a>
         </li>
         <!--{if $saData['CCODUSU'] == '2321'}
            <li class="list-group-item"><span class="glyphicon glyphicon-import text-primary"></span>
               <a href="Tdo2660.php"> Datos de los trámites</a>
            </li>
         {/if}-->
         <!--<li class="list-group-item"><span class="glyphicon glyphicon-list-alt text-success"></span>
            <a href="Tdo2150.php">Reportes de Trámites </a>
         </li> -->
         <!--<li class="list-group-item"><span class="glyphicon glyphicon-bookmark text-success"></span>
            <a href="Tdo2160.php">Certificados Generados </a>
         </li>--> 
         <!-- 
         <li class="list-group-item"><span class="glyphicon glyphicon-bookmark text-success"></span>
            <a href="Tdo2170.php"> Pendientes Auspicio</a>
         </li> -->
         <!--<li class="list-group-item"><span class="glyphicon glyphicon-adjust text-primary"></span>
            <a href="Tdo2260.php">Mantenimiento de Proveedores</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-upload text-primary"></span>
            <a href="Tdo2270.php">Subida de Certificados</a>
         </li>-->
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5050.php"> Aprobación de Cursos por Jurado</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo5060.php"> Cursos por Jurado Aprobados</a>
         </li>
      </ul>	
   </div>
   {elseif $saData['CNIVEL'] == 'L'}        <!--BIBLIOTECA-->
   <div class="panel-heading">
      <h4 class="panel-title">BIBLIOTECA<br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE ACADEMICO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Paq1450.php"> Bandeja de Pendientes Turnitin</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Paq1500.php"> Bandeja de Observados Turnitin</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Paq1510.php"> Bandeja de Empastados y CD</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Paq1520.php"> Bandeja de Observados de Empastados y CD</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Paq1550.php"> Seguimiento de Empastados Titulación Online</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2290.php">Reportes por Codigo</a>
         </li> 
      </ul>   
   </div>
   {elseif $saData['CNIVEL'] == 'F'}        <!--OFICINA DE REGISTRO Y ARCHIVO ACADEMICO-->
   <div class="panel-heading">
      <h4 class="panel-title">PERSONAL TECNICO
         <br>{$saData['CNOMBRE']}<br><br>
         <a data-toggle="collapse" href="#collapse1"><span class="glyphicon glyphicon-folder-open"></span><b> BANDEJA DE TRÁMITE</b></a>
      </h4>
   </div>
   <div id="collapse1" class="collapse in">	  
      <ul class="list-group">
         {if $saData['CCODUSU'] == '1999' OR $saData['CCODUSU'] == '1015' OR $saData['CCODUSU'] == '2035'}
            <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
               <a href="Paq1040.php"> Firmar Documentos</a>
            </li>
         {/if}
         {if $saData['CCODUSU'] == '1015' OR $saData['CCODUSU'] == '0660' OR $saData['CCODUSU'] == '0538'}
            <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
               <a href="Paq1130.php"> Aprobar Solicitudes</a>
            </li>
         {/if}
         {if $saData['CCODUSU'] == '2173' OR $saData['CCODUSU'] == '0283'}
            <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
               <a href="Paq1300.php"> Descuento de bachiller para investigación</a>
            </li>
         {/if}
      </ul>
   </div>
   {elseif $saData['CNIVEL'] == 'C'}       
   <div class="panel-heading">
      <h4 class="panel-title">VICERRECTORADO ACADÉMICO
         <br>{$saData['CNOMBRE']}<br><br>
         <a data-toggle="collapse" href="#collapse1"><span class="glyphicon glyphicon-folder-open"></span><b> BANDEJA DE TRÁMITE</b></a>
      </h4>
   </div>
   <div id="collapse1" class="collapse in">    
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Cnv2140.php"> Bandeja de Solicitudes de Convalidacion</a>
         </li>
      </ul>
   </div>
   {elseif $saData['CNIVEL'] == 'V'}       
   <div class="panel-heading">
      <h4 class="panel-title">VICERRECTORADO ADMINISTRATIVO
         <br>{$saData['CNOMBRE']}<br><br>
         <a data-toggle="collapse" href="#collapse1"><span class="glyphicon glyphicon-folder-open"></span><b> BANDEJA DE TRÁMITE</b></a>
      </h4>
   </div>
   <div id="collapse1" class="collapse in">	  
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Paq1210.php"> Paquetes Online</a>
         </li>
      </ul>
   </div>
   {elseif $saData['CNIVEL'] == 'D'}        <!--DIGITALIZACIÓN DE IMAGEN-->
   <div class="panel-heading">
      <h4 class="panel-title">DIGITALIZACION DE IMAGEN 
         <br>{$saData['CNOMBRE']}<br><br>
         <a data-toggle="collapse" href="#collapse1"><span class="glyphicon glyphicon-folder-open"></span><b> BANDEJA DE TRÁMITE</b></a>
      </h4>
   </div>
   <div id="collapse1" class="collapse in">	  
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a onclick="funcion('B');" href="Paq1060.php"> Pendientes</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-ok text-primary"></span>
            <a href="Paq1240.php"> Aprobados</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-eye-open text-primary"></span>
            <a href="Paq1290.php"> Observados</a>
         </li>
      </ul>
   </div>

   {elseif  $saData['CNIVEL']=='I'}      <!--INFORMATICA-->
   <div class="panel-heading">
      <h4 class="panel-title">OFICINA DE INFORMÁTICA <br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRAMITE ACADEMICO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo2250.php">Mantenimiento de Alumnos</a>
         </li>
      </ul>
   </div> 
   {elseif  $saData['CNIVEL']=='U'}       <!--CURSOS COMPLEMENTARIOS-->
   <div class="panel-heading">
      <h4 class="panel-title">{$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> CURSOS COMPLEMENTARIOS </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2250.php">Mantenimiento de Alumnos</a>
         </li> 
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2290.php">Reportes por Codigo</a>
         </li> 
      </ul>
   </div>
   {elseif  $saData['CNIVEL']=='S'}       <!--INSTITUTOS UCSM-->
   <div class="panel-heading">
      <h4 class="panel-title">{$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> INSTITUTOS UCSM </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2110.php"> Pendientes</a><span class="badge">{$saData['NNROPEN']}</span>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-eye-open text-success"></span>
            <a href="Tdo2120.php"> Observados</a><span class="badge">{$saData['NNROOBS']}</span>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-list text-primary"></span>
            <a href="Tdo2650.php"> Historial de Trámites </a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-flag text-primary"></span>
            <a href="Tdo2720.php"> Busqueda de Estado de Trámite</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo2250.php"> Mantenimiento de Alumnos</a>
         </li>
         {if $saData['CCODUSU'] == 'I020'}
         <li class="list-group-item"><span class="glyphicon glyphicon-education text-primary"></span>
            <a href="Tdo5120.php"> Constancias Especiales</a>
         </li>
         {/if} 
      </ul>
   </div>
   {elseif  $saData['CNIVEL']=='P'}       <!--OFICINA DE PRESUPUESTO-->
   <div class="panel-heading">
      <h4 class="panel-title">{$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> OFICINA DE PRESUPUESTO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="collapse in">          
      <ul class="list-group">
	     <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2220.php">Mantenimiento de Documentos</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-search text-primary"></span>
            <a href="Tdo2290.php">Reportes por Codigo</a>
         </li> 
      </ul>
   </div>
   {elseif $saData['CNIVEL'] == 'J'}        <!--JEFE DE DEPARTAMENTO-->
   <div class="panel-heading">
      <h4 class="panel-title">JEFE DE DEPARTAMENTO<br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE ACADEMICO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Cnv2110.php"> Bandeja de Solicitudes Convalidación</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Cnv2120.php"> Bandeja de Aprobación de Cursos para Convalidacin</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Tdo4210.php"> Bandeja de Solicitudes de Silabos Certificados</a>
         </li>
      </ul>   
   </div>
   {elseif $saData['CNIVEL'] == 'E'}        <!--SECRETARIA GENERAL-->
   <div class="panel-heading">
      <h4 class="panel-title">SECRETARIA GENERAL<br> {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> TRÁMITE ADMINISTRATIVO </b></a>
      </h4>
   </div>  
   <div id="collapse1" class="panel-collapse collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Tdo4170.php"> Bandeja de Autenticaciones Pendientes</a>
         </li>
         <li class="list-group-item"><span class="glyphicon glyphicon-inbox text-primary"></span>
            <a href="Tdo4180.php"> Búsqueda de Trámites - Autenticaciones</a>
         </li>
      </ul>   
   </div>
   {elseif $saData['CCENCOS'] == '034'}      <!--BIBLIOTECA--> 
   <div class="panel-heading">
      <h4 class="panel-title">BIBLIOTECA {$saData['CNOMBRE']}<a data-toggle="collapse" href="#collapse1"><br><br>
         <span class="glyphicon glyphicon-folder-open"></span><b> CONSTANCIAS DE NO ADEUDAR MATERIAL BIBLIOGRÁFICO </b></a>
      </h4>
   </div> 
   <div id="collapse1" class="collapse in">          
      <ul class="list-group">
         <li class="list-group-item"><span class="glyphicon glyphicon-bookmark text-success"></span>
            <a href="Tdo1250.php">Trámites Pendientes</a>
         </li> 
      </ul> 
   </div>
   {/if}
</div>
</div>
   {if $saOption neq 0}
   <a href="../UCSMERP/Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block"><i class="glyphicon glyphicon-log-out"></i> REGRESAR ERP</a>
   {else}
   <button type="submit" name="Boton" value="Cerrar Sesion" class="center-block btn btn-danger btn-lg btn-block"><i class="glyphicon glyphicon-log-out"></i> Cerrar Sesión</button>
   {/if}
</div> 
   <div class="col-sm-3"></div>       
   </div>
</div>
</form> 
</div> 
<div id="footer">
</div>
</body>
</html>