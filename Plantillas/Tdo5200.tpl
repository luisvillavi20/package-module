<!DOCTYPE html>
<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
      <style type="text/css">
      *{
         margin: 0px;
         padding: 0px;
      }
      .menu{
         margin: auto;
         font-size: 12px;
         margin-top: 4%;
         width: 200px;
      }
      .menu.segundo{
      }
      .menu.segundo.boton a{
         border: 2px solid #0062AE;
         font-size: 18px;
         padding-top: 10px;
         margin: 5px;
         float: left;
         border-radius: 15px;
         height: 100px;
         cursor: pointer;
         text-align: center;
         text-decoration: none;
         width: 200px;
         height: 130px;
      }
      .menu.segundo.boton img{
         width: 90px;
         display: flex;
         justify-content: center !important;
         margin: 0px auto;
      }
      .menu.segundo.boton p{
         font-size: 14px;
         display: flex;
         justify-content: center;
         color: #0062AE;
         font-style: none;
      }
      .menu.segundo.boton:hover p{
         justify-content: center;
         color: #0062AE;
         font-size: 15px;
      }
      .menu.segundo.boton:hover img{
         width: 95px;
      }
   </style>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    AUTENTIFIACION DE DOCUMENTOS</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="main.php" method="post">
   <div class="container">
      {if $snBehavior eq 0}
         <div class="menu">
            <div class="menu segundo">
              <div class="menu segundo boton">
                 <a href="Tdo2760.php">
                   <div><img src="Images/deuda.png"></div>
                   <div><p>GENERAR CODIGO DE PAGO</p></div>
                 </a>
              </div>
            </div>
         </div>
         <div class="menu">
            <div class="menu segundo">
              <div class="menu segundo boton">
                 <a href="Tdo2750.php">
                   <div><img src="Images/Pdf.ico"></div>
                   <div><p>SUBIR DOCUMENTACION</p></div>
                 </a>
              </div>
            </div>
         </div>
      {/if}
   </div>
</form>
</div>
<div id="footer">
</div>
</body>
</html>
