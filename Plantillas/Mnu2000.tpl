<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
	<script>
		function f_cambiarCodigo() {
			var loForm = document.getElementById("form");
			var loId = document.createElement("input");
			loId.setAttribute("name", "Id");
			loId.setAttribute("value", "Cambiar");
			loId.setAttribute("type", "hidden");
			loForm.appendChild(loId);
			loForm.submit();
		}
	</script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody">
   <form action="Mnu0000.php" id="form" method="post"> 
   <div class="row">
   <div class="col-sm-3"></div>  
   <div class="col-sm-6">
      <div class="panel-default">
      <div class="panel-group">
         <div class="panel">
            <div class="panel-heading" >
               {if $saData['CESTENT'] == 0}
                  <h4 class="panel-title" title="{$saData['CNRODNI']}">
                     {$saData['CNRODNI']} - {$saData['CNOMBRE']}
                     <br><br>
                     <b>{$saData['CCODALU']} - {$saData['CNOMUNI']}</b>
                     <!--<b><a data-toggle="collapse" href="#collapse1"><br><br>
                           <span class="glyphicon glyphicon-folder-open" disabled></span> BACHILLERATO ONLINE</a></b-->  
                  </h4>
               {elseif $saData['CESTENT'] == 1}
                  <h4 class="panel-title" title="">{$saData['CNRORUC']}<br>{$saData['CRAZSOC']}<br>{$saData['CNRODNI']}-{$saData['CNOMBRE']}
                     <b><a data-toggle="collapse" href="#collapse1"><br><br>
                           <span class="glyphicon glyphicon-folder-open"></span> AUSPICIO ACADEMICO</a></b>
                  </h4>
               {/if}
            </div>
            <div class="panel-body panel-default">
            <div id="collapse1" class="collapse in">
               <ul class="list-group  panel-default">
                  {if $saData['CCODIGO'] == 'T'}
                     <!--li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                     <a href="Paq1470.php">GENERAR DEUDA PROVISIONAL AUTENTICACION DE DOCUMENTOS SOLO PARA TITULACION ON LINE</a>
                     </li-->
                     <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                        <a href="Paq1540.php">TESIS PARA TURNITIN</a>
                     </li>
                     <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                        <a href="Paq1490.php">SUBIR LOS DOCUMENTOS AUTENTICADOS PARA LA TITULACIÓN</a>
                     </li>
                  {/if}
                  <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                    <a href="Paq1010.php">GENERAR DEUDA PROVISIONAL</a>
                  </li>
                  <li class="list-group-item  panel-default"><span class="glyphicon glyphicon-search text-success"></span>
                     <a href="Paq1020.php">ESTADO DE DEUDA PROVISIONAL</a>	
                  </li>
                  <li class="list-group-item panel-default"><span class="glyphicon glyphicon-comment text-success"></span>
                     <a href="Paq1030.php">LLENADO DE FORMULARIO</a>	
                  </li>
                  <li class="list-group-item  panel-default"><span class="glyphicon glyphicon-upload text-success"></span>
                    <a href="Paq1070.php">SUBIR DOCUMENTOS</a>	
                  </li>
                  <li class="list-group-item panel-default"><span class="glyphicon glyphicon-file text-success"></span>
                     <a href="Paq1050.php">ESTADO DE TRÁMITES</a>	
                  </li>
               </ul>
            </div>
            </div>
         </div>	
         <a href="Mnu0000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
   </div>
   </div>
   </div>
   <div class="row">
   <div class="col-sm-3"></div>
   <div class="col-sm-6"><div class="panel panel-default">
   <div class="panel-heading">
     <h3 class="panel-title"><p class="text-justify"><b>MANUAL DE USUARIO</b></p></h3>
   </div>   
   <div class="panel-body "> 
      <p class="text-center"><a class ="center-block btn btn-success btn-lg btn-block" href="./Resumen_Bachillerato.pdf" download>DESCARGAR MANUAL - PAQUETES</a>
   </div>
   </div>
   </div> 
   <div class="col-sm-3"></div>
   </div> 
</form>
</div>
<div id="footer">
</div>
</body>
</html>