<html>
<head>
   <title>Bachiller</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <!--<script>
      $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip();
         if ($("#llModal").val() == 0) {
            $('#modalReq').modal('show');
         }         
      });

      $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip();
         if ($("#llModal").val() == 0) {
            $('#modalReqTit').modal('show');
         }         
      });
  </script>-->
</head>
<body class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos - Carpeta de Documentos </b></h3></div>
<div class="container-fluid">
   <input type="hidden" id="llModal" value="{$saData['LLMODAL']}">
  <form action="Paq1070.php" method="post" enctype="multipart/form-data">
  {if $snBehavior == 0}
  <div class="row col-md-10 col-md-push-1">
    <div class="panel panel-default">
      <div class="panel-heading"><h3 class="panel-title"><b>CARPETA</b></h3></div>
        <div class="table table-condensed table-responsive">
         <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>       
          <table class="table table-condensed table-striped">
            <thead>
              <th style="text-align: center" class="col-xs-2"><label>CÓDIGO</label></th>
              <th style="text-align: center" class="col-xs-5"><label>DESCRIPCIÓN</label></th>
              <th style="text-align: center" class="col-xs-2"><label>SELECCIONAR ARCHIVO</label></th>
              <th style="text-align: center" class="col-xs-2"><label>SUBIR ARCHIVO</label></th>
              <th style="text-align: center" class="col-xs-1"><label>ESTADO</label></th>
            </thead>
            <tbody>
              {$j=0}
              {foreach from = $saDatos item = i}
                {if $i['CIDCATE'] != 'PD0001'}
                  <form action="Paq1070.php" method="POST" enctype="multipart/form-data">
                  {if $i['CIDCATE'] == 'PQ0027'}
                    <tr class="bg-danger">
                  {else}
                    <tr>
                  {/if}
                    <td style="text-align: center">{$i['CIDCATE']}</td>
                    <td>{$i['CDESDOC']}</td>
                    {if $i['CIDCATE'] == 'PDAOTR' OR $i['CIDCATE'] == 'PDADOC' OR $i['CIDCATE'] == 'PDACBU' }
                      <td style="text-align: center">
                        {if $i['CESTADO'] == 'A'}
                          <input type="hidden" name="paData[CCODTRE]" value="{$i['CCODTRE']}">
                          <input name="pfDocBac" accept=".pdf, .jpg" type="file" class="form-control-file" required>
                        {/if}
                      </td>
                      <td>
                        {if $i['CESTADO'] == 'A' AND $i['CIDCATE'] != 'PQ0009'}
                          <button type="submit" name="Boton" value="Subir" onclick="confirm('¿Está seguro que desea subir éste archivo? Esta operación no podrá ser revertida.')" class="center-block btn"/><i class="glyphicon glyphicon-upload"></i> Subir</button>
                        {/if}
                      </td>
                      <td style="text-align: center">
                        {if $i['CESTADO'] == 'F'}
                          <img src="Images/pendiente.png" width="30%" data-toggle="tooltip" data-placement="button" title="Pendiente">
                        {elseif $i['CESTADO'] == 'A'}
                          <img src="Images/subido.png" width="30%" data-toggle="tooltip" data-placement="button" title="Subido">
                        {elseif $i['CESTADO'] == 'R'}
                          <img src="Images/revisado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Revisado">
                        {elseif $i['CESTADO'] == 'E'}
                          <img src="Images/observado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Observado">
                        {elseif $i['CESTADO'] == 'B'}
                          <img src="Images/aprobado2.png" width="30%" data-toggle="tooltip" data-placement="button" title="Aprobado">
                        {elseif $i['CESTADO'] == 'S'}
                          <img src="Images/mesa.png" width="30%" data-toggle="tooltip" data-placement="button" title="Subido">
                        {elseif $i['CESTADO'] == 'M'}
                          <img src="Images/mesa.png" width="30%" data-toggle="tooltip" data-placement="button" title="Mesa de partes">
                        {/if}
                      </td>
                    {elseif (substr($i['CIDCATE'], 0, 2) != 'PQ' OR $i['CIDCATE'] == 'PQ0002' OR $i['CIDCATE'] == 'PQ0033' OR $i['CIDCATE'] == 'PQ0034') AND ($i['CIDCATE'] != 'PDAOTR' OR $i['CIDCATE'] != 'PDADOC') }
                      <td></td>
                      <td></td>
                      <td style="text-align: center">
                        {if $i['CESTADO'] == 'F'}
                          <img src="Images/pendiente.png" width="30%" data-toggle="tooltip" data-placement="button" title="Pendiente">
                        {elseif $i['CESTADO'] == 'A'}
                          <img src="Images/subido.png" width="30%" data-toggle="tooltip" data-placement="button" title="Subido">
                        {elseif $i['CESTADO'] == 'R'}
                          <img src="Images/revisado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Revisado">
                        {elseif $i['CESTADO'] == 'E'}
                          <img src="Images/observado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Observado">
                        {elseif $i['CESTADO'] == 'B'}
                          <img src="Images/aprobado2.png" width="30%" data-toggle="tooltip" data-placement="button" title="Aprobado">
                        {elseif $i['CESTADO'] == 'S'}
                          <img src="Images/mesa.png" width="30%" data-toggle="tooltip" data-placement="button" title="Subido">
                        {elseif $i['CESTADO'] == 'M'}
                          <img src="Images/mesa.png" width="30%" data-toggle="tooltip" data-placement="button" title="Mesa de partes">
                        {/if}
                      </td>
                    {else}    
                      <td style="text-align: center">
                        {if $i['CESTADO'] == 'E' OR $i['CESTADO'] == 'F' AND $i['CIDCATE'] != 'PQ0009'}
                          <input type="hidden" name="paData[CCODTRE]" value="{$i['CCODTRE']}">
                          <input name="pfDocBac" accept=".pdf, .jpg" type="file" class="form-control-file" required>
                        {/if}
                      </td>
                      <td>
                        {if $i['CESTADO'] == 'E' OR $i['CESTADO'] == 'F' AND $i['CIDCATE'] != 'PQ0009'}
                          <button type="submit" name="Boton" value="Subir" onclick="confirm('¿Está seguro que desea subir éste archivo? Esta operación no podrá ser revertida.')" class="center-block btn"/><i class="glyphicon glyphicon-upload"></i> Subir</button>
                        {/if}
                      </td>
                      <td style="text-align: center">
                        {if $i['CESTADO'] == 'F'}
                          <img src="Images/pendiente.png" width="30%" data-toggle="tooltip" data-placement="button" title="Pendiente">
                        {elseif $i['CESTADO'] == 'A'}
                          <img src="Images/subido.png" width="30%" data-toggle="tooltip" data-placement="button" title="Subido">
                        {elseif $i['CESTADO'] == 'R'}
                          <img src="Images/revisado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Revisado">
                        {elseif $i['CESTADO'] == 'E'}
                          <img src="Images/observado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Observado">
                        {elseif $i['CESTADO'] == 'B'}
                          <img src="Images/aprobado2.png" width="30%" data-toggle="tooltip" data-placement="button" title="Aprobado">
                        {elseif $i['CESTADO'] == 'S'}
                          <img src="Images/mesa.png" width="30%" data-toggle="tooltip" data-placement="button" title="Subido">
                        {elseif $i['CESTADO'] == 'M'}
                          <img src="Images/mesa.png" width="30%" data-toggle="tooltip" data-placement="button" title="Mesa de partes">
                        {/if}
                      </td>
                    {/if}
                </tr>
                </form>
                  {$j = $j + 1}
                {else}
                  {$j = $j + 1}
                {/if}
              {/foreach} 
            </tbody>        
        </table>
      </div>
    </div>
    {if $i['CIDCATE'] == 'PQ0027'}
    <div class="alert alert-danger" align="left">
        <h4><u>Consideraciones para la revisión de Documentos:</u></h3> 
        <h5>LA CONSTANCIA DE PRIMERA MATRICULA DE LA UNIVERSIDAD DE ORIGEN, debe de ser presentada de manera física en la Escuela a la cual pertenece.</h4> 
    </div>
    {/if}
    <div class="row">
      <div class="col-sm-4">
        <a href="https://forms.gle/W81aX6pQM1x6zmYc7" class="center-block btn btn-primary btn-lg btn-block" role="button"><i class="glyphicon glyphicon-list"></i> Encuesta</a>
      </div>
      {if $saData['CCODIGO']== 'B'}
        <div class="col-sm-4">
          <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modalReq" role="button">Mostrar especificaciones&nbsp;&nbsp;<i class="glyphicon glyphicon-th-list"></i></a>
        </div>
      {elseif $saData['CCODIGO']== 'T'}
        <div class="col-sm-4">
          <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modalReqTit" role="button">Mostrar especificaciones&nbsp;&nbsp;<i class="glyphicon glyphicon-th-list"></i></a>
        </div>
      {else}
        <div class="col-sm-4">
          <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modalReqDocMae" role="button">Mostrar especificaciones&nbsp;&nbsp;<i class="glyphicon glyphicon-th-list"></i></a>
        </div>
      {/if}
      <div class="col-sm-4">
        <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
     </div>
     <div class="row"></div>
  </div>
  {/if}
<!-- Modales-->
<div id="ModalNuevo" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="well well-sm text-success">
            <h3 class="modal-title">ESPECIFICACIONES</h3>
        </div>
        <div class="modal-body">
          <span id="divData">LA CONSTANCIA DE PRIMERA MATRICULA DE LA UNIVERSIDAD DE ORIGEN, DEBERA SER ENTREGADA DE MANERA FISICA EN LA ESCUELA A LA CUAL USTED PERTENECE</span>
        </div>
       <div class="modal-footer">
          <div class="button-group text-right">
            <button class="btn btn-success" type="submit" type="submit" name="Boton" value="Subir" class="center-block btn"/>Subir</button>
            <input class="btn btn-danger" type="submit" class="boton" data-dismiss="modal" value="Cerrar"/>
          </div>
       </div>
    </div>
  </div>
</div>
<div id="ModalNuevo2" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 class="modal-title">ESPECIFICACIONES</h3>
        </div>
        <div class="modal-body">
            <span id="divData">CONSTANCIA DE PRIMERA MATRICULA DE LA UNIVERSIDAD DE ORIGEN</span>
         </div>
      <div class="modal-footer">
         <div class="button-group text-right">
            <input class="btn btn-success" type="submit" class="boton" name="Boton" value="Insertar"/>
            <input class="btn btn-danger" type="submit" class="boton" data-dismiss="modal" value="Cerrar"/>
         </div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
<div class="modal fade" id="modalReq">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3 class="modal-title">ESPECIFICACIONES</h3>
   </div>
   <div class="modal-body">
      <ol>
      <b>Especificaciones de Documentos</b></li>
      <ol><li> Debe subir un archivo a la vez</li>
      <li> Los formatos deben ser PDF para todos los documentos excepto en el caso de la fotografía</li>
      <li> Los archivos no deben exceder los 5MB cada uno</li></ol>
      <br>
      <b>Especificaciones de Fotografía</li></b>
      <ol><li> Formato JPG, dimensiones: 378 Píxeles x 491 Píxeles</li>
      <li> A colores, fondo blanco, mirando de frente, sin lentes.</li>
      <li> Saco oscuro y corbata para varones, traje sastre para las damas.</li></ol>
      <br>
      <b>Especificaciones de Declaración Jurada</li></b>
      <ol><li> El documento debe tener la huella digital del alumno.</li>
      <li> La firma debe coincidir con la que actualmente se encuentra en el DNI.</li>
      <li> Solo se especifica no tener antecedentes penales y judiciales.</li></ol>      
      <br>
      <b>Especificaciones del DNI</li></b>
      <ol><li> El documento debe ser vigente</li>
      <li> El documento debe ser escaneado por ambas caras.</li></ol>
      Los documentos deben ser escaneados en buena calidad y a color.
   </div>
   <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>
</div>
</div>
<div class="modal fade" id="modalReqTit">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3 class="modal-title">ESPECIFICACIONES</h3>
   </div>
   <div class="modal-body">
      <ol>
      <b>Especificaciones de Documentos</b></li>
      <ol><li> Debe subir un archivo a la vez</li>
      <li> Los formatos deben ser PDF para todos los documentos excepto en el caso de la fotografía</li>
      <li> Los archivos no deben exceder los 5MB cada uno</li></ol>
      <br>
      <b>Especificaciones de Fotografía</li></b>
      <ol><li> Formato JPG, dimensiones: 378 Píxeles x 491 Píxeles</li>
      <li> A colores, fondo blanco, mirando de frente, sin lentes.</li>
      <li> Saco oscuro y corbata para varones, traje sastre para las damas.</li></ol>
      <br>
      <b>Especificaciones del DNI</li></b>
      <ol><li> El documento debe ser vigente</li>
      <li> El documento debe ser escaneado por ambas caras.</li></ol>
      <br>
      <b>Documentos de Autenticación </li></b>
      <ol><li> Usted debe personarse a Secretaria General para que se lleve a cabo la autenticación del documento</li>
      <li> Luego los documentos autenticados deben ser escaneados por ambas caras y subidos a esta plataforma.</li>
      <li> Por último el documento autenticado debe ser entregado en la Oficina de la ORAA</li>
      <li> Los documentos deben ser escaneados en buena calidad y a color.</li></ol>
   </div>
   <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>
</div>
</div>

<div class="modal fade" id="modalReqDocMae">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3 class="modal-title">ESPECIFICACIONES</h3>
   </div>
   <div class="modal-body">
      <ol>
      <b>Especificaciones de Documentos</b></li>
      <ol><li> Debe subir un archivo a la vez</li>
      <li> Los formatos deben ser PDF para todos los documentos excepto en el caso de la fotografía</li>
      <li> Los archivos no deben exceder los 5MB cada uno</li></ol>
      <br>
      <b>Especificaciones de Fotografía</li></b>
      <ol><li> Formato JPG, dimensiones: 378 Píxeles x 491 Píxeles</li>
      <li> A colores, fondo blanco, mirando de frente, sin lentes.</li>
      <li> Saco oscuro y corbata para varones, traje sastre para las damas.</li></ol>
      <br>
      <b>Especificaciones del DNI</li></b>
      <ol><li> El documento debe ser vigente</li>
      <li> El documento debe ser escaneado por ambas caras.</li></ol>
      <br>
      <b>Especificaciones del Diploma</li></b>
      <ol><li> El Escaneo del Diploma debe contener ambas caras del diploma en un solo archivo PDF</li>
      <li>Si el diploma es de alguna Universidad externa EL ARCHIVO PDF DEBE CONTENER UNA DECLARACION JURADA AVALANDO LA VERACIDAD DEL DIPLOMA</li>
      <li> Los documentos deben ser escaneados en buena calidad y a color.</li>
      </ol>
   </div>
   <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>
</div>
</div>

<div id="footer">
</div>
</body>
</html>