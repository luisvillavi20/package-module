<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      function f_ValidaNumericos(e) {
         if (e.which == 8) {
            return true;
         }
         if (e.key < '0' || e.key > '9') {
            return false;
         }
         return true;
      }
      function f_ValidaAlumno(e) {
         var lcNroDni = document.getElementById('text').value; 
         if (lcNroDni.length == 7) {
            $.get("Paq1180.php?Id=Verificar&CNRODNI=" + lcNroDni + e.key, (data) => {
               let json = JSON.parse(data);
               if (json['ERROR'] != undefined) {
                  $('#cNomAlu').css("color", "#ce4646");
                  $('#cNomAlu').text(json.ERROR);
               } else {
                  $('#cNomAlu').css("color", "#333333");
                  $('#cNomAlu').text(json.CNOMBRE);
               }
            });
         }
      }
</script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody table-responsive">
<form action="Paq1180.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE ALUMNOS DEUDORES - COORDINACION DE LABORATORIOS</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <div class="table-responsive mh-50">
            <table class="table table-condensed">
               <thead>
                  <tr>
                  <th class="col-xs-3" style='text-align: center'>Fecha Generación</th>
                  <th class="col-xs-2" style='text-align: center'>DNI</th>
                  <!--<th class="col-xs-2" style='text-align: left'>Cod.Alumno</th>-->
                  <th class="col-xs-4" style='text-align: left'>Nom.Alumno</th>
                  <!--<th class="col-xs-2" style='text-align: left'>Unidad Academica</th>-->
                  <th class="col-xs-3" style='text-align: center'>Activar</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                     <tr>
                     <td style='text-align: center'>{$i['DGENERA']}</td>
                     <td style='text-align: center'>{$i['CNRODNI']}</td>
                     <!--<td style='text-align: left'>{$i['CCODALU']}</td>-->
                     <td style='text-align: left'>{$i['CNOMBRE']}</td>
                     <!--<td style='text-align: left'>{$i['CNOMUNI']}</td>-->
                     <td style='text-align: center'><input type="radio" name="paData[NSERIAL]"  value="{$i['NSERIAL']}"/></td>
                     </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div id="Agregar" class="col-sm-4">
            <button type="submit" name="Boton" value="Agregar" class="center-block btn btn-success btn-lg btn-block"> Agregar <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div id="Activar" class="col-sm-4">
            <button type="submit" name="Boton" value="Detalle" class="center-block btn btn-primary btn-lg btn-block"> Detalle <i class="glyphicon glyphicon-eye-open"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 1}
      <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DE DEUDA</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">    
         <div class="col-md-12 form-line">  
            <div class="form-group">
               <input type="hidden" name="paData[NSERIAL]" value="{$saData['NSERIAL']}">
               <table class="table table-condensed">   
                  <tr class="warning2">
                     <th width= '30%'><label>Datos del Alumno:</label></th>
                     <th></th>
                  </tr><tr>
                     <th><label>DNI: </label></th>
                     <th><label>{$saData['CNRODNI']}</label></th>
                  </tr><tr>
                     <th><label>Nombres: </label></th>
                     <th><label>{$saData['CNOMBRE']}</label></th>
                  </tr><!--<tr>
                     <th><label>Código: </label></th>
                     <th><label>{$saDatos['CCODALU']}</label></th>
                  </tr>-->
                  <tr class="warning2">
                     <th>Observación:</th>
                     <th></th>
                  </tr><tr>
                     <th>Descripción: </th>
                     <th>
                        <textarea rows="3" class="form-control" name="saData[MDESCRI]" placeholder="{$saData['MDESCRI']}" readonly="true"></textarea>
                     </th>
                  </tr>
               </table>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" onclick="return confirm('¿Esta seguro que desea Levantar la deuda al alumno?')" name="Boton" value="Remover" class="center-block btn btn-warning btn-lg btn-block"> Remover <i class="glyphicon glyphicon-remove"></i></button>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Paq1180.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 2}
      <div class="panel-heading"><h3 class="panel-title"><b>AÑADIR NUEVO ALUMNO </b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">    
         <div class="col-md-12 form-line">  
            <div class="form-group">
               <table class="table table-condensed">   
                    <tr class="warning2">
                        <th width= '30%'><label>Datos del Alumno:</label></th>
                        <th></th>
                     </tr><tr>
                        <th><label>DNI del Alumno: </label></th>
                        <th>
                           <input type="text" id="text" class="form-control" name="paData[CNRODNI]" maxlength="8" placeholder="Ingrese DNI de alumno" required onkeydown="f_ValidaAlumno(event);" onkeypress="return f_ValidaNumericos(event);">
                        </th>
                     </tr><tr>
                        <th><label>Nombres: </label></th>
                        <th><label><b id="cNomAlu" style="width: 250px"></b></label></th>
                     </tr>
                     <tr class="warning2">
                        <th>Ingreso detalle de observación:</th>
                        <th></th>
                     </tr><tr>
                        <th>Descripción: </th>
                        <th>
                           <textarea rows="3" class="form-control" name="paData[MDESCRI]"></textarea>
                        </th>
                      </tr>
                  </table>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" onclick="return confirm('¿Esta seguro que desea Añadir a este Alumno?')" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4"> </div>
         <div class="col-sm-4">
            <a href="Paq1180.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer">
</div>
</body>
</html>