<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script>
   function f_verDiploma(e){
      $lcNroDni = e.value;
      window.open("DocumentoPaquete.php?CNRODNI="+$lcNroDni+"&CCODTRE=000001");
   }

   function fileValidator(e)
   {
      alert("Archivo "+e.value.replace("C:\\fakepath\\", "")+" seleccionado correctamente");
   }
   </script>
   <style>
   </style>
</head>
<body class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Solicitudes de Autenticacion</b></h3></div>
<div class="container-fluid divBody">
<form action="Paq1480.php" method="post" enctype="multipart/form-data">
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title"><b>Bandeja de Solicitudes de Autenticación</b></h3></div>
<div class="panel-body">
   <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>     
   <div class="table-responsive">
   <table class="table table-condensed"> 
      <thead>
         <tr> 
            <th class="col-xs-1">Código Alumno</th>
            <th class="col-xs-1">Tipo</th> 
            <th class="col-xs-1">DNI</th> 
            <th class="col-xs-3">Nombre Alumno</th>
            <th class="col-xs-3">Unidad Académica</th>
            <th style="text-align: center" class="col-xs-1">Ver</th>
            <th style="text-align: center" class="col-xs-1">Aprobar</th>
            <th style="text-align: center" class="col-xs-1">Observar</i></th>
         </tr>
      </thead>
      <tbody>
      {foreach from = $saDatos item = i}
         <form action="Paq1480.php" method="POST" enctype="multipart/form-data">
         <tr>
            <!--<input type="hidden" name = "pcCodTre" value="{$i['CCODTRE']}">-->
            <input type="hidden" name = "pcNSerial" value="{$i['NSERIAL']}">
            <td class="col-xs-1">{$i['CCODALU']}</td>  
            <td class="col-xs-3">{$i['CTIPO']}</td>  
            <td class="col-xs-1">{$i['CNRODNI']}</td>
            <td class="col-xs-1">{$i['CNOMBRE']}</td>  
            <td class="col-xs-3">{$i['CNOMUNI']}</td>
            {if $i['CTIPO'] == 'DIPLOMA'}
            <td><button class="btn btn-default btn-block btn-xs" onClick="window.open('DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE=000002', 'Algo', 'height=850, width=850');">
            {else}
            <td><button class="btn btn-default btn-block btn-xs" onClick="window.open('DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE=000003', 'Algo', 'height=850, width=850');">
            {/if}
				<img src="Images/eye.png" width="20" height="20"></button></td>   
            <td><button class="btn btn-default btn-block btn-xs" name="Boton" value="Confirmar" onclick="return confirm('¿Seguro(a) que desea dar la conformidad de este tramite?')">
				<img src="Images/aprobado.png" width="20" height="20"></button></td>  
            <td><button class="btn btn-default btn-block btn-xs" name="Boton" value="Observar"  onclick="return confirm('¿Seguro(a) que desea habilitar este tramite?')">
				<img src="Images/alerta.png" width="20" height="20"></button></td>  
         </tr>
         </form>
      {/foreach}
      </tbody>      
   </table>
   </div>  
</div>
</div>
<div>
   <div class="col-xs-3"></div>
   <div class="col-xs-6"><a href="Mnu1000.php" role="button" class="btn btn-danger btn-block btn-lg">Salir</a></div>
   <div class="col-xs-3"></div>
</div>
</div>
</form>
<div id="footer">
</div>
</div>
</body>
</html>