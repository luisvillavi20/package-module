<html>

<head>
   <title>REGISTRO Y MANTENIMIENTO DE USUARIOS</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>
   <script>
      function f_ValidaNumericos(e) {
         if (e.which == 8) {
            return true;
         }
         if (e.key < '0' || e.key > '9') {
            return false;
         }
         return true;
      }
      function f_ValidaPersona(e) {
         var lcNroDni = document.getElementById('text').value;
         if (lcNroDni.length == 7) {
            $.get("Paq1170.php?Id=Verificar&CNRODNI=" + lcNroDni + e.key, (data) => {
               let json = JSON.parse(data);
               if (json['ERROR'] != undefined) {
                  $('#cNombre').css("color", "#ce4646");
                  $('#cNombre').text(json.ERROR);
               } else {
                  $('#cNombre').css("color", "#333333");
                  $('#cNombre').text(json.CNOMBRE);
               }
            });
         }
      }
      function f_ValidaUsuario(e) {
         var lcNroDni = document.getElementById('text').value;
         if (lcNroDni.length == 7) {
            $.get("Paq1170.php?Id=VerificarUsu&CNRODNI=" + lcNroDni + e.key, (data) => {
               let json = JSON.parse(data);
               if (json['ERROR'] != undefined) {
                  $('#cNombre').css("color", "#ce4646");
                  $('#cNombre').text(json.ERROR);
                  document.getElementById("pcCodUsu").readOnly = false;
                  document.getElementById('pcCodUsu').value = '';
               } else if (json['CCODUSU'] == undefined) {
                  $('#cNombre').css("color", "#333333");
                  $('#cNombre').text(json.CNOMBRE);
                  document.getElementById("pcCodUsu").readOnly = false;
                  document.getElementById('pcCodUsu').value = '';
               } else {
                  $('#cNombre').css("color", "#333333");
                  $('#cNombre').text(json.CNOMBRE);
                  $('#pcCodUsu').val(json.CCODUSU);
                  document.getElementById("pcCodUsu").readOnly = true;
               }
            });
         }
      }
   </script>
</head>
<div class="panel-heading" style="background-color: #245433; color: #ffffff;">
   <h3><b>Trámite Académico - Administrativo</b></h3>
</div>
<div class="container">     
   <div class="row">
      <div class="col-sm-12">
         {if $snBehavior == 0}
         <form action="Paq1170.php" method="post">
         <div class="panel panel-success">
            <div class="panel-heading">
               <h4>Mantenimiento de Usuarios</h4>
            </div>
            <div class="panel-body">                  
               <p class="text-muted"><b>{$saData['CNOMBRE']}</b></p>
               <div class="row">
                  <div class="col-sm-6">
                     <select class="form-control" name="pcNivel*">
                        <option value="*">TODOS LOS USUARIOS</option>
                        {foreach from = $saCargos item = i}
                           <option value="{$i['CNIVEL']}">{$i['CNIVEL']} - {$i['CDESCRI']}</option>
                        {/foreach}
                     </select><br>
                  </div>
                  <div class="col-sm-6">
                     <button type="submit" name="Boton" value="Buscar" class="center-block btn btn-info btn-lg btn-block">Buscar <i class="glyphicon glyphicon-search"></i></button><br>
                  </div>
               </div>
               <div class="table-responsive">
                  <table class="table table-condensed">
                     <thead>
                        <th style='text-align: center' class="col-xs-1">Codigo</th>
                        <th style='text-align: center' class="col-xs-2">Nro DNI</th>
                        <th style='text-align: left' class="col-xs-3">Apellidos y Nombres</th>
                        <th style='text-align: left' class="col-xs-3">Cargo </th>
                        <th style='text-align: left' class="col-xs-4">Unidad A. </th>
                        <th style='text-align: left' class="col-xs-3">Estado </th>
                        <th style='text-align: center' class="col-xs-1 glyphicon glyphicon-ok"></th>
                     </thead>
                     {$j = 1}
                     {foreach from = $saDatos item = i}
                     {if $i['CESTADO'] == 'I'}
                     <tr class="danger">
                        {/if}
                        <td style='text-align: center' class="col-xs-1">{$i['CCODUSU']}</td>
                        <td style='text-align: center' class="col-xs-2">{$i['CNRODNI']}</td>
                        <td style='text-align: left' class="col-xs-3">{$i['CNOMBRE']}</td>
                        <td style='text-align: left' class="col-xs-3">{$i['CDESCRI']}</td>
                        <td style='text-align: left' class="col-xs-4">{$i['CNOMUNI']}</td>
                        {if $i['CESTADO'] == 'A'}
                        <td style='text-align: left' class="col-xs-2">ACTIVADO</span>
                           {elseif $i['CESTADO'] == 'I'}
                        <td style='text-align: left' class="col-xs-2">INACTIVO</span>
                           {/if}
                     
                        <td style='text-align: center' class="col-xs-1"><input type="radio" name="pcCodUsu" value="{$i['CCODUSU']}"></td>         
                     </tr>
                     {$j = $j + 1}
                     {/foreach}
                  </table>
               </div>
            </div>
         </div>
         <div class="col-sm-3">
            <button type="submit" name="Boton" value="Nuevo" class="center-block btn btn-success btn-lg btn-block">Nuevo <i class="glyphicon glyphicon-plus"></i>
         </div>
         <div class="col-sm-3">
            <button type="submit" name="Boton" value="Asignar" class="center-block btn btn-warning btn-lg btn-block">Asignar Nivel <i class="glyphicon glyphicon-plus"></i>
         </div>
         <!--<div class="col-sm-2">
            <button type="submit" name="Boton" value="Editar" class="center-block btn btn-info btn-lg btn-block">Editar <i class="glyphicon glyphicon-edit"></i>
         </div>-->
         <div class="col-sm-3">
            <button type="submit" name="Boton" value="Detalle" class="center-block btn btn-primary btn-lg btn-block">Detalle <i class="glyphicon glyphicon-th-list"></i>
         </div>
         <div class="col-sm-3">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
         </div>            
         </form>            
         {elseif $snBehavior == 1}
         <form action="Paq1170.php" method="post">
         <div class="panel panel-success">
            <div class="panel-heading">
               <h4>Mantenimiento de Usuarios</h4>
            </div>
            <div class="panel-body">
               <h3><kbd>Datos del Usuario </kbd></h3>
               <table class="table table-hover">
                  <tr>
                     <th>Usuario</th>
                     <td>{$saData['CCODUSU']}</td>
                  </tr>
                  <tr>
                     <th>Cargo</th>
                     <td>{$saData['CCARGO']}</td>
                  </tr>
                  <tr>
                     <th>DNI</th>
                     <td>{$saData['CNRODNI']}</td>
                     <input type='hidden' name='pcNroDni' value="{$saData['CNRODNI']}">
                  </tr>
                  <tr>
                     <th>Nombre</th>
                     <td>{$saData['CNOMBRE']}</td>
                  </tr>
               </table>
               <div class="table-responsive">
                  <h3><kbd>Centros de Costo del Usuario </kbd></h3>
                  <table class="table table-hover">
                     <input type='hidden' name='pcCodUsu' value="{$saData['CCODUSU']}">
                     <th style='text-align: center'>Nro</th>
                     <th style='text-align: center'>Centro de Costo</th>
                     <th style='text-align: center'>Estado</th>
                     <th></th>
                     {$j = 0}
                     {foreach from = $saDatos['paUsuCen'] item = i}
                        {if $i['CESTADO'] =='I'}
                           <tr class="danger">
                        {else}
                           <tr>
                        {/if}
                           <td style='text-align: center'>{$j+1}</td>
                           <td style='text-align: center'>{$i['CDESCRI']}</td>
                           <td>
                              <select class="form-control" name="pcEstadoCenCos[]">
                                 {foreach from = $saEstado item = k}
                                 <option value="{$k['CCODIGO']}" {if $k['CCODIGO'] eq $i['CESTADO']} selected {/if}>{$k['CDESCRI']}</option> 
                                 {/foreach} 
                              </select> 
                           </td></tr>
                           {$j=$j + 1} 
                        {/foreach} 
                  </table> 
               </div> 
               <div class="row">
                  <div class="col-sm-4">
                     <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#myModal" role="button">Nuevo Centro de Costo <i class="glyphicon glyphicon-plus-sign"></i></a>
                  </div>
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4">
                     <button type="submit" name="Boton1" value="Actualizar" class="center-block btn btn-primary btn-lg btn-block"> Actualizar <i class="glyphicon glyphicon-refresh"></i></button>
                  </div>
               </div>
               <div id="myModal" class="modal fade" role="dialog"> <!--Inicia Modal-->
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Asignación de Centros de Costo </h4>
                        </div>
                        <div class="modal-body">
                           <table class="table table-condensed">
                              <tr>
                                 <th><b>CODIGO</b></th>
                                 <td>{$saData['CCODUSU']}</td>
                                 <input type='hidden' name='pcCodUsu' value="{$saData['CCODUSU']}" id='paData[CCODUSU]'> 
                              </tr>
                              <tr>
                                 <td><b>USUARIO </b></td>
                                 <td>{$saData['CNOMBRE']}</td>
                              </tr>
                              <tr>
                                 <td><b>CENTRO DE COSTO </b></td>
                                 <td>
                                    <select class="selectpicker form-control" data-live-search="true" name="pcCenCos" id='paData[CCENCOS]'>
                                       <option selected="true" disabled value="">Seleccione Centro de Costo</option>
                                       {foreach from = $saCencos['pcCenCos'] item = i}
                                          <option value="{$i['CCENCOS']}">{$i['CCENCOS']} - {$i['CDESCRI']}</option>
                                       {/foreach}
                                    </select>
                                 </td>
                              </tr>
                           </table>
                        </div>
                        <div class="modal-footer">
                           <div class="row">
                              <div class="col-sm-6">
                                 <input type="submit" name="Boton1" onclick="return confirm('¿Está seguro que desea agregar este Centro de Costo?')" value="Grabar" class="center-block btn btn-success btn-lg btn-block" />
                              </div>
                              <div class="col-sm-6">
                                 <button type="button" class="center-block btn btn-danger btn-lg btn-block" data-dismiss="modal">Cancelar</button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>                                             <!--Termina Modal-->
               <hr>  
               <!---AQUI -->
               <div class="table-responsive">
                  <h3><kbd>Permisos de Niveles del Usuario </kbd></h3>
                  <table class="table table-hover">
                     <input type='hidden' name='pcCodUsu' value="{$saData['CCODUSU']}">
                     <th style='text-align: center'>Nro</th>
                     <th style='text-align: center'>Unidad Academica</th>
                     <th style='text-align: center'>Cargo</th>
                     <th style='text-align: center'>Estado</th>
                     <th></th>
                     {$j = 0}
                     {foreach from = $saDatos['paUsuNiv'] item = i}
                        {if $i['CESTADO'] =='I'}
                           <tr class="danger">
                        {else}
                           <tr>
                        {/if}
                     <td style='text-align: center'>{$j+1}</td>
                     <td style='text-align: center'>{$i['CNOMUNI']}</td>
                     <td style='text-align: center'>{$i['CDESCRI']}</td>
                     <td>
                     <select class="form-control" name="pcEstado[]">
                        {foreach from = $saEstado item = k}
                           <option value="{$k['CCODIGO']}" {if $k['CCODIGO'] eq $i['CESTADO']} selected {/if}>{$k['CDESCRI']}
                           </option> 
                        {/foreach} 
                     </select> 
                     </td> 
                     <input type="hidden" name="pnNserial" value="{$i['NSERIAL']}">
                     </tr> 
                     {$j=$j + 1} 
                        {/foreach} 
                  </table> 
               </div> 
               <div class="row">
                  <div class="col-sm-4">
                     <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#myModal1" role="button">Nuevo Nivel <i class="glyphicon glyphicon-plus-sign"></i></a>
                  </div>
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4">
                     <button type="submit" name="Boton2" value="Actualizar" class="center-block btn btn-primary btn-lg btn-block"> Actualizar <i class="glyphicon glyphicon-refresh"></i></button>
                  </div>
               </div>
               <div id="myModal1" class="modal fade" role="dialog"> <!--Inicia Modal-->
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal1">&times;</button>
                           <h4 class="modal-title">Asignación de Permisos </h4>
                        </div>
                        <div class="modal-body">
                           <table class="table table-condensed">
                              <tr>
                                 <td><b>USUARIO </b></td>
                                 <td>{$saData['CNOMBRE']}</td>
                              </tr>
                              <tr>
                                 <th><b>CODIGO</b></th>
                                 <td>{$saData['CCODUSU']}</td>
                                 <input type='hidden' name='pcCodUsu' value="{$saData['CCODUSU']}" id='paData[CCODUSU]'> 
                              </tr>
                              <tr>
                                 <td><b>CARGO </b></td>
                                 <td>
                                    <select class="form-control" name="pcNivel" id="paData[CNIVEL]">
                                       <option selected="true" disabled value="">Seleccione Nivel</option>
                                       {foreach from = $saCargos item = i}
                                          <option value="{$i['CNIVEL']}">{$i['CNIVEL']} - {$i['CDESCRI']}</option>
                                       {/foreach}
                                    </select>
                                 </td>
                              </tr>
                              <tr>
                                 <td><b>Unidad Academica </b></td>
                                 <td>
                                    <select class="selectpicker form-control" data-live-search="true" name="pcUniAca" id="paData[CUNIACA]">
                                       <option selected="true" disabled value="">Seleccione una Unidad Academica</option>
                                       {foreach from = $saUniAca item = i}
                                       <option value="{$i['CUNIACA']}">{$i['CUNIACA']} - {$i['CNOMUNI']}</option>
                                       {/foreach}
                                    </select>
                                 </td>
                              </tr>
                           </table>
                        </div>
                        <div class="modal-footer">
                           <div class="row">
                              <div class="col-sm-6">
                                 <input type="submit" name="Boton2" value="Grabar" onclick="return confirm('¿Está seguro que desea agregar este Nivel?')" class="center-block btn btn-success btn-lg btn-block" />
                              </div>
                              <div class="col-sm-6">
                                 <button type="button" class="center-block btn btn-danger btn-lg btn-block"data-dismiss="modal">Cancelar</button>
                              </div>
                           </div>
                        </div>
                     </div>    
                  </div>
               </div><!--Termina Modal-->
            </div>
         </div>         
         <div class="row">
            <div class="col-sm-4">
               <button type="submit" name="Boton"  onclick="return confirm('¿Está seguro que desea Reestablecer Contraseña?')" value="Reestablecer"  class="center-block btn btn-info btn-lg btn-block"> Reestablecer Contraseña <i class="glyphicon glyphicon-repeat"></i></button>
            </div>
            <div class="col-sm-4"></div>
            <!--<div class="col-sm-4">
               <button type="submit" name="Boton1" value="Actualizar" class="center-block btn btn-primary btn-lg btn-block"> Actualizar <i class="glyphicon glyphicon-refresh"></i></button>
            </div>-->
            <div class="col-sm-4">
               <a href="Paq1170.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
            </div>
         </div>
         </form>
         {elseif $snBehavior == 2}
         <form action="Paq1170.php" method="post">
         <div class="panel panel-success">
            <div class="panel-heading">
               <h4>Mantenimiento de Usuarios</h4>
            </div>
            <div class="panel-body">
               <h4 class="modal-title"><b>Editar Estado de Usuario </b></h4>
               <div class="modal-body">
                  <table class="table table-hover">
                     <tr>
                        <td><b>CÓDIGO USUARIO </b></td>
                        <td><input class="form-control" type='text' name="pcCodUsu" value="{$saDatos['CCODUSU']}" disabled>
                           <input type='hidden' name="pcCodUsu" value="{$saDatos['CCODUSU']}">
                           <input type='hidden' name="pnNserial" value="{$saData['NSERIAL']}">
                        </td>
                     </tr>
                     <tr>
                        <td><b>PERSONA </b></td>
                        <td><input class="form-control" type='text' name="pcDescri" value="{$saDatos['CNOMBRE']}" style="text-transform:uppercase"
                              disabled></td>
                     </tr>
                     <tr>
                        <td><b>ESTADO </b></td>
                        <td>
                           <select class="form-control" name="pcEstado">
                              {foreach from = $saEstado item = i}
                              <option value="{$i['CCODIGO']}" {if $i['CCODIGO'] eq $saDatos['CESTADO']} selected {/if}>{$i['CDESCRI']}</option> 
                              {/foreach} 
                           </select> 
                        </td> 
                     </tr> 
                  </table> 
               </div> 
            </div> 
         </div> 
         <div class="row col-md-4 col-md-push-8">
            <button type="submit" name="Boton" value="Actualizar" class="center-block btn btn-success btn-lg btn-block">Actualizar <i class="glyphicon glyphicon-refresh"></i></button>
            <a href="Paq1170.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
         </div>
         </form>
         {elseif $snBehavior == 3}
         <form action="Paq1170.php" method="post">
         <div class="panel panel-success">
            <div class="panel-heading">
               <h4>Mantenimiento de Usuarios</h4>
            </div>
            <div class="panel-body">
               <h4 class="modal-title"><b>Crear Nueva Persona</b></h4>
               <div class="modal-body">
                  <table class="table table-hover">
                     <tr>
                        <td><b>DNI</b></td>
                        <td><input type="text" id="text" class="form-control" name="paData[CNRODNI]" maxlength="8" placeholder="INGRESE DNI"
                              required onkeydown="f_ValidaPersona(event);" onkeypress="return f_ValidaNumericos(event);">
                           <label id="cNombre"></label></td>
                     </tr>
                     <tr>
                        <td><b>APELLIDO PATERNO</b></td>
                        <td><input class="form-control" name="paData[CAPEPAT]" placeholder="APELLIDO PATERNO" required='required'></td>
                     </tr>
                     <tr>
                        <td><b>APELLIDO MATERNO</b></td>
                        <td><input class="form-control" name="paData[CAPEMAT]" placeholder="APELLIDO MATERNO" required='required'></td>
                     </tr>
                     <tr>
                        <td><b>NOMBRES</b></td>
                        <td><input class="form-control" name="paData[CNOMUSU]" placeholder="NOMBRES COMPLETO" required='required'></td>
                     </tr>
                     <tr>
                        <td><b>SEXO</b></td>
                        <td>
                           <select class="form-control" name="paData[CSEXO]">
                              {foreach from = $saSexo item = i}
                              <option value="{$i['CCODIGO']}">{$i['CDESCRI']}</option>
                              {/foreach}
                           </select>
                        </td>
                     </tr>
                     <tr>
                        <td><b>FECHA DE NACIMIENTO </b></td>
                        <div class="form-group md-form">
                           <th><input type="date" class="form-control" name="paData[DNACIMI]" required /></th>
                        </div>
                     </tr>
                     <tr>
                        <td><b>CODIGO DE UBICACIÓN DE NACIMIENTO</b></td>
                        <td><input class="form-control" name="paData[CUBINAC]" placeholder="UBIGEO" required='required' maxlength="6">
                        </td>
                     </tr>
                     <tr>
                        <td><b>E-MAIL</b></td>
                        <td><input class="form-control" type="email" name="paData[CEMAIL]" placeholder="E-MAIL" required='required'>
                        </td>
                     </tr>
                     <tr>
                        <td><b>CELULAR</b></td>
                        <td><input class="form-control" name="paData[CNROCEL]" placeholder="CELULAR" required='required'>
                        </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
         <div class="col-sm-4">
            <button type="submit" name="Boton" onclick="return confirm('¿Está seguro que desea agregar a esta Nueva Persona?')" value="GrabarPersona" class="center-block btn btn-success btn-lg btn-block">
               Grabar <i class="glyphicon glyphicon-edit"></i>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Paq1170.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
         </div>
         </form>
         {elseif $snBehavior == 4}
         <form action="Paq1170.php" method="post">
         <div class="panel panel-success">
            <div class="panel-heading">
               <h4>Mantenimiento de Usuarios</h4>
            </div>
            <div class="panel-body">
               <h4 class="modal-title"><b>Asignar Cargo a Persona</b></h4>
               <div class="modal-body">
                  <table class="table table-hover">
                     <tr>
                        <td><b>DNI </b></td>
                        <td><input type="text" id="text" class="form-control" name="paData[CNRODNI]" maxlength="8" placeholder="INGRESE DNI"
                              required onkeydown="f_ValidaUsuario(event);" onkeypress="return f_ValidaNumericos(event);">
                     </tr>
                     <tr>
                        <td><b>NOMBRE </b></td>
                        <td><label id="cNombre"></label></td>
                     </tr>
                     <tr>
                        <td><b>CODIGO </b></td>

                        <td><input class="form-control" name="paData[CCODNUE]" id="pcCodUsu"  maxlength="4"></td>  
                     </tr>
                     <tr>
                        <td><b>NIVEL </b></td>
                        <td>
                           <select class="form-control" name='paData[CNIVEL]' id="pcNivel">
                              <option selected="true" disabled value="">Seleccione Nivel de Usuario</option>
                              {foreach from = $saCargos item = i}
                              <option value="{$i['CNIVEL']}">{$i['CNIVEL']} - {$i['CDESCRI']}</option>
                              {/foreach}
                           </select>
                        </td>
                     </tr>
                     <td><b>CENTRO DE COSTO </b></td>
                     <td>
                        <select class="selectpicker form-control" data-live-search="true" name='paData[CCENCOS]'>
                           <option selected="true" disabled value="">Seleccione Centro de Costo</option>
                           {foreach from = $saCencos['pcCenCos'] item = i}
                           <option value="{$i['CCENCOS']}">{$i['CCENCOS']} - {$i['CDESCRI']}</option>
                           {/foreach}
                        </select>
                     </td>
                     </tr>
                  </table>
               </div>
            </div>
         </div>
         <div class="col-sm-4">
            <button type="submit" name="Boton" onclick="return confirm('¿Está seguro que desea agregar este Nivel?')" value="GrabarNivelUsuario" class="center-block btn btn-success btn-lg btn-block">Grabar<i class="glyphicon glyphicon-edit"></i>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Paq1170.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
         </div>
         </form>
         {/if}
      </div>
   </div>
</div>
{if $snBehavior == 1}
<form action="Paq1170.php" method="POST">
<!--<div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Asignación de Centros de Costo </h4>
            </div>
            <div class="modal-body">
               <table class="table table-condensed">
                  <tr>
                     <th><b>CODIGO</b></th>
                     <td>{$saData['CCODUSU']}</td>
                     <input type='hidden' name='pcCodUsu' value="{$saData['CCODUSU']}" id='paData[CCODUSU]'> 
                  </tr>
                  <tr>
                     <td><b>USUARIO </b></td>
                     <td>{$saData['CNOMBRE']}</td>
                  </tr>
                  <tr>
                     <td><b>CENTRO DE COSTO </b></td>
                     <td>
                        <select class="selectpicker form-control" data-live-search="true" name="pcCenCos" id='paData[CCENCOS]'>
                           <option selected="true" disabled value="">Seleccione Centro de Costo</option>
                           {foreach from = $saCencos['pcCenCos'] item = i}
                              <option value="{$i['CCENCOS']}">{$i['CCENCOS']} - {$i['CDESCRI']}</option>
                           {/foreach}
                        </select>
                     </td>
                  </tr>
               </table>
            </div>
            <div class="modal-footer">
               <div class="row">
                  <div class="col-sm-6">
                     <input type="submit" name="Boton1" value="Grabar" class="center-block btn btn-success btn-lg btn-block" />
                  </div>
                  <div class="col-sm-6">
                     <button type="button" class="center-block btn btn-danger btn-lg btn-block" data-dismiss="modal">Cancelar</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>-->
<!--<form action="Paq1170.php" method="POST">
<div id="myModal1" class="modal fade" role="dialog"> <Inicia Modal-->
   <!--<div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal1">&times;</button>
            <h4 class="modal-title">Asignación de Permisos </h4>
         </div>
         <div class="modal-body">
            <table class="table table-condensed">
               <tr>
                  <td><b>USUARIO </b></td>
                  <td>{$saData['CNOMBRE']}</td>
               </tr>
               <tr>
                  <th><b>CODIGO</b></th>
                  <td>{$saData['CCODUSU']}</td>
                  <input type='hidden' name='pcCodUsu' value="{$saData['CCODUSU']}" id='paData[CCODUSU]'> 
               </tr>
               <tr>
                  <td><b>CARGO </b></td>
                  <td>
                     <select class="form-control" name="pcNivel" id="paData[CNIVEL]">
                        {foreach from = $saCargos item = i}
                           <option value="{$i['CNIVEL']}">{$i['CNIVEL']} - {$i['CDESCRI']}</option>
                        {/foreach}
                     </select>
                  </td>
               </tr>
               <tr>
                  <td><b>Unidad Academica </b></td>
                  <td>
                     <select class="selectpicker form-control" data-live-search="true" name="pcUniAca" id="paData[CUNIACA]">
                        <option selected="true" disabled value="">Seleccione una Unidad Academica</option>
                        {foreach from = $saUniAca item = i}
                        <option value="{$i['CUNIACA']}">{$i['CUNIACA']} - {$i['CNOMUNI']}</option>
                        {/foreach}
                     </select>
                  </td>
               </tr>
            </table>
            <div class="modal-footer">
               <div class="row">
                  <div class="col-sm-6">
                     <input type="submit" name="Boton2" value="Grabar" class="center-block btn btn-success btn-lg btn-block" />
                  </div>
                  <div class="col-sm-6">
                     <button type="button" class="center-block btn btn-danger btn-lg btn-block"
                        data-dismiss="modal">Cancelar</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>Termina Modal-->
<!--</form>-->
{/if}
</html>