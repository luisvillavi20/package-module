<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
</head>
<body onload="f_Init()" class="divBody">
   <div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
   <div class="container-fluid">
   <form action="Paq1250.php" method="post">        
   <div class="row col-sm-12">
      <div class="panel panel-success">
         <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA GRUPOS DE COLACIÓN</b>
            <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
         </div>
         <div class="panel-body">
            <table class="table">
               <thead>
                  <tr>
                  <th>#</th>
                  <th>Descripción</th>
                  <th>Unidad Académica</th>
                  <th style="text-align: center">Activar</th>
                  </tr>
               </thead>
               <tbody>
                  {$j = 1}
                  {foreach from=$saDatos item=i}
                  <tr>
                  <td>{$j}</td>
                  <td>{$i['CDESCRI']}</td>
                  <td>{$i['CUNIACA']} - {$i['CNOMUNI']}</td>
                  <td style="text-align: center"><input type="radio" name="paData[CCOLUAC]" value="{$i['CCOLUAC']}" required></td>
                  </tr>
                  {$j = $j + 1}
                  {/foreach}
               </tbody>
            </table>            
         </div>
      </div>
      <div>
         <div class="col-md-4">
            <button name="Boton" value="Imprimir" class="btn btn-primary btn-block btn-lg">Imprimir</button>
         </div>
         <div class="col-md-4"></div>
         <div class="col-md-4">
            <a class="btn btn-danger btn-block btn-lg" href="Mnu1000.php">Salir</a>
         </div>
      </div>
   </div>
   </form>
   </div>
</body>
</html>