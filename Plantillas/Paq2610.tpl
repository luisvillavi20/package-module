<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
    
    function f_ObtenerActasDeColacion() {
         lcUniAca = $('#cUniAca').val();
         lcIdCola = $('#cIdCola').val();
         $('#tb_grupCola').html('');
         var lcSend = "Id=ObtenerActas&paData[CUNIACA]="+lcUniAca.trim()+"&paData[CIDCOLA]="+lcIdCola.trim();
         $.post("Paq2610.php",lcSend).done(function(p_cResult) {
            var loJson = (isJson(p_cResult.trim()))? JSON.parse(p_cResult.trim()) : p_cResult;
            if (loJson.ERROR){
               alert(loJson.ERROR);
               return;
            }
            $('#tb_grupCola').html(p_cResult);
            $('#div_grupCola').css("display", "block");
         });
    }

    function f_VerActa(e){
      lcColUac = e.value;
      lcActa = './EXP/ACTAS/'+lcColUac+'.pdf';
      window.open(lcActa);
    }

   </script>
</head>
<body class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody">
<form id="poForm" action="Paq2610.php" method="post">
   <input type="hidden" id="Id" name="Id">
   <div class="row">
   <div class="col-sm-12">
   <div class="panel panel-success">
      {if $snBehavior == 0}
         <div class="panel-heading"><h3 class="panel-title"><b>GRUPOS DE COLACIÓN</b></h3></div>
         <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-hover">
              <tr>
               <td><b>COLACIONES</b></td>
               <td colspan="3">
                  <select id="cIdCola" class="form-control" required name="paData[CUNIACA]">
                     <option selected="true" disabled value="">Seleccione Colacion...</option>                     
                     {foreach from = $saHisCol item = i}
                        <option name="paData[CIDCOLA]" value="{$i['CIDCOLA']}">{$i['CDESCRI']}</option>
                     {/foreach}
                  </select>          
               </td>
               </tr>
               <tr>
               <td><b>UNIDAD ACADÉMICA</b></td>
               <td colspan="3">
                  <select id="cUniAca" class="form-control" required name="paData[CUNIACA]">
                     <option selected="true" disabled value="">Seleccione escuela profesional...</option>                     
                     {foreach from = $saEscuel item = i}
                        <option name="paData[CUNIACA]" value="{$i['CUNIACA']}">{$i['CUNIACA']} - {$i['CNOMUNI']}</option>
                     {/foreach}
                  </select>          
               </td>
               </tr>
            </table>            
            </div>
            <div>
               <div class="col-sm-2">
               </div>
               <div class="col-sm-5">
                  <button type="button" onclick="f_ObtenerActasDeColacion();" class="btn btn-info btn-block btn-lg">Cargar Grupos</button>
               </div>
               <div class="col-sm-5">
                  <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir </a>
               </div>
            </div>
            <br><br>
            <div class="row" id="div_grupCola" style="display: none;">
            <div class="col-sm-12">
            <div class="panel panel-success">
            <div class="panel-heading"><h3 class="panel-title"><b>ACTAS DE COLACIÓN</b></h3></div>
            <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-hover">
                <th style='text-align: center'>ID grupo</th>
                <th style='text-align: center'>Fecha Creación</th>
                <th style='text-align: center'>Ver</th>
                <tbody id="tb_grupCola">
                </tbody>
            </table>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
         </div>
      {/if}
   </div>
   </div>
   </div>
</form>
</div>
</body>
</html>