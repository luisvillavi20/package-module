<div class="row col-md-10 col-md-push-1">
<div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE CONSTANCIAS DE EXPEDIENTES COMPLETOS</b></h3></div>
<div class="table table-condensed table-responsive">     
    <table class="table table-condensed table-striped">
      <thead>
        <th style="text-align: center" class="col-xs-3"><label>CÓDIGO</label></th>
        <th style="text-align: center" class="col-xs-4"><label>ALUMNO</label></th>
        <th style="text-align: center" class="col-xs-2"><label>DNI</label></th>
        <th style="text-align: center" class="col-xs-3"><label>DESCARGAR ARCHIVO</label></th>
      </thead>
      <tbody>
        {$j=0}
        {foreach from = $saDatos item = i}
          <form action="Paq2070.php" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="paData[CCODALU]" value="{$i['CCODALU']}">
            <input type="hidden" name="paData[CNRODNI]" value="{$i['CNRODNI']}">
            <tr>
              <td style="text-align: center">{$i['CCODALU']}</td>
              <td>{$i['CNOMBRE']}</td>
              <td style="text-align: center">{$i['CNRODNI']}</td>
              {if $saData['CCODIGO'] == 'B'}
              <td>
                <a class="btn btn-info btn-block btn-lg" href="DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE=000000&download=true">Descargar <i class="glyphicon glyphicon-download-alt"></i></a></td>  
              {else}
                <td>
                <a class="btn btn-info btn-block btn-lg" href="DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE=P00005&download=true">Descargar <i class="glyphicon glyphicon-download-alt"></i></a></td>  
              {/if}
            </tr>
          </form>
          {$j = $j + 1}
        {/foreach} 
      </tbody>        
  </table>
</div>
</div>
</div>