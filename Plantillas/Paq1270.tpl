<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
     function f_CargarGrupos()
     {
      let lcIdCola = $('[name="paData[CIDCOLA]"]').val();
      let lcTipCol = $('[name="paData[CTIPCOL]"]').val();
      $.get('Paq1270.php', {
        Id: 'CargarGrupos',
        paData:{
          CIDCOLA: lcIdCola,
          CTIPCOL: lcTipCol
        }
      }, function (data) {
        $('#select_grupos').html(data);
      });
     }
     function f_valCrearGrupo() {
        let lcIdCola = $('[name="paData[CIDCOLA]"]').val();
        if (lcIdCola == null) {
           alert("SELECCION COLACIÓN");
           return false;
        }
        return true;
     }
     function f_cargarUltimaColacion(self) {
         $.get('Paq1270.php', {
            Id: 'CargarColacion',
            paData: {
               CTIPCOL: self.value
            }
         }, function (data) {
            var data = JSON.parse(data);
            if (data['ERROR']) {
               alert(data['ERROR']);
            } else {
               $("#tr_colacion").css('display', 'table-row');
               $('[name="paData[CIDCOLA]"]').val(data['CIDCOLA']);
               $("#td_colacion").html(data['CDESCRI']);
            }
         });
      }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody">
<form action="Paq1270.php" method="post">
   <div class="row">
   <div class="col-sm-10 col-sm-push-1">
   <div class="panel panel-success">
      {if $snBehavior == 0}
         <div class="panel-heading"><h3 class="panel-title"><b>GRUPOS DE COLACIÓN</b></h3></div>
         <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-hover"><tr>
               <td><b>TIPO DE COLACION</b></td>
               <td colspan="3">
                  <select class="form-control" name="paData[CTIPCOL]" onchange="f_cargarUltimaColacion(this)">
                     <option selected="true" disabled value="">Seleccione tipo de colación...</option>
                     {foreach from=$saTipCol item=i}
                        <option name="paData[CTIPCOL]" value="{$i['CCODIGO']}">{$i['CDESCRI']}</option>
                     {/foreach}
                  </select>          
               </td> 
               </tr><tr id="tr_colacion" style="display: none">
                  <th>COLACION<input type="hidden" name="paData[CIDCOLA]"></th>
                  <td id="td_colacion" colspan="3"></td>
               </tr>
               <tr><td><b>UNIDAD ACADÉMICA</b></td>
               <td colspan="3">
                  <select class="form-control" required name="paData[CUNIACA]">
                     <option selected="true" disabled value="">Seleccione escuela profesional...</option>                     
                     {foreach from = $saEscuel item = i}
                        <option name="paData[CUNIACA]" value="{$i['CUNIACA']}">{$i['CNOMUNI']}</option>
                     {/foreach}
                  </select>          
               </td>
               </tr>
            </table>            
            </div>
            <div>
               <div class="col-sm-4">
                  <button type="submit" name="Boton" value="VerificarEstadoRevisores" class="center-block btn btn-success btn-lg btn-block" onclick="mostrarDocumento()"> Ver Revisores <i class="glyphicon glyphicon-plus"></i>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir </a>
               </div>
            </div>
         </div>
      {elseif $snBehavior == 2}
         <div class="panel-heading"><h3 class="panel-title"><b>GRUPOS DE COLACIÓN - ASIGNAR DOCENTES</b><span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3></div>
         <div class="panel-body">
            <div class="table-responsive mh-50">
            <table class="table table-condensed">
               <tr class="warning2">
                  <th colspan="5">Docentes revisores:</th>
               </tr>
                  <th>Cargo</th>
                  <th>Nombre</th>
                  <th style='text-align: center'>Estado</th>
               </tr>
               <tbody>
                  <tr>
                  <th>Presidente:</th>
                  <td>{$saDatos['paRevisor'][0]['CNOMBRE']}</td>
                  <td style='text-align: center'>{if $saDatos['paRevisor'][0]['CESTADO'] == 'I'}
                     <a class="label label-success">REVISO</a>
                     {else}
                     <a class="label label-danger">NO REVISO</a>
                     {/if}
                  </td>
                  </tr><tr>
                  <th>Vocal:</th>
                  <td>{$saDatos['paRevisor'][1]['CNOMBRE']}</td>
                  <td style='text-align: center'>{if $saDatos['paRevisor'][1]['CESTADO'] == 'I'}
                     <a class="label label-success">REVISO</a>
                     {else}
                     <a class="label label-danger">NO REVISO</a>
                     {/if}
                  </td>
                  </tr><tr>
                  <th>Secretario:</th>
                  <td>{$saDatos['paRevisor'][2]['CNOMBRE']}</td>
                  <td style='text-align: center'>{if $saDatos['paRevisor'][2]['CESTADO'] == 'I'}
                     <a class="label label-success">REVISO</a>
                     {else}
                     <a class="label label-danger">NO REVISO</a>
                     {/if}
                  </td>
                  </tr>
               </tbody>               
            </table>
            </div>
            <div class="table-responsive mh-50">
            <table class="table table-condensed">
               <tr class="warning2">
                  <th colspan="5">Alumnos asignados para revisión:</th>
               </tr>
                  <th>Código del alumno</th>
                  <th>DNI</th>
                  <th>Nombre</th>
                  <th style='text-align: center'>Fecha</th>
               </tr>
               <tbody>
                  {foreach from = $saDatos['paAlumno'] item = i}
                  <td>{$i['CCODALU']}</td>
                  <td>{$i['CNRODNI']}</td>
                  <td>{$i['CNOMBRE']}</td>
                  <td style='text-align: center'>{$i['TMODIFI']}</td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
            </div>
            <div class="row">
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <a href="Paq1270.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
               </div>
               <div class="col-sm-4"></div>
            </div>
         </div>
      {/if}
   </div>
   </div>
   </div>
</form>
</div>
</body>
</html>