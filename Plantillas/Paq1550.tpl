<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
   <script>
      function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
      }

      $(document).ready(function() {
         $('#table').dataTable();
      });
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1550.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive">
            <table id="table" class="table table-condensed display">
               <thead>
                  <tr>
                     <th class="col-xs-1" style='text-align: left'>Fecha de Solicitud</th>
                     <th class="col-xs-1" style='text-align: left'>DNI</th>
                     <th class="col-xs-1" style='text-align: left'>Cod.Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre</th>
                     <th class="col-xs-4" style='text-align: center'>Unidad Academica</th>
                     <th class="col-xs-1" style='text-align: center'>Estado</th>
                     <th class="col-xs-1" style='text-align: center'>Oficio</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                  <tr class="Datos" onclick="f_seleccionarFila(this)">
                     <td style='text-align: left'>{$i['TFECHA']}</td>
                     <td style='text-align: left'>{$i['CNRODNI']}</td>
                     <td style='text-align: left'>{$i['CCODALU']}</td>
                     <td style='text-align: left'>{$i['CNOMBRE']}</td>
                     <td style='text-align: left'>{$i['CNOMUNI']}</td>
                     <td style='text-align: left'>
                     {if $i['CESTADO'] == 'F' }
                     <span class="label label-default">BIBLIOTECA</span> 
                     {elseif $i['CESTADO'] == 'A' }
                        <span class="label label-primary">ESCUELA</span>
                     {elseif $i['CESTADO'] == 'R' }  
                        <span class="label label-warning">EN RECEPCION</span>
                     {elseif $i['CESTADO'] == 'B' }  
                        <span class="label label-success">RECEPCIONADO</span>
                     {/if}
                     </td>
                     {if $i['CESTADO'] == 'R' OR $i['CESTADO'] == 'B'}
                     <td style='text-align: center'>
                        <button type="button" class="btn btn-info btn-block btn-xs" onClick="window.open('DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE={$i['CCODTRE']}', 'Algo', 'height=850, width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>                     
                     </td>
                     {else}
                     <td style='text-align: center'>
                        <label><i class="glyphicon glyphicon-ban-circle"></i></label>
                     </td>
                    {/if}
                  </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-3"></div>
         <div class="col-sm-6">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
         <div class="col-sm-3"></div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer">
</div>
</body>
</html>