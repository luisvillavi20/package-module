<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      
      function myCodigo(){
         if( $("input[type='radio']").is(':checked')){
            var observ;
            $('.codalu:checked').each(function(indice, elemento){
               var fila = $(this).parents(".Datos");
               cCodAlu = fila.find(".pcCodAlu").val();
               $('input[name="paData[CCODALU]"]').val(cCodAlu);
               return true;
            });
         }
         else{
            window.alert('Seleccione un alumno');
            return false;
         }
      }

    
      function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
      }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1530.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed table-hover">
               <thead>
                  <tr>
                     <th class="col-xs-2" style='text-align: left'>DNI</th>
                     <th class="col-xs-2" style='text-align: left'>Cod.Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre</th>
                     <th class="col-xs-1" style='text-align: center'>Activar</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                  <tr class="Datos" onclick="f_seleccionarFila(this)">
                    <td style='text-align: left'>{$i['CNRODNI']}</td>
                    <td style='text-align: left'>{$i['CCODALU']}</td>
                    <td style='text-align: left'>{$i['CNOMBRE']}</td>
                    <td style='text-align: center'>
                    <input type="radio" class="codalu" name="paData[CIDDEUD]" value="{$i['CIDDEUD']}" required/>
                    <input type ="hidden" class="pcCodAlu" value ="{$i['CCODALU']}">
                    <input type="hidden" name="paData[CCODALU]" id="cCodAlu">
                    </td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="DetalleAlumno" class="center-block btn btn-success btn-lg btn-block"> Activar <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4">
            <button type = "submit" class="center-block btn btn-primary btn-lg btn-block" onclick="myCodigo()" name="Boton" value ="Reporte">Reporte&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 1}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed table-hover">
               <thead>
                  <tr onclick="f_seleccionarFila(this)">
                     <th class="col-xs-2" style='text-align: center'>Fecha Recepción</th>
                     <th class="col-xs-3" style='text-align: left'>Tip.Documento</th>
                     <th class="col-xs-2" style='text-align: left'>DNI</th>
                     <th class="col-xs-2" style='text-align: left'>Cod.Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre</th>
                     <th class="col-xs-1" style='text-align: center'>Ver</th>
                     <th class="col-xs-1" style='text-align: center'>Estado</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                  <tr onclick="f_seleccionarFila(this)">
                    <td style='text-align: center'>{$i['DRECEPC']}</td>
                    <td style='text-align: left'>{$i['CDESCRI']}</td>
                    <td style='text-align: left'>{$i['CNRODNI']}</td>
                    <td style='text-align: left'>{$i['CCODALU']}</td>
                    <td style='text-align: left'>{$i['CNOMBRE']}</td>
                    <td>
                     {if $i['CIDCATE'] == 'PQ0034' OR $i['CIDCATE'] == 'CCCONB'}
                     <button class="btn btn-default btn-block btn-xs" type="button" onClick="window.open('{$i['MDETALL']['CURLPDF']}', 'Algo', 'height=850, width=850');">
                      <img src="Images/eye.png" width="20" height="20"></button></td>
                     {else}
                      <button class="btn btn-default btn-block btn-xs" type="button"  onClick="window.open('DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE={$i['CCODTRE']}', 'Algo', 'height=850, width=850');">
                      <img src="Images/eye.png" width="20" height="20"></button></td>
                      {/if}
                    </td>
                    <td style='text-align: center'>
                        {if $i['CESTMTR']=='E'}   
                        <img src="Images/eye.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                        {else if $i['CESTMTR']=='B'}
                        <img src="Images/aprobado.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Cerrado">
                        {else}
                        <img src="Images/clock.png" width="40%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                        {/if}
                     </td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div id="Activar" class="col-sm-4">
         </div>
         <div class="col-sm-4">
         <button class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</button>
         </div>
         <div class="col-sm-4">
         </div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer">
</div>
</body>
</html>