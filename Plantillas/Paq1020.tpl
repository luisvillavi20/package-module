<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<body onload="f_Init();f_Sumar('dataTable');" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Estado de Deuda Provisional Paquetes</b></h3></div>
<div class="container-fluid divBody">
<form action="Paq1020.php" method="post">
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title"><b>Selección de Deuda Provisional - Paquetes</b></h3></div>
<div class="panel-body">
   <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>   
   {if $snBehavior == '0'}   
   <div class="table-responsive">
   <table class="table table-condensed"> 
      <thead>
         <tr> 
            <th class="col-xs-3">Fecha</th>  
            <th class="col-xs-3">Código de Pago</th>  
            <th class="col-xs-2">Monto Pagado</th>
            <th class="col-xs-3">Estado</th>  
            <th class="col-xs-1"></th>
         </tr>
      </thead>
      {$j = 0}
      {foreach from = $saDatos item = i}
         <input type='hidden' name='paDatos[{$j}][0]' value='{$i[0]}'>
         <input type='hidden' name='paDatos[{$j}][1]' value='{$i[1]}'>     
         <input type='hidden' name='paDatos[{$j}][2]' value='{$i[2]}'>
         <input type='hidden' name='paDatos[{$j}][3]' value='{$i[3]}'>
         <input type='hidden' name='paDatos[{$j}][4]' value='{$i[4]}'>
         <tr>
            <td class="col-xs-3">{$i[0]}</td>                 
            <td class="col-xs-3 text-danger"><b>{$i[2]}</b></td>
            <td class="col-xs-2">S/.{number_format($i[3], 2)}</td>
            {if $i[4]== "A"||$i[4]== "V"} <!---ESTADO GENERADO -->
            <td class="col-xs-3"><input type="submit" name="Boton" value="PENDIENTE" class="btn btn-warning btn-block" disabled></td>          
            {elseif $i[4]== "C"}
            <td class="col-xs-3"><input type="submit" name="Boton" value="PAGADO" class="btn btn-success btn-block" disabled></td>            
            {elseif $i[4]== "X"}
            <td class="col-xs-3"><input type="submit" name="Boton" value="ANULADO" class="btn btn-danger btn-block" disabled></td>            
            {/if}
            <td class="col-xs-1"><input type="radio" name="pcIdDeud"  value="{$i[1]}"/></td>
            <td class="col-xs-1"></td>
         </tr>
         {$j = $j + 1}
      {/foreach}      
   </table>
   </div>
   <div class="row col-md-4 col-md-push-8">
       <button type="submit" name="Boton" value="Detalle" class="center-block btn btn-info btn-lg btn-block"><i class="glyphicon glyphicon-list-alt"></i> Detalle </button>
       <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>                    
   </div>     
   {elseif $snBehavior == '1'}    
   <div class="table-responsive">     
   <table class="table table-condensed"> 
      <thead>
         <tr> 
            <th class="col-xs-2">Fecha</th>  
            <th class="col-xs-2">Nro. Recibo</th>  
            <th class="col-xs-5">Tipo Documento</th>
            <th style='text-align: right' class="col-xs-2">Monto</th>
            <th class="col-xs-2" align="right">Derecho de tramite</th>
         </tr>
      </thead>
      <tbody>
      {foreach from = $saDatos item = i}
         <tr>
            <td class="col-xs-2">{$i[0]}</td>
            <td class="col-xs-2">{$i[1]}</td>
            <td class="col-xs-5">{$i[2]}</td>
            <td style='text-align: right' class="col-xs-2">{number_format($i[3], 2)}</td>
            <td class="col-xs-3"></td>
         </tr> 
      {/foreach}
      </tbody>
   </table>
   <h1 align="right" class="panel-default mr-5">
        Total: S/.
        <label>{number_format($saData['NMONTO'], 2)}</label>
    </h1>
   </div>     
   <div class="row col-md-4 col-md-push-8">
      <!--<button type="submit"  name="Boton" value="Imprimir" class="center-block btn btn-info btn-lg btn-block"><i class="glyphicon glyphicon-download-alt"></i> Descargar Detalle </button>-->
      <a href="Paq1020.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>                   
   </div>
   {/if}
</div></div>
</div> 
</form>
<div id="footer">
</div>
</div>
</body>
</html>