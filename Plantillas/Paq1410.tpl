
<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
   function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
   }

   function myFunctionObs(e){
      if( $("input[type='radio']").is(':checked')){
         return window.confirm('¿Segúro que desea levantar la observación?');
      }
      else{
         window.alert('Seleccione un alumno');
      }
   }
   </script>

   <style>
   textarea{
      resize: none;
   }
   </style>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1410.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed table-hover">
               <thead>
                  <tr>
                     <th class="col-xs-1" style='text-align: left'>Nro Expediente</th>
                     <th class="col-xs-2" style='text-align: left'>Tipo Documento</th>
                     <th class="col-xs-1" style='text-align: left'>DNI</th>
                     <th class="col-xs-1" style='text-align: left'>Cod.Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre</th>
                     <th class="col-xs-1" style='text-align: center'>Activar</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                  <tr onclick="f_seleccionarFila(this)">
                    <td style='text-align: left'>E-{$i['CCODTRE']}</td>
                    <td style='text-align: left'>{$i['CDESCRI']}</td>
                    <td style='text-align: left'>{$i['CNRODNI']}</td>
                    <td style='text-align: left'>{$i['CCODALU']}</td>
                    <td style='text-align: left'>{$i['CNOMBRE']}</td>
                    <td style='text-align: center'><input type="radio" name="paData[CCODTRE]" value="{$i['CCODTRE']}" required/></td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type = "submit" class="center-block btn btn-success btn-lg btn-block" onclick = "return myFunctionObs()" name="Boton" value = "Observar">Levantar Obser.&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4">
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {/if}

      </div>
      </div>
    </div>
  </div>
</form>
</body>
</html>