<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>
   <script>
   function myFunctionObs(e){
      if ($("input[type='radio']").is(':checked')) {
         $('#modalNew').modal('show');
      } else {
         window.alert('Seleccione una Solicitud para Observar');
      }
   }

   function f_seleccionarFila(e) {
      let loRadio = e.querySelector('input[type="radio"]');
      if (loRadio != undefined && !loRadio.disabled) {
         loRadio.checked = true;
      }
   }

   function myFuncApro() {
      if (!$("input[type='radio']").is(':checked')){
         alert('Seleccione una Solicitud para Aprobar');
         return false;
      }
   }

   function f_FormularioChange(p_status) {   
      let formModalidad_cTipo = document.getElementById('formModalidad_cTipo');
      let formModalidad_cTipo2 = document.getElementById('formModalidad_cTipo2');
      let formModalidad_cTipo3 = document.getElementById('formModalidad_cTipo3');
      let formModalidad_cTipo4 = document.getElementById('formModalidad_cTipo4');
      if (p_status.value == 'Ordinario'){
         formModalidad_cTipo.style.display = "table-row";
         formModalidad_cTipo2.style.display = "none";
         formModalidad_cTipo3.style.display = "none";
         formModalidad_cTipo4.style.display = "none";
      } else if (p_status.value == 'Precatolica') {
         formModalidad_cTipo.style.display = "none";
         formModalidad_cTipo3.style.display = "none";
         formModalidad_cTipo4.style.display = "none";
         formModalidad_cTipo2.style.display = "table-row";
      } else if (p_status.value == 'ExtraordinarioI') {
         formModalidad_cTipo.style.display = "none";
         formModalidad_cTipo2.style.display = "none";
         formModalidad_cTipo3.style.display = "table-row";
         formModalidad_cTipo4.style.display = "none";
      } else if (p_status.value == 'ExtraordinarioII') {
         formModalidad_cTipo.style.display = "none";
         formModalidad_cTipo2.style.display = "none";
         formModalidad_cTipo3.style.display = "none";
         formModalidad_cTipo4.style.display = "table-row";
      }

   }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<form action="Tdo5100.php" method="post" enctype="multipart/form-data">
<div class="container-fluid divBody">
   {if $snBehavior == 0}
   <div class="panel panel-success">
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
      <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <div class="table-responsive mh-50">
         <table class="table table-condensed">
            <thead style="font-size: 15px;">
               <tr>
                  <th style='text-align: center;'>#</th>
                  <th style='text-align: left'>Fecha Recepción</th>
                  <th style='text-align: left'>Cod. Alumno</th>
                  <th style='text-align: left'>Nombre Alumno</th>
                  <th style='text-align: left'>Cod. Trámite</th>
                  <th style='text-align: left'>Trámite</th>
                  <th style='text-align: left'>Unidad Académica</th>
                  <th style='text-align: center'>Estado</th>
                  <th style='text-align: center'><i class="glyphicon glyphicon-ok"></i></th>
               </tr>
            </thead>
            <tbody style="font-size: 14px;">
               {$j = 0}
               {foreach from = $saDatos item = i}
               <tr onclick="f_seleccionarFila(this)" class = "Datos">
                  <td style='text-align: center;'>{$j + 1}</td>
                  <td style='text-align: left'>{$i['TFECHA']}</td>
                  <td style='text-align: left'>{$i['CCODALU']}</td>
                  <td style='text-align: left'>{$i['CNOMBRE']}</td>
                  <td style='text-align: left'>E-{$i['CCODTRE']}</td>
                  <td style='text-align: left'>{$i['CDESCRI']}</td>
                  <td style='text-align: left'>{$i['CNOMUNI']}</td>
                  <td style='text-align: center'>
                     {if $i['CESTADO']=='E'}   
                     <img src="Images/eye.png" width="30%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {else if $i['CESTADO']=='B'}
                     <img src="Images/aprobado.png" width="30%" data-toggle="tooltip" data-placement="bottom" title="Cerrado">
                     {else}
                     <img src="Images/clock.png" width="30%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  </td>
                  <td style='text-align: center' class="col-xs-1">
                     <input type="radio" name="pnIndice" value="{$j}"required/>
                  </td>
               </tr>
               {$j = $j + 1}
               {/foreach}
            </tbody>
         </table>
      </div>
   </div>
   </div>                                         
   <div class="row">
      <div class="col-xs-4">
         <button class="center-block btn btn-success btn-lg btn-block" onclick ="return myFuncApro();" name="Boton" value="Aprueba">Aprobar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
      </div>
      <div class="col-xs-4">
         <button type = "button" class="center-block btn btn-warning btn-lg btn-block" onclick ="myFunctionObs()">Observar&nbsp;&nbsp;<i class="glyphicon glyphicon-eye-open"></i></button>
      </div>
      <div class="col-sm-4">
         <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
   </div>
   {elseif $snBehavior == 1}
   <!--PROMEDIO PONDERADO SIN NOTAS DE INTERNADO-->
   <div class="panel panel-success">
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
      <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <input type="hidden" name = "paData[CCODTRE]" value={$saData['CCODTRE']}>
      <div class="table-responsive">
         <table class="table table-condensed">   
            <tr>
            <td><div class="form-group">
               <label for="icTramit">Nro Expediente / Fecha de Recepción</label>
               <input type="text" class="form-control" value="E-{$saDatos['CCODTRE']} / {$saDatos['TFECHA']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icPago">Código Pago / Recibo de Pago</label>
               <input type="text" class="form-control" id="icPago" value="{$saDatos['CNROPAG']} / {$saDatos['CRECIBO']}" readonly>
            </div></td>
            </tr><tr>
            <td><div class="form-group">
               <label for="icDescri">Tipo de Constancia</label>
               <input type="text" class="form-control" id="icDescri" value="{$saDatos['CDESCRI']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icNombre">Alumno: </label>
               <input type="text" class="form-control" id="icNombre" value="{$saDatos['CNRODNI']} - {$saDatos['CNOMBRE']}" readonly>
            </div></td>
            </tr><tr>
            <td><div class="form-group">
               <label for="icEmail">E-mail y Celular: </label>
               <input type="text" class="form-control" id="icEmail" value="{$saDatos['CEMAIL']} - {$saDatos['CNROCEL']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icAlumno">Código Alumno - Unidad Académica: </label>
               <input type="text" class="form-control" id="icAlumno" value="{$saDatos['CCODALU']} - {$saDatos['CNOMUNI']}" readonly>
            </div></td>
            </tr>
         </table>
         <table class="table table-condensed">
            <tr>
            <td>Promedio Ponderado:</td>
            <td><input type="number" step="any" class="form-control" name="paData[NPROMED]" placeholder="Ingrese el promedio ponderado, ejemplo: 12.3409" required></td>
            </tr><tr>
            <td>Nº Créditos Aprobados:</td>
            <td><input type="number" step="any" class="form-control" name="paData[NCREAPR]" placeholder="Ingrese cantidad de creditos aprobados, ejemplo: 162" required></td>
            </tr><tr>
            <td>Puesto Ocupado:</td>
            <td><input type="number" class="form-control" name="paData[NPUESOC]" placeholder="Ingrese el puesto ocupado, ejemplo: 12" required></td>
            </tr><tr>
            <td>Total de Alumnos:</td>
            <td><input type="number" class="form-control" name="paData[NTOTALU]" placeholder="Ingrese el total de alumnos, ejemplo: 123" required></td>
            </tr><tr>
            <td>Año de Ranking:</td>
            <td><input type="number" min="1000" max="9999" class="form-control" name="paData[NRANKIN]" placeholder="Ingrese el año de egresados, ejemplo: 2017" maxlength="4" required></td>
            </tr><tr>
            <td>Fecha de Egreso:</td>
            <td><input type="date" class="form-control" name="paData[DFECHAR]" required></td>
            </tr>
         </table>
      </div>
   </div>
   </div>
   <div class="row">
      <div class="col-xs-4">
         <button type="submit" class="center-block btn btn-success btn-lg btn-block" name="Boton" onclick="return confirm('¿Está seguro que desea enviar esta información?')" value="Aprueba">Aprobar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
      </div>
      <div class="col-xs-4"></div>
      <div class="col-sm-4">
         <a href="Tdo5100.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
   </div>
   {elseif $snBehavior == 2}
   <!--CONSTANCIA DE INGRESO-->
   <div class="panel panel-success">
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
      <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <input type="hidden" name = "paData[CCODTRE]" value={$saData['CCODTRE']}>
      <div class="table-responsive">
         <table class="table table-condensed">   
            <tr>
            <td><div class="form-group">
               <label for="icTramit">Nro Expediente / Fecha de Recepción</label>
               <input type="text" class="form-control" value="E-{$saDatos['CCODTRE']} / {$saDatos['TMODIFI']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icPago">Código Pago / Recibo de Pago</label>
               <input type="text" class="form-control" id="icPago" value="{$saDatos['CNROPAG']} / {$saDatos['CRECIBO']}" readonly>
            </div></td>
            </tr><tr>
            <td><div class="form-group">
               <label for="icDescri">Tipo de Constancia</label>
               <input type="text" class="form-control" id="icDescri" value="{$saDatos['CDESCRI']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icNombre">Alumno: </label>
               <input type="text" class="form-control" id="icNombre" value="{$saDatos['CNRODNI']} - {$saDatos['CNOMBRE']}" readonly>
            </div></td>
            </tr><tr>
            <td><div class="form-group">
               <label for="icEmail">E-mail y Celular: </label>
               <input type="text" class="form-control" id="icEmail" value="{$saDatos['CEMAIL']} - {$saDatos['CNROCEL']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icAlumno">Código Alumno - Unidad Académica: </label>
               <input type="text" class="form-control" id="icAlumno" value="{$saDatos['CCODALU']} - {$saDatos['CNOMUNI']}" readonly>
            </div></td>
            </tr>
         </table>
         <table class="table table-condensed">
            <tr>
            <th colspan="4" class="warning2">Datos requeridos para generar la constancia</th>
            </tr><tr>
            <td><label style='text-align: left'>Especialidad:</label></td>
            <td colspan="3"><input type="text" class="form-control" name="paData[CNOMUNI]" value="{$saDatos['CNOMUNI']}" readonly></td>
            </tr><tr>
            <td><label style='text-align: left'>Modalidad:</label></td>
            <td colspan="3"><input type="text" class="form-control" name="paData[CMODALI]" placeholder="Ingrese la modalidad con la que llevo la especialidad, ejemplo: Semipresencial" maxlength="30" required pattern="[A-Za-z]+"></td>
            </tr><tr>
            <td><label style='text-align: left'>Año de Admisión:</label></td>
            <td colspan="3"><input type="text" class="form-control" name="paData[CADMISI]" placeholder="Ingrese el año del proceso de admisión, ejemplo: 2013" maxlength="4" required pattern="[0-9]+"></td>
            </tr><tr>
            <td><label style='text-align: left'>Resolucion Nº</label></td>
            <td><input type="text" class="form-control" name="paData[NNUMRES]" placeholder="Número de resolución, ejemplo: 18785"  required pattern="[0-9]+"></td>
            <td>-R-</td>
            <td><input type="text" class="form-control" name="paData[CANORES]" placeholder="Año de resolución, ejemplo: 2013 " required maxlength="4" pattern="[0-9]+"></td>
            </tr><tr>
            <td><label style='text-align: left'>Fecha de Admisión:</label></td>
            <td colspan="4"><input type="date" class="form-control" name="paData[DFECHAR]" required></td>
            </tr>
         </table>
      </div>
   </div>
   </div>
   <div class="row">
      <div class="col-xs-4">
         <button type="submit" class="center-block btn btn-success btn-lg btn-block" name="Boton" onclick="return confirm('¿Está seguro que desea enviar esta información?')" value="Aprueba">Aprobar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
      </div>
      <div class="col-xs-4"></div>
      <div class="col-sm-4">
         <a href="Tdo5100.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
   </div>
   {elseif $snBehavior == 3}
   <!--MODALIDAD DE TITULACION-->
   <div class="panel panel-success">
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
      <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <input type="hidden" name = "paData[CCODTRE]" value={$saData['CCODTRE']}>
      <div class="table-responsive">
         <table class="table table-condensed">   
            <tr>
            <td><div class="form-group">
               <label for="icTramit">Nro Expediente / Fecha de Recepción</label>
               <input type="text" class="form-control" value="E-{$saDatos['CCODTRE']} / {$saDatos['TMODIFI']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icPago">Código Pago / Recibo de Pago</label>
               <input type="text" class="form-control" id="icPago" value="{$saDatos['CNROPAG']} / {$saDatos['CRECIBO']}" readonly>
            </div></td>
            </tr><tr>
            <td><div class="form-group">
               <label for="icDescri">Tipo de Constancia</label>
               <input type="text" class="form-control" id="icDescri" value="{$saDatos['CDESCRI']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icNombre">Alumno: </label>
               <input type="text" class="form-control" id="icNombre" value="{$saDatos['CNRODNI']} - {$saDatos['CNOMBRE']}" readonly>
            </div></td>
            </tr><tr>
            <td><div class="form-group">
               <label for="icEmail">E-mail y Celular: </label>
               <input type="text" class="form-control" id="icEmail" value="{$saDatos['CEMAIL']} - {$saDatos['CNROCEL']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icAlumno">Código Alumno - Unidad Académica: </label>
               <input type="text" class="form-control" id="icAlumno" value="{$saDatos['CCODALU']} - {$saDatos['CNOMUNI']}" readonly>
            </div></td>
            </tr>
         </table>
         <table class="table table-condensed">   
            <tr>
            <td><label>Título Profesional:</label></td>
            <td colspan="4"><input type="text" style="text-transform:uppercase" class="form-control" name="paData[CTITPRO]" placeholder="Ingrese el Titulo Profesional" required='required'></td>
            </tr><tr>
            <td><label>Título de Trabajo de Investigación:</label></td>
            <td colspan="4"><input type="text" style="text-transform:uppercase" class="form-control" name="paData[CTITINV]" placeholder="Ingrese el Titulo del Trabajo de Investigacion" required='required'></td>
            </tr><tr>
            <td><label>Modalidad:</label></td>
            <td colspan="4"><select class="selectpicker form-control form-control-sm col-12" name="paData[CMODALI]" data-live-search="true" required>
                  <option value="Unaminidad">Unaminidad</option>
                  <option value="Mayoria">Mayoria</option>
                  <option value="Unaminidad con Felicitación Pública">Unaminidad con Felicitación Pública</option>
                  </select></td>
            </tr><tr>
            <td><label>Consta en:</label></td>
            <td>Tomo:</td>
            <td><input style="text-transform:uppercase" class="form-control" type="text" name="paData[CNROTOM]" placeholder="Ingrese Tomo, ejem: XIIV" required='required'></td>
            <td>Nº Folio:</td>
            <td><input type="number" class="form-control" name="paData[NROFOLI]" placeholder="Ingrese Folio, ejem: 32" required='required'></td>
            </tr><tr>
            <td><label>Fecha de Titulación:</label></td>
            <td colspan="4"><input type="date" class="form-control" name="paData[DFECHAR]" required='required'></td>
            </tr><tr>
            <td><label>Copia de Acta de Titulación</label></td>
            <td colspan="4"><input name="poFile" accept="application/pdf, .png, .jpg, .jpeg" id = "file" type="file" class="file"></td>
            </tr>
         </table>
      </div>
   </div>
   </div>
   <div class="row">
      <div class="col-xs-4">
         <button type="submit" class="center-block btn btn-success btn-lg btn-block" name="Boton" onclick="return confirm('¿Está seguro que desea enviar esta información?')" value="Aprueba">Aprobar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
      </div>
      <div class="col-xs-4"></div>
      <div class="col-sm-4">
         <a href="Tdo5100.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
   </div>
   {elseif $snBehavior == 4}
   <!--PLAN DE ESTUDIOS CON CARGA HORARIA-->
   <div class="panel panel-success">
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
      <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <input type="hidden" name = "paData[CCODTRE]" value={$saData['CCODTRE']}>
      <div class="table-responsive">
         <table class="table table-condensed">   
            <tr>
            <td><div class="form-group">
               <label for="icTramit">Nro Expediente / Fecha de Recepción</label>
               <input type="text" class="form-control" value="E-{$saDatos['CCODTRE']} / {$saDatos['TMODIFI']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icPago">Código Pago / Recibo de Pago</label>
               <input type="text" class="form-control" id="icPago" value="{$saDatos['CNROPAG']} / {$saDatos['CRECIBO']}" readonly>
            </div></td>
            </tr><tr>
            <td><div class="form-group">
               <label for="icDescri">Tipo de Constancia</label>
               <input type="text" class="form-control" id="icDescri" value="{$saDatos['CDESCRI']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icNombre">Alumno: </label>
               <input type="text" class="form-control" id="icNombre" value="{$saDatos['CNRODNI']} - {$saDatos['CNOMBRE']}" readonly>
            </div></td>
            </tr><tr>
            <td><div class="form-group">
               <label for="icEmail">E-mail y Celular: </label>
               <input type="text" class="form-control" id="icEmail" value="{$saDatos['CEMAIL']} - {$saDatos['CNROCEL']}" readonly>
            </div></td>
            <td><div class="form-group">
               <label for="icAlumno">Código Alumno - Unidad Académica: </label>
               <input type="text" class="form-control" id="icAlumno" value="{$saDatos['CCODALU']} - {$saDatos['CNOMUNI']}" readonly>
            </div></td>
            </tr>
         </table>
         <table class="table table-condensed">   
            <tr>
            <td><label>Archivo con Plan de Estudios</label></td>
            <td colspan="4"><input type="file" id="file" name="poFile" class="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"></td>
            </tr><tr>
            <td><label>Modelo de Excel a Subir</label></td>
            <td colspan="4"><a class="btn btn-info btn-sm" href="./Xls/FormatoCargaHoraria.xlsx" target="#">Click para Descargar</a></td>
            </tr>
         </table>
         <label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* El formato a subir, debe contener sólo los datos del plan de estudios del solicitante.</label>
         <label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Para consultas adicionales sobre el formato Excel a llenar, puede hacerlas a la Oficina de Registro y Archivo Académico (Emisión de Constancias).</label>
      </div>
   </div>
   </div>
   <div class="row">
      <div class="col-xs-4">
         <button type="submit" class="center-block btn btn-success btn-lg btn-block" name="Boton" onclick="return confirm('¿Está seguro que desea enviar esta información?')" value="Aprueba">Aprobar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
      </div>
      <div class="col-xs-4"></div>
      <div class="col-sm-4">
         <a href="Tdo5100.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
   </div>
   {/if}
</div>
<div class="modal fade" id="modalNew" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">AGREGAR OBSERVACIÓN</h4>
         </div>
         <div class="modal-body">
            <table class="table table-condesed">
               <tr>
                  <th>OBSERVACIÓN</th>
                  <td><textarea style="text-transform:uppercase" id = "txtObser" class="form-control" rows = "4" name="paData[MOBSERV]" pattern="\d*"></textarea></td>
               </tr>
            </table>
         </div>
         <div class="modal-footer">
            <button type="submit" class="btn btn-warning" name="Boton" value="Observar">Observar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
         </div>
      </div>
   </div>
</div>
</form>
</body>
</html>