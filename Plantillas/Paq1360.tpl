<html>   
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script> 
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
         function myFunction(){
            $('#modalNew').modal('show');
         }
		});
	</script>
</head>
<body onload="f_Init()" class="divBody">
   <div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Seguimiento de Trámites </b></h3></div>
   <div class="container-fluid divBody">
   {if $snBehavior == 0}
   <form action="Paq1360.php" method="post">
      <div class="container-fluid">
      <div class="row col-md-10 col-md-push-1"> 
      <div class="panel panel-default">  
      <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITES</b></h3></div>
      <div class="panel-body">
         <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>       
      <div class="table-responsive mh-50">
      <table class="table table-condensed"> 
         <thead> 
           <th style='text-align: center' class="col-xs-2">Codigo de Alumno</th>  
           <th style='text-align: center' class="col-xs-3">Nombre de Alumno</th>
           <th style='text-align: center' class="col-xs-4">Tipo</th>
           <th style='text-align: center' class="col-xs-3">Estado</th>       
         </thead>
         {foreach from = $saDatos item = i}      
            <tr>         
               <td style='text-align: center' class="col-xs-2">{$i['CCODALU']}</td>        
               <td style='text-align: left' class="col-xs-5">{$i['CNOMBRE']}</td>
               <td style='text-align: left' class="col-xs-2">{$i['CDESCRI']}</td>
               <td style='text-align: center'><h4>
                  {if $i['CESTADO'] == 'E' }
                     <span class="label label-default">PENDIENTE</span> 
                  {elseif $i['CESTADO'] == 'A' }
                     <span class="label label-success">APROBADO</span>
                  {elseif $i['CESTADO'] == 'U' }  
                     <span class="label label-warning">USADO</span>
                  {elseif $i['CESTADO'] == 'R' }  
                     <span class="label label-danger">RECHAZADO</span>
                  {elseif $i['CESTADO'] == 'X' }  
                     <span class="label label-info">ANULADO</span>
                  {/if}
                  </h4></td>
             </tr>
         {/foreach}
      </table>
      </div>
      </div>
      </div>    
      </div>
      <div class="row">
         <div class="col-sm-4">
            <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#modalNew" role = "button" onclick="myFunction();">Solicitar&nbsp;&nbsp;<i class="glyphicon glyphicon-plus"></i></a>
         </div>  
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu0000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div> 
      </div> 
      </div> 
      </div>
   </form>
   <div id="footer"></div>
   <div class="modal fade" id="modalNew" role="dialog">
   <div class="modal-dialog">
   <form action="Paq1360.php" method="POST">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">AGREGAR SOLICITUD</h4>
         </div>
      <div class="modal-body">
      <table class="table table-condesed">
         <tr>
            <th>Codigo de Alumno</th>
            <td><input  class="form-control" name="paData[CCODALU]" pattern="\d*" type="text" placeholder="Codigo de Alumno" maxlength="10" minlength="10"></td>
         </tr><tr>
            <th>Porcentaje de descuento</th>
            <td><input  class="form-control" name="paData[NDESCUE]" pattern="\d*" type="text" ></td>
         </tr>
      </table>
      </div>
      <div class="modal-footer">
         <button onsubmit="return false" class="btn btn-success" name="Boton" value="Grabar">Agregar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
      </div>
   </form>
   </div>
   </div>
   </div>
   {/if}
</body>
</html>