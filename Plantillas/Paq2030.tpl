<html>
  <head>
    <title>Mantenimiento de Unidades Académicas</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Styles/css/bootstrap.css">
    <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
    <script src="Styles/js/jquery-3.2.0.js"></script>
    <script src="Styles/js/bootstrap.js"></script>
    <script type="text/javascript" src="CSS/jquery.modal.js"></script> 
  </head>
  <body>
    <div class="panel-heading" style="background-color: #245433; color: #ffffff;"><h3><b>Trámite administrativo</b></h3></div>
      <form action="Paq2030.php" method="post"> 
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="panel with-nav-tabs panel-success">
                <div class="panel-heading"><h4>Mantenimiento de Unidades Académicas</h4></div>
                  <div class="panel-body">   
                    {if $snBehavior == 0}
                      <p class="text-muted"><b>{$saData['CNOMBRE']}</b></p>   
                      <div class="table-responsive">
                        <table class="table table-hover"> 
                          <th style='text-align: center'>Cod. Unidad Académica</th>
                          <th style='text-align: center'>Nombre</th>
                          <th style='text-align: center'>Estado</th>
                          <th style='text-align: center'>Fecha</th>
                          <th></th>
                          {$j = 1}
                          {foreach from = $saDatos item = i}
                            <tr>
                              <td style='text-align: center'>{$i['CUNIACA']}</td>
                              <td style='text-align: left'>{$i['CNOMUNI']}</td>
                              {if $i['CESTADO']=='A'}
                                <td style='text-align: center; color: #6fe300'>ACTIVO</td>
                              {else}
                                <td style='text-align: center; color: #e60000'>INACTIVO</td>
                              {/if}
                              <td style='text-align: center'>{$i['TMODIFI']}</td>
                              <td style='text-align: center'><input type="radio" name="paData[CUNACED]" value="{$i['CUNIACA']}"/></td>
                            </tr>
                            {$j = $j + 1}
                          {/foreach}
                        </table> 
                      </div>
                    </div>
                  </div>
                <div class="row"> 
                  <div class="col-sm-4">
                    <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#myModal" role="button"> Nuevo <i class="glyphicon glyphicon-plus-sign"></i></a> 
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header modal-header-success">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title"><b>Crear Unidad Academica </b></h3>
                          </div>
                          <div class="modal-body">
                            <table class="table table-hover">
                              <tr>
                                 <td><b>CÓDIGO U. ACADÉMICA </b></td>
                                 <td><input class="form-control" type='text' placeholder="Ingrese codigo..." maxlength="2" minlength="2"  name="paData[CUNIACA]"></td>
                              </tr>
                              <tr>
                                 <td><b>NOMBRE U. ACADÉMICA </b></td>
                                 <td><input class="form-control" type='text' placeholder="Ingrese nombre..." maxlength="100" name="paData[CNOMUNI]"></td>
                              </tr>
                            </table>     
                            <div class="modal-footer">
                              <div class="row">
                                <div class="col-sm-6"><input type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block" title="Registra nuevo paquete"/></div>
                                <div class="col-sm-6"><button type="button" class="center-block btn btn-danger btn-lg btn-block" data-dismiss="modal">Cancelar</button></div>
                              </div>  
                            </div>
                            </div>
                            </div>      
                         </div>
                         </div>
                      </div>     
                      <div class="col-sm-4">
                        <button type="submit" name="Boton" value="Editar" class="center-block btn btn-info btn-lg btn-block"/> Editar <i class="glyphicon glyphicon-edit"></i>
                      </div>
                      <div class="col-sm-4">
                        <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
                      </div>
                    </div>      
                    </div>
           {elseif $snBehavior == 1}
            <h4 class="modal-title"><b>Editar Unidad Académica </b></h4>      
            <div class="modal-body">
               <table class="table table-hover">
                  <tr>
                     <td><b>CÓDIGO U. ACADÉMICA </b></td>         
                     <td>
                        <input class="form-control" type='text' name="paData[CUNIACA]" value="{$saDatos[0]['CUNIACA']}" readonly>
                     </td>
                  </tr>
                  <tr>
                     <td><b>NOMBRE U. ACADÉMICA </b></td>
                     <td><input class="form-control" type='text' name="paData[CNOMUNI]" value="{$saDatos[0]['CNOMUNI']}" style="text-transform:uppercase"  ></td> 
                  </tr>
                  <tr>
                     <td><b>CODIGO DE USUARIO </b></td>
                     <td><input class="form-control" type='text' name="paData[CCODUSU]" value="{$saDatos[0]['CCODUSU']}" style="text-transform:uppercase"  ></td>
                  </tr>
                  <tr>
                     <td><b>ESTADO </b></td>
                     <td>
                        <select class="form-control" name="paData[CESTADO]">
                           <option value="A" {if $saDatos[0]['CESTADO'] == 'A'} selected="true"{/if}>ACTIVO</option>
                           <option value="I" {if $saDatos[0]['CESTADO'] == 'I'} selected="true"{/if}>INACTIVO</option>
                        </select>          
                     </td>         
                  </tr>
               </table>
            </div>
         </div>
      </div>
            <div class="row col-md-4 col-md-push-8">
                <button type="submit" name="Boton" value="Actualizar" class="center-block btn btn-success btn-lg btn-block"> Actualizar <i class="glyphicon glyphicon-refresh"></i></button>    
               <a href="Paq2010.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a> 
            </div>  
            </div>
         {/if}   
         </div>
         </div>
      </div>
      </div>
    </form>
</html>