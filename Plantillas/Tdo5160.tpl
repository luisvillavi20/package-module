<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<script>
   function f_Calculavalor(){
		var lntotal = 0.0;
      var lnCosCre = parseFloat($('[name="paData[NCOSCRE]"]').val()); //68
      var lnCredi = parseFloat($('[name="paData[NCREDIT]"]').val()); //1
      lntotal =  lnCredi * lnCosCre;
      console.log('calcular');
	   $('#Costo').html(Number(lntotal).toFixed(2));
      $('[name="paData[NMONTO]"]').val(Number(lntotal).toFixed(2));
   }

   function f_Mostrar() {
      document.getElementById("mySelect").disabled = false;
      document.getElementById("Mostrar").style.display = '';
      document.getElementById("Mostrar1").disabled = false;
   }

   function f_ConsultaUniAca(data){
      var CodAlu=$("#CodAlu option:selected").val();nCredit
      var lcSend = "ID=ConsultaUniAcaCosto&paData[CCODALU]='"+CodAlu+"'";
      $.post("Tdo5160.php", lcSend).done(function(lcResult) {
         var laJson = JSON.parse(lcResult);
         if (laJson.ERROR) {
            alert(laJson.ERROR);
         } else {
            $('[name="paData[NCOSCRE]"]')[0].value = laJson.NCOSCRE;
            $('#nCosCre').html(laJson.NCOSCRE);
            $('#cNomUni').html(laJson.CNOMUNI);
            var lnCosCre = parseFloat($('[name="paData[NCOSCRE]"]').val()); //68
            if ($("#nCredit option:selected").val() != "Seleccione cantidad de créditos") {
              var lnCredi = parseFloat($('[name="paData[NCREDIT]"]').val()); //1
              lntotal =  lnCredi * lnCosCre;
              $('#Costo').html(Number(lntotal).toFixed(2));
              $('[name="paData[NMONTO]"]').val(Number(lntotal).toFixed(2));
            }
         }
      });
   }
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Matrícula por Créditos </b></h3></div>
<form action="Tdo5160.php" method="post"  enctype="multipart/form-data">
   <input type="hidden" name="paData[NCOSCRE]" value="{$saData['NCOSCRE']}">
   <input type="hidden" name="paData[CPERIOD]" value="{$saData['CPERIOD']}">
   <div class="container">
      <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}  
      <div id="myModal" class="modal fade">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><b>Generación De Pensión Por Créditos</b></h4>
               </div>
               <div class="modal-body">
                  <p><b>Solo se pueden admitir desde 0.5 hasta 11.5 créditos, en la matrícula por créditos, a partir de 12 créditos se considera una matrícula regular.</b></p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
               </div>
            </div>
         </div>
      </div>
      <div class="panel-heading"><h3 class="panel-title"><b>GENERACIÓN DE PENSIÓN POR CRÉDITOS</b></h3></div>
      <div class="panel-body">    
        <div class="col-md-12 form-line">  
          <div class="form-group">
            <table class="table table-condensed table-responsive">   
               <tr class="warning2">
                  <th colspan="2">Datos del Alumno:</th>
               </tr><tr>
                  <th><label>Código: </label></th>
                  <th>
                     <select id="CodAlu" onchange="f_ConsultaUniAca()">
                        {foreach from=$saDatos item=i}
                           <option value="{$i['CCODALU']}">{$i['CCODALU']}</option>
                        {/foreach}
                     </select>
                  </th>
                  <!--<th><label>{$saData['CCODALU']}</label></th>-->
               </tr><tr>
                  <th><label>Nombres: </label></th>
                  <th><label>{$saData['CNOMBRE']}</label></th>
               </tr><tr>
                  <th><label>Unidad Académica: </label></th>
                  <th><label id="cNomUni">{$saData['CNOMUNI']}</label></th>
               </tr><tr>
                  <th><label>Periodo Académico: </label></th>
                  <th><label>{substr_replace($saData['CPERIOD'], '-', 4, 0)}</label></th>
               </tr><tr>
                  <th><label>Créditos Registrados: </label></th>
                  <th><label>{number_format($saData['NCREREG'], 1)}</label></th>
               </tr><tr>
                  <th><label>Cantidad de Créditos: </label></th>
                  <th><select class="form-control" id="nCredit" name="paData[NCREDIT]" onchange="f_Calculavalor()">
                        <option select="true" required> Seleccione cantidad de créditos</option>
                           {for $i = 0.5; $i <= 11.5-$saData['NCREREG']; $i=$i+0.5}
                              <option value="{$i}" class="monto" >{$i}</option>
                           {/for}
                        </select></th>
               </tr><tr>
                  <th><label>Costo por Crédito: </label></th>
                  <th>S/.<label id="nCosCre">{$saData['NCOSCRE']}</label></th>
               </tr><tr> 
                  <th><label>Monto Total: </label></th>
                  <th>S/.<label id="Costo"></label></th>
                  <input type = "hidden" name="paData[NMONTO]">
               </tr><tr>
            </table>
          </div>
        </div>
      </div>
      </div>
      <label style="font-size:14px; text-align:center; color: #000000; font-style: italic; ">* Los créditos registrados son aquellos créditos que fueron pagados.</label>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" onclick="return confirm('¿Está seguro que desea generar su deuda?')" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
         </div>  
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu0000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>      
      {/if}
      </div> 
      </div>      
    </div>    
  </div>
</form>
<div id="footer">
</div>
</body>
</html>
