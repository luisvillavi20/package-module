<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<body class="divBody">
   <div class="panel-heading divHeader" style="padding: 0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos</b></h3></div>
   <!--Responsive!-->
   <div class="container-fluid divBody table-responsive">
   <form action="Paq1280.php" method="post">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-success">
               <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA PENDIENTES - CERTIFICADO DE CONDUCTA</b>
                  <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
               </div>
               <div class="panel-body">
                  <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th>DNI</th>
                           <th>Código alumno</th>
                           <th>Nombre</th>
                           <th>Unidad academica</th>
                           <th>Activar</th>
                        </tr>
                     </thead>
                     <tbody>
                        {foreach from=$saDatos item=i}
                        <tr>
                           <td>{$i['CNRODNI']}</td>
                           <td>{$i['CCODALU']}</td>
                           <td>{$i['CNOMBRE']}</td>
                           <td>{$i['CNOMUNI']}</td>
                           <td><input type="radio" name="paData[CCODALU]"></td>
                        </tr>
                        {/foreach}
                     </tbody>
                  </table>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-4">
                  <button class="btn btn-lg btn-success btn-block">Activar</button>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <button class="btn btn-lg btn-danger btn-block">Salir</button>
               </div>
            </div>
         </div>
      </div>
   </form>
</body>
</html>