<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script>
      function fileValidator(e)
      {
         alert("Archivo "+e.value.replace("C:\\fakepath\\", "")+" seleccionado correctamente");
      }
   </script>
</head>
<body class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Solicitudes de Autenticacion</b></h3></div>
<div class="container-fluid divBody">
<form action="Tdo2750.php" method="post" enctype="multipart/form-data">
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title"><b>Bandeja de Solicitudes de Mesa de Partes Virtual</b></h3></div>
<div class="panel-body">
   
   <div class="table-responsive">
     <table class="table table-condensed">   
       <th><label> Descripcion Breve:</label></th>
         <th><textarea style="resize: none; text-transform: uppercase;" class="form-control" name="paData[CDESCOR]" rows="3"></textarea></th>
     </table>
   </div>   
   <div class="table-responsive">
   <table class="table table-condensed"> 
      <thead>
         <tr> 
            <th class="col-xs-4">Tipo de Documento</th>  
            <th class="col-xs-8">Seleccionar</th>  
            <th class="col-xs-1">Subir</th>  
         </tr>
      </thead>
      <tbody>
         <form action="Tdo2750.php" method="POST" enctype="multipart/form-data">
          <tr>
             <td class = "col-xs-4">{$saDatos['CDESCRI']}</td>
             <td class="col-xs-8">
             <input type="hidden" name="paData[CCODTRE]" value="{$saDatos['CCODTRE']}"/>
             <input type="file" name="fAutenticacion" onchange="fileValidator(this);"/>
             </td>
             <td><button type="submit" class="btn btn-default btn-block btn-xs" onclick="return confirm('¿Seguro que desea subir este archivo?')" name="Boton" value="SubirDocumento" >
			   	   <img src="Images/aprobado.png" width="20" height="20"></button></td>  
          </tr>
         </form>
      </tbody>
      <tr><td colspan = "3"><label style="font-size:14px; text-align:left; color: #9B0000; font-style: italic; ">* Aqui debe subir el documento de la solicitud con la informacion pertinente y dirigido a la entidad correspondiente</label></td></tr>      
      <tr><td colspan = "3"><label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Los Archivos subidos tienen que estar en formato pdf y el peso no debe exceder de los 2Mb.</label></td></tr>      
      
   </table>
   </div>  
</div>
</div>
<div>
   <div class="col-xs-3"></div>
   <div class="col-xs-6"><a href="index.php" role="button" class="btn btn-danger btn-block btn-lg">Salir</a></div>
   <div class="col-xs-3"></div>
</div>
</div>
</form>
<div id="footer">
</div>
</div>
</body>
</html>