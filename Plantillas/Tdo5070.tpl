<html>
<head>
<title>Trámites</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="Styles/css/bootstrap.css">
<link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
<link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
<link rel="stylesheet" href="Styles/css/bootstrap-select.css">
<link rel="stylesheet" href="CSS/style.css">
<script src="js/java.js"></script>
<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
<script src="Styles/js/jquery-3.2.0.js"></script>
<script src="Styles/js/bootstrap.js"></script>
<script src="Styles/js/bootstrap-select.js"></script>
<script>
   function f_Init(p_nBehavior) {
      if (p_nBehavior == 0) {
         f_CargarSolictudes();
      }
   }

   function f_validarDocentesAsignados(){
      var cJurad1 = $(".cJurad1")[0].innerHTML;
      var cJurad2 = $(".cJurad1")[0].innerHTML;
      if(cJurad1 == 'SIN ASIGNAR' || cJurad2 == 'SIN ASIGNAR'){
         alert('EL REPORTE NO SE PUEDE GENERAR SIN DOCENTES ASIGNADOS');
         return false;
      }
      var cJurad1 = $(".cJurad1")[1].innerHTML;
      var cJurad1 = $(".cJurad1")[1].innerHTML;
      if(cJurad1 == 'SIN ASIGNAR' || cJurad2 == 'SIN ASIGNAR'){
         alert('EL REPORTE NO SE PUEDE GENERAR SIN DOCENTES ASIGNADOS');
         return false;
      }
      return true; 
   }

   function f_seleccionarFila(e) {
      var a = e.cells[e.cells.length-1].children.item(0);
      a.checked = true;
   }

   function f_CargarSolictudes() {
      var lcEstAsi = $("#pcEstAsi").val();
      if (lcEstAsi == null || lcEstAsi.trim() === "" || lcEstAsi.trim().length != 1) {
         alert("DEBE SELECCIONAR UN ESTADO DE SOLICITUD");
         $("#pcEstAsi").focus();
         return;
      }
      $('#divSolicitudes').html('');
      var lcSend = "Id=cargarSolicitudes&paData[CESTASI]=" + lcEstAsi;
      $.post("Tdo5070.php",lcSend).done(function(p_cResult) {
         var loJson = (isJson(p_cResult.trim()))? JSON.parse(p_cResult.trim()) : p_cResult;
         if (loJson.ERROR){
            alert(loJson.ERROR);
            return;
         }
         $('#divSolicitudes').html(p_cResult);
      });
   }
</script>
</head>
<body class="divBody" onload="f_Init({$snBehavior})">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Cursos por Jurado </b></h3></div>
<div class="container-fluid">
<form action="Tdo5070.php" method="POST">
   {if $snBehavior == 0}
   <div class="panel panel-default">
   <div class="panel-heading">
      <h3 class="panel-title"><b>CURSOS POR JURADO - ASIGNACION DE DOCENTES</b>
         <select id="pcEstAsi" class="selectpicker col-sm-2" onchange="f_CargarSolictudes();">
            <option value="S">ASIGNADOS</option>
            <option value="N">SIN ASIGNAR</option>
         </select>
         <span style="float:right"><b>ENCARGADO: </b>  {$scNombre}</span>
      </h3>
   </div>
   <div class="panel-body table-responsive mh-50">
      <table class="table table-condensed table-hover">
         <thead>
            <tr>
            <th>Fecha</th>
            <th>Código alumno</th>
            <th>Nombre</th>
            <th>Unidad Académica</th>
            <th style="text-align: center">Activar</th>
            </tr>
         </thead>
         <tbody id="divSolicitudes"></tbody>
      </table>
   </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         <button name="Boton" value="Activar" class="btn btn-success btn-block btn-lg">Activar</button>
      </div>
      <div class="col-sm-4"></div>
      <div class="col-sm-4">
         <a href="Mnu1000.php" class="btn btn-danger btn-block btn-lg">Salir</a>
      </div>
   </div>
   {elseif $snBehavior == 1}
   <div class="panel panel-default">
   <div class="panel-heading"><h3 class="panel-title"><b>CURSOS POR JURADO - ASIGNACION DE DOCENTES</b>
      <span style="float:right"><b>ENCARGADO: </b>  {$scNombre}</span></h3>
   </div>
   <div class="panel-body">
      <input type="hidden" name="paData[CNRODNI]" value={$saData['CNRODNI']}>
      <input type="hidden" name="paData[CRECIBO]" value={$saData['CRECIBO']}>
      <input type="hidden" name="paData[CCODALU]" value={$saData['CCODALU']}>
      <table class="table table-condensed">
         <tr>
            <th>Estudiante:</th>
            <td>{$saData['CCODALU']} - {$saData['CNRODNI']} - {$saData['CNOMALU']}</td>
            <th>Escuela:</th>
            <td>{$saData['CUNIACA']} - {$saData['CNOMUNI']}</label></td>
         </tr>
         <tr>
            <th>Email:</th>
            <td>{$saData['CEMAIL']}</td>
            <th>Celular:</th>
            <td>{$saData['CNROCEL']}</td>
         </tr><tr>
            <th>ID Pago:</th>
            <td>{$saData['CNROPAG']}</td>
            <th>Fecha Pago:</th>
            <td>{$saData['TPAGO']}</label></td>
         </tr>
      </table>
      <table class="table table-hover">
         <thead>
            <tr>
            <th style='text-align: left'>Curso</th>
            <th style='text-align: center'>Cred. Curso</th>
            <th>Jurado 1</th>
            {if !$saData['CNIVUNI']|in_array:['03','04']}
            <th>Jurado 2</th>
            {/if}
            </tr>
         </thead>
         <tbody>
            {foreach from = $saDatos item = i}
            {$lcCodCur = $i['CCODCUR']}
            <tr>
               <td style='text-align: left'>{$i['CCODCUR']} - {$i['CDESCRI']}</td>
               <td style='text-align: center'>{$i['NCRETOT']}</td>
               {if $i['CJURAD1'] == '0000'}                           
                  <td class = "cJurad1">SIN ASIGNAR</td>
               {else}
                  <td class = "cJurad1">{$i['CNOMJU1']}</td>
               {/if}
               {if !$saData['CNIVUNI']|in_array:['03','04']}
               {if $i['CJURAD2'] == '0000'}                           
                  <td class = "cJurad2">SIN ASIGNAR</td>
               {else}
                  <td class = "cJurad2">{$i['CNOMJU2']}</td>
               {/if}
               {/if}
            </tr>
            {/foreach}
         </tbody>
      </table>
   </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         <button class="btn btn-success btn-block btn-lg" onclick = "return f_validarDocentesAsignados()" name="Boton" value="Report">Reporte <i class="glyphicon glyphicon-print"></i></button>
      </div>
      <div class="col-sm-4"></div>
      <div class="col-sm-4">
         <button class="btn btn-danger btn-block btn-lg" name="Boton" value="Salir">Salir</button>
      </div>
   </div>
   {/if}
</form>
</body>
</html>