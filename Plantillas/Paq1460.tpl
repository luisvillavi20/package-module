<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>
   <script>
      $(document).ready(function() {
         $('#editarSolucitud').on('shown.bs.modal', function (e) {
            let nSerial = $('input[name="nSerial"]:checked').val();
            if (nSerial == undefined) {
               $('#editarSolucitud').modal('hide');
               alert('Seleccione una solicitud para aprobar');
            } else {
               $('#editarSolucitud input[name="paData[NSERIAL]"]').val(nSerial);
               $('#editarSolucitud input[name="paData[NSERIAL1]"]').text(nSerial);
            }
         });
      });

      $(document).ready(function () {
      $('table tr').each(function(a,b){
                  $(b).click(function(){
                        $('table tr').css('background','#ffffff');
                        $(this).css('background','#e6e6e6');   
                  });
               });
      });
      function f_seleccionarFila(e) {
               var a = e.cells[e.cells.length-1].children.item(0);
               a.checked = true;
      }
      
      function f_ValidaNumericos(e) {
         if (e.which == 8) {
            return true;
         }
         if (e.key < '0' || e.key > '9') {
            return false;
         }
         return true;
      }
      function f_ValidaAlumno(e) {
         var lcCodAlu = document.getElementById('text').value; 
         if (lcCodAlu.length == 9) {
            $.get("Paq1460.php?Id=Verificar&CCODALU=" + lcCodAlu + e.key, (data) => {
               let json = JSON.parse(data);
               if (json['ERROR'] != undefined) {
                  $('#cNomAlu').css("color", "#ce4646");
                  $('#cNomAlu').text(json.ERROR);
               } else {
                  $('#cNomAlu').css("color", "#333333");
                  $('#cNomAlu').text(json.CNOMBRE);
               }
            });
         }
      }
   </script>

</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1460.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed">
               <thead>
                  <tr>
                     <th class="col-xs-2" style='text-align: center'>Fecha Recepción</th>
                     <th class="col-xs-2" style='text-align: left'>Tip.Solicitud</th>
                     <th class="col-xs-1" style='text-align: left'>Cód.Solicitante</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre Solicitante</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre Egresado</th>
                     <th class="col-xs-1" style='text-align: center'>Activar</th>
                  </tr>
               </thead>
                  {foreach from = $saDatos item = i}
                  <tr onclick="f_seleccionarFila(this)">
                    <td style='text-align: center'>{$i['TMODIFI']}</td>
                    <td style='text-align: left'>{$i['CDESCRI']}</td>
                    <td style='text-align: left'>{$i['CCODUSU']}</td>
                    <td style='text-align: left'>{$i['CNOMPAD']}</td>
                    <td style='text-align: left'>{$i['CNOMBRE']}</td>
                    <td style='text-align: center'><input type="radio" name="nSerial" value="{$i['NSERIAL']}"/></td>
                  </tr>
                  {/foreach}
            </table>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-4">
            <button type="button" data-toggle="modal" href="#GenerarSolucitud" class="btn btn-block btn-success btn-lg">Generar Solicitud</button>
         </div>
         <div class="col-xs-4"></div>
         <div class="col-xs-4">
            <button type="submit" onclick="return confirm('¿Esta seguro que desea Denegar la Solicitud?')" name="Boton" value="Denegar" class="center-block btn btn-warning btn-lg btn-block"> Denegar <i class="glyphicon glyphicon-remove"></i></button>
         </div>
      </div>
      </div>                                         
      <div class="row">
         <div class="col-sm-4"></div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
<form action="Paq1460.php" method="POST">
<div class="modal fade" id="GenerarSolucitud" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">DESCUENTO BACHILLER POR SEGURO DE PADRES</h4>
   </div>
   <div class="modal-body">
      <table class="table" id="table_edit_documents">
         <th>Código de Alumno</th>
         <td><input type="text" id="text" class="form-control" name="paData[CCODALU]" maxlength="10" placeholder="Ingrese Código de alumno..." onkeydown="f_ValidaAlumno(event);" onkeypress="return f_ValidaNumericos(event); "></td>
         </tr><tr>
         <th>Nombre de Alumno</th>
         <td><label><b id="cNomAlu" style="width: 250px"></b></label></td>
         </tr><tr>
         <th>Cantidad de descuento</th>
         <td><input name="paData[NMONTO]" maxlength="4" class="form-control"></td>
         </tr>
      </table>
   </div>
   <div class="modal-footer">
      <div class="row">
         <div class="col-sm-6">
            <input type="submit" name="Boton" value="Solicitar" onclick="return confirm('¿Está seguro que desea generar este descuento de Bachiller?')" class="center-block btn btn-success btn-lg btn-block" />
         </div>
         <div class="col-sm-6">
            <button type="button" class="center-block btn btn-danger btn-lg btn-block"data-dismiss="modal">Cancelar</button>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</form>
</div>
</body>
</html>