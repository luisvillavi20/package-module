<html>
<head>
   <title>Trámites Administrativos</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      function validaAlumno(e) {
         var lcNroDni = document.getElementById('text').value; 
         if (lcNroDni.length == 7) {
            $.get("Paq1180.php?Id=Verificar&CNRODNI=" + lcNroDni + e.key, (data) => {
               let json = JSON.parse(data);
               if (json['ERROR'] != undefined) {
                  $('#cNomAlu').css("color", "#ce4646");
                  $('#cNomAlu').text(json.ERROR);
               } else {
                  $('#cNomAlu').css("color", "#333333");
                  $('#cNomAlu').text(json.CNOMBRE);
               }
            });
         }
      }
      function f_changeUnidad(self) {
         $.get('Paq1220.php', {
            Id: 'CambioUnidadAcademica',
            paData: {
               CUNIACA: self.value,
               CPROYEC: $('[name="paData[CPROYEC]"]').val()
            }            
         }, function(data) {
            $('#table_tutoria').html(data);
         });
      }
      function f_changeProyecto(self) {
         $.get('Paq1220.php', {
            Id: 'CambioUnidadAcademica',
            paData: {
               CUNIACA: $('[name="paData[CUNIACA]"]').val(),
               CPROYEC: self.value
            }            
         }, function(data) {
            $('#table_tutoria').html(data);
         });
      }
   </script>
</head>
<body class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80"> Trámites Administrativos</b></h3></div>
<div class="container-fluid">
<form action="Paq1220.php" method="POST">
<div class="row">
{if $snBehavior == 0}
   <div class="col-md-8 col-md-push-2">
      <div class="panel panel-success">
         <div class="panel-heading">
            <h3 class="panel-title"><b>BANDEJA TUTORIA UNIVERSITARIA</b><span style="float: right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
         </div>
         <div class="panel-body">
            <div class="">
               <div class="col-xs-6">
                  <select name="paData[CPROYEC]" class="selectpicker form-control" data-live-search="true" onchange="f_changeProyecto(this)">
                     {foreach from = $saDatos['paProyec'] item = i}
                     <option value="{$i['CPROYEC']}">{$i['CPROYEC']} - {$i['CDESCRI']}</option>
                     {/foreach}
                  </select>
               </div>
               <div class="col-xs-6">
                  <select name="paData[CUNIACA]" class="selectpicker form-control" data-live-search="true" onchange="f_changeUnidad(this)">
                     <option value="" disabled selected>Seleccione Unidad Academica</option>
                     {foreach from = $saDatos['paUniAca'] item = i}
                     <option value="{$i['CUNIACA']}">{$i['CUNIACA']} - {$i['CNOMUNI']}</option>
                     {/foreach}
                  </select>
               </div>
            </div>
            <hr>
            <table class="table table-condesed">
               <thead>
                  <tr>
                  <th>Cod. Alumno</th>
                  <th>DNI</th>
                  <th>Nombre</th>
                  <th>Unidad Academica</th>
                  <th style="text-align: center">Activar</th>
                  </tr>
               </thead>
               <tbody id="table_tutoria">
               </tbody>
            </table>
         </div>
      </div>
      <div>
         <div class="col-sm-4">
            <button type="button" data-toggle="modal" href="#nuevoDialog" class="btn btn-success btn-block btn-lg">Nuevo</button>
         </div>
         <div class="col-sm-4">
            <button name="Boton" value="Editar" class="btn btn-primary btn-lg btn-block">Editar</button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="btn btn-danger btn-block btn-lg">Salir</a>
         </div>
      </div>
   </div>
{elseif $snBehavior == 1}
   <div class="col-md-8 col-md-push-2">
      <div class="panel panel-success">
         <div class="panel-heading">
            <h3 class="panel-title"><b>BANDEJA TUTORIA UNIVERSITARIA</b><span style="float: right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
         </div>
         <div class="panel-body">
            <input type="hidden" name="paData[NSERIAL]" value="{$saDatos['NSERIAL']}">
            <table class="table table-condensed">
               <tr>
               <th class="col-xs-4">Nombre del alumno</th>
               <td class="col-xs-8">{$saDatos['CNOMBRE']}</td>
               </tr>
               <tr>
               <th class="col-xs-4">Fecha de inscripción</th>
               <td class="col-xs-8">{$saDatos['DGENERA']}</td>
               </tr>
               <tr>
               <th class="col-xs-4">Estado</th>
               <td class="col-xs-8">
                  {if $saDatos['CESTTUT'] == 'A'}
                     APROBADO
                  {elseif $saDatos['CESTTUT'] == 'D'}
                     DESAPROBADO
                  {else}
                     SIN CALIFICAR
                  {/if}
               </td>
               </tr>
            </table>
            <div class="row col-xs-10 col-xs-push-1">
               <div class="col-xs-4">
                  <button class="btn btn-lg btn-block btn-success" name="Boton" value="Aprobar">Aprobar</button>
               </div>
               <div class="col-xs-4"></div>
               <div class="col-xs-4">
                  <button class="btn btn-lg btn-block btn-danger" name="Boton" value="Desaprobar">Desaprobar</button>
               </div>
            </div>
            <div class="row"></div>
         </div>
      </div>
      <div>
      <div class="col-xs-12">
         <button class="btn btn-lg btn-block btn-danger">Salir  <i class="glyphicon glyphicon-log-out"></i></button>
      </div>
      </div>
   </div>
{/if}
</div>
</form>
</div>
{if $snBehavior == 0}
   <div class="modal fade" id="nuevoDialog" role="dialog">
   <div class="modal-dialog">
   <form action="Paq1220.php" method="POST">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">AGREGAR NUEVO</h4>
         </div>
         <div class="modal-body">
         <table class="table table-condesed">
            <tr>
            <th>DNI Alumno</th>
            <td><input onkeydown="validaAlumno(event)" class="form-control" name="paData[CNRODNI]" pattern="\d*" type="text" placeholder="DNI de alumno" maxlength="8" minlength="8"></td>
            </tr><tr>
            <td colspan="2"><b id="cNomAlu"></b></td>
            </tr><tr>
            <td>Proyecto</td>
            <td>
               <select name="paData[CPROYEC]" class="selectpicker form-control" data-live-search="true" onchange="f_changeProyecto(this)">
                  {foreach from = $saDatos['paProyec'] item = i}
                     <option value="{$i['CPROYEC']}">{$i['CPROYEC']} - {$i['CDESCRI']}</option>
                  {/foreach}
               </select>
            </td>
            </tr>
         </table>
         </div>
         <div class="modal-footer">
            <button onsubmit="return false" class="btn btn-success" name="Boton" value="Agregar">Agregar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
         </div>
      </div>
   </form>
   </div>
   </div>
{/if}
</body>
</html>