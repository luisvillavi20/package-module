<html>   
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script> 
	
</head>
<script>
   function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
   }

   function myFunctionObs(e){
      if( $("input[type='radio']").is(':checked')){
         var observ;
         $('.obsrv:checked').each(function(indice, elemento){
            var fila = $(this).parents(".Datos");
            observ = fila.find(".mobsrv").val();
            $("#mObserv").val(observ);
            $("#obsModal").modal('show');
         });
      }
      else{
         window.alert('Seleccione un alumno');
      }
   }
   
</script>
<style>
   .modal-body {
       background: #fff;
       width: 500px;
       margin: 20px;
       padding: 20px;
       border-radius: 5px;
   }
   
   textarea {
      padding: small;
      resize: none;
      width: 100%;
   }
</style>
<body onload="f_Init()" class="divBody">
   <div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Seguimiento de Trámites </b></h3></div>
   <div class="container-fluid divBody">
   <form action="Paq1430.php" method="post">
   {if $snBehavior == 0}
      <div class="container-fluid">
      <div class="row col-md-10 col-md-push-1"> 
      <div class="panel panel-default">  
      <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITE</b></h3></div>
      <div class="panel-body">
         <p class="text-muted"><b>ENCARGADO: {$saData['CNOMBRE']}</b></p>       
      <div class="table-responsive mh-50">
      <table class="table table-condensed"> 
         <thead> 
            <th style='text-align: left' class="col-xs-1">Cod. Curso</th>
            <th style='text-align: left' class="col-xs-4">Nom. Curso</th>
            <th style='text-align: left' class="col-xs-2">Cod. de Alumno</th>
            <th style='text-align: center' class="col-xs-3">Nombre</th>
            <th style='text-align: center' class="col-xs-1">Estado</th>               
            <th style='text-align: center' class="col-xs-1">Seleccionar</th>               
         </thead>
         {foreach from = $saDatos item = i}  
            <tr onclick="f_seleccionarFila(this)" class="Datos">         
               <td style='text-align: left' class="col-xs-1">{$i['CCODCUR']}</td>
               <td style='text-align: left' class="col-xs-4">{$i['CDESCRI']}</td>
               <td style='text-align: left' class="col-xs-2">{$i['CCODALU']}</td>
               <td style='text-align: center' class="col-xs-3">{$i['CNOMBRE']}</td>
               <td style='text-align: center' class="col-xs-1">
                  {if $i['CESTESC'] == 'A'}
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
                  {elseif $i['CESTESC'] == 'D'}  
                     <img src="Images/denegar.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Denegado">
                  {elseif $i['CESTESC'] == 'P'}  
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
               </td>
               <td style='text-align: center' class="col-xs-1">         
                  <input  class = "obsrv" type="radio" name="paData[NSERIAL]" value="{$i['NSERIAL']}"/>
                  <input  type="hidden" name="paData[CIDENTI]" value="{$i['CIDENTI']}"/>
                  <input  class = "mobsrv" type = "hidden" value="{$i['MOBSERV']}" />
               </td>
            </tr>
         {/foreach}
      </table>
      </div>
      </div>
      </div>    
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type = "submit" class="center-block btn btn-success btn-lg btn-block" onclick ="return confirm('¿Segúro que desea levantar la observación?');" name="Boton" value ="Levantar">Levantar Obs.&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
         </div>  
         <div class="col-sm-4">
            <button type = "button" class="center-block btn btn-warning btn-lg btn-block" onclick ="myFunctionObs()">Ver Observacion&nbsp;&nbsp;<i class="glyphicon glyphicon-eye-open"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu0000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div> 
      </div> 
      </div> 
      </div>
      <div class="modal fade" id="obsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
           <div class="modal-content">
             <div class="modal-header">
               <h5 class="modal-title">OBSERVACION</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
             </div>
             <div class="modal-body">
             <table class="table table-condesed">
               <tr>
                  <td><textarea style="text-transform:uppercase" row="3" id = "mObserv" class="form-control" readonly></textarea></td>
               </tr>
             </table>
             </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
             </div>
           </div>
         </div>
      </div>
   </form>
   <div id="footer"></div>
   </div>
   {/if}
</body>
</html>

