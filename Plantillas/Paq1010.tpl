<html>
<head>
  <title>Trámites</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="Styles/css/bootstrap.css">
    <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
    <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
    <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
    <link rel="stylesheet" href="CSS/style.css">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    <script src="js/java.js"></script>
    <script src="Styles/js/jquery-3.2.0.js"></script>
    <script src="Styles/js/bootstrap.js"></script>
    <script src="Styles/js/bootstrap-select.js"></script>
  <script>
    function f_seleccionarFila(e) {
      let loRadio = e.querySelector('input[type="radio"]');
      if (loRadio != undefined && !loRadio.disabled) {
         loRadio.checked = true;
      }
    }

    var cont = 2;
    function agregarFila() {
      var nuevaFila   = '<tr>';
      nuevaFila  += '<th class="col-xs-2">TESISTA '+cont+':</th>';
      nuevaFila  += '<td class="col-xs-2"><input onblur="fxBuscarDNI(this)" type="text" class="form-control" maxlength="8" name="paData[CTESDN'+cont+']" id="pcNrDni'+cont+'" placeholder="DNI" title="Ingrese el DNI del tesista" minlength="8" required autofocus/></td>';
      nuevaFila  += '<td id="iTesis'+cont+'" colspan="3"><input type="hidden" name="paData[CTESIS'+cont+']" id="DatosHi'+cont+'"/> <input type="text" id="DatosAl'+cont+'" class="form-control" readonly></td>';
      nuevaFila  += '<td id="sTesis'+cont+'" style="display:none"><select name="paData[CTESIS'+cont+']" id="DatosAls'+cont+'" class="selectpicker form-control"></select></td>';
      nuevaFila  += '</tr>';
      if(cont<=3){
        $("#tBody").append(nuevaFila);
        cont++;
      }
    }
    function quitarFila(){
       trs=$("#tBody tr").length;
       if(trs>1)
       {
          $("#tBody tr:last").remove();
          cont--;
       }
    }

    function fxBuscarDNI(e){         
      var lcNroDni = e.value;
      var pos = e.id.substr(-1);
      var lcNomUni = $('#pcNomUni').val(); 
      $.get("Paq1010.php?DNI="+lcNroDni+"&CNOMUNI="+lcNomUni+"&Id=BuscarDNI", 
      function( data ) {
        var content  = jQuery.parseJSON(data);
        var Nombre = content[0].Nombre.split("/").join(" ");
        var tamaño  = content.length;
        if(tamaño > 1) {
          var sel = '#sTesis'+pos+'';
          var inp = '#iTesis'+pos+'';
          var selp = '#DatosAl'+pos+'';
          $.each(content,function(){
            var Nombre = this.Nombre.split("/").join(" ");
            var option = '<option value="'+this.Codigo+'">'+this.Codigo+'-'+Nombre+'-'+this.CNomUni+'</option>';
            $('#DatosAls'+pos).append(option);
            $('#DatosAls'+pos).selectpicker('refresh');
          });
          $(sel).css("display", "block");
          $(inp).css("display", "none");
        }
        else {
          document.getElementById("DatosAl"+pos).value = content[0].Codigo+"-"+Nombre+"-"+content[0].CNomUni;
          document.getElementById("DatosHi"+pos).value = content[0].Codigo;
        }
      });
    }
  </script>
  <style>
  .btn-circle {
      width: 32px;
      height: 32px;
      padding: 6px 0px;
      border-radius: 18px;
      text-align: center;
      font-size: 12px;
      line-height: 1.42857;
  }
  </style>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos - Generación de Deuda Para Bachiller</b></h3></div>
<form action="Paq1010.php" method="post">
<div class="container-fluid divBody">
   <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
        <div class="panel-heading"><h3 class="panel-title"><b>Generación de Deuda Para Bachiller - Datos del trámite</b></h3></div>   
        <div class="panel-body">
          <div class="col-md-12 form-line">  
            <div class="form-group">
              <div class="table-responsive">
                <table class="table table-condensed">   
                  <th><label> Código Alumno:</label></th>
                    <th colspan="1"><label>{$saData['CCODALU']}</label></th>
                    <th><label></label></th>
                    <th colspan="1"><label>{$saData['CNOMBRE']}</label></th>
                    <th><label>Escuela:</label></th>
                    <th colspan="2"><label>{$saData['CUNIACA']}</label></th>
                    <th><label></label></th>
                    <th colspan="2"><label>{$saData['CNOMUNI']}</label></th>
                </table>
              </div> 
              <table class="table table-condensed table-hover">                  
                <thead>
                <tr class="warning2">
                  <th width= '30%'><label>Detalle Paquete:</label></th>
                  <th></th><th></th>
                </tr><tr>
                      <tr>   
                        <th class="col-xs-2" style='text-align: left'>Cód. Paquete</th>
                        <th class="col-xs-4" style='text-align: left'>Descripción de paquete</th>
                        <th class="col-xs-1" style='text-align: center'>Activar</th>      
                      </tr>
                    </thead>
                    {foreach from = $saDatos item = i}
                      <tr onclick="f_seleccionarFila(this)">
                        <td style='text-align: left'>{$i['CIDPAQU']}</td>
                        <td style='text-align: left'>{$i['CDESCRI']}</td>
                        <td style='text-align: center'><input type="radio" required name="paData[CIDPAQU]"  value="{$i['CIDPAQU']}"/></td>
                      </tr>
                    {/foreach}
                  </table>
                </div>
            </div>
          </div>
        </div>
        </div>      
        <div>
          <div class="col-sm-4">
            <button type="submit" name="Boton" value="Aplicar" class="center-block btn btn-success btn-lg btn-block"> Aplicar <i class="glyphicon glyphicon-ok"></i></button>
          </div>
          <div class="col-sm-4">
          </div>
          <div class="col-sm-4">
            <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
          </div>
        </div>
      {elseif $snBehavior == 1}
        <div class="panel-heading"><h3 class="panel-title"><b>Detalle de Deuda para Bachiller - Datos del trámite</b></h3></div> 
        <div class="panel-body">
        <div class="col-md-12 form-line">  
          <div class="form-group">
            <input type="hidden" name="paData[CCODALU]" value="{$saData['CCODALU']}">
            <input type="hidden" name="paData[CIDPAQU]" value="{$saData['CIDPAQU']}">
            <input type="hidden" name="paData[NMONTOT]" value="{$saData['NMONTO']}">
            <div class="table-responsive">
            <table class="table table-condensed">
              <tr>
              <th><label> Código Alumno:</label></th>
              <th colspan="1"><label>{$saData['CCODALU']}</label></th>
              <th><label></label></th>
              <th colspan="1"><label>{$saData['CNOMBRE']}</label></th>
              <th><label>Escuela:</label></th>
              <th colspan="2"><label>{$saData['CUNIACA']}</label></th>
              <th><label></label></th>
              <th colspan="2"><label>{$saData['CNOMUNI']}</label></th>
              </tr><tr>
              <th>TELÉFONO:</th>
              <td colspan="8"><input type="text" class="form-control btn-lg" maxlength="9" name="paData[CNROCEL]" value="{$saData['CNROCEL']}" placeholder="Número Telefónico" title="Ingrese su Numero Telefónico" minlength="9" required autofocus/></td>
              </tr><tr>
              <th>CORREO:</th>
              <td colspan="8"><input type="text" class="form-control btn-lg" maxlength="50" name="paData[CEMAIL]" value="{$saData['CEMAIL']}" placeholder="Correo Electrónico" title="Ingrese su Correo Electrónico" required/></td>
              </tr><tr>
              <td colspan="9"><label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Los datos personales solicitados, serán utilizados para contactarlos en caso de algun inconveniente.</label></td>
              </tr>
              {if $saData['CCODIGO'] == 'T'}
              <table class="table table-condensed">
              <tr class="warning2">
                 <th colspan="4">Seleccione si ingresó por translado externo o no.</th>
              </tr>
              <tr>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODEXT]" value="S" required/> TRANSLADO EXTERNO</th>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODEXT]" value="N" required/> SIN TRANSLADO EXTERNO</th>
              </tr>
              </table>
              {/if}
            </table>
            </div>
            <!--{if $saData['CCODIGO'] == 'T'}
            <input type="hidden" name="pcNomUni" id="pcNomUni" value="{$saData['CUNIACA']}">
            <div class="table-responsive">
            <table class="table table-condensed">
            <thead>
              <tr class="warning2">
              <th><label>Detalle Tesista(s): </label></th>
              <th></th>
              <td style="float: right"> 
                <button style="float: rigth" type="button" onclick = agregarFila(); class="btn btn-success btn-circle"><i class="glyphicon glyphicon-plus"></i></button>
                <button style="float: rigth"type="button" onclick = quitarFila(); class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-minus"></i></button> 
              </td>
              </tr>
            </thead>
            <tbody id="tBody">
              <tr>
              <th class="col-xs-2">TESISTA 1:</th>
              <td class="col-xs-2"><input type="hidden" value="{$saData['CCODALU']}" name="paData[CTESIS1]" id="DatosHi1"/><input readonly name="paData[CTESDN1]" value="{$saData['CNRODNI']}" type="text" class="form-control" maxlength="8" name="pcNroDni" id="pcNrDni1" placeholder="DNI" title="Ingrese el DNI del tesista" minlength="9" required autofocus/></td>
              <td colspan="3"><input type="text" value="{$saData['CCODALU']}-{$saData['CNOMBRE']}-{$saData['CNOMUNI']}" id="DatosAl1" class="form-control" readonly/></td>
              </tr>
            </tbody>  
              <td colspan="6"><label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Ingrese el DNI de sus compañeros tesistas, de ser necesario. Verificar que los datos sean los correctos</label></td>
            </table>
            </div>
            {/if}-->
            {if $saData['CCODIGO'] == 'M'}
              {if $saData['CCODIDM'] == 'S'}
              <table class="table table-condensed">
              <tr class="warning2">
                 <th colspan="4">Seleccione el Centro o Modalidad con la cual realizó el idioma.</th>
              </tr>
              <tr>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="I" required/> CENTRO DE IDIOMAS DE LA UCSM</th>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="X" required/> CENTROS O INSTITUTOS EXTERNOS</th>
              </tr>
              </table>
              {/if}
              {if $saData['CCODINF'] == 'S'}
              <table class="table table-condensed">
              <tr class="warning2">
                 <th colspan="4">Seleccione el Centro o Modalidad con la cual realizó Informatica.</th>
              </tr>
              <tr>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODINF]" value="N" required/> INSTITUTO DE LA UCSM</th>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODINF]" value="X" required/> CENTROS O INSTITUTOS EXTERNOS</th>
              </tr>
              </table>
              {/if}
            {/if}
            {if $saData['CCODIGO'] == 'D'}
              {if $saData['CCODIDM'] == 'S'}
              <table class="table table-condensed">
              <tr class="warning2">
                 <th colspan="4">Seleccione el Centro o Modalidad con la cual realizó el idioma.</th>
              </tr>
              <tr>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="I" required/> AMBOS IDIOMAS EN EL CENTRO DE IDIOMAS DE LA UCSM</th>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="X" required/> CENTROS O INSTITUTOS EXTERNOS</th>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="M" required/> AL MENOS 1 IDIOMA EN EL CENTRO DE IDIOMAS DE LA UCSM</th>
              </tr>
              </table>
              {/if}
              {if $saData['CCODINF'] == 'S'}
              <table class="table table-condensed">
              <tr class="warning2">
                 <th colspan="4">Seleccione el Centro o Modalidad con la cual realizó Informatica.</th>
              </tr>
              <tr>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODINF]" value="N" required/> INSTITUTO DE LA UCSM</th>
                 <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODINF]" value="X" required/> CENTROS O INSTITUTOS EXTERNOS</th>
              </tr>
              </table>
              {/if}
            {/if}
            {if $saData['CCODIGO'] == 'B'}
            {if $saData['CCODIDM'] == 'S'}
            <table class="table table-condensed">
            <tr class="warning2">
               <th colspan="4">Seleccione el Centro o Modalidad con la cual realizó el idioma.</th>
            </tr>
            <tr>
               <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="I" required/> CENTRO DE IDIOMAS</th>
               <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="C" required/> INSTITUTO CONFUCIO</th>
               <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="T" required/> TOEFL ITP</th>
               <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="A" required/> CAMBRIDGE</th>
               <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODIDM]" value="N" required/> NINGUNO</th>
            </tr>
            <tr>
               <td colspan="9"><label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* La opción NINGUNO es sólo es para las Escuelas Profesionales que no tienen como requisito el Idioma Extranjero. Para el proceso de Convalidación, debe seleccionar el CENTRO en el cual realizó su trámite.</label></td>
            </tr>
            </table>
            {/if}
            {if $saData['CCODINF'] == 'S'}
            <table class="table table-condensed">
            <tr class="warning2">
               <th colspan="4">Seleccione el Centro o Modalidad con la cual realizó Informatica.</th>
            </tr>
            <tr>
               <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODINF]" value="N" required/> ANTIGUA MODALIDAD (INFORMATICA BASICA)</th>
               <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODINF]" value="S" required/> NUEVA MODALIDAD (CURSOS OFFICE)</th>
            </tr>
            </table>
            {/if}
            {/if}
            <table class="table table-condensed">         
              <thead>
                <tr class="warning2">
                <th colspan="6" ><label>Detalle Paquete:</label></th>
                </tr><tr>  
                <th class="col-xs-2" style='text-align: left'>Código</th>
                <th class="col-xs-3" style='text-align: left'>Descripción</th> 
                <th class="col-xs-2" style='text-align: right'>Monto</th> 
                <th class="col-xs-2" style='text-align: right'>Derecho de trámite</th> 
                <th class="col-xs-2"  style='text-align: center' data-toggle="tooltip" data-placement="bottom" title="Semestres Académicos">Sem.</th>
                <th class="col-xs-2" style='text-align: center' data-toggle="tooltip" data-placement="bottom" title="Complementación Curricular">C.C.</th>
                </tr>
              </thead>
              <tbody>
                {foreach from = $saDatos item = i}
                  <tr>
                    <td style='text-align: left'>{$i['CIDCATE']}</td>
                    <td style='text-align: left'>{$i['CDESCRI']}</td>
                    <td style='text-align: right'>{number_format($i['NMONTO'],2,'.',',')}</td>
                    <td style='text-align: right'>{number_format($i['NCOSFOR'],2)}</td>
                    <td style='text-align: center'>
                      {if $i['CIDCATE'] == 'CCESTU'}
                        {$i['NCANSEM']}
                      {/if}
                    </td>
                    <td style='text-align: center'>
                      {if $i['CIDCATE'] == 'CCESTU' and $i['CCURCOM'] == 'S'}
                        <img src="Images/aprobado.png" width="23px" data-toggle="tooltip" data-placement="bottom" title="Cursos Complementarios">
                      {/if}
                    </td>
                  </tr>
                {/foreach}
              </tbody>
            </table>
            </div>
            <h1 align="right" class="panel-default">
              Total: S/.
              <label>{number_format($saData['NMONTO'], 2)}</label>
            </h1>
          </div>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <button type="submit" onclick="return confirm('¿Está seguro que desea generar deuda?')" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
        </div>
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
          <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
        </div>
      </div>
      {elseif $snBehavior == 2}
        <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-md-6">
            <div class="alert alert-success" align="center">
              <h1><strong>¡Deuda Provisional Generada!</strong><br>Su ID de pago es: <br><strong>{$saData['CNROPAG']}</strong><br></h1>
              <h4><strong>Indicar que el pago es por pensiones</strong></h4>
              <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
            </div>
            <div class="alert alert-success" align="left">
              <h4><u><strong>Tiempo Disponible en Bancos:</strong></u></h4> 
              <h4><strong>Para los días laborables:</strong></h4> 
              <h5>Si usted genera la deuda en la mañana antes de la <strong>1 p.m.</strong>, la deuda está disponible en los bancos a partir de las <strong>3 p.m.</strong>.<br> 
                  Si genera la deuda pasada la <strong>1 p.m.</strong> recién estará disponible al día siguiente.<br>
              <h4><strong>Para los días no laborables:</strong></h4> Si la deuda fue generada un dia no laborable, la misma estará disponible en los bancos a partir de las <strong>10 a.m.</strong> del primer día laborable.</h5>
            </div>
          </div>
          <div class="col-md-3">
            <div class="alert alert-warning" align="center">
              <h3>Una vez realizado el pago no olvides hacer el llenado del formulario en <strong>"LLENADO DE FORMULARIO"</strong></h3>
            </div>
          </div>
        </div>
      {/if}
</div>
</form>
</body>
</html>