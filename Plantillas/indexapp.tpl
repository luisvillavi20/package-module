<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css?version=1">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<body onload="f_Init()" class="divBody">


<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="indexapp.php" method="post">
   <div class="row">
   <div class="col-sm-3"></div>  
   <div class="col-sm-6">
   <div class="panel warning2 text-center" role="alert">
      <h4>Desde ahora debe ingresar con su DNI y contraseña del Sistema de Matrículas de la UCSM</h4>
   </div>
   <div class="panel panel-default">
   <div class="panel-heading">
      <h3 class="panel-title"><p class="text-justify"><b>INICIO DE SESIÓN </b></p></h3>
   </div>
   <div class="panel-body divPanel"> 
      <br><b>DNI</b>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input type="text" class="form-control btn-lg" maxlength="8" name="paData[CNRODNI]" placeholder="Nro DNI" title="Ingrese su número de DNI" minlength="8" required autofocus/> 
      </div>
      <br><b>Contraseña</b>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input type="password" class="form-control btn-lg" name="paData[CCLAVE]" required/>
      </div>
      <div align="center">
        <br><b>Captcha</b>      
        <span class="help-block">Resuelva la operación</span><img src="Captcha.php">
        <input type="text" class="form-control" name="pcCaptcha" placeholder="Ingrese su respuesta" tile="Resuelva la operación aritmética" required/>
        <br><button type="submit" name="Boton" value="Iniciar" class="center-block btn btn-success btn-lg btn-block">Iniciar Sesión <i class="glyphicon glyphicon-log-in"></i> </button>
      </div>    
      </div>      
      </div>
   </div>
   <div class="col-sm-3"></p>
   </div>      
   </div>
   <div class="row">
   <div class="col-sm-3"></div>
   <div class="col-sm-6"><div class="panel panel-default">
   <div class="panel-heading">
     <h3 class="panel-title"><p class="text-justify"><b>MANUAL DE USUARIO</b></p></h3>
   </div>   
   <div class="panel-body "> 
      <p class="text-center"><a class ="center-block btn btn-success btn-lg btn-block" href="./Certificados.pdf" download>DESCARGAR MANUAL - CERTIFICADOS</a>
   </div>
   <div class="panel-body "> 
      <p class="text-center"><a class ="center-block btn btn-success btn-lg btn-block" href="./Constancias.pdf" download>DESCARGAR MANUAL - CONSTANCIAS</a>
   </div>
   <div class="panel-body "> 
      <p class="text-center"><a class ="center-block btn btn-info btn-lg btn-block" href="invitados.php" target="_blank">SESIÓN DE INVITADOS</a>
   </div>
   </div>
   </div> 
   <div class="col-sm-3"></div>
   </div> 
</form> 
</div>
<div id="footer">
</div>
</body>
</html>