<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1040.php" method="post">
   <div class="container divBody">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-success">
               {if $snBehavior == 0}
               <div class="panel-heading"><h3 class="panel-title"><b>Datos del tramite</b></h3></div>   
               <div class="panel-body">
                  <div class="table-responsive mh-50">
                     <table class="table table-condensed"> 
                        <thead> 
                           <th style='text-align: center' class="col-xs-2">Fecha Recepción</th>
                           <th style='text-align: center'>Cod. Alumno</th>
                           <th style='text-align: center' class="col-xs-1">DNI</th>
                           <th style='text-align: center' class="col-xs-3">Nombre</th>
                           <th style='text-align: center' class="col-xs-2">Unidad Académica</th>
                           <th style='text-align: center' class="col-xs-2">Tipo de documento</th>
                           <!--<th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-download-alt"></i></th>-->
                           <th style='text-align: center' class="col-xs-1">Firma 1</th>
                           <th style='text-align: center' class="col-xs-1">Firma 2</th>
                           <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                        </thead>
                        {foreach from = $saDatos item = i}
                        <tr>
                           <td style='text-align: center'>{$i['DRECEPC']}</td>
                           <td style='text-align: center'>{$i['CCODALU']}</td>
                           <td style='text-align: center'>{$i['CNRODNI']}</td>
                           <td style='text-align: center'>{$i['CNOMBRE']}</td>
                           <td style='text-align: center'>{$i['CNOMUNI']}</td>
                           <td style='text-align: center'>{$i['CDESCRI']}</td>
                           <td style='text-align: center'>
                           {if $i['NCANFIR'] >= 1}
                           <img style="width:25%" src="Images/aprobado.png">
                           {/if}
                           </td>
                           <td style='text-align: center'>
                           {if $i['NCANFIR'] >= 2}
                           <img style="width:25%" src="Images/aprobado.png">
                           {/if}
                           </td>
                           <!--<td><a class="btn btn-info" href="DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE={$i['CCODTRE']}&download=true">Descargar</a></td>-->
                           <td style="text-align: center"><input type="radio" name="paData[CCODTRE]" value="{$i['CCODTRE']}" required></td>
                        </tr>
                        {/foreach}
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>                
      <div class="row">
        <div class="col-sm-4">
          <button type="submit" onclick="return confirm('¿Esta seguro que desea firmar el documento?')" name="Boton" value="Firmar" class="center-block btn btn-success btn-lg btn-block"> Firmar <i class="glyphicon glyphicon-ok"></i></button>
        </div>
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
          <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
        </div>
      </div>
      {elseif $snBehavior == 1}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE DOCUMENTOS PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <input type="hidden" name="paData[CCODTRE]" value="{$saDatos['CCODTRE']}">
         <table class="table table-condensed">
            <tr><th colspan="3" class="warning2">Datos del documento</th></tr>
            <tr>
               <th colspan="2">{$saDatos['CNOMBRE']} - {$saDatos['CCODALU']}</th>
               <th>Documento: {$saDatos['CDESCRI']}</th>
            </tr>
            <tr class="warning2">
               <th colspan="3">Ingresa detalle de trámite:</th>
            </tr><tr>
               <td colspan="3">
                  <table class="table table-condensed">
                     <tr>
                        <th>Conforme: </th>
                        <th colspan="2">
                           <input onchange="f_ConformeChange(this)" type="radio" name="paData[CRESULT]" value="S" checked/><img src="Images/aprobado.png" style="width: 30px; margin-left: 10px; margin-right: 10px">
                           <input onchange="f_ConformeChange(this)" type="radio" name="paData[CRESULT]" value="N"/><img src="Images/denegar.png" style="width: 30px; margin-left: 10px">
                        </th>
                        </tr>                        
                        <tr id="formAprobado_dvencim">
                            {if $saDatos['CIDCATE'] == 'CCCOND'}
                           <th>Fecha Vencimiento: </th>
                           <th colspan="2"><input type="date" class="form-control" name="paData[DVENCIM]" required/></th>
                           {/if}
                        </tr>
                        <tr id="formAprobado_ccodint">
                           <th>Codigo interno: </th>
                           <th colspan="2"><input type="text" class="form-control" maxlength="10" name="paData[CCODINT]" placeholder="Codigo interno" required/></th>
                        </tr><tr id="formDenegado" style="display: none">
                           <th>Observaciones: </th>
                           <th><textarea rows="5" class="form-control" name="paData[MOBSERV]" placeholder="Observaciones"></textarea></th>
                        </tr>
                  </table>
               </td>
            </tr>
         </table>
         <div class="row col-md-10 col-md-push-1">
            <div class="col-sm-4">
               <button id="visualizar" type="button" class="btn btn-info btn-block btn-lg" onclick="mostrarDocumento()">Visualizar en linea</button>
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <a class="btn btn-info btn-block btn-lg" href="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}&download=true">Descargar</a>
            </div>
         </div>
         <div class="row col-md-10 col-md-push-1">
            <iframe id="frameDocumento" style="width: 100%; height:80vh; display: none" src="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}"></iframe>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Paq1060.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 2}
      <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-md-6">
          <div class="alert alert-success" align="center">
            <h1><strong>¡Deuda Provisional Generada!</strong><br>Su ID de pago es: <br><strong>{$saData['CNROPAG']}</strong><br></h1>
            
            <h4><strong>Indicar que el pago es por pensiones</strong></h4>
            <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
          </div>
          <div class="alert alert-danger" align="left">
            <h4><u><strong>AVISO</strong></u></h4> 
            <h4><strong>El periodo de entrega para constancias y certificados solicitados es de hasta 7 días hábiles por Mesa de Partes.</strong></h4> 
          </div>
          <div class="alert alert-success" align="left">
            <h4><u><strong>Tiempo Disponible en Bancos:</strong></u></h4> 
            <h4><strong>Para los días laborables:</strong></h4> 
            <h5>Si usted genera la deuda en la mañana antes de la <strong>1 p.m.</strong>, la deuda está disponible en los bancos a partir de las <strong>3 p.m.</strong>.<br> 
                Si genera la deuda pasada la <strong>1 p.m.</strong> recién estará disponible al día siguiente.<br>
            <h4><strong>Para los días no laborables:</strong></h4> Si la deuda fue generada un dia no laborable, la misma estará disponible en los bancos a partir de las <strong>10 a.m.</strong> del primer día laborable.</h5>
          </div>
        </div>
        <div class="col-md-3">
          <div class="alert alert-warning" align="center">
            <h3>Una vez realizado el pago no olvides hacer el llenado del formulario en <strong>"LLENADO DE FORMULARIO"</strong></h3>
          </div>
        </div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer"></div>
</body>
</html>