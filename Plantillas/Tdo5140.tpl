<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>
   <script>
   function myFunctionObs(e){
      if( $("input[type='radio']").is(':checked')){
         $('#modalNew').modal('show');
      }
      else{
         window.alert('Seleccione una Solicitud para Observar');
      }
   }
   function myFuncApro(){
      if( $("input[type='radio']").is(':checked')){
         var apro;
         $('.Aprob:checked').each(function(indice, elemento){
            var fila = $(this).parents(".Datos");
            apro = fila.find(".IdCate").val();
            $('input[name="paData[CIDCATE]"]').val(apro);
         });
         return confirm('¿Seguro que desea Aprobar la Solicitud?');
      }
      else {
         alert('Seleccione una Solicitud para Aprobar');
         return false;
      }
   }
   function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
   }
   function f_FormularioChange(p_status) {   
      let formModalidad_cTipo = document.getElementById('formModalidad_cTipo');
      let formModalidad_cTipo2 = document.getElementById('formModalidad_cTipo2');
      let formModalidad_cTipo3 = document.getElementById('formModalidad_cTipo3');
      let formModalidad_cTipo4 = document.getElementById('formModalidad_cTipo4');
      if (p_status.value == 'Ordinario'){
         formModalidad_cTipo.style.display = "table-row";
         formModalidad_cTipo2.style.display = "none";
         formModalidad_cTipo3.style.display = "none";
         formModalidad_cTipo4.style.display = "none";
      }
      else if (p_status.value == 'Precatolica') {
         formModalidad_cTipo.style.display = "none";
         formModalidad_cTipo3.style.display = "none";
         formModalidad_cTipo4.style.display = "none";
         formModalidad_cTipo2.style.display = "table-row";
      } else if (p_status.value == 'Extraordinario I') {
         formModalidad_cTipo.style.display = "none";
         formModalidad_cTipo2.style.display = "none";
         formModalidad_cTipo3.style.display = "table-row";
         formModalidad_cTipo4.style.display = "none";
      } else if (p_status.value == 'Extraordinario II') {
         formModalidad_cTipo.style.display = "none";
         formModalidad_cTipo2.style.display = "none";
         formModalidad_cTipo3.style.display = "none";
         formModalidad_cTipo4.style.display = "table-row";
      }

   }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Tdo5140.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
         <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed">
               <thead>
                  <tr>
                     <th class="col-xs-2" style='text-align: left'>Fecha Recepción</th>
                     <th class="col-xs-1" style='text-align: left'>Cod. Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre Alumno</th>
                     <th class="col-xs-1" style='text-align: left'>Cod.Trámite</th>
                     <th class="col-xs-2" style='text-align: left'>Trámite</th>
                     <th class="col-xs-3" style='text-align: left'>Unidad Académica</th>
                     <th class="col-xs-1" style='text-align: left'>Estado</th>
                     <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                  </tr>
               </thead>
                  {foreach from = $saDatos item = i}
                  <tr onclick="f_seleccionarFila(this)" class = "Datos">
                     <td style='text-align: left'>{$i['TMODIFI']}</td>
                     <td style='text-align: left'>{$i['CCODALU']}</td>
                     <td style='text-align: left'>{$i['CNOMBRE']}</td>
                     <td style='text-align: left'>E-{$i['CCODTRE']}</td>
                     <td style='text-align: left'>{$i['CDESCRI']}</td>
                     <td style='text-align: left'>{$i['CNOMUNI']}</td>
                     <td style='text-align: left'>
                        {if $i['CESTADO']=='E'}   
                        <img src="Images/eye.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                        {else if $i['CESTADO']=='B'}
                        <img src="Images/aprobado.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Cerrado">
                        {else}
                        <img src="Images/clock.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                        {/if}
                     </td>
                     <td style='text-align: center' class="col-xs-1">            
                        <input class = "Aprob" type="radio" name="paData[CCODTRE]"  value="{$i['CCODTRE']}"required/>
                        <input class = "IdCate" type="hidden" value="{$i['CIDCATE']}"/>
                        <input type="hidden"  name = "paData[CIDCATE]" value="" />
                     </td>
                  </tr>
                  {/foreach}
            </table>
         </div>
      </div>
      </div>                                         
      <div class="row">
         <div class="col-xs-4">
            <button class="center-block btn btn-success btn-lg btn-block" onclick ="return myFuncApro()" name="Boton" value="Aprueba">Aprobar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-xs-4">
            <button type = "button" class="center-block btn btn-warning btn-lg btn-block" onclick ="myFunctionObs()">Observar&nbsp;&nbsp;<i class="glyphicon glyphicon-eye-open"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 1}
         <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
            <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
         </div>
         <div class="panel-body">
         <input type="hidden" name = "paData[CCODTRE]" value={$saData['CCODTRE']}>
            <div class="table-responsive mh-50">
               <table class="table table-condensed">   
                  <tr>
                  <td><div class="form-group">
                     <label for="icTramit">Nro Expediente / Fecha de Recepción</label>
                     <input type="text" class="form-control" value="E-{$saDatos['CCODTRE']} / {$saDatos['TMODIFI']}" readonly>
                  </div></td>
                  <td><div class="form-group">
                     <label for="icPago">Código Pago / Recibo de Pago</label>
                     <input type="text" class="form-control" id="icPago" value="{$saDatos['CNROPAG']} / {$saDatos['CRECIBO']}" readonly>
                  </div></td>
                  </tr>
                  <tr>
                  <td><div class="form-group">
                     <label for="icDescri">Tipo de Constancia</label>
                     <input type="text" class="form-control" id="icDescri" value="{$saDatos['CDESCRI']}" readonly>
                  </div></td>
                  <td><div class="form-group">
                     <label for="icNombre">Alumno: </label>
                     <input type="text" class="form-control" id="icNombre" value="{$saDatos['CNRODNI']} - {$saDatos['CNOMBRE']}" readonly>
                  </div></td>
                   </tr>
                  <tr>
                  <td><div class="form-group">
                     <label for="icEmail">E-mail y Celular: </label>
                     <input type="text" class="form-control" id="icEmail" value="{$saDatos['CEMAIL']} - {$saDatos['CNROCEL']}" readonly>
                  </div></td>
                  <td><div class="form-group">
                     <label for="icAlumno">Código Alumno - Unidad Académica: </label>
                     <input type="text" class="form-control" id="icAlumno" value="{$saDatos['CCODALU']} - {$saDatos['CNOMUNI']}" readonly>
                  </div></td>
                  </tr>
               </table>
               <table class="table table-condensed">   
                  <tr><th colspan="4" class="warning2">Datos requeridos para generar la constancia</th></tr>
                  <tr>
                  <td><label class="col-xs-6" style='text-align: left'>Especialidad:</label></td>
                  <td colspan="3"><input type="text" class="form-control" name="paData[CNOMUNI]" value="{$saDatos['CNOMUNI']}" readonly></td>
                  </tr>
                  <td><label class="col-xs-6" style='text-align: left'>Modalidad:</label></td>
                  <td colspan="3"><input type="text" class="form-control" name="paData[CMODALI]" placeholder="Ingrese la modalidad con la que llevo la especialidad, ejemplo: Semipresencial" maxlength="30" required='required' pattern="[A-Za-z]+"></td>
                  <tr>
                  <td><label class="col-xs-6" style='text-align: left'>Año de Admisión:</label></td>
                  <td colspan="3"><input type="text" class="form-control" name="paData[CADMISI]" placeholder="Ingrese el año del proceso de admisión, ejemplo: 2013" maxlength="4" required='required' pattern="[0-9]+"></td>
                  <tr>
                  <td><label class="col-xs-6" style='text-align: left'>Resolucion Nº</label></td>
                  <td><input type="text" class="form-control" name="paData[NNUMRES]" placeholder="Número de resolución, ejemplo: 18785"  required='required' pattern="[0-9]+"></td>
                  <td>   -R-</td>
                  <td><input type="text" class="form-control" name="paData[CANORES]" placeholder="Año de resolución, ejemplo: 2013 " required='required' maxlength="4" pattern="[0-9]+"></td>
                  </tr>
                  <tr>
                  <td><label class="col-xs-6" style='text-align: left'>Fecha de Admisión:</label></td>
                  <td colspan="4"><input type="date" class="form-control" name="paData[DFECHAR]" required='required'></td>
                  </tr>
               </table>
            </div>  
         </div>
         </div>                                         
         <div class="row">
            <div class="col-xs-4">
               <button class="center-block btn btn-success btn-lg btn-block" name="Boton" onclick="return confirm('¿Está seguro que desea enviar esta información?')" value="Aprueba">Aprobar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
            </div>
            <div class="col-xs-4">
            </div>
            <div class="col-sm-4">
               <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
            </div>
         </div>
      {/if}
      <div class="modal fade" id="modalNew" role="dialog">
   <div class="modal-dialog">
   <form action="Paq5100.php" method="POST">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">AGREGAR OBSERVACIÓN</h4>
         </div>
      <div class="modal-body">
      <table class="table table-condesed">
         <tr>
            <th>OBSERVACIÓN</th>
            <td><textarea style="text-transform:uppercase" id = "txtObser" class="form-control" rows = "4" name="paData[MOBSERV]" pattern="\d*"></textarea></td>
         </tr>
      </table>
      </div>
      <div class="modal-footer">
         <button class="btn btn-warning" name="Boton" value="Observar">Observar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
      </div>
   </form>
   </div>
   </div>
      </div>
      </div>
    </div>
  </div>
</form>
</div>
</body>
</html>