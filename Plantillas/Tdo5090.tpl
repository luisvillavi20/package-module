<html>   
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="Styles/js/bootstrap-select.js"></script>
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
   <script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
   <script>
      $(document).ready(function() {
         $('#LevantarSolicitud').on('shown.bs.modal', function (e) {
            let nSerial = $('input[name="nSerial"]:checked').val();
            if (nSerial == undefined) {
               $('#LevantarSolicitud').modal('hide');
               alert('Seleccione un curso para aprobar');
            } else {
               $('#LevantarSolicitud input[name="paData[NSERIAL]"]').val(nSerial);
               $('#LevantarSolicitud input[name="paData[NSERIAL1]"]').text(nSerial);
            }
         });
      });

      function f_seleccionarFila(e) {
               var a = e.cells[e.cells.length-1].children.item(0);
               a.checked = true;
      }

      $(document).ready(function() {
         $('#table').dataTable();
      });
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Solicitudes de Cursos por Jurado </b></h3></div>
<div class="container-fluid divBody">
<form action="Tdo5090.php" method="post">
   {if $snBehavior == 0}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES</b>
      <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <div class="table-responsive mh-50">
      <table id ="table" class="table table-condensed display">
         <thead>             
            <th style='text-align: left' class="col-xs-1">Cod. Alumno</th>
            <th style='text-align: left' class="col-xs-3">Nom. Alumno</th>
            <th style='text-align: left' class="col-xs-2">Uni. Académica</th>
            <th style='text-align: left' class="col-xs-3">Curso</th>
            <th style='text-align: center' class="col-xs-1">Fecha de Solicitud</th>  
            <th style='text-align: center' class="col-xs-1"></th>
         </thead>
         {foreach from = $saDatos item = i}  
            <tr>         
            <td style='text-align: left' class="col-xs-2">{$i['CCODALU']}</td>
            <td style='text-align: left' class="col-xs-2">{$i['CNOMBRE']}</td>
            <td style='text-align: left' class="col-xs-2">{$i['CNOMUNI']}</td>
            <td style='text-align: left' class="col-xs-2">{$i['CDESCRI']}</td>
            <td style='text-align: center' class="col-xs-2">{$i['DGENERA']}</td>
            <td style='text-align: center' class="col-xs-1">         
               <input type="radio" name="pcCidenti" value="{$i['CIDENTI']}"required>
            </td>
            </tr>
         {/foreach}
      </table>
      </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         <button type="submit" name="Boton" value="Revisar" class="center-block btn btn-primary btn-lg btn-block"> Revisar <i class="glyphicon glyphicon-forward"></i></button><br>
      </div>  
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
         <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div> 
   </div> 
   </div> 
   </div>
   {elseif $snBehavior == 1}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel-heading"><h3 class="panel-title"><b>DETALLE DE SOLICITUD </b></h3></div>
   <div class="panel-body">
   <input type="hidden" name="paData[CCODALU]" value="{$saDatos[0]['CCODALU']}">
   <input type="hidden" name="paData[CIDENTI]" value="{$saData['CIDENTI']}">
   <input type="hidden" name="cIdenti" value="{$saData['CIDENTI']}">
      <p class="text-muted"><b>ALUMNO:</b> {$saDatos[0]['CNOMBRE']}  - <b>COD. ALUMNO: </b> {$saDatos[0]['CCODALU']} - {$saDatos[0]['CNOMUNI']} </b></p>
      <h4><b>ESTADO DEL TRAMITE: </b>
      {if $saData['CESTADO'] == 'A'}
         <a class="label label-info"> ACTIVO </a>
      {elseif $saData['CESTADO'] == 'E'}  
         <a class="label label-info"> DECRETO ENTREGADO </a>
      {elseif $saData['CESTADO'] == 'O'}  
         <a class="label label-info"> OBSERVADO </a>
      {elseif $saData['CESTADO'] == 'X'}  
         <a class="label label-info"> ANULADO </a>
      {elseif $saData['CESTADO'] == 'S'}
         <a class="label label-info"> SOLICITADO </a>
      {else}
         <a class="label label-info"> PENDIENTE </a>     
      {/if}
      </h4>
      <div class="col-md-12 form-line">
      <table class="table table-condensed table-hover"> 
         <thead> 
            <th style='text-align: left' class="col-xs-1">Cod. Curso</th>
            <th style='text-align: left' class="col-xs-3">Nom. Curso</th>
            <th style='text-align: left' class="col-xs-4">Observación</th>
            <th style='text-align: center' class="col-xs-1">Estado Escuela</th>
            <th style='text-align: center' class="col-xs-1">Estado ORAA</th>               
         </thead>
         <tr>         
         <td style='text-align: left' class="col-xs-1">{$saData['CCODCUR']}</td>
         <td style='text-align: left' class="col-xs-4">{$saData['CDESCRI']}</td>
         <td style='text-align: left' class="col-xs-2">{$saData['MOBSERV']}</td>
         <td style='text-align: center' class="col-xs-1">
            {if $saData['CESTESC'] == 'A'}
               <a class="label label-success">APROBADO</a>
            {elseif $saData['CESTESC'] == 'D'}  
               <a class="label label-danger">DENEGADO</a>
            {elseif $saData['CESTESC'] == 'P'}  
               <a class="label label-default">PENDIENTE</a>
            {elseif $saData['CESTESC'] == 'V'}  
               <a class="label label-primary">VICERRECTORADO</a>
            {else}
               <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
            {/if}
         </td>
         <td style='text-align: center' class="col-xs-1">
            {if $saData['CESTSAC'] == 'A'}
               <a class="label label-success">APROBADO</a>
            {elseif $saData['CESTSAC'] == 'D'}  
               <a class="label label-danger">DENEGADO</a>
            {elseif $saData['CESTSAC'] == 'P'}  
               <a class="label label-default">PENDIENTE</a>
            {else}
               <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
            {/if}
         </td>
         </tr>
      </table>
      <table class="table table-condensed table-hover"> 
         <thead> 
            <th style='text-align: left' class="col-xs-3">Primer Jurado</th>
            <th style='text-align: left' class="col-xs-3">Segundo Jurado</th>            
         </thead>
         <tr>         
         <td style='text-align: left' class="col-xs-1">
         {if $saData['CJURAD1'] == '0000'}
            SIN ASIGNAR
         {else}
            {$saData['CNOMJU1']}
         {/if}
         </td>
         <td style='text-align: left' class="col-xs-4">
         {if $saData['CJURAD2'] == '0000'}
            SIN ASIGNAR
         {else}
            {$saData['CNOMJU2']}
         {/if}
         </td>
         </td>
         </tr>
      </table>
      </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-4">
         <!--<button type="button" data-toggle="modal" href="#LevantarSolicitud" class="center-block btn btn-success btn-lg btn-block"> Levantar Denegación <i class="glyphicon glyphicon-check"></i></button>-->
         <!--<input type="submit" name="Boton" value="Levantar" onclick="return confirm('¿Está seguro que desea Levantar la denegación de esta solicitud?')" class="center-block btn btn-warning btn-lg btn-block" />-->
      </div>  
      <div class="col-xs-4">
      </div>
      <div class="col-xs-4">
         <a href="Tdo5090.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div> 
   </div> 
   </div> 
   </div>
   
   {/if} 
   </div>
   </div> 
   </div>
</form>
<form action="Tdo5080.php" method="POST">
<div class="modal fade" id="LevantarSolicitud" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">APROBACIÓN DE CURSO</h4>
   </div>
   <div class="modal-body">
   <input type="hidden" name="paData[NSERIAL]">
      <table class="table">
         <tr><th>Observación</th>
         <td><textarea required="" rows="5" class="form-control" name="paData[MOBSERV]" placeholder="OPCIONAL (PARA ACOTACIONES ADICIONALES A LA APROBACION COMO INCISO DEL CURSO)" value="{$saDatos['MOBSERV']}" style="text-transform:uppercase resize:none"></textarea></td>
      </table>
   </div>
   <div class="modal-footer">
      <div class="row">
         <div class="col-sm-6">
            <input type="submit" name="Boton" value="Levantar" onclick="return confirm('¿Está seguro que desea Levantar la denegación de esta solicitud?')" class="center-block btn btn-warning btn-lg btn-block" />
         </div>
         <div class="col-sm-6">
            <button type="button" class="center-block btn btn-danger btn-lg btn-block"data-dismiss="modal">Cancelar</button>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</form>
</body>
</html>