<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      function f_ValidaNumericos(e) {
         if (e.which == 8) {
            return true;
         }
         if (e.key < '0' || e.key > '9') {
            return false;
         }
         return true;
      }      
      function f_ValidaPersona(e) {
         var lcNroDni = document.getElementById('text').value;
         if (lcNroDni.length == 7) {
            $.get("Paq1240.php?Id=Verificar&CNRODNI=" + lcNroDni + e.key, (data) => {
               let json = JSON.parse(data);
               console.log($('#CCODTRE'));
               $('#CCODTRE').val(json.CCODTRE);
               $('#CNRODNI').val(json.CNRODNI);
               if (json['ERROR'] != undefined) {
                  $('#cNombre').css("color", "#ce4646");
                  $('#cNombre').text(json.ERROR);
                  var lcRuta = "";
                  document.getElementById('lcRuta').href = lcRuta;
               } else if (json.CNRODNI == undefined || json.CCODTRE == undefined) {
                  $('#cNombre').css("color", "#333333");          
                  var lcRuta = "";
                  document.getElementById('lcRuta').href = lcRuta;     
               } else {
                  $('#cNombre').css("color", "#333333");
                  $('#cNombre').text(json.CNOMBRE);          
                  var lcRuta = "DocumentoPaquete.php?CNRODNI="+json.CNRODNI+"&CCODTRE="+json.CCODTRE+"&download=true";
                  
                  document.getElementById('lcRuta').href = lcRuta;
               }
            });
         }
      }

      function mostrarDocumento() {
         $('#frameDocumento').toggle();
         if ($('#frameDocumento').is(":visible")) {
            f_LoadPdf();
            $("#fileinfo").css("display", "block");
            $("#visualizar").text("Ocultar documento");
         }
         else {
            $(
               "#visualizar").text("Visualizar en linea");
            $("#fileinfo").css("display", "none");
         }
      }

      function salirDocumento() {
         $('input[name="paData[CCODINT]"]').removeAttr('required');
         return true;
      }

      function f_LoadPdf() {
         console.log("loadPdf");
         let lcNroDni = $('#CNRODNI').val();
         let lcCodTre = $('#CCODTRE').val();
         $("#frameDocumento").attr("src", "data:text/html;charset=utf-8," + escape("<center>Cargando...</center>"));
         $.get('DocumentoPaquete.php?json=true&CNRODNI=' + lcNroDni + "&CCODTRE=" + lcCodTre, function(data) {
            let loJson = JSON.parse(data);
            if (loJson["ext"] == "jpg") {
                let laSize = loJson.size;
                $("#fileinfo").html("Dimensiones: " + laSize['width'] + "px x " + laSize['height'] + "px");
            }
            $("#frameDocumento").attr("src", loJson["data"]);
         });
      }
      
      function mostrarDocumento2() {
         $('#frameDocumento').toggle();
         if ($('#frameDocumento').is(":visible")) {
            f_LoadPdf2();
            $("#fileinfo").css("display", "block");
            $("#visualizar").text("Ocultar documento");
         }
         else {
            $("#visualizar").text("Visualizar en linea");
            $("#fileinfo").css("display", "none");
         }
      }
      
      function salirDocumento2() {
         $('input[name="paData[CCODINT]"]').removeAttr('required');
         return true;
      }

      function f_LoadPdf2() {
         console.log("loadPdf");
         let lcNroDni = $('[name="paData[CNRODNI]"]').val();
         let lcCodTre = $('[name="paData[CCODTRE]"]').val();
         $("#frameDocumento").attr("src", "data:text/html;charset=utf-8," + escape("<center>Cargando...</center>"));
         $.get('DocumentoPaquete.php?json=true&CNRODNI=' + lcNroDni + "&CCODTRE=" + lcCodTre, function(data) {
            let loJson = JSON.parse(data);
            if (loJson["ext"] == "jpg") {
                let laSize = loJson.size;
                $("#fileinfo").html("Dimensiones: " + laSize['width'] + "px x " + laSize['height'] + "px");
            }
            $("#frameDocumento").attr("src", loJson["data"]);
         });
      }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1240.php" method="post">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE IMAGENES APROBADAS</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table">
            <table class="table table-condensed">   
               <tr class="warning2">
                  <th colspan="4"><label>Busqueda de Alumno - Datos del Alumno:</label></th>
                  <th></th>
               </tr><tr>
                  <th><label>DNI: </label></th>
                  <th><input type="text" id="text" class="form-control" name="paData[CNRODNI]" maxlength="8" placeholder="INGRESE DNI"
                       onkeydown="f_ValidaPersona(event);" onkeypress="return f_ValidaNumericos(event);"></th>
               </tr><tr>
                  <th><label>Nombres: </label></th>
                  <th><label id="cNombre"></label></th>
               </tr><tr>
            </table>
            <div class="row">
               <div class="col-sm-4">
                  <button id="visualizar" type="button" class="btn btn-info btn-block btn-lg" onclick="mostrarDocumento()">Visualizar en linea <i class="glyphicon glyphicon-eye-open"></i></button>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <a class="btn btn-info btn-block btn-lg" id = "lcRuta" href = "#">Descargar <i class="glyphicon glyphicon-download-alt"></i></a> 
               </div>
            </div>
         </div>
         <div class="table-responsive">
            <div class="row col-md-10 col-md-push-1">
            <input type="hidden" id="CCODTRE">
            <input type="hidden" id="CNRODNI">
               <div id="fileinfo" style="font-size: large"></div>
               <iframe id="frameDocumento" style="width: 100%; height:80vh; display: none; border: none; box-shadow: 0 0 4px -1px gray">
                  <center>Cargando...</center>
               </iframe>
            </div>
            <br>
            <table class="table table-condensed">
               <thead>
                  <tr class="warning2">
                     <th colspan="4">Lista de Aprobados:</th>
                     <th></th>
                  </tr><tr>
                     <th class="col-xs-2" style='text-align: left'>DNI</th>
                     <th class="col-xs-2" style='text-align: left'>Cod.Alumno</th>
                     <th class="col-xs-4" style='text-align: left'>Nombre</th>
                     <th class="col-xs-3" style='text-align: left'>Unidad Académica</th>
                     <th class="col-xs-1" style='text-align: center'>Seleccionar</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                  <tr>
                    <td style='text-align: left'>{$i['CNRODNI']}</td>
                    <td style='text-align: left'>{$i['CCODALU']}</td>
                    <td style='text-align: left'>{$i['CNOMBRE']}</td>
                    <td style='text-align: left'>{$i['CNOMUNI']}</td>   
                    <td style='text-align: center'><input type="radio" name="paData[CCODTRE]" value="{$i['CCODTRE']}" required/></td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="Activar" class="center-block btn btn-primary btn-lg btn-block"> Seguimiento <i class="glyphicon glyphicon-arrow-right"></i></button>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 2}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE DOCUMENTOS PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <input type="hidden" name="paData[CCODTRE]" value="{$saDatos['CCODTRE']}">
         <input type="hidden" name="paData[CIDDEUD]" value="{$saDatos['CIDDEUD']}">
         <table class="table table-condensed">
            <tr><th colspan="3" class="warning2">Datos del documento</th></tr>
            <tr>
               <th colspan="2">{$saDatos['CNOMBRE']} - {$saDatos['CCODALU']}</th>
               <th>Documento: {$saDatos['CDESCRI']}</th>
            </tr>
         </table>
         <div class="row col-md-10 col-md-push-1">
            <div class="col-sm-4">
               <input type="hidden" name="paData[CNRODNI]" value="{$saDatos['CNRODNI']}">
               <input type="hidden" name="paData[CCODTRE]" value="{$saDatos['CCODTRE']}">
               <button id="visualizar" type="button" class="btn btn-info btn-block btn-lg" onclick="mostrarDocumento2()">Visualizar en linea <i class="glyphicon glyphicon-eye-open"></i></button>
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <!--<a class="btn btn-info btn-block btn-lg" href="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}&download=true">Descargar</a>-->
               <a class="btn btn-info btn-block btn-lg" href="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}&download=true">Descargar <i class="glyphicon glyphicon-download-alt"></i></a>
            </div>
         </div>
         <div class="row col-md-10 col-md-push-1">
            <div id="fileinfo" style="font-size: large"></div>
            <iframe id="frameDocumento" style="width: 100%; height:80vh; display: none; border: none; box-shadow: 0 0 4px -1px gray">
               <center>Cargando...</center>
            </iframe>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4"> </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Paq1240.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer">
</div>
</body>
</html>