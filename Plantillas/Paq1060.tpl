<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      function mostrarDocumento() {
         $('#frameDocumento').toggle();
         if ($('#frameDocumento').is(":visible")) {
            f_LoadPdf();
            $("#fileinfo").css("display", "block");
            $("#visualizar").text("Ocultar documento");
         }
         else {
            $("#visualizar").text("Visualizar en linea");
            $("#fileinfo").css("display", "none");
         }
      }
      function f_ConformeChange2(p_status) {
         let formDenegado = document.getElementById('formDenegado2');
         let inputDenegado = formDenegado.querySelector('textarea');
         let formAprobado_ccodint = document.getElementById('formAprobado_ccodint2');
         let inputAprobado_ccodint = formAprobado_ccodint.querySelector('input');
         if (p_status.value == 'S'){
            //formAprobado_dvencim.style.display = "table-row";
            //inputAprobado_dvencim.setAttribute('required', 'true');
            formAprobado_ccodint.style.display = "table-row";
            inputAprobado_ccodint.setAttribute('required', 'true');
            formDenegado.style.display = "none";
            inputDenegado.removeAttribute('required');
         }
         else {
            //formAprobado_dvencim.style.display = "none";
            //inputAprobado_dvencim.removeAttribute('required');
            formAprobado_ccodint.style.display = "none";
            inputAprobado_ccodint.removeAttribute('required');
            formDenegado.style.display = "table-row";
            inputDenegado.setAttribute('required', 'true');
         }
      }
      function f_ConformeChange(p_status) {
         let formDenegado = document.getElementById('formDenegado');
         let inputDenegado = formDenegado.querySelector('textarea');
         let formAprobado_dvencim = document.getElementById('formAprobado_dvencim');
         let inputAprobado_dvencim = formAprobado_dvencim.querySelector('input');
         let formAprobado_ccodint = document.getElementById('formAprobado_ccodint');
         let inputAprobado_ccodint = formAprobado_ccodint.querySelector('input');
         if (p_status.value == 'S'){
            formAprobado_dvencim.style.display = "table-row";
            inputAprobado_dvencim.setAttribute('required', 'true');
            formAprobado_ccodint.style.display = "table-row";
            inputAprobado_ccodint.setAttribute('required', 'true');
            formDenegado.style.display = "none";
            inputDenegado.removeAttribute('required');
         }
         else {
            formAprobado_dvencim.style.display = "none";
            inputAprobado_dvencim.removeAttribute('required');
            formAprobado_ccodint.style.display = "none";
            inputAprobado_ccodint.removeAttribute('required');
            formDenegado.style.display = "table-row";
            inputDenegado.setAttribute('required', 'true');
         }
      }

      function salirDocumento() {
         $('input[name="paData[CCODINT]"]').removeAttr('required');
         return true;
      }

      function f_LoadPdf() {
         console.log("loadPdf");
         let lcNroDni = $('[name="paData[CNRODNI]"]').val();
         let lcCodTre = $('[name="paData[CCODTRE]"]').val();
         $("#frameDocumento").attr("src", "data:text/html;charset=utf-8," + escape("<center>Cargando...</center>"));
         $.get('DocumentoPaquete.php?json=true&CNRODNI=' + lcNroDni + "&CCODTRE=" + lcCodTre, function(data) {
            let loJson = JSON.parse(data);
            if (loJson["ext"] == "jpg") {
                let laSize = loJson.size;
                $("#fileinfo").html("Dimensiones: " + laSize['width'] + "px x " + laSize['height'] + "px");
            }
            $("#frameDocumento").attr("src", loJson["data"]);
         });
      }
      $(document).ready(function () {
         $('table tr').each(function(a,b){
            $(b).click(function(){
               $('table tr').css('background','#ffffff');
               $(this).css('background','#e6e6e6');   
            });
         });
      });
      function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
         //var a = e.cells[e.cells.length-1].children.item(0);
         //a.checked = true;
      }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1060.php" method="post" enctype="multipart/form-data">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed table-hover">
               <thead>
                  <tr>
                     <th class="col-xs-2" style='text-align: left'>DNI</th>
                     <th class="col-xs-2" style='text-align: left'>Cod.Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre</th>
                     <th class="col-xs-1" style='text-align: center'>Activar</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                  <tr onclick="f_seleccionarFila(this)">
                    <td style='text-align: left'>{$i['CNRODNI']}</td>
                    <td style='text-align: left'>{$i['CCODALU']}</td>
                    <td style='text-align: left'>{$i['CNOMBRE']}</td>
                    <td style='text-align: center'><input type="radio" name="paData[CIDDEUD]" value="{$i['CIDDEUD']}" required/></td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="DetalleAlumno" class="center-block btn btn-success btn-lg btn-block"> Activar <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 1}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed table-hover">
               <thead>
                  <tr onclick="f_seleccionarFila(this)">
                     <th class="col-xs-2" style='text-align: center'>Fecha Recepción</th>
                     <th class="col-xs-3" style='text-align: left'>Tip.Documento</th>
                     <th class="col-xs-2" style='text-align: left'>DNI</th>
                     <th class="col-xs-2" style='text-align: left'>Cod.Alumno</th>
                     <th class="col-xs-3" style='text-align: left'>Nombre</th>
                     <th class="col-xs-1" style='text-align: center'>Activar</th>
                     <th class="col-xs-1" style='text-align: center'>Estado</th>
                  </tr>
               </thead>
               <tbody id="tramitesPendientes">
                  {foreach from = $saDatos item = i}
                  <input type="hidden" name="paData[CNRODNI]" value="{$i['CNRODNI']}">
                  <input type="hidden" name="paData[CNOMBRE]" value="{$i['CNOMBRE']}">
                  <tr onclick="f_seleccionarFila(this)">
                    <td style='text-align: center'>{$i['DRECEPC']}</td>
                    <td style='text-align: left'>{$i['CDESCRI']}</td>
                    <td style='text-align: left'>{$i['CNRODNI']}</td>
                    <td style='text-align: left'>{$i['CCODALU']}</td>
                    <td style='text-align: left'>{$i['CNOMBRE']}</td>
                    <td style='text-align: center'><input type="radio" name="paData[CCODTRE]"  value="{$i['CCODTRE']}" {if $i['CESTMTR'] == 'B' OR $i['CESTMTR'] == 'F'} disabled {/if}/></td>
                    <td style='text-align: center'>
                        {if $i['CESTMTR']=='E'}   
                        <img src="Images/eye.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                        {else if $i['CESTMTR']=='B'}
                        <img src="Images/aprobado.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Cerrado">
                        {else}
                        <img src="Images/clock.png" width="40%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                        {/if}
                     </td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div id="Activar" class="col-sm-4">
            <button type="submit" name="Boton" value="Activar" class="center-block btn btn-success btn-lg btn-block"> Activar <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <button class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</button>
         </div>
      </div>
      {elseif $snBehavior == 2}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE DOCUMENTOS PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <input type="hidden" name="paData[CCODTRE]" value="{$saDatos['CCODTRE']}">
         <input type="hidden" name="paData[CIDDEUD]" value="{$saDatos['CIDDEUD']}">
         <table class="table table-condensed">
            <tr><th colspan="3" class="warning2">Datos del documento</th></tr>
            <tr>
               <th colspan="2">{$saDatos['CNOMBRE']} - {$saDatos['CCODALU']}</th>
               <th>Documento: {$saDatos['CDESCRI']}</th>
            </tr>
            <tr class="warning2">
               <th colspan="3">Ingresa detalle de trámite:</th>
            </tr><tr>
               <td colspan="3">
                  <table class="table table-condensed">
                     <tr>
                        <th>Conforme: </th>
                        <th colspan="2">
                           <input onchange="f_ConformeChange2(this)" type="radio" name="paData[CRESULT]" value="S" checked/><img src="Images/aprobado.png" style="width: 30px; margin-left: 10px; margin-right: 10px">
                           <input onchange="f_ConformeChange2(this)" type="radio" name="paData[CRESULT]" value="N"/><img src="Images/denegar.png" style="width: 30px; margin-left: 10px">
                        </th>
                        </tr>                        
                        <tr id="formAprobado_dvencim2">
                            {if $saDatos['CIDCATE'] == 'CCCOND'}
                           <th>Fecha Vencimiento: </th>
                           <th colspan="2"><input type="date" class="form-control" name="paData[DVENCIM]" required/></th>
                           {/if}
                        </tr>
                        <tr id="formAprobado_ccodint2">
                           <th>Codigo interno: </th>
                           <th colspan="2"><input type="text" class="form-control" maxlength="10" name="paData[CCODINT]" placeholder="Codigo interno" required/></th>
                        </tr><tr id="formDenegado2" style="display: none">
                           <th>Observaciones: </th>
                           <th><textarea rows="5" class="form-control" name="paData[MOBSERV]" placeholder="Observaciones"></textarea></th>
                        </tr>
                  </table>
               </td>
            </tr>
         </table>
         <div class="row col-md-10 col-md-push-1">
            <div class="col-sm-4">
               <input type="hidden" name="paData[CNRODNI]" value="{$saDatos['CNRODNI']}">
               <input type="hidden" name="paData[CCODTRE]" value="{$saDatos['CCODTRE']}">
               <button id="visualizar" type="button" class="btn btn-info btn-block btn-lg" onclick="mostrarDocumento()">Visualizar en linea <i class="glyphicon glyphicon-eye-open"></i></button>
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <a class="btn btn-info btn-block btn-lg" href="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}&download=true">Descargar <i class="glyphicon glyphicon-download-alt"></i></a>
            </div>
         </div>
         <div class="row col-md-10 col-md-push-1">
            <div id="fileinfo" style="font-size: large"></div>
            <iframe id="frameDocumento" style="width: 100%; height:80vh; display: none; border: none; box-shadow: 0 0 4px -1px gray">
               <center>Cargando...</center>
            </iframe>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="Grabar" onclick="return confirm('¿Está seguro de realizar esta operación?')" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <button name="Boton" value="DetalleAlumno" onclick="return salirDocumento()" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 3}
      <div class="panel-heading"><h3 class="panel-title"><b>AÑADIR DATOS AL TRÁMITE</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body" > 
         <div class="col-md-12 form-line">  
            <div class="form-group">
               <input type="hidden" name="paData[CCODTRE]" value="{$saDatos['CCODTRE']}">
               <input type="hidden" name="paData[CIDDEUD]" value="{$saDatos['CIDDEUD']}">
               <table class="table table-condensed">   
                    <tr class="warning2">
                        <th colspan="3">Detalle del Trámite:</th>
                    </tr><tr>
                        <th><label>Nro. de Trámite: </label></th>
                        <th colspan="2"><label>E-{$saDatos['CCODTRE']}</label></th>
                    </tr><tr>
                        <th><label>Descripción: </label></th>
                        <th colspan="2"><label>{$saDatos['CDESDOC']}</label></th>
                    </tr>
                    <tr class="warning2">
                        <th width= '30%'><label>Datos del Alumno:</label></th>
                        <th></th>
                    </tr><tr>
                        <th><label>DNI: </label></th>
                        <th><label>{$saDatos['CNRODNI']}</label></th>
                    </tr><tr>
                        <th><label>Nombres: </label></th>
                        <th><label>{$saDatos['CNOMBRE']}</label></th>
                    </tr><tr>
                        <th><label>Código: </label></th>
                        <th><label>{$saDatos['CCODALU']}</label></th>
                    </tr>
                    <tr class="warning2">
                        <th>Ingreso detalle de trámite:</th>
                        <th></th>
                    </tr><tr>
                        <th>Conforme: </th>
                        <th>
                          <input onchange="f_ConformeChange(this)" type="radio" name="paData[CRESULT]" value="S" checked/><img src="Images/aprobado.png" style="width: 30px; margin-left: 10px; margin-right: 10px">
                          <input onchange="f_ConformeChange(this)" type="radio" name="paData[CRESULT]" value="N"/><img src="Images/denegar.png" style="width: 30px; margin-left: 10px">
                        </th>
                    </tr><tr id="formAprobado_dvencim">
                      <th>Fecha Vencimiento: </th>
                      <th><input type="date" class="form-control" name="paData[DVENCIM]" required/></th>
                    </tr><tr id="formAprobado_ccodint">
                      <th>Codigo interno: </th>
                      <th><input type="text" class="form-control" maxlength="10" name="paData[CCODINT]" placeholder="Codigo interno" required/></th>
                    </tr>
                    <tr id="formDenegado" style="display: none">
                        <th>Observaciones: </th>
                        <th>
                           <textarea rows="5" class="form-control" name="paData[MOBSERV]" placeholder="Observaciones"></textarea>
                        </th>
                    </tr>
                  </table>
              </div>
            </div>
        </div>
      </div>
      <div class="row">
          <div class="col-sm-4">
            <button type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
          </div>
          <div class="col-sm-4">
          </div>
          <div class="col-sm-4">
            <button type="submit" name="Boton" value="Aplicar" class="center-block btn btn-success btn-lg btn-block"> Salir <i class="glyphicon glyphicon-ok"></i></button>
          </div>
      </div>
      {elseif $snBehavior == 4}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <!--<b>ENCARGADO: </b>  {$saData['CNOMBRE']}<br>-->
         <div class="table-responsive mh-50">
            <table class="table table-condensed table-hover">
               <input type="hidden" name="paData[CCODTRE]" value="{$saData['CCODTRE']}">
               <input type="hidden" name="paData[CNRODNI]" value="{$saData['CNRODNI']}">
               <input type="hidden" name="paData[CNOMBRE]" value="{$saData['CNOMBRE']}">
               <tr>
               <td>Título Profesional:</td>
               <td colspan="4"><input type="text" style="text-transform:uppercase" class="form-control" name="paData[CTITPRO]" placeholder="Ingrese el Titulo que obtendra el alumno" required='required'></td>
               </tr>
               <tr>
               <td><label style='text-align: left'>Título de Tesis:</label></td>
               <td colspan="4"><input type="text" class="form-control" name="paData[CTITTES]" placeholder="Ingrese el Titulo del Trabajo de Investigacion" required='required'></td>
               </tr>
               <tr>
               <td>Modalidad:</td>
               <td colspan="4"><select class="selectpicker form-control form-control-sm col-12" name="paData[CMODALI]" data-live-search="true" required>
                     <option value="Unaminidad">Unaminidad</option>
                     <option value="Mayoria">Mayoria</option>
                     <option value="Unaminidad con Felicitación Pública">Unaminidad con Felicitación Pública</option>
                   </select></td>
               </tr>
               <tr>
               <td>Consta en:</td>
               <td>Tomo:</td><td><input style="text-transform:uppercase" class="form-control" type="text" name="paData[CNROTOM]" placeholder="Ingrese Tomo, ejem: XIIV" required='required'></td>
               <td>Nº Folio:</td><td><input type="number" class="form-control" name="paData[NROFOLI]" placeholder="Ingrese Folio, ejem: 32" required='required'></td>
               </tr>
               <tr>
               <td>Fecha de Sustentación:</td>
               <td colspan="4"><input type="date" class="form-control" name="paData[DFECSUS]" required='required'></td>
               </tr>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="Reporte" class="center-block btn btn-success btn-lg btn-block"> Reporte <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 4}
      <div class="table-responsive">
         <table class="table table-condensed"> 
            <thead>
               <tr> 
                  <th class="col-xs-4">Tipo de Documento</th>  
                  <th class="col-xs-8">Seleccionar</th>  
                  <th class="col-xs-1">Subir</th>  
               </tr>
            </thead>
            <tbody>
               <form action="Paq1490.php" method="POST" enctype="multipart/form-data">
                  <tr class="bg-success">
                     <td class = "col-xs-4">{$saDatos['CDESCRI']}</td>
                     <td class="col-xs-8">
                     <input disabled type="file" name="fAutenticacion" onchange="fileValidator(this);"/>
                     </td>
                     <td><button disabled type="submit" class="btn btn-default btn-block btn-xs" onclick="return confirm('¿Seguro que desea subir este archivo?')" name="Boton" value="SubirDocumento" >
	      		   	<img src="Images/aprobado.png" width="20" height="20"></button></td>  
               </form>
            </tbody>
         </table>
         </div>  
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer">
</div>
</body>
</html>