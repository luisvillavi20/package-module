<html>   
<head>
<title>Trámites</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="Styles/css/bootstrap.css">
<link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
<link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
<link rel="stylesheet" href="CSS/style.css">
<script src="Styles/js/bootstrap-select.js"></script>
<script src="js/java.js"></script>
<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
<script src="Styles/js/jquery-3.2.0.js"></script>
<script src="Styles/js/bootstrap.js"></script>
<script type="text/javascript" src="CSS/jquery.modal.js"></script>
<script>
   $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
   });
</script>
<script>
   $(document).ready(function() {
      $('#AprobarSolicitud').on('shown.bs.modal', function (e) {
         let nSerial = $('input[name="nSerial"]:checked').val();
         if (nSerial == undefined) {
            $('#AprobarSolicitud').modal('hide');
            alert('Seleccione un curso para aprobar');
         } else {
            $('#AprobarSolicitud input[name="paData[NSERIAL]"]').val(nSerial);
            $('#AprobarSolicitud input[name="paData[NSERIAL1]"]').text(nSerial);
         }
      });
   });
   $(document).ready(function() {
      $('#PendienteSolicitud').on('shown.bs.modal', function (e) {
         let nSerial = $('input[name="nSerial"]:checked').val();
         if (nSerial == undefined) {
            $('#PendienteSolicitud').modal('hide');
            alert('Seleccione un curso');
         } else {
            $('#PendienteSolicitud input[name="paData[NSERIAL]"]').val(nSerial);
            $('#PendienteSolicitud input[name="paData[NSERIAL1]"]').text(nSerial);
         }
      });
   });
   $(document).ready(function() {
      $('#DenegarSolicitud').on('shown.bs.modal', function (e) {
         let nSerial = $('input[name="nSerial"]:checked').val();
         if (nSerial == undefined) {
            $('#DenegarSolicitud').modal('hide');
            alert('Seleccione un curso para denegar');
         } else {
            $('#DenegarSolicitud input[name="paData[NSERIAL]"]').val(nSerial);
            $('#DenegarSolicitud input[name="paData[NSERIAL1]"]').text(nSerial);
         }
      });
   });

   function f_seleccionarFila(e) {
      var a = e.cells[e.cells.length-1].children.item(0);
      a.checked = true;
   }
</script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Solicitudes de Cursos por Jurado </b></h3></div>
<div class="container-fluid divBody">
<form action="Tdo5030.php" method="post">
   {if $snBehavior == 0}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES</b>
      <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <div class="table-responsive mh-50">
      <table class="table table-condensed table-hover">
         <thead>             
            <th style='text-align: left' class="col-xs-2">Cod. Alumno</th>
            <th style='text-align: left' class="col-xs-4">Nom. Alumno</th>
            <th style='text-align: left' class="col-xs-3">Uni. Académica</th>
            <th style='text-align: center' class="col-xs-1">Fecha de Solicitud</th>  
            <th style='text-align: center' class="col-xs-1">Estado</th>       
            <th style='text-align: center' class="col-xs-1"></th>
         </thead>
         {foreach from = $saDatos item = i}  
            <tr onclick="f_seleccionarFila(this)">
            <td style='text-align: left' class="col-xs-2">{$i['CCODALU']}</td>
            <td style='text-align: left' class="col-xs-2">{$i['CNOMBRE']}</td>
            <td style='text-align: left' class="col-xs-2">{$i['CUNIACA']}</td>
            <td style='text-align: center' class="col-xs-2">{$i['DGENERA']}</td>
            <td style='text-align: center' class="col-xs-1">
               {if $i['CESTADO'] == 'A'}
                  <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
               {elseif $i['CESTADO'] == 'D'}  
                  <img src="Images/subir.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Denegado">
               {elseif $i['CESTADO'] == 'P'}  
                  <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               {else}
                  <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               {/if}
            </td>
            <td style='text-align: center' class="col-xs-1">         
               <input type="radio" name="pcCidenti" value="{$i['CIDENTI']}"required>
            </td>
            </tr>
         {/foreach}
      </table>
      </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         <button type="submit" name="Boton" value="Revisar" class="center-block btn btn-primary btn-lg btn-block"> Revisar <i class="glyphicon glyphicon-forward"></i></button><br>
      </div>  
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
         <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div> 
   </div> 
   </div> 
   </div>
   {elseif $snBehavior == 1}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel-heading"><h3 class="panel-title"><b>DETALLE DE SOLICITUD </b></h3></div>
   <div class="panel-body">
   <input type="hidden" name="paData[CCODALU]" value="{$saDatos[0]['CCODALU']}">
   <input type="hidden" name="paData[CIDENTI]" value="{$saData['CIDENTI']}">
   <input type="hidden" name="cIdenti" value="{$saData['CIDENTI']}">
      <p class="text-muted"><b>ALUMNO:</b> {$saDatos[0]['CNOMBRE']}  - <b>NRO. DNI: </b> {$saDatos[0]['CNRODNI']} - <b>COD. ALUMNO: </b> {$saDatos[0]['CCODALU']} - {$saDatos[0]['CNOMUNI']}</p>
      <p class="text-muted"><b>EXPEDIENTE: </b>ERP-CPJU<b>{$saData['CIDENTI']}</p>
      <div class="col-md-12 form-line">
      <table class="table table-condensed table-hover"> 
         <thead class="warning2"> 
            <th style='text-align: left' class="col-xs-1">Cod. Curso</th>
            <th style='text-align: left' class="col-xs-4">Nom. Curso</th>
            <th style='text-align: left' class="col-xs-2">Plan Estudios</th>
            <th style='text-align: center' class="col-xs-1" data-toggle="right" title="Créditos Teoría">Cred.Teoría</th>
            <th style='text-align: center' class="col-xs-1" data-toggle="right" title="Créditos Practicas">Cred.Prácticas</th>
            <th style='text-align: center' class="col-xs-1">Estado</th>               
            <th style='text-align: center' class="col-xs-1">Seleccionar</th>               
         </thead>
         {foreach from = $saDatos item = i}  
            <tr onclick="f_seleccionarFila(this)">
            <td style='text-align: left' class="col-xs-1">{$i['CCODCUR']}</td>
            <td style='text-align: left' class="col-xs-4">{$i['CDESCRI']}</td>
            <td style='text-align: left' class="col-xs-2">{$i['CPLAEST']}</td>
            <td style='text-align: center' class="col-xs-1">{$i['NCRETEO']}</td>
            <td style='text-align: center' class="col-xs-1">{$i['NCRELAB']}</td>
            <td style='text-align: center' class="col-xs-1">
               {if $i['CESTESC'] == 'A'}
                  <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
               {elseif $i['CESTESC'] == 'D'}  
                  <img src="Images/denegar.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Denegado">
               {elseif $i['CESTESC'] == 'P'}  
                  <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               {else}
                  <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               {/if}
            </td>
            <td style='text-align: center' class="col-xs-1">         
            <input type="radio" name="nSerial" value="{$i['NSERIAL']}"required>
            </td> 
            </tr>
         {/foreach}
      </table>
      </div>
      </div>
      {if $saDatos[0]['CESTADO'] == 'O'} 
         <div id="error" class="alert alert-danger" role="alert">
            Especificaciones
            <li> El alumno con esta solicitud esta excediendo los 11 créditos en el presente periodo.</li>
            <li> Se deberá pedir autorización al Vicerrectorado Académico si es que se desea emitir un decreto de esta solicitud. </li>
            <li> Nº de expediente: <b>ERP-CPJU<b>{$saData['CIDENTI']}.</b> </li> 
         </div>
      {/if}
   </div>
   <div class="row">
      <div class="col-xs-3">
         <button type="button" data-toggle="modal" href="#AprobarSolicitud" class="center-block btn btn-success btn-lg btn-block"> Aprobar <i class="glyphicon glyphicon-check"></i></button>
      </div>
      <div class="col-xs-3">
         <button type="button" data-toggle="modal" href="#PendienteSolicitud" class="btn btn-block btn-primary btn-lg">Enviar<i class="glyphicon glyphicon-check"></i></button>
      </div>  
      <div class="col-xs-3">
         <button type="button" data-toggle="modal" href="#DenegarSolicitud" class="btn btn-block btn-danger btn-lg">Denegar Curso <i class="glyphicon glyphicon-remove"></i></button>
      </div>
      <div class="col-xs-3">
         <a href="Tdo5030.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div> 
   </div> 
   </div> 
   </div>
   
   {/if} 
   </div>
   </div> 
   </div>
</form>
<form action="Tdo5030.php" method="POST">
<div class="modal fade" id="AprobarSolicitud" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">APROBACIÓN DE CURSO</h4>
   </div>
   <div class="modal-body">
   <input type="hidden" name="paData[NSERIAL]">
      <table class="table">
         <tr><th>Inciso</th>
         <td><input class="form-control" name="paData[CINCISO]" required="" maxlength="1" style="text-transform:uppercase" placeholder ="EJEMPLO: A" ></td>
         <tr><th>Observación</th>
         <td><textarea required="" rows="5" class="form-control" name="paData[MOBSERV]" style="text-transform:uppercase" placeholder="OPCIONAL (PARA ACOTACIONES ADICIONALES A LA APROBACION COMO INCISO DEL CURSO)" value="{$saDatos['MOBSERV']}" style="text-transform:uppercase resize:none"></textarea></td>
      </table>
   </div>
   <div class="modal-footer">
      <div class="row">
         <div class="col-sm-6">
            <input type="submit" name="Boton" value="Aprobar" onclick="return confirm('¿Está seguro que desea Aprobar esta solicitud?')" class="center-block btn btn-warning btn-lg btn-block" />
         </div>
         <div class="col-sm-6">
            <button type="button" class="center-block btn btn-danger btn-lg btn-block"data-dismiss="modal">Cancelar</button>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</form>
<form action="Tdo5030.php" method="POST">
<div class="modal fade" id="PendienteSolicitud" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">OBSERVACION DE CURSO COMPLEMENTARIO</h4>
   </div>
   <div class="modal-body">
   <input type="hidden" name="paData[NSERIAL]">
      <table class="table" id="table_edit_documents">
         <tr><th>Observación</th>
         <td><textarea rows="5" class="form-control" value="afasdfsaf" style="text-transform:uppercase resize:none"readonly>Esta opción es solo para Cursos Complementarios que se llevarán por jurado, emita un documento al Vicerrectorado Académico con el número de expediente:
EXPEDIENTE: ERP-CPJU{$saData['CIDENTI']}.
Adjunte también la ficha académica del alumno.</textarea></td>
      </table>
   </div>
   <div class="modal-footer">
      <div class="row">
         <div class="col-sm-6">
            <input type="submit" name="Boton" value="Enviar" onclick="return confirm('¿Está seguro que desea Enviar esta solicitud?')" class="center-block btn btn-warning btn-lg btn-block" />
         </div>
         <div class="col-sm-6">
            <button type="button" class="center-block btn btn-danger btn-lg btn-block"data-dismiss="modal">Cancelar</button>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</form>
<form action="Tdo5030.php" method="POST">
<div class="modal fade" id="DenegarSolicitud" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">DENEGACION DE CURSO</h4>
   </div>
   <div class="modal-body">
   <input type="hidden" name="paData[NSERIAL]">
      <table class="table" id="table_edit_documents">
         <tr><th>Observación</th>
         <td><textarea required="" rows="5" class="form-control" name="paData[MOBSERV]" placeholder="Observaciones" style="text-transform:uppercase resize:none"></textarea></td>
      </table>
   </div>
   <div class="modal-footer">
      <div class="row">
         <div class="col-sm-6">
            <input type="submit" name="Boton" value="Denegar" onclick="return confirm('¿Está seguro que desea Denegar esta solicitud?')" class="center-block btn btn-warning btn-lg btn-block" />
         </div>
         <div class="col-sm-6">
            <button type="button" class="center-block btn btn-danger btn-lg btn-block"data-dismiss="modal">Cancelar</button>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</form>
</body>
</html>