<html>
<head>
   <title>Trámites Administrativos</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      $(document).ready(function(){
         //$("#myModal").modal('show');
      });

      var paCodCur = {};
      var pnTotal  = 6.00;

      function f_agregarCurso() {
         let loCur = document.querySelector('[name="paData[CIDCATE]"]');
         let loOpt = loCur.options[loCur.selectedIndex];
         let lcDescri = loOpt.getAttribute('cDescri');
         let lnCredit = loOpt.getAttribute('cIdCate');
         let lnMonto  = parseFloat(loOpt.getAttribute('nMonto'));
         let lcCodCur = loOpt.value;
         
         let t_cursos = document.getElementById('t_cursos');

         if (paCodCur[lcCodCur]) {
            alert('Documento ya seleccionado.');
            return;
         }

         if (t_cursos.childElementCount >= 2) {
            alert('Solo se admiten hasta 2 Documento.');
            return;
         }

         let loFila = document.createElement("tr");

         let loDescri = document.createElement("td");
         let loCodCur = document.createElement("input");
         loCodCur.type = "hidden";
         loCodCur.name = "paData[CIDCATE][]";
         loCodCur.value = lcCodCur;
         loDescri.appendChild(loCodCur);
         loDescri.innerHTML += lcDescri;
         loFila.appendChild(loDescri);


         let loMonto = document.createElement("td");
         loMonto.innerHTML = "S/." + lnMonto;
         loFila.appendChild(loMonto);

         let loBorrar = document.createElement("td");
         loBorrar.style.textAlign = "center";
         let loBtnBor = document.createElement("button");
         loBtnBor.className = "btn btn-danger";
         loBtnBor.textContent = "X";
         loBtnBor.onclick = function() {
            t_cursos.removeChild(loFila);
            pnTotal -= paCodCur[lcCodCur];
            $("#r_total").html("Total: S/." + pnTotal.toFixed(2));
            delete paCodCur[lcCodCur];
         };
         loBorrar.appendChild(loBtnBor);
         loFila.appendChild(loBorrar);
         
         t_cursos.append(loFila);
         pnTotal += lnMonto;
         $("#r_total").html("Total: S/." + pnTotal.toFixed(2));
         paCodCur[lcCodCur] = lnMonto;
      }

      function f_changeCurso(self) {
         let loOpt = self.options[self.selectedIndex];
         let nCredit = loOpt.getAttribute('nCredit');
         $("#nNumCre").html(nCredit);
         $("#nCosTot").html("S/." + (nCredit * nCosCre));
      }
      
      function f_init() {         
         MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
         var observer = new MutationObserver(function(mutations, observer) {
            // fired when a mutation occurs
            if (mutations.length == 1) {
               alert('Se modificó un valor del sistema, éste caso será reportado');
               console.log(mutations, observer);
            }
         });
         observer.observe(document, {
            subtree: true,
            attributeFilter: ['ncredit', 'value']
         });
      }
   </script>
</head>
<body class="divBody" onload="f_init()">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80"> Trámites Administrativos</b></h3></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-md-push-2">
<form action="Paq1470.php" method="POST">
   <div class="panel panel-success">
   {if $snBehavior == 0}
      <div class="panel-heading">
         <h3 class="panel-title"><b>AUTENTICACIÓN DE DOCUMENTO PARA TITULACION ONLINE</b></h3>
      </div>
      <div class="table-responsive">
            <table class="table table-condensed">
              <tr>
              <th><label> Código Alumno:</label></th>
              <th colspan="1"><label>{$saData['CCODALU']}</label></th>
              <th><label></label></th>
              <th colspan="1"><label>{$saData['CNOMBRE']}</label></th>
              <th><label>Escuela:</label></th>
              <th colspan="2"><label>{$saData['CUNIACA']}</label></th>
              <th><label></label></th>
              <th colspan="2"><label>{$saData['CNOMUNI']}</label></th>
              </tr><tr>
              <th>TELÉFONO:</th>
              <td colspan="8"><input type="text" class="form-control btn-lg" maxlength="9" name="paData[CNROCEL]" value="{$saData['CNROCEL']}" placeholder="Número Telefónico" title="Ingrese su Numero Telefónico" minlength="9" required autofocus/></td>
              </tr><tr>
              <th>CORREO:</th>
              <td colspan="8"><input type="text" class="form-control btn-lg" maxlength="50" name="paData[CEMAIL]" value="{$saData['CEMAIL']}" placeholder="Correo Electrónico" title="Ingrese su Correo Electrónico" required/></td>
              </tr><tr>
              <td colspan="9"><label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Los datos personales solicitados, serán utilizados para contactarlos en caso de algun inconveniente.</label></td>
              </tr>
            </table>
      </div>
      <div class="panel-heading">
         <h3 class="panel-title"><b>DOCUMENTO PARA TITULACION ONLINE</b></h3>
      </div>
      <div class="panel-body">
         <table class="table">
            <tr>
            <th>Alumno</th>
            <td>{$saData['CNRODNI']} - {$saData['CCODALU']} - {$saData['CNOMBRE']}</td>
            </tr><tr>
            <th>Documentos</th>
            <td>
               <select class="form-control selectpicker" name="paData[CIDCATE]" data-live-search="true" onchange="f_changeCurso(this)" required>
                  <option value="" disabled selected>Seleccione un curso</option>
                  {foreach from = $saDatos['paDocAut'] item = i}
                     <option nMonto="{$i['NMONTO']}" cIdCate="{$i['CIDCATE']}" cDescri="{$i['CDESCRI']}" value="{$i['CIDCATE']}">{$i['CIDCATE']} - {$i['CDESCRI']} ({$i['NMONTO']})</option>
                  {/foreach}
               </select>
            </td>               
            </tr><tr>
            <td colspan="2"><button type="button" style="float: right" class="btn btn-success" onclick="f_agregarCurso();">Agregar</button></td>
            </tr>
         </table>
         <div class="table-responsive">
            <table class="table table-hover">
               <thead>
                  <tr>
                  <th class="col-sm-10">Nombre del Documento</th>
                  <th>Monto</th>
                  </tr>
               </thead>
               <tbody id="t_cursos">
                  {$lnTotal = 6.00}
                  <!--{foreach from = $saDatos['paInscr'] item = i}-->
                     <tr>
                     <td>{$i['CDESCRI']}</td>
                     <td>S/.{$i['NMONTO']}</td>
                     <td>{$i['DGENERA']}</td>
                     <td style='text-align: center'>
                        {if $i['CESTADO'] == 'P'}
                           {$lnTotal = $lnTotal + $i['NMONTO']}
                           <span class="label label-default">PENDIENTE</span>                    
                        {elseif $i['CESTADO'] == 'A'}
                           {$lnTotal = $lnTotal + $i['NMONTO']}
                           <span class="label label-success">APROBADO</span>
                        {elseif $i['CESTADO'] == 'D'}
                           <a class="label label-danger">RECHAZADO</a>
                        {elseif $i['CESTADO'] == 'X'}
                           <span class="label label-warning">ANULADO</span>
                        {/if}
                     </td>
                     <td style='text-align: center' class="col-xs-1">            
                        <input type="radio" name="paData[NSERIAL]"  value="{$i['NSERIAL']}"/>
                     </tr>
                  <!--{/foreach}-->
               </tbody>
            </table>
         </div>
         <div class="table-responsive">
            <label style="float: right; text-align: right"><h4>Derecho de trámite: S/.6.00</h4>
            {if !empty($saData['CNROPAG'])}
               <!--<h4>Código de pago: {$saData['CNROPAG']}</h4>-->
            {/if}
            <h3 id="r_total">Total: S/.{number_format($lnTotal, 2)}</h3></label>
         </div>
         {if !empty($saData['CNROPAG'])}
            <div class="table-responsive">
               <div class="alert alert-success" align="center">
                  <h3><strong>¡Deuda Provisional Generada!</strong><br>Su ID de pago es: <br><strong>{$saData['CNROPAG']}</strong><br></h3>
                  <h3><strong>Indicar que el pago es por pensiones</strong></h3>
               </div>
            </div>
         {/if}

      </div>
   </div>
   <div class="row">
      <div class="col-md-4">
         <button class="btn btn-success btn-block btn-lg" name="Boton" value="Grabar" onclick="return confirm('¿Está que desea generar este codigo de pago? (ESTE TRAMITE SOLO ES VALIDO PARA TITULACIÓN ONLINE)')">Solicitar</button>
      </div>
      <div class="col-md-4"></div>
      <div class="col-md-4">
         <a href="Mnu2000.php" class="btn btn-danger btn-block btn-lg"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
      </div>
   </div>
</form>
{/if}
<div id="myModal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title"><b>Información general</b></h4>
   </div>
   <div class="modal-body">
      <p><b>Solo se admiten hasta 2 Documentos.</b></p>
   </div>
   <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>