<html>
<head>
   <title>Trámites Administrativos</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
</head>
<body class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80"> Trámites Administrativos</b></h3></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-10 col-md-push-1">
<form action="Paq2020.php" method="post">
   {if $snBehavior == 0}
   <div class="panel panel-success">
      <div class="panel-heading"><h3 class="panel-title"><b>MANTENIMIENTO DE CENTROS DE COSTO</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <table style="margin-bottom: 15px">
            <tr>
            <td class="col-xs-11"><input placeholder="Busqueda" class="form-control" type="text" name="paData[CBUSQUE]"></td>
            <td class="col-xs-1"><button name="Boton" value="Buscar" class="btn btn-primary">Buscar</button></td>
            </tr>
         </table>
         <div class="table-responsive">
            <table class="table table-hover">
               <tr>
               <th style='text-align: center'>Centro de Costo</th>
               <th style='text-align: left'>Unidad Académica</th>
               <th style='text-align: center'>Estado</th>
               <th style='text-align: center'>Activar</th>
               </tr>
               {foreach from = $saDatos item = i}
                  <tr>
                  <td style='text-align: left'>{$i['CCENCOS']} - {$i['CDESCRI']}</td>
                  <td style='text-align: left'>{$i['CUNIACA']} - {$i['CNOMUNI']}</td>
                  {if $i['CESTADO'] == 'A'}
                     <td style='text-align: center; color: #6fe300'>ACTIVO</td>
                  {else}
                     <td style='text-align: center; color: #e60000'>INACTIVO</td>
                  {/if}
                  <td style='text-align: center'><input type="radio" name="paData[CCENCOS]" value="{$i['CCENCOS']}"/></td>
                  </tr>
               {/foreach}
            </table>
            <input type="hidden" name="paData[NPAGINA]" value="{$saData['NPAGINA']}">
            <div class="col-xs-2 col-xs-push-5 btn-group">
               <button name="Boton" value="PaginaAnterior" class="btn btn-default" type="submit"><</button>
               <button onclick="return false" class="btn btn-default" type="submit">{$saData['NPAGINA']}</label>
               <button name="Boton" value="PaginaSiguiente" class="btn btn-default" type="submit">></button>
            </div>
         </div>
      </div>
   </div>
   <div class="row"> 
      <div class="col-sm-4">
         <button type="submit" name="Boton" value="Editar" class="center-block btn btn-info btn-lg btn-block"/> Editar <i class="glyphicon glyphicon-edit"></i>
      </div>
      <div class="col-sm-4"></div>
      <div class="col-sm-4">
         <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
   </div>
   {elseif $snBehavior == 1}
   <div class="panel panel-success">
      <div class="panel-heading"><h3 class="panel-title"><b>MANTENIMIENTO DE CENTROS DE COSTO</b>
         <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
      </div>
      <div class="panel-body">
         <input type="hidden" name="paData[CCENCOS]" value="{$saData['CCENCOS']}">
         <table class="table table-hover">
            <tr>
            <td><b>CENTRO DE COSTO </b></td>
            <td>{$saData['CCENCOS']} - {$saData['CDESCRI']}</td>
            </tr><tr>
            <td><b>UNIDAD ACADEMICA</b></td>
            <td>
            <select class="selectpicker form-control" data-live-search="true" name="paData[CUNIACA]">
               <option selected="true" readonly value="{$saData['CUNIACA']}">{$saData['CUNIACA']} - {$saData['CNOMUNI']}</option>
               {foreach from = $saDatos item = i}  
                  <option value="{$i['CUNIACA']}">{$i['CUNIACA']} - {$i['CNOMUNI']}</option> 
               {/foreach}
            </select>      
            </td>
            </tr><tr>
            <td><b>ESTADO </b></td>
            <td>
            <select class="form-control" name="paData[CESTADO]">
               <option value="A" {if $saData['CESTADO'] == 'A'} selected="true"{/if}>ACTIVO</option>
               <option value="I" {if $saData['CESTADO'] == 'I'} selected="true"{/if}>INACTIVO</option>
            </select>
            </td>
            </tr>
         </table>
      </div>
   </div>
   <div class="row">
      <div class="col-xs-4">
         <button type="submit" name="Boton" value="Actualizar" class="btn btn-success btn-lg btn-block"> Actualizar <i class="glyphicon glyphicon-refresh"></i></button>
      </div>
      <div class="col-xs-4"></div>
      <div class="col-xs-4">
        <a href="Paq2020.php" class="btn btn-danger btn-lg btn-block"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>      
   </div>
   {/if}   
</form>
</div>
</div>
</div>
</body>
</html>