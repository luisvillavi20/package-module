<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      function f_CargarGrupos(self) {
         $('#table_alumnos').html("");
         $.get('Paq2070.php', {
            Id: 'CargarGrupos',
            paData: {
               CTIPCOL: self.value
            }
         }, function (data) {
            $("#tr_gruposcolacion").css("display", "table-row")
            $('[name="paData[CCOLUAC]"]').html(data);
         });
      }
      function f_CargarAlumnos() {
         $.get('Paq2070.php', {
            Id: 'CargarAlumnos',
            paData:{
               CCOLUAC: $('[name="paData[CCOLUAC]"]').val(),
               CTIPCOL: $('[name="paData[CTIPCOL]"]').val(),
            }
         }, function (data) {
            $('#table_alumnos').html(data);
         });
      }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody">
<form action="Paq2070.php" method="post">
   <div class="row">
   <div class="col-sm-10 col-sm-push-1">
   <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>CONSTANCIAS EXPEDIENTES COMPLETOS</b></h3></div>
      <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-hover">
            <tr>
            <td><b>TIPO DE COLACION</b></td>
            <td colspan="3">
               <select class="form-control" name="paData[CTIPCOL]" onchange="f_CargarGrupos(this)">
                  <option selected="true" disabled value="">Seleccione tipo de colación...</option>
                  {foreach from=$saArrays['paTipCol'] item=i}
                     <option name="paData[CTIPCOL]" value="{$i['CCODIGO']}">{$i['CDESCRI']}</option>
                  {/foreach}
               </select>
            </td>
            </tr>
            <tr id="tr_colacion" style="display: none">
               <th>COLACION<input type="hidden" name="paData[CIDCOLA]"></th>
               <td id="td_colacion" colspan="3"></td>
            </tr>
            <tr id="tr_gruposcolacion" style="display:none">
            <th>GRUPOS DE COLACION</th>
            <td colspan="3"><select class="form-control" name="paData[CCOLUAC]"></select></td>
            </tr>
            <tr>
            <td><b>UNIDAD ACADEMICA</b></td>
            <td colspan="3">
               <select class="form-control" name="paData[CUNIACO]" onchange="f_CargarGrupos(this)">
                  <option selected="true" disabled value="">Seleccione tipo de colación...</option>
                  {foreach from=$saDatos['paEscuelas'] item=i}
                     <option name="paData[CUNIACO]" value="{$i['CUNIACA']}">{$i['CNOMUNI']}</option>
                  {/foreach}
               </select>
            </td>
            </tr>
            </table>            
         </div>
         <div>
            <div class="col-sm-4">
               <button onclick="f_CargarAlumnos(); return false;" class="btn btn-success btn-block btn-lg">Cargar lista alumnos</button>
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir </a>
            </div>
         </div>
      </div>
      {/if}
   </div>
   </div>
   </div>
   <div id="table_alumnos"></div>
</form>
</div>
</body>
</html>