<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css?version=1">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
  <script>
    function fxBuscarCargos(){        
       var lcNroDni = $('#pcNroDni').val();         
       $.get( "index1.php?DNI="+lcNroDni+"&Id=BuscarCargos",
       function( data ) { 
       var lista = document.getElementById("pcNivel")    
        if (data.endsWith("*")){
          //QUITAR ULTIMO *  
          var data = data.slice(0,-1);        
          var lacargo = data.split('*');
          //LLENAR SELECT VACIO
          if (!lista.length>0){
            for(i = 0; i <lacargo.length; i++){    
              var option = document.createElement('option');
              lista.options.add(option, 0);
              var cnivel = lacargo[i].slice(lacargo[i].length-1,lacargo[i].length);
              lacargo[i] = lacargo[i].slice(0,-1);
              lista.options[0].value = cnivel;
              lista.options[0].innerText = lacargo[i]; 
            }
          //LLENAR SELECT QUE ESTABA LLENO
        }else{
          while (lista.length > 0) {
            lista.remove(lista.length-1);
          } 
          for(i = 0; i <lacargo.length; i++){    
            var option = document.createElement('option');
            lista.options.add(option, 0);
            var cnivel = lacargo[i].slice(lacargo[i].length-1,lacargo[i].length);
            lacargo[i] = lacargo[i].slice(0,-1);
            lista.options[0].value = cnivel;
            lista.options[0].innerText = lacargo[i]; 
          }
        } 
      }else{
        //"NO DISPONIBLE"
        while (lista.length > 0) {
            lista.remove(lista.length-1);
        } 
        var option = document.createElement('option');
        lista.options.add(option, 0);
        lista.options[0].value = '*';
        lista.options[0].innerText = data; 
      }        
    });
  }
  </script>
</head>  
<body onload="f_Init()">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>

<div class="container-fluid divBody table-responsive">
<form action="index1.php" method="post">
<div class="container">
  <div class="row">
  <div class="col-sm-3"></div>  
  <div class="col-sm-6">
    <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title"><p class="text-justify"><b>INICIO DE SESIÓN </b></p></h3>
    </div>      
    <div class="panel-body ">
      <br><b>DNI</b>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> <input onblur="fxBuscarCargos()" type="text" id="pcNroDni" class="form-control btn-lg" maxlength="8"  minlength="8" name="paData[CNRODNI]" placeholder="Nro DNI" title="Ingrese su número de DNI" required autofocus/>
      </div>
      <br><b>Rol Sistema</b>
      <div class="input-group ">
        <span class="input-group-addon"><i class="glyphicon glyphicon-tags"></i></span>
        <select class="form-control" name="paData[CNIVEL]" id="pcNivel"></select>  
      </div>
      <br><b>Contraseña</b>
      <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input type="password" class="form-control btn-lg" name="paData[CCLAVE]" required/>
      </div>
      <div align="center">
         <br><button type="submit" name="Boton" value="Iniciar" class="center-block btn btn-success btn-lg btn-block">Iniciar Sesión <i class="glyphicon glyphicon-log-in"></i> 
        </button>
        <a href="index.php" class="center-block btn btn-primary btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Solo Alumnos</a>
      </div>
    </div>
  </div>
  </div>
  <div class="col-sm-3"></div> 
  </div>
</div>
</form>
</div>
<div id="footer">
</div>
</body>
</html>
