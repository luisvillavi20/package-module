<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script>
      function f_changeColacion(self) {
         lcIdCola = self.value;
         $.get('Paq1260.php', {
            Id: 'CargarExpedientes',
            paData: {
               CIDCOLA: lcIdCola
            }
         }, function(data) {
            $('#t_expalumnos').html(data);
         });
         if (lcIdCola != 'T') {
            $('button[value="Imprimir"]').removeClass("hidden");
         } else {
            $('button[value="Imprimir"]').addClass("hidden");
         }
      }

      function f_changeFacultad(self) {
         $('#cUniAca').html('');
         lcEstPre = self.value;
         var lcSend = "Id=CargaUniAca&paData[CESTPRE]="+lcEstPre.trim();
         $.post("Paq1260.php",lcSend).done(function(p_cResult) {
            if (!isJson(p_cResult.trim())) {
               alert('RESULTADO INVALIDO');
            }
            var laJson = JSON.parse(p_cResult.trim());
            if (laJson.ERROR){
               alert(laJson.ERROR);
            } else {
               $('#cUniAca').prop('options')[0] = new Option('--- SELECCIONE UNIDAD ACADEMICA ---', '', true);
               $('#cUniAca').prop('options')[0].disabled = true;
               $.each(laJson, function(i, loObjeto) {
                  $('#cUniAca').prop('options')[i+1] = new Option(loObjeto.CNOMUNI, loObjeto.CUNIACA);
               });
               $('#divUniAca').show();
            }
         });
      }

      $(document).ready(function() {
        $("#cUniAca").change(function(e) {
            $('button[value="Actas"]').removeClass("hidden");
        });
      });
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader" style="padding: 0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Seguimiento de Bachiller</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1260.php" method="post">
   {if $snBehavior == 0}
   <div class="container divBody">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-success">
               <div class="panel-heading"><h3 class="panel-title"><b>HISTORIAL EXPEDIENTES - COLACION</b></h3></div>   
               <div class="panel-body">
                  <table class="table table-condensed">
                     <tr>
                     <td class="col-xs-1">Colación</td>
                     <td class="col-xs-11">
                        <select class="form-control" onchange="f_changeColacion(this)" name="paData[CIDCOLA]">
                           <option value="T">TODOS</option>
                           {foreach from = $saHisCol item=i}
                           <option value="{$i['CIDCOLA']}">{$i['CDESCRI']}</option>
                           {/foreach}
                        </select>
                     </td>
                     </tr>
                     <tr>
                     <td class="col-xs-1">Facultades</td>
                     <td class="col-xs-11">
                        <select class="form-control" id="cFacult" onchange="f_changeFacultad(this)" name="paData[CESTPRE]" hidden>
                           <option value="T">TODOS</option>
                           {foreach from = $saLisFac item=i}
                           <option value="{$i['CESTPRE']}">{$i['CDESCRI']}</option>
                           {/foreach}
                        </select>
                     </td>
                     </tr>
                     <tr id="divUniAca" hidden>
                     <td class="col-xs-1">Unidad Académica</td>
                     <td class="col-xs-11">
                        <select class="form-control" id="cUniAca"name="paData[CUNIACA]" hidden>
                        </select>
                     </td>
                     </tr>
                  </table>
                  <div class="table-responsive mh-50">
                  <table class="table table-condensed"> 
                     <thead> 
                        <th style='text-align: center' class="col-xs-2">Fecha Recepción</th>
                        <th style='text-align: center' class="col-xs-1">Cod. Alumno</th>
                        <th style='text-align: center' class="col-xs-1">DNI</th>
                        <th style='text-align: left' class="col-xs-3">Nombre</th>
                        <th style='text-align: left' class="col-xs-4">Unidad Académica</th>       
                        <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                     </thead>
                     <tbody id="t_expalumnos">
                     {foreach from = $saDatos item = i}
                        <tr>
                        <td style='text-align: center'>{$i['DRECEPC']}</td>
                        <td style='text-align: center'>{$i['CCODALU']}</td>
                        <td style='text-align: center'>{$i['CNRODNI']}</td>
                        <td style='text-align: left'>{$i['CNOMBRE']}</td>
                        <td style='text-align: left'>{$i['CNOMUNI']}</td>
                        <td style="text-align: center"><input type="radio" name="paData[CCODALU]" value="{$i['CCODALU']}" required></td>
                        </tr>
                     {/foreach}
                     </tbody>
                  </table>
                  </div>
               </div>
            </div>
         </div>
      </div>                
      <div class="row">
         <div class="col-sm-3">
            <button type="submit" name="Boton" value="Seguimiento" class="center-block btn btn-primary btn-lg btn-block"> Seguimiento <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-3">
            <button type="submit" name="Boton" value="Imprimir" class="hidden btn btn-info btn-block btn-lg" formnovalidate>Imprimir Cargo</button>
         </div>
         <div class="col-sm-3">
            <button type="submit" name="Boton" value="Actas" class="hidden btn btn-success btn-block btn-lg" formnovalidate>Imprimir Actas</button>
         </div>
         <div class="col-sm-3">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 1}
      <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITES</b>
              <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
           </div>
         <div class="panel-body">
            <p class="text-muted"><b>ALUMNO: {$saData['CNRODNI']} - {$saData['CNOMALU']} - {$saData['CNOMUNI']}</b></p>       
            <div class="table-responsive mh-50">
            <table class="table table-condensed"> 
               <thead> 
                  <th style='text-align: center' class="col-xs-4">Tipo Documento</th>
                  <th style='text-align: center' class="col-xs-2">Nro Expediente</th>
                  <th style='text-align: center' class="col-xs-2">Fecha de Envio</th>  
                  <th style='text-align: center' class="col-xs-1">Estado</th>       
                  <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
               </thead>
               {$j = 0}
               {foreach from = $saDatos item = i}  
                  <tr>         
                  <td style='text-align: left' class="col-xs-4">{$i['CDESDOC']}</td>
                  <td style='text-align: center' class="col-xs-2">E-{$i['CCODTRE']}</td>
                  <td style='text-align: center' class="col-xs-2">{$i['TFECREC']}</td>
                  <td style='text-align: center' class="col-xs-1"> 
                  {if $i['CESTDTR'] !=  null and ($i['CESTMTR'] == 'B' or $i['CESTMTR'] != 'S' )}
                     {if $i['CESTDTR'] == 'A' }
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {elseif $i['CESTDTR'] == 'B' or $i['CESTMTR'] == 'E'}  
                        <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {elseif $i['CESTDTR'] == 'F' or $i['CESTMTR'] == 'F' }
                        <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                     
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M' }  
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Mesa de partes">
                     {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S' }  
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                     {else}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  {else}
                     {if $i['CESTMTR'] == 'E'}   
                        <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'CCCONB' OR ($i['CIDCATE'] != 'CCCSID' OR $i['CIDCATE'] != 'CCCOND'))}  
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
                     {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 2) == 'CC' and ($i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCESTU')}
                        <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                     {elseif $i['CESTMTR'] == 'M' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Mesa de partes">
                     {elseif $i['CESTMTR'] == 'S' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND')}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                     {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 1) == 'P'}
                        <img src="Images/subir.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Subir Archivo">      
                     {else}
                        <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  {/if}
                  </td>
                  <td style='text-align: center' class="col-xs-1">            
                  <!--RADIO BUTTON PARA SEGUIMIENTO / OBSERVACION / GENERAR CONSTANCIA !--> 
                  <input type="radio" name="pcCodtre"  value="{$i['CCODTRE']}"/>
                  <input type="hidden" name="pcNroDni" value="{$i['CNRODNI']}">
                  </td>
                  </tr>
                  {$j = $j + 1}
               {/foreach}
            </table>
            </div>
            </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-sm-4">
               <button type="submit" name="Boton" value="SeguimientoDocumento" class="center-block btn btn-primary btn-lg btn-block"> Seguimiento <i class="glyphicon glyphicon-forward"></i></button><br>
            </div>  
         <div class="col-sm-4">
         </div>
         <div class="col-sm-4">
            <a href="Paq1260.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div> 
         </div> 
         </div> 
         </div>

         {elseif $snBehavior == 2}
         <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL TRÁMITE</b></h3></div>
            <div class="panel-body">    
            <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
            <div class="form-group">
            <table class="table table-striped">   
               <tr class="info">
                  <th colspan="2">Datos del Trámite:</th>
               </tr><tr>
                  <th><label>Tipo de Trámite: </label></th>
                  <th><label>{$saDatos[0]['CDESCRI']}</label></th>
               </tr><tr>
                  <th><label>Observaciones: </label></th>
                  {if $saDatos[0]['MOBSERM'] != null}
                     <th><label>{$saDatos[0]['MOBSERM']}</label></th>
                  {elseif isset($saDatos[0]['MOBSERD']['MOBSERV']) != null}
                     <th><label>{$saDatos[0]['MOBSERD']['MOBSERV']}</label></th>
                  {elseif $saDatos[0]['MOBSERM'] == null or $saDatos[0]['MOBSERD']['MOBSERV']== null }
                     <th><label> </label></th>
                  {/if}
               </tr><tr>   
            </table>
            </div>      
            <br>
            <div class="table-responsive">
            <table class="table table-hover">
               <tr class="info">
                     <th colspan="5">Ubicación del Documento:</th>
                  </tr>
               <th class="col-xs-2" style='text-align: center'>Recepción</th>  
               <th class="col-xs-2" style='text-align: center'>Envio/Aprobación</th> 
               <th class="col-xs-2" style='text-align: center'>Responsable</th>
               <th class="col-xs-4" style='text-align: center'>Unidad</th>     
               <th class="col-xs-2" style='text-align: center'>Estado</th>            
                  {foreach from = $saDatos item = i} 
                  <tr>
                     <td style='text-align: center'>{$i['TRECIBI']}</td>
                     <td style='text-align: center'>
                     {if $i['TULTIMA'] == null }
                        <span class="label label-default">-----</span>
                     {else}
                        {$i['TULTIMA']}
                     {/if}
                     </td>
                     <td style='text-align: center'>{$i['CNOMPER']}</td>
                     <td style='text-align: center'>{$i['CDESCRI']}</td> 
                     <td style='text-align: center'><h4>
                     {if $i['CESTDTR'] != null}
                        {if $i['CESTDTR'] == 'A' }
                           <span class="label label-default">PENDIENTE</span>                    
                        {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] != 'M' }
                           <span class="label label-success">REVISADO</span>
                        {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M' }
                           <span class="label label-success">VISTO - APROBADO</span>
                        {elseif $i['CESTDTR'] == 'B' }  
                           <span class="label label-warning">VISTO - OBSERVADO</span>
                        {elseif $i['CESTDTR'] == 'F' }  
                           <span class="label label-danger">LLENAR FORMULARIO</span>
                        {else}
                           <span class="label label-default">PENDIENTE</span>
                        {/if}
                     {else}
                        {if $i['CESTMTR'] == 'A' and ($i['CIDCATE'] != 'CCCOND' or $i['CIDCATE'] == 'CCCONB')}
                           <span class="label label-default">PENDIENTE</span> 
                        {elseif $i['CESTMTR'] == 'F' and ($i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == 'CCCONB')}
                           <span class="label label-default">PENDIENTE</span> 
                        {elseif $i['CESTMTR'] == 'B' }
                           <span class="label label-success">EN REVISIÓN</span>
                        {elseif $i['CESTMTR'] == 'E' }  
                           <span class="label label-warning">VISTO - OBSERVADO</span>
                        {elseif  $i['CESTMTR'] == 'F' }  
                           <span class="label label-danger">SUBIR ARCHIVO</span>
                        {elseif  $i['CESTMTR'] == 'M' }  
                           <span class="label label-success">VISTO - APROBADO</span> 
                        {else}
                           <span class="label label-default">PENDIENTE</span>
                        {/if}
                     {/if}
                     </h4></td>
                  </tr>  
               {/foreach}
            </table> 
                <br>
                {if $i['CESTMTR'] =='M' or $i['CESTMTR'] == 'S' and ($i['CIDCATE'] == 'CCESTU' or $i['CIDCATE'] == 'CCCSID')}
                  <div class="row col-md-10 col-md-push-1">
                     <div class="col-sm-4"></div>
                     <div class="col-sm-4">
                        {if $i['CESTPRO'] == 'R'}
                           <a class="btn btn-info btn-block btn-lg" onClick="window.open('{$i['CDOCDIG']}', 'Algo', 'height=850 ,width=850');">Ver Documento</a>
                        {else}   
                           <button type="button" class="btn btn-info btn-block btn-lg" name="pcCodtre" onClick="window.open('DocumentoPaquete.php?CNRODNI={$saData['CNRODNA']}&CCODTRE={$saData['CCODTRE']}', 'Algo', 'height=850, width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>
                        {/if}   
                     </div>
                  </div>
                  <div class="row col-md-10 col-md-push-1">
                     <iframe id="frameDocumento" style="width: 100%; height:80vh; display: none" src="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}"></iframe>
                  </div>
               {/if}
            </div>         
         </div>
         </div>
         <div class="row col-md-4 col-md-push-8">     
         <a href="Paq1260.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
         </div>
         </div>
         </div> 
         </div>
      

         {elseif $snBehavior == 3}
         <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DE TRÁMITE</b></h3></div>
            <div class="panel-body">  
            <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
               {foreach from = $saDatos item = i} 
                  <div class="form-group">
                     <table class="table table-striped">   
                     <tr class="info">
                        <th colspan="2">Datos del Trámite</th>
                     </tr><tr>
                        <th class="col-xs-3"><label>Tipo de Trámite: </label></th>
                        <th><label>{$saDatos[0]['CDESCRI']}</label></th>
                     </tr><tr>
                        <th class="col-xs-3"><label>Codigo Usuario: </label></th>
                        <th><label>{$i['CCODUSU']}</label></th>
                     </tr><tr>
                        <th class="col-xs-3"><label>Fecha de termino: </label></th>
                        <th><label>{$i['TRECIBI']}</label></th>
                     </tr>
                     </table>
                  </div>  
               {/foreach}     
            </div>
         </div>   
         <div class="row">
            <div class="col-sm-4">
               <button type="button" class="btn btn-info btn-block btn-lg" name="pcCodtre" onClick="window.open('DocumentoPaquete.php?CNRODNI={$saData['CNRODNA']}&CCODTRE={$saData['CCODTRE']}', 'Algo', 'height=850, width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <a href="Paq1260.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
            </div>
         </div> 
        
         {elseif $snBehavior == 4}
         <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL TRÁMITE</b></h3></div>
            <div class="panel-body">  
            <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
               {foreach from = $saDatos item = i} 
                  <div class="form-group">
                     <table class="table table-striped">   
                     <tr class="info">
                        <th colspan="2">Datos del Trámite</th>
                     </tr><tr>
                        <th class="col-xs-3"><label>Tipo de Trámite: </label></th>
                        <th ><label>{$saDatos[0]['CDESCRI']}</label></th>
                     </tr><tr>
                        <th><label>Codigo Usuario: </label></th>
                        <th><label>{$i['MDETALL']['CCODUSU']}</label></th>
                     </tr><tr>
                        <th><label>Fecha de termino: </label></th>
                        <th><label>{$i['MDETALL']['DVENCIM']}</label></th>
                     </table>
                  </div>  
               {/foreach}    
            </div>
         </div>   
         <div class="row">
            <div class="col-sm-4">
               <a class="btn btn-info btn-block btn-lg" onClick="window.open('{$i['MDETALL']['CURLPDF']}', 'Algo', 'height=850 ,width=850');">Ver Documento</a>
            </div>
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
               <a href="Paq1260.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
            </div>
          </div> 
         {elseif $snBehavior == 4}
         <div class="container-fluid">
         <div class="row col-md-10 col-md-push-1"> 
         <div class="panel panel-default">  
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL ARCHIVO</b></h3></div>
            <div class="panel-body">  
               <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>     
               <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
               <div class="form-group">
                  <table class="table table-striped">   
                     <tr class="info">
                        <th colspan="2">Datos del Trámite:</th>
                     </tr><tr>
                        <th><label>Tipo de Trámite: </label></th>
                        <th><label>{$saDatos[0]['CDESDOC']}</label></th>
                     </tr><tr>
                        <th><label>Observaciones: </label></th>
                        <th><label>{$saDatos[0]['MOBSERV']}</label></th>
                     </tr><tr>   
                  </table>
               </div>      
               <br>
               <div class="table-responsive">
               <table class="table table-hover">
                  <tr class="info">
                        <th colspan="5">Ubicación del Documento:</th>
                  </tr>
                  <th class="col-xs-2" style='text-align: center'>Recepción</th>  
                  <th class="col-xs-2" style='text-align: center'>Envio/Aprobación</th> 
                  <th class="col-xs-2" style='text-align: center'>Responsable</th>
                  <th class="col-xs-4" style='text-align: center'>Unidad</th>     
                  <th class="col-xs-2" style='text-align: center'>Estado</th>            
                     {foreach from = $saDatos item = i} 
                     <tr>
                        <td style='text-align: center'>{$i['TFECREC']}</td>
                        <td style='text-align: center'>
                        {if $i['TFECFIN'] == '' }
                           <span class="label label-default">-----</span>
                        {else}
                           {$i['TFECFIN']}
                        {/if}
                        </td>
                        <td style='text-align: center'>{$i['CNOMPER']}</td>
                        <td style='text-align: center'>{$i['CDESCCO']}</td> 
                        <td style='text-align: center'><h4>
                        {if $i['CESTDTR'] == 'A' }
                        <span class="label label-default">PENDIENTE</span> 
                        {elseif $i['CESTDTR'] == 'C' }
                           <span class="label label-success">VISTO - APROBADO</span>
                        {elseif $i['CESTDTR'] == 'B' }  
                           <span class="label label-warning">VISTO - OBSERVADO</span>
                        {elseif $i['CESTDTR'] == 'X' }  
                           <span class="label label-danger">VISTO - ANULADO</span>
                        {/if}
                        </h4></td>
                     </tr>  
                  {/foreach}
               </table> 
               </div>     
            </div>
            </div>
         <div class="row col-md-4 col-md-push-8">     
         <a href="Paq1260.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
         </div>
         </div>
         </div> 
         </div>
         {/if} 
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer"></div>
</body>
</html>