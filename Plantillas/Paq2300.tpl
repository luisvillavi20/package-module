<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script >
      function valSoloNumericos(e){
         if (e.which == 8) {
            return true;
         }
         if (e.key < '0' || e.key > '9') {
            return false;
         }
         return true;
      }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<div class="container-fluid">
<form action="Paq2300.php" method="post">
<div class="row">
<div class="col-sm-10 col-sm-push-1">
   <div class="panel panel-success">
      <div class="panel-heading"><h3 class="panel-title"><b>REGISTRAR COLACIÓN</b><span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3></div>
      <div class="panel-body">
         <p class="text-muted">
            <b> Última colación registrada: </b>
            <b>{$saData['CPERIOD']} - {$saData['CDESCRI']}</b><br>
            <b> Fecha: </b> 
            <b>{$saData['DFECHA']}</b>
         </p>
         <div class="table-responsive">
            <table class="table table-condensed">
               <tr>
                  <td class="col-sm-4">TIPO DE COLACIÓN:</td>
                  <td class="col-sm-8">
                     <select name="paData[CTIPCOL]" class="form-control">
                        {foreach from=$saDatos['paTipoCola'] item=i}
                           <option value="{$i['CCODIGO']}">{$i['CDESCRI']}</option>
                        {/foreach}
                     </select>
                  </td>
               </tr><tr>
                  <td>FECHA DE COLACIÓN:</td>
                  <td><input type="date" class="form-control" name="paData[DFECHA]" required/></td>
               </tr>               
            </table>
         </div>      
         <div class="row">
            <div class="col-sm-6">
               <button type="submit" name="Boton" value="Generar" class="center-block btn btn-success btn-lg btn-block"> Nuevo <i class="glyphicon glyphicon-plus-sign"></i></button>
            </div>
            <div class="col-sm-6">
               <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</form>
</div>
</body>
</html>