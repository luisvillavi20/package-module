<html>   
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script> 
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
         function myFunction(){
            $('#modalNew').modal('show');
         }
		});
      function f_activateButtons() {
         var lcOpcion = $('#pcOpcion').val();
         if (lcOpcion == 'E') {
            $('#Entregar').css("display", "none");
         }
         else {
            $('#Entregar').css("display", "block");
         }
      }

      function f_checkAll(p_self) {
         if (p_self.checked) {
            $('input[name="paCIdenti[]"]').prop('checked', true);
            f_activateButtons();
         }
         else {
            $('input[name="paCIdenti[]"]').prop('checked', false);
            document.getElementById("Entregar").style.display = "none";
         }
      }

      function f_changeCheckSelection(p_self) {
         if (p_self.checked) {
            f_activateButtons();
         }
         else if ($('input[name="paCIdenti[]"]').filter(':checked').length == 0) {
            document.getElementById("Entregar").style.display = "none";
         }
      }
      function f_Opcion(p_self){
         var rows = $('table.table tr');
         if (p_self.value == 'T'){
            rows.filter('.Entreg').show();
            rows.filter('.nEntrega').show();
            $('input[name="paCIdenti[]"]').prop('disabled', );
         }
         if (p_self.value == 'E'){
            rows.filter('.Entreg').show();
            rows.not('.Entreg').hide();
            document.getElementById("Entregar").style.display = "none";
            //document.getElementById("Reporte").style.display = "block";
            $('input[name="paCIdenti[]"]').prop('disabled', false);

         }
         if (p_self.value == 'A'){
            rows.filter('.nEntrega').show();
             rows.filter('.Entreg').hide();
         } 
         rows.filter('.tHead').show();
      }
	</script>
</head>
<body onload="f_Init()" class="divBody">
   <div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Seguimiento de Trámites </b></h3></div>
   <div class="container-fluid divBody">
   <form action="Tdo5060.php" method="post">
   {if $snBehavior == 0}
      <div class="container-fluid">
      <div class="row col-md-10 col-md-push-1"> 
      <div class="panel panel-default">  
      <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITES</b>
      <select id="pcOpcion" name="paData[CESTADO]"  class="selectpicker form-control" onchange="f_Opcion(this);">
         <option value="T">TODOS</option>
			<option value="A">PENDIENTES</option>
			<option value="E">ENTREGADOS</option>
		</select></h3></div>
      <div class="panel-body">
         <p class="text-muted"><b>USUARIO: {$saData['CNOMBRE']}</b></p>       
      <div class="table-responsive mh-50">
      <table class="table table-condensed"> 
         <tr class="tHead"> 
           <th style='text-align: center' class="col-xs-2">Codigo de Tramite</th>  
           <th style='text-align: center' class="col-xs-2">Codigo de Alumno</th>  
           <th style='text-align: center' class="col-xs-3">Nombre de Alumno</th>
           <th style='text-align: center' class="col-xs-3">Nombre Uni. Aca.</th> 
           <th style='text-align: center' class="col-xs-2">Estado</th>
           <th class="col-xs-1" style='text-align: center'>Activar<br><input type="checkbox" id="checkAll" class="form-check-input position-static" onclick="f_checkAll(this);"></th>
           <th style='text-align: center'><i class="glyphicon glyphicon-ok"></i></th>       
         </tr>
         {foreach from = $saDatos item = i}
            {if $i['CESTADO'] eq 'E'}
               <tr class="Entreg bg-success">
            {else}      
               <tr class="nEntrega">
            {/if}
               <td style='text-align: center' class="col-xs-2">E-{$i['CCODTRE']}</td>         
               <td style='text-align: center' class="col-xs-2">{$i['CCODALU']}</td>        
               <td style='text-align: left' class="col-xs-3">{$i['CNOMBRE']}</td>
               <td style='text-align: left' class="col-xs-3">{$i['CNOMUNI']}</td>
               <td style='text-align: center' class = "col-xs-2"><h4>
                  {if $i['CESTADO'] == 'S' }
                     <span class="label label-default">SOLICITADO</span> 
                  {elseif $i['CESTADO'] == 'A' }
                     <span class="label label-success">ACTIVO</span>
                  {elseif $i['CESTADO'] == 'C' }  
                     <span class="label label-warning">CERRADO</span>
                  {elseif $i['CESTADO'] == 'X' }  
                     <span class="label label-danger">ANULADO</span>
                     {elseif $i['CESTADO'] == 'E' }  
                     <span class="label label-primary">ENTREGADO</span>
                  {/if}
                  </h4></td>
               {if $i['CESTADO'] eq 'E'}
                  <td style='text-align: center'><input onchange="f_changeCheckSelection(this)" type="checkbox" name="paCIdenti[]" value="{$i['CIDENTI']}" disabled/></td>
               {else}
                  <td style='text-align: center'><input onchange="f_changeCheckSelection(this)" type="checkbox" name="paCIdenti[]" value="{$i['CIDENTI']}"/></td>
               {/if}
               <td style='text-align: center' class="col-xs-1">            
                  <input type="radio" name="paData[CIDENTI]"  value="{$i['CIDENTI']}" required >
               </td>
             </tr>
         {/foreach}
      </table>
      </div>
      </div>
      </div>    
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button class="center-block btn btn-success btn-lg btn-block" name = "Boton" value= "Seguir">Seguimiento&nbsp;&nbsp;<i class="glyphicon glyphicon-eye-open"></i></a>
         </div>  
         <div class="col-sm-4" id="Entregar" style="display: none">
            <button type="submit" class="center-block btn btn-primary btn-lg btn-block" name = "Boton" value= "Entregar" formnovalidate>Entregar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4" id="Reporte" style="display: none">
            <button type="submit" class="center-block btn btn-primary btn-lg btn-block" name = "Boton" value= "Reporte" formnovalidate>Reporte&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div> 
      </div> 
      </div> 
      </div>
   </form>
   {elseif $snBehavior == 1}
   <div class="container-fluid">
      <div class="row col-md-10 col-md-push-1"> 
      <div class="panel panel-default">  
      <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITES</b></h3></div>
      <div class="panel-body">
         <p class="text-muted"><b>USUARIO: {$saData['CNOMBRE']}</b></p>       
      <div class="table-responsive mh-50">
      <input type="hidden" name="paData[CIDENTI]" value="{$saDatos[0]['CIDENTI']}">
      <table class="table table-condensed"> 
         <thead> 
           <th style='text-align: center' class="col-xs-2">Cód. de Curso</th>  
           <th style='text-align: center' class="col-xs-2">Nomb. de Curso</th>
           <th style='text-align: center' class="col-xs-2">Sem.</th>  
           <th style='text-align: center' class="col-xs-2">Créditos</th>
           <th style='text-align: center' class="col-xs-2">Inc.</th>  
           <th style='text-align: center' class="col-xs-3">Estado</th>
         </thead>
         {$j = 0}
         {foreach from = $saDatos item = i}      
            <tr>
               <td style='text-align: center' class="col-xs-2">{$i['CCODCUR']}</td>
               <td style='text-align: left' class="col-xs-2">{$i['CDESCRI']}</td>
               <td style='text-align: center' class="col-xs-2"><input type="text" class="form-control" name="paDataSem[]" required='required' value="{substr($i['CCODCUR'],2,2)}"></td>  
               <td style='text-align: center' class="col-xs-2">{$i['NSUMA']}</td>
               <td style='text-align: center' class="col-xs-2"><input type="text" class="form-control" name="paDataInc[]"  required='required'></td>               
               <td style='text-align: center' class = "col-xs-2"><h4>
                  {if $i['CESTESC'] == 'P' }
                     <span class="label label-default">PENDIENTE</span> 
                  {elseif $i['CESTESC'] == 'A' }
                     <span class="label label-success">REVISADO</span>
                  {elseif $i['CESTESC'] == 'D' }  
                     <span class="label label-danger">DENEGADO</span>
                  {/if}
                  </h4></td>
            </tr>
            {$j=$j + 1} 
         {/foreach}
      </table>
      </div>
      </div>
      </div>    
      </div>
      <div class="row">  
         <div class="col-sm-4">
         <button class="center-block btn btn-success btn-lg btn-block" name = "Boton" value= "PrintDecreto">Decreto&nbsp;&nbsp;<i class="glyphicon glyphicon-eye-open"></i></button>
         </div>
         <div class="col-sm-4"></div>
         <div class = "col-sm-4">
            <a href="Tdo5060.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div> 
      </div> 
      </div> 
      </div>
   </form>
   {/if}
</body>
</html>