<html>
  <head>
     <title>Trámites Administrativos</title>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="Styles/css/bootstrap.css">
     <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
     <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
     <link rel="stylesheet" href="CSS/style.css">
     <script src="js/java.js"></script>
     <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
     <script src="Styles/js/jquery-3.2.0.js"></script>
     <script src="Styles/js/bootstrap.js"></script>
     <script>
        $(document).ready(function(){
           $('[data-toggle="tooltip"]').tooltip();
        });
     </script>
  </head>
  <body class="divBody">
    <div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80"> Trámites Administrativos</b></h3></div>
    <div class="container-fluid">
      <form action="Paq1150.php" method="post">
         <div class="container divBody">
            <div class="row">
               <div class="col-sm-12">
               <div class="panel panel-success">
        {if $snBehavior == 0}
            <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES PENDIENTE</b>
               <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
            </div>
              <div class="table table-condensed table-responsive">
                <table class="table table-condensed table-striped">
                  <thead>
                     <th style="text-align: center" class="col-xs-1"><label>NRO. SERIE</label></th>
                     <th style="text-align: center" class="col-xs-2"><label>SOLICITANTE</label></th>
                     <th style="text-align: center" class="col-xs-3"><label>ALUMNO</label></th>
                     <th style="text-align: center" class="col-xs-2"><label>SOLICITUD</label></th>
                     <th style="text-align: center" class="col-xs-2"><label>ENCARGADO</label></th>
                     <th style="text-align: center" class="col-xs-2"><label>FECHA</label></th>
                     <th style="text-align: center" class="col-xs-1"><label>ESTADO</label></th>
                  </thead>
                  <tbody>
                    {$j=0}
                    {foreach from = $saDatos item = i}
                        <tr>
                          <td style="text-align: center">{$i['NSERIAL']}</td>
                          <td style="text-align: center">{$i['CNOMSOL']}</td>
                          <td style="text-align: center">{$i['CNOMALU']}</td>
                          <td style="text-align: center">{$i['CDESCRI']}</td>
                          <td style="text-align: center">{$i['CNOMREV']}</td>
                          <td style="text-align: center">{$i['CMODIFI']}</td>
                          <td style="text-align: center">
                            {if $i['CESTADO'] == 'E'}
                              <img src="Images/clock.png" width="30%" data-toggle="tooltip" data-placement="button" title="Pendiente">
                            {elseif $i['CESTADO'] == 'A'}
                              <img src="Images/aprobado2.png" width="30%" data-toggle="tooltip" data-placement="button" title="Aceptado">
                            {elseif $i['CESTADO'] == 'U'}
                              <img src="Images/aprobado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Usado">
                            {elseif $i['CESTADO'] == 'R'}
                              <img src="Images/denegar.png" width="30%" data-toggle="tooltip" data-placement="button" title="Rechazado">
                            {elseif $i['CESTADO'] == 'X'}
                              <img src="Images/anulado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Anulado">
                            {/if}
                          </td>
                        </tr>
                      {$j = $j + 1}
                    {/foreach} 
                  </tbody>        
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
              <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
            </div>
           </div>
        </div>
        {/if}
      </form>
    </div>
  </body>
  <div id="footer">
  </div>
</html>