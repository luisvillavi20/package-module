<html>
<head>
<title>Trámites</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="Styles/css/bootstrap.css">
<link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
<link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
<link rel="stylesheet" href="Styles/css/bootstrap-select.css">
<link rel="stylesheet" href="CSS/style.css">
<script src="js/java.js"></script>
<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
<script src="Styles/js/jquery-3.2.0.js"></script>
<script src="Styles/js/bootstrap.js"></script>
<script src="Styles/js/bootstrap-select.js"></script>
<script>
   function f_Init(p_nBehavior) {
      if (p_nBehavior == 0) {
         f_CargarSolictudes();
      }
   }

   function f_buscarDocente() {
      let lcBusque = $('[name="paData[CBUSQUE]"]').val();
      $.get('Tdo5020.php', {
         Id: 'BuscarDocente',
         paData: {
            CBUSQUE: lcBusque
         }
      }, function (data) {
         $('[name="paData[CCODDOC]"]').html(data);
      });
   }
   
   function f_cargarDatosModal(p_cCodCur, p_nJurado) {
      $('[name="paData[CCODCUR]"]').val(p_cCodCur);
      $('[name="paData[NJURADO]"]').val(p_nJurado);
   }

   function f_seleccionarFila(e) {
      var a = e.cells[e.cells.length-1].children.item(0);
      a.checked = true;
   }

   function f_CargarSolictudes() {
      var lcEstAsi = $("#pcEstAsi").val();
      if (lcEstAsi == null || lcEstAsi.trim() === "" || lcEstAsi.trim().length != 1) {
         alert("DEBE SELECCIONAR UN ESTADO DE SOLICITUD");
         $("#pcEstAsi").focus();
         return;
      }
      $('#divSolicitudes').html('');
      var lcSend = "Id=cargarSolicitudes&paData[CESTASI]=" + lcEstAsi;
      $.post("Tdo5020.php",lcSend).done(function(p_cResult) {
         var loJson = (isJson(p_cResult.trim()))? JSON.parse(p_cResult.trim()) : p_cResult;
         if (loJson.ERROR){
            alert(loJson.ERROR);
            return;
         }
         $('#divSolicitudes').html(p_cResult);
      });
   }
</script>
</head>
<body class="divBody" onload="f_Init({$snBehavior})">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Cursos por Jurado </b></h3></div>
<div class="container-fluid">
<form action="Tdo5020.php" method="POST">
   {if $snBehavior == 0}
   <div class="panel panel-default">
   <div class="panel-heading">
      <h3 class="panel-title"><b>CURSOS POR JURADO - ASIGNACION DE DOCENTES</b>
         <select id="pcEstAsi" class="selectpicker col-sm-2" onchange="f_CargarSolictudes();">
            <option value="N">SIN ASIGNAR</option>
            <option value="S">ASIGNADOS</option>
         </select>
         <span style="float:right"><b>ENCARGADO: </b>  {$scNombre}</span>
      </h3>
   </div>
   <div class="panel-body table-responsive mh-50">
      <table class="table table-condensed table-hover">
         <thead>
            <tr>
            <th>Fecha</th>
            <th>Código alumno</th>
            <th>Nombre</th>
            <th>Unidad Académica</th>
            <th style="text-align: center">Activar</th>
            </tr>
         </thead>
         <tbody id="divSolicitudes"></tbody>
      </table>
   </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         <button name="Boton" value="Activar" class="btn btn-success btn-block btn-lg">Activar</button>
      </div>
      <div class="col-sm-4"></div>
      <div class="col-sm-4">
         <a href="Mnu1000.php" class="btn btn-danger btn-block btn-lg">Salir</a>
      </div>
   </div>
   {elseif $snBehavior == 1}
   <div class="panel panel-default">
   <div class="panel-heading"><h3 class="panel-title"><b>CURSOS POR JURADO - ASIGNACION DE DOCENTES</b>
      <span style="float:right"><b>ENCARGADO: </b>  {$scNombre}</span></h3>
   </div>
   <div class="panel-body table-responsive">
      <table class="table table-condensed">
         <tr>
            <th>Estudiante:</th>
            <td>{$saData['CCODALU']} - {$saData['CNRODNI']} - {$saData['CNOMALU']}</td>
            <th>Escuela:</th>
            <td>{$saData['CUNIACA']} - {$saData['CNOMUNI']}</label></td>
         </tr>
         <tr>
            <th>Email:</th>
            <td>{$saData['CEMAIL']}</td>
            <th>Celular:</th>
            <td>{$saData['CNROCEL']}</td>
         </tr><tr>
            <th>ID Pago:</th>
            <td>{$saData['CNROPAG']}</td>
            <th>Fecha Pago:</th>
            <td>{$saData['TPAGO']}</label></td>
         </tr>
      </table>
      <table class="table table-hover">
         <thead>
            <tr>
            <th>Curso</th>
            <th>Jurado 1</th>
            {if !$saData['CNIVUNI']|in_array:['03','04']}
            <th>Jurado 2</th>
            {/if}
            </tr>
         </thead>
         <tbody>
            {foreach from=$saDatos item=i}
            {$lcCodCur = $i['CCODCUR']}
            <tr>
               <td>{$i['CCODCUR']} - {$i['CDESCRI']}</td>
               <td>
                  {if $i['CJURAD1'] == '0000'}                           
                     <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#ModalAsignar" onclick="f_cargarDatosModal('{$lcCodCur}', 1);" type="button">ASIGNAR</button>
                  {else}
                     {$i['CNOMJU1']}
                     <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#ModalAsignar" onclick="f_cargarDatosModal('{$lcCodCur}', 1);" type="button">EDITAR</button>
                  {/if}
               </td>
               {if !$saData['CNIVUNI']|in_array:['03','04']}
               <td>
                  {if $i['CJURAD2'] == '0000'}
                  <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#ModalAsignar" onclick="f_cargarDatosModal('{$lcCodCur}', 2);" type="button">ASIGNAR</button>
                  {else}
                     {$i['CNOMJU2']}
                     <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#ModalAsignar" onclick="f_cargarDatosModal('{$lcCodCur}', 2);" type="button">EDITAR</button>
                  {/if}
               </td>
               {/if}
            </tr>
            {/foreach}
         </tbody>
      </table>
   </div>
   </div>
   <div class="row">
      <div class="col-sm-8"></div>
      <div class="col-sm-4">
         <button class="btn btn-danger btn-block btn-lg" name="Boton" value="Salir">Salir</button>
      </div>
   </div>
   <!-- Modales-->
   <div id="ModalAsignar" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h3 class="modal-title">Asignar jurado</h3>
      </div>
      <div class="modal-body">
         <table class="table">
            <tr>
            <td><input style="text-transform: uppercase" placeholder="CÓDIGO TRABAJADOR, DNI O APELLIDOS Y NOMBRES DOCENTE" class="form-control" type="text" name="paData[CBUSQUE]"></td>
            <td><button class="btn btn-info btn-block" onclick="f_buscarDocente()" type="button">Buscar</button></td>
            </tr><tr>
            <td colspan="2">
               <select class="form-control" name="paData[CCODDOC]"></select>
               <input type="hidden" name="paData[CCODCUR]">
               <input type="hidden" name="paData[NJURADO]">
               <input type="hidden" name="paData[CIDENTI]" value="{$saData['CIDENTI']}">
            </td>
            </tr>
         </table>
      </div>
      <div class="modal-footer">
         <div class="button-group text-right">
            <button class="btn btn-success" name="Boton" value="AsignarJurado">Asignar Jurado</button>
            <input class="btn btn-danger" type="submit" class="boton" data-dismiss="modal" value="Cerrar"/>
         </div>
      </div>
   </div>
   </div>
   </div>
   {/if}
</form>
</body>
</html>