<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

</head>
<script>
   function showMod(e){
      if( $("input[type='radio']").is(':checked')){
         let cCodTre = $('input[name="paData[CCODTRA]"]:checked').val();
         $('input[name="paData[CCODTRE]"]').val(cCodTre);
         $('#modalObs').modal('show');  
      }
      else{
         window.alert('Seleccione un alumno');
      }
   }

   function cEstadoCaptura(){
      if( $("input[type='radio']").is(':checked')){
         var observ;
         $('.selex:checked').each(function(indice, elemento){
            var fila = $(this).parents(".Datos");
            cEstado = fila.find(".cestado").val();
            $('input[name="paData[CESTADO]"]').val(cEstado);
         });
      }
      else{
         window.alert('Seleccione un alumno');
      }
   }

   $(document).ready(function() {
      $('#table').dataTable();
   });
</script>
<body class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Empastados</b></h3></div>
<div class="container-fluid divBody">
<form action="Paq1510.php" method="post" enctype="multipart/form-data">
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title"><b>Bandeja de Solicitudes de Entrega de Empastados</b></h3></div>
<div class="panel-body">
   <p class="text-muted"><b>ENCARGADO: {$saData['CNOMBRE']}</b></p> 
   <div class="table-responsive">
   <table id ="table" class="table table-condensed display"> 
      <thead>
         <tr> 
            <th class="col-xs-1">Código</th>  
            <th class="col-xs-2">Fecha de Solicitud</th>  
            <th class="col-xs-1">DNI</th>
            <th class="col-xs-1">Cod. Alumno</th>  
            <th class="col-xs-4">Nombre Alumno</th>
            <th class="col-xs-3">Unidad Académica</th>
            <th class="col-xs-1"></th>
            <th style="text-align: center" class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
         </tr>
      </thead>
      <tbody>
      {foreach from = $saDatos item = i}
         <tr class="Datos">
            <td class="col-xs-1">E-{$i['CCODTRE']}</td>  
            <td class="col-xs-2">{$i['TFECHA']}</td>  
            <td class="col-xs-1">{$i['CNRODNI']}</td>
            <td class="col-xs-1">{$i['CCODALU']}</td>  
            <td class="col-xs-4">{$i['CNOMBRE']}</td>
            <td class="col-xs-3">{$i['CNOMUNI']}</td>
            {if $i['CESTADO'] == 'R'}
            <td class="col-xs-1">
               <button type="button" onclick="window.open('DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE={$i['CCODTRE']}', 'Algo', 'height=850, width=850');" class="center-block btn-primary"><i class="glyphicon glyphicon-eye-open"></i></button>
            </td>
            {else}
            <td class="col-xs-1"></td>
            {/if}
            <td class="col-xs-1">
            <input class="cestado" type="hidden" value="{$i['CESTADO']}">
            <input id="cEstado" type="hidden" name="paData[CESTADO]">
            <input class="selex" type="radio" name="paData[CCODTRA]" value="{$i['CCODTRE']}" onclick="cEstadoCaptura();">
            </td>
         </tr>
      {/foreach}
      </tbody>      
   </table>
   </div>  
</div>
</div>
<div>
   <div class="col-xs-4">
      <button type="submit" name="Boton" value="Aprobar" class="center-block btn btn-success btn-lg btn-block"> Aprobar <i class="glyphicon glyphicon-check"></i></button><br>
   </div>
   <div class="col-xs-4">
      <button type="button" onclick="showMod('O')" class="center-block btn btn-warning btn-lg btn-block"> Observar <i class="glyphicon glyphicon-eye"></i></button>
   </div>
   <div class="col-xs-4">
      <a href="Mnu1000.php" role="button" class="btn btn-danger btn-block btn-lg">Salir</a>
   </div>
</div>
</div>
</form>
<div class="modal fade" id="modalObs" role="dialog">
   <div class="modal-dialog">
   <form action="Paq1510.php" method="POST">
      <input type = "hidden" name = "paData[CCODTRE]" id = "moCodTre"> 
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">TURNITING OBSERVACION</h4>
         </div>
      <div class="modal-body">
      <table class="table table-condesed">
         <tr>
            <th>Observación</th>
            <td><textarea  style="resize: none; text-transform:uppercase" rows = "4" class="form-control" name="paData[MOBSERV]"></textarea></td>
         </tr>
      </table>
      </div>
      <div class="modal-footer">
         <button class="btn btn-warning" name="Boton" value="Observar">Observar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
      </div>
   </form>
   </div>
</div>
<div id="footer">
</div>
</div>
</body>
</html>