<html>   
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="Styles/js/bootstrap-select.js"></script>
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
	<script type="text/javascript" src="CSS/jquery.modal.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
   <script>
      $(document).ready(function() {
         $('#editarSolucitud').on('shown.bs.modal', function (e) {
            let nSerial = $('input[name="nSerial"]:checked').val();
            if (nSerial == undefined) {
               $('#editarSolucitud').modal('hide');
               alert('Seleccione una solicitud para denegar');
            } else {
               $('#editarSolucitud input[name="paData[NSERIAL]"]').val(nSerial);
               $('#editarSolucitud input[name="paData[NSERIAL1]"]').text(nSerial);
            }
         });
      });

      function f_Opcion(){
      var lcOpcion = $('#pcOpcion').val();
		var lcSend = "Id=Opcion&paData[COPCION]="+lcOpcion;
		$.post("Tdo5050.php",lcSend).done(function(p_cResult) {
			var laJson = (isJson(p_cResult))? JSON.parse(p_cResult) : p_cResult;
			if (laJson.ERROR){
				alert(laJson.ERROR);
				$('#TBody').html('');
			} else {
				$('#TBody').html(p_cResult);
			}
      });      
   }
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Solicitudes de Cursos por Jurado </b></h3></div>
<div class="container-fluid divBody">
<form action="Tdo5050.php" method="post">
   {if $snBehavior == 0}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel-heading">
   <h3 class="panel-title"><b>BANDEJA DE SOLICITUDES</b>
      <select id="pcOpcion" name="paData[CESTADO]" class="selectpicker" style="float:center" onchange="f_Opcion();">
			<option selected="true" value="TD">CURSOS NORMALES</option>
         <option value="V">CURSOS COMPLEMENTARIOS</option>
         <option value="O">SOLICITUDES CON MAS DE 11 CREDITOS </option>
         <option value="R">SOLICITUDES RETENIDAS</option>
		</select>		  
      <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <div class="table-responsive mh-50">
      <table class="table table-condensed table-hover">
         <thead>       
            <th style='text-align: left' class="col-xs-2">Cod. Alumno</th>
            <th style='text-align: left' class="col-xs-4">Nom. Alumno</th>
            <th style='text-align: left' class="col-xs-3">Uni. Académica</th>
            <th style='text-align: center' class="col-xs-1">Fecha de Solicitud</th>  
            <th style='text-align: center' class="col-xs-1">Estado</th>       
            <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
         </thead>
         <tbody id="TBody">
         {foreach from = $saDatos item = i}  
            <tr>         
            <td style='text-align: left' class="col-xs-2">{$i['CCODALU']}</td>
            <td style='text-align: left' class="col-xs-2">{$i['CNOMBRE']}</td>
            <td style='text-align: left' class="col-xs-2">{$i['CUNIACA']}</td>
            <td style='text-align: center' class="col-xs-2">{$i['DGENERA']}</td>
            <td style='text-align: center' class="col-xs-1">
               {if $i['CESTADO'] == 'A'}
                  <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
               {elseif $i['CESTADO'] == 'D'}  
                  <img src="Images/denegar.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Denegado">
               {elseif $i['CESTADO'] == 'P'}  
                  <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               {else}
                  <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               {/if}
            </td>
            <td style='text-align: center' class="col-xs-1">         
            <input type="radio" name="pcCidenti" value="{$i['CIDENTI']}" required></td>
            </tr>
         {/foreach}
      </tbody>
      </table>
      </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         <button type="submit" name="Boton" value="Revisar" class="center-block btn btn-primary btn-lg btn-block"> Revisar <i class="glyphicon glyphicon-forward"></i></button><br>
      </div>  
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
         <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div> 
   </div> 
   </div> 
   </div>
   {elseif $snBehavior == 1}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel-heading"><h3 class="panel-title"><b>DETALLE DE SOLICITUD </b>
      <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <p class="text-muted"><b>ALUMNO:</b> {$saDatos[0]['CNOMBRE']}  - <b>NRO. DNI: </b> {$saDatos[0]['CNRODNI']} - <b>COD. ALUMNO: </b> {$saDatos[0]['CCODALU']} - {$saDatos[0]['CNOMUNI']} </b></p>
      <div class="col-md-12 form-line">
      <input type="hidden" name="paData[CCODALU]" value="{$saDatos[0]['CCODALU']}">
      <input type="hidden" name="paData[CNRODNI]" value="{$saDatos[0]['CNRODNI']}">
      {if $saDatos[0]['MOBSERV'] != null}
         <table class="table table-condensed"> 
            <thead class="warning2">
               <th>Observaciones de la Escuela: </th>
            </thead>
            <tr id="formDenegado2" style="display:">
               {if $saDatos[1]['MOBSERB'] eq ''}
               <th><textarea rows="9" style="resize:none" class="form-control" name="paData[MOBSERV]" placeholder="CURSO              : {$saDatos[0]['CDESCRI']}{"\n"}INCISO              : {$saDatos[0]['MDETALL']['CINCISO']}{"\n"}OBSERVACION : {{$saDatos[0]['MDETALL']['MOBSERV']}}" disabled></textarea></th>
               {else}
               <th><textarea rows="9" style="resize:none" class="form-control" name="paData[MOBSERV]" readonly>- {$saDatos[0]['MOBSERV']}{"\n"}INCISO              : {$saDatos[0]['MDETALL']['CINCISO']}{"\n"}DETALLE           : {$saDatos[0]['MDETALL']['MOBSERV']}{"\n"}{"\n"}- {$saDatos[1]['MOBSERV']}{"\n"}INCISO              : {$saDatos[1]['MDETALL']['CINCISO']}{"\n"}DETALLE           : {$saDatos[1]['MDETALL']['MOBSERV']}</textarea></th>
               {/if}
            </tr>
         </table>
      {else}
         <label>NOTA:</label>
         <input type="text" class="form-control" id="usr" placeholder="* La escuela profesional de {$saDatos[0]['CNOMUNI']} no ha realizado ninguna observación con respecto a esta solicitud." disabled>
         <br>
      {/if}
      <div class="table-responsive mh-50">
         <table class="table table-condensed table-hover"> 
            <thead class="warning2"> 
               <th style='text-align: left' class="col-xs-1">Cod. Curso</th>
               <th style='text-align: left' class="col-xs-4">Nom. Curso</th>
               <th style='text-align: left' class="col-xs-2">Plan Estudios</th>
               <th style='text-align: center' class="col-xs-1" data-toggle="right" title="Créditos Teoría">Cred.Teoría</th>
               <th style='text-align: center' class="col-xs-1" data-toggle="right" title="Créditos Practicas">Cred.Prácticas</th>    
               <th style='text-align: center' class="col-xs-1">Estado</th>               
               <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
            </thead>
            {foreach from = $saDatos item = i}  
               <tr>         
               <td style='text-align: left' class="col-xs-1">{$i['CCODCUR']}</td>
               <td style='text-align: left' class="col-xs-4">{$i['CDESCRI']}</td>
               <td style='text-align: left' class="col-xs-2">{$i['CPLAEST']}</td>
               <td style='text-align: center' class="col-xs-1">{$i['NCRETEO']}</td>
               <td style='text-align: center' class="col-xs-1">{$i['NCRELAB']}</td>
               <td style='text-align: center' class="col-xs-1">
                  {if $i['CESTSAC'] == 'A'}
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
                  {elseif $i['CESTSAC'] == 'D'}  
                     <img src="Images/denegar.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Denegado">
                  {elseif $i['CESTSAC'] == 'P'}  
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
               </td>
                  <td style='text-align: center'><input type="radio" name="nSerial"  value="{$i['NSERIAL']}" {if $i['CESTSAC'] == 'A' OR $i['CESTSAC'] == 'D'} disabled {/if} required=""></td>
               </tr>
            {/foreach}
         </table>
      </div>
      </div> 
      </div>
      {if $saDatos[0]['CESTADO'] == 'O'} 
         <div id="error" class="alert alert-danger" role="alert">
            Especificaciones
            <li> El alumno con esta solicitud esta excediendo los 11 créditos en el presente periodo.</li>
         </div>
      {/if}
   </div>
   <div class="row">
      <div class="col-xs-3">
         <button type="submit" onclick="return confirm('¿Esta seguro que desea Aprobar el curso?')" name="Boton" value="Aprobar" class="center-block btn btn-success btn-lg btn-block">Aprobar <i class="glyphicon glyphicon-check"></i></button>
      </div>  
      <div class="col-xs-3">
         <button type="button"  onclick="return confirm('¿Esta seguro que desea Denegar el curso?')" data-toggle="modal" href="#editarSolucitud" class="btn btn-block btn-danger btn-lg">Denegar Curso <i class="glyphicon glyphicon-remove"></i></button>
      </div>
      <div class="col-xs-3">
         <button type="submit" onclick="return confirm('¿Esta seguro que desea detener la Solicitud?')"  name="Boton" value="Detener"  class="btn btn-block btn-warning btn-lg" formnovalidate>Solicitud Con deuda <i class="glyphicon glyphicon-eye-open"></i></button>
      </div>
      <!--<div class="col-xs-3">
         <button type="button" data-toggle="modal" href="#editarSolucitud" class="btn btn-block btn-info btn-lg">Observar Solicitud <i class="glyphicon glyphicon-eye-open"></i></button>
      </div>-->
      <div class="col-xs-3">
         <a href="Tdo5050.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div> 
   </div> 
   </div> 
   </div>  
   {/if} 
   </div>
   </div> 
   </div>
</form>
<form action="Tdo5050.php" method="POST">
<div class="modal fade" id="editarSolucitud" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">DENEGAR SOLICITUD</h4>
   </div>
   <div class="modal-body">
   <input type="hidden" name="paData[CCODALU]" value="{$saDatos[0]['CCODALU']}">
      <input type="hidden" name="paData[NSERIAL]">
      <table class="table" id="table_edit_documents">
         <tr><th>Observación</th>
         <td><textarea required="" rows="4" class="form-control" id="mobserv" name="paData[MOBSERV]" placeholder="Observaciones" style="resize:none text-transform:uppercase"></textarea></td>
      </table>
   </div>
   <div class="modal-footer">
      <div class="row">
         <div class="col-sm-6">
            <input type="submit" name="Boton" value="Denegar" onclick="return confirm('¿Está seguro que desea Denegar esta solicitud?') " class="center-block btn btn-warning btn-lg btn-block" />
         </div>
         <div class="col-sm-6">
            <button type="button" class="center-block btn btn-danger btn-lg btn-block"data-dismiss="modal">Cancelar</button>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</form>
</body>
</html>