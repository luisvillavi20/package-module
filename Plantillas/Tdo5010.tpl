<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<script>
function f_Calculavalor(){
		var lntotal = 0.0;
      var lnCosCre = parseFloat($('[name="paData[NCOSCRE]"]').val()); //68
      var lnCredi = parseFloat($('[name="paData[NCREDIT]"]').val()); //1
      lntotal =  lnCredi * lnCosCre;
      console.log('calcular');
	   $('#Costo').html(Number(lntotal).toFixed(2));
      $('[name="paData[NMONTO]"]').val(Number(lntotal).toFixed(2));
   }

function f_Mostrar() {
  document.getElementById("mySelect").disabled = false;
  document.getElementById("Mostrar").style.display = '';
  document.getElementById("Mostrar1").disabled = false;
}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Matrícula por Créditos </b></h3></div>
<form action="Tdo5010.php" method="post"  enctype="multipart/form-data">
   <input type="hidden" name="paData[NCOSCRE]" value="{$saData['NCOSCRE']}">
   <input type="hidden" name="paData[CPERIOD]" value="{$saData['CPERIOD']}">
   <div class="container">
      <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}  
      <div id="myModal" class="modal fade">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title"><b>Generación De Pensión Por Créditos</b></h4>
               </div>
               <div class="modal-body">
                  <p><b>Solo se pueden admitir desde 0.5 hasta 11.5 créditos, en la matrícula por créditos, a partir de 12 créditos se considera una matrícula regular.</b></p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
               </div>
            </div>
         </div>
      </div>
      <div class="panel-heading"><h3 class="panel-title"><b>GENERACIÓN DE PENSIÓN POR CRÉDITOS</b></h3></div>
      <div class="panel-body">    
        <div class="col-md-12 form-line">  
          <div class="form-group">
            <table class="table table-condensed table-responsive">   
               <tr class="warning2">
                  <th colspan="2">Datos del Alumno:</th>
               </tr><tr>
                  <th><label>Código: </label></th>
                  <th><label>{$saData['CCODALU']}</label></th>
               </tr><tr>
                  <th><label>Nombres: </label></th>
                  <th><label>{$saData['CNOMBRE']}</label></th>
               </tr><tr>
                  <th><label>Unidad Académica: </label></th>
                  <th><label>{$saData['CNOMUNI']}</label></th>
               </tr><tr>
                  <th><label>Periodo Académico: </label></th>
                  <th><label>{substr_replace($saData['CPERIOD'], '-', 4, 0)}</label></th>
               </tr><tr>
                  <th><label>Créditos Registrados: </label></th>
                  <th><label>{number_format($saData['NCREREG'], 1)}</label></th>
               </tr><tr>
                  <th><label>Cantidad de Créditos: </label></th>
                  <th><select class="form-control" name="paData[NCREDIT]" onchange="f_Calculavalor()">
                        <option select="true" required> Seleccione cantidad de créditos</option>
                           {for $i = 0.5; $i <= 11.5-$saData['NCREREG']; $i=$i+0.5}
                              <option value="{$i}" class="monto" >{$i}</option>
                           {/for}
                        </select></th>
               </tr><tr>
                  <th><label>Costo por Crédito: </label></th>
                  <th>S/.<label>{$saData['NCOSCRE']}</label></th>
               </tr><tr> 
                  <th><label>Monto Total: </label></th>
                  <th>S/.<label id="Costo"></label></th>
                  <input type = "hidden" name="paData[NMONTO]">
               </tr><tr>
            </table>
          </div>
        </div>
      </div>
      </div>
      <label style="font-size:14px; text-align:center; color: #000000; font-style: italic; ">* Los créditos registrados son aquellos créditos que fueron pagados.</label>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" onclick="return confirm('¿Está seguro que desea generar su deuda?')" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
         </div>  
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu0000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 1}
      <div class="panel-heading"><h3 class="panel-title"><b>GENERACIÓN DE PENSIÓN POR CRÉDITOS </b></h3></div>
      <div class="panel-body">    
        <div class="col-md-12 form-line">  
          <div class="form-group">
          <input type="hidden" name="paData[CCODALU]" value="{$saData['CCODALU']}">
          <input type="hidden" name="paData[NSERIAL]" value="{$saData['NSERIAL']}">
          <input type="hidden" name="paData[NCOSCRE]" value="{$saData['NCOSCRE']}">
     
            <table class="table table-condensed table-responsive">   
               <tr class="warning2">
                  <th colspan="2">Datos del Alumno:</th>
               </tr><tr>
                  <th><label>Código: </label></th>
                  <th><label>{$saData['CCODALU']}</label></th>
               </tr><tr>
                  <th><label>Nombres: </label></th>
                  <th><label>{$saData['CNOMBRE']}</label></th>
               </tr><tr>
                  <th><label>Unidad Académica: </label></th>
                  <th><label>{$saData['CNOMUNI']}</label></th>
               </tr><tr>
                  <th><label>Periodo Académico: </label></th>
                  <th><label>{substr_replace($saData['CPERIOD'], '-', 4, 0)}</label></th>
               </tr><tr>
                  <th><label>Créditos Registrados: </label></th>
                  <th><label>{number_format($saData['NCREREG'], 1)}</label></th>
               </tr><tr>
                  <th><label>Cantidad de Créditos: </label></th>
                  <th><select id="mySelect" class="form-control" name="paData[NCREDIT]" disabled onchange="f_Calculavalor()">
                        {for $i = 0.5; $i <= 11.5-$saData['NCREREG']; $i=$i+0.5}
                           <option value="{$i}" {if $i eq $saData['NCREDIT']} selected {/if} class="monto">{$i}</option>
                        {/for}
                     </select></th>
               </tr><tr>
                  <th><label>Costo por Crédito: </label></th>
                  <th>S/.<label>{$saData['NCOSCRE']}</label></th>
               </tr><tr>
                  <th><label>Monto Total: </label></th>
                  <th>S/.<label>{number_format($saData['NMONTO'], 2)}</label></th>
               </tr><tr id="Mostrar" style="display: none">
                  <th><label>Nueva Monto Total: </label></th>
                  <th>S/.<label id="Costo"></label><input type = "hidden" name="paData[NMONTO]"></th>
               </tr><tr>
            </table>
          </div>
        </div>
      </div>
      </div>
      <label style="font-size:14px; text-align:center; color: #000000; font-style: italic; ">* Los créditos registrados son aquellos créditos que fueron pagados.</label>
      <div class="row">
         <div class="col-sm-3">
               <button id="Mostrar1" type="submit" onclick="return confirm('¿Está seguro que desea Actualizar la Cantidad de créditos?')" name="Boton" value="Actualizar" class="center-block btn btn-success btn-lg btn-block" disabled> Actualizar Créditos <i class="glyphicon glyphicon-refresh"></i></button>
            </div>
         <div class="col-sm-3">
            <button type="submit" onclick="return confirm('¿Está seguro que Anular la Matrícula por créditos?')" name="Boton" value="Anular" class="center-block btn btn-warning btn-lg btn-block"> Anular <i class="glyphicon glyphicon-remove"></i></button>
         </div>
         <div class="col-sm-3">
            <a onclick="f_Mostrar()" class="center-block btn btn-info btn-lg btn-block" role="button"><i class="glyphicon glyphicon-edit"></i> Editar</a>
         </div>
         <div class="col-sm-3">
            <a href="Mnu0000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 2}
      <div class="panel-heading"><h3 class="panel-title"><b>GENERACIÓN DE PENSIÓN POR CRÉDITOS </b></h3></div>
      <div class="panel-body">    
         <div class="col-md-12 form-line">  
            <div class="form-group">
            <input type="hidden" name="paData[CCODALU]" value="{$saData['CCODALU']}">
               <table class="table table-condensed table-responsive">   
                  <tr class="warning2">
                     <th colspan="2" >Datos del Alumno:</th>
                  </tr><tr>
                     <th><label>Código: </label></th>
                     <th><label>{$saData['CCODALU']}</label></th>
                  </tr><tr>
                     <th><label>Nombres: </label></th>
                     <th><label>{$saData['CNOMBRE']}</label></th>
                  </tr><tr>
                     <th><label>Unidad Académica: </label></th>
                     <th><label>{$saData['CNOMUNI']}</label></th>
                  </tr><tr>
                     <th><label>Periodo Académico: </label></th>
                     <th><label>{substr_replace($saData['CPERIOD'], '-', 4, 0)}</label></th>
                  </tr>
               </table>
            </div>
         </div>
      </div>
         <div class="panel-body">
            <div class="table-responsive mh-50">
               <table class="table table-condensed table-bordered table-hover"> 
                  <thead>
                     <tr class="warning2">
                        <th colspan="5" >Detalle de Matrícula</th>
                     </tr> 
                     <th style='text-align: center' class="col-xs-2">Fecha de Generación</th>
                     <th style='text-align: center' class="col-xs-2">Periodo</th>
                     <th style='text-align: center' class="col-xs-2">Costo por Crédito</th>
                     <th style='text-align: center' class="col-xs-3">Número de Créditos</th>
                     <th style='text-align: center' class="col-xs-4">Monto</th>
                  </thead>
                  {foreach from = $saDatos['paDetall'] item = i}
                  <tr>
                     <td style='text-align: center'>{$i['TGENERA']}</td>
                     <td style='text-align: center'>{substr_replace($i['CPERIOD'], '-', 4, 0)}</td>
                     <td style='text-align: center'>{$saData['NCOSCRE']}</td>
                     <td style='text-align: center'>{$i['NCREDIT']}</td>
                     <td style='text-align: center'>{$i['NMONTO']}</td>                           
                  </tr>
                  {/foreach}
                  <tr>
                     <td colspan="3"></td>
                     <td class="warning2" style='text-align: center'><b>Total de Créditos</b></td>
                     <td class="warning2" style='text-align: center'><b>Monto Total de matrícula</b></td>
                  </tr>  
                  <tr>
                     <td colspan="3"></td>
                     <td style='text-align: center'><b>{$saDatos['paTotal']['NCRETOT']}</b></td>
                     <td style='text-align: center'><b>{$saDatos['paTotal']['NMONTOT']}</b></td>
                  </tr> 
               </table>
              
            </div>
         </div>
          <div class="row">
            {if $i['CESTINF'] == 'A' OR $i['CESTINF'] == 'E' }
               <div class="col-sm-4" >
                  <button type="submit" name="Boton" value="Nuevo" class="center-block btn btn-success btn-lg btn-block"> Ampliar <i class="glyphicon glyphicon-plus-sign"></i></button>
               </div>
            {elseif $i['CESTINF'] == 'M'}
               <div class="col-sm-4">
                  <button type="submit" name="Boton" value="Nuevo" class="center-block btn btn-warning btn-lg btn-block"> Retificar <i class="glyphicon glyphicon-edit"></i></button>
               </div>  
            {/if}
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <a href="Mnu0000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
            </div>
      </div>
      
      {/if}
      </div> 
      </div>      
    </div>    
  </div>
</form>
<div id="footer">
</div>
</body>
</html>
