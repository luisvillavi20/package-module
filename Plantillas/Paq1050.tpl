<html>   
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
	<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
      


	</script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Seguimiento de Trámites </b></h3></div>
<div class="container-fluid divBody">
<form action="Paq1050.php" method="post">
   {if $snBehavior == 0}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITES</b></h3></div>
   <div class="panel-body">
      <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>       
      <div><tr><td colspan = "3"><label style="font-size:14px; text-align:left; color: #9B0000; font-style: italic; ">* Lamentamos informarles que el servicio de envío de correos está fuera de servicio por el momento, motivo por el cual no podemos informarles personalmente de la fecha y hora de su colación.
Para saber si entra a la colación siguiente basta con tener las tres columnas de revisión aprobadas.
La fecha de la siguiente colación es el día 23 de Enero para bachiller, y 22 de Enero para Titulación
. Cualquier consulta mande un mensaje al facebook de la Universidad.
</label></td></tr></div>
      <div class="table-responsive mh-50">
      <table class="table table-condensed">
         <thead class="warning2"> 
         <!-- <thead> -->
            <th style='text-align: center' class="col-xs-4">Tipo Documento</th>
            <th style='text-align: center' class="col-xs-1">Nro Expediente</th>
            <th style='text-align: center' class="col-xs-2">Fecha de Envio</th>  
            <th style='text-align: center' class="col-xs-1">Estado Escuela</th>
            {if $saData['CCODIGO'] == 'B'}       
            <th style='text-align: center' class="col-xs-1">Estado Docentes</th>       
            {/if}
            <th style='text-align: center' class="col-xs-1">Estado ORAA</th>       
            <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
         </thead>
         {foreach from = $saDatos item = i}  
            <tr>         
            <td style='text-align: left' class="col-xs-4">{$i['CDESDOC']}</td>
            <td style='text-align: center' class="col-xs-1">E-{$i['CCODTRE']}</td>
            <td style='text-align: center' class="col-xs-2">{$i['TFECREC']}</td>
            <td style='text-align: center' class="col-xs-1">
               {if ($i['CESTDTR'] !=  null and ($i['CESTMTR'] == 'B' or $i['CESTMTR'] != 'S') and ($i['CETAPA'] == 'A'))}
                  {if $i['CESTDTR'] == 'A'}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTDTR'] == 'B' or $i['CESTMTR'] == 'E'}  
                     <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                  {elseif $i['CESTDTR'] == 'F' or $i['CESTMTR'] == 'F'}  
                     <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                  {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M'}  
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Mesa de partes">
                  {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S'}  
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
               {elseif $i['CETAPA'] == 'A' }
                  {if $i['CESTMTR'] == 'E'}   
                     <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                  {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'CCCONB' OR ($i['CIDCATE'] != 'CCCSID' OR $i['CIDCATE'] != 'CCCOND' OR $i['CIDCATE'] == 'PQ0033' or $i['CIDCATE'] == 'PDADOC' or $i['CIDCATE'] == 'PDAOTR'))}  
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
                  {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 2) == 'CC' and ($i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCESTU')}
                     <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                  {elseif $i['CESTMTR'] == 'M' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND')}
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Mesa de partes">
                  {elseif $i['CESTMTR'] == 'S' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == '000084' or $i['CIDCATE'] == 'PDADOC'or $i['CIDCATE'] == 'PDAOTR')}
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Archivo Subido">
                  {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 1) == 'P'}
                     <img src="Images/subir.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Subir Archivo">      
                  {elseif $i['CESTMTR'] == 'R' and $i['CIDCATE'] == 'PQ0033'}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Ultima Revision">      
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
               {elseif $i['CETAPA'] == 'B' or $i['CETAPA'] == 'C' or $i['CETAPA'] == 'D' }
                  <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
               {/if}
               </td>
               {if $saData['CCODIGO'] == 'B'}
               <td style='text-align: center' class="col-xs-1">
               {if ($i['CESTDTR'] !=  null and ($i['CESTMTR'] == 'B' or $i['CESTMTR'] != 'S') and $i['CETAPA'] == 'B')}
                  {if $i['CESTDTR'] == 'A' }
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTDTR'] == 'B' or $i['CESTMTR'] == 'E'}  
                     <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                  {elseif $i['CESTDTR'] == 'F' or $i['CESTMTR'] == 'F' }  
                     <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                  {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M' }  
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S' }  
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
               {elseif $i['CETAPA'] == 'B'}
                  {if $i['CESTMTR'] == 'E'}   
                     <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                  {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'CCCONB' OR ($i['CIDCATE'] != 'CCCSID' OR $i['CIDCATE'] != 'CCCOND' OR $i['CIDCATE'] == 'PQ0033'))}  
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDADOC' or $i['CIDCATE'] == 'PDAOTR')}
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">
                  {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 2) == 'CC' and ($i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCESTU')}
                     <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                  {elseif $i['CESTMTR'] == 'M' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND')}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTMTR'] == 'S' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND'or $i['CIDCATE'] == '000084' or $i['CIDCATE'] == 'PDADOC'or $i['CIDCATE'] == 'PDAOTR')}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 1) == 'P'}
                     <img src="Images/subir.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Subir Archivo">      
                  {elseif $i['CESTMTR'] == 'R' and $i['CIDCATE'] == 'PQ0033'}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Ultima Revision">      
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
               {elseif $i['CETAPA'] == 'C' or $i['CETAPA'] == 'D' }
                  <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">  
               {else}
                  <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               {/if}
               </td>
               {/if}
               <td style='text-align: center' class="col-xs-1">
               {if ($i['CESTDTR'] !=  null and ($i['CESTMTR'] == 'B' or $i['CESTMTR'] != 'S') and $i['CETAPA'] == 'D')}
                  {if $i['CESTDTR'] == 'C' }
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTMTR'] == 'S' and ($i['CIDCATE'] == 'PDADOC'or $i['CIDCATE'] == 'PDAOTR' )}
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">   
                  {elseif $i['CESTDTR'] == 'B' or $i['CESTMTR'] == 'E'}  
                     <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                  {elseif $i['CESTDTR'] == 'F' or $i['CESTMTR'] == 'F' }  
                     <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                  {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M' }  
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'S' }  
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {else}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {/if}
               {elseif $i['CETAPA'] == 'C'}
                  {if $saData['CCODIGO'] == 'T'}
                        <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">         
                  {elseif $i['CESTMTR'] == 'E'}   
                     <img src="Images/eye.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                  {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'CCCONB' OR ($i['CIDCATE'] != 'CCCSID' OR $i['CIDCATE'] != 'CCCOND' OR $i['CIDCATE'] == 'PQ0033'))}  
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'PDADOC' or $i['CIDCATE'] == 'PDAOTR')}
                     <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Revisado">
                  {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 2) == 'CC' and ($i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCESTU')}
                     <img src="Images/llenar.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                  {elseif $i['CESTMTR'] == 'M' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND')}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTMTR'] == 'S' and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == '000084' or $i['CIDCATE'] == 'PDADOC'or $i['CIDCATE'] == 'PDAOTR')}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                  {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 1) == 'P'}
                     <img src="Images/subir.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Subir Archivo">      
                  {elseif $i['CESTMTR'] == 'R' and $i['CIDCATE'] == 'PQ0033'}
                     <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Ultima Revision">      
               {else}
                  <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               {/if}
            {elseif $i['CETAPA'] == 'D' }
               <img src="Images/aprobado.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
            {else}
               <img src="Images/clock.png" width="27%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
            {/if}
            </td>
            <td style='text-align: center' class="col-xs-1">            
            <input type="radio" name="pcCodtre"  value="{$i['CCODTRE']}"/>
            </td>
            </tr>
         {/foreach}
      </table>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-4">
         <button type="submit" name="Boton" value="Seguimiento" class="center-block btn btn-primary btn-lg btn-block"> Seguimiento <i class="glyphicon glyphicon-forward"></i></button><br>
      </div>  
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
         <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div> 
   </div> 
   </div> 
   </div>
   {elseif $snBehavior == 1}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL TRÁMITE</b></h3></div>
      <div class="panel-body">  
      <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>     
      <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
      <div class="form-group">
      <table class="table table-striped">   
         <tr class="info">
            <th colspan="2">Datos del Trámite:</th>
         </tr><tr>
            <th class="col-xs-3"><label>Tipo de Trámite: </label></th>
            <th><label>{$saDatos[0]['CDESCRI']}</label></th>
         </tr><tr>
            <th class="col-xs-3"><label>Observaciones: </label></th>
            {if $saDatos[0]['MOBSERM'] != null}
               <th><label>{$saDatos[0]['MOBSERM']}</label></th>
            {elseif isset($saDatos[0]['MOBSERD']['MOBSERV']) != null}
               <th><label>{$saDatos[0]['MOBSERD']['MOBSERV']}</label></th>
            {elseif $saDatos[0]['MOBSERM'] == null or $saDatos[0]['MOBSERD']['MOBSERV']== null }
               <th><label> </label></th>
            {/if}
         </tr><tr>   
      </table>
      </div>      
      <br>
      <div class="table-responsive">
      <table class="table table-hover">
         <tr class="info">
               <th colspan="5">Ubicación del Documento:</th>
            </tr>
         <th class="col-xs-2" style='text-align: center'>Recepción</th>  
         <th class="col-xs-2" style='text-align: center'>Envio/Aprobación</th> 
         <th class="col-xs-4" style='text-align: center'>Unidad</th>     
         <th class="col-xs-1" style='text-align: center'>Estado</th>            
            {foreach from = $saDatos item = i} 
            <tr>
               <td style='text-align: center'>{$i['TRECIBI']}</td>
               <td style='text-align: center'>
               {if $i['TULTIMA'] == null }
                  <span class="label label-default">-----</span>
               {else}
                  {$i['TULTIMA']}
               {/if}
               </td>
               <td style='text-align: center'>{$i['CDESCRI']}</td> 
               <td style='text-align: center'><h4>
               {if $i['CESTDTR'] != null}
                  {if $i['CESTDTR'] == 'A' }
                     <span class="label label-default">PENDIENTE</span>                    
                  {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] != 'M' }
                     <span class="label label-success">REVISADO</span>
                  {elseif $i['CESTDTR'] == 'C' and $i['CESTMTR'] == 'M' }
                     <span class="label label-success">VISTO - APROBADO</span>
                  {elseif $i['CESTDTR'] == 'D' and $i['CESTMTR'] == 'S' }
                     <span class="label label-success">VISTO - APROBADO</span>
                  {elseif $i['CESTDTR'] == 'B' }  
                     <span class="label label-warning">VISTO - OBSERVADO</span>
                  {elseif $i['CESTDTR'] == 'F' }  
                     <a class="label label-danger" href="Paq1030.php">LLENAR FORMULARIO</a>
                  {else}
                     <span class="label label-default">PENDIENTE</span>
                  {/if}
               {else}
                  {if $i['CESTMTR'] == 'A' and ($i['CIDCATE'] != 'CCCOND' or $i['CIDCATE'] == 'CCCONB')}
                     <span class="label label-default">PENDIENTE</span> 
                  {elseif $i['CESTMTR'] == 'F' and ($i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == 'CCCONB')}
                     <span class="label label-default">PENDIENTE</span> 
                  {elseif $i['CESTMTR'] == 'B' }
                     <span class="label label-success">EN REVISIÓN</span>
                  {elseif $i['CESTMTR'] == 'E' }  
                     <span class="label label-warning">VISTO - OBSERVADO</span>
                  {elseif  $i['CESTMTR'] == 'F' }  
                     <a class="label label-danger" href="Paq1070.php">SUBIR ARCHIVO</a>
                  {elseif  $i['CESTMTR'] == 'M' }  
                     <span class="label label-success ">VISTO - APROBADO</span> 
                  {else}
                     <span class="label label-default">PENDIENTE</span>
                  {/if}
               {/if}
               </h4></td>
            </tr>  
         {/foreach}
      </table> 
      <br>
         {if $i['CESTMTR'] == 'M' or $i['CESTMTR'] == 'S' and ($i['CIDCATE'] == 'CCESTU' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCSUC')}
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
               {if $i['CESTPRO'] == 'R'}
                  <a class="btn btn-info btn-block btn-lg" onClick="window.open('{$i['CDOCDIG']}', 'Algo', 'height=850 ,width=850');">Ver Documento</a>
               {else}
                  <a class="btn btn-info btn-block btn-lg" href="DocumentoPaquete.php?CNRODNI={$saData['CNRODNI']}&CCODTRE={$saData['CCODTRE']}&download=true">Descargar</a>
               {/if}
            </div>
            <div class="row col-md-10 col-md-push-1">
               <iframe id="frameDocumento" style="width: 100%; height:80vh; display: none" src="DocumentoPaquete.php?CNRODNI={$saDatos['CNRODNI']}&CCODTRE={$saDatos['CCODTRE']}"></iframe>
            </div>
         {/if}
         <label style="font-size:14px; text-align:center; color: #000000; font-style: italic; ">* Una vez culminado el trámite recibira sus certificados el dia de su colación. En el caso de el certificado de estudios solo se le hara entrega, si es que lo solicitó en físico.</label>
      </div>         
   </div>
   </div>
   <div class="row col-md-4 col-md-push-8">     
   <a href="Paq1050.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
   </div>
   </div>
   </div> 
   </div>
   {elseif $snBehavior == 2}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DE TRÁMITE</b></h3></div>
      <div class="panel-body">  
         <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>     
      <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
         {foreach from = $saDatos item = i} 
            <div class="form-group">
               <table class="table table-striped">   
               <tr class="info">
                  <th colspan="2">Datos del Trámite</th>
               </tr><tr>
                  <th class="col-xs-3"><label>Tipo de Trámite: </label></th>
                  <th><label>{$saDatos[0]['CDESCRI']}</label></th>
               </tr><tr>
                  <th class="col-xs-3"><label>Codigo Usuario: </label></th>
                  <th><label>{$i['CCODUSU']}</label></th>
               </tr><tr>
                  <th class="col-xs-3"><label>Fecha de termino: </label></th>
                  <th><label>{$i['TRECIBI']}</label></th>
               </tr>
               </table>
            </div>  
         {/foreach}     
      </div>
   </div>   
   <div class="row">
      <div class="col-sm-4">
         <a class="btn btn-info btn-block btn-lg" href="DocumentoPaquete.php?CNRODNI={$saData['CNRODNI']}&CCODTRE={$saData['CCODTRE']}&download=true">Descargar</a>
      </div>
      <div class="col-sm-4"></div>
      <div class="col-sm-4">
         <a href="Paq1050.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
      </div>
   </div> 
   {elseif $snBehavior == 3}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL TRÁMITE</b></h3></div>
      <div class="panel-body">  
         <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>     
      <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
         {foreach from = $saDatos item = i} 
            <div class="form-group">
               <table class="table table-striped">   
               <tr class="info">
                  <th colspan="2">Datos del Trámite</th>
               </tr><tr>
                  <th class="col-xs-3"><label>Tipo de Trámite: </label></th>
                  <th ><label>{$saDatos[0]['CDESCRI']}</label></th>
               </tr><tr>
                  <th class="col-xs-3"><label>Codigo Usuario: </label></th>
                  <th><label>{$i['MDETALL']['CCODUSU']}</label></th>
               </tr><tr>
                  <th class="col-xs-3"><label>Fecha de termino: </label></th>
                  <th><label>{$i['MDETALL']['DVENCIM']}</label></th>
               </table>
            </div>  
         {/foreach}    
      </div>
   </div>   
   <div class="row">
      <div class="col-sm-4">
         <a class="btn btn-info btn-block btn-lg" onClick="window.open('{$i['MDETALL']['CURLPDF']}', 'Algo', 'height=850 ,width=850');">Ver Documento</a>
      </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
         <a href="Paq1050.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
      </div>
   </div> 
   {elseif $snBehavior == 4}
   <div class="container-fluid">
   <div class="row col-md-10 col-md-push-1"> 
   <div class="panel panel-default">  
   <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO DEL ARCHIVO</b></h3></div>
      <div class="panel-body">  
         <p class="text-muted"><b>ALUMNO: {$saData['CNOMBRE']}</b></p>     
         <!--<p class="text-muted"><b>ESCUELA: {$saData['CNOMUNI']}</b></p>-->
         <div class="form-group">
            <table class="table table-striped">   
               <tr class="info">
                  <th colspan="2">Datos del Trámite:</th>
               </tr><tr>
                  <th><label>Tipo de Trámite: </label></th>
                  <th><label>{$saDatos[0]['CDESDOC']}</label></th>
               </tr><tr>
                  <th><label>Observaciones: </label></th>
                  <th><label>{$saDatos[0]['MOBSERV']}</label></th>
               </tr><tr>   
            </table>
         </div>      
         <br>
         <div class="table-responsive">
         <table class="table table-hover">
            <tr class="info">
                  <th colspan="5">Ubicación del Documento:</th>
            </tr>
            <th class="col-xs-2" style='text-align: center'>Recepción</th>  
            <th class="col-xs-2" style='text-align: center'>Envio/Aprobación</th> 
            <th class="col-xs-2" style='text-align: center'>Responsable</th>
            <th class="col-xs-4" style='text-align: center'>Unidad</th>     
            <th class="col-xs-2" style='text-align: center'>Estado</th>            
               {foreach from = $saDatos item = i} 
               <tr>
                  <td style='text-align: center'>{$i['TFECREC']}</td>
                  <td style='text-align: center'>
                  {if $i['TFECFIN'] == '' }
                     <span class="label label-default">-----</span>
                  {else}
                     {$i['TFECFIN']}
                  {/if}
                  </td>
                  <td style='text-align: center'>{$i['CNOMPER']}</td>
                  <td style='text-align: center'>{$i['CDESCCO']}</td> 
                  <td style='text-align: center'><h4>
                  {if $i['CESTDTR'] == 'A' }
                  <span class="label label-default">PENDIENTE</span> 
                  {elseif $i['CESTDTR'] == 'C' }
                     <span class="label label-success">VISTO - APROBADO</span>
                  {elseif $i['CESTDTR'] == 'B' }  
                     <span class="label label-warning">VISTO - OBSERVADO</span>
                  {elseif $i['CESTDTR'] == 'X' }  
                     <span class="label label-danger">VISTO - ANULADO</span>
                  {/if}
                  </h4></td>
               </tr>  
            {/foreach}
         </table> 
         </div>     
      </div>
      </div>
   <div class="row col-md-4 col-md-push-8">     
   <a href="Paq1050.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>          
   </div>
   </div>
   </div> 
   </div>
   {/if} 
   </div>
   </div> 
   </div>
</form>
<div id="footer">
</div>
</body>
</html>