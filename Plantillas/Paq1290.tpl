<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip();
         if ($("#llModal").val() == 0) {
            $('#modalReq').modal('show');
         }         
      });
   </script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos - Documentos Observados </b></h3></div>
<div class="container-fluid divBody table-responsive">
<form action="Paq1290.php" method="post" enctype="multipart/form-data">
   <div class="container divBody">
      <div class="row">
         <div class="col-sm-12">
         <!--<div class="row col-md-12 col-md-push-1">-->
         <!--<div class="panel panel-default">-->
         <div class="panel panel-success">
         {if $snBehavior == 0}
         <div class="panel-heading"><h3 class="panel-title"><b>DOCUMENTOS OBSERVADOS</b></h3></div>
         <div class="table table-condensed table-responsive">
            <table class="table table-condensed">
               <thead>
               <th style="text-align: left" class="col-xs-1"><label>DNI</label></th> 
               <th style="text-align: left" class="col-xs-4"><label>Nombre</label></th>
               <th style="text-align: center" class="col-xs-3"><label>Unidad Académica</label></th>
               <th style="text-align: center" class="col-xs-2"><label>Seleccionar Archivo</label></th>
               <th style="text-align: center" class="col-xs-1"><label>Subir Archivo</label></th>
               <th style="text-align: center" class="col-xs-1"><label>Estado</label></th>
               </thead>
               <tbody>
               {$j=0}
               {foreach from = $saDatos item = i}  
                  <form action="Paq1290.php" method="POST" enctype="multipart/form-data">
                  <tr>
                     <td style="text-align: left">{$i['CNRODNI']}</td>
                     <td style="text-align: left">{$i['CNOMBRE']}</td>
                     <td style="text-align: center">{$i['CNOMUNI']}</td>        
                     <td style="text-align: center">
                     <input type="hidden" name="paData[CNRODNI]" value="{$i['CNRODNI']}">
                     <input type="hidden" name="paData[CCODTRE]" value="{$i['CCODTRE']}">
                     <input name="pfDocBac" accept=".pdf, .jpg" type="file" class="form-control-file" required>
                     </td>
                     <td>
                     <button type="submit" name="Boton" value="Subir" onclick="confirm('¿Está seguro que desea subir éste archivo? Esta operación no podrá ser revertida.')" class="center-block btn"><i class="glyphicon glyphicon-upload"></i> Subir</button>
                     </td>
                     <td style="text-align: center">
                     {if $i['CESTADO'] == 'E'}
                        <img src="Images/eye.png" width="35%" data-toggle="tooltip" data-placement="button" title="Observado">
                     {else}
                        <img src="Images/subido.png" width="30%" data-toggle="tooltip" data-placement="button" title="Subido">
                     {/if}
                     </td>
                  </tr>
                  </form>
               {/foreach} 
               </tbody>        
         </table>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-4"></div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
  </div>
  {/if}
</form>
</div>

<div id="footer">
</div>
</body>
</html>