<html>
<head>
   <title>REPORTE DE TRÁMITES</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>     
</head>
<div class="panel-heading" style="background-color: #245433; color: #ffffff;"><h3><b>Trámite Académico - Administrativo</b></h3></div>
<form action="Paq2600.php" method="post" enctype="multipart/form-data"> 
<div class="container-fluid">
   <div class="row">
      {if $snBehavior == 0}
      <!--<div class="col-sm-2"></div>-->
      <div class="col-sm-12">
         <div class="panel with-nav-tabs panel-success">
            <div class="panel-heading"><h4>Reporte de Trámites Finalizados</h4></div>
            <div class="panel-body">
               <p class="text-muted"><b>{$saData['CNOMBRE']} </b></p> 
               <select class="selectpicker form-control" data-live-search="true" name="paIdCate">
                  {foreach from = $saIdCate item = i}  
                     <option value="{$i['CIDCATE']}">{$i['CIDCATE']} - {$i['CDESCRI']}</option>  
                  {/foreach}
               </select> 
               <div class="row">
                  <div class="col-sm-6">
                     <button type="submit" name="Boton" value="Generar" class="center-block btn btn-success btn-lg btn-block"> Generar Reporte <i class="glyphicon glyphicon-ok"></i></button>
                  </div> 
                  <div class="col-sm-6">
                     <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
                  </div>
               </div>      
            </div>            
         </div>         
      </div>           
      {/if}
      <div class="col-sm-2"></div>      
   </div>
</div>
</form>
</html>