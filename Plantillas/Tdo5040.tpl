<html>
<head>
   <title>Trámites Administrativos</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      $(document).ready(function(){
         //$("#myModal").modal('show');
      });

      var paCodCur = {};
      var pnTotal  = 6.00;

      function f_agregarCurso() {
         let loCur = document.querySelector('[name="paData[CCODCUR]"]');
         let loOpt = loCur.options[loCur.selectedIndex];
         let lcDescri = loOpt.getAttribute('cDescri');
         let lnCredit = loOpt.getAttribute('nCredit');
         let lnMonto  = parseFloat(loOpt.getAttribute('nMonto'));
         let lcCodCur = loOpt.value;
         let nCosCre = $('[name="paData[NCOSCRE]"]').val();
         let t_cursos = document.getElementById('t_cursos');

         if (paCodCur[lcCodCur]) {
            alert('Curso ya seleccionado.');
            return;
         }

         if (t_cursos.childElementCount >= 2) {
            alert('Solo se admiten hasta 2 cursos por jurado.');
            return;
         }

         let loFila = document.createElement("tr");

         let loDescri = document.createElement("td");
         let loCodCur = document.createElement("input");
         loCodCur.type = "hidden";
         loCodCur.name = "paData[CCODCUR][]";
         loCodCur.value = lcCodCur;
         loDescri.appendChild(loCodCur);
         loDescri.innerHTML += lcDescri;
         loFila.appendChild(loDescri);

         let loCredit = document.createElement("td");
         loCredit.innerHTML = lnCredit;
         loFila.appendChild(loCredit);

         let loMonto = document.createElement("td");
         loMonto.innerHTML = "S/." + lnMonto;
         loFila.appendChild(loMonto);

         let loBorrar = document.createElement("td");
         loBorrar.style.textAlign = "center";
         let loBtnBor = document.createElement("button");
         loBtnBor.className = "btn btn-danger";
         loBtnBor.textContent = "X";
         loBtnBor.onclick = function() {
            t_cursos.removeChild(loFila);
            pnTotal -= paCodCur[lcCodCur];
            $("#r_total").html("Total: S/." + pnTotal.toFixed(2));
            delete paCodCur[lcCodCur];
         };
         loBorrar.appendChild(loBtnBor);
         loFila.appendChild(loBorrar);
         
         t_cursos.append(loFila);
         pnTotal += lnMonto;
         $("#r_total").html("Total: S/." + pnTotal.toFixed(2));
         paCodCur[lcCodCur] = lnMonto;
      }

      function f_changeCurso(self) {
         let loOpt = self.options[self.selectedIndex];
         let nCredit = loOpt.getAttribute('nCredit');
         let nCosCre = $('[name="paData[NCOSCRE]"]').val();
         $("#nNumCre").html(nCredit);
         $("#nCosTot").html("S/." + (nCredit * nCosCre));
      }
      
      function f_init() {         
         MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
         var observer = new MutationObserver(function(mutations, observer) {
            // fired when a mutation occurs
            if (mutations.length == 1) {
               alert('Se modificó un valor del sistema, éste caso será reportado');
               console.log(mutations, observer);
            }
         });
         observer.observe(document, {
            subtree: true,
            attributeFilter: ['ncredit', 'value']
         });
      }
   </script>
</head>
<body class="divBody" onload="f_init()">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80"> Trámites Administrativos</b></h3></div>
<div class="container-fluid">
<div class="row">
<div class="col-md-8 col-md-push-2">
<form action="Tdo5040.php" method="POST">
   <input type="hidden" name="paData[NCOSCRE]" value="{$saData['NCOSCRE']}">
   <div class="panel panel-success">
   {if $snBehavior == 0}
      <div class="panel-heading">
         <h3 class="panel-title"><b>CURSOS POR JURADO</b></h3>
      </div>
      <div class="table-responsive">
            <table class="table table-condensed">
              <tr>
              <th><label> Código Alumno:</label></th>
              <th colspan="1"><label>{$saData['CCODALU']}</label></th>
              <th><label></label></th>
              <th colspan="1"><label>{$saData['CNOMBRE']}</label></th>
              <th><label>Escuela:</label></th>
              <th colspan="2"><label>{$saData['CUNIACA']}</label></th>
              <th><label></label></th>
              <th colspan="2"><label>{$saData['CNOMUNI']}</label></th>
              </tr><tr>
              <th>TELÉFONO:</th>
              <td colspan="8"><input type="text" class="form-control btn-lg" maxlength="9" name="paData[CNROCEL]" value="{$saData['CNROCEL']}" placeholder="Número Telefónico" title="Ingrese su Numero Telefónico" minlength="9" required autofocus/></td>
              </tr><tr>
              <th>CORREO:</th>
              <td colspan="8"><input type="text" class="form-control btn-lg" maxlength="50" name="paData[CEMAIL]" value="{$saData['CEMAIL']}" placeholder="Correo Electrónico" title="Ingrese su Correo Electrónico" required/></td>
              </tr><tr>
              <td colspan="9"><label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Los datos personales solicitados, serán utilizados para contactarlos en caso de algun inconveniente.</label></td>
              </tr>
            </table>
      </div>
      <div class="panel-heading">
         <h3 class="panel-title"><b>CURSOS POR JURADO</b></h3>
      </div>
      <div class="panel-body">
         {if count($saDatos['paInscr']) == 0}
            <table class="table">
               <tr>
               <th>Alumno</th>
               <td>{$saData['CNRODNI']} - {$saData['CCODALU']} - {$saData['CNOMBRE']}</td>
               </tr><tr>
               <th>Curso</th>
               <td>
                  <select class="form-control selectpicker" name="paData[CCODCUR]" data-live-search="true" onchange="f_changeCurso(this)" required>
                     <option value="" disabled selected>Seleccione un curso</option>
                     {foreach from = $saDatos['paCursos'] item = i}
                        <option nMonto="{$i['NMONTO']}" nCredit="{$i['NCREDIT']}" cDescri="{$i['CDESCRI']}" value="{$i['CCODCUR']}">{$i['CCODCUR']} - {$i['CDESCRI']} ({$i['NCREDIT']})</option>
                     {/foreach}
                  </select>
               </td>               
               </tr><tr>
               <td colspan="2"><button type="button" style="float: right" class="btn btn-success" onclick="f_agregarCurso();">Agregar</button></td>
               </tr>
            </table>
         {/if}  
         <div class="table-responsive">
            <table class="table table-hover">
               <thead>
                  <tr>
                  <th class="col-sm-7">Nombre del curso</th>
                  <th class="col-sm-2">Cant. Créditos</th>
                  <th>Monto</th>
                  {if count($saDatos['paInscr']) != 0}
                     <th class="col-sm-3">Fecha de solicitud</th>
                     <th>Estado</th>
                  {else}
                     <th>Eliminar</th>
                  {/if}
                  </tr>
               </thead>
               <tbody id="t_cursos">
                  {$lnTotal = 6.00}
                  {foreach from = $saDatos['paInscr'] item = i}                  
                     <tr>
                     <td>{$i['CDESCRI']}</td>
                     <td>{$i['NCREDIT']}</td>
                     <td>S/.{$i['NMONTO']}</td>
                     <td>{$i['DGENERA']}</td>
                     <td style='text-align: center'>
                        {if $i['CESTADO'] == 'P'}
                           {$lnTotal = $lnTotal + $i['NMONTO']}
                           <span class="label label-default">PENDIENTE</span>                    
                        {elseif $i['CESTADO'] == 'A'}
                           {$lnTotal = $lnTotal + $i['NMONTO']}
                           <span class="label label-success">APROBADO</span>
                        {elseif $i['CESTADO'] == 'D'}
                           <a class="label label-danger">RECHAZADO</a>
                        {elseif $i['CESTADO'] == 'X'}
                           <span class="label label-warning">ANULADO</span>
                        {/if}
                     </td>
                     <td style='text-align: center' class="col-xs-1">            
                        <input type="radio" name="paData[NSERIAL]"  value="{$i['NSERIAL']}"/>
                     </tr>
                  {/foreach}
               </tbody>
            </table>
         </div>
         <div class="table-responsive">
            <label style="float: right; text-align: right"><h4>Derecho de trámite: S/.6.00</h4>
            {if !empty($saData['CNROPAG'])}
               <!--<h4>Código de pago: {$saData['CNROPAG']}</h4>-->
            {/if}
            <h3 id="r_total">Total: S/.{number_format($lnTotal, 2)}</h3></label>
         </div>
         {if !empty($saData['CNROPAG'])}
            <div class="table-responsive">
               <div class="alert alert-success" align="center">
                  <h3><strong>¡Deuda Provisional Generada!</strong><br>Su ID de pago es: <br><strong>{$saData['CNROPAG']}</strong><br></h3>
                  <h3><strong>Indicar que el pago es por pensiones</strong></h3>
               </div>
            </div>
         {/if}
            <div class="alert alert-warning" align="left">
               <h4><u><strong>Tiempo disponible de deuda provisional en Bancos:</strong></u></h4> 
               <h5>Cuando la solicitud sea aprobada, el ID de pago estará disponible solo por 7 días. </h5>   
            </div>
            <div class="alert alert-danger" align="left">
               <h4><u><strong>Costos de los cursos por jurado:</strong></u></h4> 
               <h5>Los costos mostrados son referenciales a la tasa educativa vigente. Tenga en cuenta la fecha en la que realizará el pago.</h5>   
            </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-3">
         {if count($saDatos['paInscr']) == 0}
            <button class="btn btn-success btn-block btn-lg" name="Boton" value="Grabar" onclick="return confirm('¿Está seguro?')">Solicitar</button>
         {/if}
      </div>
      <div class="col-md-3">
         <button class="btn btn-primary btn-block btn-lg" name="Boton" value="Historial" formnovalidate>Historial de Sol.</button>
      </div>
      <div class="col-md-3">
         <button class="btn btn-info btn-block btn-lg" name="Boton" value="Seguimiento">Seguimiento</button>
      </div>
      <div class="col-md-3">
         <a href="Mnu0000.php" class="btn btn-danger btn-block btn-lg"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
      </div>
   </div>
</form>
{elseif $snBehavior == 1}
   <div class="panel-heading"><h3 class="panel-title"><b>SEGUIMIENTO</b>
      <span style="float:right"><b>ALUMNO: </b>{$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <div class="table-responsive">
      <div class="form-group">
      <table class="table table-striped">
         <thead>
         <tr>
            <th class="col-xs-2" style='text-align: center'>Cód. del Curso</th> 
            <th class="col-xs-3" style='text-align: center'>Nombre del Curso</th>
            <th class="col-xs-2" style='text-align: center'>Escuela Encargada</th>
            <th class="col-xs-1" style='text-align: center'>Estado</th>
         </tr>
         </thead>
         <tr>
            <td style='text-align: center'>{$saDatos['paCursos']['CCODCUR']}</td>
            <td style='text-align: center'>{$saDatos['paCursos']['CDESCRI']}</td>
            <td style='text-align: center'>{$saDatos['paCursos']['CDESCRIE']}</td>
            <td style='text-align: center'><h4>
            {if $saDatos['paCursos']['CESTADO'] != null}
               {if $saDatos['paCursos']['CESTADO'] == 'P' }
                  <span class="label label-default">PENDIENTE</span>                    
               {elseif $saDatos['paCursos']['CESTADO'] == 'A' }
                  <span class="label label-success">APROBADO</span>
               {elseif $saDatos['paCursos']['CESTADO'] == 'D' }
                  <span class="label label-danger">DENEGADO</span>
               {/if}
            {/if}</h4>   
         </tr>
      </table>
      </div>
      <div class="form-group">
         <thead><label>Docentes Revisores: </label></thead>
         <table class = "table table-striped">
            <tr>
               <th class="col-xs-3" style='text-align: center'>Nombre del Docente</th>
            </tr>
            <tr colspan="2">
               <ul> 
               <tr>
                  {if $saDatos['paDocentes'][0]['CCODJU1'] == '0000'}
                     <td class="col-xs-2" style='text-align: left'>DOCENTE SIN ASIGNAR</td>
                  {else}
                     <td class="col-xs-2" style='text-align: left'>{$saDatos['paDocentes'][0]['CNOMJU1']}</td>
                  {/if}      
               </tr>
               </ul>
            </tr>
            <tr colspan="2">
               <ul>
               <tr>
                  {if $saDatos['paDocentes'][0]['CCODJU2'] == '0000'}
                     <td class="col-xs-2" style='text-align: left'>DOCENTE SIN ASIGNAR</td>
                  {else}
                     <td class="col-xs-2" style='text-align: left'>{$saDatos['paDocentes'][0]['CNOMJU2']}</td>
                  {/if}      
               </tr>
               </ul>
            </tr>
         </table>
      </div>
      <div class="form-group">
         <thead><label>Observaciones: </label></thead>
         <table class = "table table-striped">
            <tr colspan="2">
               <td style = "text-align: left">{$saDatos['paCursos']['MOBSERV']}</td>
            </tr>
         </table> 
      </div>
      </div>
   <div class="col-sm-4"></div>
   <div class="col-sm-4"></div>   
   <div class="col-sm-4">
      <a href="Tdo5040.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
   </div>
   </div>
   {elseif $snBehavior == 2}
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
         <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <div class="table-responsive mh-50">
         <table class="table table-condensed">
            <thead>
               <tr>
                  <th class="col-xs-2" style='text-align: left'>Fecha Recepción</th>
                  <th class="col-xs-1" style='text-align: left'>Cod. Alumno</th>
                  <th class="col-xs-3" style='text-align: left'>Nombre Alumno</th>
                  <th class="col-xs-1" style='text-align: left'>Cod. Trámite</th>
                  <th class="col-xs-3" style='text-align: left'>Descripción de Trámite</th>
                  <th class="col-xs-1" style='text-align: left'>Estado</th>
                  <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
               </tr>
            </thead>
               {foreach from = $saDatos item = i}
               <tr onclick="f_seleccionarFila(this)" class = "Datos">
                 <td style='text-align: left'>{$i['TMODIFI']}</td>
                 <td style='text-align: left'>{$i['CCODALU']}</td>
                 <td style='text-align: left'>{$i['CNOMBRE']}</td>
                 <td style='text-align: left'>E-{$i['CCODTRE']}</td>
                 <td style='text-align: left'>{$i['CDESCRI']}</td>
                 <td style='text-align: left'>
                     {if $i['CESTADO']=='E'}   
                     <img src="Images/eye.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                     {else if $i['CESTADO']=='B'}
                     <img src="Images/aprobado.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Cerrado">
                     {else}
                     <img src="Images/clock.png" width="28%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                     {/if}
                  </td>
                 <td style='text-align: center' class="col-xs-1">            
                     <input class = "Aprob" type="radio" name="paData[CCODTRE]"  value="{$i['CCODTRE']}"required/>
                     <input class = "IdCate" type="hidden" value="{$i['CIDCATE']}"/>
                     <input type="hidden"  name = "paData[CIDCATE]" value="" />
                  </td>
               </tr>
               {/foreach}
         </table>
      </div>
   </div>
   </div>
   {elseif $snBehavior == 3}
   <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
         <span style="float:right"><b>ENCARGADO: </b>{$saData['CNOMBRE']}</span></h3>
   </div>
   <div class="panel-body">
      <div class="table-responsive mh-50">
         <table class="table table-condensed">
            <thead>
               <tr bgcolor="#F5C932">
                  <th class="col-xs-1" style='text-align: left'>Solicitud.</th>
                  <th class="col-xs-2" style='text-align: left'>Fecha Solicitada</th>
                  <th class="col-xs-2" style='text-align: left'>Codigo de Pago</th>
               </tr>
            </thead>
               {$j=1}
               {foreach from = $saDatos['paMaCpj'] item = i}
               <tr data-toggle="collapse" data-target="#accordion{$j}" class='clickable' role="button">
                  <td style='text-align: left'>{$j}</td>
                  <td style='text-align: left'>{$i['DGENERA']}</td>
                  <td style='text-align: left'>{$i['CNROPAG']}</td>
               </tr>
               <tr>
               <td colspan="4">
               <div id="accordion{$j}" class="collapse">
               <table class = "table table-condensed table-bordered">
                  <thead>
                  <tr bgcolor="#F5DF7F">
                     <th class="col-xs-1" style='text-align: left'>Curso</th>
                     <th class="col-xs-3" style='text-align: left'>Descripción</th>
                     <th class="col-xs-6" style='text-align: left'>Observacion</th>
                     <th class="col-xs-6" style='text-align: left'>Jurado 1</th>
                     <th class="col-xs-6" style='text-align: left'>Jurado 2</th>
                     <th class="col-xs-2" style='text-align: left'>Estado</th>
                  </tr>
                  </thead>
                  {$l = 1}
                  {foreach from = $i['paDeCpj'] item = k}
                  <tr>
                     <td style='text-align: left'>{$l}</td>
                     <td style='text-align: left'>{$k['CDESCRI']}</td>
                     <td style='text-align: left'>{$k['MOBSERV']}</td>
                     <td style='text-align: left'>{$k['CJURAD1']}</td>
                     <td style='text-align: left'>{$k['CJURAD2']}</td>
                     {if $k['CESTESC'] == 'D' AND $k['CESTSAC'] == 'D'}
                     <td style='text-align: left'><span class="label label-danger">DENEGADO POR LA ESCUELA</span></td>
                     {else if $k['CESTSAC'] == 'D' AND $k['CESTESC'] == 'A'}
                     <td style='text-align: left'><span class="label label-danger">DENEGADO POR LA ORAA</span></td>
                     {elseif $k['CESTADO'] == 'R'}
                     <td style='text-align: left'><span class="label label-info">RETENIDO</span></td>
                     {else}
                     <td style='text-align: left'><span class="label label-success">NO DENEGADO</span></td>
                     {/if}
                  </tr>
                  {$l=$l+1}
                  {/foreach}
                  </div>
               </table>
               </div>
               {$j=$j+1}
               {/foreach}
         </table>
      </div>
   </div>
   </div>
    <div class="col-sm-4"></div>
   <div class="col-sm-4"></div>   
   <div class="col-sm-4">
      <a href="Tdo5040.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
   </div>         
   {/if}
<div id="myModal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title"><b>Información general</b></h4>
   </div>
   <div class="modal-body">
      <p><b>Solo se admiten hasta 2 cursos por jurado.</b></p>
   </div>
   <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
   </div>
</div>
</div>
</div>
</div>
</div>
</body>
</html>