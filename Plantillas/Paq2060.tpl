<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody table-responsive">
<form action="Paq2060.php" method="post">
   {if $snBehavior == 0}
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-success">
               <div class="panel-heading"><h3 class="panel-title"><b>Datos del tramite</b></h3></div>   
               <div><tr><td colspan = "3"><label style="font-size:14px; text-align:left; color: #9B0000; font-style: italic; ">
                  * El boton de revisado solo debe ser usado al final de la revisión de todos los alumnos. Pues al darle click se aprobará todos los expedientes en bloque.
                  Así que primero debera hacer la revisión de los documentos de todos los alumnos, para recién poder usar el boton de revisión.
                  </label></td></tr>
               </div>
               <div class="panel-body">
               <table class="table table-condensed"> 
                  <thead> 
                  <th style='text-align: center' class="col-xs-2">Fecha Recepción</th>
                  <th style='text-align: center' class="col-xs-1">Cod. Alumno</th>
                  <th style='text-align: center' class="col-xs-1">DNI</th>
                  <th style='text-align: left' class="col-xs-3">Nombre</th>
                  <th style='text-align: left' class="col-xs-3">Unidad Académica</th>       
                  <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                  </thead>
                  {foreach from = $saDatos item = i}
                     <tr>
                     <td style='text-align: center'>{$i['DRECEPC']}</td>
                     <td style='text-align: center'>{$i['CCODALU']}</td>
                     <td style='text-align: center'>{$i['CNRODNI']}</td>
                     <td style='text-align: left'>{$i['CNOMBRE']}</td>
                     <td style='text-align: left'>{$i['CNOMUNI']}</td>
                     <td style="text-align: center"><input type="radio" name="paData[CCODALU]" value="{$i['CCODALU']}"><input type="hidden" name="paData[paCodAlu][{$j}]" value="{$i['CCODALU']}"></td>
                     </tr>
                  {/foreach}
               </table>
               </div>
            </div>
         </div>
      </div>                
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" onclick="return confirm('¿Esta seguro que desea aprobar toda la lista de alumnos?')" name="Boton" value="RevisarDocente" class="center-block btn btn-success btn-lg btn-block"> Revisado <i class="glyphicon glyphicon-check"></i></button><br> 
         </div>
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="Seguimiento" class="center-block btn btn-primary btn-lg btn-block"> Seguimiento <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
   {elseif $snBehavior == 1}
      <div class="row">
         <div class="col-sm-10 col-sm-push-1">
         <div class="panel panel-success">
            <div class="panel-heading"><h3 class="panel-title" class="col-xs-4"><b>BANDEJA DE EXPEDIENTE DE DOCUMENTOS </b>
               <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
            </div>
            <div class="panel-body">
               <input type="hidden" name="paData[CCODALU]" value="{$saDatos[0]['CCODALU']}">
               <p class="text-muted"><b>ALUMNO: {$saDatos[0]['CNOMBRE']}  -  {$saDatos[0]['CCODALU']}</b></p>
               <div class="table-responsive mh-50">
               <table class="table table-condensed"> 
                  <thead> 
                  <th style='text-align: center' class="col-xs-4">Tipo Documento</th>
                  <th style='text-align: center' class="col-xs-2">Nro Expediente</th>
                  <th style='text-align: center' class="col-xs-2">Fecha de Envio</th>  
                  <th style='text-align: center' class="col-xs-2">Estado</th>       
                  <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></th>
                  <th style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-eye-open"></i></th>
                  </thead>
                  {foreach from = $saDato item = i}  
                     <tr>         
                     <td style='text-align: left' class="col-xs-4">{$i['CDESDOC']}</td>
                     <td style='text-align: center' class="col-xs-2">E-{$i['CCODTRE']}</td>
                     <td style='text-align: center' class="col-xs-2">{$i['TFECREC']}</td>
                     <td style='text-align: center' class="col-xs-2"> 
                     {if $i['CESTDTR'] !=  null and ($i['CESTMTR'] == 'B' or ($i['CESTMTR'] != 'M' and $i['CESTMTR'] != 'S'))}
                        {if $i['CESTDTR'] == 'A' }
                           <img src="Images/clock.png" width="9%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                        {elseif $i['CESTDTR'] == 'B' or $i['CESTMTR'] == 'E'}  
                           <img src="Images/eye.png" width="10%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                        {elseif $i['CESTDTR'] == 'F' or $i['CESTMTR'] == 'F' }  
                           <img src="Images/llenar.png" width="9%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                        {else}
                           <img src="Images/clock.png" width="9%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                        {/if}
                     {else}
                        {if $i['CESTMTR'] == 'E'}   
                        <img src="Images/eye.png" width="10%" data-toggle="tooltip" data-placement="bottom" title="Observado">
                        {elseif $i['CESTMTR'] == 'B' and ($i['CIDCATE'] == 'CCCONB' OR ($i['CIDCATE'] != 'CCCSID' OR $i['CIDCATE'] != 'CCCOND'))}  
                        <img src="Images/aprobado.png" width="9%" data-toggle="tooltip" data-placement="bottom" title="Aprobado">
                        {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 2) == 'CC' and ($i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCESTU')}
                        <img src="Images/llenar.png" width="9%" data-toggle="tooltip" data-placement="bottom" title="Llenar formulario">
                        {elseif ($i['CESTMTR'] == 'S' or $i['CESTMTR'] == 'M') and (substr($i['CIDCATE'], 0, 2) == 'CC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == 'CCCOND' or $i['CIDCATE'] == '000084')}
                        <img src="Images/aprobado.png" width="9%" data-toggle="tooltip" data-placement="bottom" title="Mesa de partes">
                        {elseif $i['CESTMTR'] == 'F' and substr($i['CIDCATE'], 0, 1) == 'P'}
                        <img src="Images/subir.png" width="9%" data-toggle="tooltip" data-placement="bottom" title="Subir Archivo">      
                        {else}
                        <img src="Images/clock.png" width="9%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
                        {/if}
                     {/if}
                     </td>
                     <td style='text-align: center' class="col-xs-1">            
                     <!--RADIO BUTTON PARA SEGUIMIENTO / OBSERVACION / GENERAR CONSTANCIA !--> 
                     {if $i['CESTMTR'] =='B' and $i['CIDCATE'] == 'CCCONB'}                        
                           <button  type="button"  class="btn btn-info btn-block btn-xs" onClick="window.open('{$i['MDETALL']['CURLPDF']}', 'Algo', 'height=850 ,width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>                        
                     {elseif $i['CESTMTR'] =='S' and $i['CESTPRO'] == 'R' and ($i['CIDCATE'] == 'CCESTU' or $i['CIDCATE'] == 'CCCSUC' or $i['CIDCATE'] == 'CCCSID' or $i['CIDCATE'] == '000017' or $i['CIDCATE'] == '000084')}
                        <button type="button" class="btn btn-info btn-block btn-xs" onClick="window.open('{$i['CDOCDIG']}', 'Algo', 'height=850 ,width=850');">Ver<i class="glyphicon glyphicon-eye-open"></i></button>
                     {else}
                        <button type="button" class="btn btn-info btn-block btn-xs" name="pcCodtre" onClick="window.open('DocumentoPaquete.php?CNRODNI={$i['CNRODNI']}&CCODTRE={$i['CCODTRE']}', 'Algo', 'height=850, width=850');">Ver <i class="glyphicon glyphicon-eye-open"></i></button>
                     {/if}                     
                     <!--<a class="btn btn-info btn-block btn-XS"  href="DocumentoPaquete.php?CNRODNI={$saData['CNRODNI']}&CCODTRE={$saData['CCODTRE']}&download=true">Ver</a>-->
                     <!--<button type="submit" name="pcCodtre"  value="{$i['CCODTRE']}"  class="center-block btn btn-success btn-xs btn-block">Ver <i class="glyphicon glyphicon-eye-open"></i></button>-->
                     </td>           
                     <td style='text-align: center' class="col-xs-1"><input type="radio" name="paData[CCODTRE]" value="{$i['CCODTRE']}"/>
                     </tr>
                  {/foreach}
               </table>
               </div>
            </div>
         </div>
         <div class="row">            
            <div class="col-sm-4">
              <button type="submit" name="Boton" value="Observacion" class="center-block btn btn-primary btn-lg btn-block"> Observación <i class="glyphicon glyphicon-eye-open"></i></button><br> 
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
              <a href="Paq2060.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
            </div> 
         </div> 
         </div> 
         </div>

      {elseif $snBehavior == 2}
        <div class="container-fluid">
          <div class="row col-md-10 col-md-push-1"> 
            <div class="panel panel-default">  
              <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITES</b></h3></div>
                <div class="panel-body">
                  <input type="hidden" name="paData[CCODALU]" value="{$saDatos['CCODALU']}">
                  <div class="table-responsive">
                    <div class="panel-heading"><h3 class="panel-title"><b>OBSERVACIÓN DE DOCUMENTO</b>
                      <span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3>
                    </div>
                    <table class="table table-hover">
                       <tr>
                         <td><b>CÓDIGO DOCUMENTO</b></td>         
                         <td>
                            <input class="form-control" type='text' name="paData[CCODTRE]" value="{$saDatos['CCODTRE']}" readonly>
                         </td>
                      </tr>
                      <tr>
                         <td><b>DOCUMENTO</b></td>
                         <td>
                            <input class="form-control" type='text' name="paData[CDESCRI]" value="{$saDatos['CDESCRI']}" readonly>
                         </td> 
                      </tr>
                      <tr>
                           <td><b>OBSERVACIÓN</b></td>
                           <th><textarea rows="5" class="form-control" name="paData[MOBSERV]" placeholder="Observaciones" value="{$saDatos['MOBSERV']}" style="text-transform:uppercase"></textarea></th>
                           <!--<td><input class="form-control" type='text' name="paData[MOBSERV]" value="{$saDatos['MOBSERV']}" style="text-transform:uppercase"></td>--> 
                      </tr>
                   </table>
                   <div class="row">
                    <div class="col-sm-4">
                      <button type="submit" name="Boton" value="Observar" class="center-block btn btn-info btn-lg btn-block"/> Observación <i class="glyphicon glyphicon-edit"></i>
                    </div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                      <button type="submit" name="Boton" value="Seguimiento" class="center-block btn btn-danger btn-lg btn-block"/> Volver <i class="glyphicon glyphicon-log-out"></i>
                    </div>
                  </div>
                  </div>
                </div>
              </div>      
            </div>
          </div> 
        </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer"></div>
</body>
</html>