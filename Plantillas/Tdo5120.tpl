<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="CSS/style.css">
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="js/java.js"></script>
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script>
   <script>
   function myFunctionObs(e){
      $("#mdCodTre").val(e.value);
      $('#btnMod').show();
      $('#modalNew').modal('show');
   }

   function myFunctionApro(e){
      if( $("input[type='radio']").is(':checked')){
         $('#modalArch').modal('show');
      }
      else{
         window.alert('Seleccione un Tramíte');
      }
   }

   function f_MarcarTerminado(p_cCodTre) {
		if(confirm("¿Desea marcar como recepcionado?\nEste acción no puede ser revertida.")) {
			$('#p_cCodTre').val(p_cCodTre.value);
			$('#Id').val('Recepcion');
			$('#poForm').submit();
		}
   }

   function myFunctionShowObs(e){
      $('#btnMod').hide();
      if( $("input[type='radio']").is(':checked')){
         var observ;
         $('.obsrv:checked').each(function(indice, elemento){
            var fila = $(this).parents(".Datos");
            observ = fila.find(".mobsrv").val();
            $("#txtObser").val(observ);
            $("#modalNew").modal('show');
         });
      }
      else{
         window.alert('Seleccione un alumno');
      }
   }

   function f_Opcion(){
      var lcOpcion = $('#pcOpcion').val();
		var lcSend = "Id=Opcion&paData[COPCION]="+lcOpcion;
		$.post("Tdo5120.php",lcSend).done(function(p_cResult) {
			var laJson = (isJson(p_cResult))? JSON.parse(p_cResult) : p_cResult;
			if (laJson.ERROR){
				alert(laJson.ERROR);
				$('#TBody').html('');
			} else {
            if(lcOpcion == 'E') {
               $('#btnBody1').hide();
               $('#btnBody2').show();
            }
				$('#TBody').html(p_cResult);
			}
      });      
   }
   </script>
</head>
<body class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos - Carpeta de Documentos </b></h3></div>
<div class="container-fluid">
   <input type="hidden" id="llModal" value="{$saData['LLMODAL']}">
  <form action="Tdo5120.php" method="post" enctype="multipart/form-data">
  {if $snBehavior == 0}
  <div class="row col-md-10 col-md-push-1">
    <div class="panel panel-default">
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SOLICITUDES REALIZADAS</b>
         <select class="selectpicker" id="pcOpcion" onchange= "f_Opcion()">
            <option value="P">PENDIENTES</option>
            <option value="E">OBSERVADOS</option>
         </select></h3>
      </div>
         <div class="table-responsive mh-60">
            <table class="table table-condensed" id="TBody">
            <thead>
               <th class="col-xs-2" style='text-align: center'>Codigo de Tramite</th>
               <th class="col-xs-3" style='text-align: left'>Descripción de Trámite</th>
               <th class="col-xs-2" style='text-align: left'>Código Alumno</th>
               <th class="col-xs-3" style='text-align: left'>Nombre</th>
               <th class="col-xs-2" style='text-align: left'>Fecha de Modificacion</th>
               <th class="col-xs-2" style='text-align: left'>Recepcionar</th>
               <th class="col-xs-2" style='text-align: left'>Subir</th>
               <th class="col-xs-2" style='text-align: left'>Aprobar</th>
               <th class="col-xs-2" style='text-align: left'>Observar</th>
            </thead>
            <tbody>
              {foreach from = $saDatos item = i}
                  <form action="Tdo5120.php" method="POST" enctype="multipart/form-data">                 
                    {if $i['CESTDET'] eq 'A'}
                     <tr class="bg-success" class ="Datos">
                    {else}
                     <tr class ="Datos">
                    {/if}
                     <td style='text-align: center'>{$i['CCODTRE']}</td>
                     <td style='text-align: left'>{$i['CDESCRI']}</td>
                     <td style='text-align: left'>{$i['CCODALU']}</td>
                     <td style='text-align: left'>{$i['CNOMBRE']}</td>
                     <td style='text-align: left'>{$i['TMODIFI']}</td>
                     {if $i['CESTDET'] eq 'A'}
                        <td style='text-align: center' class="col-xs-1"><i class="glyphicon glyphicon-ok"></i></td>
                     {else}
                        <td class="text-center">
							      <button class="center-block btn-primary"value="{$i['CCODTRE']}" onclick="f_MarcarTerminado(this);" required>
							      	<img src="Images/folder.png" width="20" height="18">
							      </button>
                        </td>
                     {/if}
                     <td>
                        <input name="fTraduccion" id = "file" type="file" class="file">                  
                     </td>
                     <td>
                        <button name="Boton" value="Aprobar" onclick="return confirm('¿Está seguro que desea subir y aprobar éste archivo? Esta operación no podrá ser revertida.')" class="center-block btn-success"/><i class="glyphicon glyphicon-ok"></i></button>
                     </td> 
                     <td style='text-align: center' class="col-xs-1">            
                        <input type="hidden" class="pcCodTr" name = "pcCodTre" value="{$i['CCODTRE']}">
                        <button type="button" value="{$i['CCODTRE']}" onclick="myFunctionObs(this)" class="center-block btn-warning"><i class="glyphicon glyphicon-eye-open"></i></button>
                     </td>
                  </tr>
                </form>
              {/foreach} 
            </tbody>        
        </table>
      </div>
    </div>
    <div class="row" id="btnBody1">
         <div class="col-xs-4"></div>
         <div class="col-xs-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
         <div class="col-sm-4"></div>
      </div>                                         
      <div class="row" id="btnBody2" hidden="true">
         <div class="col-xs-4">
            <button type="submit" name="Boton" value="LevObserv" class="center-block btn btn-success btn-lg btn-block"> Lev. Observacion <i class="glyphicon glyphicon-ok"></i></button>
         </div>
         <div class="col-xs-4">
            <button type = "button" onclick = "myFunctionShowObs()" class="center-block btn btn-warning btn-lg btn-block">Ver Observacion&nbsp;&nbsp;<i class="glyphicon glyphicon-eye-open"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Tdo5120.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
  {/if}
  <div class="modal fade" id="modalNew" role="dialog">
      <div class="modal-dialog">
      <form action="Tdo5120.php" method="POST">
         <input type = "hidden" id="mdCodTre" name="paData[CCODTRE]">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">AGREGAR OBSERVACIÓN</h4>
            </div>
         <div class="modal-body">
         <table class="table table-condesed">
            <tr>
               <th>OBSERVACIÓN</th>
               <td><textarea style="text-transform:uppercase" id = "txtObser" class="form-control" rows = "4" name="paData[MOBSERV]" pattern="\d*"></textarea></td>
            </tr>
         </table>
         </div>
         <div class="modal-footer">
            <button id="btnMod" class="btn btn-warning" name="Boton" value="Observar">Observar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
         </div>
         </div>
      </form>
      </div>
      </div>
<div id="footer">
</div>
</body>