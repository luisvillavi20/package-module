<html>   
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script type="text/javascript" src="CSS/jquery.modal.js"></script> 
	
</head>
<script>
   function f_seleccionarFila(e) {
         let loRadio = e.querySelector('input[type="radio"]');
         if (loRadio != undefined && !loRadio.disabled) {
            loRadio.checked = true;
         }
   }

   var cont = 1;
   
   function agregarFila() {
      var nuevaFila   = '<tr>';
      nuevaFila  += '<td style= "text-align: center"><textarea style="text-transform:uppercase" rows = "1" class="form-control col-xs-12" type="text" name="paData[CASIGNA'+cont+']" placeholder = "Descripcion" required></textarea></td>';
      nuevaFila  += '<td style="text-align: center"><input class="form-control col-xs-12" type="text" name="paData[CNOTA'+cont+']" placeholder = "0-20" maxlength = "2" required/></td>';
      nuevaFila  += '<td style="text-align: center"><textarea style="text-transform:uppercase" rows = "1" class="form-control col-xs-12" type="text" name="paData[CCAMCLI'+cont+']" placeholder = "Descripcion"  required></textarea></td>';
      nuevaFila  += '<td style="text-align: center"><input class="form-control col-xs-12" type="text" name="paData[CDURACI'+cont+']" placeholder = "1-12" maxlength = "2" required/></td>';
      nuevaFila  += '</tr>';
      cont++;
      $("#tbody").append(nuevaFila);

   }
   function quitarFila(){
      trs=$("#tbody tr").length;
      if(trs>1)
      {
         $("#tbody tr:last").remove();
      }
   }

   function myFunctionObs(e){
      if( $("input[type='radio']").is(':checked')){
         $('#modalNew').modal('show');
      }
      else{
         window.alert('Seleccione un alumno');
      }
   }
</script>
<style>
   .btn-circle {
      width: 32px;
      height: 32px;
      padding: 6px 0px;
      border-radius: 18px;
      text-align: center;
      font-size: 12px;
      line-height: 1.42857;
   }
   textarea{
      padding: small;
      resize: none;
   }
</style>
<body class="divBody">
   <div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Seguimiento de Trámites </b></h3></div>
   <div class="container-fluid divBody">
   <form action="Paq1420.php" method="post">
   {if $snBehavior == 0}
      <div class="container-fluid">
      <div class="row col-md-10 col-md-push-1"> 
      <div class="panel panel-default">  
      <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b>
         <span style="float:right"><b>ALUMNO: </b>  {$saData['CNOMALU']} </span></h3>
      </div>
      <div class="panel-body">
      <div class="table-responsive mh-50">
      <table class="table table-condensed"> 
         <thead> 
           <th style='text-align: center' class="col-xs-2">Codigo de Alumno</th> 
           <th style='text-align: center' class="col-xs-2">DNI</th>  
           <th style='text-align: center' class="col-xs-3">Nombre de Alumno</th>
           <th style='text-align: center' class="col-xs-4">Tipo</th>
           <th style='text-align: center' class="col-xs-2">Estado</th>
         </thead>
         {foreach from = $saDatos item = i}      
            <tr onclick="f_seleccionarFila(this)">         
               <td style='text-align: center' class="col-xs-2">{$i['CCODALU']}</td>        
               <td style='text-align: left' class="col-xs-2">{$i['CNRODNI']}</td>
               <td style='text-align: left' class="col-xs-5">{$i['CNOMBRE']}</td>
               <td style='text-align: left' class="col-xs-2">{$i['CDESCRI']}</td>
               <td style='text-align: center'>
                  <img src="Images/clock.png" width="50%" data-toggle="tooltip" data-placement="bottom" title="Pendiente">
               </td>
               <td style='text-align: center'><input type="radio" name="paData[CCODTRE]" value="{$i['CCODTRE']}" required/></td>
            </tr>
         {/foreach}
      </table>
      </div>
      </div>
      </div>    
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type = "submit" class="center-block btn btn-success btn-lg btn-block" name = "Boton" value = "Llenar">Aprobar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
         </div>  
         <div class="col-sm-4">
            <button type = "button" class="center-block btn btn-warning btn-lg btn-block" onclick ="myFunctionObs()">Observar&nbsp;&nbsp;<i class="glyphicon glyphicon-eye-open"></i></button>
         </div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div> 
      </div> 
      </div> 
      </div>
   </form>
   {elseif $snBehavior == 1}
   <form action="Paq1420.php" method="post">
      <div class="container-fluid">
      <div class="row col-md-10 col-md-push-1"> 
      <div class="panel panel-default">  
      <div class="panel panel-info">
      <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DE TRÁMITES</b></h3></div>
      <div class="panel-body">
         <!--<input type = "hidden" name = "paData[CCODTRE]" value = "{$saDatos['CCODTRE']}">!-->
         <p class="text-muted">
         <b>ALUMNO: {$saData['CNOMALU']} &nbsp;&nbsp;&nbsp;&nbsp; CÓDIGO: {$saData['CCODALU']}
         &nbsp;&nbsp;&nbsp;&nbsp; CÓDIGO EXPEDIENTE: E-{$saData['CCODTRE']}
         </b> 
         </p> 
      <div class="table-responsive mh-50">
      <table class="table table-condensed">
         <thead> 
           <th style='text-align: center' class="col-xs-4">Asignatura</th>  
           <th style='text-align: center' class="col-xs-1">Nota</th>
           <th style='text-align: center' class="col-xs-4">Campo Clínico</th>
           <th style='text-align: center' class="col-xs-1">Duración</th>
           <th style='text-align: center' class="col-xs-2">
            <button type="button" onclick = agregarFila() class="btn btn-success btn-circle"><i class="glyphicon glyphicon-plus"></i></button>
            <button type="button" onclick = quitarFila() class="btn btn-danger btn-circle"><i class="glyphicon glyphicon-minus"></i></button> 
           </th>
         </thead>
         <tbody id="tbody">
         <tr>
            <td style='text-align: center'><textarea style="text-transform:uppercase" rows = "1" class="form-control col-xs-12" type="text" name="paData[CASIGNA0]" placeholder = "Descripcion" required></textarea></td>
            <td style='text-align: center'><input class="form-control col-xs-12" type="number" name="paData[CNOTA0]" placeholder = "0-20"  required/></td>
            <td style='text-align: center'><textarea style="text-transform:uppercase" rows = "1" class="form-control col-xs-12" type="text" name="paData[CCAMCLI0]" placeholder = "Descripcion" required></textarea></td>
            <td style='text-align: center'><input class="form-control col-xs-12" type="number" name="paData[CDURACI0]" placeholder = "1-12" required/></td>
         </tr>
         </tbody>
      </table>
      </div>
      </div>
      </div>
       <div>
         <p type ="text-muted" class = "col-xs-10"></p>
         
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type = "submit" class="center-block btn btn-success btn-lg btn-block" name = "Boton" value = "Grabar">Grabar&nbsp;&nbsp;<i class="glyphicon glyphicon-ok"></i></button>
         </div>  
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Paq1420.php" class="center-block btn btn-danger btn-lg btn-block" ><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div> 
      </div>
      </div>
   </form>
   </div>
   {/if}
   <div class="modal fade" id="modalNew" role="dialog">
   <div class="modal-dialog">
   <form action="Paq1420.php" method="POST">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">AGREGAR OBSERVACIÓN</h4>
         </div>
      <div class="modal-body">
      <table class="table table-condesed">
         <tr>
            <th>OBSERVACIÓN</th>
            <td><textarea style="text-transform:uppercase" id = "txtObser" class="form-control" rows = "4" name="paData[MOBSERV]" pattern="\d*"></textarea></td>
         </tr>
      </table>
      </div>
      <div class="modal-footer">
         <button class="btn btn-warning" name="Boton" value="Observar">Observar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
      </div>
   </form>
   </div>
   </div>
</body>
</html>

