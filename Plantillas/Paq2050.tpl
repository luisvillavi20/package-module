<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
   <script>
      function f_Init(p_nBehavior) {
         if (p_nBehavior == 0) {
            f_CambiarUnidadAcademica();
         }
      }
      
      function f_CargarGrupos() {
         let lcIdCola = $('[name="paData[CIDCOLA]"]').val();
         let lcTipCol = $('[name="paData[CTIPCOL]"]').val();
         $.get('Paq2050.php', {
           Id: 'CargarGrupos',
           paData:{
             CIDCOLA: lcIdCola,
             CTIPCOL: lcTipCol
           }
         }, function (data) {
           $('#select_grupos').html(data);
         });
      }
      
      function f_valCrearGrupo() {
         let lcIdCola = $('[name="paData[CIDCOLA]"]').val();
         if (lcIdCola == null) {
            alert("SELECCION COLACIÓN");
            return false;
         }
         return true;
      }
      
      function f_cargarUltimaColacion(self) {
         $.get('Paq2050.php', {
            Id: 'CargarColacion',
            paData: {
               CTIPCOL: self.value
            }
         }, function (data) {
            var data = JSON.parse(data);
            if (data['ERROR']) {
               alert(data['ERROR']);
            } else {
               $("#tr_colacion").css('display', 'table-row');
               $('[name="paData[CIDCOLA]"]').val(data['CIDCOLA']);
               $("#td_colacion").html(data['CDESCRI']);
            }
         });
      }
      
      function f_CambiarUnidadAcademica() {
         if ($('#pcTipCol').val() == 'B' && ($('#pcUniAca').val() == '4E' || $('#pcUniAca').val() == '4A')) {
            $('#divEspecialidadMecanica').show();
            $('#pcUniEsp').prop('required', true);
         } else {
            $('#divEspecialidadMecanica').hide();
            $('#pcUniEsp').prop('required', false);
         }
      }
   </script>
</head>
<body onload="f_Init({$snBehavior})" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<div class="container-fluid divBody">
<form action="Paq2050.php" method="post">
   <div class="row">
   <div class="col-sm-12">
   <div class="panel panel-success">
      {if $snBehavior == 0}
         <div class="panel-heading"><h3 class="panel-title"><b>GRUPOS DE COLACIÓN</b></h3></div>
         <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-hover"><tr>
               <td><b>TIPO DE COLACION</b></td>
               <td colspan="3">
                  <select id="pcTipCol" class="form-control" name="paData[CTIPCOL]" required onchange="f_cargarUltimaColacion(this)">
                     <option selected="true" disabled value="">Seleccione tipo de colación...</option>
                     {foreach from = $saTipCol item=i}
                        <option name="paData[CTIPCOL]" value="{$i['CCODIGO']}">{$i['CDESCRI']}</option>
                     {/foreach}
                  </select>          
               </td> 
               </tr><tr id="tr_colacion" style="display: none">
                  <th>COLACION<input type="hidden" name="paData[CIDCOLA]"></th>
                  <td id="td_colacion" colspan="3"></td>
               </tr>
               <tr>
               <td><b>UNIDAD ACADÉMICA</b></td>
               <td colspan="3">
                  <select id="pcUniAca" class="form-control" required name="paData[CUNIACA]" onclick="f_CambiarUnidadAcademica();">
                     <option selected="true" disabled value="">Seleccione escuela profesional...</option>                     
                     {foreach from = $saEscuel item = i}
                        <option name="paData[CUNIACA]" value="{$i['CUNIACA']}">{$i['CUNIACA']} - {$i['CNOMUNI']}</option>
                     {/foreach}
                  </select>          
               </td>
               </tr>
               <tr id="divEspecialidadMecanica">
               <td><b>ESPECIALIDAD</b></td>
               <td colspan="3">
                  <select id="pcUniEsp" class="form-control" name="paData[CUNIESP]">
                     <option selected="true" disabled value="">Seleccione especialidad...</option>                     
                     {foreach from = $saEspMec item = i}
                        <option name="paData[CUNIACA]" value="{$i['CUNIACA']}">{$i['CUNIACA']} - {$i['CNOMUNI']}</option>
                     {/foreach}
                  </select>          
               </td>
               </tr>
            </table>            
            </div>
            <div>
            {if $saData['CCODIGO'] == 'B'}
               <div class="col-sm-4">
                  <button type="submit" name="Boton" value="VerificarRevisores" class="center-block btn btn-info btn-lg btn-block"/> Asignar Revisores <i class="glyphicon glyphicon-plus"></i>
               </div>
            {else}
               <div class="col-sm-4"></div>
            {/if}
               <div class="col-sm-4">
                  <button type="submit" name="Boton" value="VerificarGrupo" class="center-block btn btn-success btn-lg btn-block"/> Asignar Alumnos <i class="glyphicon glyphicon-plus"></i>
               </div>
               <div class="col-sm-4">
                  <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir </a>
               </div>
            </div>
         </div>
      {elseif $snBehavior == 1}
         <div class="panel-heading"><h3 class="panel-title"><b>GRUPOS DE COLACIÓN - CREAR GRUPO</b><span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3></div>
         <div class="panel-body">
            <input type="hidden" name="paData[CIDCOLA]" value="{$saData['CIDCOLA']}">
            <input type="hidden" name="paData[CTIPCOL]" value="{$saData['CTIPCOL']}">
            <table class="table table-hover">
               <tr>
               <td><b>CÓDIGO DE COLACIÓN</b></td>
               <td><input class="form-control" type='text' name="paData[CIDCOLA]" value="{$saData['CIDCOLA']}" readonly></td> 
               </tr><tr>
               <td><b>UNIDAD ACADÉMICA</b></td>
               <td><input class="form-control" type='text' name="paData[CUNIACA]" value="{$saData['CUNIACA']}" style="text-transform:uppercase"></td>
               </tr><tr>
               <td><b>TIPO DE COLACION</b></td>
               <td>
                  <select class="form-control" name="paData[CTIPCOL]">
                     <option selected="true" disabled value="">Seleccione tipo de colación...</option>                     
                     {foreach from=$saDatos item=i}
                        <option name="paData[CTIPCOL]" value="{$i['CCODIGO']}">{$i['CDESCRI']}</option>
                     {/foreach}
                  </select>          
               </td> 
               </tr>
            </table>
            <div class="row">
               <div class="col-sm-4">
                  <button type="submit" name="Boton" value="GrabarGrupo" class="center-block btn btn-success btn-lg btn-block"/> Grabar <i class="glyphicon glyphicon-plus"></i>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <a href="Paq2050.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
               </div>
            </div>
         </div>
      {elseif $snBehavior == 2}
         <div class="panel-heading"><h3 class="panel-title"><b>GRUPOS DE COLACIÓN - ASIGNAR DOCENTES</b><span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3></div>
         <div class="panel-body">
            <table class="table table-hover">
               <tr>
               <td><b>ID COLACION</b></td>
               <td>{$saData['CIDCOLA']}</td>
               <td><b>TIPO COLACION</b></td>
               <td>{$saData['CDESCOL']}</td>
               </tr><tr>
               <td><b>UNIDAD ACADÉMICA</b></td>
               <td colspan="3">{$saData['CUNIACA']} - {$saData['CNOMUNI']}</td>
               </tr>
            </table>
            {if !isset($saDatos[2])}
            <input type="hidden" name="paData[CIDCOLA]" value="{$saData['CIDCOLA']}">
            <input type="hidden" name="paData[CTIPCOL]" value="{$saData['CTIPCOL']}">
            <input type="hidden" name="paData[CUNIACA]" value="{$saData['CUNIACA']}">
            <table class="table table-hover">
               <tr>
               <td><b>DOCENTE A ASIGNAR</b></td>
               <td>
                  <select class="selectpicker form-control" data-live-search="true" name="paData[CCODDOC]">
                     <option selected="true" disabled value="">Seleccione docente...</option>
                     {foreach from = $saArrays['paDocentes'] item = i}
                        <option value="{$i['CCODDOC']}">
                           {$i['CCODDOC']} - {$i['CNOMBRE']} - {$i['CNRODNI']}
                        </option>
                     {/foreach}
                  </select>
               </td>
               </tr>
            </table>
            <div class="row">
               <div class="col-sm-8"></div>
               <div class="col-sm-4">
                     <button type="submit" name="Boton" value="AsignarRevisor" class="center-block btn btn-success btn-lg btn-block"/> Asignar <i class="glyphicon glyphicon-plus"></i>
               </div>
            </div>
            <hr>
            {/if}
            <table class="table table-condensed">
               <thead>
                  <tr>
                  <th colspan="2">DOCENTES REVISORES</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                  <th>Presidente:</th>
                  <td>{if isset($saDatos[0])}{$saDatos[0]['CNOMBRE']}{/if}</td>
                  <td>{if isset($saDatos[0])}{$saDatos[0]['CCODDOC']}{/if}</td>
                  </tr><tr>
                  <th>Vocal:</th>
                  <td>{if isset($saDatos[1])}{$saDatos[1]['CNOMBRE']}{/if}</td>
                  <td>{if isset($saDatos[1])}{$saDatos[1]['CCODDOC']}{/if}</td>
                  </tr><tr>
                  <th>Secretario:</th>
                  <td>{if isset($saDatos[2])}{$saDatos[2]['CNOMBRE']}{/if}</td>
                  <td>{if isset($saDatos[2])}{$saDatos[2]['CCODDOC']}{/if}</td>
                  </tr>
               </tbody>               
            </table>
            <div class="row">
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <a href="Paq2050.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
               </div>
               <div class="col-sm-4"></div>
            </div>
         </div>
      {elseif $snBehavior == 3}
         <div class="panel-heading"><h3 class="panel-title"><b>GRUPOS DE COLACIÓN</b><span style="float:right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3></div>
         <div class="panel-body">
            <input type="hidden" name="paData[CIDCOLA]" value="{$saData['CIDCOLA']}">
            <input type="hidden" name="paData[CTIPCOL]" value="{$saData['CTIPCOL']}">
            <input type="hidden" name="paData[CUNIACA]" value="{$saData['CUNIACA']}">
            <table class="table table-hover">
               <tr>
               <td><b>ID COLACION</b></td>
               <td>{$saData['CIDCOLA']}</td>
               <td><b>TIPO COLACION</b></td>
               <td>{$saData['CDESCOL']}</td>
               </tr><tr>
               <td><b>UNIDAD ACADÉMICA</b></td>
               <td colspan="3">{$saData['CUNIACA']} - {$saData['CNOMUNI']}</td>
               </tr>
            </table>
            <table class="table">
               <thead>
                  <tr>
                  <th>Código del alumno</th>
                  <th>DNI</th>
                  <th>Nombre</th>
                  <th>Unidad Académica</th>
                  <th style="text-align: center">Activar</th>
                  </tr>
               </thead>
               <tbody>
                  {foreach from=$saDatos item=i}
                  <tr {if isset($i['NSERIAL'])}class="success"{/if}>
                  <td>{$i['CCODALU']}</td>
                  <td>{$i['CNRODNI']}</td>
                  <td>{$i['CNOMBRE']}</td>
                  <td>{$i['CUNIACA']} - {$i['CNOMUNI']}</td>
                  <td style="text-align: center"><input type="checkbox" name="paData[CCODALU][]" {if isset($i['NSERIAL'])}checked disabled{else}value="{$i['CCODALU']}"{/if}></td>
                  </tr>
                  {/foreach}
               </tbody>
            </table>
            <div class="row">
               <div class="col-sm-4">
                  <button type="submit" name="Boton" value="AsignarAlumno" class="center-block btn btn-success btn-lg btn-block"/> Asignar <i class="glyphicon glyphicon-plus"></i>
               </div>
               <div class="col-sm-4"></div>
               <div class="col-sm-4">
                  <a href="Paq2050.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
               </div>
            </div>
         </div>
      {/if}
   </div>
   </div>
   </div>
</form>
</div>
</body>
</html>