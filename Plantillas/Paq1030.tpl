<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<script>   
   function f_EstadoPersona(val){
      if(val == 'Alumno'){
         $( "#pcAlumno" ).prop( "disabled", false );
         $( "#pcEgresa" ).prop( "disabled", true );
         $( "#pcRetSem" ).prop( "disabled", true );
         $( "#pcRetAno" ).prop( "disabled", true );
      }else if(val == 'Egresado'){
         $( "#pcAlumno" ).prop( "disabled", true );
         $( "#pcEgresa" ).prop( "disabled", false);
         $( "#pcRetSem" ).prop( "disabled", true );
         $( "#pcRetAno" ).prop( "disabled", true );
      }else if(val == 'Retiro'){
         $( "#pcAlumno" ).prop( "disabled", true );
         $( "#pcEgresa" ).prop( "disabled", true );
         $( "#pcRetSem" ).prop( "disabled", false );
         $( "#pcRetAno" ).prop( "disabled", false );
      }
   }   
</script>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">Trámites Administrativos - Llenado de Formulario </b></h3></div>
<form action="Paq1030.php" method="post"  enctype="multipart/form-data">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>AGREGAR DATOS AL TRÁMITE</b></h3></div>
      <!--<input type="hidden" name="paData[CCODTRE]" value="{$saData['CCODTRE']}"/>
      <input type="hidden" name="paData[CIDCATE]" value="{$saData['CIDCATE']}"/>
      <input type="hidden" name="paData[NSERIAL]" value="{$saData['NSERIAL']}"/>-->
      <div class="panel-body">    
        <div class="col-md-12 form-line">  
          <div class="form-group">
            <table class="table table-condensed table-responsive">   
               <tr class="warning2">
                  <th colspan="4">Datos del Alumno:</th>
               </tr><tr>
                  <th><label>Código: </label></th>
                  <th><label>{$saData['CCODALU']}</label></th>
               </tr><tr>
                  <th><label>Nombres: </label></th>
                  <th><label>{$saData['CNOMBRE']}</label></th>
               </tr>
               {if isset($saDatos['CCESTU'])}
               <tr class="warning2">
                  <th colspan="4">CERTIFICADO DE ESTUDIOS</th>
                  <input type="hidden" name="paData[CCESTU][CCODTRE]" value="{$saDatos['CCESTU']['CCODTRE']}"/>
                  <input type="hidden" name="paData[CCESTU][CIDCATE]" value="CCESTU"/>
                  <input type="hidden" name="paData[CCESTU][NSERIAL]" value="{$saDatos['CCESTU']['NSERIAL']}"/>
                  <input type="hidden" name="paData[CCESTU][MDETALL][CAPROBA]" value="SOLO APROBADAS"/>
                  <input type="hidden" name="paData[CCESTU][MDETALL][MANOTAC]" value=""/>
                  <input type="hidden" name="paData[CCESTU][MDETALL][CESTPER]" value="Egresado"/>
               </tr><tr>
                  <th >
                     <label class="form-check-label">Año de Egreso</label>
                  </th><th colspan="2">
                     <input type="text" class="form-control" id="pcEgresa" name="paData[CCESTU][MDETALL][CANOEGR]" required></th>
                  </th>
               </tr>
               <tr>
                  <th>
                     <label class="form-check-label">Modalidad</label>
                  </th><th>{*<input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODEST]" value="N" required/> CERTIFICADO DE ESTUDIOS EN FISICO*}</th>
                  <th><input onchange="f_ConformeChange2(this)" type="radio" name="paData[CCODEST]" value="S" required/> CERTIFICADO DE ESTUDIOS EN DIGITAL</th>
               </tr>
               {/if}
               {if isset($saDatos['CCCSID'])}
               <tr class="warning2">
                  <th colspan="4">CERTIFICADO DE IDIOMA EXTRANJERO</th>
                  <input type="hidden" name="paData[CCCSID][CCODTRE]" value="{$saDatos['CCCSID']['CCODTRE']}"/>
                  <input type="hidden" name="paData[CCCSID][CIDCATE]" value="CCCSID"/>
                  <input type="hidden" name="paData[CCCSID][NSERIAL]" value="{$saDatos['CCCSID']['NSERIAL']}"/>
               </tr><tr>
                  <th><label>Idioma Solicitado:</label></th>
                  <th colspan="2">
                  <select class="selectpicker form-control"  data-live-search="true" name="paData[CCCSID][MDETALL][CCURSO]" onChange="f_CambioIdioma(this.value);">
                    {foreach from = $saCurso item = i}  
                    <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
                    {/foreach}
                  </select>
                </th>
              </tr><tr>
                  <th><label>Tipo de Curso:</label></th>
                  <th colspan="2">
                    <select class="selectpicker form-control"  data-live-search="true" name="paData[CCCSID][MDETALL][CTIPCUR]">
                      {foreach from = $saDatos['CCCSID']['paTipCur'] item = i}
                      <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
                      {/foreach}
                    </select>
                  </th>
               </tr>
               {/if}
               {if isset($saDatos['CCSID2'])}
               <tr class="warning2">
                  <th colspan="4">CERTIFICADO DE IDIOMA EXTRANJERO</th>
                  <input type="hidden" name="paData[CCSID2][CCODTRE]" value="{$saDatos['CCSID2']['CCODTRE']}"/>
                  <input type="hidden" name="paData[CCSID2][CIDCATE]" value="CCCSID"/>
                  <input type="hidden" name="paData[CCSID2][NSERIAL]" value="{$saDatos['CCSID2']['NSERIAL']}"/>
               </tr><tr>
                  <th><label>Idioma Solicitado:</label></th>
                  <th colspan="2">
                  <select class="selectpicker form-control"  data-live-search="true" name="paData[CCSID2][MDETALL][CCURSO]" onChange="f_CambioIdioma(this.value);">
                    {foreach from = $saCurso item = i}  
                    <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
                    {/foreach}
                  </select>
                </th>
              </tr><tr>
                  <th><label>Tipo de Curso:</label></th>
                  <th colspan="2">
                    <select class="selectpicker form-control"  data-live-search="true" name="paData[CCSID2][MDETALL][CTIPCUR]">
                      {foreach from = $saDatos['CCCSID']['paTipCur'] item = i}
                      <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
                      {/foreach}
                    </select>
                  </th>
               </tr>
               {/if}
               {if isset($saDatos['000084'])}
               <tr class="warning2">
                  <th colspan="2">CERTIFICADO DE IDIOMA EXTRANJERO</th>
                  <input type="hidden" name="paData[000084][CCODTRE]" value="{$saDatos['000084']['CCODTRE']}"/>
                  <input type="hidden" name="paData[000084][CIDCATE]" value="000084"/>
                  <input type="hidden" name="paData[000084][NSERIAL]" value="{$saDatos['000084']['NSERIAL']}"/>
               </tr><tr>
                  <th><label>Idioma Solicitado:</label></th>
                  <th>
                  <select class="selectpicker form-control"  data-live-search="true" name="paData[000084][MDETALL][CCURSO]" onChange="f_CambioIdioma(this.value);">
                    {foreach from = $saCurso item = i}  
                    <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
                    {/foreach}
                  </select>
                </th>
              </tr><tr>
                  <th><label>Tipo de Curso:</label></th>
                  <th>
                    <select class="selectpicker form-control"  data-live-search="true" name="paData[000084][MDETALL][CTIPCUR]">
                      {foreach from = $saDatos['000084']['paTipCur'] item = i}
                      <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
                      {/foreach}
                    </select>
                  </th>
               </tr>
               {/if}
               {if isset($saDatos['CCCSUC'])}
               <tr class="warning2">
                  <th colspan="3">CERTIFICADO DE INSTITUTO DE INFORMÁTICA</th>
                  <input type="hidden" name="paData[CCCSUC][CCODTRE]" value="{$saDatos['CCCSUC']['CCODTRE']}"/>
                  <input type="hidden" name="paData[CCCSUC][CIDCATE]" value="CCCSUC"/>
                  <input type="hidden" name="paData[CCCSUC][NSERIAL]" value="{$saDatos['CCCSUC']['NSERIAL']}"/>
                  <input type="hidden" name="paData[CCCSUC][CTIPCUR]" value="CURSO REGULAR"/>
               </tr><tr>
                  <th><label>Modalidad:</label></th>
                  <th colspan="3">
                     <select class="selectpicker form-control"  data-live-search="true" name="paData[CCCSUC][MDETALL][CTIPCUR]">
                      {foreach from = $saDatos['CCCSUC']['paTipCur'] item = i}
                        <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
                        {/foreach}
                     </select>
                  </th>
               </tr>
               {/if}
            </table>
          </div>
        </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="Enviar" class="center-block btn btn-success btn-lg btn-block"> Enviar <i class="glyphicon glyphicon-ok"></i></button>
         </div>  
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 5}

      <!-- BANDEJA DE DATOS PENDIENTES PARA LLENADO DE FORMULARIO -->
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE TRÁMITES PENDIENTES</b></h3></div>
      <div class="panel-body">
        <b>ALUMNO: </b>  {$saData['CNOMBRE']}<br>
        <div class="table-responsive">
          <table class="table table-condensed"> 
          <thead>
            <tr> 
              <th class="col-xs-3" style='text-align: center'>Fecha Recepción</th>  
              <th class="col-xs-3" style='text-align: center'>Tipo Trámite</th>
              <th class="col-xs-1" style='text-align: center'>Activar</th>         
            </tr>
          </thead>
          {$j = 0}
          {foreach from = $saDatos item = i}
            <!--<input type='hidden' name='paDatos[{$j}][0]' value='{$i[0]}' >
            <input type='hidden' name='paDatos[{$j}][1]' value='{$i[1]}' >     
            <input type='hidden' name='paDatos[{$j}][2]' value='{$i[2]}' >
            <input type='hidden' name='paDatos[{$j}][3]' value='{$i[3]}'>
            <input type='hidden' name='paDatos[{$j}][4]' value='{$i[4]}'>
            <input type='hidden' name='paDatos[{$j}][5]' value='{$i[5]}'>-->
            <tr>
            <td style='text-align: center'>{$i['TFECREC']}</td>                 
            <td style='text-align: center'>{$i['CDESDOC']}</td>
            <td style='text-align: center'><input type="radio" name="pcIdDeud"  value="{$i['CIDLOG']}"/></td>
            </tr>
            {$j = $j + 1}
          {/foreach}
          </table>
        </div>  
      </div>
      </div>
        <div class="row">
          <div class="col-sm-4">
            <button type="submit" name="Boton" value="Agregar" class="center-block btn btn-success btn-lg btn-block"> Agregar Detalle <i class="glyphicon glyphicon-ok"></i></button>
          </div>  
          <div class="col-sm-4">
          </div>
          <div class="col-sm-4">
            <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
          </div>
        </div>      
      </div>
      {elseif $snBehavior == 1}
      <!-- LLENADO DE FORMULARIO PARA AUSPICIO ACADEMICO -->
      <div class="panel-heading"><h3 class="panel-title"><b>AGREGAR DATOS AL TRÁMITE</b></h3></div>
         <input type="hidden" name="paData[CCODTRE]" value="{$saData['CCODTRE']}"/>
         <input type="hidden" name="paData[CIDCATE]" value="{$saData['CIDCATE']}"/>
         <input type="hidden" name="paData[NSERIAL]" value="{$saData['NSERIAL']}"/>
        <div class="panel-body">    
        <div class="col-md-12 form-line">  
          <div class="form-group">
          <table class="table table-condensed">   
            <tr class="warning2">
               <th colspan="2">Datos del Alumno:</th>
            </tr><tr>
               <th><label>DNI: </label></th>
               <th><label>{$saData['CNRODNI']}</label></th>
            </tr><tr>
               <th><label>Nombres: </label></th>
               <th><label>{$saData['CNOMBRE']}</label></th>
            </tr><tr class="warning2">
               <th colspan="2">Detalle del Trámite:</th>
            </tr><tr>
               <th><label>Descripción: </label></th>
               <th><label>{$saData['CDESCRI']}</label></th>
            </tr><tr>
               <th><label>Fecha de Recepción: </label></th>
               <th><label>{$saData['TFECREC']}</label></th>
            </tr><tr class="warning2">
               <th colspan="2">Ingrese los Siguiente Datos:</th>
            </tr><tr>
               <th><label>Descripción del Proyecto:</label></th>
               <th><textarea class="form-control" rows="4"  name="paData[MDETALL][MDETALL]" placeholder="Escriba la descripcion del Proyecto..." style="text-transform: uppercase; resize:none"></textarea></th>
            </tr><tr>
               <th>Suba el Archivo: </th>
               <th><input type="file" name="fileToUpload" id="fileToUpload"></th>
            </tr>
         </table>
         </div>
      </div>   
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
         </div>  
         <div class="col-sm-4">
            
         </div>
         <div class="col-sm-4">
             <a href="Paq1030.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 2}
      <!-- LLENADO DE FORMULARIO PARA CERTIFICADO DE ESTUDIOS -->
      <div class="panel-heading"><h3 class="panel-title"><b>AGREGAR DATOS AL TRÁMITE</b></h3></div>
      <input type="hidden" name="paData[CCODTRE]" value="{$saData['CCODTRE']}"/>
      <input type="hidden" name="paData[CIDCATE]" value="{$saData['CIDCATE']}"/>
      <input type="hidden" name="paData[NSERIAL]" value="{$saData['NSERIAL']}"/>
      <div class="panel-body">    
         <div class="col-md-12 form-line">  
         <div class="form-group">
            <table class="table table-condensed table-responsive">   
            <tr class="warning2">
               <th colspan="5">Datos del Alumno:</th>
            </tr><tr>
               <th colspan="1"><label>Código: </label></th>
               <th colspan="4"><label>{$saData['CCODALU']}</label></th>
            </tr><tr>
               <th colspan="1"><label>Nombres: </label></th>
               <th colspan="4"><label>{$saData['CNOMBRE']}</label></th>
            </tr><tr class="warning2">
               <th colspan="5">Detalle del Trámite:</th>
            </tr><tr>
               <th colspan="1"><label>Descripción: </label></th>
               <th colspan="4"><label>{$saData['CDESCRI']}</label></th>
            </tr><tr>
               <th colspan="1"><label>Fecha de Recepción: </label></th>
               <th colspan="4"><label>{$saData['TFECREC']}</label></th>
            </tr><tr>
               <th colspan="1"><label>Semestres Pagados: </label></th>
               <th colspan="4"><label>{$saData['NCANSEM']}</label></th>
            </tr><tr>
               <th colspan="1">Certificado de Complementación Curricular: </th>
               <th colspan="4"><label>{$saData['CCURCOM']}</label></th>
            </tr><tr class="warning2">
               <th colspan="5">Ingrese los Siguiente Datos:</th>
            </tr><tr>
                  <th colspan="1">
                     <label class="form-check-label"><input class="form-check-input" type="radio" name="paData[MDETALL][CESTPER]" value="Alumno" onClick="f_EstadoPersona(this.value);" checked autofocus>
                        Alumno(a)</label>
                     </th>
                     <th colspan="1"><label class="form-check-label">Semestre</label></th>
                     <th colspan="3"><input type="text" class="form-control"  id="pcAlumno" name="paData[MDETALL][CSEMACT]" ></th>
                  </tr><tr>
                     <th colspan="1"><label class="form-check-label"><input class="form-check-input" type="radio"  name="paData[MDETALL][CESTPER]" value="Egresado" onClick="f_EstadoPersona(this.value);">
                        Egresado(a)</label>
                     </th>
                     <th colspan="1"><label class="form-check-label">Año</label></th>
                     <th colspan="3"><input type="text" class="form-control" id="pcEgresa" name="paData[MDETALL][CANOEGR]" disabled></th>
                  </tr><tr>
                     <th colspan="1"><label class="form-check-label"><input class="form-check-input" type="radio"  name="paData[MDETALL][CESTPER]" value="Retiro" onClick="f_EstadoPersona(this.value);">
                        Deje mis Estudios</label>
                     </th>
                     <th colspan="1"><label class="form-check-label">Semestre</label></th>
                     <th colspan="1"><input type="text" class="form-control" id="pcRetSem" name="paData[MDETALL][CSEMACT]" disabled></th>
                     <th colspan="1"><label class="form-check-label">Año</label></th>
                     <th colspan="1"><input type="text" class="form-control" id="pcRetAno" name="paData[MDETALL][CANOEGR]" disabled></th>
                  </tr><tr>
                     <th>Seleccione Semestres Deseados: </th>
                     <th colspan="5">
                        <table>
                           <tr>
                           {for $i=1 to 7}
                              <th style="width: 50px;" ><label class="form-check-label">{$i} <input type="checkbox" name="paData[MDETALL][CSEMEST][]" value="{$i}"></label></th>
                           {/for}
                           </tr><tr>
                           {for $i=8 to 14}
                              <th style="width: 50px;" ><label class="form-check-label">{$i} <input type="checkbox" name="paData[MDETALL][CSEMEST][]" value="{$i}"></label></th>
                           {/for}
                           </tr>
                        </table>
                     </th>
                  </tr><tr>
                     <th colspan="1">Asignaturas: </th>
                     <th colspan="4">
                        <select class="selectpicker form-control" data-live-search="true" name="paData[MDETALL][CAPROBA]">
                           <option value="SOLO APROBADAS">SOLO APROBADAS</option>  
                           <option value="APROBADAS Y DESAPROBADAS">APROBADAS Y DESAPROBADAS</option>  
                        </select>
                     </th>
                  </tr><tr>
                     <th colspan="1">Anotaciones: </th>
                     <th colspan="4">
                        <textarea class="form-control" rows="4"  name="paData[MDETALL][MANOTAC]" placeholder="Escriba las Anotaciones Necesarias Para el trámite..." style="text-transform: uppercase; resize:none"></textarea>
                     </th>
                  </tr><tr>
                  </tr>
               </table>
                  <label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* En caso de solicitar un certificado de Postgrado, especifique en el apartado de anotaciones el grado de estudio, asi como la cantidad de semestres solicitados</label>
            </div>
         </div>   
      </div>
      </div>
      <label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Los certificados utilizan la foto disponible en la UCSM. 
            Si desea cambiar su foto apersónese a la Oficina de Digitalización de Imagen con una foto tamaño pasaporte para realizar el cambio.</label>
      <div class="row">
         <div class="col-sm-4">
            <button type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
         </div>  
         <div class="col-sm-4">
            
         </div>
         <div class="col-sm-4">
             <a href="Paq1030.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {elseif $snBehavior == 3}
         
      <!-- ///////////////////////////////////////////////////////////// -->
      <!-- LLENADO DE FORMULARIO PARA EL CERTIFICADO DEL INSTITUTO DE IDIOMAS -->
      <!-- ///////////////////////////////////////////////////////////// -->
      <div class="panel-heading"><h3 class="panel-title"><b>AGREGAR DATOS AL TRÁMITE</b></h3></div>
         <input type="hidden" name="paData[CCODTRE]" value="{$saData['CCODTRE']}"/>
         <input type="hidden" name="paData[CIDCATE]" value="{$saData['CIDCATE']}"/>
         <input type="hidden" name="paData[NSERIAL]" value="{$saData['NSERIAL']}"/>
        <div class="panel-body">    
        <div class="col-md-12 form-line">  
          <div class="form-group">
          <table class="table table-condensed table-responsive">   
            <tr class="warning2">
               <th colspan="2">Datos del Alumno:</th>
            </tr><tr>
               <th><label>Código: </label></th>
               <th><label>{$saData['CCODALU']}</label></th>
            </tr><tr>
               <th><label>Nombres: </label></th>
               <th><label>{$saData['CNOMBRE']}</label></th>
            </tr><tr class="warning2">
               <th colspan="2">Detalle del Trámite:</th>
            </tr><tr>
               <th><label>Descripción: </label></th>
               <th><label>{$saData['CDESCRI']}</label></th>
            </tr><tr>
               <th><label>Fecha de Recepción: </label></th>
               <th><label>{$saData['TFECREC']}</label></th>
            </tr><tr class="warning2">
               <th colspan="2">Ingrese los Siguiente Datos:</th>
            </tr><tr>
               <th><label>Idioma Solicitado:</label></th>
               <th><select class="selectpicker form-control"  data-live-search="true" name="paData[MDETALL][CCURSO]" onChange="f_CambioIdioma(this.value);">
               {foreach from = $saCurso item = i}  
                  <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
               {/foreach}
               </select>
             </th>
            </tr><tr>
               <th><label>Tipo de Curso:</label></th>
               <th><select class="selectpicker form-control"  data-live-search="true" name="paData[MDETALL][CTIPCUR]">
               {foreach from = $saTipCur item = i}  
                  <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
               {/foreach}
               </select></th>
            </tr>
         </table>
          </div>
        </div>   
        </div>
      <label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Los certificados utilizan la foto disponible en la UCSM. 
            Si desea cambiar su foto apersónese a la Oficina de Informática con una foto tamaño pasaporte para realizar el cambio.</label>
      <div class="row col-md-4 col-md-push-8">       
        <button type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
        <a href="Paq1030.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
      </div>
      {elseif $snBehavior == 4}
      <!-- LLENADO DE FORMULARIO PARA CERTIFICADO DE INSTITUTO DE INFORMÁTICA -->
      <div class="panel-heading"><h3 class="panel-title"><b>AGREGAR DATOS AL TRÁMITE</b></h3></div>
         <input type="hidden" name="paData[CCODTRE]" value="{$saData['CCODTRE']}"/>
         <input type="hidden" name="paData[CIDCATE]" value="{$saData['CIDCATE']}"/>
         <input type="hidden" name="paData[NSERIAL]" value="{$saData['NSERIAL']}"/>
        <div class="panel-body">    
        <div class="col-md-12 form-line">  
          <div class="form-group">
          <table class="table table-condensed table-responsive">   
            <tr class="warning2">
               <th colspan="2">Datos del Alumno:</th>
            </tr><tr>
               <th><label>Código: </label></th>
               <th><label>{$saData['CCODALU']}</label></th>
            </tr><tr>
               <th><label>Nombres: </label></th>
               <th><label>{$saData['CNOMBRE']}</label></th>
            </tr><tr class="warning2">
               <th colspan="2">Detalle del Trámite:</th>
            </tr><tr>
               <th><label>Descripción: </label></th>
               <th><label>{$saData['CDESCRI']}</label></th>
            </tr><tr>
               <th><label>Fecha de Recepción: </label></th>
               <th><label>{$saData['TFECREC']}</label></th>
            </tr><tr class="warning2">
               <th colspan="2">Ingrese los Siguiente Datos:</th>
            </tr><tr>
               <th><label>Modalidad:</label></th>
               <th><select class="selectpicker form-control"  data-live-search="true" name="paData[MDETALL][CTIPCUR]">
               {foreach from = $saTipCur item = i}  
                  <option value="{$i['CDESCRI']}">{$i['CDESCRI']}</option>
               {/foreach}
               </select>
             </th>
            </tr>
            </table>
            </div>
         </div>   
         </div>
      <label style="font-size:14px; text-align:center; color: #9B0000; font-style: italic; ">* Los certificados utilizan la foto disponible en la UCSM. 
            Si desea cambiar su foto apersónese a la Oficina de Informática con una foto tamaño pasaporte para realizar el cambio.</label>
         <div class="row">
            <div class="col-sm-4">
               <button type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"> Grabar <i class="glyphicon glyphicon-ok"></i></button>
            </div>  
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
                <a href="Paq1030.tpl" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
            </div>
         </div>
      {/if}
      </div> 
      </div>      
    </div>    
  </div>
</form>

<div id="footer">
</div>
</body>
</html>