<html>
<head>
   <title>Trámites Administrativos</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-select.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
   <script src="Styles/js/bootstrap-select.js"></script>
</head>
<body class="divBody">
<div class="panel-heading divHeader" style="padding:0"><h3><b><img src="Images/ucsm-01.png" width="80" height="80"> Trámites Administrativos</b></h3></div>
<div class="container-fluid">
<form action="Paq1230.php" method="POST">
<div class="row">
<div class="col-md-8 col-md-push-2">
   {if $snBehavior == 0}
      <div class="panel panel-success">
         <div class="panel-heading">
            <h3 class="panel-title"><b>INSCRIPCCIÓN CURSO LIDERAZGO </b></h3>
         </div>
         <div class="panel-body">
            <input type="hidden" name="paData[CNRODNI]" value="{$saData['CNRODNI']}">
            <table class="table table-dark">
               <tr>
               <th class="col-xs-4">DNI del alumno</th>
               <td class="col-xs-8">{$saData['CNRODNI']}</td>
               </tr><tr>
               <th class="col-xs-4">Nombre del alumno</th>
               <td class="col-xs-8">{$saData['CNOMBRE']}</td>
               </tr><tr>
               <th class="col-xs-4">Nombre del curso</th>
               <td class="col-xs-8">LIDERAZGO MORAL Y EMPRESARIAL</td>
               </tr><tr>
               <th class="col-xs-4">Fecha del curso</th>
               <td class="col-xs-8">{$saDatos['DFECCUR']}</td>
               </tr><tr>
               </tr><tr>
               <th class="col-xs-4">Costo del curso</th>
               <td class="col-xs-8">S/.10</td>
               </tr><tr>
               <th class="col-xs-4">Detalles del curso</th>
               <td class="col-xs-8"><p><b>NOTA:</b> Debe llevar un folder</p>
               </td>
               </tr>
            </table>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-4 col-xs-6">
            <button name="Boton" value="Inscribirse" onclick="return confirm('Confirmar inscripción')" class="btn btn-success btn-lg btn-block">Inscribirse</button>
         </div>
         <div class="col-sm-4 d-xs-none"></div>
         <div class="col-sm-4 col-xs-6">
            <a href="Mnu0000.php" class="btn btn-danger btn-lg btn-block">Salir</a>
         </div>
      </div>
   {elseif $snBehavior == 1}
      <div class="panel panel-success">
      <div class="panel-body">
         <div class="alert alert-success" align="center">
            <h1><strong>¡Deuda Provisional Generada!</strong><br>Su ID de pago es: <br><strong>{$saData['CNROPAG']}</strong><br></h1>             
            <h4><strong>Indicar que el pago es por pensiones</strong></h4>
            <a href="Mnu2000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
         </div>
         <div class="alert alert-danger" align="left">
            <h4><u><strong>AVISO</strong></u></h4> 
            <h4><strong>La subida de archivos para el próximo bachiller es hasta el día 11 de marzo del 2019.</strong></h4> 
         </div>
         <div class="alert alert-success" align="left">
            <h4><u><strong>Tiempo Disponible en Bancos:</strong></u></h4> 
            <h4><strong>Para los días laborables:</strong></h4> 
            <h5>Si usted genera la deuda en la mañana antes de la <strong>1 p.m.</strong>, la deuda está disponible en los bancos a partir de las <strong>3 p.m.</strong>.<br> 
            Si genera la deuda pasada la <strong>1 p.m.</strong> recién estará disponible al día siguiente.<br>
            <h4><strong>Para los días no laborables:</strong></h4> Si la deuda fue generada un dia no laborable, la misma estará disponible en los bancos a partir de las <strong>10 a.m.</strong> del primer día laborable.</h5>
         </div>
      </div>
      </div>
   {elseif $snBehavior == 2}
      <div class="panel panel-success">
      <div class="panel-heading">
         <h3 class="panel-title"><b>INSCRIPCCIÓN CURSO LIDERAZGO </b></h3>
      </div>
      <div class="panel-body">         
         <table class="table">
            <tr>
            <th class="col-xs-4">DNI del alumno</th>
            <td class="col-xs-8">{$saData['CNRODNI']}</td>
            </tr><tr>
            <th class="col-xs-4">Nombre del alumno</th>
            <td class="col-xs-8">{$saData['CNOMBRE']}</td>
            </tr><tr>
            <th class="col-xs-4">Nombre del curso</th>
            <td class="col-xs-8">LIDERAZGO MORAL Y EMPRESARIAL</td>
            </tr><tr>
            <th>Estado</th>
            <td>
               {if $saData['CESTTUT'] == 'A'}
                  YA LLEVÓ EL CURSO
               {elseif $saData['CESTTUT'] == 'I'}
                  YA INSCRITO - SIN PAGAR
               {elseif $saData['CESTTUT'] == 'P'}
                  YA INSCRITO - PAGADO
               {/if}
            </td>
            </tr><tr>
            <th>Fecha generación</th>
            <td>{$saData['DGENERA']}</td>
            </tr>
         </table>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <a href="Mnu0000.php" class="btn btn-danger btn-lg btn-block">Salir</a>
         </div>
      </div>
   {/if}
</div>
</div>
</form>
</div>
</body>
</html>