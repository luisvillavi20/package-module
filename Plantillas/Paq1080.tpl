<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Trámites Administrativos</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1080.php" method="post" enctype="multipart/form-data">
  <div class="container divBody">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel panel-success">
      {if $snBehavior == 0}
      <div class="panel-heading"><h3 class="panel-title"><b>BANDEJA DE SUBIDA DE CERTIFICADO DE ESTUDIOS</b><span style="float: right"><b>ENCARGADO: </b>  {$saData['CNOMBRE']}</span></h3></div>
      <div class="panel-body">
         <div class="table-responsive mh-50">
            <table class="table table-condensed">
               <thead>
                  <th class="col-xs-1" style='text-align: center'>Fecha Recepción</th>
                  <th class="col-xs-1" style='text-align: center'>Cod. Tra.</th>
                  <th class="col-xs-2" style='text-align: center'>Nombre</th>
                  <th class="col-xs-2" style='text-align: center'>U. Académica</th>
                  <th class="col-xs-2" style='text-align: center'>Tramite</th>
                  <th class="col-xs-2" style='text-align: center'>Seleccionar archivo</th>
                  <th class="col-xs-1" style='text-align: center'>Subir archivo</th>
                  <th class="col-xs-1" style='text-align: center'>Estado</th>
               </thead>
               <tbody>
                  {$j = 0}
                  {foreach from = $saDatos item = i}
                     <form action="Paq1080.php" method="post" enctype="multipart/form-data">
                        <tr>
                           <td style='text-align: center'>{$i['TFECHA']}</td>
                           <td style='text-align: center'>{$i['CCODTRE']}</td>
                           <td style='text-align: left'>{$i['CNOMBRE']}</td>
                           <td style='text-align: left'>{$i['CNOMUNI']}</td>
                           <td style='text-align: left'>{$i['CDESCRI']}</td>
                           <td style="text-align: center">
                              {if $i['CESTADO'] == 'B'}
                                 <input type="hidden" name="paData[CCODTRE]" value="{$i['CCODTRE']}">
                                 <input type="hidden" name="paData[CNRODNI]" value="{$i['CNRODNI']}">
                                 <input name="pfDocBac" type="file" class="form-control-file" required>
                              {/if}
                           </td>
                           <td>
                              {if $i['CESTADO'] == 'B'}
                                 <button type="submit" name="Boton" value="Subir" class="center-block btn"/><i class="glyphicon glyphicon-upload"></i> Subir</button>
                               {/if}
                           </td>
                           <td style="text-align: center">
                              {if $i['CESTADO'] == 'F'}
                                 <img src="Images/pendiente.png" width="30%" data-toggle="tooltip" data-placement="button" title="Pendiente">
                              {elseif $i['CESTADO'] == 'A'}
                                 <img src="Images/subido.png" width="30%" data-toggle="tooltip" data-placement="button" title="Subido">
                              {elseif $i['CESTADO'] == 'R'}
                                 <img src="Images/revisado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Revisado">
                              {elseif $i['CESTADO'] == 'E'}
                                 <img src="Images/observado.png" width="30%" data-toggle="tooltip" data-placement="button" title="Observado">
                              {elseif $i['CESTADO'] == 'B'}
                                 <img src="Images/aprobado2.png" width="30%" data-toggle="tooltip" data-placement="button" title="Aprobado">
                              {elseif $i['CESTADO'] == 'M'}
                                 <img src="Images/mesa.png" width="30%" data-toggle="tooltip" data-placement="button" title="Mesa de partes">
                              {/if}
                           </td>
                        </tr>
                     </form>
                     {$j = $j + 1}
                  {/foreach}
               </tbody>
            </table>
         </div>
      </div>
      </div>
      <div class="row">
         <div class="col-sm-4"></div>
         <div class="col-sm-4"></div>
         <div class="col-sm-4">
            <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
         </div>
      </div>
      {/if}
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<div id="footer">
</div>
</body>
</html>