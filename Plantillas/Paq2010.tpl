<html>
  <head>
    <title>Mantenimiento de Autorizaciones</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Styles/css/bootstrap.css">
    <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
    <script src="Styles/js/jquery-3.2.0.js"></script>
    <script src="Styles/js/bootstrap.js"></script>
    <script type="text/javascript" src="CSS/jquery.modal.js"></script> 
  </head>
  <body>
    <div class="panel-heading" style="background-color: #245433; color: #ffffff;"><h3><b>Trámite administrativo</b></h3></div>
      <form action="Paq2010.php" method="post"> 
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="panel with-nav-tabs panel-success">
                <div class="panel-heading"><h4>Mantenimiento de Autorizaciones</h4></div>
                  <div class="panel-body">   
                    {if $snBehavior == 0}
                      <p class="text-muted"><b>{$saData['CNOMBRE']}</b></p>   
                      <div class="table-responsive">
                        <table class="table table-hover"> 
                          <th style='text-align: center'>Cod. Autorización</th>
                          <th style='text-align: center'>Certificado</th>
                          <th style='text-align: center'>Encargado</th>
                          <th style='text-align: center'>Estado</th>
                          <th style='text-align: center'>Fecha</th>
                          <th></th>
                          {$j = 1}
                          {foreach from = $saDatos item = i}
                            <tr>
                              <td style='text-align: center'>{$i['CCODAUT']}</td>
                              <td style='text-align: left'>{$i['CDESCRI']}</td>
                              <td style='text-align: left'>{$i['CNOMBRE']}</td>
                              {if $i['CESTADO']=='A'}
                                <td style='text-align: center; color: #6fe300'>ACTIVO</td>
                              {else}
                                <td style='text-align: center; color: #e60000'>INACTIVO</td>
                              {/if}
                              <td style='text-align: center'>{$i['TMODIFI']}</td>
                              <td style='text-align: center'><input type="radio" name="paData[CCOAUED]" value="{$i['CCODAUT']}"/></td>
                            </tr>
                            {$j = $j + 1}
                          {/foreach}
                        </table> 
                      </div>
                    </div>
                  </div>
                <div class="row"> 
                  <div class="col-sm-4">
                    <a class="center-block btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#myModal" role="button"> Nuevo <i class="glyphicon glyphicon-plus-sign"></i></a> 
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header modal-header-success">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title"><b>Crear autorización </b></h3>
                          </div>
                          <div class="modal-body">
                            <table class="table table-hover">
                              <tr>
                                 <td><b>CÓDIGO AUTORIZACIÓN </b></td>
                                 <td><input class="form-control" type='text' placeholder="Ingrese codigo de autorizacion..." maxlength="4" minlength="4"  name="paData[CCODAUT]" style="text-transform:uppercase"></td>
                              </tr>
                              <tr>
                                 <td><b>DOCUMENTO </b></td>
                                 <td>
                                    <select class="selectpicker form-control" data-live-search="true" name="paData[CIDCATE]">
                                       {$j = 0}
                                       <option selected="true" disabled="disabled">Seleccione documento...</option>
                                       {foreach from = $saDocs item = i}  
                                          <option value="{$i['CIDCATE']}">{$i['CIDCATE']} - {$i['CDESCRI']}</option> 
                                          {$j = $j + 1}
                                       {/foreach}
                                    </select>
                                 </td>
                              </tr>
                              <tr>
                                 <td><b>CODIGO DE USUARIO </b></td>
                                 <td><input class="form-control" type='text' placeholder="Ingrese codigo de usuario.." maxlength="4" minlength="4"  name="paData[CCODNUE]" style="text-transform:uppercase"></td>
                              </tr>
                            </table>     
                            <div class="modal-footer">
                              <div class="row">
                                <div class="col-sm-6"><input type="submit" name="Boton" value="Grabar" class="center-block btn btn-success btn-lg btn-block"/></div>
                                <div class="col-sm-6"><button type="button" class="center-block btn btn-danger btn-lg btn-block" data-dismiss="modal">Cancelar</button></div>
                              </div>  
                            </div>
                            </div>
                            </div>      
                         </div>
                         </div>
                      </div>     
                      <div class="col-sm-4">
                        <button type="submit" name="Boton" value="Editar" class="center-block btn btn-info btn-lg btn-block"/> Editar <i class="glyphicon glyphicon-edit"></i>
                      </div>
                      <div class="col-sm-4">
                        <a href="Mnu1000.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a>
                      </div>
                    </div>      
                    </div>
           {elseif $snBehavior == 1}
            <h4 class="modal-title"><b>Editar Autorización </b></h4>      
            <div class="modal-body">
               <table class="table table-hover">
                  <tr>
                     <td><b>CÓDIGO AUTORIZACIÓN </b></td>         
                     <td>
                        <input class="form-control" type='text' name="paData[CCODAUT]" value="{$saDatos[0]['CCODAUT']}" readonly>
                     </td>
                  </tr>
                  <tr>
                     <td><b>DOCUMENTO </b></td>
                     <td>
                        <select class="selectpicker form-control" data-live-search="true" name="paData[CIDCATE]">
                           <option selected="true" readonly value="{$saDatos[0]['CIDCATE']}">{$saDatos[0]['CIDCATE']}</option>
                           {foreach from = $saDocs item = i}  
                              <option value="{$i['CIDCATE']}">{$i['CIDCATE']} - {$i['CDESCRI']}</option> 
                           {/foreach}
                        </select>      
                     </td>         
                  </tr>
                  <tr>
                     <td><b>CODIGO DE USUARIO </b></td>
                     <td><input class="form-control" type='text' name="paData[CCODUSU]" value="{$saDatos[0]['CCODUSU']}" style="text-transform:uppercase"  ></td>
                  </tr>
                  <tr>
                     <td><b>ESTADO </b></td>
                     <td>
                        <select class="form-control" name="paData[CESTADO]">
                           <option value="A" {if $saDatos[0]['CESTADO'] == 'A'} selected="true"{/if}>ACTIVO</option>
                           <option value="I" {if $saDatos[0]['CESTADO'] == 'I'} selected="true"{/if}>INACTIVO</option>
                        </select>          
                     </td>         
                  </tr>
               </table>
            </div>
         </div>
      </div>
            <div class="row col-md-4 col-md-push-8">
                <button type="submit" name="Boton" value="Actualizar" class="center-block btn btn-success btn-lg btn-block"> Actualizar <i class="glyphicon glyphicon-refresh"></i></button>    
               <a href="Paq2010.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i> Salir</a> 
            </div>  
            </div>
         {/if}   
         </div>
         </div>
      </div>
      </div>
    </form>
</html>