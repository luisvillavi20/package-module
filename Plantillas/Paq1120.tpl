<html>
<head>
   <title>Trámites</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="Styles/css/bootstrap-theme.css">
   <link rel="stylesheet" href="Styles/css/scroll-bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="js/java.js"></script>
   <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.js"></script>
</head>
<body onload="f_Init()" class="divBody">
<div class="panel-heading divHeader"><h3><b><img src="Images/ucsm-01.png" width="80" height="80">    Verificador de Firmas Digitales</b></h3></div>
<!--Responsive!-->
<div class="container-fluid divBody table-responsive">
<form action="Paq1120.php" method="post" enctype="multipart/form-data">
   <div class="container divBody">
      <div class="row col-md-8 col-md-push-2">
         {if $snBehavior == 0}
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>SELECCIÓN DE DOCUMENTO </b></h3></div>
            <div class="panel-body">
               <div >
                  <table class="table table-condesed">
                     <tr>
                        <th>Suba el Archivo PDF: </th>
                        <th><input type="file" name="fileToUpload" id="fileToUpload"></th>
                     </tr>
                  </table>
               </div>
               <div class="row">
                     <div class="col-sm-4"></div>
                     <div class="col-sm-4">
                        <input class="btn btn-success btn-block btn-lg" type="submit"  name="Boton" value="Verificar"/>
                     </div>
                     <div class="col-sm-4"></div>
                  </div>   
            </div>
            <div class="row"></div>
         </div>
         {elseif $snBehavior == 1}
         <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title"><b>ESTADO DEL DOCUMENTO </b></h3></div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4">
                     {if $saData['CESTADO'] == 'A'}
                     <img class="img-responsive col-sm-8 col-sm-push-2" src="Images/aprobado.png">
                     {else}
                     <img class="img-responsive col-sm-8 col-sm-push-2" src="Images/denegar.png">
                     {/if}
                     <div>
                        <h3 class="text-center">{$saData['CDESCRI']}</h3>
                     </div>
                  </div>
                  <div class="col-sm-4"></div>
               </div>
               <div class="col-sm-4 col-sm-push-4" style="margin-top: 20px">
                  <input class="btn btn-danger btn-block btn-lg" type="submit"  name="Boton" value="Salir"/>
               </div>
            </div>
            <div class="row">
               
            </div>
         </div>
         {/if}
      </div>
   </div>
</form>
</div>
</body>
</html>