<!DOCTYPE html>
<html>
<head>
<title>Generar Deuda Provisional - Sesión de Invitados</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="bootstrap4/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap4/bootstrap-select.min.css">
<link rel="stylesheet" href="CSS/style.css">
<script src="js/jquery-3.1.1.min.js"></script>
<script src="bootstrap4/js/bootstrap.bundle.min.js"></script>
<script src="bootstrap4/bootstrap-select.min.js"></script>
<script>
   function f_onChangeConstancia() {
      var cDescri = $("#icConsta option:selected").text();
      cargarMonto(cDescri);  
   }

   function cargarMonto(p_cDescri) {
      p_cDescri = p_cDescri.substring(0, p_cDescri.indexOf('-'));
      p_cDescri = p_cDescri.slice(3, p_cDescri.length);
      $("#pnMonto").html(p_cDescri);
   }

   function f_inscribirse() {
      var lcNroDni = $('#pcNroDni').val();
      if (lcNroDni.trim().length != 8) {
         alert('Debe ingresar un DNI válido');
         return;
      }
      lcNroDni = lcNroDni.toUpperCase();
      var lcDigVer = $('#pcDigVer').val();
      var lcPriApe = $('#pcPriApe').val();
      if (lcPriApe.trim().length == 0) {
         alert('Debe ingresar su primer apellido');
         return;
      }
      var lcSegApe = $('#pcSegApe').val();
      var lcNombre = $('#pcNombre').val() 
      if (lcNombre.trim().length == 0) {
         alert('Debe ingresar su nombre');
         return;
      }
      var lcEmail  = $('#pcEmail').val();
      if (lcEmail.trim().length < 7) {
         alert('Debe ingresar su email');
         return; 
      }
      var lcNroCel = $('#pcNroCel').val();
      if (lcNroCel.trim().length < 6) {
         alert('Debe ingresar su número de celular');
         return;
      }
      var lcSend = "Id=RegistrarParticipanteTramites" +
                     "&paData[CNRODNI]=" + lcNroDni +
                     "&paData[CDIGVER]=" + lcDigVer +
                     "&paData[CPRIAPE]=" + lcPriApe +
                     "&paData[CSEGAPE]=" + lcSegApe +
                     "&paData[CNOMBRE]=" + lcNombre +
                     "&paData[CEMAIL]=" + lcEmail +
                     "&paData[CNROCEL]=" + lcNroCel;
      $.post("wsEventos.php",lcSend).done(function(p_cResult) {
         loJson = JSON.parse(p_cResult);
         if (loJson.ERROR) {
            alert(loJson.ERROR);
         } else {
            $('#Id').val('Generar');
            $('#poForm').submit();
         }
      });
   }

</script>
</head>
<body>
<div class="jumbotron jumbotron-fluid mb-3" style="background: #245433; color: #fff;">
   <div class="container-fluid text-center">
      <h1 class="display-5">GENERACIÓN DE DEUDA PROVISIONAL</h1>
      <p class="lead mb-0">Sesión de Invitados</p>
   </div>
</div>
<form id="poForm" action="invitados.php" method="POST">
<div class="container-fluid">
   {if $snBehavior == 0}
      <input type="hidden" name="Id" id="Id"/>
      <div class="card">
         <div class="p-1 card-body">
            <div class="input-group mb-0">
               <div class="input-group-prepend col-lg-2 px-0"><span class="input-group-text w-100 bg-ucsm">DNI</span></div>
               <input type="text" id="pcNroDni" name="paData[CNRODNI]" class="form-control text-uppercase" maxlength="8" value="{$saData['CNRODNI']}"  placeholder="NRO DE DNI (8 DÍGITOS)" required>
               <input type="text" id="pcDigVer" name="paData[CDIGVER]" class="form-control text-uppercase" maxlength="1" value="{$saData['CDIGVER']}"  placeholder="DÍGITO DE VERIFICACIÓN DEL DNI (A LA DERECHA DEL NÚMERO DE DNI)">
            </div>
            <small class="form-text mt-0 mb-1 text-12">* En caso de ser extranjero colocar P y los primeros 7 dígitos de su documento de identidad, dejando el campo de DÍGITO DE VERIFICACIÓN vacío. Ejemplo (P0028429).</small>
            <small class="form-text mt-0 mb-1 text-12">** En caso de no ser alumno, ni exalumno a la UCSM se le creará un usuario con su DNI como contraseña.</small>
            <div class="input-group mb-1">
               <div class="input-group-prepend col-lg-2 px-0"><span class="input-group-text w-100 bg-ucsm">Apellidos</span></div>
               <input type="text" id="pcPriApe" name="paData[CPRIAPE]" class="form-control text-uppercase" placeholder="PRIMER APELLIDO" value="{$saData['CPRIAPE']}" required>
               <input type="text" id="pcSegApe" name="paData[CSEGAPE]" class="form-control text-uppercase" placeholder="SEGUNDO APELLIDO" value="{$saData['CSEGAPE']}">
            </div>
            <div class="input-group mb-0">
               <div class="input-group-prepend col-lg-2 px-0"><span class="input-group-text w-100 bg-ucsm">Nombres</span></div>
               <input type="text" id="pcNombre" name="paData[CNOMBRE]" class="form-control text-uppercase" placeholder="NOMBRES" value="{$saData['CNOMBRE']}" required>
            </div>
            <div class="input-group mb-1">
               <div class="input-group-prepend col-lg-2 px-0"><span class="input-group-text w-100 bg-ucsm">E-mail</span></div>
               <input type="email" id="pcEmail" name="paData[CEMAIL]" class="form-control text-lowercase" placeholder="ejemplo@email.com" value="{$saData['CEMAIL']}" required>
            </div>
            <div class="input-group mb-1">
               <div class="input-group-prepend col-lg-2 px-0"><span class="input-group-text w-100 bg-ucsm">Celular</span></div>
               <input type="text" id="pcNroCel" name="paData[CNROCEL]" class="form-control text-uppercase" placeholder="+51 999 999 999" value="{$saData['CNROCEL']}" required>
            </div>
            <div class="input-group mb-1">
               <div class="input-group-prepend col-lg-2 px-0"><span class="input-group-text w-100 bg-ucsm">Concepto Pago</span></div>
               <select id="icConsta" class="selectpicker form-control text-12" data-live-search="true" data-size="9" name="paData[CIDCATE]" onchange="f_onChangeConstancia();" required>
                  <option class="text-12" value="" disabled selected>--------- CLICK PARA SELECCIONAR UN CONCEPTO DE PAGO ---------</option>
                  {foreach from = $saConsta item = i}
                  {$i['CIDCATE']}
                     <option class="text-12" value="{$i['CIDCATE']}" {if $saData['CIDCATE'] eq $i['CIDCATE']} selected {/if}>S/ {$i['NMONTO']}- {$i['CDESCRI']}</option>
                  {/foreach}
               </select>
            </div>
            <div class="input-group mb-1">
               <div class="input-group-prepend col-lg-2 px-0"><span class="input-group-text w-100 bg-ucsm">Resuelva la operación</span></div>
               <img src="Captcha.php"><input type="number" class="form-control" name="pcCaptcha" placeholder="Ingrese su respuesta" title="Resuelva la operación aritmética" required/>
            </div>
         </div>
         <div class="card-footer">
            <h1>Total: S/ <label id="pnMonto">0.00</label></h1>
         </div>
      </div>
      <div class="card mb-lg-5">
         <div class="p-1 text-center mb-lg-5"><button type="button" onclick="f_inscribirse();" class="btn bg-sc-2 col-lg-3" style="background: #245433; color: #fff;">Generar Deuda</button></div>
      </div>
   {elseif $snBehavior == 1}
      <div class="row">
         <div class="col-sm-3"></div>
         <div class="col-md-6">
            <div class="alert alert-success text-center">
               <h1><strong>¡Deuda Provisional Generada!</strong><br>Su ID de pago es: <br><strong>{$saData['CNROPAG']}</strong><br></h1>
               <h4>Válido sólo por {$saData['NDURACI']} días calendario</h4>
               <h4><strong>Indicar que el pago es por <u>PENSIONES</u></strong></h4>
               <a href="invitados.php" class="center-block btn btn-danger btn-lg btn-block" role="button"><i class="glyphicon glyphicon-log-out"></i>Salir</a>
            </div>
            <div class="alert alert-success text-justify">
               <h4><u><strong>Centros de Recaudación:</strong></u></h4>
               <h5>Las entidades financieras asociadas a la Universidad (BCP, Caja Arequipa, Interbank y Scotiabank).</h5>
               <h5>El pago puede realizarse a través de los medios que brinden las entidades financieras antes mencionadas (ventanilla, agente, aplicativo, etc).</h5>
               <h5>La deuda se verá reflejada en la entidad financiera en un plazo máximo de 30 minutos.</h5>
            </div>
         </div>  
      </div>
   {/if}
</div>
<footer class="mt-5">
   <div class="container"><span class="text-muted">© 2018 Universidad Católica de Santa María - Sistema de Eventos y Certificados Digitales. Todos los derechos reservados. <a target="_blank" rel="nofollow" href="http://www.ucsm.edu.pe">UCSM</a></span></div>
</footer>
</form>
</body>
</html>