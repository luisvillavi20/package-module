<?php
require_once 'class/PHPMailer/PHPMailer.php';
require_once 'class/PHPMailer/Exception.php';
require_once 'class/PHPMailer/OAuth.php';
require_once 'class/PHPMailer/POP3.php';
require_once 'class/PHPMailer/SMTP.php';
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
class CEmail {
   protected $php_mailer, $lcUser, $lcPass, $lcCopia, $lcRutAdj, $lcDocAdj;
   public $paData, $pcError;

   public function __construct() {
      $this->php_mailer = null;
      $this->paData     = null;
      $this->pcError    = null;
      $this->lcUser     = '';
      $this->lcPass     = '';
      $this->lcCopia    = null;
      $this->lcRutAdj   = null;
      $this->lcDocAdj   = null;
   }

   public function omIngresarOrigen($p_cUser, $p_cPass) {
      $llOk = $this->mxValParamIngresarOrigen();
      if (!$llOk) {
         return false;
      }
      $this->lcUser = $p_cUser;
      $this->lcPass = $p_cPass;
      return true;
   }

   protected function mxValParamIngresarOrigen() {
      return true;
   }

   public function omAñadirDestinosCopia($p_cCopia) {
      $llOk = $this->mxValParamIngresarCopia();
      if (!$llOk) {
         return false;
      }
      $this->lcCopia = $p_cCopia;
      return true;
   }

   protected function mxValParamIngresarCopia() {
      return true;
   }

   public function omAñadirDocumento($p_RutAdj, $p_DocAdj) {
      $llOk = $this->mxValParamAñadirDocumento();
      if (!$llOk) {
         return false;
      }
      $this->lcRutAdj = $p_RutAdj;
      $this->lcDocAdj = $p_DocAdj;
      return true;
   }

   protected function mxValParamAñadirDocumento() {
      return true;
   }

   public function omConnect() {
      $this->php_mailer = new PHPMailer();
      $this->php_mailer->IsSMTP(); // enable SMTP
      $this->php_mailer->CharSet = 'UTF-8';
      //$this->php_mailer->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $this->php_mailer->SMTPAuth = true; // authentication enabled
      $this->php_mailer->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
      //$this->php_mailer->Host = "smtp.gmail.com";
      $this->php_mailer->Host = 'smtp.office365.com';
      $this->php_mailer->Port = 465; // or 587
      $this->php_mailer->IsHTML(true);
      /*$this->php_mailer->Username = "efb.devs@gmail.com";
      $this->php_mailer->Password = "SistemasFPM";*/
      $this->php_mailer->Username = $this->lcUser;
      $this->php_mailer->Password = $this->lcPass;  
      $this->php_mailer->SMTPSecure = 'tls';
      $this->php_mailer->Port = 25;
      $this->php_mailer->SetFrom($this->lcUser);
      
      //PARA PROBAR LOCALMENE EN XAMPP
      $this->php_mailer->SMTPOptions = array(
         'ssl' => array(
         'verify_peer' => false,
         'verify_peer_name' => false,
         'allow_self_signed' => true
         )
      );

      $this->php_mailer->addAttachment($this->lcRutAdj, $this->lcDocAdj,  'base64', 'application/pdf');
      $this->php_mailer->addCC($this->lcCopia);
      return true;
   }

   public function omSend() {
      $llOk = $this->mxValParamSend();
      if (!$llOk) {
         return false;
      }
      $llOk = $this->mxAñadirDestinos();
      if (!$llOk) {
         return false;
      }
      $this->php_mailer->Subject = $this->paData['CSUBJEC'];
      $this->php_mailer->AddEmbeddedImage('Images/ucsm-03.png', 'logo');
      $this->php_mailer->Body = '<div><img src="cid:logo" height="80"></div><br>';
      $this->php_mailer->Body .= $this->paData['CBODY'];
      if (!$this->php_mailer->Send()) {
         $this->pcError = $this->php_mailer->ErrorInfo;
         return false;
      }
      return true;
   }

   protected function mxValParamSend() {
      if (!isset($this->paData['CSUBJEC']) || empty($this->paData['CSUBJEC'])) {
         $this->pcError = "ASUNTO DEL EMAIL NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['AEMAILS']) || count($this->paData['AEMAILS']) == 0) {
         $this->pcError = "EMAIL DESTINO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CBODY']) || empty($this->paData['CBODY'])) {
         $this->pcError = "CUERPO DEL EMAIL NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxAñadirDestinos() {
      foreach ($this->paData['AEMAILS'] as $value) {
         $this->php_mailer->AddAddress($value);
      }
      return true;
   }

   public function omSendBachillerato() {
      $llOk = $this->mxValParamSendBachillerato();
      if (!$llOk) {
         return false;
      }
      $llOk = $this->mxAñadirDestinosBachillerato();
      if (!$llOk) {
         return false;
      }
      $this->php_mailer->Subject = $this->paData['CSUBJEC'];
      //$this->php_mailer->AddEmbeddedImage('Images/Colacion.png', 'imagenColacion');
      $this->php_mailer->Body = $this->paData['CBODY'];
      //$this->php_mailer->Body .= '<div><img src="cid:imagenColacion" height="580" class="center"></div><br>';
      if (!$this->php_mailer->Send()) {
         $this->pcError = $this->php_mailer->ErrorInfo;
         return false;
      }
      return true;
   }

   protected function mxValParamSendBachillerato() {
      if (!isset($this->paData['CSUBJEC']) || empty($this->paData['CSUBJEC'])) {
         $this->pcError = "ASUNTO DEL EMAIL NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['AEMAILS']) || count($this->paData['AEMAILS']) == 0) {
         $this->pcError = "EMAIL DESTINO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CBODY']) || empty($this->paData['CBODY'])) {
         $this->pcError = "CUERPO DEL EMAIL NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxAñadirDestinosBachillerato() {
      foreach ($this->paData['AEMAILS'] as $value) {
         $this->php_mailer->AddAddress($value);
      }
      return true;
   }

   public function omSendTitulacion() {
      $llOk = $this->mxValParamSendTitulacion();
      if (!$llOk) {
         return false;
      }
      $llOk = $this->mxAñadirDestinosTitulacion();
      if (!$llOk) {
         return false;
      }
      $this->php_mailer->Subject = $this->paData['CSUBJEC'];
      //$this->php_mailer->AddEmbeddedImage('Images/Colacion.png', 'imagenColacion');
      $this->php_mailer->Body = $this->paData['CBODY'];
      //$this->php_mailer->Body .= '<div><img src="cid:imagenColacion" height="580" class="center"></div><br>';
      if (!$this->php_mailer->Send()) {
         $this->pcError = $this->php_mailer->ErrorInfo;
         return false;
      }
      return true;
   }

   protected function mxValParamSendTitulacion() {
      if (!isset($this->paData['CSUBJEC']) || empty($this->paData['CSUBJEC'])) {
         $this->pcError = "ASUNTO DEL EMAIL NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['AEMAILS']) || count($this->paData['AEMAILS']) == 0) {
         $this->pcError = "EMAIL DESTINO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CBODY']) || empty($this->paData['CBODY'])) {
         $this->pcError = "CUERPO DEL EMAIL NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxAñadirDestinosTitulacion() {
      foreach ($this->paData['AEMAILS'] as $value) {
         $this->php_mailer->AddAddress($value);
      }
      return true;
   }

   public function omSendCETraduccion() {
      $llOk = $this->mxValParamSendCETraduccion();
      if (!$llOk) {
         return false;
      }
      $llOk = $this->mxAñadirDestinosCETraduccion();
      if (!$llOk) {
         return false;
      }
      $this->php_mailer->Subject = 'ARCHIVO DE TRADUCCIÓN';
      $this->php_mailer->AddEmbeddedImage('Images/ucsm-03.png', 'logo');
      $this->php_mailer->Body = '<div><img src="cid:logo" height="80"></div><br>';
      $this->php_mailer->Body .= 'ADJUNTO ARCHIVO DE TRADUCCIÓN';
      $this->php_mailer->AddAttachment($this->paData['CARCHIV'],$this->paData['CARCHNM']);
      if (!$this->php_mailer->Send()) {
         $this->pcError = $this->php_mailer->ErrorInfo;
         return false;
      }
      return true;
   }

   protected function mxValParamSendCETraduccion() {
      if (!isset($this->paData['CARCHIV']) || empty($this->paData['CARCHIV'])) {
         $this->pcError = "ARCHIVO NO ADJUNTADO";
         return false;
      }
      return true;
   }

   protected function mxAñadirDestinosCETraduccion() {
      foreach ($this->paData['AEMAILS'] as $value) {
         $this->php_mailer->AddAddress($value);
      }
      return true;
   }
}
?>