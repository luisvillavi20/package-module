<?php
require_once "Clases/CBase.php";
require_once "Clases/CSql.php";
require_once 'Clases/CEmail.php';
require_once 'Clases/CWebService.php';
require_once 'PDF/CODE128.php';
require_once 'PDF/fpdf.php';
// Gestion de paquetes por usuario - centro de costo
class CMatricula extends CBase {
   public $paData, $paDatos, $paEstado, $pcFile, $paCursos, $paSolCnv, $paTipCon, $paAluAnt, $paAluAct, $paCurCnv, $paDepAca,
          $paSolJur;
   protected $laData, $lnCosCre, $laDatos;

   public function __construct() {
      parent::__construct();
      $this->paData = $this->laData = $this->paDatos = $this->paEstado = $this->paCursos = $this->paSolCnv = $this->paTipCon =
      $this->paAluAnt = $this->paAluAct = $this->paCurCnv = $this->paDepAca = $this->paSolJur = null;
      $this->pcFile = 'FILES/R' . rand() . '.pdf';
   }

   // ------------------------------------------------------------------------------
   // Init modificar el creditaje de matricula del alumno
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitMatriculaxCreditos() {
      $llOk = $this->mxValParamInitMatriculaxCreditos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitMatriculaxCreditos($loSql);
      $loSql->omDisconnect();
      $this->paData = $this->laData;  
      return $llOk;
   }

   protected function mxValParamInitMatriculaxCreditos() {
      if (!isset($this->paData['CCODALU']) or (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'PARÁMETRO CÓDIGO DE ALUMNO INVÁLIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxInitMatriculaxCreditos($p_oSql) {
      $lcSql = "SELECT TRIM(cConten) FROM S01TVAR WHERE cVariab = 'PCPERCRE';";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);     
      if (!isset($laTmp[0])) {
         $this->pcError = "PERIODO DE MATRICULAS POR CREDITAJE NO EXISTE";
         return false;
      }
      $lcSql = "SELECT B.cPeriod, A.cUniAca, B.nCosCre FROM A01MALU A
                 INNER JOIN B05DCCR B ON B.cUniAca = A.cUniAca
                 WHERE B.cEstado = 'A' AND B.cTipo = 'C' AND A.cCodAlu = '{$this->paData['CCODALU']}' AND B.cPeriod = '{$laTmp[0]}';";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);     
      if (!isset($laTmp[0])) {
         $this->pcError = "COSTO POR CRÉDITO NO ENCONTRADO";
         return false;
      }
      $this->laData = ['CPERIOD' => $laTmp[0], 'CUNIACA' => $laTmp[1], 'NCOSCRE' => $laTmp[2]];
      $lcSql = "SELECT nSerial, cEstado, nCredit, nMonto, cCodAlu, cPeriod, cEstInf FROM B05DMPC 
                WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cPeriod = '{$this->laData['CPERIOD']}' AND cEstado IN ('P', 'U') 
                ORDER BY nSerial DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->laData = $this->laData + ['NSERIAL' => -1, 'NMONTO' => 0.00, 'CESTADO' => NULL, 'NCREREG' => 0.00];
         return true;
      }   
      $this->laData = $this->laData + ['NSERIAL' => $laTmp[0], 'CESTADO' => $laTmp[1], 
                                       'NCREDIT' => $laTmp[2], 'NMONTO' => $laTmp[3], 
                                       'CCODALU' => $laTmp[4], 'CPERIOD' => $laTmp[5],
                                       'CESTINF' => $laTmp[6]];

      $lcSql = "SELECT SUM(NCREDIT) FROM B05DMPC WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cPeriod = '{$this->laData['CPERIOD']}' AND cEstado = 'U'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);    
      $this->laData = $this->laData + ['NCREREG' => $laTmp[0]];
      return true; 
   }
   
   // ------------------------------------------------------------------------------
   // Registra deuda de matricula por creditos
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRegistrarMatricula() {
      $llOk = $this->mxValParamRegistrarMatricula();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
     
      $llOk = $this->mxInitMatriculaxCreditos($loSql);
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }   
      $llOk = $this->mxRegistrarMatricula($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRegistrarMatricula() {
      $this->paData['NCREDIT'] = (float)$this->paData['NCREDIT']; 
      $this->laData['NCOSCRE'] = (float)$this->laData['NCOSCRE'];
      $this->paData['NMONTO']  = (float)$this->paData['NMONTO'];
      if ((!preg_match("(^[0-9]{1,2}(\.5){0,1}$)", $this->paData['NCREDIT'])) or $this->paData['NCREDIT'] > 11.5 or $this->paData['NCREDIT'] <= 0) {
         $this->pcError = 'VALOR DE CREDITAJE INVÁLIDO';
         return false;
      } elseif (!isset($this->paData['CCODALU']) or (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'CÓDIGO DE ALUMNO INVÁLIDO';
         return false;
      }
      return true;
   }

   protected function mxRegistrarMatricula($p_oSql) {
      $lcSql = "SELECT TRIM(cConten) FROM S01TVAR WHERE cVariab = 'PCPERCRE'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "PERIODO DE MATRICULAS POR CREDITAJE NO EXISTE";
         return false;
      }
      $laTmp = $p_oSql->fetch($R1);
      $lcPeriod = $laTmp[0];
      if (!in_array($this->laData['CESTADO'], ['U', NULL])) {
         $this->pcError = "ESTADO DE TRANSACCIÓN ERRADO - REINTENTE";
         return false;
      } elseif ($this->laData['NCOSCRE'] != $this->paData['NCOSCRE']) {
         $this->pcError = "ERROR AL VALIDAR COSTO DE CREDITAJE - REINTENTE";
         return false;
      } elseif ($this->laData['CPERIOD'] != $this->paData['CPERIOD']) {
         $this->pcError = "ERROR AL VALIDAR COSTO DE CREDITAJE - REINTENTE";
         return false;
      } elseif (round($this->laData['NCOSCRE'] * $this->paData['NCREDIT'], 2) != $this->paData['NMONTO']) {
         $this->pcError = "ERROR AL VALIDAR MONTO - REINTENTE";
         return false;
      }
      // Valida que el alumno no exceda a mas de 11.6
      $lcSql = "SELECT SUM(NCREDIT) FROM B05DMPC WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado IN('U','P') AND cPeriod IN ('$lcPeriod')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      $this->laData = ['NSUMA' => $laTmp[0]];
      if ($this->paData['NCREDIT'] + $laTmp[0] >= 11.6) {
         $this->pcError = 'EXCEDE CANTIDAD DE CRÉDITOS LA CANTIDAD MÁXIMA DE CRÉDITOS ES DE 11.5';
         return false;
      }
      // Valida que exista codigo de alumno
      $lcSql = "SELECT cCodAlu FROM A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "CÓDIGO DE ALUMNO NO EXISTE";
         return true;
      }
      //ESTE 
      /* //cuando solo el ultimo registro por ingresar se vuelve a estado reingreso CTIPO = 'R'
      $lcSql = "INSERT INTO B05DMPC (cCodAlu, cPeriod, cTipo, nCredit, nMonto, cUsuCod) 
                VALUES ('{$this->paData['CCODALU']}', '{$this->paData['CPERIOD']}', 'R' ,{$this->paData['NCREDIT']}, {$this->paData['NMONTO']}, 'U666')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL INSERTAR MODIFICACIÓN DE CREDITAJE";
         return false;
      }
      return true;
      */ 

      //O ESTE
      // verifica si es que existe un registro anterios para que vuelva a todos los registros anteriores en CTIPO a estado 'R'
      $lcSql = "SELECT nSerial FROM B05DMPC WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado = 'U' AND (cEstInf = 'M' OR cEstInf = 'E')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) { //Si es que no existe un registro se solo se registra con CTIPO 'N'
         $lcSql = "INSERT INTO B05DMPC (cCodAlu, cPeriod, nCredit, nMonto, cUsuCod) 
                   VALUES ('{$this->paData['CCODALU']}', '$lcPeriod', {$this->paData['NCREDIT']}, {$this->paData['NMONTO']}, 'U666')"; // cTipo por defecto se ingreSa en estado 'N' [N: Matricula Normal]
         
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL INSERTAR MODIFICACIÓN DE CREDITAJE";
            return false;
         }
         return true;
      } // SI NO SE INGRESA CON CTIPO R DE RETIFICACION
      $lcSql = "INSERT INTO B05DMPC (cCodAlu, cPeriod, cTipo, nCredit, nMonto, cUsuCod)  
                VALUES ('{$this->paData['CCODALU']}', '$lcPeriod', 'R' ,{$this->paData['NCREDIT']}, {$this->paData['NMONTO']}, 'U666')"; //cTipo se ingresa en estado R: Matricula Retificacion
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL INSERTAR MODIFICACIÓN DE CREDITAJE";
         return false;
      }
      /*$lcSql = "UPDATE B05DMPC SET  cTipo = 'R', tModifi = NOW() WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado = 'U'";       
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL ACTUALIZAR REGISTROS PARA LA RETIFICACIÓN";
         return false;
      }*/
      // cuando este en estado enviado recien lo pongo po retificacion
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualiza matricula por creditos
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------
      public function omActualizarMatricula() {
      $llOk = $this->mxValParamActualizarMatricula();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      
      $llOk = $this->mxInitMatriculaxCreditos($loSql); 
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }
      $llOk = $this->mxActualizarMatricula($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamActualizarMatricula() {
      $this->paData['NCREDIT'] = (float)$this->paData['NCREDIT'];
      $this->laData['NCOSCRE'] = (float)$this->laData['NCOSCRE'];
      $this->paData['NMONTO']  = (float)$this->paData['NMONTO'];
      $this->paData['NSERIAL'] = (int)$this->paData['NSERIAL'];
      if (!preg_match("(^[0-9]{1,2}(\.5){0,1}$)", $this->paData['NCREDIT']) or $this->paData['NCREDIT'] > 11.5 or $this->paData['NCREDIT'] <= 0) {
         $this->pcError = 'VALOR DE CREDITAJE INVÁLIDO';
         return false;
      } elseif (!isset($this->paData['CCODALU']) or (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'CÓDIGO DE ALUMNO INVÁLIDO';
         return false;
      } elseif ($this->paData['NSERIAL'] <= 0) {
         $this->pcError = 'ID.SERIAL INVÁLIDO';
         return false;
      }
      return true;
   }

   protected function mxActualizarMatricula($p_oSql) {
      if (in_array($this->laData['CESTADO'], ['U', 'X'])) {
         $this->pcError = "ESTADO DE TRANSACCIÓN ERRADO PARA ACTUALIZAR - REINTENTE";
         return false;
      } elseif ($this->laData['NSERIAL'] != $this->paData['NSERIAL']) {
         $this->pcError = "ERROR DE ESTADO DE TRANSACCIÓN SERIAL PARA ACTUALIZAR - REINTENTE";
         return false;
      } elseif ($this->laData['NCOSCRE'] != $this->paData['NCOSCRE']) {
         $this->pcError = "ERROR AL VALIDAR COSTO DE CREDITAJE - REINTENTE";
         return false;
      } elseif ($this->laData['NCREDIT'] == $this->paData['NCREDIT']) {
         $this->pcError = "LA CANTIDAD DE CRÉDITOS ES LA MISMA";
         return false;
      } elseif (round($this->laData['NCOSCRE'] * $this->paData['NCREDIT'], 2) != $this->paData['NMONTO']) {
         $this->pcError = "ERROR AL VALIDAR MONTO - REINTENTE";
         return false;
      }      
      $lcSql = "SELECT SUM(NCREDIT) FROM B05DMPC WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado = 'U' AND cPeriod = '201901'"; 
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      $this->laData = ['NSUMA' => $laTmp[0]];
      if ($this->paData['NCREDIT'] + $laTmp[0] >= 11.6) {
         $this->pcError = 'EXCEDE CANTIDAD DE CRÉDITOS, LA CANTIDAD MÁXIMA DE CRÉDITOS ES DE 11.5';
         return false;
      } 
      $lcSql = "UPDATE B05DMPC SET nCredit = {$this->paData['NCREDIT']}, nMonto = {$this->paData['NMONTO']}, cEstPro = 'A', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR PENSIÓN POR CRÉDITOS';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Anula la matricula por creditos
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAnularMatricula() {
      $llOk = $this->mxValParamAnularMatricula();
      if (!$llOk) {
         return false;
      }
      $llOk = $this->omInitMatriculaxCreditos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAnularMatricula($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAnularMatricula() {
      $this->paData['NSERIAL'] = (int)$this->paData['NSERIAL'];
      if (!isset($this->paData['CCODALU']) or (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'PARÁMETRO CÓDIGO DE ALUMNO INVÁLIDO O NO DEFINIDO';
         return false;
      } elseif ($this->paData['NSERIAL'] <= 0) {
         $this->pcError = 'ID.SERIAL INVÁLIDO';
         return false;
      }
      return true;
   }

   protected function mxAnularMatricula($p_oSql) {
      if (!in_array($this->laData['CESTADO'], ['P'])) {
         $this->pcError = "ESTADO DE TRANSACCIÓN ERRADO PARA ACTUALIZAR - REINTENTE";
         return false;
      } elseif ($this->laData['NSERIAL'] != $this->paData['NSERIAL']) {
         $this->pcError = "ERROR DE ESTADO DE TRANSACCIÓN SERIAL PARA ACTUALIZAR - REINTENTE";
         return false;
      }
      $lcSql = "UPDATE B05DMPC SET cEstado = 'X', cEstPro = 'A', tModifi = NOW() WHERE nSerial = {$this->paData['NSERIAL']}";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ANULAR PENSIÓN POR CRÉDITOS';
         return false;
      }
      return true;// CUANDO ANULA O CUANDO CAMBIAS DE CREDITOS PASAR A ESTADO 'A' PORQUE CARLOS LO PASA A ESTADO P
   }

   // ------------------------------------------------------------------------------
   // Detalle de deudas generadas la matricula por creditos
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------

   public function omDetalleDeuda() {
      $llOk = $this->mxValParamDetalleDeuda();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDetalleDeuda($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDetalleDeuda() {
      if (!isset($this->paData['CCODALU']) or (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'PARÁMETRO CÓDIGO DE USUARIO INVÁLIDO O NO DEFINIDO'; 
         return false;
      } 
      return true;
   }

   protected function mxDetalleDeuda($p_oSql) {
      $lcSql = "SELECT cCodAlu, cPeriod, cEstado, nCredit, nMonto, cEstPro, TO_CHAR(tGenera, 'yyyy-mm-dd hh24:mi'), cEstInf FROM B05DMPC 
      WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado = 'U' AND cPeriod = '201901' ORDER BY nSerial";     
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paDetall'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
            $this->paDatos['paDetall'][] = ['CCODALU' => $laFila[0], 'CPERIOD' => $laFila[1], 
                                            'CESTADO' => $laFila[2], 'NCREDIT' => $laFila[3], 
                                            'NMONTO' => $laFila[4], 'CESTPRO' => $laFila[5], 
                                            'TGENERA' => $laFila[6], 'CESTINF' => $laFila[7]];
      }
      $lcSql = "SELECT SUM(nMonto), SUM(nCredit) FROM B05DMPC WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado = 'U'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "MONTOS NO DEFINIDOS";
         return false;
      }
      $this->paDatos['paTotal'] = ['NMONTOT' => $laFila[0], 'NCRETOT' => $laFila[1]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init cursos por jurado
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitCursosPorJurado() {
      $llOk = $this->mxValParamInitCursosPorJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitCursosPorJurado($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitCursosPorJurado() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DE ALUMNO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CUSUCOD']) || (strlen($this->paData['CUSUCOD']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxInitCursosPorJurado($p_oSql) {
      //$laData = ['CCODALU'=>'2014402321', 'CUSUCOD'=>$_SESSION['GCCODUSU']];
      $lcSql = "SELECT nDescue FROM B01DAUT WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cTipAut = '004' AND cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $laData['NDESCUE'] = $laFila[0];
      if (!isset($laFila[0])) {
         $laData['NDESCUE'] = 1;
      }
      $lcSql = "SELECT nCosCre FROM B05DCCR A
                INNER JOIN A01MALU B ON B.cUniAca = A.cUniAca
                WHERE B.cCodAlu = '{$this->paData['CCODALU']}' AND cTipo = 'J'";      
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "COSTO POR CRÉDITO NO ENCONTRADO";
         return false;
      }
      $laData['NCOSCRE'] = $laFila[0];
      $lo = new CWebService();
      $lo->paData = $this->paData;
      $llOk = $lo->omRevisarCursosJurado();
      if (!$llOk) {
         $this->pcError = $lo->pcError;
         return false;
      }
      $this->paDatos['paCursos'] = [];
      foreach ($lo->paData['MDATOS'] as $laFila) {
         $this->paDatos['paCursos'][] = $laFila + ['NMONTO' => strpos($laFila['CCODCUR'], 'CC') ? (100 * $laData['NDESCUE']) : (($laFila['NCREDIT'] * $laData['NCOSCRE']) * $laData['NDESCUE'])];
         continue;
      }
      $lcSql = "SELECT B.nSerial, A.cCodAlu, B.cCodCur, C.cDescri, B.cEstEsc, B.cEstSac, TO_CHAR(A.dGenera, 'YYYY-MM-DD'), C.nCreTeo + C.nCreLab, B.nMonto
                FROM B05MCPJ A
                INNER JOIN B05DCPJ B ON B.cIdenti = A.cIdenti
                INNER JOIN A02MCUR C ON C.cCodCur = B.cCodCur
                INNER JOIN A01MALU D ON D.cCodAlu = A.cCodAlu
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado IN ('S','A','O','E')";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paInscr'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paInscr'][] = ['NSERIAL' => $laFila[0], 'CCODALU' => $laFila[1],
                                        'CCODCUR' => $laFila[2], 'CDESCRI' => $laFila[3],
                                        'CESTADO' => ($laFila[4].$laFila[5] == 'AA' ? 'A' : (strpos($laFila[4].$laFila[5], 'D') !== false ? 'D' : 'P')),
                                        'DGENERA' => $laFila[6], 'NCREDIT' => $laFila[7],
                                        'NMONTO' => $laFila[8]];
      }
      $lcSql = "SELECT TRIM(cNroPag)
                FROM B05MCPJ A
                INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}' AND B.cEstado = 'A' ORDER BY A.cIdenti DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $laData['CNROPAG'] = $laFila[0];
      $lcSql = "SELECT B.cNroCel, B.cEmail FROM A01MALU A INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni WHERE A.cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp)) {
         $this->pcError = "ERROR AL RECUPERAR INFORMACIÓN DEL ALUMNO";
         return false;
      }
      $laData['CNROCEL'] = $laTmp[0];
      $laData['CEMAIL'] = $laTmp[1];
      $this->paData = $laData;
      return true;  
   }
   /*
   public function omInitCursosPorJurado2() {
      $llOk = $this->mxValParamInitCursosPorJurado2();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitCursosPorJurado2($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitCursosPorJurado2() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DE ALUMNO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitCursosPorJurado2($p_oSql) {
      // CURSOS POR UNIDAD ACADEMICA
      $lcSql = "SELECT DISTINCT ON (A.cCodCur) A.cCodCur, A.cDescri, A.nCreTeo + A.nCreLab, C.nCosCre
                FROM A02MCUR A
                INNER JOIN A01MALU B ON B.cPlaEst = A.cPlaEst
                INNER JOIN B05DCCR C ON C.cUniAca = B.cUniAca
                WHERE cCodAlu = '{$this->paData['CCODALU']}' AND C.cTipo = 'J'
                ORDER BY A.cCodCur, C.cPeriod DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paCursos'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paCursos'][] = ['CCODCUR' => $laFila[0], 'CDESCRI' => $laFila[1], 
                                          'NCREDIT' => $laFila[2], 'NMONTO' => strpos($laFila[0], 'CC') ? 100.00 : $laFila[2] * $laFila[3]];
      }
      if (count($this->paDatos['paCursos']) == 0) {
         $this->pcError = "SIN CURSOS DISPONIBLES";
         return false;
      }
      // SOLICITUDES POR ALUMNO
      $lcSql = "SELECT B.cIdenti, B.nSerial, A.cCodAlu, B.cCodCur, C.cDescri, B.cEstEsc, B.cEstSac, TO_CHAR(A.dGenera, 'YYYY-MM-DD'), C.nCreTeo + C.nCreLab, B.nMonto
                FROM B05MCPJ A
                INNER JOIN B05DCPJ B ON B.cIdenti = A.cIdenti
                INNER JOIN A02MCUR C ON C.cCodCur = B.cCodCur
                INNER JOIN A01MALU D ON D.cCodAlu = A.cCodAlu
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado IN ('S', 'A')";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paInscr'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paInscr'][] = ['CIDENTI' => $laFila[0],'NSERIAL' => $laFila[1], 'CCODALU' => $laFila[2],
                                        'CCODCUR' => $laFila[3], 'CDESCRI' => $laFila[4],
                                        'CESTADO' => ($laFila[5].$laFila[6] == 'AA' ? 'A' : (strpos($laFila[5].$laFila[6], 'P') !== false ? 'P' : 'D')),
                                        'DGENERA' => $laFila[7], 'NCREDIT' => $laFila[8],
                                        'NMONTO' => $laFila[9]];
      }
      // COSTO POR CREDITO
      $lcSql = "SELECT nCosCre FROM B05DCCR A
                INNER JOIN A01MALU B ON B.cUniAca = A.cUniAca
                WHERE B.cCodAlu = '{$this->paData['CCODALU']}' AND cTipo = 'J'";      
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "COSTO POR CRÉDITO NO ENCONTRADO";
         return false;
      }
      $this->paData['NCOSCRE'] = $laFila[0];
      // NRO PAGO
      $lcSql = "SELECT TRIM(cNroPag)
                FROM B05MCPJ A
                INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $this->paData['CNROPAG'] = $laFila[0];
      return true;
   } */

   // ------------------------------------------------------------------------------
   // Grabar curso por jurado a alumno
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarCursoPorJurado() {
      $llOk = $this->mxValParamGrabarCursoPorJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarCursoPorJurado($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarCursoPorJurado() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DE ALUMNO NO DEFINIDO O NO VALIDO";
         return false;
      } if (!isset($this->paData['CCODCUR']) || count($this->paData['CCODCUR']) == 0) {
         $this->pcError = "SIN CURSOS SELECCIONADOS";         
         return false;
      }
      return true;
   }

   protected function mxGrabarCursoPorJurado($p_oSql) {
      $lcSql = "SELECT cConten FROM S01TVAR WHERE cVariab = 'PCPERCRE'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila)) {
         $this->pcError = "NO SE ENCONTRO PERIDO ACADEMICO INTENTELO MAS TARDE";
         return false;
      }
      $lcSql = "SELECT SUM(C.nCreTeo + C.nCreLab) FROM B05MCPJ A
                INNER JOIN B05DCPJ B ON B.cIdenti = A.cIdenti 
                INNER JOIN A02MCUR C ON C.cCodCur = B.cCodCur
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}' AND B.cEstSac IN ('A','P') AND A.cEstado IN ('C','E','S','O') AND A.cPeriod = '{$laFila[0]}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila1 = $p_oSql->fetch($R1);
      if ($laFila1[0] >= 11) {
         $lcEstado = 'O';
      }
      else {
         $lcEstado = 'S';
      }
      $lcCreTot = $laFila1[0];
      foreach ($this->paData['CCODCUR'] as $laFila) {
         //$lcCreCur = 0;
         $laData['CCODCUR'] = $laFila;
         $lcSql = "SELECT SUM(nCreTeo + nCreLab) FROM A02MCUR WHERE cCodCur = '{$laData['CCODCUR']}'";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila2 = $p_oSql->fetch($R1);
         $lcCreTot = $lcCreTot + $laFila2[0];
         if ($lcCreTot >= 11) {//creo que es 10.5 CONSULTAR
            $lcEstado = 'O';
         } else {
            $lcEstado = 'S';
         }
      }
      foreach ($this->paData['CCODCUR'] as $laFila) {
         $laData = $this->paData + ['CESTADO' => $lcEstado];
         $laData['CCODCUR'] = $laFila;
         $lcJson = json_encode($laData);
         $lcSql = "SELECT P_B05MCPJ_1('$lcJson')";
         //print_r($lcSql); 
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) {
            $this->pcError = "ERROR EN PROCESAR LA TRANSACCION";
            return false;
         }
         $laJson = json_decode($laFila[0], true);
         if (isset($laJson['ERROR'])) {
            $this->pcError = $laJson['ERROR'];
            return false;
         }
      }
      //$this->paData = $laJson;
      return true;
   }
   
   // ------------------------------------------------------------------------------
   // Bandeja para la revision de la escuela de los cursos por jurado
   // 2019-04-01 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaSolicitudesCursosJurado() {
      $llOk = $this->mxValParamInitBandejaSolicitudesCursosJurado();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudesCursosJurado($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudesCursosJurado() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudesCursosJurado($p_oSql) {
      $lcSql = "SELECT DISTINCT ON (A.cIdenti) A.cIdenti, A.cCodAlu, B.cNombre, B.cNomUni, (TO_CHAR(A.dGenera, 'YYYY-MM-DD HH24:MI')),
                       A.cEstado FROM B05MCPJ A
                INNER JOIN V_A01MALU B ON B.cCodAlu = A.cCodAlu
                INNER JOIN S01TCCO C ON C.cUniAca = B.cUniAca
                INNER JOIN B05DCPJ D ON D.cIdenti = A.cIdenti
                WHERE A.cEstado IN ('S','O') AND D.cEstesc ='P' AND C.cCenCos IN 
                      (SELECT t_cCenCos FROM F_B03PUSU_1('{$this->paData['CCODUSU']}') WHERE t_cUniAca != '00')";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR SOLICITUDES";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDENTI' => $laFila[0], 'CCODALU' => $laFila[1], 'CNOMBRE' => $laFila[2], 'CUNIACA' => $laFila[3],
                             'DGENERA' => $laFila[4], 'CESTADO' => $laFila[5]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Seguimiento de la Escuela para revisar solicitudes de Cursos por Jurado
   // 2019-04-01 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRevisarSolicitud() {
      $llOk = $this->mxValParamRevisarSolicitud();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitRevisarSolicitud($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarSolicitud() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CIDENTI']) || (strlen($this->paData['CIDENTI']) != 6)) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      }
      return true;
   }

   protected function mxInitRevisarSolicitud($p_oSql) {
      $lcSql = "SELECT B.cCodAlu, D.cNombre, C.cDescri, C.cCodCur, A.nMonto, D.cNomUni, D.cPlaEst, C.nCreTeo, C.nCreLab, A.cEstEsc,
                       A.nSerial, A.cEstSac, D.cNroDni, B.cEstado FROM B05DCPJ A 
                INNER JOIN B05MCPJ B ON B.cIdenti = A.cIdenti 
                INNER JOIN A02MCUR C ON C.cCodCur = A.cCodCur 
                INNER JOIN V_A01MALU D ON D.cCodAlu = B.cCodAlu 
                WHERE A.cIdenti = '{$this->paData['CIDENTI']}' AND A.cEstEsc = 'P' 
                ORDER BY C.cDescri";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR DETALLE DE SOLICITUD";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "SOLICITUD NO TIENE DETALLE";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1], 'CDESCRI' => $laFila[2], 'CCODCUR' => $laFila[3],
                             'NMONTO'  => $laFila[4], 'CNOMUNI' => $laFila[5], 'CPLAEST' => $laFila[6], 'NCRETEO' => $laFila[7],
                             'NCRELAB' => $laFila[8], 'CESTESC' => $laFila[9], 'NSERIAL' => $laFila[10],'CESTSAC' => $laFila[11],
                             'CNRODNI' => $laFila[12],'CESTADO' => $laFila[13]];
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Aprobacion de la escuela de cursos por jurado
   // 2019-06-24 LVA Creacion
   // --------------------------------------------------------------------------
   public function omSeguimientoCursoPorJurado() {  
      $llOk = $this->mxValParamSegCursoPorJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxSeguimientoCursoPorJurado($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamSegCursoPorJurado() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL'])) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      }
      return true;
   }
   protected function mxSeguimientoCursoPorJurado($p_oSql) {
      $lcSql = "SELECT  A.cCodCur,B.cDescri, C.cDescri, A.cEstEsc, A.cEstSac, A.mObserv FROM B05DCPJ A 
                INNER JOIN A02MCUR B ON B.cCodCur = A.cCodCur
                INNER JOIN S01TCCO C ON C.cUniAca = B.cUniAca
                WHERE A.nSerial = '{$this->paData['NSERIAL']}'";              
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL SEGUIR SOLICITUD PARA CURSOS DE JURADO';
         return false;
      }
      $this->paDatos['paCursos'] = [];
      while ($laFila = $p_oSql->fetch($llOk)){
         $this->paDatos['paCursos'] = ['CCODCUR' => $laFila[0], 'CDESCRI' => $laFila[1],
                                       'CDESCRIE' => $laFila[2], 'CESTADO' => ($laFila[3].$laFila[4] == 'AA' ? 'A' : (strpos($laFila[3].$laFila[4], 'D') !== false ? 'D' : 'P')),
                                       'MOBSERV' => $laFila[5]];
      }        
      $lcSql = "SELECT B.cJurad1, B.cJurad2, I.cNombre AS cNomJu1, K.cNombre AS cNomJu2
                FROM B05MCPJ A
                INNER JOIN B05DCPJ B ON B.cIdenti = A.cIdenti
                LEFT  JOIN A01MDOC H ON H.cCodDoc = B.cJurad1
                LEFT  JOIN S01MPER I ON I.cNroDni = H.cNroDni
                LEFT  JOIN A01MDOC J ON J.cCodDoc = B.cJurad2
                LEFT  JOIN S01MPER K ON K.cNroDni = J.cNroDni
                WHERE B.nSerial = '{$this->paData['NSERIAL']}'
                GROUP BY B.cJurad1, B.cJurad2, I.cNombre, K.cNombre";
      
      $llOk = $p_oSql->omExec($lcSql);
      $this->paDatos['paDocentes'] = [];
      while ($laFila = $p_oSql->fetch($llOk)){    
         $this->paDatos['paDocentes'][] = ['CCODJU1' => $laFila[0],'CCODJU2' => $laFila[1],
                                           'CNOMJU1' => $laFila[2],'CNOMJU2' => $laFila[3]];
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Aprobacion de la escuela de cursos por jurado
   // 2019-04-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omAprobarSolicitudCursoJurado() {  
      $llOk = $this->mxValParamAprobarSolicitudCursoJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAprobarSolicitudCursoJurado($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAprobarSolicitudCursoJurado() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL'])) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      }
      return true;
   }
   
   protected function mxAprobarSolicitudCursoJurado($p_oSql) {
      $lcSql = "SELECT cCodCur FROM B05DCPJ WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL VALIDAR REGISTRO";
         return false;
      }
      $lcCodCur = $p_oSql->fetch($R1);
      $lcCodCom = substr($lcCodCur[0], 2, 2);
      if ($lcCodCom == 'CC'){
         $this->pcError = 'ESTE ES UN CURSO COMPLEMENTARIO NECESITA PREVIA APROBACION DE VICERRECTORADO ACADEMICO USE LA OPCION ENVIAR';
         return false;
      }
      $laData = ['CINCISO' => $this->paData['CINCISO'], 'MOBSERV' => $this->paData['MOBSERV']];
      $lcParam = json_encode($laData);
      $lcSql = "UPDATE B05DCPJ SET cEstEsc = 'A', cUsuCod = '{$this->paData['CCODUSU']}', mDetall = UPPER('{$lcParam}') , tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";              
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL APROBAR SOLICITUD PARA CURSOS DE JURADO';
         return false;
      }
      //CUENTA LOS ESTADOS NO PENDIENTES
      $lcSql = "SELECT COUNT(cEstEsc) FROM B05DCPJ WHERE cEstEsc NOT IN ('P') AND cIdenti = '{$this->paData['CIDENTI']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = 'ERROR AL VALIDAR CURSOS NO PENDIENTES EN SOLICITUD';
         return false;
      }
      $tmpCountEst = $p_oSql->fetch($R1);
      //CUENTA CURSOS SOLICITADOS
      $lcSql = "SELECT COUNT(nSerial) FROM B05DCPJ WHERE cIdenti = '{$this->paData['CIDENTI']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = 'ERROR AL VALIDAR CURSOS SOLICITADOS';
         return false;
      }
      $tmpCountCan = $p_oSql->fetch($R1);
      if($tmpCountEst[0] == $tmpCountCan[0]) {
         $lcSql = "UPDATE B05MCPJ SET tRevEsc = NOW(), cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cIdenti = '{$this->paData['CIDENTI']}'";
         $llOk = $p_oSql->omExec($lcSql); 
         if (!$llOk) {
            $this->pcError = 'ERROR AL ACTUALIZAR REVISION DE ESCUELA';
            return false;
         }
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Denegar la escuela solicitud de cursos por jurado
   // 2019-04-05 LVA - LVA Creacion
   // --------------------------------------------------------------------------
   public function omDenegarSolicitudCursoJurado() {  
      $llOk = $this->mxValParamDenegarSolicitudCursoJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDenegarSolicitudCursoJurado($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDenegarSolicitudCursoJurado() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL'])) {
         $this->pcError = "IDENTIFICADOR DE CURSO NO ESPECIFICADA";
         return false;
      } elseif (!isset($this->paData['CIDENTI'])) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      } elseif (!isset($this->paData['MOBSERV'])) {
         $this->pcError = 'PARAMETRO OBSERVACIONES [MOBSERV] INVALIDO O NO DEFINIDO';
         return false;
      } elseif (strlen($this->paData['MOBSERV']) >= 1000) {
         $this->pcError = 'LAS OBSERVACIONES DEBEN TENER MÁXIMO 1000 CARACTERES';
         return false;
      } elseif (!isset($this->paData['MOBSERV']) || !preg_match("(^[a-zA-Z0-9áéíóúñÁÉÍÓÚÑ \n\r\/\.,;:_-]+$)", $this->paData['MOBSERV'])) {
         $this->pcError = 'NO USAR CARACTERES ESPECIALES';
         return false;
      }
      return true;
   }

   protected function mxDenegarSolicitudCursoJurado($p_oSql) {
      $lcSql = "UPDATE B05DCPJ SET cEstEsc = 'D', cEstSac = 'D', mObserv = '{$this->paData['MOBSERV']}', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL DENEGAR SOLICITUD PARA CURSOS DE JURADO';
         return false;
      }
      //CUENTA LOS ESTADOS DENEGADOS
      $lcSql = "SELECT COUNT(cEstEsc) FROM B05DCPJ WHERE cEstEsc = 'D' AND cIdenti = '{$this->paData['CIDENTI']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = 'ERROR AL VALIDAR CURSOS DENEGADOS';
         return false;
      }
      $tmpCountEst = $p_oSql->fetch($R1);
      //CUENTA CURSOS SOLICITADOS
      $lcSql = "SELECT COUNT(nSerial) FROM B05DCPJ WHERE cIdenti = '{$this->paData['CIDENTI']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL VALIDAR CURSOS SOLICITADOS';
         return false;
      }
      $tmpCountCan = $p_oSql->fetch($llOk);
      if($tmpCountEst[0] == $tmpCountCan[0]){
         $lcSql = "UPDATE B05MCPJ SET cEstado = 'C', tRevEsc = NOW(), cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cIdenti = '{$this->paData['CIDENTI']}'";
         $llOk = $p_oSql->omExec($lcSql); 
         if (!$llOk) {
            $this->pcError = 'ERROR AL ACTUALIZAR REVISION DE ESCUELA';
            return false;
         }
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Denegar la escuela solicitud de cursos por jurado
   // 2019-04-05 LVA - LVA Creacion
   // --------------------------------------------------------------------------
   public function omEnviarCursosPorJurado() {  
      $llOk = $this->mxValParamEnviarCursosPorJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEnviarCursosPorJurado($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEnviarCursosPorJurado() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL'])) {
         $this->pcError = "IDENTIFICADOR DE CURSO NO ESPECIFICADA";
         return false;
      } elseif (!isset($this->paData['CIDENTI'])) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      } /*elseif (substr($this->paData['CCODCUR'],2,2) != 'CC') { 
         $this->pcError = "PARECE QUE NO ES UN CURSO POR JURADO";
         return false;
      }*/
      return true;
   }

   protected function mxEnviarCursosPorJurado($p_oSql) {
      $lcSql = "UPDATE B05DCPJ SET cEstEsc = 'V', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ENVIAR SOLICITUD A VIC. ACADEMICO';
         return false;
      }
      //CUENTA ESTADOS NO PENDIENTES
      $lcSql = "SELECT COUNT(cEstEsc) FROM B05DCPJ WHERE cEstEsc NOT IN ('P') AND CIDENTI = '{$this->paData['CIDENTI']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = 'ERROR AL VALIDAR CURSOS NO PENDIENTES EN SOLICITUD';
         return false;
      }
      $tmpCountEst = $p_oSql->fetch($R1);
      //CUENTA CURSOS SOLICITADOS
      $lcSql = "SELECT COUNT(NSERIAL) FROM B05DCPJ WHERE CIDENTI = '{$this->paData['CIDENTI']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = 'ERROR AL VALIDAR CURSOS SOLICITADOS';
         return false;
      }
      $tmpCountCan = $p_oSql->fetch($R1);
      if($tmpCountEst[0] == $tmpCountCan[0]){
         $lcSql = "UPDATE B05MCPJ SET tRevEsc = NOW(), cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cIdenti = '{$this->paData['CIDENTI']}'";
         $llOk = $p_oSql->omExec($lcSql); 
         if (!$llOk) {
            $this->pcError = 'ERROR AL ACTUALIZAR REVISION DE ESCUELA';
            return false;
         }
      }
      return true;
   }

   // ------------------------------------------------------------------------------------
   // Bandeja para Servicios Academicos de los cursos por jurados aprobados por la escuela
   // 2019-04-01 LVA Creacion
   // ------------------------------------------------------------------------------------
   public function omInitBandejaSolicitudesCursosJuradoAc() {
      $llOk = $this->mxValParamInitBandejaSolicitudesCursosJuradoAc();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudesCursosJuradoAc($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudesCursosJuradoAc() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudesCursosJuradoAc($p_oSql) {
      $lcSql = "SELECT A.cCodAlu, D.cNombre, C.cNomUni, (TO_CHAR(A.dGenera, 'YYYY-MM-DD HH24:MI')), A.cEstado, A.cIdenti 
                FROM B05MCPJ A 
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu 
                INNER JOIN S01TUAC C ON C.cUniAca = B.cUniAca
                INNER JOIN S01MPER D ON D.cNroDni = B.cNroDni
                INNER JOIN B05DCPJ E ON E.cIdenti = A.cIdenti              
                WHERE A.cEstado = 'S' 
                GROUP BY A.cCodAlu, D.cNombre, C.cNomUni, A.dGenera, A.cEstado, A.cIdenti 
                HAVING count(CASE WHEN E.cEstEsc IN ('A','D') THEN 1 END) = count(CASE WHEN E.cEstSac IN ('D','P','A') THEN 1 END) 
                ORDER BY A.dGenera DESC";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CUNIACA' => $laFila[2], 'DGENERA' => $laFila[3],
                             'CESTADO' => $laFila[4], 'CIDENTI' => $laFila[5]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------------
   // Seguimiento de la Servicios Academicos para revisar solicitudes de Cursos por Jurado CC
   // 2019-04-05 LVA Creacion
   // ------------------------------------------------------------------------------------
   public function omCargarOpcion() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
          $this->pcError = $loSql->pcError;
          return false;
      }
      $llOk = $this->mxCagarOpcion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxCagarOpcion($p_oSql) {
      $lcOpcion = $this->paData['COPCION'];
      if ($lcOpcion == 'V') {
         $lcCurCom1 = ",'V'";
         $lcCurCom2 = " AND E.cEstEsc = 'V'";
         $lcCurCom3 = 'S';
      } elseif ($lcOpcion == 'O') {
         $lcCurCom1 = ",'V'";
         $lcCurCom2 = " AND E.cEstEsc IN ('V','A')";
         $lcCurCom3 = 'O';
      } elseif ($lcOpcion == 'R') {
         $lcCurCom1 = ",'V'";
         $lcCurCom2 = " AND E.cEstEsc IN ('V','A')";
         $lcCurCom3 = 'R';
      } else {
         $lcCurCom1 = "";
         $lcCurCom2 = "";
         $lcCurCom3 = 'S';
      }
      $lcSql = "SELECT A.cCodAlu, D.cNombre, C.cNomUni, (TO_CHAR(A.dGenera, 'YYYY-MM-DD HH24:MI')), A.cEstado, A.cIdenti 
                FROM B05MCPJ A 
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu 
                INNER JOIN S01TUAC C ON C.cUniAca = B.cUniAca
                INNER JOIN S01MPER D ON D.cNroDni = B.cNroDni
                INNER JOIN B05DCPJ E ON E.cIdenti = A.cIdenti              
                WHERE A.cEstado = '{$lcCurCom3}'".$lcCurCom2."
                GROUP BY A.cCodAlu, D.cNombre, C.cNomUni, A.dGenera, A.cEstado, A.cIdenti 
                HAVING count(CASE WHEN E.cEstEsc IN ('A','D'".$lcCurCom1.") THEN 1 END) = count(CASE WHEN E.cEstSac IN ('D','P','A') THEN 1 END) 
                ORDER BY A.dGenera DESC";
      $R1 = $p_oSql->omExec($lcSql); 
      while ($laFila = $p_oSql->fetch($R1)) {              
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CUNIACA' => $laFila[2], 'DGENERA' => $laFila[3],
                             'CESTADO' => $laFila[4], 'CIDENTI' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------------
   // Seguimiento de la Servicios Academicos para revisar solicitudes de Cursos por Jurado
   // 2019-04-05 LVA Creacion
   // ------------------------------------------------------------------------------------
   public function omRevisarSolicitudAc() {
      $llOk = $this->mxValParamRevisarSolicitudAc();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRevisarSolicitudAc($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarSolicitudAc() {
      if (!isset($this->paData['CIDENTI']) || (strlen($this->paData['CIDENTI']) != 6)) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRevisarSolicitudAc($p_oSql) {
      $lcSql = "SELECT A.cCodAlu 
                FROM B05MCPJ A 
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu 
                INNER JOIN S01TUAC C ON C.cUniAca = B.cUniAca
                INNER JOIN S01MPER D ON D.cNroDni = B.cNroDni
                INNER JOIN B05DCPJ E ON E.cIdenti = A.cIdenti              
                WHERE E.cIdenti = '{$this->paData['CIDENTI']}'
                GROUP BY A.cCodAlu, D.cNombre, C.cNomUni, A.dGenera, A.cEstado, A.cIdenti 
                HAVING count(CASE WHEN E.cEstEsc IN ('A','D','V') THEN 1 END) = count(CASE WHEN E.cEstSac IN ('D','P','A') THEN 1 END) 
                ORDER BY A.dGenera DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "AUN NO SE REVISA TODA LA SOLICITUD POR LA ESCUELA";
         return false;
      }
      $lcSql = "SELECT B.cCodAlu, D.cNombre, C.cDescri, C.cCodCur, A.nMonto, D.cNomUni, D.cPlaEst, C.ncreteo, C.ncrelab, A.cEstEsc, A.nSerial, A.cEstSac, A.mObserv, D.cNroDni, A.mDetall, B.cEstado FROM B05DCPJ A
                INNER JOIN B05MCPJ B ON B.cIdenti = A.cIdenti
                INNER JOIN A02MCUR C ON C.cCodCur = A.cCodCur
                INNER JOIN V_A01MALU D ON D.cCodAlu = B.cCodAlu
                WHERE A.cIdenti = '{$this->paData['CIDENTI']}' AND A.cEstEsc IN('A','V') AND A.cEstSac IN('P','A','D')  
                ORDER BY A.nSerial ASC";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1], 'CDESCRI' => $laFila[2], 'CCODCUR' => $laFila[3],
                             'NMONTO'  => $laFila[4], 'CNOMUNI' => $laFila[5], 'CPLAEST' => $laFila[6], 'NCRETEO' => $laFila[7],
                             'NCRELAB' => $laFila[8], 'CESTESC' => $laFila[9], 'NSERIAL' => $laFila[10],'CESTSAC' => $laFila[11],
                             'MOBSERB' => $laFila[12],'MOBSERV' => strtoupper('CURSO            :'.' '.$laFila[2]."\n".'OBSERVACIÓN : '.$laFila[12]).'         ' , 'CNRODNI' => $laFila[13], 'MDETALL' => json_decode($laFila[14],true),
                             'CESTADO' => $laFila[15]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Aprobación de curso por jurado - Servicios academico - Envio de Email de Deuda
   // 2019-04-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAprobarSolicitudCursoJuradoAc() {  
      $llOk = $this->mxValParamAprobarSolicitudCursoJuradoAc();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAprobarSolicitudCursoJuradoAc($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAprobarSolicitudCursoJuradoAc() {      
      $this->paData['NSERIAL'] = (int)$this->paData['NSERIAL'];
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL']) || $this->paData['NSERIAL'] <= 0) {
         $this->pcError = 'PARAMETRO SERIAL INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODALU']) || (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'PARAMETRO CODIGO DE ALUMNO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }
   protected function mxAprobarSolicitudCursoJuradoAc($p_oSql) {
       $lcSql = "SELECT A.cIdenti, COUNT(B.cIdenti) FROM B05DCPJ A 
                 INNER JOIN B05DCPJ B ON B.cIdenti = A.cIdenti 
                 WHERE A.nSerial = '{$this->paData['NSERIAL']}' GROUP BY A.cIdenti";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);  
      if (!isset($laTmp[0])) {
         $this->pcError = "REGISTROS DE CURSOS NO ENCONTRADO";
         return false;
      }  
      $this->laData = ['CIDENTI' => $laTmp[0], 'NCANSOL' => $laTmp[1]]; //NCANSOL - CANTIDAD DE SOLICITUDES
      $lcSql = "UPDATE B05DCPJ SET cEstSac = 'A', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";              
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL DENEGAR SOLICITUD PARA CURSOS DE JURADO';
         return false;
      } 
      $lcSql = "SELECT COUNT(CIDENTI) FROM B05DCPJ WHERE CIDENTI = '{$this->laData['CIDENTI']}' AND cEstSac IN ('A','D')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "ERROR REVISION DE CURSOS PENDIENTES";
         return false;
      } 
      if ($laTmp[0] >= 1 AND $laTmp[0] == $this->laData['NCANSOL']) { 
         $lcSql = "UPDATE B05MCPJ SET cEstado = 'A', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}' WHERE cIdenti = '{$this->laData['CIDENTI']}'";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL ACTUALIZAR ESTADO ACTIVO A LA SOLICITUD";
            return false;
         }
         $lcJson = json_encode(['CCODALU' => $this->paData['CCODALU'], 'CIDENTI' => $this->laData['CIDENTI']]);
         $lcSql = "SELECT P_B03MDEU_4('$lcJson')";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) {
            $this->pcError = "ERROR AL GENERAR DEUDA";
            return false;
         }
         $laJson = json_decode($laFila[0], true);
         if (isset($laJson['ERROR'])) {
            $this->pcError = $laJson["ERROR"];
            return false;
         }
         $lcSql = "SELECT SUM(nMonto) FROM B05DCPJ WHERE cIdenti = '{$this->laData['CIDENTI']}' AND (cEstEsc = 'A' OR cEstEsc = 'V') AND cEstSac = 'A';";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);    
         if (!isset($laTmp[0])) {
            $this->pcError = "MONTO DE CURSO NO ESPECIFICADO";
            return false;
         }
         $this->laData = $this->laData + ['NMONTO' => $laTmp[0]];
         $lcSql = "UPDATE B05MCPJ SET cIdDeud = '{$laJson['CIDDEUD']}', nMonto = '{$this->laData['NMONTO']}', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}' WHERE cIdenti = '{$this->laData['CIDENTI']}'";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL MODIFICAR MONTO Y DEUDA";
            return false;
         }
         // CORREO DE CONFIRMACION A ALUMNO
         $lcSql = "SELECT cEmail FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         if (!isset($laTmp[0]) || empty($laTmp[0])) {
            $this->pcError = "CORREO DE ALUMNO NO ENCONTRADO";
            return false;
         }
         $lo = new CEmail();
         $llOk = $lo->omConnect();
         $lcMensa = "Ya puedes pagar tu deuda para llevar cursos por jurado. Su ID de pago es: ".$laJson['CNROPAG'].". La cantidad a pagar es de S/.".$laJson['NMONTO']." . Indicar que es pago por pensiones. El ID de pago estará disponible solo por 7 días.";
         $lo->paData = ['CSUBJEC' => "AVISO: DEUDA PARA CURSOS POR JURADO", "CBODY" => $lcMensa, "AEMAILS" => [$laTmp[0]]];
         $llOk = $lo->omSend();
         if (!$llOk) {
            $this->pcError = $lo->pcError;
            return false;
         }
         return true;
      }         
      return true;
   }

   // ----------------------------------------------------------------------------
   // Recuperar docentes por criterio de busqueda
   // 2019-04-22 LVA Creacion
   // ----------------------------------------------------------------------------
   public function omBuscarDocente() {
      $llOk = $this->mxValParamBuscarDocente();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBuscarDocente($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBuscarDocente() {
      if (!isset($this->paData['CBUSQUE']) || empty($this->paData['CBUSQUE'])) {
         $this->pcError = "EXPRESION DE BUSQUEDA VACIA";
         return false;
      }
      return true;
   }

   protected function mxBuscarDocente($p_oSql) {
      $lcBusque = strtoupper(str_replace(' ', '%', $this->paData['CBUSQUE']));
      $lcSql = "SELECT A.cCodDoc, A.cNroDni, B.cNombre 
                FROM A01MDOC A 
                INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni 
                WHERE A.cEstado = 'A' AND (A.cCodDoc = '$lcBusque' OR A.cNroDni = '$lcBusque' OR B.cNombre LIKE '%$lcBusque%')";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODDOC' => $laFila[0], 'CNRODNI' => $laFila[1],
                             'CNOMBRE' => str_replace('/', ' ', $laFila[2])];
      }
      if (count($this->paDatos) == 0) {
         $this->paDatos[] = ['CCODDOC' => '0000', 'CNRODNI' => '00000000', 'CNOMBRE' => 'SIN REGISTROS'];
      }
      return true;
   }

   // ----------------------------------------------------------------------------
   // Recuperar alumnos pendientes de asignacion de docentes para curso por jurado
   // 2019-04-05 LVA Creacion
   // ----------------------------------------------------------------------------
   public function omAlumnosParaAsignarDocentes() {
      $llOk = $this->mxValParamAlumnosParaAsignarDocentes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAlumnosParaAsignarDocentes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAlumnosParaAsignarDocentes() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = "CODIGO DE USUARIO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CESTASI']) || strlen(trim($this->paData['CESTASI'])) != 1) {
         $this->pcError = "ESTADO DE ASIGNACION INVALIDO";
         return false;
      }
      return true;
   }
 
   protected function mxAlumnosParaAsignarDocentes($p_oSql) {
      $lcTmp = ($this->paData['CESTASI'] == 'N')? "!=" : "=";
      $lcSql = "SELECT A.cIdenti, A.cCodAlu, E.cNroDni, E.cNombre, E.cUniAca, E.cNomUni, TO_CHAR(A.dGenera, 'YYYY-MM-DD HH24:MM'),
                       C.cRecibo, D.cCodTre, B.cNroPag, E.cNivel, TO_CHAR(A.tModifi, 'YYYY-MM-DD HH24:MM'), E.cEmail, E.cNroCel,
                       TO_CHAR(B.dRecepc, 'YYYY-MM-DD HH24:MM')
                FROM B05MCPJ A
                INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B03DDEU C ON C.cIdDeud = B.cIdDeud
                INNER JOIN B04MTRE D ON D.cIdLog = C.cIdLog
                INNER JOIN V_A01MALU E ON E.cCodAlu = A.cCodAlu
                INNER JOIN S01TCCO F ON F.cUniAca = E.cUniAca
                INNER JOIN B05DCPJ G ON G.cIdenti = A.cIdenti
                WHERE B.cEstado = 'C' AND F.cCenCos IN 
                (SELECT t_cCenCos FROM F_B03PUSU_1('{$this->paData['CUSUCOD']}') WHERE t_cUniAca != '00')
                GROUP BY A.cIdenti, B.cIdDeud, C.cIdLog, D.cCodTre, E.cNroDni, E.cNombre, E.cUniAca, E.cNomUni, E.cNivel, E.cEmail,
                         E.cNroCel
                HAVING CASE WHEN E.cNivel IN ('03','04') THEN
                   COUNT (CASE WHEN G.cJurad1 != '0000' THEN 1 END) $lcTmp COUNT(CASE WHEN G.cEstSAc = 'A' THEN 1 END)
                ELSE
                   COUNT (CASE WHEN G.cJurad1 != '0000' AND G.cJurad2 != '0000' THEN 1 END) $lcTmp COUNT(CASE WHEN G.cEstSAc = 'A' THEN 1 END)
                END
                ORDER BY A.tModifi DESC";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR SOLICITUDES";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paSolJur[] = ['CIDENTI' => $laFila[0], 'CCODALU' => $laFila[1], 'CNRODNI' => $laFila[2], 'CNOMALU' => $laFila[3],
                              'CUNIACA' => $laFila[4], 'CNOMUNI' => $laFila[5], 'DGENERA' => $laFila[6], 'CRECIBO' => $laFila[7],
                              'CCODTRE' => $laFila[8], 'CNROPAG' => $laFila[9], 'CNIVUNI' => $laFila[10],'TFECHA'  => $laFila[11],
                              'CEMAIL'  => $laFila[12],'CNROCEL' => $laFila[13],'TPAGO'   => $laFila[14]];
      }
      return true;
   }

   // ----------------------------------------------------------------------------
   // Asignar jurado - Cursos por jurado
   // 2019-04-14 LVA Creacion
   // ----------------------------------------------------------------------------
   public function omAsignarJurado() {
      $llOk = $this->mxValParamAsignarJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAsignarJurado($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAsignarJurado() {
      if (!isset($this->paData['CCODDOC']) || !preg_match('(^[0-9]{4}$)', $this->paData['CCODDOC'])) {
         $this->pcError = "CODIGO DOCENTE NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CCODCUR']) || !preg_match('(^[0-9A-Z]{7}$)', $this->paData['CCODCUR'])) {
         $this->pcError = "CODIGO CURSO NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['NJURADO']) || !in_array($this->paData['NJURADO'], ['1', '2'])) {
         $this->pcError = "ID. JURADO NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CIDENTI']) || !preg_match('(^[0-9]{6}$)', $this->paData['CIDENTI'])) {
         $this->pcError = "IDENTIFICADOR CURSO POR JURADO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxAsignarJurado($p_oSql) {
      $lcSql = "UPDATE B05DCPJ SET cJurad{$this->paData['NJURADO']} = '{$this->paData['CCODDOC']}', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW()
                WHERE cCodCur = '{$this->paData['CCODCUR']}' AND cIdenti = '{$this->paData['CIDENTI']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR ASIGNANDO JURADO";
         return false;
      }
      return true;
   }

   // ----------------------------------------------------------------------------
   // Recuperar alumnos pendientes de asignacion de docentes para curso por jurado
   // 2019-04-14 LVA Creacion
   // ----------------------------------------------------------------------------
   public function omCursosParaAsignarDocentes() {
      $llOk = $this->mxValParamCursosParaAsignarDocentes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCursosParaAsignarDocentes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCursosParaAsignarDocentes() {
      if (!isset($this->paData['CIDENTI']) || !preg_match('(^[0-9]{6}$)', $this->paData['CIDENTI'])) {
         $this->pcError = "CODIGO DE ALUMNO NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxCursosParaAsignarDocentes($p_oSql) {
      $lcSql = "SELECT B.cCodCur, C.cDescri, C.nCreTeo, C.nCreLab, (C.nCreTeo + C.nCreLab) AS nCreTot, B.cJurad1, B.cJurad2,
                       D.cNombre AS cNomJu1, E.cNombre AS cNomJu2, B.nSerial, TO_CHAR(B.tModifi, 'YYYY-MM-DD HH24:MI')
               FROM B05MCPJ A
               INNER JOIN B05DCPJ B ON B.cIdenti = A.cIdenti
               INNER JOIN A02MCUR C ON C.cCodCur = B.cCodCur
               INNER JOIN V_A01MDOC D ON D.cCodDoc = B.cJurad1
               INNER JOIN V_A01MDOC E ON E.cCodDoc = B.cJurad2
               WHERE A.cIdenti = '{$this->paData['CIDENTI']}' AND B.cEstSac = 'A'
               ORDER BY B.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR DETALLE DE SOLICITUD";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "SOLICITUD SIN DETALLE DEFINIDO";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODCUR' => $laFila[0],  'CDESCRI' => $laFila[1], 'NCRETEO' => $laFila[2], 'NCRELAB' => $laFila[3],
                             'NCRETOT' => $laFila[4],  'CJURAD1' => $laFila[5], 'CJURAD2' => $laFila[6], 'CNOMJU1' => $laFila[7], 
                             'CNOMJU2' => $laFila[8],  'NSERIAL' => $laFila[9], 'TFECHA'  => $laFila[10]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Denegacion de curso por jurado - Servicios academico - Envio de Email de Deuda
   // 2019-04-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omDenegarSolicitudCursoJuradoAc() {  
      $llOk = $this->mxValParamDenegarSolicitudCursoJuradoAc();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDenegarSolicitudCursoJuradoAc($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDenegarSolicitudCursoJuradoAc() {
      $this->paData['NSERIAL'] = (int)$this->paData['NSERIAL'];
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL']) || $this->paData['NSERIAL'] <= 0) {
         $this->pcError = 'PARAMETRO SERIAL INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODALU']) || (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'PARAMETRO CODIGO DE ALUMNO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['MOBSERV']) || $this->paData['MOBSERV'] == NULL ) {
         $this->pcError = 'PARAMETRO OBSERVACIONES NO DEFINIDO';
         return false;
      } elseif (strlen($this->paData['MOBSERV']) >= 1000) {
         $this->pcError = 'LAS OBSERVACIONES DEBEN TENER MÁXIMO 1000 CARACTERES';
         return false;
      } elseif (!preg_match("(^[a-zA-Z0-9áéíóúñÁÉÍÓÚÑ \n\r\/\.,;:_-]+$)", $this->paData['MOBSERV'])) {
         $this->pcError = 'NO USAR CARACTERES ESPECIALES';
         return false;
      } 
      return true;
   }
   protected function mxDenegarSolicitudCursoJuradoAc($p_oSql) {
      $lcSql = "SELECT A.cIdenti, COUNT(B.cIdenti) FROM B05DCPJ A 
                INNER JOIN B05DCPJ B ON B.cIdenti = A.cIdenti 
                WHERE A.nSerial = '{$this->paData['NSERIAL']}' GROUP BY A.cIdenti";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);  
      if (!isset($laTmp[0])) {
         $this->pcError = "REGISTROS DE CURSOS NO ENCONTRADO";
         return false;
      }  
      $this->laData = ['CIDENTI' => $laTmp[0], 'NCANSOL' => $laTmp[1]]; //NCANSOL - CANTIDAD DE SOLICITUDES
      $lcSql = "UPDATE B05DCPJ SET cEstSac = 'D', cUsuCod = '{$this->paData['CCODUSU']}', mObserv = '{$this->paData['MOBSERV']}', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";              
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL DENEGAR SOLICITUD PARA CURSOS DE JURADO';
         return false;
      }
      $lcSql = "SELECT COUNT(CIDENTI) FROM B05DCPJ WHERE CIDENTI = '{$this->laData['CIDENTI']}' AND cEstSac IN ('D')"; //OBTENER CURSOS DENEGADOS
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);    
      if (!isset($laTmp[0])) {
         $this->pcError = "CURSOS EN ESTADO DENEGADO NO ENCONTRADOS";
         return false;
      }
      if ($laTmp[0] == $this->laData['NCANSOL']) {
         $lcSql = "UPDATE B05MCPJ SET cEstado = 'C', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}' WHERE cIdenti = '{$this->laData['CIDENTI']}'"; // SI ES QUE TODOS LOS CURSOS FUERON DENEGADOS SE CIERRA O TERMINA 'C' LA SOLICITUD
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL INSERTAR MODIFICACIÓN DE CREDITAJE";
            return false;
         }
         return true;
      }
      $lcSql = "SELECT COUNT(CIDENTI) FROM B05DCPJ WHERE CIDENTI = '{$this->laData['CIDENTI']}' AND cEstSac IN ('A','D')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);    
      if (!isset($laTmp[0])) {
         $this->pcError = "ERROR REVISION DE CURSOS PENDIENTES";
         return false;
      }
      if ($laTmp[0] >= 1 AND $laTmp[0] == $this->laData['NCANSOL']) { 
         $lcSql = "UPDATE B05MCPJ SET cEstado = 'A', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}' WHERE cIdenti = '{$this->laData['CIDENTI']}'";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL ACTUALIZAR ESTADO ACTIVO A LA SOLICITUD";
            return false;
         }
         $lcJson = json_encode(['CCODALU' => $this->paData['CCODALU'], 'CIDENTI' => $this->laData['CIDENTI']]);
         $lcSql = "SELECT P_B03MDEU_4('$lcJson')";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) {
            $this->pcError = "ERROR AL GENERAR DEUDA";
            return false;
         }
         $laJson = json_decode($laFila[0], true);
         if (isset($laJson['ERROR'])) {
            $this->pcError = $laJson["ERROR"];
            return false;
         }
         $lcSql = "SELECT SUM(nMonto) FROM B05DCPJ WHERE cIdenti = '{$this->laData['CIDENTI']}' AND (cEstEsc = 'A' OR cEstEsc = 'V') AND cEstSac = 'A';";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);    
         if (!isset($laTmp[0])) {
            $this->pcError = "MONTO DE CURSO NO ESPECIFICADO";
            return false;
         }
         $this->laData = $this->laData + ['NMONTO' => $laTmp[0]];
         $lcSql = "UPDATE B05MCPJ SET cIdDeud = '{$laJson['CIDDEUD']}', nMonto = '{$this->laData['NMONTO']}', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}' WHERE cIdenti = '{$this->laData['CIDENTI']}'";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL MODIFICAR MONTO Y DEUDA";
            return false;
         }
         // CORREO DE CONFIRMACION A ALUMNO
         $lcSql = "SELECT cEmail FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         if (!isset($laTmp[0]) || empty($laTmp[0])) {
            $this->pcError = "CORREO DE ALUMNO NO ENCONTRADO";
            return false;
         }
         $lo = new CEmail();
         $llOk = $lo->omConnect();
         $lcMensa = "Ya puedes pagar tu deuda para llevar cursos por jurado. Su ID de pago es: ".$laJson['CNROPAG'].". La cantidad a pagar es de S/.".$laJson['NMONTO']." . Indicar que es pago por pensiones. El ID de pago estará disponible solo por 7 días.";
         $lo->paData = ['CSUBJEC' => "AVISO: DEUDA PARA CURSOS POR JURADO", "CBODY" => $lcMensa, "AEMAILS" => [$laTmp[0]]];
         $llOk = $lo->omSend();
         if (!$llOk) {
            $this->pcError = $lo->pcError;
            return false;
         }         
         return true;
      } 
      return true;
   }

   // --------------------------------------------------------------------------
   // Denegar la escuela solicitud de cursos por jurado
   // 2019-04-05 LVA - LVA Creacion
   // --------------------------------------------------------------------------
   public function omDetenerSolicitudCursoJuradoAc() {  
      $llOk = $this->mxValParamDetenerSolicitudCursoJuradoAc();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDetenerSolicitudCursoJuradoAc($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDetenerSolicitudCursoJuradoAc() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CIDENTI'])) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      } 
      return true;
   }
   protected function mxDetenerSolicitudCursoJuradoAc($p_oSql) {
      $lcSql = "UPDATE B05MCPJ SET cEstado = 'R', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cIdenti = '{$this->paData['CIDENTI']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL DETENER SOLICITUD PARA CURSOS DE JURADO';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // ws para informatica - firma de documentos
   // 2019-03-15 LVA - LVA Creacion
   // ------------------------------------------------------------------------------
   public function omCursosPorJurado() {
      $llOk = $this->mxValParamCursosPorJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCursosPorJurado($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCursosPorJurado() {
     if (!isset($this->paData['CCODALU'])|| !preg_match("(^[0-9]+$)", $this->paData['CCODALU'])|| strlen($this->paData['CCODALU']) != 10) {
         $this->pcError = "CODIGO DE ALUMNO INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxCursosPorJurado($p_oSql) {
      $lcParam = json_encode($this->paData);
      $lcSql = "SELECT P_B05MCPJ_2('$lcParam')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!$laTmp[0]) {
         $this->pcError = "ERROR EN LA PETICION";
         return false;
      }
      $laData = json_decode($laTmp[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      $this->paData = $laData;
      return true;
   }

   // --------------------------------------------------------------------------------
   // Init bandeja de tramites para Cursos por Jurado
   // 2019-07-01 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omIniBandejaSolicitudesCPJ() {
      $llOk = $this->mxValParamInitBandejaCPJ();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudesCPJ($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaCPJ() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudesCPJ($p_oSql) {
      $lcSql = "SELECT F.cCodTre, A.cCodAlu, B.cNombre, C.cNomUni, A.cEstado, A.cIdenti
                FROM B05MCPJ A
                INNER JOIN V_A01MALU B ON B.cCodAlu = A.cCodAlu
                INNER JOIN S01TUAC C ON C.cUniAca = B.cUniAca
                INNER JOIN B03MDEU D ON D.cIdDeud = A.cIdDeud
                INNER JOIN B03DDEU E ON E.cIdDeud = D.cIdDeud
                INNER JOIN B04MTRE F ON F.cIdLog = E.cIdLog
                WHERE A.cEstado IN ('A', 'E') ORDER BY A.tModifi DESC";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODTRE' => $laFila[0],'CCODALU' => $laFila[1], 'CNOMBRE' => $laFila[2],
                             'CNOMUNI' => $laFila[3], 'CESTADO' => $laFila[4], 'CIDENTI' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return true;
      }
      return true;
   }

   // --------------------------------------------------------------------------------
   // Seguimiento de tramites para Cursos por Jurado
   // 2019-07-01 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omSeguimientoSolicitudCPJ() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxSeguimientoSolicitudCPJ($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamISeguimientoCPJ() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxSeguimientoSolicitudCPJ($p_oSql) {
      $lcSql = "SELECT A.cCodCur, B.cDescri, SUM(B.nCreTeo + B.nCreLab), A.cEstEsc,  A.cIdenti
                FROM B05DCPJ A
                INNER JOIN A02MCUR B ON B.cCodCur = A.cCodCur
                WHERE A.cIdenti = '{$this->paData['CIDENTI']}' AND (A.cEstEsc = 'A' OR A.cEstEsc = 'V') AND A.cEstSac = 'A'
                GROUP BY A.cCodCur, B.cDescri, A.cEstEsc, A.cIdenti ORDER BY A.cCodCur "; 
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODCUR' => $laFila[0], 'CDESCRI' => $laFila[1],
                             'NSUMA' => $laFila[2], 'CESTESC' => $laFila[3], 'CIDENTI' => $laFila[4]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return true;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualiza datos del alumno - celular, email
   // 2019-06-23 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omActualizarDatosAlumno() {
      $llOk = $this->mxValParamActualizarDatosAlumno();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxActualizarDatosAlumno($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamActualizarDatosAlumno() {
      if (!isset($this->paData['CNROCEL'])) {
         $this->pcError = "NÚMERO DE CELULAR NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CEMAIL']) || !filter_var($this->paData['CEMAIL'], FILTER_VALIDATE_EMAIL)) {
         $this->pcError = "EMAIL NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CNRODNI']) || !preg_match('(^[0-9]{8}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxActualizarDatosAlumno($p_oSql) {
      $lcSql = "UPDATE S01MPER SET cNroCel = '{$this->paData['CNROCEL']}', cEmail = '{$this->paData['CEMAIL']}', tModifi = NOW() WHERE cNroDni = '{$this->paData['CNRODNI']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "NO SE PUDO ACTUALIZAR DATOS DEL ALUMNO";
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // DECRETO CPJ
   // 2019-26-05 LVA Creacion
   // --------------------------------------------------------------------------<
   public function omGenerarDecretoCpj() {
      $llOk = $this->mxValParamGenerarDecretoCpj();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk  = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGenerarDecretoCpj($loSql);
      $loSql->omDisconnect();
      if (!$llOk) {
         return false;
      }
      //$this->paData = $this->paData;  
      $llOk = $this->mxPrintGenerarDecretoCpj($loSql);
      return $llOk;
   }

   protected function mxValParamGenerarDecretoCpj() {
      if (!isset($this->paData['CIDENTI'])) {
         $this->pcError = 'PARAMETRO DE  SOLICITUD VACIO';
         return false;
      } 
      return true;
   }

   protected function mxGenerarDecretoCpj($p_oSql) {
      $lcSql  = "SELECT E.cCodtre, A.cCodAlu, G.cNombre, G.cNomUni, B.cCodCur, F.cDescri, (F.nCreTeo + F.nCreLab ) AS nCreTot,
                        H.cEstPre, A.cIdDeud, C.cNroPag, TO_CHAR(E.tFecha, 'YYYY-MM-DD'), G.cNivel
                 FROM B05MCPJ A
                 INNER JOIN B05DCPJ B ON B.cIdenti = A.cIdenti
                 INNER JOIN B03MDEU C ON C.cIdDeud = A.cIdDeud
                 INNER JOIN B03DDEU D ON D.cIdDeud = C.cIdDeud
                 INNER JOIN B04MTRE E ON E.cIdLog = D.cIdLog
                 INNER JOIN A02MCUR F ON F.cCodCur = B.cCodCur
                 INNER JOIN V_A01MALU G ON G.cCodAlu = A.cCodAlu
                 INNER JOIN S01TCCO H ON H.cUniAca = G.cUniAca
                 WHERE A.cIdenti = '{$this->paData['CIDENTI']}' AND A.cEstado IN ('A','E') AND B.cEstSac = 'A'
                 ORDER BY B.cCodCur";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR DE EJECUCION SQL";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "NO HAY REGISTROS PARA IMPRESIÓN";
         return false;
      }
      $this->paDatos['paCurAlu'] = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos['paCurAlu'][] = ['CCODTRE' => $laTmp[0], 'CCODALU' => $laTmp[1], 'CNOMBRE' => $laTmp[2], 
                                         'CNOMUNI' => $laTmp[3], 'CCODCUR' => $laTmp[4], 'CDESCRI' => $laTmp[5], 
                                         'NCRETOT' => $laTmp[6], 'CESTPRE' => $laTmp[7], 'CIDDEUD' => $laTmp[8],
                                         'CNROPAG' => $laTmp[9], 'TFECHA'  => $laTmp[10],'CNIVUAC' => $laTmp[11]];
      }
      if (in_array($this->paDatos['paCurAlu'][0]['CNIVUAC'], ['01','02'])) {
         $lcSql= "SELECT cDesCri FROM S01TCCO WHERE cEstPre = '{$this->paDatos['paCurAlu'][0]['CESTPRE']}' AND cDescri LIKE '%FACU%'";
         $R1 = $p_oSql->omExec($lcSql);
         if ($R1 == false || $p_oSql->pnNumRow == 0) {
            $this->pcError = "ESCUELA NO TIENE DEFINIDA FACULTAD";
            return false;
         }
         $laTmp = $p_oSql->fetch($R1);
         $this->paData = $this->paData + ['CDESCRI' => $laTmp[0]];
      }
      return true;
   }   

   protected function mxPrintGenerarDecretoCpj() {
      $lcAnio = date("Y");
      $loPdf = new FPDF('portrait','cm','A4');
      $loPdf->SetMargins(1.5, 2.5, 1.5);
      $loDate = new CDate();
      $loPdf->AddPage();
      $lnWidth = 0;
      $lnHeight = 0.5;
      $loPdf->SetFont('Courier','B',12);
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode('DIRECCIÓN DE REGISTRO Y ARCHIVO ACADÉMICO'), 0, 2, 'R');
      $loPdf->Ln(1);
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode("EXPEDIENTE: E-{$this->paDatos['paCurAlu'][0]['CCODTRE']}"), 0, 2, 'L');
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode("ALUMNO: ".substr(str_replace('/', ' ', $this->paDatos['paCurAlu'][0]['CNOMBRE']), 0, 62)), 0, 2, 'L');
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode("CÓDIGO: {$this->paDatos['paCurAlu'][0]['CCODALU']}"), 0, 2, 'L');
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode("DECRETO Nº {$this->paDatos['paCurAlu'][0]['CCODTRE']}-ORAA-VRACAD-$lcAnio"), 0, 2, 'L');
      $loPdf->Ln($lnHeight);
      $loPdf->SetFont('Courier', '', 12);
      if (in_array($this->paDatos['paCurAlu'][0]['CNIVUAC'], ['01','02'])) {
         $loPdf->Multicell($lnWidth, $lnHeight, utf8_decode("Vistos, el expediente en el que se solicita NOMBRAMIENTO DE JURADO, considerando lo estipulado en las ".
                           "Resoluciones Nº 1372-CU-96, Nº 2344-CU-2003, Nº 4265-CU-2010 en su Art.6, Nº 7286-CU-2020, y los informes y opinión del Director de la Escuela Profesional ".
                           $this->paDatos['paCurAlu'][0]['CNOMUNI']." y el(la) Decano(a) de la ".$this->paData['CDESCRI']."\nVRACAD\n\nSE DECRETA:\nPROCEDENTE la petición formulada por ".
                           substr(str_replace('/',' ',$this->paDatos['paCurAlu'][0]['CNOMBRE']),0,100)." con código Nº {$this->paDatos['paCurAlu'][0]['CCODALU']}, quien abonará los ".
                           "derechos pertinentes al número de créditos correspondiente a:"), 0, 'J');
      } elseif (in_array($this->paDatos['paCurAlu'][0]['CNIVUAC'], ['03','04','05'])) {
         $loPdf->Multicell($lnWidth, $lnHeight, utf8_decode("Vistos, el expediente en el que se solicita NOMBRAMIENTO DE JURADO, considerando lo estipulado en las ".
                           "Resoluciones Nº 1372-CU-96, Nº 2344-CU-2003, Nº 4265-CU-2010 en su Art.6, Nº 7286-CU-2020, y los informes y opinión del Director de la Escuela de Post-Grado ".
                           "y el(la) Coordinador(a) de ".$this->paDatos['paCurAlu'][0]['CNOMUNI']."\nVRACAD\n\nSE DECRETA:\nPROCEDENTE la petición formulada por ".
                           substr(str_replace('/',' ',$this->paDatos['paCurAlu'][0]['CNOMBRE']),0,100)." con código Nº {$this->paDatos['paCurAlu'][0]['CCODALU']}, quien abonará los ".
                           "derechos pertinentes al número de créditos correspondiente a:"), 0, 'J');
      }
      $loPdf->Ln(0.3);
      $loPdf->SetFont('Courier', 'B', 12);
      $loPdf->Cell($lnWidth, 0.4,             '----------------------------------------------------------------------', 0, 2, 'L');
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode(' # CÓDIGO  ASIGNATURA                                     SEM CRED INC'), 0, 2, 'L');
      $loPdf->Cell($lnWidth, 0.4,             '----------------------------------------------------------------------', 0, 2, 'L');
      $loPdf->SetFont('Courier', '', 12);
      $i = 0;
      $laDatos = [];
      foreach ($this->paDatos['paCurAlu'] as $laFila) {
         $laFila = array_map("utf8_decode", $laFila);
         $laDatos[] = [fxNumber($i+1, 2, 0).' '.$laFila['CCODCUR'].' '.fxString($laFila['CDESCRI'], 46).' '.substr($laFila['CCODCUR'], 2, 2).'  '.fxNumber($laFila['NCRETOT'], 4, 1).' '.utf8_decode($this->paData['INCISO'][$i])];
         $lcDescri = trim(fxStringTail($laFila['CDESCRI'], 46));
         $i++;
         do {
            if ($lcDescri == '') break;
            $laDatos[] = [fxString('', 11).fxString($lcDescri, 46).fxString('', 13)];
            $lcDescri = trim(fxStringTail($lcDescri, 46));
         } while(true);
      }
      foreach ($laDatos as $laFila) {
         $loPdf->Cell($lnWidth, $lnHeight, $laFila[0], 0, 2, 'L');
      }
      $loPdf->SetFont('Courier', 'B', 12);
      $loPdf->Cell($lnWidth, 0.4,             '----------------------------------------------------------------------', 0, 2, 'L');
      $loPdf->Ln(0.3);
      $loPdf->SetFont('Courier', '', 12);
      $loPdf->Multicell($lnWidth, $lnHeight, utf8_decode("En ningún caso se excederá al alcance de lo dispuesto por las Resoluciones ".
                        "Nº 1372-CU-96, Nº 2344-CU-2003, Nº 4265-CU-2010 en su Art.6 y Nº 7286-CU-2020.\nEl Director de la Escuela se encargará ".
                        "de disponer el cumplimiento del presente así como de la transcripción del Art. 10 de la mencionada Resolución ".
                        "en el reverso del Decreto de Nombramiento de Jurado, para mayor información de los docentes examinadores. Los ".
                        "documentos resultantes serán remitidos a la Dirección de Registro y Archivo Académico para su correspondiente ".
                        "distribución. ComunÍquese y procécese."), 0, 'J');
      $loPdf->Ln($lnHeight);
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode("Arequipa, ". $loDate->dateSimpleText($this->paDatos['paCurAlu'][0]['TFECHA'])), 0, 2, 'R');
      $loPdf->Ln($lnHeight*2);
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode("c.c. Archivo Académico"), 0, 2, 'L');
      $loPdf->Ln($lnHeight*2);
      $loPdf->SetFont('Courier', 'B', 11);
      $loPdf->Cell($lnWidth, $lnHeight, utf8_decode("*El alumno realizará el pago de derecho con el siguiente código: ".$this->paDatos['paCurAlu'][0]['CNROPAG']), 0, 2, 'L');
      $loPdf->Output('F', $this->pcFile, true);
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init cursos por jurado limitador de escuela
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omLimitadorDeEscuela() {
      $llOk = $this->mxValParamLimitadorDeEscuela();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxLimitadorDeEscuela($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamLimitadorDeEscuela() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DE ALUMNO DEFINIDO O NO VALIDO";
         return false;
      } 
      return true;
   }

   protected function mxLimitadorDeEscuela($p_oSql) {
      $lcSql = "SELECT cCodAlu FROM A01MALU A 
                INNER JOIN S01TUAC B ON A.cUniAca = B.cUniAca 
                WHERE B.cNivel IN ('01','02','03','04') AND A.cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "ESTA OPCIÓN AÚN NO ESTA HABILITADA PARA ESTE TIPO DE UNIDAD ACADEMICA";
         return false;
      }
      return true;  
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Denegados CPJ
   // 2019-07-23 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaCPJObservados() {
      $llOk = $this->mxValParamInitBandejaCPJObservados();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaCPJObservados($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaCPJObservados() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaCPJObservados($p_oSql) {
      $lcSql = "SELECT A.cIdenti, B.cCodAlu, D.cNombre, C.cDescri, 
                       C.cCodCur, A.cEstEsc,  A.nSerial, A.mObserv ,A.tModifi
                FROM B05DCPJ A 
                INNER JOIN B05MCPJ B ON A.cIdenti = B.cIdenti 
                INNER JOIN A02MCUR C ON C.cCodCur = A.cCodCur
                INNER JOIN V_A01MALU D ON D.cCodAlu = B.cCodAlu
                INNER JOIN A01MALU E ON E.cPlaEst = C.cPlaEst
                WHERE A.cEstEsc = 'D'
                GROUP BY A.cIdenti, B.cCodAlu, D.cNombre, C.cDescri, C.cCodCur, A.cEstEsc, A.nSerial, A.mObserv, A.tModifi ORDER BY A.tModifi DESC;";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDENTI' => $laFila[0],'CCODALU' => $laFila[1],
                             'CNOMBRE' => $laFila[2], 'CDESCRI' => $laFila[3],
                             'CCODCUR' => $laFila[4], 'CESTESC' => $laFila[5], 
                             'NSERIAL' => $laFila[6], 'MOBSERV' => $laFila[7],
                             'TMODIFI' => $laFila[8]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return false;
      }
      return true;
  }

   // ------------------------------------------------------------------------------
   // Levantar Observados Curso por Jurado
   // 2019-07-23 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omBandejaCPJLevObser() {
      $llOk = $this->mxValParamBandejaCPJLevObser();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBandejaCPJLevObser($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBandejaCPJLevObser() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxBandejaCPJLevObser($p_oSql) {
      $lcSql = "UPDATE B05DCPJ SET cEstEsc ='P', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "NO SE PUDO ACTUALIZAR";
         return false;
      }
      $lcSql = "UPDATE B05MCPJ SET cEstado ='S', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cIdenti = '{$this->paData['CIDENTI']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "NO SE PUDO ACTUALIZAR";
         return false;
      }
      return true;
  }
    
   public function omGeneraReporteDecretoJuradCPJ() {
      $llOk = $this->mxValParamGeneraReporteDecretoJuradCPJ();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGeneraReporteDecretoJuradCPJ($loSql);
      $loSql->omDisconnect();
      if (!$llOk) {
         return false;
      }
      //$this->paData = $this->paData;  
      $llOk = $this->mxPrintGeneraReporteDecretoJuradCPJ($loSql);
      return $llOk;
   }

   protected function mxValParamGeneraReporteDecretoJuradCPJ() {
      if (!isset($this->paData['CCODALU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } elseif (strlen($this->paData['CCODTRE']) != 6) {
         $this->pcError = "CODIGO DE TRAMITE INVALIDO";
      }
      return true;
   }

   protected function mxGeneraReporteDecretoJuradCPJ($p_oSql) {
      //FIRMAS DE RESPONSABLES
      $lcSql  = "SELECT F.cNombre, A.cNomUni, B.cEstPre FROM S01TUAC A 
                 INNER JOIN S01TCCO B ON B.cUniAca = A.cUniAca 
                 INNER JOIN B03PUSU C ON C.cCenCos = B.cCenCos AND C.cEstado = 'A'
                 INNER JOIN B03DUSU D ON D.cCodUsu = C.cCodUsu AND D.cEstado = 'A'
                 INNER JOIN S01TUSU E ON E.cCodUsu = D.cCodUsu
                 INNER JOIN S01MPER F ON F.cNroDni = E.cNroDni
                 WHERE A.cUniAca = '{$this->paData['CUNIACA']}' AND D.cNivel = '2'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->laDatos['paDirector'] = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->laDatos['paDirector'][] = ['CNOMBRE' => $laTmp[0], 'CNOMUNI' => $laTmp[1], 'CESTPRE' => $laTmp[2]];
      }
      if (count($this->laDatos) == 0) {
         $this->pcError = "DIRECTOR DE LA ESCUELA NO REGISTRADO EN EL SISTEMA";
         return false;
      }
      $lcSql  = "SELECT F.cNombre, B.cDescri FROM S01TUAC A 
                 INNER JOIN S01TCCO B ON B.cUniAca = A.cUniAca 
                 INNER JOIN B03PUSU C ON C.cCenCos = B.cCenCos AND C.cEstado = 'A'
                 INNER JOIN B03DUSU D ON D.cCodUsu = C.cCodUsu AND D.cEstado = 'A'
                 INNER JOIN S01TUSU E ON E.cCodUsu = D.cCodUsu
                 INNER JOIN S01MPER F ON F.cNroDni = E.cNroDni
                 WHERE B.cEstPre = '{$this->laDatos['paDirector'][0]['CESTPRE']}' AND B.cDescri LIKE '%FAC%'  AND D.cNivel = '3'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->laDatos['paDecano'] = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->laDatos['paDecano'][] = ['CNOMBRE' => $laTmp[0], 'CDESCRI' => $laTmp[1]];
      }
      if (count($this->laDatos) == 0) {
         $this->pcError = "DECANO DE LA ESCUELA NO REGISTRADO EN EL SISTEMA";
         return false;
      } 
      return true;
   }   

   // ------------------------------------------------------------------------------
   // Reporte decreto de nombramiento CPJ 
   // 2019-08-02 LVA Creacion
   // ------------------------------------------------------------------------------
   public function mxPrintGeneraReporteDecretoJuradCPJ() {
      $cont = 0;
      $loDate = new CDate();
      $lcCrecibo = $this->paData['CRECIBO'];
      $lcNroDni = $this->paData['CNRODNI'];
      $lcNomDire = str_replace('/', ' ',$this->laDatos['paDirector'][0]['CNOMBRE']);
      $lcNomDeca = str_replace('/', ' ',$this->laDatos['paDecano'][0]['CNOMBRE']);
      try {
         $fecha_actual = date ("Y-m-d");
         $año = date("Y");
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(20);
         $pdf->SetFont('Courier','B',15);
         if (in_array($this->paData['CNIVUNI'], ['03','04'])) {
            $pdf->Multicell(186, 5, utf8_decode('ESCUELA DE POSTGRADO'), 0, 'C');
         } else {
            $pdf->Multicell(186, 5, utf8_decode('ESCUELA PROFESIONAL DE '. $this->paData['CNOMUNI']), 0, 'C');
         }
         $pdf->Cell(186, 10, utf8_decode('NOMBRAMIENTO DE JURADO EVALUADOR DE CURSOS CON JURADO'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->SetFont('Courier','',10);
         $pdf->Cell(186, 10, utf8_decode('DECRETO Nº '.'E-'.$this->paData['CCODTRE'].'-ORAA-VRACAD-'.$año), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('FECHA     : '.$loDate->dateSimpleText($fecha_actual)), 0, 0, 'L');
         $pdf->Ln(10);
         $pdf->SetFont('Courier', '', 10);
         $pdf->SetX(10);
         if (in_array($this->paData['CNIVUNI'], ['03','04'])) {
            $pdf->Multicell(186,5,utf8_decode('NOMBRE: '. str_replace('/', ' ',$this->paData['CNOMALU']) .' con matricula Nº '.$this->paData['CCODALU'].' alumno(a) de '.$this->paData['CNOMUNI'].' '),0,'J');
         } else {
            $pdf->Multicell(186,5,utf8_decode('NOMBRE: '. str_replace('/', ' ',$this->paData['CNOMALU']) .' con matricula Nº '.$this->paData['CCODALU'].' alumno(a) de la Escuela Profesional de '.$this->paData['CNOMUNI'].' '),0,'J');
         }
         $pdf->Ln(5);
         $pdf->Cell(186, 8, utf8_decode('SE DECRETA:'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->SetX(10);
         $pdf->SetFont('Courier', '', 10);
         $pdf->Multicell(186, 5, utf8_decode('Nombrar al jurado que se encargará de evaluar al estudiante en las asignaturas mencionadas, el cual estará integrado por docentes de la especialidad, de acuerdo a la Res. Nº 1372-CU-96.'),0, 'J');
         $pdf->SetFont('Courier', 'B', 8);
         $pdf->SetX(10);
         $pdf->Ln(10);
         //Cabecera
         $pdf->Cell(17, 8, utf8_decode("CODIGO"), 1,0,'C');
         $pdf->Cell(63, 8, utf8_decode("ASIGNATURA"), 1, 0,'C');
         $posX = $pdf->GetX()+10;
         $posY = $pdf->GetY();
         $pdf->Multicell(10, 4, utf8_decode("T o T/P"), 1,'C');
         $pdf->SetXY($posX,$posY);
         $pdf->Cell(10, 8, utf8_decode("Pract"), 1, 0,'C');
         $pdf->Cell(10, 8, utf8_decode("SEM"), 1, 0,'C');
         $posX = $pdf->GetX()+10;
         $posY = $pdf->GetY();
         $pdf->Multicell(10, 4, utf8_decode("Nº CRED"), 1,'C');
         $pdf->SetXY($posX,$posY);
         $pdf->Cell(63, 8, utf8_decode("JURADOS"), 1, 0, 'C');
         $pdf->SetFont('Courier', '', 8);
         $EjeY = $pdf->GetY()+8;
         foreach ($this->paDatos as $laFila) {
            $lcPadd = 2;
            $lcPaddCal = 4;
            if(strlen($laFila['CNOMJU1']) > 34 ){
               $lcPaddCal=6;
            }
            if(strlen($laFila['CNOMJU2']) > 34 ){
               $lcPaddCal=8;
            }
            $pdf->SetXY(10,$EjeY);
            $pdf->Cell(17, $lcPadd * $lcPaddCal, utf8_decode($laFila['CCODCUR']), 1, 0,'C');
            if(strlen($laFila['CDESCRI'])>34){
               $pdf->Multicell(63, $lcPadd * ($lcPaddCal/2), utf8_decode($laFila['CDESCRI']), 1,'L');
            }
            else {
               $pdf->Multicell(63, $lcPadd * $lcPaddCal, utf8_decode($laFila['CDESCRI']), 1,'L');
            }
            $pdf->SetXY(90,$EjeY);
            if(empty($laFila['NCRETEO']) and !empty($laFila['NCRELAB'])){
               $pdf->Cell(10, $lcPadd * $lcPaddCal, utf8_decode("T"), 1, 0,'C');
               $pdf->Cell(10, $lcPadd * $lcPaddCal, utf8_decode(" "), 1, 0,'C');
            }
            if(!empty($laFila['NCRETEO']) and empty($laFila['NCRELAB'])){
               $pdf->Cell(10, $lcPadd * $lcPaddCal, utf8_decode(" "), 1, 0,'C');
               $pdf->Cell(10, $lcPadd * $lcPaddCal, utf8_decode("P"), 1, 0,'C');
            }
            else{
               $pdf->Cell(10, $lcPadd * $lcPaddCal, utf8_decode("T"), 1, 0,'C');
               $pdf->Cell(10, $lcPadd * $lcPaddCal, utf8_decode("P"), 1, 0,'C');
            }
            $pdf->Cell(10, $lcPadd * $lcPaddCal, utf8_decode(substr($laFila['CCODCUR'],2,-3)), 1, 0,'C');
            $pdf->Cell(10, $lcPadd * $lcPaddCal, utf8_decode($laFila['NCRETOT']), 1, 0,'C');
            $cont = $cont + $laFila['NCRETOT'];
            $pdf->SetXY(130,$EjeY);
            if (in_array($this->paData['CNIVUNI'], ['03','04'])) {
               $pdf->Multicell(63,$lcPadd * $lcPaddCal,utf8_decode('-'.str_replace('/',' ',$laFila['CNOMJU1'])),1,'L');
            } else {
               $pdf->Multicell(63,$lcPadd * 2,utf8_decode('-'.str_replace('/',' ',$laFila['CNOMJU1'])."\n".'-'.str_replace('/',' ',$laFila['CNOMJU2'])),1,'L');
            }
            $EjeY = $EjeY + $lcPadd * $lcPaddCal;
         }
         $pdf->Cell(110, 5, '', 1,'C');
         $pdf->Cell(10, 5, utf8_decode($cont), 1, 0,'C');
         $pdf->Ln(10);
         $pdf->SetX(10);
         $pdf->SetFont('Courier', 'B', 10);
         $pdf->Multicell(186, 5, utf8_decode('El tiempo máximo para la culminacion de la recepción de exámenes de suficiencia con jurado, es de sesenta días calendario, y el plazo mínimo es de quince días calendario; contados a partir de la fecha de nombramiento del jurado.'),0, 'J');
         $pdf->Ln(5);
         $pdf->Multicell(186, 5, utf8_decode('El estudiante realizó el pago con el ID '.$this->paData['CNROPAG'].' en la fecha '.$this->paData['TPAGO']. '.'),0, 'J');
         $pdf->SetFont('Courier', '', 10);
         $pdf->Ln(10);
         $pdf->SetFont('Courier', 'B', 7);
         if ($this->paData['CUNIACA'] == '70'){
            $pdf->SetX(13);
            $pdf->Cell(64,10,utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 'L');
            $pdf->Cell(64,10,utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 'L');
            $pdf->Cell(64,10,utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 'L');
            $pdf->Ln(15);
            $pdf->SetX(13);
            $pdf->Cell(64,10,utf8_decode('-----------------------------------'), 0, 'L');
            $pdf->Cell(64,10,utf8_decode('-----------------------------------'), 0, 'L');
            $pdf->Cell(64,10,utf8_decode('-----------------------------------'), 0, 'L');
            $pdf->Ln(5);
            $pdf->SetX(10);
            $pdf->Multicell(64,5, utf8_decode($lcNomDire),0,'C');
            $pdf->SetXY(74, $pdf->GetY()-5);
            $pdf->Multicell(64,5, utf8_decode(str_replace('/', ' ',$this->laDatos['paDirector'][1]['CNOMBRE'])),0,'C');
            $pdf->SetXY(138, $pdf->GetY()-5);
            $pdf->Multicell(64,5, utf8_decode($lcNomDeca),0,'C');
            $lcDesUni = 'Director de la Escuela Profesional de '.$this->laDatos['paDirector'][0]['CNOMUNI'];
            $lcDesFac = 'Decano de la '.$this->laDatos['paDecano'][0]['CDESCRI']; 
            $pdf->SetX(10);
            $pdf->Multicell(63,5,utf8_decode($lcDesUni),0,'C');
            $pdf->SetXY(73, $pdf->GetY()-10);
            $pdf->Multicell(63,5,utf8_decode($lcDesUni),0,'C');
            $pdf->SetXY(136, $pdf->GetY()-10);
            $pdf->Multicell(63,5, utf8_decode($lcDesFac),0,'C');
            $pdf->Ln(5);
         } elseif ($this->paData['CUNIACA'] == '64') {
            $pdf->SetX(30);
            $pdf->Cell(186,10,utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 'C');
            $pdf->Ln(15);
            $pdf->SetX(30);
            $pdf->Cell(186,10,utf8_decode('-----------------------------------                           -----------------------------------'), 0, 'L');
            $pdf->Ln(5);
            $pdf->SetX(10);
            $pdf->Multicell(186,5, utf8_decode($lcNomDeca),0,'C');
            $lcDesFac = 'Decano de la '.$this->laDatos['paDecano'][0]['CDESCRI']; 
            $pdf->SetX(25);
            $pdf->Multicell(63,5, utf8_decode($lcDesFac),0,'C');
            $pdf->Ln(5);
         } elseif (in_array($this->paData['CNIVUNI'], ['03','04'])) {
            $pdf->SetX(80);
            $pdf->Cell(186,10,utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 'C');
            $pdf->Ln(15);
            $pdf->SetX(80);
            $pdf->Cell(186,10,utf8_decode('-----------------------------------'), 0, 'L');
            $pdf->Ln(5);
            $pdf->SetX(75);
            $pdf->Multicell(63,5, utf8_decode('Director de la Escuela de Postgrado'),0,'C');
            $pdf->Ln(5);
         }else {
            $pdf->SetX(30);
            $pdf->Cell(93,10,utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 'L');
            $pdf->Cell(93,10,utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 'L');
            $pdf->Ln(15);
            $pdf->SetX(30);
            $pdf->Cell(186,10,utf8_decode('-----------------------------------                           -----------------------------------'), 0, 'L');
            $pdf->Ln(5);
            $pdf->SetX(10);
            $pdf->Multicell(93,5, utf8_decode($lcNomDire),0,'C');
            $pdf->SetXY(103, $pdf->GetY()-5);
            $pdf->Multicell(93,5, utf8_decode($lcNomDeca),0,'C');
            $lcDesUni = 'Director de la Escuela Profesional de '.$this->laDatos['paDirector'][0]['CNOMUNI'];
            $lcDesFac = 'Decano de la '.$this->laDatos['paDecano'][0]['CDESCRI']; 
            $pdf->SetX(25);
            $pdf->Multicell(63,5,utf8_decode($lcDesUni),0,'C');
            $pdf->SetXY(118, $pdf->GetY()-10);
            $pdf->Multicell(63,5, utf8_decode($lcDesFac),0,'C');
            $pdf->Ln(5);
         }
         $llOk = $pdf->Output($this->pcFile, "F");         
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Bandeja para el levantamiento de denegados de la escuela, cursos por jurado
   // 2019-04-01 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaSolicitudesCursosJuradoDenegadas() {
      $llOk = $this->mxValParamInitBandejaSolicitudesCursosJuradoDenegadas();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudesCursosJuradoDenegadas($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudesCursosJuradoDenegadas() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudesCursosJuradoDenegadas($p_oSql) {
      $lcSql = "SELECT DISTINCT ON (A.cIdenti) A.cIdenti, A.cCodAlu, B.cNombre, B.cNomUni, (TO_CHAR(A.dGenera, 'YYYY-MM-DD HH24:MI')),
                       A.cEstado FROM B05MCPJ A 
                INNER JOIN V_A01MALU B ON B.cCodAlu = A.cCodAlu
                INNER JOIN S01TCCO C ON C.cUniAca = B.cUniAca
                INNER JOIN B05DCPJ D ON D.cIdenti = A.cIdenti
                WHERE A.cEstado = 'C' AND D.cEstEsc ='D' AND D.cEstSac = 'D' AND C.cCenCos IN
                (SELECT t_cCenCos FROM F_B03PUSU_1('{$this->paData['CCODUSU']}') WHERE t_cUniAca != '00')";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDENTI' => $laFila[0], 'CCODALU' => $laFila[1], 'CNOMBRE' => $laFila[2], 'CUNIACA' => $laFila[3],
                             'DGENERA' => $laFila[4], 'CESTADO' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES DENEGADAS";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Seguimiento de la Escuela para revisar solicitudes de Cursos por Jurado
   // 2019-04-01 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRevisarSolicitudDenegada() {
      $llOk = $this->mxValParamRevisarSolicitudDenegada();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitRevisarSolicitudDenegada($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarSolicitudDenegada() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CIDENTI']) || (strlen($this->paData['CIDENTI']) != 6)) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      }
      return true;
   }

   protected function mxInitRevisarSolicitudDenegada($p_oSql) {
      $lcSql = "SELECT B.cCodAlu, E.cNombre, C.cDescri, C.cCodCur, A.nMonto, F.cNomUni, D.cPlaEst, C.ncreteo, C.ncrelab, A.cEstEsc, A.nSerial, A.cEstSac, E.cNroDni FROM B05DCPJ A 
                INNER JOIN B05MCPJ B ON A.cIdenti = B.cIdenti 
                INNER JOIN A02MCUR C ON C.cCodCur = A.cCodCur
                INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
                INNER JOIN S01MPER E ON E.cNroDni = D.cNroDni
                INNER JOIN S01TUAC F ON F.cUniAca = D.cUniAca
                WHERE A.cIdenti = '{$this->paData['CIDENTI']}' AND A.cEstEsc = 'D' AND B.cEstado = 'C' 
                GROUP BY B.cCodAlu, E.cNombre, C.cDescri, C.cCodCur, A.nMonto, F.cNomUni, D.cPlaEst, C.ncreteo, C.ncrelab, A.cEstEsc, A.nSerial, A.cEstSac, E.cNroDni ORDER BY C.cDescri;";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $laFila[13] = $laFila[7] + $laFila[8];
         $this->paDatos[] = ['CCODALU' => $laFila[0],'CNOMBRE' => $laFila[1], 'CDESCRI' => $laFila[2],
                             'CCODCUR' => $laFila[3], 'NMONTO' => $laFila[4],
                             'CNOMUNI' => $laFila[5], 'CPLAEST' => $laFila[6], 
                             'NCRETEO' => $laFila[7], 'NCRELAB' => $laFila[8], 
                             'CESTESC' => $laFila[9], 'NSERIAL' => $laFila[10], 
                             'CESTSAC' => $laFila[11],'CNRODNI' => $laFila[12], 'NCRETOT' => $laFila[13]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Denegar la escuela solicitud de cursos por jurado
   // 2019-04-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omLevantarDenegacionSolicitudCursoJurado() {  
      $llOk = $this->mxValParamLevantarDenegacionSolicitudCursoJurado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxLevantarDenegacionSolicitudCursoJurado($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamLevantarDenegacionSolicitudCursoJurado() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL'])) {
         $this->pcError = "IDENTIFICADOR DE CURSO NO ESPECIFICADA";
         return false;
      } elseif (!isset($this->paData['CIDENTI'])) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      } 
      return true;
   }
   protected function mxLevantarDenegacionSolicitudCursoJurado($p_oSql) {
      $lcSql = "UPDATE B05DCPJ SET cEstEsc = 'P', cEstSac = 'P', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL DENEGAR SOLICITUD PARA CURSOS DE JURADO';
         return false;
      }
      //CUENTAS LOS ESTADOS DENEGADOS
      $lcSql = "SELECT COUNT(CESTESC) FROM B05DCPJ WHERE CESTESC = 'P' AND CIDENTI = '{$this->paData['CIDENTI']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         return false;
      }
      $tmpCountEst = $p_oSql->fetch($llOk);
      //CUENTA CUANTOS CURSOS SOLICITADOS HAY
      $lcSql = "SELECT COUNT(NSERIAL) FROM B05DCPJ WHERE CIDENTI = '{$this->paData['CIDENTI']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         return false;
      }
      $tmpCountCan = $p_oSql->fetch($llOk);
      if($tmpCountEst[0] == $tmpCountCan[0]){
         $lcSql = "UPDATE B05MCPJ SET cEstado = 'S', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cIdenti = '{$this->paData['CIDENTI']}'";
         $llOk = $p_oSql->omExec($lcSql); 
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Detalle historial de CPJ llevados por alumno
   // 2019-08-28 LVA Creacion
   // --------------------------------------------------------------------------
   public function omHistorialDeCPJ() {  
      $llOk = $this->mxValParamHistorialDeCPJ();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxHistorialDeCPJ($loSql);
      if (!$llOk) {
         return false;
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamHistorialDeCPJ() {
      if (!isset($this->paData['CCODALU']) || empty($this->paData['CCODALU']) || strlen($this->paData['CCODALU'])>10) {
         $this->pcError = "CODIGO DE ALUMNO INVALIDO";
         return false;
      }
      return true;
   }
   protected function mxHistorialDeCPJ($p_oSql) {
      /*$lcSql = "SELECT A.cIdenti, A.tModifi, A.dGenera, B.cNroPag FROM B05MCPJ A
                INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud AND A.cIdDeud <> '000000'
                WHERE A.cEstado NOT IN ('X') AND A.cCodAlu = '{$this->paData['CCODALU']}'";*/
      $lcSql = "SELECT A.cIdenti, A.tModifi, A.dGenera, B.cNroPag, A.cEstado FROM B05MCPJ A
                INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud 
                WHERE A.cEstado NOT IN ('X') AND A.cCodAlu = '{$this->paData['CCODALU']}'";
      $llOk = $p_oSql->omExec($lcSql);
      $this->paDatos['paMcpj'] = [];
      if (!$llOk) {
         $this->pcError = 'ERROR AL BUSCAR TRAMITE';
         return false;
      }
      $i=0;
      while ($laFila = $p_oSql->fetch($llOk)){
         $lcCidenti = $laFila[0];
         $lcSql = "SELECT A.cCodCur, B.cDescri, A.mObserv, A.cEstEsc, A.cEstSac, C.cNombre, D.cNombre FROM B05DCPJ A 
                   INNER JOIN A02MCUR B ON A.cCodCur = B.cCodCur
                   INNER JOIN V_A01MDOC_1 C ON C.cCodDoc = A.cJurad1
                   INNER JOIN V_A01MDOC_1 D ON D.cCodDoc = A.cJurad2 
                   WHERE cIdenti = '$lcCidenti'";
         $llOk1 = $p_oSql->omExec($lcSql);
         $this->paDatos['paMaCpj'][] = ['CIDENTI' => $laFila[0],'TMODIFI' => $laFila[1], 'DGENERA' => $laFila[2], 'CNROPAG' => $laFila[3], 
                                        'CESTADO' => $laFila[4], 'paDeCpj' => []]; 
         while ($laFila1 = $p_oSql->fetch($llOk1)){
            if(empty($laFila1[2])) {
               $laFila1[2] = 'SIN OBSERVACIÓN';
            }
            $this->paDatos['paMaCpj'][$i]['paDeCpj'][] = ['CCODCUR' => $laFila1[0], 'CDESCRI' => $laFila1[1], 'MOBSERV' => $laFila1[2], 
                                                          'CESTESC' => $laFila1[3], 'CESTSAC' => $laFila1[4], 'CJURAD1' => $laFila1[5],
                                                          'CJURAD2' => $laFila1[6]];
         }
         $i++; 
      }
      if(count($this->paDatos['paMaCpj']) == 0){
         $this->pcError = 'NO TIENE CURSOS POR JURADO PREVIOS';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Entregar Tramite de Curso por Jurado Completo ORAA
   // 2019-09-13 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omEntregarTramiteCpj() {
      $llOk = $this->mxValParamEntregarTramiteCpj();
      if (!$llOk) {        
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEntregarTramiteCpj($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEntregarTramiteCpj() {
      if (!isset($this->paData['paCIdenti']) || count($this->paData['paCIdenti']) == 0) {
         $this->pcError = "SIN TRÁMITES POR APROBAR";
         return false;
      }
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO O NO VALIDO";
      }
      return true;
   }

   protected function mxEntregarTramiteCpj($p_oSql) {
      foreach ($this->paData['paCIdenti'] as $lcIdenti) {
         $lcSql = "UPDATE B05MCPJ SET cEstado = 'E', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}' WHERE cIdenti = '$lcIdenti'";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL APROBAR CONSTANCIA";
            return false;
         }
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Reporte Historila de Cursos pos Jurado que un Docente a llevado
   // 2019-09-24 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omReporteDeCPJDocentes() {
      $llOk = $this->mxValmxReporteDeCPJDocentes();
      if (!$llOk) {        
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxReporteDeCPJDocentes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValmxReporteDeCPJDocentes() {
      if (!isset($this->paData['CUNIACA']) || empty($this->paData['CUNIACA'])) {
         $this->pcError = "NO SE ENCUENTRA UNIDAD ACADEMICA";
         return false;
      }
      return true;
   }
   public function mxReporteDeCPJDocentes($p_oSql){
      //CPJ APROBADOS FILTRO POR CUNIACA
      $lcSql = "SELECT A.cIdenti FROM B05MCPJ A 
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu
                AND B.cUniAca = '71' WHERE A.cEstado = 'A'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL BUSCAR CURSOS POR JURADO';
         return false;
      }
      $lcSqlIn = '(';
      while ($laFila = $p_oSql->fetch($llOk)) {
         $lcSqlIn .= "'$laFila[0]',";
      }
      $lcSqlIn = substr($lcSqlIn,0,-1).')';
      //DETALLE DE CPJ
      $lcSql = "SELECT cJurad1, cJurad2, cCodCur FROM B05DCPJ 
                WHERE cIdenti IN $lcSqlIn";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL BUSCAR CURSOS POR JURADO';
         return false;
      }
      while ($laFila = $p_oSql->fetch($llOk)) {
         if($laFila[0] != '0000'){
            $this->paDatos['CpjDetall']["$laFila[0]"][]=$laFila[2]; 
            $this->paDatos['CpjDetall']["$laFila[1]"][]=$laFila[2];
         }
      }
      foreach($this->paDatos as $laData){
         foreach ($laData as $lcCodDoc=>$lcCodCur) {
            $lcSql = "SELECT A.cNombre,B.cCodDoc, B.cEstado FROM S01MPER A
                      INNER JOIN A01MDOC B ON B.cNroDni = A.cNroDni
                      WHERE B.cCodDoc = '$lcCodDoc'"; 
            $llOk = $p_oSql->omExec($lcSql);
            if (!$llOk) {
               $this->pcError = 'DOCENTE NO SE ENCONTRO';
               return false;
            }
            $R1 = $p_oSql->fetch($llOk);
            $this->paDatos['cDocCpj'][] = [$R1[0], $R1[1], $R1[2]];
            $lcCurNom = [];
            foreach($lcCodCur as $lcCodDes) {
               $lcSql = "SELECT cDescri FROM A02MCUR
                         WHERE cCodCur = '$lcCodDes'"; 
               $llOk = $p_oSql->omExec($lcSql);
               if (!$llOk) {
                  $this->pcError = 'CURSO NO SE ENCONTRO';
                  return false;
               }
               $R1 = $p_oSql->fetch($llOk);
               array_push($lcCurNom, $R1[0]);
            }
            $this->paDatos['cCurDes'][] = $lcCurNom;
         }    
      }
      try {
         $pdf=new PDF_Code128();
         $i = 0;
         foreach($this->paDatos['cDocCpj'] as $lcDocent) {
            $pdf->AddPage('P','A4');
            $pdf->SetFont('Courier', 'B', 13);
            $pdf->Ln(5);
            $pdf->Multicell(190, 4, utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'), 0, 'C');
            $pdf->Ln(6);
            $pdf->SetFont('Courier', 'B', 12);
            $pdf->Cell(1, 0, utf8_decode('INFORME CURSOS POR JURADO, DOCENTE:'), 0, 0, 'L');
            $pdf->Ln(6);
            $pdf->SetFont('Courier','',12);
            $pdf->Cell(35, 0, utf8_decode('Nombre'), 0, 0, 'L');
            $pdf->Cell(72, 0, utf8_decode(':  '.str_replace('/',' ',$lcDocent[0])), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->Cell(35, 0, utf8_decode('Codigo'), 0, 0, 'L');
            $pdf->Cell(72, 0, utf8_decode(':  '.$lcDocent[1]), 0, 0, 'L');
            $pdf->Ln(5);
            $pdf->Cell(35, 0, utf8_decode('Estado'), 0, 0, 'L');
            if($lcDocent[2] == 'A') {
               $pdf->Cell(72, 0, utf8_decode(':  ACTIVO'), 0, 0, 'L');
            } else {
               $pdf->Cell(72, 0, utf8_decode(':  INACTIVO'), 0, 0, 'L');
            }
            $pdf->Ln(5);
            $pdf->SetFont('Courier', 'B', 10);
            $pdf->Cell(0, 5, '------------------------------------------------------------------------------------', 0);
            $pdf->Ln(15);
            $pdf->Cell(180, 8, utf8_decode("ASIGNATURA(S)"), 1, 0, 'C');
            $pdf->Ln();
            $pdf->SetFont('Courier', '', 10);
            $pdf->SetXY($pdf->GetX(),$pdf->GetY());
            $j=0;
            for($j; $j< count($this->paDatos['cCurDes'][$i]);$j++) {
               $pdf->Multicell(180, 5, utf8_decode($this->paDatos['cCurDes'][$i][$j]), 1,'L');
            }
            $i++;
         }
         $pdf->Output('F',$this->pcFile);
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // DECRETO CPJ DENEGADO
   // 2019-26-09 LVA Creacion
   // --------------------------------------------------------------------------<
   public function omGenerarDecretoCpjDenegado() {
      $llOk = $this->mxValParamGenerarDecretoCpjDenegado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk  = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGenerarDecretoCpjDenegado($loSql);
      $loSql->omDisconnect();
      if (!$llOk) {
         return false;
      }
      //$this->paData = $this->paData;  
      $llOk = $this->mxPrintGenerarDecretoCpjDenegado($loSql);
      return $llOk;
   }
   protected function mxValParamGenerarDecretoCpjDenegado() {
      if (!isset($this->paData['CIDENTI'])) {
         $this->pcError = 'TRAMITE NO SELECCIONADO';
         return false;
      } 
      return true;
   }

   protected function mxGenerarDecretoCpjDenegado($p_oSql) {
      $lcSql  = "SELECT A.cCodCur, B.cDescri, A.mObserv, C.cCodAlu, E.cNombre, F.cNomUni, C.tModifi FROM B05DCPJ A
                 INNER JOIN A02MCUR B ON B.cCodCur = A.cCodCur
                 INNER JOIN B05MCPJ C ON C.cIdenti = A.cIdenti
                 INNER JOIN A01MALU D ON D.cCodAlu = C.cCodAlu
                 INNER JOIN S01MPER E ON E.cNroDni = D.cNroDni
                 INNER JOIN S01TUAC F ON F.cUniAca = D.cUniAca
                 WHERE A.cIdenti = '{$this->paData['CIDENTI']}'";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODCUR' => $laTmp[0], 'CDESCRI' => $laTmp[1],
                             'MOBSERV' => strtoupper($laTmp[2]), 'CCODALU' => $laTmp[3],
                             'CNOMBRE' => $laTmp[4], 'CNOMUNI' => $laTmp[5],
                             'TMODIFI' => $laTmp[6]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN REGISTROS ENCONTRADOS";
         return false;
      }
      return true;
   }   

   protected function mxPrintGenerarDecretoCpjDenegado() {
      try {
         $pdf = new PDF_Code128('P', 'mm');
         $loDate = new CDate();
         $pdf->AddPage();         
         $pdf->SetFont('Courier','B',13);
         $pdf->Cell(276, 12, utf8_decode("ESCUELA PROFESIONAL DE {$this->paDatos[0]['CNOMUNI']}"), 0, 0, 'L');
         $pdf->Ln(8);
         $pdf->SetFont('Courier','B',12);
         $pdf->Cell(53, 12, utf8_decode("EXPEDIENTE: ERP-CPJU{$this->paData['CIDENTI']}"), 0, 0);
         $pdf->Ln(5);
         $pdf->Cell(55, 12, utf8_decode("ALUMNO: ".substr(str_replace('/', ' ', $this->paDatos[0]['CNOMBRE']), 0, 300)), 0, 0);
         //$pdf->Cell(97, 12, utf8_decode("ALUMNO:" .substr(str_replace('/',' ', $this->paDatos['paCurAlu'][0]['CNOMBRE']),0,100)), 0, 0, 'R');
         $pdf->Ln(5);
         $pdf->Cell(72, 12, utf8_decode("CÓDIGO DE ALUMNO: {$this->paDatos[0]['CCODALU']}"), 0, 0);
         //$pdf->Cell(62, 8, utf8_decode(substr(str_replace('/', ' ', $laFila['CNOMBRE']), 0, 28)), 1, 0);
         $pdf->Ln(7);
         $pdf->SetFont('Courier', '', 12);
         $pdf->SetY(44);
         $pdf->Multicell(189, 5, utf8_decode("La solicitud de Cursos Por Jurado realizada por el alumno(a) ".substr(str_replace('/',' ',$this->paDatos[0]['CNOMBRE']),0,100). " con código Nº {$this->paDatos[0]['CCODALU']} ".
         "fue denegada por la escuela profesional: \n"), 0, 'J');
         //$pdf->Cell(145, 12, utf8_decode(substr(str_replace('/',' ',$this->paDatos['paCurAlu'][0]['CNOMBRE']),0,100). " con código Nº {$this->paDatos['paCurAlu'][0]['CCODALU']} "), 0, 0);
         $pdf->Ln(10);
         $pdf->SetFont('Courier','B',12);
         $pdf->Cell(90, 4, utf8_decode("ASIGNATURA"), 1, 0,'C');
         $pdf->Cell(90, 4, utf8_decode("OBSERVACION"), 1, 0,'C');
         $pdf->SetFont('Courier','',12);
         $pdf->SetXY(10,$pdf->GetY()+4);
         foreach ($this->paDatos as $lcCursos) {
            if(strlen($lcCursos['CDESCRI'])>34 && strlen($lcCursos['MOBSERV'])<34){
               $pdf->Multicell(90,4, utf8_decode($lcCursos['CDESCRI']),0,'L');
               $pdf->SetXY(100,$pdf->GetY()-8);
               $pdf->Multicell(90,4*2, utf8_decode($lcCursos['MOBSERV']),0,'L');
            } elseif(strlen($lcCursos['MOBSERV'])>34 && strlen($lcCursos['CDESCRI'])<34){
               $pdf->Multicell(90,4*2, utf8_decode($lcCursos['CDESCRI']),0,'L');
               $pdf->SetXY(100,$pdf->GetY()-8);
               $pdf->Multicell(90,4, utf8_decode($lcCursos['MOBSERV']),0,'L');
            } else {
               $pdf->Multicell(90,4, utf8_decode($lcCursos['CDESCRI']),0,'L');
               $pdf->SetXY(100,$pdf->GetY()-4);
               $pdf->Multicell(90,4, utf8_decode($lcCursos['MOBSERV']),0,'L');
            }
         }
         $pdf->Ln(4);
         $pdf->Cell(67, 12, utf8_decode("Arequipa, ". $loDate->dateSimpleText($this->paDatos[0]['TMODIFI'])), 0, 0);
         $pdf->Ln(15);
         $llOk = $pdf->Output($this->pcFile, "F");         
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init modificar el creditaje de matricula del alumno
   // Consulta los codigos de los alumnos
   // 2020-02-13 FLC Creacion
   // ------------------------------------------------------------------------------
   public function omInitMatriculaxCreditosConsulta() {

      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitMatriculaxCreditosConsulta($loSql);
      $loSql->omDisconnect();
      $this->paData = $this->laData;  
      return $llOk;
   }

   protected function mxInitMatriculaxCreditosConsulta($p_oSql) {
      $lcSql = "SELECT A.cCodAlu FROM A01MALU A
                  INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca
                     WHERE B.CNIVEL = '01' AND B.cEstado != 'X' AND A.cNroDni = '{$this->paData['CNRODNI']}'";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU'=> $laFila[0]];
      }
      $lcSql = "SELECT TRIM(cConten) FROM S01TVAR WHERE cVariab = 'PCPERCRE';";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);     
      if (!isset($laTmp[0])) {
         $this->pcError = "PERIODO DE MATRICULAS POR CREDITAJE NO EXISTE";
         return false;
      }
      $lcSql = "SELECT B.cPeriod, A.cUniAca, B.nCosCre, C.cNomUni FROM A01MALU A
                 INNER JOIN B05DCCR B ON B.cUniAca = A.cUniAca
                 INNER JOIN S01TUAC C ON C.cUniAca = A.cUniAca
                 WHERE B.cEstado = 'A' AND B.cTipo = 'C' AND A.cCodAlu = '{$this->paDatos[0]['CCODALU']}' AND B.cPeriod = '{$laTmp[0]}';";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);     
      if (!isset($laTmp[0])) {
         $this->pcError = "COSTO POR CRÉDITO NO ENCONTRADO";
         return false;
      }
      $this->laData = ['CPERIOD' => $laTmp[0], 'CUNIACA' => $laTmp[1], 'NCOSCRE' => $laTmp[2], 'CNOMUNI' => $laTmp[3]];
      $lcSql = "SELECT nSerial, cEstado, nCredit, nMonto, cCodAlu, cPeriod, cEstInf FROM B05DMPC 
                WHERE cCodAlu = '{$this->paDatos[0]['CCODALU']}' AND cPeriod = '{$this->laData['CPERIOD']}' AND cEstado IN ('P', 'U') 
                ORDER BY nSerial DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->laData = $this->laData + ['NSERIAL' => -1, 'NMONTO' => 0.00, 'CESTADO' => NULL, 'NCREREG' => 0.00];
         return true;
      }   
      $this->laData = $this->laData + ['NSERIAL' => $laTmp[0], 'CESTADO' => $laTmp[1], 
                                       'NCREDIT' => $laTmp[2], 'NMONTO' => $laTmp[3], 
                                       'CCODALU' => $laTmp[4], 'CPERIOD' => $laTmp[5],
                                       'CESTINF' => $laTmp[6]];

      $lcSql = "SELECT SUM(NCREDIT) FROM B05DMPC WHERE cCodAlu = '{$this->paDatos[0]['CCODALU']}' AND cPeriod = '{$this->laData['CPERIOD']}' AND cEstado = 'U'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);    
      $this->laData = $this->laData + ['NCREREG' => $laTmp[0]];
      return true; 
   }

   // ------------------------------------------------------------------------------
   // Consulta Costo por credito Unidad Academica
   // 2020-02-13 FLC Creacion
   // ------------------------------------------------------------------------------
   public function omConsultaUniAcaCosto() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxConsultaUniAcaCosto($loSql);
      $loSql->omDisconnect();
      $this->paData = $this->laData;  
      return $llOk;
   }

   protected function mxConsultaUniAcaCosto($p_oSql) {
      $lcSql = "SELECT TRIM(cConten) FROM S01TVAR WHERE cVariab = 'PCPERCRE';";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);     
      if (!isset($laTmp[0])) {
         $this->pcError = "PERIODO DE MATRICULAS POR CREDITAJE NO EXISTE";
         return false;
      }
      $lcSql = "SELECT B.cPeriod, A.cUniAca, B.nCosCre, C.cNomUni FROM A01MALU A
                 INNER JOIN B05DCCR B ON B.cUniAca = A.cUniAca
                 INNER JOIN S01TUAC C ON C.cUniAca = A.cUniAca
                 WHERE B.cEstado = 'A' AND B.cTipo = 'C' AND A.cCodAlu = {$this->paData['CCODALU']} AND B.cPeriod = '{$laTmp[0]}';";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);     
      if (!isset($laTmp[0])) {
         $this->pcError = "COSTO POR CRÉDITO NO ENCONTRADO";
         return false;
      }
      $this->laData = ['CPERIOD' => $laTmp[0], 'CUNIACA' => $laTmp[1], 'NCOSCRE' => $laTmp[2], 'CNOMUNI'=>$laTmp[3]];
      $lcSql = "SELECT nSerial, cEstado, nCredit, nMonto, cCodAlu, cPeriod, cEstInf FROM B05DMPC 
                WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cPeriod = '{$this->laData['CPERIOD']}' AND cEstado IN ('P', 'U') 
                ORDER BY nSerial DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->laData = $this->laData + ['NSERIAL' => -1, 'NMONTO' => 0.00, 'CESTADO' => NULL, 'NCREREG' => 0.00];
         return true;
      }   
      $this->laData = $this->laData + ['NSERIAL' => $laTmp[0], 'CESTADO' => $laTmp[1], 
                                       'NCREDIT' => $laTmp[2], 'NMONTO' => $laTmp[3], 
                                       'CCODALU' => $laTmp[4], 'CPERIOD' => $laTmp[5],
                                       'CESTINF' => $laTmp[6]];

      $lcSql = "SELECT SUM(NCREDIT) FROM B05DMPC WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cPeriod = '{$this->laData['CPERIOD']}' AND cEstado = 'U'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);    
      $this->laData = $this->laData + ['NCREREG' => $laTmp[0]];
      return true; 
   }

   //REGISTRO DE CONVALIDACION DE CURSOS
   public function omInitRegistroConvalidacionDeCursos() {
      $llOk = $this->mxValParamInitRegConvalidacionDeCursos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitRegConvalidacionDeCursos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitRegConvalidacionDeCursos() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxInitRegConvalidacionDeCursos($p_oSql) {
       //CARGA TIPOS DE CONVALIDACION
       $lcSql = "SELECT TRIM(cCodigo), cDescri FROM V_S01TTAB WHERE cCodTab = '506'";
       $R1 = $p_oSql->omExec($lcSql);
       if ($R1 == false || $p_oSql->pnNumRow == 0) {
          $this->pcError = "ERROR AL RECUPERAR TIPO DE CONVALIDACION";
          return false;
       }
       while ($laTmp = $p_oSql->fetch($R1)) {
          $this->paTipCon[] = ['CCODIGO' => $laTmp[0], 'CDESCRI' => $laTmp[1]];
       }
       return true;
   }
   // CONVALIDACION DE CURSOS
   // 2020-04-28 JLF - Creación
   public function omInitConvalidacionDeCursos() {
      $llOk = $this->mxValParamInitConvalidacionDeCursos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitConvalidacionDeCursos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitConvalidacionDeCursos() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DE ALUMNO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CUNIACA']) || strlen(trim($this->paData['CUNIACA'])) != 2) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CNRODNI']) || strlen(trim($this->paData['CNRODNI'])) != 8) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxInitConvalidacionDeCursos($p_oSql) {
      //CARGA TIPOS DE CONVALIDACION
      $lcSql = "SELECT TRIM(cCodigo), cDescri FROM V_S01TTAB WHERE cCodTab = '506'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "ERROR AL RECUPERAR TIPO DE CONVALIDACION";
         return false;
      }
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paTipCon[] = ['CCODIGO' => $laTmp[0], 'CDESCRI' => $laTmp[1]];
      }
      //CARGA CODIGOS DE ALUMNO INACTIVOS (TRSLADO INTERNO)
      $lcSql = "SELECT cCodAlu, cNomUni FROM V_A01MALU WHERE cNroDni = '{$this->paData['CNRODNI']}' AND cEstado = 'I' ORDER BY cCodAlu DESC";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL RECUPERAR TIPO DE CONVALIDACION";
         return false;
      }
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paAluAnt[] = ['CCODALU' => $laTmp[0], 'CNOMUNI' => $laTmp[1]];
      }
      //CARGA CODIGOS DE ALUMNO ACTIVOS DEL MISMO NIVEL DE UNIDAD (DOBLE CARRERA ACTIVA)
      $lcSql = "SELECT cCodAlu, cNomUni FROM V_A01MALU WHERE cNroDni = '{$this->paData['CNRODNI']}' AND cEstado = 'A' AND cNivel IN 
                   (SELECT cNivel FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}') AND cCodAlu != '{$this->paData['CCODALU']}'
                ORDER BY cCodAlu DESC";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL RECUPERAR TIPO DE CONVALIDACION";
         return false;
      }
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paAluAct[] = ['CCODALU' => $laTmp[0], 'CNOMUNI' => $laTmp[1]];
      }
      //CARGA INFORMACION DEL ALUMNO
      $lcSql = "SELECT B.cNroCel, B.cEmail FROM A01MALU A INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni WHERE A.cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "ERROR AL RECUPERAR INFORMACIÓN DEL ALUMNO";
         return false;
      }
      $laTmp = $p_oSql->fetch($R1);
      $this->paData['CNROCEL'] = $laTmp[0];
      $this->paData['CEMAIL'] = $laTmp[1];
      //CARGA CURSOS PENDIENTES EN PLAN DE ESTUDIOS
      $lo = new CWebService();
      $lo->paData = $this->paData;
      $llOk = $lo->omRevisarCursosJurado();
      if (!$llOk) {
         $this->pcError = 'NO SE PUEDE RECUPERAR CURSOS PENDIENTES PARA CONVALIDAR';
         return false;
      }
      foreach ($lo->paData['MDATOS'] as $laFila) {
         if (strpos($laFila['CCODCUR'], 'CC')) continue;
         $this->paCursos[] = $laFila;
      }
      //CARGA COSTO DE CONVALIDACIONES POR CURSO
      $lcSql = "SELECT cIdCate, cDescri, nMonto FROM B03TDOC WHERE cIdCate = 'PDCONV'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "ERROR AL RECUPERAR COSTO DE CONVALIDACION POR CURSO";
         return false;
      }
      $laTmp = $p_oSql->fetch($R1);
      $this->paData['NMONCON'] = $laTmp[2];
      //CARGA COSTO DE DERECHO DE TRAMITE CONVALIDACION
      $lcSql = "SELECT cIdCate, cDescri, nMonto FROM B03TDOC WHERE cIdCate = '000109'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "ERROR AL RECUPERAR COSTO DERECHO DE TRAMITE CONVALIDACION";
         return false;
      }
      $laTmp = $p_oSql->fetch($R1);
      $this->paData['NMONDER'] = $laTmp[2];
      //CARGA ULTIMA SOLICITUD
      $lcSql = "SELECT A.cIdConv, A.cEstado, C.cDescri AS cDesEst, TO_CHAR(A.tGenera, 'YYYY-MM-DD HH24:MI'), A.cTipo,
                       D.cDescri AS cDesTip, A.cAluAnt, E.cUniAca AS cUAcAnt, E.cNomUni AS cNomAnt, A.cUsuDep,
                       TO_CHAR(A.tRevDep, 'YYYY-MM-DD HH24:MI'), A.cUsuAca, TO_CHAR(A.tRevAca, 'YYYY-MM-DD HH24:MI'),
                       A.mObserv, TRIM(B.cNroPag), B.cEstado AS cEstDeu, TO_CHAR(B.dFecha, 'YYYY-MM-DD HH24:MI'),
                       TO_CHAR(B.dRecepc, 'YYYY-MM-DD HH24:MI'), F.cDescri AS cDesDeu, A.mDetall FROM B06MCNV A
                INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                LEFT OUTER JOIN V_S01TTAB C ON C.cCodTab = '505' AND C.cCodigo = A.cEstado
                LEFT OUTER JOIN V_S01TTAB D ON D.cCodTab = '506' AND D.cCodigo = A.cTipo
                INNER JOIN V_A01MALU E ON E.cCodAlu = A.cAluAnt
                LEFT OUTER JOIN V_S01TTAB F ON F.cCodTab = '157' AND F.cCodigo = B.cEstado
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado NOT IN ('F','X') ORDER BY A.tGenera DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR CONVALIDACIONES";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $laData = ['CIDCONV' => '', 'CTIPCON' => '', 'CALUANT' => '', 'CUNIANT' => '', 'CESCANT' => ''];
         $this->paData = array_merge($this->paData, $laData);
         return true;
      }
      $laTmp = $p_oSql->fetch($R1);
      $laDetall = json_decode($laTmp[19], true);
      $laData = ['CIDCONV' => $laTmp[0], 'CESTADO' => $laTmp[1], 'CDESEST' => $laTmp[2], 'TGENERA' => $laTmp[3],
                 'CTIPCON' => $laTmp[4], 'CDESTIP' => $laTmp[5], 'CALUANT' => $laTmp[6], 'CUACANT' => $laTmp[7],
                 'CNOMANT' => $laTmp[8], 'CUSUDEP' => $laTmp[9], 'TREVDEP' => ($laTmp[10] == null)? 'S/D' : $laTmp[10],
                 'CUSUACA' => $laTmp[11],'TREVACA' => ($laTmp[12] == null)? 'S/D' : $laTmp[12],'MOBSERV' => $laTmp[13],
                 'CNROPAG' => $laTmp[14],'CESTDEU' => $laTmp[15],'TGENDEU' => $laTmp[16],
                 'TPAGO' => ($laTmp[17] == null)? 'S/D' : $laTmp[17],'CDESDEU' => $laTmp[18],
                 'CUNIANT' => (isset($laDetall['CUNIANT']))? $laDetall['CUNIANT'] : 'S/D',
                 'CESCANT' => (isset($laDetall['CESCANT']))? $laDetall['CESCANT'] : 'S/D'];
      //CARGA CURSOS DE SOLICITUD
      $lcSql = "SELECT A.cIdConv, A.cCodCur, B.cDescri, A.cEstado, C.cDescri AS cDesEst, A.cSilCur, A.cSilCnv,
                       A.cCodDoc, REPLACE(D.cNombre, '/', ' ') AS cNomDoc, TO_CHAR(A.tAsigna, 'YYYY-MM-DD HH24:MI'),
                       TO_CHAR(A.tRevisi, 'YYYY-MM-DD HH24:MI'), A.mObserv, A.cCurCnv, E.cDescri AS cDesCnv, A.mAnotac
                FROM B06DCNV A
                INNER JOIN A02MCUR B ON B.cCodCur = A.cCodCur
                LEFT OUTER JOIN V_S01TTAB C ON C.cCodTab = '507' AND C.cCodigo = A.cEstado
                INNER JOIN V_A01MDOC D ON D.cCodDoc = A.cCodDoc
                INNER JOIN A02MCUR E ON E.cCodCur = A.cCurCnv
                WHERE A.cIdConv = '{$laData['CIDCONV']}' AND A.cEstado != 'X' ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0){
         $this->pcError = "ERROR AL CARGAR DETALLE DE SOLICITUD";
         return false;
      }
      while ($laTmp = $p_oSql->fetch($R1)) {
         $laAnotac = json_decode($laTmp[14], true);
         $this->paDatos[] = ['CCODCUR' => $laTmp[1], 'CDESCRI' => $laTmp[2], 'CESTADO' => $laTmp[3], 'CDESEST' => $laTmp[4],
                             'CSILCUR' => $laTmp[5], 'CSILCNV' => $laTmp[6], 'CCODDOC' => $laTmp[7], 'CNOMDOC' => $laTmp[8],
                             'TASIGNA' => ($laTmp[9] == null)? 'S/D' : $laTmp[9], 'TREVISI' => ($laTmp[10] == null)? 'S/D' : $laTmp[10],
                             'MOBSERV' => $laTmp[11],'CCURCNV' => $laTmp[12],'CDESCNV' => $laTmp[13],
                             'CASICUR' => (isset($laAnotac['CASICUR']))? $laAnotac['CASICUR'] : 'S/D'];
      }
      $this->paData = array_merge($this->paData, $laData);
      return true;  
   }

   // BUSCAR ASIGNATURAS CURSADAS PARA CONVALIDAR
   // 2020-05-04 JLF - Creación
   public function omBuscarCursoConvalidacion() {
      $llOk = $this->mxValParamBuscarCursoConvalidacion();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBuscarCursoConvalidacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBuscarCursoConvalidacion() {
      if (!isset($this->paData['CNRODNI']) || strlen(trim($this->paData['CNRODNI'])) != 8) {
         $this->pcError = "DNI DE ALUMNO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CALUANT']) || strlen(trim($this->paData['CALUANT'])) != 10) {
         $this->pcError = "CODIGO DE ALUMNO ANTIGUO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CBUSCUR']) || strlen(trim($this->paData['CBUSCUR'])) != 7) {
         $this->pcError = "CLAVE DE BUSQUEDA INVALIDA";
         return false;
      }
      return true;
   }

   protected function mxBuscarCursoConvalidacion($p_oSql) {
      $lcNroDni = $this->paData['CNRODNI'];
      $lcAluAnt = $this->paData['CALUANT'];
      $lcBusCur = mb_strtoupper(str_replace(' ', '%', $this->paData['CBUSCUR']));
      $lcSql = "SELECT cCodCur, cDescri FROM A02MCUR WHERE cUniAca IN (SELECT cUniAca FROM A01MALU WHERE cCodAlu = '$lcAluAnt' AND cNroDni = '$lcNroDni')
                   AND (cCodCur ='$lcBusCur' OR cDescri LIKE '%$lcBusCur%') ORDER BY cCodCur";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "SIN COINDIDENCIAS";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paCurCnv[] = ['CCODCUR' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      return true;
   }

      // BUSCAR ASIGNATURAS CURSADAS PARA CONVALIDAR
   // 2020-05-04 JLF - Creación
   public function omBuscarCursoConvalidacionAdministrativo() {
      $llOk = $this->mxValParamBuscarCursoConvalidacionAdm();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBuscarCursoConvalidacionAdm($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
  
   protected function mxValParamBuscarCursoConvalidacionAdm() {
      if (!isset($this->paData['CALUANT']) || strlen(trim($this->paData['CALUANT'])) != 10) {
         $this->pcError = "CODIGO DE ALUMNO ANTIGUO INVALIDO";
         return false;
      } elseif ($this->paData['CBUSCUR'] != 'N') {
        if (!isset($this->paData['CBUSCUR']) || strlen(trim($this->paData['CBUSCUR'])) != 7) {
           $this->pcError = "CLAVE DE BUSQUEDA INVALIDA";
           return false;
        }
      }
      return true;
   }
  
   protected function mxBuscarCursoConvalidacionAdm($p_oSql) {
      $lcAluAnt = $this->paData['CALUANT'];
      $lcBusCur = mb_strtoupper(str_replace(' ', '%', $this->paData['CBUSCUR']));
      if ($lcBusCur == 'N'){
        $lo = new CWebService();
        $lo->paData = $this->paData;
        $lo->paData['CCODALU'] = $this->paData['CALUANT'];
        $llOk = $lo->omRevisarCursosJurado();
        if (!$llOk) {
           $this->pcError = $lo->pcError;
           return false;
        }
        foreach ($lo->paData['MDATOS'] as $laFila) {
           if (strpos($laFila['CCODCUR'], 'CC')) continue;
           $this->paCurCnv[] = $laFila;
        }
      } else {
           $lcSql = "SELECT cCodCur, cDescri FROM A02MCUR WHERE cUniAca IN (SELECT cUniAca FROM A01MALU WHERE cCodAlu = '$lcAluAnt')
                          AND (cCodCur ='$lcBusCur' OR cDescri LIKE '%$lcBusCur%') ORDER BY cCodCur";
           $R1 = $p_oSql->omExec($lcSql);
           if ($R1 == false || $p_oSql->pnNumRow == 0) {
              $this->pcError = "SIN COINDIDENCIAS";
              return false;
           }
           while ($laFila = $p_oSql->fetch($R1)) {
              $this->paCurCnv[] = ['CCODCUR' => $laFila[0], 'CDESCRI' => $laFila[1]];
           }
      }
      return true;
   }

   // GRABA SOLICITUD DE CONVALIDACION DE CURSOS
   // 2020-05-01 JLF - Creación
   public function omGrabarSolicitudConvalidacion() {
      $llOk = $this->mxValParamGrabarSolicitudConvalidacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitudConvalidacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarSolicitudConvalidacion() {
      if (!isset($this->paData['CTIPCON']) || strlen(trim($this->paData['CTIPCON'])) != 1) {
         $this->pcError = 'TIPO DE CONVALIDACION INVALIDA';
         return false;
      } elseif (in_array($this->paData['CTIPCON'], ['I','D']) && (!isset($this->paData['CALUANT']) || strlen(trim($this->paData['CALUANT'])) != 10)) {
         $this->pcError = 'CODIGO DE ALUMNO ANTIGUO INVALIDO';
         return false;
      } elseif ($this->paData['CTIPCON'] == 'E' && (!isset($this->paData['CESCANT']) || empty(trim($this->paData['CESCANT'])) || strlen(trim($this->paData['CESCANT'])) > 100)) {
         $this->pcError = 'ESCUELA PROFESIONAL ANTERIOR INVALIDA';
         return false;
      } elseif ($this->paData['CTIPCON'] == 'E' && (!isset($this->paData['CUNIANT']) || empty(trim($this->paData['CUNIANT'])) || strlen(trim($this->paData['CUNIANT'])) > 100)) {
         $this->pcError = 'UNIVERSIDAD ANTERIOR INVALIDA';
         return false;
      } elseif (!isset($this->paData['MDATOS']) || count($this->paData['MDATOS']) == 0) {
         $this->pcError = 'CURSOS DE SOLICTUD INVALIDOS';
         return false;
      }
      $this->paData['CESCANT'] = strtoupper($this->paData['CESCANT']);
      $this->paData['CUNIANT'] = strtoupper($this->paData['CUNIANT']);
      $this->paData['MDETALL'] = ($this->paData['CTIPCON'] == 'E')? ['CESCANT' => $this->paData['CESCANT'], 'CUNIANT' => $this->paData['CUNIANT']] : '';
      return true;
   }

   protected function mxGrabarSolicitudConvalidacion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcJson = str_replace("'", "''", $lcJson);
      $lcSql = "SELECT P_B06MCNV_1('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos[0]['ERROR'])) {
         $this->pcError = $this->paDatos[0]['ERROR'];
         return false;
      }
      return true;
   }

   public function omGrabarConvalidacionManual() {
      $llOk = $this->mxValParamGrabarConvalidacionManual();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarConvalidacionManual($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }
  
   protected function mxValParamGrabarConvalidacionManual() {
      if (!isset($this->paData['CTIPCON']) || strlen(trim($this->paData['CTIPCON'])) != 1) {
         $this->pcError = 'TIPO DE CONVALIDACION INVALIDA';
         return false;
      } elseif (!isset($this->paData['CCODALU']) || strlen(trim($this->paData['CCODALU'])) != 10) {
         $this->pcError = 'CODIGO DE ALUMNO INVALIDO';
         return false;
      } elseif (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4 || $this->paData['CUSUCOD'] == '0000') {
        $this->pcError = 'CODIGO DE USUARIO INVALIDO';
        return false;
      } elseif ($this->paData['CTIPCON'] == 'I' && (!isset($this->paData['CALUANT']) || strlen(trim($this->paData['CALUANT'])) != 10)) {
         $this->pcError = 'CODIGO DE ALUMNO ANTIGUO INVALIDO';
         return false;
      } elseif (!isset($this->paData['MDATOS']) || count($this->paData['MDATOS']) == 0) {
         $this->pcError = 'CURSOS DE SOLICTUD INVALIDOS';
         return false;
      }   
      return true;
   }
  
   protected function mxGrabarConvalidacionManual($p_oSql) {
      $lcUsuCod = $this->paData['CUSUCOD'];
      //VALIDA SI ALUMNO TIENE SOLICITUD ACTIVA
      $lcSql = "SELECT cIdConv FROM B06MCNV WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado NOT IN ('X','F')";
      $RS = $p_oSql->omExec($lcSql);
      if ($RS == false) {
         $this->pcError = 'ERROR AL VALIDAR SOLICITUD DEL ALUMNO';
         return false;
      } elseif ($p_oSql->pnNumRow > 0) {
         $this->pcError = 'ALUMNO TIENE SOLICITUDES DE CONVALIDACION ACTIVAS';
         return false;
      }
      $lcSql = "SELECT cNroDni FROM A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      if($laFila[0] == ''){
        $this->pcError = 'NO SE ENCUENTRA DNI DEL ALUMNO';
        return false;
      }
      $this->paData['CNRODNI'] = $laFila[0];
      $this->paData['MDETALL'] = '';
      $lcJson = json_encode($this->paData);
      $lcJson = str_replace("'", "''", $lcJson);
      $lcSql = "SELECT P_B06MCNV_1('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos[0]['ERROR'])) {
         $this->pcError = $this->paDatos[0]['ERROR'];
         return false;
      }
      $lcSql = "SELECT cIdConv FROM B06MCNV WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado = 'A'";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $this->paData['CIDCONV'] = $laFila[0]; 
      $lcSql = "SELECT nSerial, cCodCur FROM B06DCNV WHERE cIdConv = '{$this->paData['CIDCONV']}'";
      $RS = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($RS)) {
        $paDetCon[] = ['NSERIAL' => $laFila[0], 'CCODCUR' => $laFila[1]];
      }
      for ($i=0; $i < count($paDetCon); $i++){
        for ($j=0; $j < count($paDetCon); $j++){
            if ($this->paData['MDATOS'][$i]['CCODCUR'] == $paDetCon[$j]['CCODCUR']){
                $this->paData['NSERIAL'] = $paDetCon[$j]['NSERIAL'];
                $this->paData['CCODDOC'] = $this->paData['MDATOS'][$i]['CCODDOC'];
                $lcJson = json_encode($this->paData);
                $lcJson = str_replace("'", "''", $lcJson);
                $lcSql = "SELECT P_B06DCNV_1('$lcJson')";
                $RS = $p_oSql->omExec($lcSql);
                $laFila = $p_oSql->fetch($RS);
                $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
                $this->paDatos = json_decode($laFila[0], true);
                if (!empty($this->paDatos[0]['ERROR'])) {
                   $this->pcError = $this->paDatos[0]['ERROR'];
                   return false;
                }
            }
        }
      }
      $lcSql = "UPDATE B06DCNV SET cEstado = 'A', tRevisi = NOW(), cUsuCod = '$lcUsuCod', tModifi = NOW() WHERE cIdConv = '{$this->paData['CIDCONV']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL ACTUALIZAR DETALLE DE CONVALIDACION";
         return false;
      }
      //GRABADO DE LA DEUDA
      $lcSql = "SELECT cIdCate, nMonto FROM B03TDOC WHERE cIdCate = 'PDCONV'";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $lcIdCate = $laFila[0];
      $lcNMonto = $laFila[1];
      $lcSql = "SELECT * FROM F_B03MDEU_9('10')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS); 
      $lcNroPag = $laFila[0];
      $lcSql = "SELECT MAX(cIdDeud) FROM B03MDEU";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $lcIdDeud = (empty($laFila[0])) ? '000000' : $laFila[0];
      $i = (int)$lcIdDeud + 1;
      $lcIdDeud = sprintf('%06d', $i);
      $lcNroCur = count($paDetCon);
      $lcMonTot = ($lcNroCur * $lcNMonto) + 6;
      $lcSql = "INSERT INTO B03MDEU (cIdDeud, cNroPag, cNroDni, cEstado, nMonto, cEnvio, cCodUsu) VALUES ('$lcIdDeud', '$lcNroPag', '{$this->paData['CNRODNI']}', 'A', $lcMonTot , 'N', '$lcUsuCod')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL GRABAR MAESTRO DE DEUDAS PARA CONVALIDACIONES";
         return false;
      }
      for ($j=0; $j < count($paDetCon)+1; $j++){
         $lcSql = "SELECT MAX(cIdLog) FROM B03DDEU";
         $RS = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($RS);
         $lcIdLog = (empty($laFila[0])) ? '000000' : $laFila[0];
         $i = (int)$lcIdLog + 1;
         $lcIdLog = sprintf('%06d', $i);
         if ($j == count($paDetCon)){
            $lcSql = "SELECT cIdCate, nMonto FROM B03TDOC WHERE cIdCate = '000109'";
            $RS = $p_oSql->omExec($lcSql);
            $laFila = $p_oSql->fetch($RS);
            $lcIdCate = $laFila[0];
            $lcNMonto = $laFila[1];
            $lcSql = "INSERT INTO B03DDEU (cIdLog, cIdDeud, cIdCate, cCodAlu, nCosto, nCosFor, cNroExp, cCodUsu) VALUES
                      ('$lcIdLog', '$lcIdDeud', '$lcIdCate', '{$this->paData['CCODALU']}', '$lcNMonto', 0, '{$this->paData['CIDCONV']}', '$lcUsuCod')";    
         } else {
            $lcSql = "INSERT INTO B03DDEU (cIdLog, cIdDeud, cIdCate, cCodAlu, nCosto, nCosFor, cNroExp, cCodUsu) VALUES
                      ('$lcIdLog', '$lcIdDeud', '$lcIdCate', '{$this->paData['CCODALU']}', '$lcNMonto', 0, '{$paDetCon[$j]['CCODCUR']}', '$lcUsuCod')";
         }
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL GRABAR DETALLE DE DEUDA PARA CONVALIDACIONES";
            return false;
         }
      }
      $lcSql = "UPDATE B06MCNV SET cEstado = 'C', cIdDeud = '$lcIdDeud', cUsuCod = '$lcUsuCod', tModifi = NOW() WHERE cIdConv = '{$this->paData['CIDCONV']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL ACTUALIZAR PAGO DE TRAMITE DE CONVALIDACION";
         return false;
      }
      //ENVIO DE EMAIL DE CONFIRMACION
      $lcSql = "SELECT cEmail FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0]) || empty($laTmp[0])) {
         $this->pcError = "CORREO DE ALUMNO NO ENCONTRADO";
         return false;
      }
      $lo = new CEmail();
      $llOk = $lo->omConnect();
      $lcMensa = "Ya puedes pagar tu deuda de Convalidacion de Cursos. Su ID de pago es: ".$lcNroPag.". La cantidad a pagar es de S/.".$lcMonTot." . Indicar que es pago por pensiones. El ID de pago estará disponible solo por 7 días.";
      $lo->paData = ['CSUBJEC' => "AVISO: DEUDA PARA CONVALIDACION DE CURSOS", "CBODY" => $lcMensa, "AEMAILS" => [$laTmp[0]]];
      $llOk = $lo->omSend();
      if (!$llOk) {
         $this->pcError = $lo->pcError;
         return false;
      }
      return true;
   }

   // BANDEJA DE SOLICITUDES DE CONVALIDACION DE CURSOS
   // 2020-05-01 JLF - Creación
   public function omInitBandejaSolicitudesConvalidacion() {
      $llOk = $this->mxValParamInitBandejaSolicitudesConvalidacion();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudesConvalidacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudesConvalidacion() {
      if (!isset($this->paData['CCODUSU']) || strlen(trim($this->paData['CCODUSU'])) != 4) {
         $this->pcError = "CODIGO DE USUARIO INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudesConvalidacion($p_oSql) {
      //CARGA ESTADOS DE SOLICITUDES DE CONVALIDACION
      $lcSql = "SELECT TRIM(cCodigo), cDescri FROM V_S01TTAB WHERE cCodTab = '505' AND cCodigo NOT IN ('D','E','X')";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "ERROR AL RECUPERAR ESTADOS DE CONVALIDACIONES";
         return false;
      }
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paEstado[] = ['CCODIGO' => $laTmp[0], 'CDESCRI' => $laTmp[1]];
      }
      //CARGA DEPARTAMENTOS ACADEMICOS
      $lcSql = "SELECT cCenCos, cDescri FROM S01TCCO WHERE cEstado = 'A' AND cTipEst = '06' ORDER BY cEstPre";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "ERROR AL RECUPERAR DEPARTAMENTOS ACADEMICOS";
         return false;
      }
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDepAca[] = ['CCENCOS' => $laTmp[0], 'CDESCRI' => $laTmp[1]];
      }
      return true;
   }

   //BUSCA ALUMNO O DOCENTE PARA CONVALIDACION MANUAL
   public function omBuscarPersonaConvalidacion() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarDatosPersona($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   
   protected function mxCargarDatosPersona($p_oSql) {
      $lcBusPar = mb_strtoupper(str_replace(' ', '%', $this->paData['CBUSPAR']));
      if ($this->paData['CCODPER'] == 'A'){
         $lcSql = "SELECT cCodAlu, cNombre, cNroDni, cNomUni FROM V_A01MALU 
                   WHERE cEstado = 'A' AND cNivel NOT IN ('02','08') AND (cCodAlu = '{$this->paData['CBUSPAR']}' OR cNroDni = '{$this->paData['CBUSPAR']}' OR
                   cNombre LIKE '%$lcBusPar%')";
      } elseif ($this->paData['CCODPER'] == 'D'){
         $lcSql = "SELECT cCodDoc, cNombre, cNroDni FROM V_A01MDOC 
                   WHERE cEstado = 'A' AND cCodDoc = '{$this->paData['CBUSPAR']}' OR cNroDni = '{$this->paData['CBUSPAR']}' OR
                   cNombre LIKE '%$lcBusPar%'";
      } else {
         $this->pcError = "ERROR DE PARAMETROS";
         return false;
      }
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         if ($this->paData['CCODPER'] == 'A') {
           $this->paDatos[] = ['CCODIGO' => $laTmp[0], 'CNOMBRE' => str_replace('/', ' ', $laTmp[1]), 'CNRODNI' => $laTmp[2], 'CNOMUNI' => $laTmp[3]];
         } else {
           $this->paDatos[] = ['CCODIGO' => $laTmp[0], 'CNOMBRE' => str_replace('/', ' ', $laTmp[1]), 'CNRODNI' => $laTmp[2]];  
         }
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "DATOS NO ENCONTRADOS";
         return false;
      }
      return true;
   }

   //BUSCA CODIGOS DE ALUMNO ANTIGUOS
   public function omBuscarCodigosAntinguosAlumnos() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarCodigosAntiguosAlumnos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   
   protected function mxCargarCodigosAntiguosAlumnos($p_oSql) {
      $lcSql = "SELECT cNroDni FROM A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";  
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      if($laFila[0] == ''){
        $this->pcError = 'NO SE ENCUENTRA DNI DEL ALUMNO';
        return false;
      }
      if ($this->paData['CTIPCON'] == 'I') {
          $lcSql = "SELECT cCodAlu, cNomUni FROM V_A01MALU WHERE cNroDni = '$laFila[0]' AND cEstado = 'I' ORDER BY cCodAlu DESC";
          $R1 = $p_oSql->omExec($lcSql);
          if ($R1 == false) {
             $this->pcError = "ERROR AL RECUPERAR TIPO DE CONVALIDACION";
             return false;
          }
          while ($laTmp = $p_oSql->fetch($R1)) {
             $this->paDatos[] = ['CCODALU' => $laTmp[0], 'CNOMUNI' => $laTmp[1]];
          }
      } elseif ($this->paData['CTIPCON'] == 'D') {
          $lcSql = "SELECT cCodAlu, cNomUni FROM V_A01MALU WHERE cNroDni = '$laFila[0]' AND cEstado = 'A' AND cNivel IN 
                  (SELECT cNivel FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}') AND cCodAlu != '{$this->paData['CCODALU']}'
               ORDER BY cCodAlu DESC";
          $R1 = $p_oSql->omExec($lcSql);
          if ($R1 == false) {
            $this->pcError = "ERROR AL RECUPERAR CODIGOS ANTIGUOS";
            return false;
          }
          while ($laTmp = $p_oSql->fetch($R1)) {
            $this->paDatos[] = ['CCODALU' => $laTmp[0], 'CNOMUNI' => $laTmp[1]];
          }
      } else {
        $this->paDatos = [];
      }
      return true;
   }

   // CARGA SOLICITUDES DE CONVALIDACION DE CURSOS
   // 2020-05-11 JLF - Creación
   public function omCargarSolicitudesConvalidacion() {
      $llOk = $this->mxValParamCargarSolicitudesConvalidacion();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarSolicitudesConvalidacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCargarSolicitudesConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = "CODIGO DE USUARIO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CESTADO']) || strlen(trim($this->paData['CESTADO'])) != 1) {
         $this->pcError = "ESTADO DE CONVALIDACION INVALIDO";
         return false;
      } elseif (!isset($this->paData['CNIVEL']) || strlen(trim($this->paData['CNIVEL'])) != 1) {
         $this->pcError = "NIVEL DE USUARIO INVALIDO";
         return false;
      } elseif ($this->paData['CNIVEL'] == 'C' && (!isset($this->paData['CDEPACA']) || strlen(trim($this->paData['CDEPACA'])) != 3)) {
         $this->pcError = "DEPARTAMENTO ACADEMICO INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxCargarSolicitudesConvalidacion($p_oSql) {
      //CARGA SOLICITUDES
      $lcCodUsu = $this->paData['CUSUCOD'];
      $lcEstado = $this->paData['CESTADO'];
      $lcNivel  = $this->paData['CNIVEL'];
      $lcSql = "SELECT DISTINCT ON (A.cIdConv) A.cIdConv, A.cCodAlu, B.cNroDni, B.cNombre, B.cUniAca, B.cNomUni,
                       TO_CHAR(A.tGenera, 'YYYY-MM-DD HH24:MI') AS tGenera, A.cEstado, E.cDescri AS cDesEst, B.cPlaEst, A.cTipo,
                       C.cDescri AS cDesTip, A.cAluAnt, D.cUniAca, D.cNomUni, D.cPlaEst, A.cIdDeud, F.cNroPag, F.cEstado AS cEstDeu,
                       TO_CHAR(F.dFecha, 'YYYY-MM-DD HH24:MI') AS tGenDeu, TO_CHAR(F.dRecepc, 'YYYY-MM-DD HH24:MI') AS tPago, B.cEmail,
                       B.cNroCel, A.mDetall
                FROM B06MCNV A 
                INNER JOIN V_A01MALU B ON B.cCodAlu = A.cCodAlu
                LEFT OUTER JOIN V_S01TTAB C ON C.cCodTab = '506' AND C.cCodigo = A.cTipo
                INNER JOIN V_A01MALU D ON D.cCodAlu = A.cAluAnt
                LEFT OUTER JOIN V_S01TTAB E ON E.cCodTab = '505' AND E.cCodigo = A.cEstado
                INNER JOIN B03MDEU F ON F.cIdDeud = A.cIdDeud
                INNER JOIN B06DCNV G ON G.cIdConv = A.cIdConv
                INNER JOIN A02MCUR H ON H.cCodCur = G.cCodCur
                WHERE G.cEstado NOT IN ('X','R') ";
      if ($lcEstado != 'T') {
         $lcSql .= "AND A.cEstado = '$lcEstado' ";
      } else {
         $lcSql .= "AND A.cEstado NOT IN ('X') ";
      }
      if ($lcNivel == 'C') {
         $lcSql .= "AND H.cDepAca = '{$this->paData['CDEPACA']}' ";
      } else {
         $lcSql .= "AND H.cDepAca IN (SELECT DISTINCT cCenCos FROM B03PUSU WHERE cCodUsu = '$lcCodUsu' AND cEstado = 'A') ";
      }
      $lcSql .= "ORDER BY A.cIdConv";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR SOLICITUDES";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "NO HAY SOLICITUDES CON EL ESTADO SELECCIONADO";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $laDetall = json_decode($laFila[23], true);
         $this->paSolCnv[] = ['CIDCONV' => $laFila[0], 'CCODALU' => $laFila[1], 'CNRODNI' => $laFila[2], 'CNOMBRE' => $laFila[3],
                              'CUNIACA' => $laFila[4], 'CNOMUNI' => $laFila[5], 'TGENERA' => $laFila[6], 'CESTADO' => $laFila[7],
                              'CDESEST' => $laFila[8], 'CPLAEST' => $laFila[9], 'CTIPO'   => $laFila[10],'CDESTIP' => $laFila[11],
                              'CALUANT' => $laFila[12],'CUACANT' => $laFila[13],'CDESANT' => $laFila[14],'CPLAANT' => $laFila[15],
                              'CIDDEUD' => $laFila[16],'CNROPAG' => $laFila[17],'CESTDEU' => $laFila[18],'TGENDEU' => $laFila[19],
                              'TPAGO'   => ($laFila[20] == null)? 'S/D' : $laFila[20], 'CEMAIL' => $laFila[21], 'CNROCEL' => $laFila[22],
                              'CUNIANT' => (isset($laDetall['CUNIANT']))? $laDetall['CUNIANT'] : 'S/D',
                              'CESCANT' => (isset($laDetall['CESCANT']))? $laDetall['CESCANT'] : 'S/D'];
      }
      return true;
   }

   // CARGA DATOS DE LA SOLICITUD
   // 2020-05-02 JLF - Creación
   public function omRevisarSolicitudConvalidacion() {
      $llOk = $this->mxValParamRevisarSolicitudConvalidacion();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitRevisarSolicitudConvalidacion($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarSolicitudConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CIDCONV']) || strlen(trim($this->paData['CIDCONV'])) != 6) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      } elseif (!isset($this->paData['CNIVEL']) || strlen(trim($this->paData['CNIVEL'])) != 1) {
         $this->pcError = "NIVEL DE USUARIO INVALIDO";
         return false;
      } elseif ($this->paData['CNIVEL'] == 'C' && (!isset($this->paData['CDEPACA']) || strlen(trim($this->paData['CDEPACA'])) != 3)) {
         $this->pcError = "DEPARTAMENTO ACADEMICO INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitRevisarSolicitudConvalidacion($p_oSql) {
      $lcIdConv = $this->paData['CIDCONV'];
      $lcCodUsu = $this->paData['CUSUCOD'];
      $lcNivel  = $this->paData['CNIVEL'];
      if ($lcNivel == 'C') {
         $lcTmp = "'{$this->paData['CDEPACA']}'";
      } else {
         $lcTmp = "SELECT DISTINCT cCenCos FROM B03PUSU WHERE cCodUsu = '$lcCodUsu' AND cEstado = 'A'";
      }
      $lcSql = "SELECT A.nSerial, C.cCodCur, C.cDescri, C.nCreTeo, C.nCreLab, A.cSilCur, A.cSilCnv, A.cCurCnv, D.cDescri,
                       D.nCreTeo, D.nCreLab, A.cCodDoc, E.cNombre, C.cDepAca, F.cDescri, TO_CHAR(A.tAsigna, 'YYYY-MM-DD HH24:MI'),
                       TO_CHAR(A.tRevisi, 'YYYY-MM-DD HH24:MI'), A.cEstado, G.cDescri AS cDesEst, A.mAnotac,
                       CASE WHEN C.cDepAca IN ($lcTmp) THEN 'A' ELSE 'X' END AS cFlag
                FROM B06DCNV A 
                INNER JOIN B06MCNV B ON A.cIdConv = B.cIdConv
                INNER JOIN A02MCUR C ON C.cCodCur = A.cCodCur
                INNER JOIN A02MCUR D ON D.cCodCur = A.cCurCnv
                INNER JOIN V_A01MDOC E ON E.cCodDoc = A.cCodDoc
                INNER JOIN S01TCCO F ON F.cCenCos = C.cDepAca
                LEFT OUTER JOIN V_S01TTAB G ON G.cCodTab = '507' AND G.cCodigo = A.cEstado
                WHERE A.cIdConv = '$lcIdConv' ";
      if ($lcNivel == 'C') {
         $lcSql .= "AND A.cEstado != 'X' ";
      } else {
         $lcSql .= "AND CASE WHEN B.cEstado = 'F' THEN A.cEstado NOT IN ('X') ELSE A.cEstado NOT IN ('R','X') END ";
      }
      $lcSql .= "ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR DETALLE DE SOLICITUD";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "SOLICITUD NO TIENE DETALLE";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $laAnotac = json_decode($laFila[19], true);
         $this->paDatos[] = ['NSERIAL' => $laFila[0], 'CCODCUR' => $laFila[1], 'CDESCRI' => $laFila[2], 'NCRETEO' => $laFila[3],
                             'NCRELAB' => $laFila[4], 'CSILCUR' => $laFila[5], 'CSILCNV' => $laFila[6], 'CCURCNV' => $laFila[7],
                             'CDESCNV' => $laFila[8], 'NTEOCNV' => $laFila[9], 'NLABCNV' => $laFila[10],'CCODDOC' => $laFila[11],
                             'CNOMDOC' => $laFila[12],'CDEPACA' => $laFila[13],'CDESDEP' => $laFila[14],'MANOTAC' => ($laAnotac == '')? [] : $laAnotac,
                             'TASIGNA' => ($laFila[15] == null)? 'S/D' : $laFila[15],'TREVISI' => ($laFila[16] == null)? 'S/D' : $laFila[16],
                             'CESTADO' => $laFila[17],'CDESEST' => $laFila[18],'CFLAG'   => $laFila[20],'CASICUR' => (isset($laAnotac['CASICUR']))? $laAnotac['CASICUR'] : 'S/D',
                             'NNOTA'   => (isset($laAnotac['NNOTA']))? $laAnotac['NNOTA'] : '',
                             'CANIO'   => (isset($laAnotac['CANIO']))? $laAnotac['CANIO'] : ''];
      }
      return true;
   }

   // CARGA DATOS DE LA SOLICITUD
   // 2020-05-02 JLF - Creación
   public function omFinalizarSolicitudConvalidacion() {
      $llOk = $this->mxValParamFinalizarSolicitudConvalidacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitFinalizarSolicitudConvalidacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamFinalizarSolicitudConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CIDCONV']) || strlen(trim($this->paData['CIDCONV'])) != 6) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      } elseif (!isset($this->paData['MDATOS']) || count($this->paData['MDATOS']) == 0) {
         $this->pcError = "DETALLE DE SOLICITUD INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitFinalizarSolicitudConvalidacion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcJson = str_replace("'", "''", $lcJson);
      $lcSql = "SELECT P_B06DCNV_6('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos[0]['ERROR'])) {
         $this->pcError = $this->paDatos[0]['ERROR'];
         return false;
      }
      return true;
   }

   // GRABA DOCENTE REVISOR EN CURSO PARA CONVALIDACION
   // 2020-05-04 JLF - Creación
   public function omAsignarDocenteCursoConvalidacion() {
      $llOk = $this->mxValParamAsignarDocenteCursoConvalidacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAsignarDocenteCursoConvalidacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAsignarDocenteCursoConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = 'CODIGO DE USUARIO INVALIDO';
         return false;
      } elseif (!isset($this->paData['CIDCONV']) || strlen(trim($this->paData['CIDCONV'])) != 6) {
         $this->pcError = 'ID DE SOLICITUD INVALIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL']) || $this->paData['NSERIAL'] < 1) {
         $this->pcError = 'DEBE SELECCIONAR UN CURSO PARA ASIGNAR';
         return false;
      } elseif (!isset($this->paData['CCODDOC']) || strlen(trim($this->paData['CCODDOC'])) != 4) {
         $this->pcError = 'CODIGO DE DOCENTE INVALIDO';
         return false;
      }   
      return true;
   }

   protected function mxAsignarDocenteCursoConvalidacion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcJson = str_replace("'", "''", $lcJson);
      $lcSql = "SELECT P_B06DCNV_1('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos[0]['ERROR'])) {
         $this->pcError = $this->paDatos[0]['ERROR'];
         return false;
      }
      return true;
   }

   // ENVIA CURSO A OTRO DEPARTAMENTO ACADEMICO
   // 2020-06-08 JLF - Creación
   public function omEnviarDepartamentoCursoConvalidacion() {
      $llOk = $this->mxValParamEnviarDepartamentoCursoConvalidacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEnviarDepartamentoCursoConvalidacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEnviarDepartamentoCursoConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = 'CODIGO DE USUARIO INVALIDO';
         return false;
      } elseif (!isset($this->paData['CIDCONV']) || strlen(trim($this->paData['CIDCONV'])) != 6) {
         $this->pcError = 'ID DE SOLICITUD INVALIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL']) || $this->paData['NSERIAL'] < 1) {
         $this->pcError = 'DEBE SELECCIONAR UN CURSO PARA ENVIAR';
         return false;
      } elseif (!isset($this->paData['CDEPACA']) || strlen(trim($this->paData['CDEPACA'])) != 3) {
         $this->pcError = 'CODIGO DE DEPARTAMENTO ACADEMICO INVALIDO';
         return false;
      }   
      return true;
   }

   protected function mxEnviarDepartamentoCursoConvalidacion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcJson = str_replace("'", "''", $lcJson);
      $lcSql = "SELECT P_B06DCNV_4('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos[0]['ERROR'])) {
         $this->pcError = $this->paDatos[0]['ERROR'];
         return false;
      }
      return true;
   }

   // BANDEJA DE SOLICITUDES DE CONVALIDACION DE CURSOS
   // 2020-05-05 JLF - Creación
   public function omInitBandejaRevisionCursosConvalidacion() {
      $llOk = $this->mxValParamInitBandejaRevisionCursosConvalidacion();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaRevisionCursosConvalidacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaRevisionCursosConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = "CODIGO DE USUARIO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CNIVEL']) || strlen(trim($this->paData['CNIVEL'])) != 1) {
         $this->pcError = "NIVEL DE USUARIO INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaRevisionCursosConvalidacion($p_oSql) {
      $lcCodUsu = $this->paData['CUSUCOD'];
      $lcNivel  = $this->paData['CNIVEL'];
      $lcSql = "SELECT DISTINCT ON (A.cIdConv) A.cIdConv, A.cCodAlu, B.cNroDni, B.cNombre, B.cUniAca, B.cNomUni, TO_CHAR(A.tGenera, 'YYYY-MM-DD HH24:MI') AS tGenera,
                       A.cEstado, B.cPlaEst, A.cTipo, C.cDescri AS cDesTip, A.cAluAnt, D.cUniAca, D.cNomUni, D.cPlaEst, B.cEmail, B.cNroCel
                FROM B06MCNV A 
                INNER JOIN V_A01MALU B ON B.cCodAlu = A.cCodAlu
                LEFT OUTER JOIN V_S01TTAB C ON C.cCodTab = '506' AND C.cCodigo = A.cTipo
                INNER JOIN V_A01MALU D ON D.cCodAlu = A.cAluAnt
                INNER JOIN B06DCNV E ON E.cIdConv = A.cIdConv
                INNER JOIN A02MCUR F ON F.cCodCur = E.cCodCur
                WHERE A.cEstado IN ('B') AND E.cEstado = 'P' ";
      if ($lcNivel == 'J') {
         $lcSql .= "AND F.cDepAca IN (SELECT DISTINCT cCenCos FROM B03PUSU WHERE cCodUsu = '$lcCodUsu' AND cEstado = 'A') ";
      } else {
         $lcSql .= "AND E.cCodDoc = '$lcCodUsu' ";
      }
      $lcSql .= "ORDER BY A.cIdConv";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR SOLICITUDES";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paSolCnv[] = ['CIDCONV' => $laFila[0], 'CCODALU' => $laFila[1], 'CNRODNI' => $laFila[2], 'CNOMBRE' => $laFila[3],
                              'CUNIACA' => $laFila[4], 'CNOMUNI' => $laFila[5], 'TGENERA' => $laFila[6], 'CESTADO' => $laFila[7],
                              'CPLAEST' => $laFila[8], 'CTIPO'   => $laFila[9], 'CDESTIP' => $laFila[10],'CALUANT' => $laFila[11],
                              'CUACANT' => $laFila[12],'CDESANT' => $laFila[13],'CPLAANT' => $laFila[14],'CEMAIL' => $laFila[15],
                              'CNROCEL' => $laFila[16]];
      }
      return true;
   }

   // CARGA DATOS DE LA SOLICITUD / REVISION DOCENTE/J.DEPARTAMENTO
   // 2020-05-05 JLF - Creación
   public function omRevisarCursosConvalidacion() {
      $llOk = $this->mxValParamRevisarCursosConvalidacion();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitRevisarCursosConvalidacion($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarCursosConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || (strlen($this->paData['CUSUCOD']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CNIVEL']) || strlen(trim($this->paData['CNIVEL'])) != 1) {
         $this->pcError = "NIVEL DE USUARIO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CIDCONV']) || (strlen($this->paData['CIDCONV']) != 6)) {
         $this->pcError = "SOLICITUD NO ESPECIFICADA";
         return false;
      }
      return true;
   }

   protected function mxInitRevisarCursosConvalidacion($p_oSql) {
      $lcIdConv = $this->paData['CIDCONV'];
      $lcNivel  = $this->paData['CNIVEL'];
      $lcCodUsu = $this->paData['CUSUCOD'];
      $lcSql = "SELECT A.nSerial, C.cCodCur, C.cDescri, C.nCreTeo, C.nCreLab, A.cSilCur, A.cSilCnv, A.cCurCnv, D.cDescri,
                       D.nCreTeo, D.nCreLab, A.cCodDoc, E.cNombre
                FROM B06DCNV A 
                INNER JOIN B06MCNV B ON A.cIdConv = B.cIdConv
                INNER JOIN A02MCUR C ON C.cCodCur = A.cCodCur
                INNER JOIN A02MCUR D ON D.cCodCur = A.cCurCnv
                INNER JOIN V_A01MDOC E ON E.cCodDoc = A.cCodDoc
                WHERE A.cIdConv = '$lcIdConv' AND A.cEstado = 'P' ";
      if ($lcNivel == 'J') {
         $lcSql .= "AND C.cDepAca IN (SELECT DISTINCT cCenCos FROM B03PUSU WHERE cCodUsu = '$lcCodUsu' AND cEstado = 'A') ";
      } else {
         $lcSql .= "AND A.cCodDoc = '$lcCodUsu' ";
      }
      $lcSql .= "ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->pcError = "ERROR AL CARGAR DETALLE DE SOLICITUD";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->pcError = "SOLICITUD NO TIENE DETALLE";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['NSERIAL' => $laFila[0], 'CCODCUR' => $laFila[1], 'CDESCRI' => $laFila[2], 'NCRETEO' => $laFila[3],
                             'NCRELAB' => $laFila[4], 'CSILCUR' => $laFila[5], 'CSILCNV' => $laFila[6], 'CCURCNV' => $laFila[7],
                             'CDESCNV' => $laFila[8], 'NTEOCNV' => $laFila[9], 'NLABCNV' => $laFila[10],'CCODDOC' => $laFila[11],
                             'CNOMDOC' => $laFila[12]];
      }
      return true;
   }

   // DENIEGA CURSO PARA CONVALIDACION
   // 2020-05-05 JLF - Creación
   public function omDenegarCursoConvalidacion() {
      $llOk = $this->mxValParamDenegarCursoConvalidacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDenegarCursoConvalidacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDenegarCursoConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = 'CODIGO DE USUARIO INVALIDO';
         return false;
      } elseif (!isset($this->paData['CIDCONV']) || strlen(trim($this->paData['CIDCONV'])) != 6) {
         $this->pcError = 'ID DE SOLICITUD INVALIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL']) || $this->paData['NSERIAL'] < 1) {
         $this->pcError = 'DEBE SELECCIONAR UN CURSO PARA ASIGNAR';
         return false;
      } elseif (!isset($this->paData['MOBSERV']) || strlen(trim($this->paData['MOBSERV'])) < 4) {
         $this->pcError = 'OBSERVACION INVALIDA';
         return false;
      }   
      return true;
   }

   protected function mxDenegarCursoConvalidacion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcJson = str_replace("'", "''", $lcJson);
      $lcSql = "SELECT P_B06DCNV_2('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos[0]['ERROR'])) {
         $this->pcError = $this->paDatos[0]['ERROR'];
         return false;
      }
      return true;
   }

   // APRUBAS CURSOS PARA CONVALIDACION
   // 2020-05-05 JLF - Creación
   public function omAprobarCursosConvalidacion() {
      $llOk = $this->mxValParamAprobarCursosConvalidacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAprobarCursosConvalidacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAprobarCursosConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = 'CODIGO DE USUARIO INVALIDO';
         return false;
      } elseif (!isset($this->paData['CIDCONV']) || strlen(trim($this->paData['CIDCONV'])) != 6) {
         $this->pcError = 'ID DE SOLICITUD INVALIDO';
         return false;
      } elseif (!isset($this->paData['MDATOS']) || count($this->paData['MDATOS']) == 0) {
         $this->pcError = 'SOLICITUD NO TIENE DETALLE PARA APROBAR';
         return false;
      }   
      return true;
   }

   protected function mxAprobarCursosConvalidacion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcJson = str_replace("'", "''", $lcJson);
      $lcSql = "SELECT P_B06DCNV_3('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos[0]['ERROR'])) {
         $this->pcError = $this->paDatos[0]['ERROR'];
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // GRABA INFORMACIÓN DE CONVALIDACION APROBADA PARA REPORTE
   // 2020-06-30 JLF Creacion
   // ------------------------------------------------------------------------------
   public function omGuardarInformacionConvalidacion() {
      $llOk = $this->mxValParamGuardarInformacionConvalidacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGuardarInformacionConvalidacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGuardarInformacionConvalidacion() {
      if (!isset($this->paData['CUSUCOD']) || strlen(trim($this->paData['CUSUCOD'])) != 4) {
         $this->pcError = 'CODIGO DE USUARIO INVALIDO';
         return false;
      } elseif (!isset($this->paData['CIDCONV']) || strlen(trim($this->paData['CIDCONV'])) != 6) {
         $this->pcError = 'ID DE SOLICITUD INVALIDO';
         return false;
      } elseif (!isset($this->paData['MDATOS']) || count($this->paData['MDATOS']) == 0) {
         $this->pcError = 'SOLICITUD NO TIENE DETALLE PARA APROBAR';
         return false;
      }
      return true;
   }

   protected function mxGuardarInformacionConvalidacion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcJson = str_replace("'", "''", $lcJson);
      $lcSql = "SELECT P_B06DCNV_5('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '[{"ERROR":"ERROR EN EJECUCION"}]' : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos[0]['ERROR'])) {
         $this->pcError = $this->paDatos[0]['ERROR'];
         return false;
      }
      return true;
   }
}
?>