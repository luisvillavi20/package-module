<?php
require_once "Clases/CBase.php";
require_once "Clases/CSql.php";

class CWebService{

   public $paData, $pcData, $pcError;
   protected $lcPost;

   public function __construct() {
      $this->paData = null;
   }
   
   protected function mxJsonEncode() {
      $this->lcPost = json_encode($this->paData);
      return true;
   }

   protected function mxValError() {
      if (isset($this->paData['ERROR'])) {
         $this->pcError = $this->paData['ERROR'];
         return false;
      }
      return true;
   }

   public function omFirmarDocumento() {
      $llOk = $this->mxJsonEncode();
      if (!$llOk) {
         return false;
      }
      // Create the context for the request
      $laContext = stream_context_create(array(
         'http' => [
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                        "Content-Length: ".strlen($this->lcPost)."\r\n",
            'method' => 'POST', 
            'content' => $this->lcPost]
      ));
      $lcData = file_get_contents('http://localhost/wsFirmarDocumento', false, $laContext);
      if (!$lcData) {
         $this->pcError = 'NO SE PUEDE ACCEDER AL SERVICIO WEB';
         return false;
      }
      $this->paData = json_decode($lcData, true);
      return $this->mxValError();
   }

   public function omVerificarDocumento() {
      $llOk = $this->mxJsonEncode();
      if (!$llOk) {
         return false;
      }
      // Create the context for the request
      $laContext = stream_context_create(array(
         'http' => [
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                        "Content-Length: ".strlen($this->lcPost)."\r\n",
            'method' => 'POST', 
            'content' => $this->lcPost]
      ));
      $lcData = file_get_contents('http://localhost/wsVerificarDocumento', false, $laContext);
      if (!$lcData) {
         $this->pcError = 'NO SE PUEDE ACCEDER AL SERVICIO WEB';
         return false;
      }
      $this->paData = json_decode($lcData, true);
      return $this->mxValError();
   }

   // REVISA SI ALUMNO TIENE FOTOGRAFIA
   // 2019-03-19 LVA Creación
   public function omRevisarFotoAlumno() {
      $llOk = $this->mxJsonEncode();
      if (!$llOk) {
         return false;
      }
      // Create the context for the request
      $laContext = stream_context_create(array(
         'http' => [
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                        "Content-Length: ".strlen($this->lcPost)."\r\n",
            'method' => 'POST', 
            'content' => $this->lcPost]
      ));
      $lcData = file_get_contents('http://localhost/wsRevisarFotoAlumno', false, $laContext);
      if (!$lcData) {
         $this->pcError = 'NO SE PUEDE VALIDAR FOTOGRAFIA';
         return false;
      }
      $this->paData = json_decode($lcData, true);
      return $this->mxValError();
   }

   // REVISA CURSOS DE UN ALUMNO PARA JURADO
   // 2019-06-21 LVA Creación
   public function omRevisarCursosJurado() {
      $llOk = $this->mxJsonEncode();
      if (!$llOk) {
         return false;
      }
      // Create the context for the request
      $laContext = stream_context_create(array(
         'http' => [
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                        "Content-Length: ".strlen($this->lcPost)."\r\n",
            'method' => 'POST', 
            'content' => $this->lcPost]
      ));
      $lcData = '{"OK":"OK", "MDATOS":[{"CCODCUR": "4101007", "NSEMCUR": 8, "CTIPCUR": "E", "NPROTEO": 0.0, "CDESCRI": "COMUNICACION ORAL Y ESCRITA", "NESTADO": 0, "NVEZMAT": 0, "NCREDIT": 3.0, "CPLNEST": "44200904", "NPROPRA": 0.0}, {"CCODCUR": "4101013", "NSEMCUR": 8, "CTIPCUR": "E", "NPROTEO": 0.0, "CDESCRI": "FILOSOFIA", "NVEZMAT": 0, "NCREDIT": 1.0, "CPLNEST": "44200904", "NPROPRA": 0.0}]}';
      //$lcData = file_get_contents('http://localhost/wsRevisarCursosJurado', false, $laContext);
      if (!$lcData) {
         $this->pcError = 'NO SE PUEDE CARGAR CURSOS PARA JURADO';
         return false;
      }
      $this->paData = json_decode($lcData, true);
      if ($this->paData == null) {
         $this->pcError = "VALOR DEVUELTO INVALIDO";
         return false;
      }
      return $this->mxValError();
   }

   // REGISTRA DEUDA PROVISIONAL - CONCURSOS DE ADMISION
   // 2020-01-20 LVA Creación
   public function omRegistrarDeudaConcursoAdmision() {
      $llOk = $this->mxJsonEncode();
      if (!$llOk) {
         return false;
      }
      // Create the context for the request
      $laContext = stream_context_create(array(
         'http' => [
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                        "Content-Length: ".strlen($this->lcPost)."\r\n",
            'method' => 'POST', 
            'content' => $this->lcPost]
      ));
      $lcData = file_get_contents('http://localhost/wsGenerarDeudaConcursoAdmision', false, $laContext);
      if (!$lcData) {
         $this->pcError = 'NO SE PUEDE REGISTRAR DEUDA PARA CONCURSO DE ADMISION';
         return false;
      }
      $this->paData = json_decode($lcData, true);
      if ($this->paData == null) {
         $this->pcError = "VALOR DEVUELTO INVALIDO";
         return false;
      }
      return $this->mxValError();
   }

   // RETORNA PATH DE FOTOGRAFIA CARGADA DE ALUMNO
   // 2020-04-28 LVA Creación
   public function omCargarFotografiaAlumno() {
      $llOk = $this->mxJsonEncode();
      if (!$llOk) {
         return false;
      }
      // Create the context for the request
      $laContext = stream_context_create(array(
         'http' => [
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                        "Content-Length: ".strlen($this->lcPost)."\r\n",
            'method' => 'POST', 
            'content' => $this->lcPost]
      ));
      //$lcData = '{"OK":"OK", "CPATH":"./Images/user.png"}';
      $lcData = file_get_contents('http://localhost/wsCargarFotografiaAlumno', false, $laContext);
      if (!$lcData) {
         $this->pcError = 'NO SE PUEDE CARGAR RUTA DE FOTOGRAFIA'; 
         return false;
      }
      $this->paData = json_decode($lcData, true);
      if ($this->paData == null) {
         $this->pcError = "VALOR DEVUELTO INVALIDO";
         return false;
      }
      return $this->mxValError();
   }

   // REGISTRA TRAMITE EN BANDEJA DIGITAL
   // 2020-05-26 LVA Creación
   public function omRegistrarTramiteBandejaDigital() {
      $llOk = $this->mxJsonEncode();
      if (!$llOk) {
         return false;
      }
      // Create the context for the request
      $laContext = stream_context_create(array(
         'http' => [
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                        "Content-Length: ".strlen($this->lcPost)."\r\n",
            'method' => 'POST', 
            'content' => $this->lcPost]
      ));
      $lcData = file_get_contents('http://localhost/wsRegistrarTramiteBandejaDigital', false, $laContext);
      if (!$lcData) {
         $this->pcError = 'NO SE PUEDE REGISTRAR TRAMITE EN BANDEJA DIGITAL';
         return false;
      }
      $this->paData = json_decode($lcData, true);
      if ($this->paData == null) {
         $this->pcError = "VALOR DEVUELTO INVALIDO";
         return false;
      }
      return $this->mxValError();
   }
}
?>

