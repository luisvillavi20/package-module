<?php
require_once "Clases/CBase.php";
require_once "Clases/CSql.php";

class CLogin extends CBase {
   public $paData, $paDatos, $paUniAca;

   public function __construct() {
      parent::__construct();
      $this->paData = $this->paDatos = $this->paUniAca = null;
   }

   // Iniciar sesion Generico
   public function omIniciarSesion() {      
      $llOk = $this->mxValIniciarSesion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();      
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxIniciarSesion($loSql);
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }      
      $loSql->omDisconnect();
      return $llOk;
   }
   
   protected function mxIniciarSesion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_LOGIN_A2('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '{"ERROR": "ERROR DE EJECUCION DE BASE DE DATOS"}' : $laFila[0];
      $this->paData = json_decode($laFila[0], true);
      if (!empty($this->paData['ERROR'])) {
         $this->pcError = $this->paData['ERROR'];
         return false; 
      }
      return true;
   }
   
   protected function mxValIniciarSesion () {
      if (empty($this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO";
         return false;
      } elseif (empty($this->paData['CCLAVE'])) {
         $this->pcError = "CONTRASEÑA NO DEFINIDA";
         return false;
      }
      return true;
   }

   // Iniciar sesion Trabajadores
   public function omIniciarSesion2() {      
      $llOk = $this->mxValIniciarSesion2();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();      
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxIniciarSesion2($loSql);
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }      
      $loSql->omDisconnect();
      return $llOk;
   }
   
   protected function mxIniciarSesion2($p_oSql) {
      $lcJson = json_encode($this->paData); 
      $lcSql = "SELECT P_LOGIN_T('$lcJson')";     
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '{"ERROR": "ERROR DE EJECUCION DE BASE DE DATOS"}' : $laFila[0];
      $this->paData = json_decode($laFila[0], true);    
      if (!empty($this->paData['ERROR'])) {
         $this->pcError = $this->paData['ERROR'];
         return false; 
      }
      return true;
   }
   
   protected function mxValIniciarSesion2 () {
      if (empty($this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO";
         return false;
      } elseif (empty($this->paData['CCLAVE'])) {
         $this->pcError = "CONTRASEÑA NO DEFINIDA";
         return false;
      }
      return true;
   }
   
   
   protected function mxGetUniAcaUsuario($p_oSql) {
      $lcNroDni = $this->paData['CNRODNI'];      
      $lcSql = "SELECT US.cNivel, DU.cUniAca, UA.cnomuni 
                FROM S01TUSU US 
                INNER JOIN S01MPER MP ON MP.cNroDni = US.cNroDni 
                INNER JOIN B03DUSU DU ON US.cCodUsu = DU.cCodUsu 
                INNER JOIN B03TDOC TD ON TD.cIdCate = DU.cIdCate 
                INNER JOIN S01TUAC UA ON UA.cUniAca = DU.cUniAca
                WHERE US.cNroDni = '$lcNroDni'
                GROUP BY US.cNivel, DU.cUniAca, UA.cNomUni";
      $R1 = $p_oSql->omExec($lcSql);
      $i = 0;
      while ($laFila = $p_oSql->fetch($R1)) { 
         $this->paUniAca[] = ['CNIVEL' => $laFila[0], 'CUNIACA' => $laFila[1], 'CNOMUNI' => $laFila[2]];
         $i++;
      }  
      if ($i == 0) {
         $this->pcError = "NO HAY UNIDADES ACADEMICAS DISPONIBLES";
         return false;
      }  
      return true;
   }
   
   // Iniciar sesion Administrador - Validar IP
   public function omIniciarSesionIP() {
      if (empty($this->paData['CTERMIP'])) {
         $this->pcError = "IP DE CONEXIÓN NO DEFINIDA";
         return false;
      }
      $llOk = $this->mxValInicioSesion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxIniciarSesion($loSql);
      if (!$llOk) {
         return false;
      }
      $llOk = $this->mxIniciarSesionIP($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   
   protected function mxIniciarSesionIP($p_oSql){
      if ($this->paDatos['CCODUSU'] === '*') {
         $this->pcError = 'USUARIO NO EXISTE';
         return false; 
      }
      $lcTermIp = $this->paData['CTERMIP'];
      
      //*** BORRAR(TEST) *******
      $lcTermIp = '10.0.130.15';
      return true;
      //************************
      
      $lcUniAca = $this->paDatos['CUNIACA'];
      $lcSql = "SELECT TRIM(cTermIp) FROM S01TTER WHERE CCODOFI IN ('00','$lcUniAca') ORDER BY cTermId";
      $R1 = $p_oSql->omExec($lcSql);
      if (empty($R1)) {
         $this->pcError = "ERROR AL EJECUTAR COMANDO BASE DE DATOS";
         return false;
      }
      $i = 0;
      while ($laFila = $p_oSql->fetch($R1)) {
         if (strpos($lcTermIp, $laFila[0]) !== false) {
            return true;
         }
         $i = $i + 1;
      }
      if ($i == 0) {
         $this->pcError = "NO EXISTE UNA LISTA DE CONEXIÓN PREDEFINIDA";
         return false;
      }
      $this->pcError = "LA IP NO PERTENECE A LA RED PERMITIDA";
      return false;
   }

   public function omIniciarSesionPendientesTramites() {
      $llOk = $this->mxValInicioSesionPendientesTramites();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxIniciarSesionPendientesTramites($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   
   protected function mxValInicioSesionPendientesTramites() {
      if (empty($this->paDataTra['CNRODNI'])) {
         $this->pcError = "INGRESAR NUMERO DE DNI VALIDO";
         return false;
      } elseif (empty($this->paDataTra['CCLAVE'])) {
         $this->pcError = "CONTRASEÑA NO DEFINIDA";
         return false;
      }
      return true;
   }
   
   protected function mxIniciarSesionPendientesTramites($p_oSql) {
      $lcJson = json_encode($this->paDataTra);
      $lcSql = "SELECT P_LOGIN_3('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '{"ERROR": "ERROR DE EJECUCION DE BASE DE DATOS"}' : $laFila[0];
      $this->paData = json_decode($laFila[0], true);
      if (!empty($this->paData['ERROR'])) {
         $this->pcError = $this->paData['ERROR'];
         return false; 
      }
      return true;
   }

   public function omIniciarSesionPendientesErp() {
      $llOk = $this->mxValInicioSesionPendientesErp();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect(1);
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxIniciarSesionPendientesErp($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   
   protected function mxValInicioSesionPendientesErp() {
      if (empty($this->paDataErp['CNRODNI'])) {
         $this->pcError = "INGRESAR NUMERO DE DNI VALIDO";
         return false;
      } elseif (empty($this->paDataErp['CCLAVE'])) {
         $this->pcError = "CONTRASEÑA NO DEFINIDA";
         return false;
      }
      return true;
   }
   
   protected function mxIniciarSesionPendientesErp($p_oSql) {
      $lcJson = json_encode($this->paDataErp);
      $lcSql = "SELECT P_LOGIN_3('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = (!$laFila[0]) ? '{"ERROR": "ERROR DE EJECUCION DE BASE DE DATOS"}' : $laFila[0];
      $this->paData = json_decode($laFila[0], true);
      if (!empty($this->paData['ERROR'])) {
         $this->pcError = $this->paData['ERROR'];
         return false; 
      }
      return true;
   }

}
?>

