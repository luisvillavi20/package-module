 <?php
// Conexion a Base de Datos
class CSql {
   public $pcError, $pnNumRow;
   protected $h;

   public function __construct() {
      $this->pcError = null;
      $this->pnNumRow = 0;
   }

   public function omDisconnect() {
      $this->omExec("COMMIT;");
      pg_close($this->h);
   }

   public function omConnect($lnFlag = 0) {
      if ($lnFlag == 1) {
         $lcConStr = "host=localhost dbname=BD1 port=port user=postgres password=bdprueba";
      } else if ($lnFlag == 2) {
         $lcConStr = "host=localhost dbname=BD2 port=port user=postgres password=bdprueba";
         
      } else if ($lnFlag == 3) {
         $lcConStr = "host=localhost dbname=BD3 port=port user=postgres password=bdprueba";
      } else if ($lnFlag == 4) {
         $lcConStr = "host=localhost dbname=BD4 port=port user=postgres password=bdprueba";
      } else {
         $lcConStr = "host=localhost dbname=BD5 port=port user=postgres password=bdprueba";
      }                
      
      @$this->h = pg_connect($lcConStr);
      if (!$this->h) {
         $this->pcError = "NO SE PUDO CONECTAR AL SERVIDOR";
         return false;
      }
      $this->omExec("BEGIN;");     
      return true;
   }
   
   public function omConnectList() {            
      
      $lcConStr = "host=1localhost dbname=DB user=postgres password=bdprueba port=port";     
      @$this->h = pg_connect($lcConStr)  or die("Can't connect to database".pg_last_error());;             
      if (!$this->h) {
         $this->pcError = "No se pudo conectar a la base de datos";
         return false;
      }      
      $this->omExec("BEGIN;");     
      return true;
   } 

   public function omExec($p_cSql) {
      $lcSql = substr(strtoupper(trim($p_cSql)), 0, 6);
      if ($lcSql === "SELECT") {
         $this->pnNumRow = 0;
         $RS = pg_query($this->h, $p_cSql);
         if (!($RS)) {
            $this->pcError = "Error al ejecutar comando SQL";
            return false;
         }
         $this->pnNumRow = pg_num_rows($RS);
         return $RS;
      } else {
         @$RS = pg_query($this->h, $p_cSql);
         if (pg_affected_rows($RS) == 0)
            if (!($RS)) {
               $this->pcError = "La operacion no afecto a ninguna fila";
               return false;
            }
         return true;
      }
   }

   public function fetch($RS) {
      return pg_fetch_row($RS);     
   }
   
   public function rollback() {
      $this->omExec("ROLLBACK;");
   }
}
?>
