<?php
   require_once "class/PHPExcel.php";
   require_once 'class/RTFTable.php';

//------------------------------------------------------
// Clase Base
//------------------------------------------------------
class CBase {
   public $pcError;

   function __construct() {
      $this->pcError = null;
   }
}

//------------------------------------------------------
// Clase para fechas
//------------------------------------------------------
class CDate extends CBase {
   public $date;
   public $days;

   public function valDate($p_dFecha) {
      $laFecha = explode('-', $p_dFecha);
      $llOk = checkdate((int)$laFecha[1], (int)$laFecha[2], (int)$laFecha[0]); 
      if (!$llOk) {
         $this->pcError = 'FORMATO DE FECHA INVALIDA';
      }
      return $llOk;
   }

   public function add($p_dFecha, $p_nDias) {
      $llOk = $this->valDate($p_dFecha);
      if (!$llOk) {
         return false;
      }
      if (!is_int($p_nDias)) {
         $this->pcError = 'PARAMETRO DE DIAS ES INVALIDO';
         return false;
      } elseif ($p_nDias >= 0) {
         $lcDias = ' + '.$p_nDias.' days';
      } else {
         $p_nDias = $p_nDias * (-1);
         $lcDias = ' - '.$p_nDias.' days';
      }
      $this->date = date('Y-m-d', strtotime($p_dFecha.$lcDias));
      return true;
   }
   
   public function diff($p_dFecha1, $p_dFecha2) {
      $llOk = $this->valDate($p_dFecha1);
      if (!$llOk) {
         return false;
      }
      $llOk = $this->valDate($p_dFecha2);
      if (!$llOk) {
         return false;
      }
      $this->days = (strtotime($p_dFecha1) - strtotime($p_dFecha2)) / 86400;
      $this->days = floor($this->days);
     return true;
   }
   
   public function dateSimpleText($p_dDate) {
      $llOk = $this->valDate($p_dDate);
      if (!$llOk) {
         return 'Error: '.$p_dDate;
      }
      $laMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
      $laDate = explode('-', $p_dDate);
      $ldDate = mktime(0, 0, 0, $laDate[1], $laDate[2], $laDate[0]);
      return date('d', $ldDate).' de '.$laMonths[date('m', $ldDate) - 1].' del '.date('Y', $ldDate);
   }

   public function dateSimpleText2($p_dDate) {
      $llOk = $this->valDate($p_dDate);
      if (!$llOk) {
         return 'Error: '.$p_dDate;
      }
      $laMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
      $laDate = explode('-', $p_dDate);
      $ldDate = mktime(0, 0, 0, $laDate[1], $laDate[2], $laDate[0]);
      return date('d', $ldDate).' de '.$laMonths[date('m', $ldDate) - 1];
   }

   public function dateText($p_dDate) {
      $llOk = $this->valDate($p_dDate);
      if (!$llOk) {
         return 'Error: '.$p_dDate;
      }
      $laDays = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
      $laMonths = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
      $laDate = explode('-', $p_dDate);
      $ldDate = mktime(0, 0, 0, $laDate[1], $laDate[2], $laDate[0]);
      return $laDays[date('w', $ldDate)].', '.date('d', $ldDate).' '.$laMonths[date('m', $ldDate) - 1].' de '.date('Y', $ldDate);
   }

   public function mxvalDate($p_dFecha) {
      $laFecha = explode('-', $p_dFecha);
      $llOk = checkdate((int)$laFecha[1], (int)$laFecha[2], (int)$laFecha[0]); 
      if (!$llOk) {
         $this->pcError = 'FORMATO DE FECHA INVALIDA';
      }
      return $llOk;
   }

}

class CXls extends CBase {
   public $pcData = "", $pcFile, $pcFilXls;
   protected $loXls, $lo, $lcFilXls;

   public function __construct() {
      parent::__construct();
      $this->loXls = new PHPExcel();
      $this->lo = PHPExcel_IOFactory::createReader('Excel2007');      
   }
   
   public function openXls($p_cFilXls) {
      $this->loXls = $this->lo->load('./Xls/'.$p_cFilXls.'.xlsx');      
      $this->lcFilXls = './Files/R'.rand().'.xlsx';      
      $this->pcFilXls = $this->lcFilXls;
   }
   
   public function sendXls($p_nSheet, $p_cCol, $p_nRow, $p_xValue) {      
      $this->loXls->setActiveSheetIndex($p_nSheet)->setCellValue($p_cCol.$p_nRow, $p_xValue);            
      return;
   }
   
   public function closeXls() {    
      $lo = PHPExcel_IOFactory::createWriter($this->loXls, 'Excel2007');                        
      $lo->save($this->lcFilXls);           
   }
   
   public function cellColor($cells, $color) {
      $this->loXls->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => $color)));
   }
   
   public function cellColor1($Sheet, $cells, $color) {
      $this->loXls->getActiveSheet($Sheet)->getStyle($cells)->getFill()->applyFromArray(array(
         'type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => $color)));
   }
   
   public function setActiveSheet($p_nSheet) {
      $this->loXls->setActiveSheetIndex($p_nSheet);
   }
   
   public function getValue($p_nSheet, $p_cCol, $p_nRow) {
      $lcCell = $p_cCol.$p_nRow;
      //$lxValue = $this->loXls->getActiveSheet($p_nSheet)->getCell($lcCell)->getValue();
      $lxValue = $this->loXls->getActiveSheet(1)->getCell($lcCell)->getValue();
      return $lxValue;
   }
   
   public function openXlsIO($p_cFilXls, $p_cPrefij) {
      $this->loXls = $this->lo->load('./Xls/'.$p_cFilXls.'.xlsx');
      $lcFile = $p_cPrefij.rand();
      $this->pcFile = './FILES/'.$lcFile.'.xlsx';
   }
   
   public function closeXlsIO() {
      $lo = PHPExcel_IOFactory::createWriter($this->loXls, 'Excel2007');                        
      $lo->save($this->pcFile);
   }
   
   public function getColor() {
      $lxValue = $this->loXls->getActiveSheet()->getStyle('D2')->getFill()->getStartColor()->getRGB();
      return $lxValue;
   }

   public function mergeCells($p_cDesde, $p_cHasta) {
      $this->loXls->getActiveSheet()->mergeCells($p_cDesde.':'.$p_cHasta);
   }

   public function cellStyle($p_cDesde, $p_cHasta, $p_llBold, $p_llItalic, $p_nSize, $p_cAliHor, $p_cAliVer, $p_cFuente = 'Arial') {
      /* 
      ALINEACION HORIZONTAL 'general','left','right','center','centerContinuous','justify','fill','distributed'; 
      ALINEACION VERTICAL 'bottom','top','center','justify','distributed'; 
      */
      $this->loXls->getActiveSheet()->getStyle($p_cDesde.':'.$p_cHasta)->applyFromArray(array(
         'font' => array(
            'bold' => $p_llBold,
            'italic' => $p_llItalic,
            'name' => $p_cFuente,
            'size' => $p_nSize,
         ),
         'alignment'  => array(
            'horizontal' => $p_cAliHor,
            'vertical' => $p_cAliVer,
         ),
      ));
   }
}

function fxAlert($p_Message) {
   echo "<script type=\"text/javascript\">";
   echo "alert('$p_Message')";
   echo "</script>";  
}

function fxHeader($p_cLocation, $p_cMensaje = '') {
   if (empty($p_cMensaje)) {
      $lcScript = "window.location='$p_cLocation';";
   } else {
      $lcScript = "alert('$p_cMensaje');window.location='$p_cLocation';";
      //$lcScript = "window.location='$p_cLocation';alert('$p_cMensaje');";
   }
   echo '<script>'.$lcScript.'</script>';
}

function right($lcCadena, $count) {
   return substr($lcCadena, ($count * -1));
}

function left($lcCadena, $count) {
   return substr($lcCadena, 0, $count);
}

function fxNumber($p_nNumero, $p_nLength, $p_nDecimal) {
   $lcNumero = number_format($p_nNumero, $p_nDecimal);
   $lcCadena = str_repeat(' ', $p_nLength).$lcNumero;
   return right($lcCadena, $p_nLength);
}
        
function fxString($p_cCadena, $p_nLength) {
   #$i = substr_count($p_cCadena, 'Ñ');
   $lcCadena = $p_cCadena.str_repeat(' ', $p_nLength);
   #$lcCadena = substr($lcCadena, 0, $p_nLength + $i);
   $lcCadena = substr($lcCadena, 0, $p_nLength);
   return $lcCadena;
}

function fxStringTail($p_cString, $p_nIndex) {
   $lcString = $p_cString;
   $lcString = substr($lcString, $p_nIndex);
   return $lcString;
}

function fxSoloAlumnos($p_cFlag = null) {
   if (isset($p_cFlag) and $p_cFlag != '') {
      fxHeader('index.php?id='.$p_cFlag);
      return false;
   }
   if (!(isset($_SESSION["GADATA"]))) {
      fxHeader('index.php', "NO REGISTRA INICIO DE SESION");   
      return false;
   }
   if (!(isset($_SESSION["GADATA"]["CCODALU"])) || $_SESSION["GADATA"]['CCODALU'] == '*') {
      fxHeader('index.php', 'NO ES ALUMNO');
      return false;
   }
   return true;
}

function fxSoloAdministrativo() {
   if (!(isset($_SESSION["GADATA"]))) {
      if (isset($_GET['dir'])) {
         $_SESSION["URL"] = $_GET['dir'];
      }
      fxHeader('index1.php', "NO REGISTRA INICIO DE SESION");
      return false;
   }
   elseif (!(isset($_SESSION["GADATA"]["CNIVEL"])) || $_SESSION["GADATA"]['CNIVEL'] == '*') {      
      fxHeader('index1.php','NO ES ADMINISTRATIVO');    
      return false;
   }   
   return true;
}

function fxStringFixed($p_cString, $p_nLenght) {
   $lcString = fxString($p_cString, $p_nLenght);
   $i= fxSubstrCount($lcString); 
   $lcString = fxString($lcString, $p_nLenght + $i);
   return $lcString;
}

function fxStringCenter($p_cString, $p_nLenght) {
   $lcString = str_pad($p_cString, $p_nLenght, ' ', STR_PAD_BOTH);
   $lcString = fxStringFixed($lcString, $p_nLenght);
   return $lcString;
}

function fxSubstrCount($p_cString) {
   $i = substr_count($p_cString, 'Á');
   $i += substr_count($p_cString, 'É');
   $i += substr_count($p_cString, 'Í');
   $i += substr_count($p_cString, 'Ó');
   $i += substr_count($p_cString, 'Ú');
   $i += substr_count($p_cString, 'Ñ');
   $i += substr_count($p_cString, 'ñ');
   $i += substr_count($p_cString, 'º');
   $i += substr_count($p_cString, '§');
   $i += substr_count($p_cString, 'ø');
   $i += substr_count($p_cString, 'á');
   $i += substr_count($p_cString, 'é');
   $i += substr_count($p_cString, 'í');
   $i += substr_count($p_cString, 'ó');
   $i += substr_count($p_cString, 'ú');
   return $i;
}

function fxDocument($FilRet){
   $lcTxt = "<script type=text/javascript>";
   $lcTxt .= "window.open('$FilRet', '', 'toolbar=yes, scrollbars=yes, resizable=yes, width=600, height=600');";
   $lcTxt .= "</script>";
   echo $lcTxt;
}

function fxDocumento($FilRet){
      $lcTxt = "<script type=text/javascript>";
      $lcTxt .= "window.open('$FilRet','_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, width=950, height=650');";
      $lcTxt .= "</script>";
      echo $lcTxt;
}

function fxSubirCSV($p_oFile,$p_cFolder,$p_cNombre){
   $tmp_name = $p_oFile["tmp_name"];
   $lcType = pathinfo($p_oFile['name'], PATHINFO_EXTENSION);
   if ($lcType != 'csv') {
      return false;
   }
   return move_uploaded_file($tmp_name, $p_cFolder.'/'.$p_cNombre.'.csv');
}

?>
