<?php
require_once "Clases/CBase.php";
require_once "Clases/CSql.php";
require_once 'Clases/CEmail.php';
require_once ('PDF/CODE128.php');
require_once "class/PHPExcel.php";
require_once 'Styles/phpqrcode/qrlib.php';

// Gestion de paquetes por usuario - centro de costo
class CPaquetes extends CBase {

   public $paData, $paDatos, $paEstado, $pcFile, $paTipCol, $paEscuel, $paEspMec, $paHisCol;
   protected $laData;

   public function __construct() {
      parent::__construct();
      $this->paData = $this->laData = $this->paDatos = $this->paEstado = $this->paTipCol = $this->paEscuel = $this->paEspMec = null;
      $this->pcFile = 'FILES/R' . rand() . '.pdf';
   }

   protected function mxVerUsuario($p_oSql) {
      // Verifica usuario
      $lcSql = "SELECT cNombre, cEstTus FROM V_S01TUSU WHERE cCodUsu = '{$this->paData['CCODUSU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!$laTmp[0]) {
         $this->pcError = "USUARIO [{$this->paData['CCODUSU']}] NO EXISTE";
         return false;
      } elseif ($laTmp[1] != 'A') {
         $this->pcError = "USUARIO [{$this->paData['CCODUSU']}] NO ESTA ACTIVO";
         return false;
      }
      $this->laData = ['CCODUSU' => $this->paData['CCODUSU'], 'CNOMBRE' => $laTmp[0]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init muestra los paquetes que puede aplicar el interesado
   // 2018-12-27 FPM Creacion
   // ------------------------------------------------------------------------------
   public function omInitPaquetes() {
      $llOk = $this->mxValParamInitPaquetes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitPaquetes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitPaquetes() {
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODALU']) or (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'PARAMETRO CODIGO DE ALUMNO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODIGO']) or (strlen($this->paData['CCODIGO']) != 1)) {
         $this->pcError = 'PARAMETRO CODIGO DE TRAMITE NO ESPECIFICADO';
         return false;
      }
      return true;
   }

   protected function mxInitPaquetes($p_oSql) {
      if($this->paData['CCODIGO'] == 'T'){
         $lcSql = "SELECT A.cNroDni FROM B03MDEU A
                  INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                  INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog AND C.cIdCate = 'CCESTU'
                  WHERE A.cNroDni = '{$this->paData['CNRODNI']}' AND B.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado = 'C' AND A.cPaquet = 'B' AND C.cEtapa IN ('A','B','C','D')";                
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         $p_cNroDni = $laFila[0];
         if (isset($laFila[0])) {
            if (!file_exists("EXP/D$p_cNroDni/P000002.pdf")) {
               $this->pcError = 'COMUNIQUESE CON LA OFICINA DEL ERP, PARA REGULARIZAR SUS DOCUMENTOS';
               return false;
            }
         }
         //QUITAR LO DE ARRIBA CUANDO SE HABILITE LOS FEDATEOS AUTOMATICOS
         $lcSql = "SELECT A.cNroDni FROM B03MDEU A
                   INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog AND C.cIdCate = 'CCESTU'
                   WHERE A.cNroDni = '{$this->paData['CNRODNI']}' AND B.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado = 'C' AND A.cPaquet = 'B' AND C.cEtapa IN ('A','B','C')";                
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (isset($laFila[0])) {// bachiller Online en tramite
               $this->pcError = 'TRAMITE DE BACHILLER EN LINEA EN PROCESO';
               return false;
         }
         $lcSql = "SELECT A.cNroDni FROM B03MDEU A
                   INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog AND C.cIdCate = 'CCESTU'
                   WHERE A.cNroDni = '{$this->paData['CNRODNI']}' AND B.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado = 'C' AND A.cPaquet = 'B' AND C.cEtapa = 'D'";                
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) {//sin bachiller Online
            $lcSql = "SELECT cCodAlu FROM B04DFED WHERE cCodAlu = '{$this->paData['CCODALU']}'";
            $R1 = $p_oSql->omExec($lcSql);
            while ($laTmp = $p_oSql->fetch($R1)){
               $laDatos[] = ['CTIPO' => $laFila[1]];
            }
            if (count($laDatos) < 2){
               $this->pcError = 'EL ALUMNO NO REALIZO BACHILLER EN LINEA, USTED DEBE SUBIR AMBOS DOCUMENTO FEDATEADOS(DIPLOMA, CERTIFICADO DE ESTUDIOS)';
               return false;
            }
         }
      }
      $lcSql = "SELECT cEstado, cNombre, cUniAca, cNomUni FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = 'CODIGO DE ALUMNO NO EXISTE';
         return false;
      } elseif ($laTmp[0] != 'A') {
         $this->pcError = 'CODIGO DE ALUMNO NO ESTA ACTIVO';
         return false;
      }
      $laData = ['CCODALU' => $this->paData['CCODALU'], 'CNOMBRE' => $laTmp[1], 'CUNIACA' => $laTmp[2], 'CNOMUNI' => $laTmp[3]];
      if ($this->paData['CCODIGO'] == 'M'){
        $lcSql = "SELECT cIdPaqu, cDescri FROM B03MPAQ WHERE cTipo = 'M' ORDER BY cIdPaqu";
      } elseif ($this->paData['CCODIGO'] == 'D') {
        $lcSql = "SELECT cIdPaqu, cDescri FROM B03MPAQ WHERE cTipo = 'D' ORDER BY cIdPaqu";
      } else {
        $lcSql = "SELECT cIdPaqu, cDescri
                  FROM B03MPAQ A
                  INNER JOIN A01MALU B ON B.cUniAca = A.cUniAca
                  --WHERE B.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado = 'A'
                  WHERE B.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado = 'A' AND A.cTipo = '{$this->paData['CCODIGO']}'
                  ORDER BY cIdPaqu";
      }
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDPAQU' => $laTmp[0], 'CDESCRI' => $laTmp[1]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY PAQUETES ACTIVOS PARA CODIGO DE ALUMNO [{$this->paData['CCODALU']}]";
         return false;
      }
      $this->paData = $laData;
      return true;
   }

   // ------------------------------------------------------------------------------
   // Graba paquete
   // 2018-12-27 FPM Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarPaquete() {
      $llOk = $this->mxValParamGrabarPaquete();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      /*$llOk = $this->mxCantidadDeSemestres($loSql);
      if (!$llOk) {
         return false;
      }*/
      //Conecta con DB Listener
      /*if ($this->laData['CUNIACA'] == '70') {
         $loSql1 = new CSql();
         $llOk1 = $loSql1->omConnectList();
         if (!$llOk1) {
            $this->pcError = $loSql1->pcError;
            return false;
         }
         //Comprueba las deudas
         $llOk1 = $this->mxComprobarDeudas($loSql1);
         $loSql1->omDisconnect();
         if (!$llOk1) {
            return false;
         }
      }*/
      //Generar la deuda
      $llOk = $this->mxGrabarPaquete($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      if (!$llOk) {
         return false;
      }
      return $llOk;
   }

   protected function mxValParamGrabarPaquete() {
      $this->paData['NMONTOT'] = (float)$this->paData['NMONTOT'];
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CIDPAQU']) or (strlen($this->paData['CIDPAQU']) != 4)) {
         $this->pcError = 'PARAMETRO ID DE PAQUETE INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODALU']) or (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'CODIGO DE ALUMNO INVALIDO O NO DEFINIDO';
         return false;
      } elseif ($this->paData['NMONTOT'] <= 0.00) {
         $this->pcError = 'MONTO TOTAL INVALIDO REFERENCIAL';
         return false;
      }
      return true;
   }

   protected function mxCantidadDeSemestres($p_oSql) {      
      $lcSql = "SELECT B.nSemest, A.cUniaca FROM A01MALU A 
                INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}' AND A.cUniAca = '70'"; // ES 70
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      $this->laData = ['NSEMEST' => $laTmp[0], 'CUNIACA' => $laTmp[1]];
      if (!isset($this->laData['NSEMEST']) && $this->laData['CUNIACA'] == '70') {   
         $this->pcError = 'ERROR EN EJECUCION DE COMANDO SQL';
         return false;
      } elseif (!isset($this->laData['NSEMEST']) && $this->laData['CUNIACA'] != '70') {   
         return true;
      }
      return true;
   }

   protected function mxComprobarDeudas($p_oSql) {
      $lcSql1 = "SELECT COUNT(*) FROM (SELECT cCodAlu, cProyec, cNroCuo FROM B09DPAG WHERE cCodAlu = '{$this->paData['CCODALU']}' AND 
                SUBSTRING(cProyec, 5, 2) in ('-1', '-2') GROUP BY cCodAlu, cProyec, cNroCuo) AS R";
      $R1 = $p_oSql->omExec($lcSql1);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = 'ERROR EN EJECUCION DE COMANDO SQL';
         return false;
      }
      $lnSemPag = $laFila[0];
      $lnSemTot = $this->laData['NSEMEST'] * 5;
      if ($lnSemPag < $lnSemTot) {
         $this->pcError = 'LA CANTIDAD DE CUOTAS PAGADAS NO SON LAS SUFICIENTES';
         return false;
      }
      return true;
   }

   protected function mxGrabarPaquete($p_oSql) {
      $lcSql = "SELECT cTipo FROM B03MPAQ WHERE cIdPaqu = '{$this->paData['CIDPAQU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = 'NO SE PUDO ENCONTRAR PAQUETE';
         return false;
      }
      if ($laTmp[0]=='B' || $laTmp[0]=='M' || $laTmp[0]=='D') {
         if ($laTmp[0]=='M'){
           $this->paData['CUNIACA'] = '01';
           $this->paData['CCODIGO'] = 'M';
         } elseif ($laTmp[0] =='D') {
           $this->paData['CUNIACA'] = '02';
           $this->paData['CCODIGO'] = 'D';
         } else {
           $this->paData['CCODIGO'] = 'B';
         }
         $lcParam = json_encode($this->paData);
         $lcSql = "SELECT P_B03MDEU_9('{$lcParam}')";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         if (!isset($laTmp[0])) {
            $this->pcError = 'ERROR EN EJECUCION DE COMANDO SQL';
            return false;
         }
         $laData = json_decode($laTmp[0], true);
         if (isset($laData['CNROPAG'])) {
            $this->paData = $laData;
            return true;
         } elseif (isset($laData['ERROR'])) {
            $this->pcError = $laData['ERROR'];
         } else {
            $this->pcError = 'ERROR EN RETORNO DE COMANDO SQL';
         }
         return false;
      } elseif ($laTmp[0]=='T') {
         $this->paData['CCODIGO'] = 'T';
         $lcCodAlu = [];
         for ($i = 1; $i <= 3  ; $i++) {
            if (!isset($this->paData['CTESIS'.$i])){
               break;
            }
            $lcSql = "SELECT cCodAlu FROM A01MALU WHERE cCodAlu = '{$this->paData['CTESIS'.$i]}'";
            $R1 = $p_oSql->omExec($lcSql);
            $laTmp1 = $p_oSql->fetch($R1);
            if (!isset($laTmp1[0])) {
               $this->pcError = 'NO SE ENCONTRO ALUMNO TESISTA';
               return false;
            }
            $lcCodAlu = $lcCodAlu + ['CTESIS'.$i => $this->paData['CTESIS'.$i]];
         }
         $lcParam = json_encode($this->paData);
         $lcSql ="SELECT P_B03MDEU_12('$lcParam')";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         if (!isset($laTmp[0])) {
            $this->pcError = 'ERROR EN EJECUCION DE COMANDO SQL';
            return false;
         }
         $laData = json_decode($laTmp[0], true);
         if (isset($laData['CNROPAG'])) {
            $this->paData = $laData;
            return true;
         } elseif (isset($laData['ERROR'])) {
            $this->pcError = $laData['ERROR'];
         } else {
            $this->pcError = 'ERROR EN RETORNO DE COMANDO SQL';
         }
         return false;
      } else {
         $this->pcError = 'NO SE PUDO IDETIFICAR TIPO DE PAQUETE';
         return false;
      }
   }

   // ------------------------------------------------------------------------------
   // Actualiza datos del alumno - celular, email
   // 2018-12-27 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omActualizarDatosAlumno() {
      $llOk = $this->mxValParamActualizarDatosAlumno();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $lo->pcError;
         return false;
      }
      $llOk = $this->mxActualizarDatosAlumno($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamActualizarDatosAlumno() {
      if (!isset($this->paData['CNROCEL'])) {
         $this->pcError = "NÚMERO DE CELULAR NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CEMAIL']) || !filter_var($this->paData['CEMAIL'], FILTER_VALIDATE_EMAIL)) {
         $this->pcError = "EMAIL NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CNRODNI']) || !preg_match('(^[E0-9]{1}[0-9]{7}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxActualizarDatosAlumno($p_oSql) {
      $lcSql = "UPDATE S01MPER SET cNroCel = '{$this->paData['CNROCEL']}', cEmail = '{$this->paData['CEMAIL']}', tModifi = NOW() WHERE cNroDni = '{$this->paData['CNRODNI']}'";
      $llOk = $p_oSql->omExec($lcSql);
      return $llOk;
   }

   // ------------------------------------------------------------------------------
   // Muestra el detalle del paquete elegido
   // 2018-12-27 FPM Creacion
   // ------------------------------------------------------------------------------
   public function omDetallePaquete() {
      $llOk = $this->mxValParamDetallePaquete();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDetallePaquete($loSql);
      $loSql->omDisconnect();
      return $llOk; 
   }

   protected function mxValParamDetallePaquete() {
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CIDPAQU']) or (strlen($this->paData['CIDPAQU']) != 4)) {
         $this->pcError = 'PARAMETRO ID DE PAQUETE INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODALU']) or (strlen($this->paData['CCODALU']) != 10)) {
         $this->pcError = 'CODIGO DEL ALUMNO INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODIGO']) or (strlen($this->paData['CCODIGO']) != 1)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxDetallePaquete($p_oSql) {

      if($this->paData['CCODIGO'] == 'B'){
         $lcSql = "SELECT A.cIdPaqu, A.cDescri, A.cEstado, A.cUniAca, B.cNomUni FROM B03MPAQ A
                   INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca WHERE A.cIdPaqu = '{$this->paData['CIDPAQU']}'";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         if (!isset($laTmp[0])) {
            //echo '<br>'.$lcSql;
            $this->pcError = 'ID DE PAQUETE NO EXISTE';
            return false;
         } elseif ($laTmp[2] != 'A') {
            $this->pcError = 'PAQUETE NO ESTA ACTIVO';
            return false;
         }
         $laData = ['CIDPAQU' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'CUNIACA' => $laTmp[3], 'CNOMUNI' => $laTmp[4], 'NMONTOT' => 0.00];
         $lcSql = "SELECT cIdCate, cDescri, nMonto, nCosFor, nCanSem, cCurCom FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}', '{$this->paData['CCODALU']}') ORDER BY cIdCate";
         $R1 = $p_oSql->omExec($lcSql);
         $this->paDatos = [];
         $laData['CCODINF'] = 'N'; 
         $laData['CCODIDM'] = 'N'; 
         while ($laTmp = $p_oSql->fetch($R1)) {
            $this->paDatos[] = ['CIDCATE' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'NMONTO' => $laTmp[2], 'NCOSFOR' => $laTmp[3], 'NCANSEM' => $laTmp[4], 'CCURCOM' => $laTmp[5]];
            if ($laTmp[0] == 'CCCSID'){
               $laData['CCODIDM'] = 'S'; 
            } elseif ($laTmp[0] == 'CCCSUC') {
               $laData['CCODINF'] = 'S'; 
            }
         } 
         if (count($this->paDatos) == 0) {
            $this->pcError = "NO HAY PAQUETES ACTIVOS PARA CODIGO DE ALUMNO [{$this->paData['CCODALU']}]";
            return false;
         }
         // RECUPERAR MONTO TOTAL DEL PAQUETE
         $lcSql = "SELECT SUM(nMonto + nCosFor) FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}', '{$this->paData['CCODALU']}')";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         if (!isset($laTmp)) {
            $this->pcError = "ERROR AL RECUPERAR DETALLE DE PAQUETE";
            return false;
         }
         $laData['NMONTO'] = $laTmp[0];
         // RECUPERAR DATOS DE CONTACTO DEL ALUMNO
         $lcSql = "SELECT cNroCel, cEmail FROM S01MPER WHERE cNroDni = '{$this->paData['CNRODNI']}'";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         if (!isset($laTmp)) {
            $this->pcError = "ERROR AL RECUPERAR INFORMACIÓN DEL ALUMNO";
            return false;
         }
         $laData['CNROCEL'] = $laTmp[0];
         $laData['CEMAIL'] = $laTmp[1];
         $this->paData = $laData;
         return true;
      } elseif($this->paData['CCODIGO'] == 'T'){ //TITULACION EN LINEA
         $lcSql = "SELECT A.cNroDni FROM B03MDEU A
                   INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog AND C.cIdCate = 'CCESTU'
                   WHERE A.cNroDni = '{$this->paData['CNRODNI']}' AND B.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado = 'C' AND A.cPaquet = 'B' AND C.cEtapa = 'D'";                
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) { //sin bachiller Online  
            $lcSql = "SELECT cCodAlu FROM B04DFED WHERE cCodAlu = '{$this->paData['CCODALU']}'";
            $R1 = $p_oSql->omExec($lcSql);
            $laFila1 = $p_oSql->fetch($R1);
            if (!isset($laFila1)) {
               $this->pcError = 'DEBE REGISTRAR SUS DOCUMENTOS FEDATEADOS';
               return false;
            }
            $lcSql = "SELECT A.cIdPaqu, A.cDescri, A.cEstado, A.cUniAca, B.cNomUni FROM B03MPAQ A
                  INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca WHERE A.cIdPaqu = '{$this->paData['CIDPAQU']}'";
            $R1 = $p_oSql->omExec($lcSql);
            $laTmp = $p_oSql->fetch($R1);
            if (!isset($laTmp[0])) {
               //echo '<br>'.$lcSql;
               $this->pcError = 'ID DE PAQUETE NO EXISTE';
               return false;
            } elseif ($laTmp[2] != 'A') {
               $this->pcError = 'PAQUETE NO ESTA ACTIVO';
               return false;
            }
            $laData = ['CIDPAQU' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'CUNIACA' => $laTmp[3], 'CNOMUNI' => $laTmp[4], 'NMONTOT' => 0.00];
            $lcSql = "SELECT cIdCate, cDescri, nMonto, nCosFor, nCanSem, cCurCom FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}', '{$this->paData['CCODALU']}') ORDER BY cIdCate";
            $R1 = $p_oSql->omExec($lcSql);
            $this->paDatos = [];
            while ($laTmp = $p_oSql->fetch($R1)) {
               $this->paDatos[] = ['CIDCATE' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'NMONTO' => $laTmp[2], 'NCOSFOR' => $laTmp[3], 'NCANSEM' => $laTmp[4], 'CCURCOM' => $laTmp[5]];
            }
            if (count($this->paDatos) == 0) {
               $this->pcError = "NO HAY PAQUETES ACTIVOS PARA CODIGO DE ALUMNO [{$this->paData['CCODALU']}]";
               return false;
            }
            // RECUPERAR MONTO TOTAL DEL PAQUETE
            $lcSql = "SELECT SUM(nMonto + nCosFor) FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}', '{$this->paData['CCODALU']}')";
            $R1 = $p_oSql->omExec($lcSql);
            $laTmp = $p_oSql->fetch($R1);
            if (!isset($laTmp)) {
               $this->pcError = "ERROR AL RECUPERAR DETALLE DE PAQUETE";
               return false;
            }
            $laData['NMONTO'] = $laTmp[0];
            // RECUPERAR DATOS DE CONTACTO DEL ALUMNO
            $lcSql = "SELECT cNroCel, cEmail FROM S01MPER WHERE cNroDni = '{$this->paData['CNRODNI']}'";
            $R1 = $p_oSql->omExec($lcSql);
            $laTmp = $p_oSql->fetch($R1);
            if (!isset($laTmp)) {
               $this->pcError = "ERROR AL RECUPERAR INFORMACIÓN DEL ALUMNO";
               return false;
            }
            $laData['CNROCEL'] = $laTmp[0];
            $laData['CEMAIL'] = $laTmp[1];
            $this->paData = $laData;
            return true;  
         } elseif (isset($laFila[0])) { //con Bachiller Online
            $lcSql = "SELECT A.cIdPaqu, A.cDescri, A.cEstado, A.cUniAca, B.cNomUni FROM B03MPAQ A
                      INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca WHERE A.cIdPaqu = '{$this->paData['CIDPAQU']}'";
            $R1 = $p_oSql->omExec($lcSql);
            $laTmp = $p_oSql->fetch($R1);
            if (!isset($laTmp[0])) {
               //echo '<br>'.$lcSql;
               $this->pcError = 'ID DE PAQUETE NO EXISTE';
               return false;
            } elseif ($laTmp[2] != 'A') {
               $this->pcError = 'PAQUETE NO ESTA ACTIVO';
               return false;
            }
            $laData = ['CIDPAQU' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'CUNIACA' => $laTmp[3], 'CNOMUNI' => $laTmp[4], 'NMONTOT' => 0.00];
            $lcSql = "SELECT cIdCate, cDescri, nMonto, nCosFor, nCanSem, cCurCom FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}', '{$this->paData['CCODALU']}') ORDER BY cIdCate";
            $R1 = $p_oSql->omExec($lcSql);
            $this->paDatos = [];
            while ($laTmp = $p_oSql->fetch($R1)) {
               $this->paDatos[] = ['CIDCATE' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'NMONTO' => $laTmp[2], 'NCOSFOR' => $laTmp[3], 'NCANSEM' => $laTmp[4], 'CCURCOM' => $laTmp[5]];
            }
            if (count($this->paDatos) == 0) {
               $this->pcError = "NO HAY PAQUETES ACTIVOS PARA CODIGO DE ALUMNO [{$this->paData['CCODALU']}]";
               return false;
            }
            //DOCUMENTOS DE ATUTENTICACION EN LINEA
            $lcSql = "SELECT cIdCate, cDescri, nMonto FROM B03TDOC WHERE CIDCATE IN ('PDAOTR','PDADOC')";    
            $R1 = $p_oSql->omExec($lcSql);
            $lnMonto = 0;
            while ($laFila = $p_oSql->fetch($R1)){
               $this->paDatos[] = $this->paDatos + ['CIDCATE' => $laFila[0], 'CDESCRI' => $laFila[1], 'NMONTO' => $laFila[2], 'NCOSFOR' => 0, 'NCANSEM' => 0, 'CCURCOM' => 'N' ];
               $lnMonto = $lnMonto + $laFila[2];
            }
            if(count($this->paDatos) == 0){
               $this->pcError = 'NO SE ENCONTRARON LOS CONCEPTOS DE PAGO';
               return false;
            }
            // RECUPERAR MONTO TOTAL DEL PAQUETE
            $lcSql = "SELECT SUM(nMonto + nCosFor) FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}', '{$this->paData['CCODALU']}')";
            $R1 = $p_oSql->omExec($lcSql);
            $laTmp = $p_oSql->fetch($R1);
            if (!isset($laTmp)) {
               $this->pcError = "ERROR AL RECUPERAR DETALLE DE PAQUETE";
               return false;
            }
            $laTmp[0] = $laTmp[0] + $lnMonto;
            $laData['NMONTO'] = $laTmp[0];
            // RECUPERAR DATOS DE CONTACTO DEL ALUMNO
            $lcSql = "SELECT cNroCel, cEmail FROM S01MPER WHERE cNroDni = '{$this->paData['CNRODNI']}'";
            $R1 = $p_oSql->omExec($lcSql);
            $laTmp = $p_oSql->fetch($R1);
            if (!isset($laTmp)) {
               $this->pcError = "ERROR AL RECUPERAR INFORMACIÓN DEL ALUMNO";
               return false;
            }
            $laData['CNROCEL'] = $laTmp[0];
            $laData['CEMAIL'] = $laTmp[1];
            $this->paData = $laData;
            return true;
         }
      } else {
        $lcSql = "SELECT A.cIdPaqu, A.cDescri, A.cEstado, A.cUniAca, B.cNomUni FROM B03MPAQ A
                  INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca WHERE A.cIdPaqu = '{$this->paData['CIDPAQU']}'";
        $R1 = $p_oSql->omExec($lcSql);
        $laTmp = $p_oSql->fetch($R1);
        if (!isset($laTmp[0])) {
           //echo '<br>'.$lcSql;
           $this->pcError = 'ID DE PAQUETE NO EXISTE';
           return false;
        } elseif ($laTmp[2] != 'A') {
           $this->pcError = 'PAQUETE NO ESTA ACTIVO';
           return false;
        }
        $laData = ['CIDPAQU' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'CUNIACA' => $laTmp[3], 'CNOMUNI' => $laTmp[4], 'NMONTOT' => 0.00];
        $lcSql = "SELECT cIdCate, cDescri, nMonto, nCosFor, nCanSem, cCurCom FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}', '{$this->paData['CCODALU']}') ORDER BY cIdCate";
        $R1 = $p_oSql->omExec($lcSql);
        $this->paDatos = [];
        $laData['CCODINF'] = 'N'; 
        $laData['CCODIDM'] = 'N'; 
        while ($laTmp = $p_oSql->fetch($R1)) {
           $this->paDatos[] = ['CIDCATE' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'NMONTO' => $laTmp[2], 'NCOSFOR' => $laTmp[3], 'NCANSEM' => $laTmp[4], 'CCURCOM' => $laTmp[5]];
           if ($laTmp[0] == 'CCCSID'){
              $laData['CCODIDM'] = 'S'; 
           } elseif ($laTmp[0] == 'CCCSUC') {
              $laData['CCODINF'] = 'S'; 
           }
        } 
        if (count($this->paDatos) == 0) {
           $this->pcError = "NO HAY PAQUETES ACTIVOS PARA CODIGO DE ALUMNO [{$this->paData['CCODALU']}]";
           return false;
        }
        // RECUPERAR MONTO TOTAL DEL PAQUETE
        $lcSql = "SELECT SUM(nMonto + nCosFor) FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}', '{$this->paData['CCODALU']}')";
        $R1 = $p_oSql->omExec($lcSql);
        $laTmp = $p_oSql->fetch($R1);
        if (!isset($laTmp)) {
           $this->pcError = "ERROR AL RECUPERAR DETALLE DE PAQUETE";
           return false;
        }
        $laTmp[0] = $laTmp[0] + $lnMonto;
        $laData['NMONTO'] = $laTmp[0];
        // RECUPERAR DATOS DE CONTACTO DEL ALUMNO
        $lcSql = "SELECT cNroCel, cEmail FROM S01MPER WHERE cNroDni = '{$this->paData['CNRODNI']}'";
        $R1 = $p_oSql->omExec($lcSql);
        $laTmp = $p_oSql->fetch($R1);
        if (!isset($laTmp)) {
           $this->pcError = "ERROR AL RECUPERAR INFORMACIÓN DEL ALUMNO";
           return false;
        }
        $laData['CNROCEL'] = $laTmp[0];
        $laData['CEMAIL'] = $laTmp[1];
        $this->paData = $laData;
        return true;
      }
      
   }

   // ------------------------------------------------------------------------------
   // Bandeja de entrada de transacciones de paquetes por usuario - centro de costo
   // 2018-12-20 FPM Creacion
   // ------------------------------------------------------------------------------
   /*
   public function omBandejaPaquetesUsuario() {
      $llOk = $this->mxValParamBandejaPaquetesUsuario();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerUsuario($loSql);
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }
      $llOk = $this->mxBandejaPaquetesUsuario($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBandejaPaquetesUsuario() {
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO [CCODUSU] INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxBandejaPaquetesUsuario($p_oSql) {
      $i = 0; //CCCOND
      //$lcSql = "SELECT cCodTre, TO_CHAR(tFecRec, 'yyyy-mm-dd hh24:mi'), cDesDoc, cEstTtr, cCodAlu, cNroDni, cNombre FROM V_B04MTRE_2
      //          WHERE cCcoDes IN (SELECT cCenCos FROM B03PUSU WHERE cCodUsu = '{$this->paData['CCODUSU']}') AND cEstTtr IN ('A', 'E') AND
      //          cIdCate LIKE 'PQ%' ORDER BY tFecRec";   OJOFPM
      $lcSql = "SELECT cCodTre, TO_CHAR(tFecRec, 'yyyy-mm-dd hh24:mi'), cDesDoc, cEstMtr, cCodAlu, cNroDni, cNombre FROM V_B04MTRE_2
                WHERE cCcoDes IN (SELECT cCenCos FROM B03PUSU WHERE cCodUsu = '{$this->paData['CCODUSU']}') AND cEstMtr IN ('A', 'E', 'F') AND
                cIdCate = 'CCCOND' ORDER BY tFecRec";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODTRE' => $laTmp[0], 'TFECREC' => $laTmp[1], 'CDESDOC' => $laTmp[2], 'CESTMTR'  => $laTmp[3], 'CCODALU' => $laTmp[4],
                             'CNRODNI' => $laTmp[5], 'CNOMBRE' => $laTmp[6]];
         $i++;
      }
      if ($i == 0) {
         $this->pcError = "NO HAY TRANSACCIONES PENDIENTES";
         return false;
      }
      $this->paData = $this->laData;
      return true;
   }*/

   // ------------------------------------------------------------------------------
   // Editar transaccion pendiente
   // 2018-12-20 FPM Creacion
   // ------------------------------------------------------------------------------
   public function omEditarTransaccion() {
      $llOk = $this->mxValParamEditarTransaccion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerUsuario($loSql);
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }
      $llOk = $this->mxEditarTransaccion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEditarTransaccion() {
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO [CCODUSU] INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODTRE']) or (strlen($this->paData['CCODTRE']) != 6)) {
         $this->pcError = 'PARAMETRO CODIGO DE TRANSACCION [CCODTRE] INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxEditarTransaccion($p_oSql) {
      $lcParam = json_encode($this->paData);
      $lcParam = str_replace("'", "''", $lcParam);
      $lcSql = "SELECT P_B04MTRE_10('$lcParam', 0)";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!$laTmp[0]) {
         $this->pcError = "ERROR EN EJECUCION DE VALIDACION DE CODIGO DE TRANSACCION Y USUARIO [P_B04MTRE_10]";
         return false;
      }
      $laData = json_decode($laTmp[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      $lcSql = "SELECT cCodTre, TO_CHAR(tFecRec, 'yyyy-mm-dd hh24:mi'), cDesDoc, cEstTtr, cCodAlu, cNroDni, cNombre, cCcoDes, mObserv, cIdCate FROM V_B04MTRE_2
                WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      $this->paDatos = ['CCODTRE' => $laTmp[0], 'TFECREC' => $laTmp[1], 'CDESDOC' => $laTmp[2], 'CESTMTR' => $laTmp[3], 'CCODALU' => $laTmp[4],
                       'CNRODNI' => $laTmp[5], 'CNOMBRE' => $laTmp[6], 'CIDCATE' => $laTmp[9]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Grabar transaccion pendiente
   // 2018-12-20 FPM Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarTransaccion() {
      $llOk = $this->mxValParamGrabarTransaccion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerUsuario($loSql);
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }
      $llOk = $this->mxGrabarTransaccion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarTransaccion() {
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO [CCODUSU] INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CCODTRE']) or (strlen($this->paData['CCODTRE']) != 6)) {
         $this->pcError = 'PARAMETRO CODIGO DE TRANSACCION [CCODTRE] INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['CRESULT']) or (strlen($this->paData['CRESULT']) != 1) or (!in_array($this->paData['CRESULT'], ['S', 'N']))) {
         $this->pcError = 'PARAMETRO RESULTADO [CRESULT] INVALIDO O NO DEFINIDO';
         return false;
      } elseif ($this->paData['CRESULT'] == 'N') {
         if (!isset($this->paData['MOBSERV'])) {
            $this->pcError = 'PARAMETRO OBSERVACIONES [MOBSERV] INVALIDO O NO DEFINIDO';
            return false;
         }
         if (strlen($this->paData['MOBSERV']) >= 200) {
            $this->pcError = 'LAS OBSERVACIONES DEBEN TENER MÁXIMO 200 CARACTERES';
            return false;
         }
         if (!isset($this->paData['MOBSERV']) || !preg_match("(^[a-zA-Z0-9áéíóúñÁÉÍÓÚÑ \n\/\.,;_-]+$)", $this->paData['MOBSERV'])) {
            $this->pcError = 'NO USAR CARACTERES ESPECIALES';
            return false;
         }
      } elseif ($this->paData['CRESULT'] == 'S') {
         if (!isset($this->paData['CCODINT']) or strlen($this->paData['CCODINT']) > 10) {
            $this->pcError = 'PARAMETRO CODIGO INTERNO [CCODINT] INVALIDO O NO DEFINIDO';
            return false;
         } elseif (!isset($this->paData['DVENCIM']) or strlen($this->paData['DVENCIM']) > 10) {   // OJO HAY QUE VALIDAR LA FECHA
            $this->pcError = 'PARAMETRO CODIGO INTERNO [CCODINT] INVALIDO O NO DEFINIDO';
            return false;
         }
      }
      return true;
   }

   protected function mxGrabarTransaccion($p_oSql) {
      $lcParam = json_encode($this->paData);
      $lcParam = str_replace("'", "''", $lcParam);
      $lcSql = "SELECT P_B04MTRE_10('$lcParam', 1)";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!$laTmp[0]) {
         $this->pcError = "ERROR EN EJECUCION DE GRABACION DE TRANSACCION [P_B04MTRE_10]";
         return false;
      }
      $laData = json_decode($laTmp[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      $lcSql = "SELECT cIdCate FROM B04MTRE WHERE cCodTre = '{$this->paData['CCODTRE']}' AND cEstado = 'B'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if ($laTmp[0] == 'PQ0008') { 
         $lcSql = "SELECT A.cIdCate, A.cCodTre, C.cNroDni, B.cCodAlu, D.cNombre FROM B04MTRE A
                  INNER JOIN B03DDEU B ON B.cIdlog = A.cIdlog
                  INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                  INNER JOIN S01MPER D ON D.cNroDni = C.cNroDni
                  WHERE A.cCodtre = '{$this->paData['CCODTRE']}' AND A.cEstado = 'B' AND A.cIdCate ='PQ0008'";
         $R1 = $p_oSql->omExec($lcSql);
         while ($laTmp = $p_oSql->fetch($R1)) {
            $this->laData = ['CIDCATE' => $laTmp[0], 'CCODTRE' => $laTmp[1], 'CNRODNI' => $laTmp[2], 'CCODALU' => $laTmp[3], 'CNOMBRE' => $laTmp[4]];
         }
         if (count($this->laData) == 0) {  
            return true;
         } else {
            $this->mxPrintConstanciaDesfile();
         }
      }
      // BEGIN SELECT * B04MTRE WHERE CCODTRE WHERE CESTADO = 'E' CONTINUE ? CESTADO = 'B' MERGE
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init muestra las deudas de paquetes del interesado
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitEstadoDeuda() {
      $llOk = $this->mxValParamInitEstadoDeuda();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitEstadoDeuda($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxValParamInitEstadoDeuda() {
      if (empty($this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO";
         return false;
      }
      return true;
   }
   protected function mxInitEstadoDeuda($p_oSql) {
      $lcNroDni = $this->paData['CNRODNI'];
     //CARGAR ESTADOS DE LOS TRAMITES REALIZADOS SEGUN EL DNI DEL ALUMNO
      $lcSql = "SELECT (TO_CHAR(MD.dfecha, 'YYYY-MM-DD HH24:MI ')),MD.cIdDeud,  MD.cNroPag, MD.nMonto,MD.cestado
               FROM B03MDEU MD
               WHERE MD.cNroDni = '$lcNroDni' AND MD.cPaquet = '{$this->paData['CCODIGO']}'
               ORDER BY MD.dfecha DESC";      
      $this->paDatos = [];
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $ldFecha = date("Y-m-d", strtotime(date("Y-m-d")."- 7 days"));
         $lcEstado = $laFila[0] >= $ldFecha?$laFila[4]:'X';
         if ($laFila[4] == 'C') {
            $lcEstado = 'C';
         }
         $lcTmp = substr($laFila[2],0,1).'-'.substr($laFila[2],1,3).'-'.substr($laFila[2],4,3).'-'.substr($laFila[2],7,3);
         $this->paDatos[] = [$laFila[0], $laFila[1], $lcTmp, $laFila[3], $lcEstado];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY DEUDAS PARA MOSTRAR";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Tramites que requieren llenar formulario
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitLlenadoFormulario() {
      $llOk = $this->mxValInitLlenadoFormulario();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitLlenadoFormulario($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxValInitLlenadoFormulario() {
      if (empty($this->paData['CNRODNI'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }
   protected function mxInitLlenadoFormulario($p_oSql) {
      $lcSql = "SELECT A.cIdLog, A.cRecibo, A.cIdCate, C.cDescri, B.cNroDni, A.cCodAlu, G.cNombre, D.cCodTre, TO_CHAR(D.tFecha, 'YYYY-MM-DD HH24:MI') as tFecRec, D.mDetall, E.nSerial, E.nOrden, E.mObserv, I.cProyec, K.cSecGru, G.cEmail, G.cNroCel, A.cPeriod, A.nCanSem, F.cNomUni, A.nCosto, A.cCurCom
                FROM B03DDEU A
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu
                INNER JOIN B03TDOC C ON C.cIdCate = A.cIdCate
                INNER JOIN B04MTRE D ON D.cIdLog  = A.cIdLog
                INNER JOIN B04DTRE E ON E.cCodTre = D.cCodTre
                INNER JOIN S01TUAC F ON F.cUniAca = B.cUniAca
                INNER JOIN S01MPER G ON G.cNroDni = B.cNroDni
                LEFT  JOIN A01PMAT H ON H.cCodAlu = A.cCodAlu
                LEFT  JOIN A02MCAR I ON I.cIdCarg = H.cIdCarg
                LEFT  JOIN A02PREL J ON J.cIdMatr = H.cIdMatr
                LEFT  JOIN A02PCAR K ON K.cIDRela = J.cIdRela
                INNER JOIN B03MDEU L ON L.cIdDeud = A.cIdDeud
                WHERE L.cNroDni = '{$this->paData['CNRODNI']}' AND L.cEstado = 'C' AND L.cPaquet = '{$this->paData['CCODIGO']}' AND D.cEstado = 'F' ORDER BY E.nSerial DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $i = 0;
      while ($laFila = $p_oSql->fetch($R1)) {
         $lcIdCate = $laFila[2];
         if ($lcIdCate == 'CCCSID') {
           $i++;
           if ($i == 2){
            $lcIdCate = 'CCSID2';
           }
         }
         $this->paDatos[$lcIdCate] = ['CIDLOG' => $laFila[0],'CRECIBO' => $laFila[1],'CIDCATE' => $laFila[2],'CDESCRI' => $laFila[3],
                          'CNRODNI' => $laFila[4],'CCODALU' => $laFila[5],'CNOMBRE' => $laFila[6],'CCODTRE' => $laFila[7],
                          'TFECREC' => $laFila[8], 'MDETALL' => json_decode($laFila[9],true), 'NSERIAL' => $laFila[10],
                          'NORDEN' => $laFila[11], 'MOBSERV' => json_decode($laFila[12],true), 'CPROYEC' => $laFila[13],
                          'CSECGRU' => $laFila[14], 'CEMAIL' => $laFila[15]?$laFila[15]:'NO DISPONIBLE',
                          'CNROCEL' => $laFila[16] ? $laFila[16] : 'NO DISPONIBLE', 'CPERIOD' => $laFila[17],
                          'NCANSEM' => $laFila[18], 'CNOMUNI' => $laFila[19], 'NMONTO' => $laFila[20],
                          'CCURCOM' => $laFila[21] == 'S' ? 'SI' : 'NO'];
         $this->paTipCurCCCSUC = [];
         if ($lcIdCate == 'CCCSID') {
            $lcSql = "SELECT cCodigo, cDescri FROM V_S01TTAB WHERE cCodTab = '043' AND cCodigo IN ('EN','PT','FR','IT','QU','DE')";
            $R2 = $p_oSql->omExec($lcSql);
            while ($laFila = $p_oSql->fetch($R2)) {
               $this->paCurso[] = ['CCODIGO' => $laFila[0], 'CDESCRI' => $laFila[1]];
            }
            $lcSql = "SELECT cCodigo, cDescri FROM V_S01TTAB WHERE cCodTab = '160' AND cCodigo IN ('1','2','3','4','5','7','9')";
            $R2 = $p_oSql->omExec($lcSql);
            while ($laFila = $p_oSql->fetch($R2)) {
               $this->paDatos['CCCSID']['paTipCur'][] = ['CCODIGO' => $laFila[0], 'CDESCRI' => $laFila[1]];
            }
         } elseif ($lcIdCate == '000084') {
            $lcSql = "SELECT cCodigo, cDescri FROM V_S01TTAB WHERE cCodTab = '043' AND cCodigo IN ('ZH')";
            $R2 = $p_oSql->omExec($lcSql);
            while ($laFila = $p_oSql->fetch($R2)) {
               $this->paCurso[] = ['CCODIGO' => $laFila[0], 'CDESCRI' => $laFila[1]];
            }
            $lcSql = "SELECT cCodigo, cDescri FROM V_S01TTAB WHERE cCodTab = '158'";
            $R2 = $p_oSql->omExec($lcSql);
            while ($laFila = $p_oSql->fetch($R2)) {
               $this->paDatos['000084']['paTipCur'][] = ['CCODIGO' => $laFila[0], 'CDESCRI' => $laFila[1]];
            }
         } elseif ($lcIdCate == 'CCCSUC') {
            $lcSql = "SELECT cCodigo, cDescri FROM V_S01TTAB WHERE cCodTab = '161'";
            $R2 = $p_oSql->omExec($lcSql);
            while ($laFila = $p_oSql->fetch($R2)) {
               $this->paDatos['CCCSUC']['paTipCur'][] = ['CCODIGO' => $laFila[0], 'CDESCRI' => $laFila[1]];
            }
         }
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY TRAMITES PENDIENTES";
         return false;
      }
      return true;
   }

   /*public function omInitBandejaLlenadoFormulario() {
      $llOk = $this->mxValInitBandejaLlenadoFormulario();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaLlenadoFormulario($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInitBandejaLlenadoFormulario() {
      if (empty($this->paData['CNRODNI'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }
   protected function mxInitBandejaLlenadoFormulario($p_oSql) {
      $lcNroDni = $this->paData['CNRODNI'];
      $lcEstado = $this->paData['CESTADO'];
      $lcSql = "SELECT cIdlog, TO_CHAR(tFecRec, 'YYYY-MM-DD HH24:MI'), cNombre, cDesDoc, cIdCate
                  FROM V_B04MTRE_3
                  WHERE cEstMtr = '$lcEstado' AND cNroDni = '$lcNroDni' AND cEstDet != 'C' AND cPaquet = 'S'
                  ORDER BY tFecRec DESC";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         //$this->paDatos[] = [$laFila[0], $laFila[1], $laFila[2], $laFila[3], $laFila[4]];
         $this->paDatos[] = ['CIDLOG'  => $laFila[0], 'TFECREC' => $laFila[1],
                             'CNOMBRE' => $laFila[2], 'CDESDOC' => $laFila[3],
                             'CIDCATE' => $laFila[4]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY TRAMITES PENDIENTES";
         return false;
      }
      return true;
   }*/

   /*public function omBandejaSubidaDocumentos() {
      $llOk = $this->mxValParamBandejaSubidaDocumentos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBandejaSubidaDocumentos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxValParamBandejaSubidaDocumentos() {
      if (!isset($this->paData['CIDPAQU']) or (strlen($this->paData['CIDPAQU']) != 4)) {
         $this->pcError = 'PARAMETRO ID DE PAQUETE INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }
   protected function mxBandejaSubidaDocumentos($p_oSql) {
      //DEUDAS TIPO PAQUETE -> PARA BUSCAR EN B04MTRE
      //SELECT * FROM B03MDEU WHERE cNroDni = '72042019' AND cPaquet = 'S' AND cEstado = 'C';
      $lcSql = "SELECT A.cIdPaqu, A.cDescri, A.cEstado, A.cUniAca, B.cNomUni FROM B03MPAQ A
                       INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca WHERE A.cIdPaqu = '{$this->paData['CIDPAQU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         echo '<br>'.$lcSql;
         $this->pcError = 'ID DE PAQUETE NO EXISTE';
         return false;
      } elseif ($laTmp[2] != 'A') {
         $this->pcError = 'PAQUETE NO ESTA ACTIVO';
         return false;
      }
      $lcSql = "SELECT cIdCate, cDescri, nMonto, nCosFor, nCanSem, cCurCom
                FROM F_B03MDEU_3('{$this->paData['CIDPAQU']}')
                WHERE cIdCate LIKE 'PQ%'";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDCATE' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'NMONTO' => $laTmp[2], 'NCOSFOR' => $laTmp[3], 'NCANSEM' => $laTmp[4], 'CCURCOM' => $laTmp[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY PAQUETES ACTIVOS PARA CODIGO DE ALUMNO [{$this->paData['CCODALU']}]";
         return false;
      }
   }*/

   // ------------------------------------------------------------------------------
   // Graba Formularios (Llenado de Formularios)
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGuardarDocumento() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      //GUARDAR CERTIFICADO DE IDIOMAS
      if (isset($this->paData['CCCSID'])) {
         $this->paDatos = $this->paData['CCCSID'];
         $llOk = $this->mxGuardarDetalleDocumento($loSql);
         if (!$llOk) {
            $loSql->rollback();
            $this->pcError = "ERROR GUARDANDO CERTIFICADO DE IDIOMAS";
            return false;
         }
      }
      //GUARDAR SEGUNDO CERTIFICADO DE IDIOMAS PARA DOCTORADOS
      if (isset($this->paData['CCSID2'])) {
        $this->paDatos = $this->paData['CCSID2'];
        $llOk = $this->mxGuardarDetalleDocumento($loSql);
        if (!$llOk) {
           $loSql->rollback();
           $this->pcError = "ERROR GUARDANDO CERTIFICADO DE IDIOMAS";
           return false;
        }
     }
      if (isset($this->paData['000084'])) {
         $this->paDatos = $this->paData['000084'];
         $llOk = $this->mxGuardarDetalleDocumento($loSql);
         if (!$llOk) {
            $loSql->rollback();
            $this->pcError = "ERROR GUARDANDO CERTIFICADO DE IDIOMAS";
            return false;
         }
      }
      //GUARDAR CERTIFICADO DE ESTUDIOS
      if (isset($this->paData['CCESTU'])) {
         $this->paDatos = $this->paData['CCESTU'] + ['CCODEST' => $this->paData['CCODEST']];
         $llOk = $this->mxGuardarDetalleDocumento($loSql);
         if (!$llOk) {
            $loSql->rollback();
            $this->pcError = "ERROR GUARDANDO CERTIFICADO DE ESTUDIOS";
            return false;
         }
      }
      //GUARDAR CERTIFICADO DE INFORMATICA
      if (isset($this->paData['CCCSUC'])) {
         $this->paDatos = $this->paData['CCCSUC'];
         $llOk = $this->mxGuardarDetalleDocumento($loSql);
         if (!$llOk) {
            $loSql->rollback();
            $this->pcError = "ERROR GUARDANDO CERTIFICADO DE INFORMATICA";
            return false;
         }
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxGuardarDetalleDocumento($p_oSql) {
      $lcCodTre = $this->paDatos['CCODTRE'];
      $lnSerial = $this->paDatos['NSERIAL'];
      if($this->paDatos['CIDCATE'] == 'CCESTU') {
         $lnCanSem = $_SESSION['paData']['NCANSEM'];
         for ($i = 1; $i <= $lnCanSem; $i++) {
            $this->paDatos['MDETALL']['CSEMEST'][] = $i;
         }
         if ($this->paDatos['CCODEST'] == 'N'){
            $lcSql = "UPDATE B04MTRE SET cEstPro = 'P' WHERE cCodTre='$lcCodTre';";
            $llOk = $p_oSql->omExec($lcSql);
            if (!$llOk) {
               $this->pcError = 'ERROR AL ACTUALIZAR TRAMITE';
               return false;
            }
         }
      }
      $lmObserv = json_encode($this->paDatos['MDETALL']);
      //AGREGAR DETALLE DE DOCUMENTO
      $lcSql = "UPDATE B04MTRE SET mDetall = '$lmObserv', cEstado='A', tFecha = NOW(), tModifi = NOW() WHERE cCodTre='$lcCodTre';";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR TRAMITE';
         return false;
      }
      //AGREGAR DETALLE DE DOCUMENTO
      $lcSql = "UPDATE B04DTRE SET cEstado='A', tRecibi = NOW(), tModifi=NOW() WHERE nSerial = '$lnSerial';";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR DETALLE DEL TRAMITE';
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Consulta el estado de un tramite del paquete
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omInitDetalleSeguimiento() {
      $llOk = $this->mxValInitDetalleSeguimiento();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitDetalleSeguimiento($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInitDetalleSeguimiento() {
      if (empty($this->paData['CCODTRE'])) {
         $this->pcError = "NUMERO DE TRAMITE NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitDetalleSeguimiento($p_oSql) {
      $lcCodtre = $this->paData['CCODTRE'];
      $lcSql = "SELECT  A.cCodTre, A.cIdCate, C.cDescri, A.cEstado AS cEstMTr, B.CEstado AS cEstDTr, A.cCCoDes, A.mObserv As mObserm, B.mObserv As mObserd, TO_CHAR(COALESCE(A.tFecha, B.tRecibi), 'YYYY-MM-DD HH24:MI') AS tRecibi, TO_CHAR(COALESCE(A.tmodifi, B.tUltima), 'YYYY-MM-DD HH24:MI') AS tUltima, COALESCE(G.cNombre, D.cDescri) AS cNomPer, A.mDetall, A.cCodUsu, A.cEstPro, A.cDocDig FROM B04MTRE A
      LEFT JOIN B04DTRE B ON B.cCodTre = A.cCodTre
      LEFT JOIN B03PUSU E ON E.cCodigo = B.cCodigo
      LEFT JOIN S01TUSU F ON F.cCodUsu = E.cCodUsu
      LEFT JOIN S01MPER G ON G.cNroDni = F.cNroDni
      INNER JOIN B03TDOC C ON C.cIdCate = A.cIdCate
      INNER JOIN S01TCCO D ON D.cCenCos = A.cCCoDes
      WHERE A.cCodTre = '$lcCodtre'";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
        $lcIdCate = $laFila[1];
        $this->paDatos[] = ['CCODTRE' => $laFila[0], 'CIDCATE' => $laFila[1],
                            'CDESCRI' => $laFila[2], 'CESTMTR' => $laFila[3], 'CESTDTR' => $laFila[4], 'CCCODES' => $laFila[5],
                            'MOBSERM' => $laFila[6], 'MOBSERD' => json_decode($laFila[7], true),'TRECIBI' => $laFila[8], 'TULTIMA' => $laFila[9],
                            'CNOMPER' => $laFila[10], 'MDETALL' => json_decode($laFila[11], true), 'CCODUSU' => $laFila[12], 'CESTPRO' => $laFila[13],
                            'CDOCDIG' => $laFila[14]];
      }
      if($this->paDatos[0]['CDOCDIG']){
         $lcPath = '/'.substr($this->paDatos[0]['CDOCDIG'],64,-1).'f';
         $lcPath = '='.addslashes($lcPath); 
         $this->paDatos[0]['CDOCDIG'] = substr($this->paDatos[0]['CDOCDIG'],0,64);
         $this->paDatos[0]['CDOCDIG'] = str_replace('=',$lcPath,$this->paDatos[0]['CDOCDIG']);
         if ($lcIdCate == 'CCCSUC' || $lcIdCate == 'CCCSID'){
           $this->paDatos[1]['CDOCDIG'] = $this->paDatos[0]['CDOCDIG'];
         }
      }
      if ($lcCodtre == '000002' || $lcCodtre == '000003'){
         $this->paDatos[] = ['CCODTRE' => $lcCodtre, 'CIDCATE' => 'PDAOTR',
                             'CDESCRI' => 'AUNTENTICACION', 'CESTMTR' => 'B', 'CESTDTR' => '', 'CCCODES' => '',
                             'MOBSERM' => '', 'MOBSERD' => '','TRECIBI' => '', 'TULTIMA' => '',
                             'CNOMPER' => '', 'MDETALL' => '', 'CCODUSU' => '9999', 'CESTPRO' => 'N',
                             'CDOCDIG' => ''];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO SE ENCONTRARON DATOS";
         return false;
      } 
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Revision Documentos (Por escuela profesional)
   // 2019-02-08 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaRevisionDocumentos() {
      $llOk = $this->mxValParamInitBandejaRevisionDocumentos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaRevisionDocumentos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaRevisionDocumentos() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CCODIGO'])) {
         $this->pcError = 'PARAMETRO CODIGO DE TRAMITE NO ESPECIFICADO';
         return false;
      }
      return true;
   }

   protected function mxInitBandejaRevisionDocumentos($p_oSql) {
      $this->paDatos = [];
      $lcSql = "SELECT A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                INNER JOIN B03TDOC D ON D.cIdCate = C.cIdCate
                INNER JOIN S01MPER E ON E.cNroDni = A.cNroDni
                INNER JOIN A01MALU F ON F.cCodAlu = B.cCodAlu
                INNER JOIN S01TUAC G ON G.cUniAca = F.cUniAca
                INNER JOIN B03PUSU H ON H.cCenCos = C.cCCoDes AND H.cEstado = 'A'
                WHERE A.cEstado = 'C' AND D.cTipo = 'PQ' AND A.cPaquet IN ('{$this->paData['CCODIGO']}') AND C.cEstado NOT IN ('B','X') AND H.cCodUsu = '{$this->paData['CCODUSU']}' AND C.cIdCate NOT IN ('PQ0009','PQ0033','PQ0032','PQ0034')
                GROUP BY A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni
                HAVING COUNT(*) = COUNT(CASE WHEN C.cEstado IN ('A', 'E') THEN 1 END)";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDDEUD' => $laFila[0], 'CNRODNI' => $laFila[1],
                             'CNOMBRE' => $laFila[2], 'CCODALU' => $laFila[3], 
                             'CNOMUNI' => $laFila[4]];
      }
      if ($this->paData['CCODIGO'] == 'T') {
         $lcSql = "SELECT A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni
                   FROM B03MDEU A
                   INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                   INNER JOIN B03TDOC D ON D.cIdCate = C.cIdCate
                   INNER JOIN S01MPER E ON E.cNroDni = A.cNroDni
                   INNER JOIN A01MALU F ON F.cCodAlu = B.cCodAlu
                   INNER JOIN S01TUAC G ON G.cUniAca = F.cUniAca
                   INNER JOIN B03PUSU H ON H.cCenCos = C.cCCoDes AND H.cEstado = 'A'
                   WHERE A.cEstado = 'C' AND D.cTipo = 'PQ' AND A.cPaquet IN ('{$this->paData['CCODIGO']}')  AND C.cEstado <> 'B' AND H.cCodUsu = '{$this->paData['CCODUSU']}' AND C.cIdCate IN ('PQ0033')
                   GROUP BY A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni
                   HAVING COUNT(*) = COUNT(CASE WHEN C.cEstado IN ('A', 'E') THEN 1 END)";
         $R1 = $p_oSql->omExec($lcSql);
         while ($laFila = $p_oSql->fetch($R1)) {
            $this->paDatos[] = $this->paDatos + ['CIDDEUD' => $laFila[0], 
                               'CNRODNI' => $laFila[1], 'CNOMBRE' => $laFila[2], 
                               'CCODALU' => $laFila[3], 'CNOMUNI' => $laFila[4]];
         }
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
      
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Revision Documentos (Por alumno)
   // 2019-02-08 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaRevisionDocumentosAlumno() {
      $llOk = $this->mxValParamInitBandejaRevisionDocumentosAlumno();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaRevisionDocumentosAlumno($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaRevisionDocumentosAlumno() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CIDDEUD'])) {
         $this->pcError = "REGISTRO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaRevisionDocumentosAlumno($p_oSql) {
      if ($this->paData['CCODIGO'] == 'B') {
         $lcSql = "SELECT TO_CHAR(A.dRecepc, 'yyyy-mm-dd hh24:mi'), C.cNombre, A.cIdDeud, F.cDescri, D.cCodTre, A.cNroDni, B.cCodAlu, D.cEstado
                   FROM B03MDEU A
                   JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   JOIN S01MPER C ON C.cNroDni = A.cNroDni
                   JOIN B04MTRE D ON D.cIdLog  = B.cIdLog
                   JOIN B03PUSU E ON E.cCenCos = D.cCCoDes  AND E.cEstado = 'A'
                   JOIN B03TDOC F ON F.cIdCate = B.cIdCate
                   WHERE A.cPaquet IN ('{$this->paData['CCODIGO']}') AND D.cEstado IN ('B','A','E') AND D.cIdCate NOT IN ('CCCOND','PQ0032','PQ0034') AND A.cIdDeud = '{$this->paData['CIDDEUD']}' AND A.cEstado = 'C' AND E.cCodUsu = '{$this->paData['CCODUSU']}' AND D.cIdCate <> 'PQ0009'
                   ORDER BY B.cIdLog";
      } else {
         /*$lcSql = "SELECT A.cCodAlu, A.cTipo, B.cNroDni, C.cNombre, D.cNomUni, A.nSerial FROM B04DFED A 
                   INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu
                   INNER JOIN S01MPER C ON C.cNroDni = B.cNroDni
                   INNER JOIN S01TUAC D ON D.cUniAca = B.cUniAca
                   WHERE A.cEstado = 'A'";
         
         $R1 = $p_oSql->omExec($lcSql);
         $this->paDatos[] = ['DRECEPC' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CIDDEUD' => $laFila[2], 'CDESCRI' => $laFila[3],
                             'CCODTRE' => $laFila[4], 'CNRODNI' => $laFila[5],
                             'CCODALU' => $laFila[6], 'CESTMTR' => $laFila[7]];*/

         $lcSql = "SELECT TO_CHAR(A.dRecepc, 'yyyy-mm-dd hh24:mi'), C.cNombre, A.cIdDeud, F.cDescri, D.cCodTre, A.cNroDni, B.cCodAlu, D.cEstado
                   FROM B03MDEU A
                   JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   JOIN S01MPER C ON C.cNroDni = A.cNroDni
                   JOIN B04MTRE D ON D.cIdLog  = B.cIdLog
                   JOIN B03PUSU E ON E.cCenCos = D.cCCoDes  AND E.cEstado = 'A'
                   JOIN B03TDOC F ON F.cIdCate = B.cIdCate
                   WHERE A.cPaquet IN ('{$this->paData['CCODIGO']}') AND D.cEstado IN ('B','A','E') AND D.cIdCate NOT IN ('CCCOND','PQ0032','PQ0034') AND A.cIdDeud = '{$this->paData['CIDDEUD']}' AND A.cEstado = 'C' AND E.cCodUsu = '{$this->paData['CCODUSU']}' AND D.cIdCate <> 'PQ0009'
                   ORDER BY B.cIdLog";
      }
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['DRECEPC' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CIDDEUD' => $laFila[2], 'CDESCRI' => $laFila[3],
                             'CCODTRE' => $laFila[4], 'CNRODNI' => $laFila[5],
                             'CCODALU' => $laFila[6], 'CESTMTR' => $laFila[7]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN DOCUMENTOS PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Detalle De Documentos para Revisar
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omDetalleRevisionDocumentos() {
      $llOk = $this->mxValParamDetalleRevisionDocumentos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDetalleRevisionDocumentos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDetalleRevisionDocumentos() {
      if (!isset($this->paData['CCODTRE'])) {
         $this->pcError = "TRAMITE NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxDetalleRevisionDocumentos($p_oSql) {
      $lcSql = "SELECT TO_CHAR(A.dRecepc, 'yyyy-mm-dd hh24:mi'), C.cNombre, B.cCodAlu, G.cDescri, D.cCodTre, A.cNroDni, D.cIdCate, A.cIdDeud
                FROM B03MDEU A
                JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                JOIN S01MPER C ON C.cNroDni = A.cNroDni
                JOIN B04MTRE D ON D.cIdLog  = B.cIdLog
                JOIN B03TDOC G ON G.cIdCate = B.cIdCate
                WHERE D.cCodTre = '{$this->paData['CCODTRE']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "TRAMITE SIN DETALLE";
         return false;
      }
      $lcNroDni = $laFila[5];
      $lcCodTre = $laFila[4];
      $this->paDatos = ['DRECEPC' => $laFila[0], 'CNOMBRE' => $laFila[1],
                        'CCODALU' => $laFila[2], 'CDESCRI' => $laFila[3],
                        'CCODTRE' => $laFila[4], 'CNRODNI' => $laFila[5],
                        'CIDCATE' => $laFila[6], 'CIDDEUD' => $laFila[7]];
      $laDoc = $this->mxVerificarArchivoDocumento($lcNroDni, $lcCodTre);
      if (!$laDoc) {
         $this->pcError = "ERROR ENCONTRANDO EL DOCUMENTO";
         return false;
      }
      return true;
   }

   protected function mxVerificarArchivoDocumento($p_cNroDni, $p_cCodTre) {
      if (file_exists("EXP/D$p_cNroDni/P$p_cCodTre.pdf") || file_exists("EXP/D$p_cNroDni/P$p_cCodTre.jpg")) {
         return true;
      }
      return false;
   }

   // ------------------------------------------------------------------------------
   // Recupera Documento (pdf/jpg) de un tramite
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarArchivoDocumento($p_cNroDni, $p_cCodTre) {
      if (file_exists("EXP/D$p_cNroDni/P$p_cCodTre.pdf")) {
         $this->pcTipoArchivo = "application/pdf";
         $this->pcExtension = "pdf";
         return file_get_contents("EXP/D$p_cNroDni/P$p_cCodTre.pdf");
      }
      if (file_exists("EXP/D$p_cNroDni/P$p_cCodTre.jpg")) {
         $this->pcTipoArchivo = "image/jpeg";
         $this->pcExtension = "jpg";
         return file_get_contents("EXP/D$p_cNroDni/P$p_cCodTre.jpg");
      }
      return false;
   }

   // ------------------------------------------------------------------------------
   // Cargar carpeta para la subida de documentos del alumno
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarDocumentosCarpeta() {
      $llOk = $this->mxValParamRecuperarDocumentosCarpeta();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarDocumentosCarpeta($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarDocumentosCarpeta() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DEL ALUMNO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarDocumentosCarpeta($p_oSql) {
      $lcCodAlu = $this->paData['CCODALU'];
      $lcSql = "SELECT A.cIdDeud FROM B03DDEU A
                INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                WHERE A.cCodAlu = '$lcCodAlu' AND B.cPaquet = '{$this->paData['CCODIGO']}' AND B.cEstado = 'C'
                ORDER BY B.tModifi DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO HAY PAQUETE PENDIENTE";
         return false;
      }
      $lcIdDeud = $laFila[0];
      $lcSql = "SELECT cCodTre, cIdCate, cDescri, TO_CHAR(tFecRec, 'yyyy-mm-dd hh24:mi'), cEstado
                FROM v_b04mtre where cidlog in (select cidlog from b03ddeu where ciddeud = '{$lcIdDeud}')
                ORDER BY cCodTre";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODTRE' => $laTmp[0], 'CIDCATE' => $laTmp[1], 'CDESDOC' => $laTmp[2], 'TFECREC' => $laTmp[3], 'CESTADO' => $laTmp[4]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY DOCUMENTOS EN CARPETA VIRTUAL";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar certificados de estudios pendientes por subir
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarCertificadosPorSubir() {
      $llOk = $this->mxValParamRecuperarCertificadosPorSubir();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarCertificadosPorSubir($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarCertificadosPorSubir() {
      if (!isset($this->paData['CIDCATE']) || strlen($this->paData['CIDCATE']) != 6) {
         $this->pcError = "CERTIFICADO NO DEFINIDO O NO VALIDO";
      }
      return true;
   }

   protected function mxRecuperarCertificadosPorSubir($p_oSql) {
      $lcSql = "SELECT t_tFecha, t_cCodTre, t_cNombre, t_cNroDni, t_cNomUni, t_cDescri, t_cEstado 
                FROM F_B04MTRE_2('{$this->paData['CIDCATE']}')";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['TFECHA' => $laTmp[0], 'CCODTRE' => $laTmp[1], 'CNOMBRE' => $laTmp[2], 'CNRODNI' => $laTmp[3],'CNOMUNI' => $laTmp[4], 'CDESCRI'=>$laTmp[5], 'CESTADO' => $laTmp[6]];
      }
      if ($this->paData['CIDCATE'] = 'CCCSID')
         $lcSql = "SELECT t_tFecha, t_cCodTre, t_cNombre, t_cNroDni, t_cNomUni, t_cDescri, t_cEstado 
                  FROM F_B04MTRE_2('000084')";
         $R1 = $p_oSql->omExec($lcSql);
         while ($laTmp = $p_oSql->fetch($R1)) {
            $this->paDatos[] = $this->paDatos + ['TFECHA' => $laTmp[0], 'CCODTRE' => $laTmp[1], 'CNOMBRE' => $laTmp[2], 'CNRODNI' => $laTmp[3],'CNOMUNI' => $laTmp[4], 'CDESCRI'=>$laTmp[5], 'CESTADO' => $laTmp[6]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY CERTIFICADOS POR SUBIR";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Subida de archivos del alumno
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omSubirArchivo() {
      $llOk = $this->mxValParamSubirArchivo();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxSubirArchivo($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamSubirArchivo() {
      if (!isset($this->paData['CCODTRE']) || !preg_match('(^[0-9]{6}$)', $this->paData['CCODTRE'])) {
         $this->pcError = "TRAMITE NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CNRODNI']) || !preg_match('(^[0-9]{8}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO";
         return false;
      }
      if (!isset($this->paFile) || $this->paFile['size'] > 5242880 || ($this->paFile['type'] != 'application/pdf' && $this->paFile['type'] != 'image/jpeg')) {
         $this->pcError = "ARCHIVO NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxSubirArchivo($p_oSql) {
      $lcNroDni = $this->paData['CNRODNI'];
      $lcCodTre = $this->paData['CCODTRE'];
      if(!isset($this->paData['CTIPDOC'])){
         $lcSql = "SELECT cEstado, cIdCate FROM B04MTRE WHERE cCodTre = '$lcCodTre'";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) {
            $this->pcError = "TRAMITE NO ENCONTRADO";
            return false;
         }
         //VALIDA ESTADO DEL TRAMITE - SI SE PERMITE SUBIR
         if ($laFila[0] != 'F' && $laFila[0] != 'E' && $laFila[0] != 'B' && $laFila[0] != 'A') {
            $this->pcError = "SUBIDA DE DOCUMENTO NO PERMITIDA";
            return false;
         }
         //VALIDA FORMATO DE ARHIVO PARA CIDCATE
         if (($laFila[1] == 'PQ0001' && $this->paFile['type'] != 'image/jpeg') || ($laFila[1] != 'PQ0001' && $this->paFile['type'] != 'application/pdf')) {
            $this->pcError = "FORMATO NO VALIDO";
            return false;
         }
         if ($laFila[1] == 'PDAOTR' || $laFila[1] == 'PDADOC'  || $laFila[1] == 'PDACBU') {
            $lcSql = "UPDATE B04MTRE SET cEstado = 'B', tModifi = NOW(), tFecha = NOW()
                   WHERE cCodTre = '$lcCodTre'";
         } else { 
            $lcSql = "UPDATE B04MTRE SET cEstado = 'A', tModifi = NOW(), tFecha = NOW()
            WHERE cCodTre = '$lcCodTre'";
         }
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "NO SE ACTUALIZO EL ESTADO DE TRAMITE";
            return false;
         }
      }
      $lcFolder = "EXP/D$lcNroDni/";
      if (!is_dir($lcFolder)) {
         $perm = "0777";             
         $modo = intval( $perm, 8 ); 
         mkdir( $lcFolder, $modo ); 
         chmod( $lcFolder, $modo);
      }
      $lcFilePath = $lcFolder."P$lcCodTre.".($this->paFile['type'] == 'application/pdf' ? 'pdf' : 'jpg');
      if (!move_uploaded_file($this->paFile['tmp_name'], $lcFilePath)) {
         $this->pcError = "NO SE PUDO SUBIR EL ARCHIVO";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Subida de archivos de la ORAA
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omSubirArchivoORAA() {
      $llOk = $this->mxValParamSubirArchivoORAA();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxSubirArchivoORAA($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamSubirArchivoORAA() {
      if (!isset($this->paData['CCODTRE']) || !preg_match('(^[0-9]{6}$)', $this->paData['CCODTRE'])) {
         $this->pcError = "CODIGO TRAMITE NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CNRODNI']) || !preg_match('(^[0-9]{8}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paFile) || $this->paFile['size'] > 5242880 || ($this->paFile['type'] != 'application/pdf' && $this->paFile['type'] != 'image/jpeg')) {
         $this->pcError = "ARCHIVO NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen($this-paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O NO VALIDO";
      }
      return true;
   }

   protected function mxSubirArchivoORAA($p_oSql) {
      $lcCodTre = $this->paData['CCODTRE'];
      $lcNroDni = $this->paData['CNRODNI'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcSql = "SELECT cEstado FROM B04MTRE WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "TRAMITE NO ENCONTRADO";
         return false;
      }
      if ($laFila[0] != 'B' && $lcCodUsu != '2093') {
         $this->pcError = "SUBIDA DE DOCUMENTO NO PERMITIDA";
         return false;
      }
      $lcSql = "UPDATE B04MTRE SET cEstado = 'S', tModifi = NOW(), cCodUsu = '{$this->paData['CCODUSU']}'
                WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "NO SE ACTUALIZO EL ESTADO DE TRAMITE";
         return false;
      }
      $lcFolder = "EXP/D$lcNroDni/";
      if (!is_dir($lcFolder)) {
         mkdir($lcFolder);
      }
      $lcFilePath = $lcFolder."P$lcCodTre.".($this->paFile['type'] == 'application/pdf' ? 'pdf' : 'jpg');
      if (!move_uploaded_file($this->paFile['tmp_name'], $lcFilePath)) {
         $this->pcError = "NO SE PUDO SUBIR EL ARCHIVO";
         return false;
      }
      chmod($lcFilePath, 0666);
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualizar etapa del paquete por la revision de la escuela
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omRevisarPaqueteEscuela() {
      $llOk = $this->mxValParamRevisarPaqueteEscuela();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRevisarPaqueteEscuela($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();

      return $llOk;
   }

   protected function mxValParamRevisarPaqueteEscuela() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DEL ALUMNO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRevisarPaqueteEscuela($p_oSql) {
      $lcCodAlu = $this->paData['CCODALU'];
      $lcSql = "SELECT A.cIdDeud, B.cNroDni FROM B03DDEU A
                INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                WHERE A.cCodAlu = '$lcCodAlu' AND B.cPaquet = '{$this->paData['CCODIGO']}' AND B.cEstado = 'C'
                ORDER BY B.tModifi DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO HAY PAQUETE PENDIENTE";
         return false;
      }
      $lcIdDeud = $laFila[0];
      $this->paData['CNRODNI'] = $laFila[1];
      $lcSql = "UPDATE B04MTRE SET CETAPA = 'B', TMODIFI = NOW()
                  WHERE CIDLOG IN (SELECT cidlog from b03ddeu where ciddeud = '{$lcIdDeud}')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR PAQUETE';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Recupera colaciones de una escuela
   // 2019-03-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarColacionesEscuela() {
      $llOk = $this->mxValParamRecuperarColacionesEscuela();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarColacionesEscuela($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarColacionesEscuela() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarColacionesEscuela($p_oSql) {
      $lcSql = "SELECT A.cColUac, A.cUniAca, E.cNomUni, D.cDescri FROM B05PUAC A
                INNER JOIN S01TCCO B ON B.cUniAca = A.cUniAca
                INNER JOIN B03PUSU C ON C.cCenCos = B.cCenCos
                INNER JOIN B05MCOL D ON D.cIdCola = A.cIdCola
                INNER JOIN S01TUAC E ON E.cUniAca = A.cUniAca
                WHERE C.cCodUsu = '{$this->paData['CCODUSU']}' AND C.cEstado = 'A' AND A.cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCOLUAC' => $laFila[0], 'CUNIACA' => $laFila[1],
                             'CNOMUNI' => $laFila[2], 'CDESCRI' => $laFila[3]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN COLACIONES REGISTRADAS";
         return false;
      }
      return true;
   }

   public function omReporteJuradosBachiller() {
      $llOk = $this->mxValParamReporteJuradosBachiller();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxReporteJuradosBachiller($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamReporteJuradosBachiller() {
      if (!isset($this->paData['CCOLUAC']) || !preg_match('(^[0-9]{6}$)', $this->paData['CCOLUAC'])) {
         $this->pcError = "GRUPO DE COLACION NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxReporteJuradosBachiller($p_oSql) {
      $lcSql = "SELECT A.cCodDoc, C.cNombre, A.tModifi, D.cUniAca, E.cNomUni, F.cDescri FROM B05PREV A
                INNER JOIN A01MDOC B ON B.cCodDoc = A.cCodDoc
                INNER JOIN S01MPER C ON C.cNroDni = B.cNroDni
                INNER JOIN B05PUAC D ON D.cColUac = A.cColUac
                INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
                INNER JOIN B05MCOL F ON F.cIdCola = D.cIdCola
                WHERE A.cColUac = '{$this->paData['CCOLUAC']}' ORDER BY nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODDOC' => $laFila[0], 'CNOMBRE' => str_replace('/', ' ', $laFila[1]),
                             'TMODIFI' => $laFila[2], 'CUNIACA' => $laFila[3],
                             'CNOMUNI' => $laFila[4], 'CDESCRI' => $laFila[5]];
      }
      if (count($this->paDatos) != 3) {
         $this->pcError = "DOCENTES REVISORES NO COMPLETOS";
         return false;
      }
      $fecha_actual = date ("Y-m-d");
      try {
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->Cell(85);
         $pdf->SetFont('Courier','B',15);
         $pdf-> Cell(20,10,utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'),0,0,'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf-> Cell(20,10,utf8_decode('ESCUELA PROFESIONAL DE '. $this->paDatos[0]['CNOMUNI']),0,0,'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf->SetFont('Courier','BU',15);
         $pdf-> Cell(20,10,utf8_decode('DOCENTES REVISORES'), 0, 0,'C');
         $pdf->Image("./Images/ucsm-02.png" , 80, 80, 50, 50);
         $pdf->Ln(100);
         $pdf->SetFont('Courier','',10);
         $pdf->Cell(1, 0, str_repeat(' ', 8).utf8_decode('Por la presente se hace constar los docentes revisores de bachiller de la'), 0);
         $pdf->Ln(8);
         $pdf->Cell(1, 0, str_repeat(' ', 8).utf8_decode('Escuela Profesional de '.$this->paDatos[0]['CNOMUNI']), 0);
         $pdf->Ln(8);
         $pdf->Cell(1, 0, str_repeat(' ', 8).utf8_decode('para la '.$this->paDatos[0]['CDESCRI']), 0, 0,'L');
         $pdf->Ln(20);
         $pdf->Cell(1, 0, str_repeat(' ', 8).utf8_decode('PRESIDENTE - '.$this->paDatos[0]['CNOMBRE']), 0, 1);
         $pdf->Ln(8);
         $pdf->Cell(1, 0, str_repeat(' ', 8).utf8_decode('VOCAL      - '.$this->paDatos[1]['CNOMBRE']), 0, 1);
         $pdf->Ln(8);
         $pdf->Cell(1, 0, str_repeat(' ', 8).utf8_decode('SECRETARIO - '.$this->paDatos[2]['CNOMBRE']), 0, 1);
         $pdf->Ln(40);
         $pdf->SetFont('Courier','',10);
         $pdf->Cell(20, 6, str_repeat(' ', 50).utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'));
         $pdf->Ln(8);
         $pdf->SetFont('Courier','',8);
         $pdf->Cell(1, 0, str_repeat(' ', 86).utf8_decode("AREQUIPA, $fecha_actual"));
         $llOk = $pdf->Output("I");
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
   }

   // ------------------------------------------------------------------------------
   // Generar constancia de expediente completo - escuela
   // 2019-03-12 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omGenerarConstancia() {
      $llOk = $this->mxValGenerarConstancia();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGenerarConstancia($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValGenerarConstancia(){
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DEL ALUMNO NO DEFINIDO";
         return false;
      }
      return true;
   }
   
   protected function mxGenerarConstancia($p_oSql){
      //PARA LLENAR LA CONSTANCIA
      $lcCodAlu = $this->paData['CCODALU'];
      $lcSql = "SELECT A.cCodAlu, A.cNroDni, B.cNombre, A.cUniAca, C.cNomUni, TO_CHAR(NOW(), 'yyyy-mm-dd')
                  FROM A01MALU A
                  INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni
                  INNER JOIN S01TUAC C ON C.cUniAca = A.cUniAca AND CNIVEL = '01'
                  WHERE A.cCodAlu = '$lcCodAlu'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
        $this->pcError = "NO SE OBTUVIERON DATOS DE ALUMNO";
        return false;
      }
      $this->paDatos = ['CCODALU' => $laFila[0],'CNRODNI' => $laFila[1], 'CNOMBRE' => str_replace('/', ' ', $laFila[2]), 'CUNIACA' => $laFila[3], 'CNOMUNI' => $laFila[4], 'DFECHA' => $laFila[5]];
      $lcNroDni = $laFila[1];
      $lcSql = "SELECT cCodTre FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog  = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                WHERE C.cPaquet = 'B' AND B.cCodAlu = '{$this->paDatos['CCODALU']}' AND A.cIdCate = 'CCESTU'
                ORDER BY tFecha DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE OBTUVIERON DATOS DEL TRAMITE";
         return false;
      }
      $this->paDatos['CCODTRE'] = $laFila[0];
      $lcPath= "./EXP/D$lcNroDni/P000000.pdf";
      try {
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->Cell(85);
         $pdf->SetFont('Courier', 'B', 15);
         $pdf-> Cell(20, 10, utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf-> Cell(20,10,utf8_decode('ESCUELA PROFESIONAL DE '. $this->paDatos['CNOMUNI']),0,0,'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf->SetFont('Courier', 'BU', 15);
         $pdf->Cell(20, 10, utf8_decode('CONSTANCIA DE EXPEDIENTE COMPLETO'), 0, 0,'C');
         $pdf->Image("./Images/ucsm-02.png" , 80, 80, 50, 50);
         $pdf->Ln(80);
         $pdf->SetFont('Courier', '', 10);
         $pdf->MultiCell(186, 8, str_repeat(' ', 20).utf8_decode("Por la presente se hace constar que el alumno {$this->paDatos['CNOMBRE']} con código de alumno {$this->paDatos['CCODALU']} egresado de la Escuela Profesional de {$this->paDatos['CNOMUNI']}, con número de expediente E-{$this->paDatos['CCODTRE']} presenta su expediente completo."), 0);
         $pdf->Ln(20);
         $pdf->Cell(68);
         $pdf->Cell(20, 5, utf8_decode('Se expide la presente a solicitud del interesado para'), 0, 0,'L');
         $pdf->Ln(8);
         $pdf->Cell(20, 5, str_repeat(' ', 53).utf8_decode('los fines que estime conveniente.'), 0, 0,'J');
         $pdf->Ln(30);
         $pdf->Cell(106);
         $pdf->SetFont('Courier', '', 10);
         $pdf->Cell(20,6,utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'));
         $pdf->Ln(10);
         $pdf->Cell(144);
         $pdf->SetFont('Courier','',8);
         $pdf->Cell(20, 5, utf8_decode('AREQUIPA, '.$this->paDatos['DFECHA']));
         $llOk = $pdf->Output($lcPath, "F");
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Generar constancia de expediente completo - escuela
   // 2019-03-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGenerarConstanciaTitulacion() {
      $llOk = $this->mxValGenerarConstancia();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGenerarConstanciaTitulacion($loSql);
      $loSql->omDisconnect();
      return $llOk; 
   }

   protected function mxValGenerarConstanciaTitulacion(){
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DEL ALUMNO NO DEFINIDO";
         return false;
      }
      return true;
   }
   
   protected function mxGenerarConstanciaTitulacion($p_oSql){
      //PARA LLENAR LA CONSTANCIA
      $lcCodAlu = $this->paData['CCODALU'];
      $lcSql = "SELECT A.cCodAlu, A.cNroDni, B.cNombre, A.cUniAca, C.cNomUni, TO_CHAR(NOW(), 'yyyy-mm-dd')
                  FROM A01MALU A
                  INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni
                  INNER JOIN S01TUAC C ON C.cUniAca = A.cUniAca AND CNIVEL = '01'
                  WHERE A.cCodAlu = '$lcCodAlu'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
        $this->pcError = "NO SE OBTUVIERON DATOS DE ALUMNO";
        return false;
      }
      $this->paDatos = ['CCODALU' => $laFila[0],'CNRODNI' => $laFila[1], 'CNOMBRE' => str_replace('/', ' ', $laFila[2]), 'CUNIACA' => $laFila[3], 'CNOMUNI' => $laFila[4], 'DFECHA' => $laFila[5]];
      $lcNroDni = $laFila[1];
      $lcSql = "SELECT cCodTre FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog  = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                WHERE C.cPaquet = 'T' AND B.cCodAlu = '{$this->paDatos['CCODALU']}' AND A.cIdCate = 'PQ0004'
                ORDER BY tFecha DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE OBTUVIERON DATOS DEL TRAMITE";
         return false;
      }
      $this->paDatos['CCODTRE'] = $laFila[0];
      $lcPath= "./EXP/D$lcNroDni/P000001.pdf";
      try {
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->Cell(85);
         $pdf->SetFont('Courier', 'B', 15);
         $pdf-> Cell(20, 10, utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf-> Cell(20,10,utf8_decode('ESCUELA PROFESIONAL DE '. $this->paDatos['CNOMUNI']),0,0,'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf->SetFont('Courier', 'BU', 15);
         $pdf->Cell(20, 10, utf8_decode('CONSTANCIA DE EXPEDIENTE COMPLETO'), 0, 0,'C');
         $pdf->Image("./Images/ucsm-02.png" , 80, 80, 50, 50);
         $pdf->Ln(80);
         $pdf->SetFont('Courier', '', 10);
         $pdf->MultiCell(186, 8, str_repeat(' ', 20).utf8_decode("Por la presente se hace constar que el alumno {$this->paDatos['CNOMBRE']} con código de alumno {$this->paDatos['CCODALU']} y Nro de DNI: {$this->paDatos['CNRODNI']}, bachiller de la Escuela Profesional de {$this->paDatos['CNOMUNI']}, con número de expediente E-{$this->paDatos['CCODTRE']} presenta su expediente completo."), 0);
         $pdf->Ln(20);
         $pdf->Cell(68);
         $pdf->Cell(20, 5, utf8_decode('Se expide la presente a solicitud del interesado para'), 0, 0,'L');
         $pdf->Ln(8);
         $pdf->Cell(20, 5, str_repeat(' ', 53).utf8_decode('los fines que estime conveniente.'), 0, 0,'J');
         $pdf->Ln(30);
         $pdf->Cell(106);
         $pdf->SetFont('Courier', '', 10);
         $pdf->Cell(20,6,utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'));
         $pdf->Ln(10);
         $pdf->Cell(144);
         $pdf->SetFont('Courier','',8);
         $pdf->Cell(20, 5, utf8_decode('AREQUIPA, '.$this->paDatos['DFECHA']));
         $llOk = $pdf->Output($lcPath, "F");
         chmod($lcPath, 0666);
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar listado de docentes
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omCargarArrayDocentes() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarArrayDocentes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxCargarArrayDocentes($p_oSql) {
      $lcSql = "SELECT A.cCodDoc, B.cNombre, A.cNroDni FROM A01MDOC A
                  INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni
                  WHERE A.cEstado = 'A' AND cCodDoc != '0000' AND A.cNroDni NOT LIKE 'X%' ORDER BY B.cNombre ASC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paArrays['paDocentes'] = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paArrays['paDocentes'][] = ['CCODDOC' => $laTmp[0], 'CNOMBRE' => str_replace('/', ' ', $laTmp[1]), 'CNRODNI' => $laTmp[2]];
      }
      if (count($this->paArrays['paDocentes']) == 0) {
         $this->pcError = "ERROR AL CARGAR DOCENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar ultima colación
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarUltimaColacion() {
      $llOk = $this->mxValParamRecuperarUltimaColacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarUltimaColacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarUltimaColacion() {
      if (!isset($this->paData['CTIPCOL']) || strlen($this->paData['CTIPCOL']) != 1) {
         $this->pcError = "TIPO DE COLACION NO DEFINIDA";
         return false;
      }
      return true;
   }

   protected function mxRecuperarUltimaColacion($p_oSql) {
      $lcSql = "SELECT cIdCola, cDescri, dFecha FROM B05MCOL WHERE cEstado = 'A' AND cTipCol = '{$this->paData['CTIPCOL']}' ORDER BY dFecha DESC, cIdCola DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "SIN COLACIONES REGISTRADAS";
         return false;
      }
      $this->paData = ['CIDCOLA' => $laTmp[0], 'CDESCRI' => $laTmp[1]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar listado de colaciones
   // 2019-02-27 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarTipoDeColaciones() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) { 
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarTipoDeColaciones($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxRecuperarTipoDeColaciones($p_oSql) {
      $lcSql = "SELECT TRIM(cCodigo), cDescri FROM S01TTAB WHERE cCodTab = '501' AND cTipReg = '1'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "LISTA DE TIPOS DE COLACION VACIA";
         return false;
      }
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paTipCol[] = ['CCODIGO' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      $lcSql = "SELECT DISTINCT C.cUniAca, C.cNomUni FROM B03PUSU A
                INNER JOIN S01TCCO B ON B.cCenCos = A.cCenCos 
                INNER JOIN S01TUAC C ON C.cUniAca = B.cUniAca
                WHERE A.cCodUsu = '{$this->paData['CCODUSU']}' AND A.cEstado = 'A' OR A.cCenCos = '031'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "NO TIENE ASIGNADA NINGUNA ESCUELA";
         return false;
      }
      $this->paEscuel = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paEscuel[] = ['CUNIACA' => $laFila[0],'CNOMUNI' => $laFila[1]];
      }
      //TRAE UNIDADES ACADEMICAS PARA ESPECIALIDAD MECANICA (OJO: NO ES LO CORRECTO)
      $lcSql = "SELECT cUniAca, cNomUni FROM S01TUAC WHERE cUniAca IN ('73','4M','4K','4L')";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "NO HAY ESPECIALIDADES DE MECANICA DEFINIDAS";
         return false;
      }
      $this->paEspMec = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paEspMec[] = ['CUNIACA' => $laFila[0], 'CNOMUNI' => $laFila[1]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar listado de colaciones
   // 2019-04-26 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omCargarListaColaciones() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarListaColaciones($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxCargarListaColaciones($p_oSql) {
      $lcSql =  "SELECT cIdCola, cDescri FROM B05MCOL WHERE cEstado = 'A' ORDER BY cIdCola DESC";
      $R1 = $p_oSql->omExec($lcSql); 
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paHisCol[] = ['CIDCOLA' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      if (count($this->paHisCol) == 0) {
         $this->pcError = "SIN COLACIONES REGISTRADAS";
         return false;
      }
      $lcSql =  "SELECT cEstPre, cDescri FROM S01TCCO WHERE cEstado = 'A' AND cDescri LIKE '%FACUL%'";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paLisFac[] = ['CESTPRE' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      if (count($this->paLisFac) == 0) {
         $this->pcError = "SIN FACULTADES ENCONTRADAS";
         return false;
      }
      $lcSql = "SELECT DISTINCT C.cUniAca, C.cNomUni FROM B03PUSU A
                INNER JOIN S01TCCO B ON B.cCenCos = A.cCenCos 
                INNER JOIN S01TUAC C ON C.cUniAca = B.cUniAca
                WHERE A.cCodUsu = '{$this->paData['CCODUSU']}' AND A.cEstado = 'A' OR A.cCenCos = '031'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "NO TIENE ASIGNADA NINGUNA ESCUELA";
         return false;
      }
      $this->paEscuel = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paEscuel[] = ['CUNIACA' => $laFila[0],'CNOMUNI' => $laFila[1]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar listado de colaciones y tipos
   // 2019-02-27 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omCargarArrayColaciones() {
      $llOk = $this->mxValParamCargarArrayColaciones();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarArrayColaciones($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCargarArrayColaciones() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DEL USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxCargarArrayColaciones($p_oSql) {
      $lcSql = "SELECT cIdCola, cPeriod, cDescri, dFecha FROM B05MCOL WHERE cEstado = 'A' ORDER BY cIdCola DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paArrays['paColaciones'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paArrays['paColaciones'][] = ['CIDCOLA' => $laFila[0], 'CPERIOD' => $laFila[1], 'CDESCRI' => $laFila[2], 'DFECHA' => $laFila[3]];
      }
      if (count($this->paArrays['paColaciones']) == 0) {
         $this->pcError = "LISTA DE COLACIONES VACIA";
         return false;
      }
      $lcSql = "SELECT TRIM(cCodigo), cDescri FROM S01TTAB WHERE cCodTab = '501' AND cTipReg = '1'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paArrays['paTipCol'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paArrays['paTipCol'][] = ['CCODIGO' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      if (count($this->paArrays['paTipCol']) == 0) {
         $this->pcError = "LISTA DE TIPOS DE COLACION VACIA";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar listado de alumnos para asignar a grupo de revision
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omCargarAlumnosParaRevision() {
      $llOk = $this->mxValParamCargarAlumnosParaRevision();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarAlumnosParaRevision($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCargarAlumnosParaRevision() {
      if (!isset($this->paData['CIDCOLA']) || !preg_match('(^[0-9]{5}$)', $this->paData['CIDCOLA'])) {
         $this->pcError = "COLACION NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen(trim($this->paData['CCODUSU'])) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CUNIACA']) || strlen(trim($this->paData['CUNIACA'])) != 2) {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (!isset($this->paData['CTIPCOL']) || strlen(trim($this->paData['CTIPCOL'])) != 1) {
         $this->pcError = "TIPO DE COLACION NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (in_array($this->paData['CUNIACA'], ['4E','4A']) && $this->paData['CTIPCOL'] == 'B' && (!isset($this->paData['CUNIESP']) || strlen(trim($this->paData['CUNIESP'])) != 2)) {
         $this->pcError = "ESPECIALIDAD NO DEFINIDA O NO VALIDA";
         return false;
      }
      return true;
   }

   protected function mxCargarAlumnosParaRevision($p_oSql) {
      if($this->paData['CTIPCOL'] == 'B') {
         $lcUniAca = (in_array($this->paData['CUNIACA'], ['4E','4A']))? $this->paData['CUNIACA']."','".$this->paData['CUNIESP'] : $this->paData['CUNIACA'];
         $lcSql = "SELECT DISTINCT ON (A.cCodTre) D.cCodAlu, D.cNombre, D.cNroDni, D.cUniAca, D.cNomUni FROM B04MTRE A
                   INNER JOIN B03DDEU B ON B.cIdLog  = A.cIdLog
                   INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud AND C.cPaquet = '{$this->paData['CCODIGO']}'
                   INNER JOIN V_A01MALU D ON D.cCodAlu = B.cCodAlu
                   WHERE A.cEtapa IN ('B', 'C') AND A.cIdCate = 'PQ0004' AND D.cUniAca IN ('$lcUniAca')
                   ORDER BY A.cCodTre";
         $R1 = $p_oSql->omExec($lcSql);
         if ($R1 == false) {
            $this->pcError = "ERROR AL RECUPERAR ALUMNOS PENDIENTES";
            return false;
         }
         $this->paDatos = [];
         while ($laFila = $p_oSql->fetch($R1)) {
            $lcUniAca = (in_array($this->paData['CUNIACA'], ['4E','4A']))? $this->paData['CUNIESP'] : $this->paData['CUNIACA'];
            $lcSql = "SELECT A.nSerial, A.cEstado, TRIM(B.cUniAca) FROM B05DUAC A INNER JOIN B05PUAC B ON B.cColUac = A.cColUac
                      WHERE B.cIdCola = '{$this->paData['CIDCOLA']}' AND A.cCodAlu = '{$laFila[0]}' AND A.cEstado != 'X'
                      ORDER BY nSerial DESC LIMIT 1";
            $R2 = $p_oSql->omExec($lcSql);
            if ($R2 == false) {
               $this->pcError = "ERROR AL VALIDAR ALUMNOS EN COLACION";
               return false;
            }
            $laTmp = $p_oSql->fetch($R2);
            $lnSerial = null;
            if (isset($laTmp[0]) && $laTmp[1] == 'I') {
               continue;
            } elseif (isset($laTmp[0]) && $laTmp[2] != $lcUniAca) {
               continue;
            } elseif (isset($laTmp[0])) {
               $lnSerial = $laTmp[0];
            }
            $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1], 'CNRODNI' => $laFila[2], 'CUNIACA' => $laFila[3],
                                'CNOMUNI' => $laFila[4], 'NSERIAL' => $lnSerial];
         }
      } elseif($this->paData['CTIPCOL'] == 'T') {
         $lcSql = "SELECT DISTINCT C.cCodAlu, C.cNombre, C.cNroDni, F.nSerial, C.cUniAca, C.cNomUni FROM B04MTRE A 
                   INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog 
                   INNER JOIN B03MDEU H ON H.cIdDeud = B.cIdDeud AND H.cPaquet = 'T' 
                   INNER JOIN V_A01MALU C ON C.cCodAlu = B.cCodAlu 
                   INNER JOIN B03PUSU E ON E.cCenCos = A.cCCoDes 
                   LEFT JOIN B05DUAC F ON F.cCodAlu = B.cCodAlu AND (F.cEstado <> 'I' and F.cEstado <> 'X')
                   LEFT JOIN B05PUAC G ON G.cColUac = F.cColUac 
                   WHERE A.cEtapa IN ('B', 'C') AND A.cIdCate = 'PQ0004' AND (F.nSerial IS NULL OR F.nSerial IS NOT NULL AND cIdCola = '{$this->paData['CIDCOLA']}') 
                   AND E.cCodUsu = '{$this->paData['CCODUSU']}' AND E.cEstado = 'A' AND C.cUniAca = '{$this->paData['CUNIACA']}'";
         $R1 = $p_oSql->omExec($lcSql);
         $this->paDatos = [];
         while ($laFila = $p_oSql->fetch($R1)) {
            $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1], 'CNRODNI' => $laFila[2], 'NSERIAL' => $laFila[3],
                                'CUNIACA' => $laFila[4], 'CNOMUNI' => $laFila[5]];
         }
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY ASIGNACION DE ALUMNOS PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar listado de alumnos del grupo de colacion seleccionado
   // 2019-03-19 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omCargarAlumnosExpComp() {
      $llOk = $this->mxValParamCargarAlumnosExpComp();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarAlumnosExpComp($loSql);
      $loSql->omDisconnect();

      return $llOk;
   }

   protected function mxValParamCargarAlumnosExpComp() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DEL USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CCOLUAC']) || !preg_match('(^[0-9]{6}$)', $this->paData['CCOLUAC'])) {
         $this->pcError = "GRUPO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxCargarAlumnosExpComp($p_oSql) {
      if ($this->paData['CCODIGO'] == 'B') {
         $lcSql = "SELECT A.cCodAlu, C.cNombre, B.cNroDni FROM B05DUAC A 
                  INNER JOIN A01MALU B ON B.CCODALU = A.cCodAlu 
                  INNER JOIN S01MPER C ON C.CNRODNI = B.cNroDni 
                  INNER JOIN B05PUAC D ON D.cColUac = A.cColUac 
                  INNER JOIN B03MDEU E ON E.cNroDni = B.cNroDni
                  INNER JOIN B03DDEU F ON F.CIDDEUD = E.CIDDEUD AND F.CIDCATE = 'PQ0004'
                  INNER JOIN B04MTRE G ON G.CIDLOG = F.CIDLOG
                  WHERE A.cColUac = '{$this->paData['CCOLUAC']}' AND A.cEstado = 'I' AND E.CPAQUET = '{$this->paData['CTIPCOL']}' AND E.CESTADO = 'C' AND G.CETAPA = 'C'";
         $R1 = $p_oSql->omExec($lcSql);
      } elseif ($this->paData['CCODIGO'] == 'T') {
         $lcSql = "SELECT A.cCodAlu, C.cNombre, B.cNroDni FROM B05DUAC A 
                  INNER JOIN A01MALU B ON B.CCODALU = A.cCodAlu 
                  INNER JOIN S01MPER C ON C.CNRODNI = B.cNroDni 
                  INNER JOIN B05PUAC D ON D.cColUac = A.cColUac 
                  INNER JOIN B03MDEU E ON E.cNroDni = B.cNroDni
                  INNER JOIN B03DDEU F ON F.CIDDEUD = E.CIDDEUD AND F.CIDCATE = 'PQ0004'
                  INNER JOIN B04MTRE G ON G.CIDLOG = F.CIDLOG
                  WHERE A.cColUac = '{$this->paData['CCOLUAC']}' AND A.cEstado = 'A' AND E.CPAQUET = '{$this->paData['CTIPCOL']}' AND E.CESTADO = 'C' AND G.CETAPA = 'B'";
         $R1 = $p_oSql->omExec($lcSql);
      }
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1], 'CNRODNI' => $laFila[2]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY ALUMNOS ASIGNADOS A ESTE GRUPO";
         return false;
      }
      $this->paArrays = [];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Verificar revisores del grupo de la colacion seleccionada
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omVerificarRevisores() {
      $llOk = $this->mxValParamVerificarRevisores();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerificarRevisores($loSql);
      $loSql->omDisconnect();

      return $llOk;
   }

   protected function mxValParamVerificarRevisores() {
      if (!isset($this->paData['CIDCOLA']) || !preg_match('(^[0-9]{5}$)', $this->paData['CIDCOLA'])) {
         $this->pcError = "COLACION NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen(trim($this->paData['CCODUSU'])) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (!isset($this->paData['CUNIACA']) || strlen(trim($this->paData['CUNIACA'])) != 2) {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (!isset($this->paData['CTIPCOL']) || strlen(trim($this->paData['CTIPCOL'])) != 1) {
         $this->pcError = "TIPO DE COLACION NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (in_array($this->paData['CUNIACA'], ['4E','4A']) && $this->paData['CTIPCOL'] == 'B' && (!isset($this->paData['CUNIESP']) || strlen(trim($this->paData['CUNIESP'])) != 2)) {
         $this->pcError = "ESPECIALIDAD NO DEFINIDA O NO VALIDA";
         return false;
      }
      return true;
   }

   protected function mxVerificarRevisores($p_oSql) {
      $lcUniAca = (in_array($this->paData['CUNIACA'], ['4E','4A']) && $this->paData['CTIPCOL'] == 'B')? $this->paData['CUNIESP'] : $this->paData['CUNIACA'];
      // RECUPERAR GRUPO COLACION
      $lcSql = "SELECT cColUac FROM B05PUAC WHERE cEstado = 'A' AND cIdCola = '{$this->paData['CIDCOLA']}' AND cUniAca = '$lcUniAca'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      // si la colacion no esta creada (se creara mas adelante)
      if (!isset($laTmp[0])) {
         return true;
      }
      $lcColUac = $laTmp[0];
      // VERIFICAR SI LOS REVISORES ESTAN COMPLETOS
      $lcSql = "SELECT A.cCodDoc, B.cNombre FROM B05PREV A
                INNER JOIN V_A01MDOC B ON B.cCodDoc = A.cCodDoc
                WHERE A.cColUac = '$lcColUac'
                ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODDOC' => $laFila[0], 'CNOMBRE' => str_replace('/', ' ', $laFila[1])];
      }
      /*if (count() == 3) {
         $this->pcError = "NRO DE REVISORES COMPLETO";
         return false;
      }*/
      return true;
   }

   // ------------------------------------------------------------------------------
   // Asignar revisor al grupo de la colacion seleccionada
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAsignarRevisor() {
      $llOk = $this->mxValParamAsignarRevisor();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAsignarRevisor($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();

      return $llOk;
   }

   protected function mxValParamAsignarRevisor() {
      if (!isset($this->paData['CCODDOC'])) {
         $this->pcError = "CODIGO DE GRUPO NO DEFINIDO";
         return false;
      }
      /*if (!isset($this->paData['CCENCOS'])) {
         $this->pcError = "CENTRO DE COSTO NO DEFINIDO";
         return false;
      }*/
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CUNIACA'])) {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDA";
         return false;
      }
      return true;
   }

   protected function mxAsignarRevisor($p_oSql) {
      // RECUPERAR/CREAR GRUPO COLACION
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_B05PUAC_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "ERROR CREANDO GRUPO DE COLACION";
         return false;
      }
      $laJson = json_decode($laTmp[0], true);
      if (isset($laJson['ERROR'])) {
         $this->pcError = $laJson['ERROR'];
         return false;
      }
      $lcColUac = $laJson['CCOLUAC'];
      $lcUniAca = $laJson['CUNIACA'];
      $lcCodDoc = $this->paData['CCODDOC'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcSql = "SELECT cCodDoc FROM A01MDOC WHERE cCodDoc = '{$this->paData['CCODDOC']}' AND cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "DOCENTE NO EXISTE O NO ESTA ACTIVO";
         return false;
      }
      $lcSql = "SELECT nSerial FROM B05PREV WHERE cColUac = '$lcColUac' AND cEstado = 'A' AND cCodDoc = '{$this->paData['CCODDOC']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "DOCENTE YA REGISTRADO";
         return false;
      }
      $lcSql = "INSERT INTO B05PREV (cColUac, cCodDoc, cUsuCod, tModifi)
                     VALUES('$lcColUac', '{$this->paData['CCODDOC']}', '{$this->paData['CCODUSU']}', NOW())";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL REGISTRAR REVISOR';
         return false;
      }
      $lcSql = "SELECT cNroDni FROM A01MDOC WHERE cCodDoc = '{$this->paData['CCODDOC']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "ERROR AL SELECCIONAR DNI";
         return false;
      }
      $lcNroDni = $laFila[0];
      $lcSql = "SELECT cCodUsu FROM S01TUSU
                  WHERE cCodUsu = '$lcCodDoc' AND cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $lcSql = "INSERT INTO S01TUSU (CCODUSU, CNRODNI, CESTADO, CUNIACA, CNIVEL, CUSUCOD, TMODIFI)
                     VALUES('$lcCodDoc', '$lcNroDni', 'A', '00', 'N', '$lcCodUsu', NOW())";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = 'ERROR AL REGISTRAR USUARIO DEL SISTEMA';
            return false;
         }
      }
      $lcSql = "SELECT cCodUsu FROM B03DUSU
                  WHERE cCodUsu = '$lcCodDoc' AND cEstado = 'A' AND cNivel = 'N'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $lcSql = "INSERT INTO B03DUSU(cCodUsu, cuniaca, cidcate, cnivel, cestado, cusucod,tmodifi)
            VALUES ('$lcCodDoc', '$lcUniAca', '000000', 'N','A','$lcCodUsu', NOW())";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = 'ERROR AL ASIGNAR REVISOR';
            return false;
         }
      }
      $lcSql = "SELECT cCodigo FROM B03PUSU ORDER BY cCodigo DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $lcCodigo = (int)$laFila[0] + 1;
      if (!$laFila) {
         $this->pcError = "ERROR AL BUSCAR CODIGO";
         return false;
      }
      $lcSql = "SELECT cCodUsu FROM B03PUSU WHERE cCodUsu = '$lcCodDoc' AND cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $lcSql = "INSERT INTO B03PUSU(cCodigo, cCenCos, cCodUsu, cEstado, cUsuCod, tModifi)
                     VALUES (LPAD('$lcCodigo', '6', '0'), '000', '$lcCodDoc', 'A', '$lcCodUsu', NOW())";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL GRABAR CE9NTRO DE COSTO";
            return false;
         }
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Asignar alumno a grupo de colacion
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAsignarAlumno() {
      $llOk = $this->mxValParamAsignarAlumno();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAsignarAlumno($loSql);
      if (!$llOk) {
         $loSql->rollback();
         return false;
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAsignarAlumno() {
      if (!isset($this->paData['CIDCOLA']) || !preg_match('(^[0-9]{5}$)', $this->paData['CIDCOLA'])) {
         $this->pcError = "COLACION NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (!isset($this->paData['CCODALU']) || count($this->paData['CCODALU']) == 0) {
         $this->pcError = "SIN ALUMNOS PARA ASIGNAR";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxAsignarAlumno($p_oSql) {
      // RECUPERAR/CREAR GRUPO COLACION
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_B05PUAC_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "ERROR CREANDO GRUPO DE COLACION";
         return false;
      }
      $laJson = json_decode($laTmp[0], true);
      if (isset($laJson['ERROR'])) {
         $this->pcError = $laJson['ERROR'];
         return false;
      }

      $laData = ["CCOLUAC" => $laJson['CCOLUAC'], "CCODUSU" => $this->paData['CCODUSU']];
      foreach ($this->paData['CCODALU'] as $value) {
         $laData['CCODALU'] = $value;
         $lcJson = json_encode($laData);
         $lcSql = "SELECT P_B05DUAC_1('$lcJson')";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) {
            $this->pcError = "ERROR ASIGNANDO ALUMNO: $value";
            return false;
         }
         $laJson = json_decode($laFila[0], true);
         if (isset($laJson['ERROR'])) {
            $this->pcError = $laJson['ERROR'];
            return false;
         }
      }
      // REVISAR Y ACTIVAR DOCENTES PARA REVISION
      $lcSql = "UPDATE B05PREV SET cEstado = 'A' WHERE cColUac = '{$laData['CCOLUAC']}'";
      $llOk = $p_oSql->omExec($lcSql);
      return $llOk;
   }

   // ------------------------------------------------------------------------------
   // Actualizar etapa del paquete por la revision del docente
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omRevisarPaqueteDocente() {
      $llOk = $this->mxValParamRevisarPaqueteDocente();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRevisarPaqueteDocente($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarPaqueteDocente() {
      if (!isset($this->paData['paCodAlu']) || count($this->paData['paCodAlu']) == 0) {
         $this->pcError = "SIN ALUMNOS PARA REVISAR";
         return false;
      }
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRevisarPaqueteDocente($p_oSql) {
      foreach ($this->paData['paCodAlu'] as $lcCodAlu) {
         $lcJson = json_encode(['CCODUSU' => $this->paData['CCODUSU'], 'CCODALU' => $lcCodAlu]);
         $lcSql = "SELECT P_B05PREV_1('$lcJson')";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) {
            $this->pcError = "ERROR AL REVISAR PAQUETE";
            return false;
         }
         $laJson = json_decode($laFila[0], true);
         if (isset($laJson['ERROR'])) {
            $this->pcError = $laJson["ERROR"];
            return false;
         }
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Observacion de documentos en la revision de paquetes
   // 2019-02-11 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omObservacionDocumento() {
      $llOk = $this->mxValParamObservacionDocumento();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxObservacionDocumento($loSql);
      $loSql->omDisconnect();

      return $llOk;
   }

   protected function mxValParamObservacionDocumento() {
      if (!isset($this->paData['CCODTRE'])) {
         $this->pcError = "CODIGO DEL DOCUMENTO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxObservacionDocumento($p_oSql) {
      $lcCodTre = $this->paData['CCODTRE'];
      $lcSql = "SELECT A.CCODTRE, B.CDESCRI, A.MOBSERV, D.CNRODNI FROM B04MTRE A
                  INNER JOIN B03TDOC B ON B.CIDCATE = A.CIDCATE
                  INNER JOIN B03DDEU C ON C.CIDLOG  = A.CIDLOG
                  INNER JOIN B03MDEU D ON D.CIDDEUD = C.CIDDEUD
                  WHERE A.CCODTRE = '$lcCodTre'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "DOCUMENTO NO DEFINIDO";
         return false;
      }
      $this->paDatos = ['CCODTRE' => $laFila[0], 'CDESCRI' => $laFila[1], 'MOBSERV' => $laFila[2], 'CNRODNI' => $laFila[3]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Grabar observacion del documento seleccionado
   // 2019-02-11 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omObservarDocumento() {
      $llOk = $this->mxValParamObservarDocumento();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxObservarDocumento($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();

      return $llOk;
   }

   protected function mxValParamObservarDocumento() {
      if (!isset($this->paData['MOBSERV'])) {
         $this->pcError = "OBSERVACIÓN NO DEFINIDA";
         return false;
      }
      return true;
   }

   protected function mxObservarDocumento($p_oSql) {
      $lcCodTre = $this->paData['CCODTRE'];
      $lmObserv = $this->paData['MOBSERV'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcSql = "UPDATE B04MTRE SET CESTADO = 'E', MOBSERV = '$lmObserv', CCODUSU = '$lcCodUsu', TMODIFI = NOW()
                  WHERE CCODTRE = '$lcCodTre'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL INSERTAR OBSERVACION';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Generar constancia revision de la ORAA
   // 2019-04-01 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGenerarConstanciaORAA() {
      $llOk = $this->mxValParamGenerarConstanciaORAA();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGenerarConstanciaORAA($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGenerarConstanciaORAA() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DE ALUMNO NO VALIDO O NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxGenerarConstanciaORAA($p_oSql) {
      $lcSql = "SELECT cCodAlu, cNroDni, cNombre, cUniAca, cNomUni FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "CÓDIGO DE ALUMNO NO ENCONTRADO";
         return false;
      }
      $this->paData = ['CCODALU' => $laTmp[0], 'CNRODNI' => $laTmp[1],
                       'CNOMBRE' => $laTmp[2], 'CUNIACA' => $laTmp[3],
                       'CNOMUNI' => $laTmp[4]];
      try {
         $fecha_actual = date ("Y-m-d");
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->SetFont('Courier','B',15);
         $pdf->Cell(186, 10, utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->Cell(186, 10, utf8_decode('OFICINA DE REGISTRO Y ARCHIVO ACADÉMICO'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf->SetFont('Courier','BU',15);
         $pdf->Cell(20, 10, utf8_decode('CONSTANCIA DE EXPEDIENTE COMPLETO'), 0, 0,'C');
         $pdf->Image("./Images/ucsm-02.png" , 80, 80, 50, 50);
         $pdf->Ln(80);
         $pdf->SetFont('Courier', '', 10);
         $pdf->Cell(1, 8, utf8_decode("CÓDIGO DE ALUMNO    : {$this->paData['CCODALU']}"), 0, 1);
         $pdf->Cell(1, 8, utf8_decode("DNI DEL ALUMNO      : {$this->paData['CNRODNI']}"), 0, 1);
         $pdf->Cell(1, 8, utf8_decode("NOMBRE DEL ALUMNO   : {$this->paData['CNOMBRE']}"), 0, 1);
         $pdf->Cell(1, 8, utf8_decode("ESCUELA PROFESIONAL : {$this->paData['CNOMUNI']}"), 0, 1);
         $pdf->Ln(30);
         $pdf->Cell(106);
         $pdf->SetFont('Courier', '', 10);
         $pdf->Cell(20,6,utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'));
         $pdf->Ln(10);
         $pdf->Cell(144);
         $pdf->SetFont('Courier','',8);
         $pdf->Cell(20, 5, utf8_decode('AREQUIPA, '.$fecha_actual));
         $llOk = $pdf->Output($this->pcFile, "F");
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualizar etapa del paquete por la revision de la ORAA
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omRevisarPaqueteORAA() {
      $llOk = $this->mxValParamRevisarPaqueteORAA();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRevisarPaqueteORAA($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarPaqueteORAA() {
      if (!isset($this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DEL ALUMNO NO DEFINIDO";
         return false;
      } 
      return true;
   }

   protected function mxRevisarPaqueteORAA($p_oSql) {
      $lcCodAlu = $this->paData['CCODALU'];
      $lcSql = "SELECT A.cIdDeud FROM B03DDEU A
                  INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                  WHERE A.cCodAlu = '$lcCodAlu' AND B.cPaquet = 'B' AND B.cEstado = 'C'
                  ORDER BY B.tModifi DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO HAY PAQUETE PENDIENTE";
         return false;
      }
      $lcIdDeud = $laFila[0];
      //CREACION DE TRAMITE PDDEBA
      $lcSql = "SELECT cIdLog FROM B03DDEU WHERE cIdDeud = '$lcIdDeud' AND cIdCate = 'PDDEBA'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $lcIdLog = $laFila[0];
      $lcSql = "SELECT cCodTre FROM B04MTRE WHERE cIdLog = '$lcIdLog' AND cIdCate = 'PDDEBA'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!empty($laFila[0])){
        $this->pcError = 'YA EXISTE DIPLOMA REGISTRADO';
        return false;
      }
      //Genera Código de Tramite
      $lcSql = "SELECT MAX(cCodTre) FROM B04MTRE";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      $lcCodTre = (empty($laTmp[0])) ? '000000' : $laTmp[0];
      $i = (int)$lcCodTre + 1;
      $lcCodTre = sprintf('%06d', $i);
      //Inserta Maestro de Tramites
      $lmDatos = json_encode(['CNRODIP' => $this->paData['CNRODIP']]);
      $lcSql = "INSERT INTO B04MTRE (cCodTre, cIdCate, cIdLog, tFecha, mDetall, cCCoDes, cEstado, cEstPro,cCodUsu)
                VALUES ('$lcCodTre','PDDEBA','$lcIdLog',NOW(),'$lmDatos','000','B', 'P','9999');";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
        $this->pcError = "ERROR AL GRABAR MAESTRO DE TRAMITES";
        return false;
      }
      $lcSql = "UPDATE B04MTRE SET cEtapa = 'D', tModifi = NOW()
                  WHERE cIdLog IN (SELECT cIdLog FROM B03DDEU WHERE cIdDeud = '{$lcIdDeud}')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR PAQUETE';
         return false;
      }
      $lcSql = "SELECT C.dFecha, E.cEmail
                FROM B05DUAC A
                INNER JOIN B05PUAC B ON B.cColUac = A.cColUac
                INNER JOIN B05MCOL C ON C.cIdCola = B.cIdCola
                INNER JOIN A01MALU D ON D.cCodAlu = A.cCodAlu
                INNER JOIN S01MPER E ON E.cNroDni = D.cNroDni
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}'
                ORDER BY C.dFecha DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false || $p_oSql->pnNumRow == 0) {
         $this->pcError = "NO SE PUEDEN RECUPERAR DATOS PARA ENVIAR EL CORREO AL ALUMNO";
         return false;
      }
      $laTmp = $p_oSql->fetch($R1);
      if (empty($laTmp[1])) {
         $this->pcError = "ALUMNO NO TIENE EMAIL";
         return false;
      }
      $ldFecha = $laTmp[0];
      $lcEmail = $laTmp[1];
      $loEmail = new CEmail(); 
      $llOk = $loEmail->omConnect();
      if (!$llOk) {
         $this->pcError = "ERROR CONECTANDO AL SERVIDOR DE CORREOS";
         return false;
      }
      $loDate = new CDate();
      $lcMensa = "Tu trámite de Bachiller en línea ha culminado; llegará a tu correo el diploma digital; tendrás que estar atento al comunicado oficial de las Autoridades para la entrega en físico; pasado el Estado de Emergencia. ¡Felicitaciones! 😃🎓";
      $loEmail->paData = ['CSUBJEC' => "AVISO: BACHILLERATO EN LINEA", "CBODY" => $lcMensa, "AEMAILS" => [$lcEmail]];
      $llOk = $loEmail->omSendBachillerato();
      if (!$llOk) {
         $this->pcError = "ERROR ENVIANDO CORREO DE AVISO";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualizar etapa del paquete por la revision de la ORAA
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRevisarPaqueteORAATitulacion() {
      $llOk = $this->mxValParamRevisarPaqueteORAATitulacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRevisarPaqueteORAATitulacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarPaqueteORAATitulacion() {
      if (!isset($this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DEL ALUMNO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRevisarPaqueteORAATitulacion($p_oSql) {
      $lcCodAlu = $this->paData['CCODALU'];
      $lcSql = "SELECT A.cIdDeud FROM B03DDEU A
                  INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                  WHERE A.cCodAlu = '$lcCodAlu' AND B.cPaquet = 'T' AND B.cEstado = 'C'
                  ORDER BY B.tModifi DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO HAY PAQUETE PENDIENTE";
         return false;
      }
      $lcIdDeud = $laFila[0];
      $lcSql = "UPDATE B04MTRE SET cEtapa = 'C', tModifi = NOW()
                  WHERE cIdLog IN (SELECT cIdLog FROM B03DDEU WHERE cIdDeud = '{$lcIdDeud}')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR PAQUETE';
         return false;
      }
      $lcSql = "SELECT C.dFecha
                FROM B05DUAC A
                INNER JOIN B05PUAC B ON B.cColUac = A.cColUac
                INNER JOIN B05MCOL C ON C.cIdCola = B.cIdCola
                WHERE A.cCodAlu = '{$this->paData['CCODALU']}'
                ORDER BY C.dFecha DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "NO SE PUEDEN RECUPERAR DATOS PARA ACTUALIZAR COLACION";
         return false;
      }
      $ldFecha = $laTmp[0];
      $lcSql = "SELECT cEmail FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
        $this->pcError = "NO SE PUEDEN RECUPERAR DATOS DEL ALUMNO PARA CORREO";
        return false;
     }
      $lcEmail = $laTmp[0];
      $lcSql = "UPDATE B05DUAC SET cEstado = 'I' WHERE CCODALU = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if (!$llOk) {
        $this->pcError = 'ERROR AL ACTUALIZAR DETALLE DE LA COLACION';
        return false;
      }
      $loEmail = new CEmail();
      $llOk = $loEmail->omConnect();
      if (!$llOk) {
         $this->pcError = "ERROR CONECTANDO AL SERVIDOR DE CORREOS";
         return false;
      }
      $loDate = new CDate();
      $lcMensa = "Tu trámite de Titulación en línea ha culminado; llegará a tu correo el diploma digital; tendrás que estar atento al comunicado oficial de las Autoridades para la entrega en físico; pasado el Estado de Emergencia. ¡Felicitaciones! 😃🎓";
      $loEmail->paData = ['CSUBJEC' => "AVISO: TITULACION EN LINEA", "CBODY" => $lcMensa, "AEMAILS" => [$lcEmail]];
      $llOk = $loEmail->omSendTitulacion();
      if (!$llOk) {
         $this->pcError = "ERROR ENVIANDO CORREO DE AVISO";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualizar etapa del paquete por la revision de la ORAA
   // ------------------------------------------------------------------------------
   public function omRevisarPaqueteORAAMaestria() {
    $llOk = $this->mxValParamRevisarPaqueteORAAMaestria();
    if (!$llOk) {
       return false;
    }
    $loSql = new CSql();
    $llOk = $loSql->omConnect();
    if (!$llOk) {
       $this->pcError = $loSql->pcError;
       return false;
    }
    $llOk = $this->mxRevisarPaqueteORAAMaestria($loSql);
    if (!$llOk) {
       $loSql->rollback();
    }
    $loSql->omDisconnect();
    return $llOk;
  }
 
  protected function mxValParamRevisarPaqueteORAAMaestria() {
     if (!isset($this->paData['CCODALU'])) {
        $this->pcError = "CODIGO DEL ALUMNO NO DEFINIDO";
        return false;
     }
     return true;
  }
 
  protected function mxRevisarPaqueteORAAMaestria($p_oSql) {
     $lcCodAlu = $this->paData['CCODALU'];
     $lcSql = "SELECT A.cIdDeud FROM B03DDEU A
                 INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                 WHERE A.cCodAlu = '$lcCodAlu' AND B.cPaquet = 'M' AND B.cEstado = 'C'
                 ORDER BY B.tModifi DESC";
     $R1 = $p_oSql->omExec($lcSql);
     $laFila = $p_oSql->fetch($R1);
     if (!isset($laFila[0])) {
        $this->pcError = "NO HAY PAQUETE PENDIENTE";
        return false;
     }
     $lcIdDeud = $laFila[0];
     $lcSql = "UPDATE B04MTRE SET cEtapa = 'C', tModifi = NOW()
                 WHERE cIdLog IN (SELECT cIdLog FROM B03DDEU WHERE cIdDeud = '{$lcIdDeud}')";
     $llOk = $p_oSql->omExec($lcSql);
     if (!$llOk) {
        $this->pcError = 'ERROR AL ACTUALIZAR PAQUETE';
        return false;
     }
     $lcSql = "SELECT C.dFecha, E.cEmail
               FROM B05DUAC A
               INNER JOIN B05PUAC B ON B.cColUac = A.cColUac
               INNER JOIN B05MCOL C ON C.cIdCola = B.cIdCola
               INNER JOIN A01MALU D ON D.cCodAlu = A.cCodAlu
               INNER JOIN S01MPER E ON E.cNroDni = D.cNroDni
               WHERE A.cCodAlu = '{$this->paData['CCODALU']}'
               ORDER BY C.dFecha DESC LIMIT 1";
     $R1 = $p_oSql->omExec($lcSql);
     $laTmp = $p_oSql->fetch($R1);
     if (!isset($laTmp[0])) {
        $this->pcError = "NO SE PUEDEN RECUPERAR DATOS PARA ENVIAR EL CORREO AL ALUMNO";
        return false;
     }
     $ldFecha = $laTmp[0];
     $lcEmail = $laTmp[1];
     $lcSql = "UPDATE B05DUAC SET cEstado = 'I' WHERE CCODALU = '{$this->paData['CCODALU']}'";
     $R1 = $p_oSql->omExec($lcSql);
     if (!$llOk) {
       $this->pcError = 'ERROR AL ACTUALIZAR DETALLE DE LA COLACION';
       return false;
     }
     /*$loEmail = new CEmail();
     $llOk = $loEmail->omConnect();
     if (!$llOk) {
        $this->pcError = "ERROR CONECTANDO AL SERVIDOR DE CORREOS";
        return false;
     }
     $loDate = new CDate();
     $lcMensa = "Tu trámite de Maestria en línea ha culminado. Tu fecha de colación se publicara en el Facebook de la Universidad, Atento!!". "Felicitaciones! 😃🎓";
     $loEmail->paData = ['CSUBJEC' => "AVISO: TITULACION EN LINEA", "CBODY" => $lcMensa, "AEMAILS" => [$lcEmail]];
     $llOk = $loEmail->omSendTitulacion();
     if (!$llOk) {
        $this->pcError = "ERROR ENVIANDO CORREO DE AVISO";
        return false;
     }*/
     return true;
  }
  
  // ------------------------------------------------------------------------------
  // Actualizar etapa del paquete por la revision de la ORAA
  // ------------------------------------------------------------------------------
  public function omRevisarPaqueteORAADoctorado() {
    $llOk = $this->mxValParamRevisarPaqueteORAADoctorado();
    if (!$llOk) {
       return false;
    }
    $loSql = new CSql();
    $llOk = $loSql->omConnect();
    if (!$llOk) {
       $this->pcError = $loSql->pcError;
       return false;
    }
    $llOk = $this->mxRevisarPaqueteORAADoctorado($loSql);
    if (!$llOk) {
       $loSql->rollback();
    }
    $loSql->omDisconnect();
    return $llOk;
  }
 
  protected function mxValParamRevisarPaqueteORAADoctorado() {
     if (!isset($this->paData['CCODALU'])) {
        $this->pcError = "CODIGO DEL ALUMNO NO DEFINIDO";
        return false;
     }
     return true;
  }
 
  protected function mxRevisarPaqueteORAADoctorado($p_oSql) {
     $lcCodAlu = $this->paData['CCODALU'];
     $lcSql = "SELECT A.cIdDeud FROM B03DDEU A
                 INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                 WHERE A.cCodAlu = '$lcCodAlu' AND B.cPaquet = 'D' AND B.cEstado = 'C'
                 ORDER BY B.tModifi DESC";
     $R1 = $p_oSql->omExec($lcSql);
     $laFila = $p_oSql->fetch($R1);
     if (!isset($laFila[0])) {
        $this->pcError = "NO HAY PAQUETE PENDIENTE";
        return false;
     }
     $lcIdDeud = $laFila[0];
     $lcSql = "UPDATE B04MTRE SET cEtapa = 'C', tModifi = NOW()
                 WHERE cIdLog IN (SELECT cIdLog FROM B03DDEU WHERE cIdDeud = '{$lcIdDeud}')";
     $llOk = $p_oSql->omExec($lcSql);
     if (!$llOk) {
        $this->pcError = 'ERROR AL ACTUALIZAR PAQUETE';
        return false;
     }
     $lcSql = "SELECT C.dFecha, E.cEmail
               FROM B05DUAC A
               INNER JOIN B05PUAC B ON B.cColUac = A.cColUac
               INNER JOIN B05MCOL C ON C.cIdCola = B.cIdCola
               INNER JOIN A01MALU D ON D.cCodAlu = A.cCodAlu
               INNER JOIN S01MPER E ON E.cNroDni = D.cNroDni
               WHERE A.cCodAlu = '{$this->paData['CCODALU']}'
               ORDER BY C.dFecha DESC LIMIT 1";
     $R1 = $p_oSql->omExec($lcSql);
     $laTmp = $p_oSql->fetch($R1);
     if (!isset($laTmp[0])) {
        $this->pcError = "NO SE PUEDEN RECUPERAR DATOS PARA ENVIAR EL CORREO AL ALUMNO";
        return false;
     }
     $ldFecha = $laTmp[0];
     $lcEmail = $laTmp[1];
     $lcSql = "UPDATE B05DUAC SET cEstado = 'I' WHERE CCODALU = '{$this->paData['CCODALU']}'";
     $R1 = $p_oSql->omExec($lcSql);
     if (!$llOk) {
       $this->pcError = 'ERROR AL ACTUALIZAR DETALLE DE LA COLACION';
       return false;
     }
     /*$loEmail = new CEmail();
     $llOk = $loEmail->omConnect();
     if (!$llOk) {
        $this->pcError = "ERROR CONECTANDO AL SERVIDOR DE CORREOS";
        return false;
     }
     $loDate = new CDate();
     $lcMensa = "Tu trámite de Maestria en línea ha culminado. Tu fecha de colación se publicara en el Facebook de la Universidad, Atento!!". "Felicitaciones! 😃🎓";
     $loEmail->paData = ['CSUBJEC' => "AVISO: TITULACION EN LINEA", "CBODY" => $lcMensa, "AEMAILS" => [$lcEmail]];
     $llOk = $loEmail->omSendTitulacion();
     if (!$llOk) {
        $this->pcError = "ERROR ENVIANDO CORREO DE AVISO";
        return false;
     }*/
     return true;
  }

   // ------------------------------------------------------------------------------
   // Cargar solicitudes de descuento para bachiller activas
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omCargarSolicitudesBachiller() {
      /*$llOk = $this->mxValParamCargarSolicitudesBachiller();
      if (!$llOk) {
         return false;
      }*/
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarSolicitudesBachiller($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCargarSolicitudesBachiller() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxCargarSolicitudesBachiller($p_oSql) {
      $lcSql = "SELECT cTipAut, cDescri FROM B01TAUT WHERE cEstado = 'A' AND cTipAut = '001'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CTIPAUT' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES EXISTENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Grabar solicitud de descuento para bachiller
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarSolicitudBachiller() {
      $llOk = $this->mxValParamGrabarSolicitudBachiller();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitudBachiller($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarSolicitudBachiller() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CTIPAUT'])) {
         $this->pcError = "AUTORIZACION NO DEFINIDA";
         return false;
      }
      return true;
   }

   protected function mxGrabarSolicitudBachiller($p_oSql) {
      $lcCodAlu = $this->paData['CCODALU'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcTipAut = $this->paData['CTIPAUT'];
      $lcSql = "SELECT CNRODNI FROM A01MALU WHERE CCODALU = '$lcCodAlu'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE ALUMNO ERRONEO";
         return false;
      }
      $lcSql = "SELECT * FROM B01DAUT WHERE CCODUSU = '$lcCodUsu' AND CCODALU = '$lcCodAlu' AND CESTADO = 'E'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "PRESENTA SOLICITUD PENDIENTE";
         return false;
      }
      $lcSql = "INSERT INTO B01DAUT (cCodAlu, cCodUsu, cEstado, cTipAut, cCodEmp, cUsuCod)
                  VALUES('$lcCodAlu', '$lcCodUsu', 'E', '$lcTipAut', '1015', '9999')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL MANDAR SOLICITUD';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Verifica alumno por codigo ingresado
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omVerificarAlumnoSolicitud() {
      $llOk = $this->mxValParamVerificarAlumnoSolicitud();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerificarAlumnoSolicitud($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamVerificarAlumnoSolicitud() {
      if ((!isset($this->paData['CCODALU'])) || (!preg_match('(^[0-9]{10}$)', $this->paData['CCODALU']))) {
         $this->pcError = "CODIGO DE ALUMNO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxVerificarAlumnoSolicitud($p_oSql) {
      $lcSql = "SELECT CNOMBRE FROM V_A01MALU WHERE CCODALU = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE ALUMNO ERRONEO";
         return false;
      }
      $this->paData = ['CNOMBRE' => $laFila[0]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar bandeja de solicitudes para la revision de estados
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omRevisarEstadoSolicitud() {
      $llOk = $this->mxValParamRevisarEstadoSolicitud();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRevisarEstadoSolicitud($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRevisarEstadoSolicitud() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRevisarEstadoSolicitud($p_oSql) {
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcSql = "SELECT A.NSERIAL, C.CNOMBRE, E.CNOMBRE, H.CDESCRI, G.CNOMBRE, TO_CHAR(A.TMODIFI, 'yyyy-mm-dd hh24:mi'), A.CESTADO
                  FROM B01DAUT A
                  INNER JOIN S01TUSU B ON B.CCODUSU = A.CCODUSU
                  INNER JOIN S01MPER C ON C.CNRODNI = B.CNRODNI
                  INNER JOIN A01MALU D ON D.CCODALU = A.CCODALU
                  INNER JOIN S01MPER E ON E.CNRODNI = D.CNRODNI
                  INNER JOIN S01TUSU F ON F.CCODUSU = A.CCODEMP
                  INNER JOIN S01MPER G ON G.CNRODNI = F.CNRODNI
                  INNER JOIN B01TAUT H ON H.CTIPAUT = A.CTIPAUT
                  WHERE A.CCODUSU = '$lcCodUsu'";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['NSERIAL' => $laTmp[0], 'CNOMSOL' => $laTmp[1], 'CNOMALU' => $laTmp[2], 'CDESCRI' => $laTmp[3],'CNOMREV' => $laTmp[4], 'CMODIFI'=>$laTmp[5], 'CESTADO' => $laTmp[6]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY SOLICITUDES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de autorizaciones - cargar autorizaciones
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omCargarAutorizaciones() {
      /*$llOk = $this->mxValParamCargarAutorizaciones();
      if (!$llOk) {
         return false;
      }*/
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarAutorizaciones($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCargarAutorizaciones() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxCargarAutorizaciones($p_oSql) {
      $lcSql = "SELECT CIDCATE, CDESCRI FROM B03TDOC WHERE CESTADO = 'A' ORDER BY CIDCATE ASC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDocs = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDocs[] = ['CIDCATE' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      if (count($this->paDocs) == 0) {
         $this->pcError = "NO HAY DOCUMENTOS";
         return false;
      }
      $lcSql = "SELECT A.CCODAUT, A.CIDCATE, B.CDESCRI,A.CCODUSU, D.CNOMBRE, A.CESTADO, TO_CHAR(A.TMODIFI, 'yyyy-mm-dd hh24:mi')
                  FROM B04MAUT A
                  INNER JOIN B03TDOC B ON B.CIDCATE = A.CIDCATE
                  INNER JOIN S01TUSU C ON C.CCODUSU = A.CCODUSU
                  INNER JOIN S01MPER D ON D.CNRODNI = C.CNRODNI
                  ORDER BY CCODAUT DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODAUT' => $laFila[0], 'CIDCATE' => $laFila[1], 'CDESCRI' => $laFila[2], 'CCODUSU' => $laFila[3], 'CNOMBRE' => $laFila[4], 'CESTADO' => $laFila[5], 'TMODIFI' => $laFila[6]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de autorizaciones - grabar nueva autorización
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarAutorizacion() {
      $llOk = $this->mxValParamGrabarAutorizacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarAutorizacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarAutorizacion() {
      if (!isset($this->paData['CCODAUT'])) {
         $this->pcError = "CODIGO DE AUTORIZACION NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CIDCATE'])) {
         $this->pcError = "CODIGO DE DOCUMENTO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CCODNUE'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxGrabarAutorizacion($p_oSql) {
      $lcCodAut = $this->paData['CCODAUT'];
      $lcIdCate = $this->paData['CIDCATE'];
      $lcCodUsu = $this->paData['CCODNUE'];
      $lcSql = "SELECT CCODAUT FROM B04MAUT WHERE CCODAUT = '$lcCodAut'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "CODIGO DE AUTORIZACION DUPLICADO";
         return false;
      }
      $lcSql = "SELECT CCODUSU FROM S01TUSU WHERE CCODUSU = '$lcCodUsu'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE USUARIO ERRONEO";
         return false;
      }
      $lcSql = "SELECT * FROM B04MAUT WHERE CCODUSU = '$lcCodUsu' AND CIDCATE = '$lcIdCate' AND CESTADO = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "PRESENTA AUTORIZACION ACTIVA";
         return false;
      }
      $lcSql = "INSERT INTO B04MAUT (cCodAut, cIdCate, cCodUsu, cUsuCod)
                  VALUES('$lcCodAut', '$lcIdCate', '$lcCodUsu', 'U666')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL REGISTRAR AUTORIZACION';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de autorizaciones - carga datos de la autorización a editar
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omEditarAutorizacion() {
      $llOk = $this->mxValParamEditarAutorizacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEditarAutorizacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEditarAutorizacion() {
      if (!isset($this->paData['CCOAUED'])) {
         $this->pcError = "CODIGO DE AUTORIZACION NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxEditarAutorizacion($p_oSql) {
      $lcCodAut = $this->paData['CCOAUED'];
      $lcSql = "SELECT CCODAUT, CIDCATE, CCODUSU, CESTADO FROM B04MAUT WHERE CCODAUT = '$lcCodAut'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODAUT' => $laFila[0], 'CIDCATE' => $laFila[1], 'CCODUSU' => $laFila[2], 'CESTADO' => $laFila[3]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "CODIGO DE AUTORIZACION NO EXISTE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de autorizaciones - edita datos de la autorización seleccionada
   // 2019-02-05 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omActualizarAutorizacion() {
      $llOk = $this->mxValParamActualizarAutorizacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxActualizarAutorizacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamActualizarAutorizacion() {
      if (!isset($this->paData['CCODAUT'])) {
         $this->pcError = "CODIGO DE AUTORIZACION NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CIDCATE'])) {
         $this->pcError = "CODIGO DE DOCUMENTO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CESTADO'])) {
         $this->pcError = "ESTADO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxActualizarAutorizacion($p_oSql) {
      $lcCodAut = $this->paData['CCODAUT'];
      $lcIdCate = $this->paData['CIDCATE'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcEstado = $this->paData['CESTADO'];
      $lcSql = "UPDATE B04MAUT
                  SET CIDCATE = '$lcIdCate', CCODUSU='$lcCodUsu', CESTADO = '$lcEstado'
                  WHERE CCODAUT = '$lcCodAut'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR AUTORIZACION';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento unidades académicas - Carga unidades académicas existentes
   // 2019-02-06 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omCargarUniAcas() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarUniAcas($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxCargarUniAcas($p_oSql) {
      $lcSql = "SELECT CUNIACA, CNOMUNI, CESTADO, CCODUSU, TO_CHAR(TMODIFI, 'YYYY-MM-DD HH24:MI')
                  FROM S01TUAC WHERE CESTADO <> 'X' ORDER BY CNOMUNI ASC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CUNIACA' => $laFila[0], 'CNOMUNI' => $laFila[1], 'CESTADO' => $laFila[2], 'CCODUSU' => $laFila[3], 'TMODIFI' => $laFila[4]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento unidades académicas - Editar unidades académicas existentes
   // 2019-02-06 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarUniAca() {
      $llOk = $this->mxValParamGrabarUniAca();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarUniAca($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarUniAca() {
      if (!isset($this->paData['CUNIACA'])) {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CNOMUNI'])) {
         $this->pcError = "NOMBRE NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxGrabarUniAca($p_oSql) {
      $lcUniAca = $this->paData['CUNIACA'];
      $lcNomUni = $this->paData['CNOMUNI'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcSql = "SELECT CUNIACA FROM S01TUAC WHERE CUNIACA = '$lcUniAca'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "UNIDAD ACADÉMICA DUPLICADA";
         return false;
      }
      $lcSql = "INSERT INTO S01TUAC (cUniAca, cNomUni, cEstado, cCodUsu)
                VALUES('$lcUniAca', '$lcNomUni', 'A', '$lcCodUsu')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL REGISTRAR UNIDAD ACADÉMICA';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento unidades académicas - Editar unidades académicas
   // 2019-02-06 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omEditarUniAca() {
      $llOk = $this->mxValParamEditarUniAca();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEditarUniAca($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEditarUniAca() {
      if (!isset($this->paData['CUNACED'])) {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDA";
         return false;
      }
      return true;
   }

   protected function mxEditarUniAca($p_oSql) {
      $lcUniAca = $this->paData['CUNACED'];
      $lcSql = "SELECT CUNIACA, CNOMUNI, CCODUSU, CESTADO FROM S01TUAC WHERE CUNIACA = '$lcUniAca'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CUNIACA' => $laFila[0], 'CNOMUNI' => $laFila[1], 'CCODUSU' => $laFila[2], 'CESTADO' => $laFila[3]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "UNIDAD ACADEMICA NO EXISTE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de unidades académicas - actualizar unidad académica
   // 2019-02-06 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omActualizarUniAca() {
      $llOk = $this->mxValParamActualizarUniAca();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxActualizarUniAca($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamActualizarUniAca() {
      if (!isset($this->paData['CUNIACA'])) {
         $this->pcError = "UNIDAD ACADÉMICA NO DEFINIDA";
         return false;
      }
      if (!isset($this->paData['CNOMUNI'])) {
         $this->pcError = "NOMBRE NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CESTADO'])) {
         $this->pcError = "ESTADO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxActualizarUniAca($p_oSql) {
      $lcUniAca = $this->paData['CUNIACA'];
      $lcNomUni = $this->paData['CNOMUNI'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcEstado = $this->paData['CESTADO'];
      $lcSql = "UPDATE S01TUAC
                  SET CNOMUNI = '$lcNomUni', CCODUSU='$lcCodUsu', CESTADO = '$lcEstado', TMODIFI = NOW()
                  WHERE CUNIACA = '$lcUniAca'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR UNIDAD ACADÉMICA';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Carga ultima colacion registrada
   // 2019-02-25 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omCargarUltimaColacion() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarUltimaColacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxCargarUltimaColacion($p_oSql) {
      $lcSql = "SELECT cIdCola, cPeriod, cDescri, dFecha FROM B05MCOL
                  WHERE cEstado = 'A' ORDER BY dFecha DESC, cIdCola DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->paData = ['CIDCOLA' => $laFila[0], 'CPERIOD' => $laFila[1], 'CDESCRI' => $laFila[2], 'DFECHA' => $laFila[3]];
      }
      else {
         $this->paData = ['CIDCOLA' => '00000', 'CPERIOD' => '2019011', 'CDESCRI' => '', 'DFECHA' => ''];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Crea registro de la nueva colacion
   // 2019-04-25 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGenerarColacion() {
      $llOk = $this->mxValParamGenerarColacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGenerarColacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGenerarColacion() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['DFECHA'])) {
         $this->pcError = "FECHA NO DEFINIDA";
         return false;
      } elseif (!isset($this->paData['CTIPCOL']) || strlen($this->paData['CTIPCOL']) != 1) {
         $this->pcError = "TIPO DE COLACION NO DEFINIDA O NO VALIDA";
         return false;
      }
      return true;
   }

   protected function mxGenerarColacion($p_oSql) {
      $loDate = new CDate();
      if($this->paData['CTIPCOL'] == 'T'){
         $cDesCol  = 'TITULACION'; 
      } elseif ($this->paData['CTIPCOL'] == 'D') {
        $cDesCol  = 'DOCTORADO';
      } elseif ($this->paData['CTIPCOL'] == 'M') {
        $cDesCol  = 'MAESTRIA';
      } else {
        $cDesCol  = 'BACHILLER';
      }
      $this->paData['CDESCRI'] = "COLACIÓN DE ".$cDesCol." ".strtoupper($loDate->dateSimpleText($this->paData['DFECHA']));
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_B05MCOL_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "ERROR EN PROCESAR TRANSACCION";
         return false;
      }
      $loJson = json_decode($laFila[0], true);
      if (isset($loJson['ERROR'])) {
         $this->pcError = $loJson['ERROR'];
         return false;
      }
      return true; 
   }

   // ------------------------------------------------------------------------------
   // Cargar colaciones por tipo y usuario
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omCargarColacionesPorUsuario() {
      $llOk = $this->mxValParamCargarColacionesPorUsuario();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarColacionesPorUsuario($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCargarColacionesPorUsuario() {
      if (!isset($this->paData['CUNIACA']) || strlen($this->paData['CUNIACA']) != 2) {
         $this->pcError = "UNIDAD ACADEMICA NO VALIDA O NO DEFINIDA";
         return false;
      } elseif (!isset($this->paData['CTIPCOL']) || strlen($this->paData['CTIPCOL']) != 1) {
         $this->pcError = "UNIDAD TIPO DE COLACION NO VALIDA O NO DEFINIDA";
         return false;
      }
      return true;
   }

   protected function mxCargarColacionesPorUsuario($p_oSql) {
      $lcSql = "SELECT B.cDescri, B.dFecha, A.cColUac, C.cDescri, C.cUniAca
                FROM B05PUAC A
                INNER JOIN B05MCOL B ON B.cIdCola = A.cIdCola
                INNER JOIN S01TCCO C ON C.cUniAca = A.cUniAca
                INNER JOIN B03PUSU D ON D.cCenCos = C.cCenCos
                WHERE D.cCodUsu = '{$this->paData['CCODUSU']}' AND D.cEstado = 'A' AND B.cTipCol = '{$this->paData['CCODIGO']}'
                ORDER BY dFecha DESC";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CDESCRI' => $laFila[0], 'DFECHA' => $laFila[1], 'CCOLUAC' => $laFila[2],
                             'CNOMUNI' => str_replace('E.P.','',$laFila[3]), 'CUNIACA' => $laFila[4]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN COLACIONES REGISTRADAS";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de colaciones - cargar colaciones
   // 2019-02-25 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omCargarColaciones() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarColaciones($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxCargarColaciones($p_oSql) {
      $lcSql = "SELECT cIdCola, cPeriod, cDescri, cEstado, dFecha FROM B05MCOL
                  WHERE cEstado != 'X' ORDER BY cIdCola DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDCOLA' => $laFila[0], 'CPERIOD' => $laFila[1], 'CDESCRI' => $laFila[2], 'CESTADO' => $laFila[3], 'DFECHA' => $laFila[4]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de colaciones - grabar nueva colación
   // 2019-02-25 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarColacion() {
      $llOk = $this->mxValParamGrabarColacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarColacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarColacion() {
      if (!isset($this->paData['CIDCOLA'])) {
         $this->pcError = "CODIGO DE COLACION NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CPERIOD'])) {
         $this->pcError = "PERIODO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CDESCRI'])) {
         $this->pcError = "DESCRIPCION NO DEFINIDA";
         return false;
      }
      if (!isset($this->paData['DFECHA'])) {
         $this->pcError = "FECHA NO DEFINIDA";
         return false;
      }
      return true;
   }

   protected function mxGrabarColacion($p_oSql) {
      $lcIdCola = $this->paData['CIDCOLA'];
      $lcPeriod = $this->paData['CPERIOD'];
      $lcDescri = $this->paData['CDESCRI'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $ldFecha = $this->paData['DFECHA'];
      // VERIFICAR SI YA EXISTE ID COLACION
      $lcSql = "SELECT cIdCola FROM B05MCOL WHERE cIdCola = '$lcIdCola'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "CODIGO DE COLACION DUPLICADO";
         return false;
      }
      // VERIFICAR SI YA EXISTE PERIODO
      $lcSql = "SELECT cPeriod FROM B05MCOL WHERE cPeriod = '$lcPeriod'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "PERIODO DUPLICADO";
         return false;
      }
      $lcSql = "INSERT INTO B05MCOL (cIdCola, cPeriod, cDescri, cUsuCod, dFecha)
                  VALUES('$lcIdCola', '$lcPeriod', '$lcDescri', '$lcCodUsu', '$ldFecha')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL REGISTRAR COLACION';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de colaciones - carga datos de la colacion a editar
   // 2019-02-25 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omEditarColacion() {
      $llOk = $this->mxValParamEditarColacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEditarColacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEditarColacion() {
      if (!isset($this->paData['CIDCOED'])) {
         $this->pcError = "CODIGO DE COLACION NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxEditarColacion($p_oSql) {
      $lcIdCola = $this->paData['CIDCOED'];
      $lcSql = "SELECT CIDCOLA, CPERIOD, CDESCRI, DFECHA, CUSUCOD, CESTADO FROM B05MCOL WHERE CIDCOLA = '$lcIdCola'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDCOLA' => $laFila[0], 'CPERIOD' => $laFila[1], 'CDESCRI' => $laFila[2], 'DFECHA' => $laFila[3], 'CCODUSU' => $laFila[4], 'CESTADO' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "CODIGO DE COLACION NO EXISTE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de colaciones - edita datos de la colacion seleccionada
   // 2019-02-25 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omActualizarColacion() {
      $llOk = $this->mxValParamActualizarColacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxActualizarColacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamActualizarColacion() {
      if (!isset($this->paData['CIDCOLA'])) {
         $this->pcError = "CODIGO DE COLACION NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CPERIOD'])) {
         $this->pcError = "PERIODO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CDESCRI'])) {
         $this->pcError = "DESCRIPCION NO DEFINIDA";
         return false;
      }
      if (!isset($this->paData['DFECHA'])) {
         $this->pcError = "FECHA NO DEFINIDA";
         return false;
      }
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CESTADO'])) {
         $this->pcError = "ESTADO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxActualizarColacion($p_oSql) {
      $lcIdCola = $this->paData['CIDCOLA'];
      $lcPeriod = $this->paData['CPERIOD'];
      $lcDescri = $this->paData['CDESCRI'];
      $ldFecha = $this->paData['DFECHA'];
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcEstado = $this->paData['CESTADO'];
      $lcSql = "UPDATE B05MCOL
                  SET CPERIOD = '$lcPeriod', CDESCRI = '$lcDescri', DFECHA = '$ldFecha', CUSUCOD='$lcCodUsu', CESTADO = '$lcEstado'
                  WHERE CIDCOLA = '$lcIdCola'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR COLACION';
         return false;
      }
      return true;
   }

   // ---------------------------------------------------------------------------
   // Reporte cargo colacion
   // 2019-06-03 LVA Creacion
   // ---------------------------------------------------------------------------
   public function omReporteCargoColacion() {
      $llOk = $this->mxValParamReporteCargoColacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxReporteCargoColacion($loSql);
      if (!$llOk) {
         return false;
      }
      $llOk = $this->mxPrintReporteCargoColacion();
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamReporteCargoColacion() {
      if (!isset($this->paData['CIDCOLA']) || !preg_match('(^[0-9]{5}$)', $this->paData['CIDCOLA'])) {
         $this->pcError = "COLACION NO DEFINIDA O NO VALIDA";
         return false;
      }
      return true;
   }

   protected function mxReporteCargoColacion($p_oSql) {
      if($this->paData['CCODIGO'] == 'B') {
         $lcWhere = 'D';
      } elseif ($this->paData['CCODIGO'] == 'T'){
         $lcWhere = 'C';
      }
      if($this->paData['CCODIGO'] == 'T'){
         $lcSql = "SELECT B.cCodAlu, F.cNombre, E.cNomUni, TO_CHAR(A.dRecepc, 'YYYY-MM-DD HH24:MI') as dRecepc, A.cNroDni
                   FROM B03MDEU A
                   INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                   INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
                   INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
                   INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni
                   INNER JOIN B05DUAC G ON G.cCodAlu = D.cCodAlu
                   INNER JOIN B05PUAC H ON H.cColUac = G.cColUac
                   WHERE A.cPaquet = '{$this->paData['CCODIGO']}' AND C.cEtapa = '$lcWhere' AND E.cUniAca NOT IN ('20', '21') AND cIdCola = '{$this->paData['CIDCOLA']}' AND C.cEstado IN ('B', 'M', 'S')
                   GROUP BY B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc, A.cNroDni, A.cIdDeud ORDER BY D.cUniAca, F.cNombre";
      } else {
         $lcSql = "SELECT B.cCodAlu, F.cNombre, E.cNomUni, TO_CHAR(A.dRecepc, 'YYYY-MM-DD HH24:MI') as dRecepc, A.cNroDni
                   FROM B03MDEU A
                   INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                   INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
                   INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
                   INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni
                   INNER JOIN B05DUAC G ON G.cCodAlu = D.cCodAlu
                   INNER JOIN B05PUAC H ON H.cColUac = G.cColUac
				           INNER JOIN S01TCCO I ON I.cUniAca = E.cUniAca
                   WHERE A.cPaquet = '{$this->paData['CCODIGO']}' AND C.cEtapa = '$lcWhere' AND E.cUniAca NOT IN ('20', '21') AND cIdCola = '{$this->paData['CIDCOLA']}'  AND I.cEstPre = '{$this->paData['CESTPRE']}' AND C.cEstado IN ('B', 'M', 'S')
                   GROUP BY B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc, A.cNroDni, A.cIdDeud ORDER BY D.cUniAca, F.cNombre";

      }
      $R1 = $p_oSql->omExec($lcSql);
      $this->pcDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CNOMUNI' => $laFila[2], 'DRECEPC' => $laFila[3],
                             'CNRODNI' => $laFila[4]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN ALUMNOS EN LA COLACION SELECCIONADA";
         return false;
      }
      return true;
   }

   protected function mxPrintReporteCargoColacion() {
      try {
         $fecha_actual = date ("Y-m-d");
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->SetFont('Courier','B',15);
         $pdf->Cell(186, 10, utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->Cell(186, 10, utf8_decode('OFICINA DE REGISTRO Y ARCHIVO ACADÉMICO'), 0, 0, 'C');
         $pdf->Ln(15);
         $pdf->SetFont('Courier', 'B', 10);
         $pdf->SetX(19);
         $pdf->Cell(25, 8, utf8_decode("CÓD. ALUMNO"), 1, 0);
         $pdf->Cell(20, 8, utf8_decode("DNI"), 1, 0);
         $pdf->Cell(62, 8, utf8_decode("NOMBRE DE ALUMNO"), 1, 0);
         $pdf->Cell(46, 8, utf8_decode("ESCUELA PROFESIONAL"), 1, 0);
         $pdf->Cell(20, 8, utf8_decode("FIRMA"), 1, 1);
         $pdf->SetFont('Courier', '', 10);
         foreach ($this->paDatos as $laFila) {
            $pdf->SetX(19);
            $pdf->Cell(25, 8, utf8_decode($laFila['CCODALU']), 1, 0);
            $pdf->Cell(20, 8, utf8_decode($laFila['CNRODNI']), 1, 0);
            $pdf->Cell(62, 8, utf8_decode(substr(str_replace('/', ' ', $laFila['CNOMBRE']), 0, 28)), 1, 0);
            $pdf->Cell(46, 8, utf8_decode(substr($laFila['CNOMUNI'], 0, 21)), 1, 0);
            $pdf->Cell(20, 8, '', 1, 1);
         }
         $pdf->Ln(15);
         $pdf->Cell(106);
         $pdf->SetFont('Courier', '', 10);
         $pdf->Cell(20,6,utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'));
         $llOk = $pdf->Output($this->pcFile, "F");         
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // ---------------------------------------------------------------------------
   // Reporte de trámites finalizados
   // 2019-02-19 JABC Creacion
   // ---------------------------------------------------------------------------
   public function omReporteTramitesFinalizados() {
      $llOk = $this->mxValParamReporteTramitesFinalizados();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk  = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxReporteTramitesFinalizados($loSql);
      $loSql->omDisconnect();
      if (!$llOk) {
         return false;
      }
      $llOk = $this->mxPrintTramitesFinalizados();
      return $llOk;
   }

   protected function mxValParamReporteTramitesFinalizados() {
      $loDate = new CDate();
      if (!isset($this->paData['CIDCATE']) or (strlen($this->paData['CIDCATE']) != 6)) {
         $this->pcError = 'PARAMETRO CATEGORIA DE DOCUMENTO INVALIDA O NO DEFINIDA';
         return false;
      } elseif (!$loDate->mxValDate($this->paData['DINICIO'])) {
         $this->pcError = 'PARAMETRO FECHA INICIAL INVALIDA O NO DEFINIDO';
         return false;
      } elseif (!$loDate->mxValDate($this->paData['DFINALI'])) {
         $this->pcError = 'PARAMETRO FECHA FINAL INVALIDA O NO DEFINIDO';
         return false;
      } elseif ($this->paData['DFINALI'] < $this->paData['DINICIO']) {
         $this->pcError = 'PARAMETRO FECHA FINAL ES MENOR QUE FECHA DE INICIO';
         return false;
      }
      return true;
   }

   protected function mxReporteTramitesFinalizados($p_oSql) {
      $this->laData = $this->paData;
      $lcSql = "SELECT cDescri FROM B03TDOC WHERE cIdCate = '{$this->laData['CIDCATE']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = 'CATEGORIA DE DOCUMENTO NO EXISTE';
         return false;
      }
      $this->laData['CDESCRI'] = $laTmp[0];
      // Deudas provisionales pagadas
      $lcSql  = "SELECT cNroDni, cNombre, TO_CHAR(dFecGen, 'YYYY-MM-DD'), TO_CHAR(dFecPag, 'YYYY-MM-DD'), nCosto, cNroPag, cCodAlu
                    FROM V_B03DDEU_4
                    WHERE cIdCate = '{$this->laData['CIDCATE']}' AND cEstMde IN ('B', 'C') AND dFecPag BETWEEN '{$this->laData['DINICIO']}' AND '{$this->laData['DFINALI']}'
                    ORDER BY dFecPag, cNombre";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->laDatos[] = ['CNRODNI' => $laTmp[0], 'CNOMBRE' => $laTmp[1], 'DFECGEN' => $laTmp[2], 'DFECPAG' => $laTmp[3],
                             'NCOSTO'  => $laTmp[4], 'CNROPAG' => $laTmp[5], 'CESTADO' => 'PAGADOS', 'CCODALU' => $laTmp[6]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "UNIDAD ACADEMICA NO EXISTE";
         return false;
      }
      return true;
   }

   protected function mxPrintTramitesFinalizados() {
      $laDatos = [];
      $i = 0;
      $lcEstado = '*';
      $lnCosTot = 0.00;
      foreach ($this->laDatos as $laTmp) {
         if ($lcEstado != $laTmp['CESTADO']) {
            if ($lcEstado != '*') {
               $laDatos[] = ['* SUBTOTAL:'.fxStringFixed(' ', 76).fxNumber($lnCosTot, 12, 2)];
               $lnCosTot = 0.00;
            }
            $laDatos[] = ['* '.$laTmp['CESTADO']];
            $lcEstado = $laTmp['CESTADO'];
            $i = 0;
         }
         $i++;
         $laDatos[] = [fxNumber($i, 3, 0).' '.$laTmp['CNRODNI'].' '.fxStringFixed($laTmp['CNOMBRE'], 40).' '.fxStringFixed($laTmp['CNROPAG'], 10).' '.fxString($laTmp['DFECGEN'], 10).' '.fxString($laTmp['DFECPAG'], 10).' '.fxNumber($laTmp['NCOSTO'], 12, 2)];
         $lnCosTot += $laTmp['NCOSTO'];
      }
      $laDatos[] = ['* SUBTOTAL:'.fxStringFixed(' ', 76).fxNumber($lnCosTot, 12, 2)];
      //
      $lcDesTit = $this->laData['CIDCATE'].' - '.$this->laData['CDESCRI'];
      $lcFechas = 'DEL: '.$this->laData['DINICIO'].' AL: '.$this->laData['DFINALI'];
      $ldDate = date('Y-m-d', time());
      $loPdf = new FPDF('portrait','cm','A4');
      $loPdf->SetMargins(1.3, 1.6, 1.3);
      $loPdf->AddPage();
      $lnPag = 0;
      $lnRow = 0;
      $lnWidth = 0;
      $lnHeight = 0.4;
      $llTitulo = true;
      foreach ($laDatos as $laFila) {
         if ($llTitulo) {
            if ($lnPag > 0) {
               $loPdf->AddPage();
            }
            $lnPag++;
            $loPdf->SetFont('Courier', 'B' , 10);
            $loPdf->Cell($lnWidth, $lnHeight, utf8_decode('UCSM-ERP '.fxStringCenter('REPORTE DE PAGOS VARIOS', 66).' PAG: ' . fxNumber($lnPag, 5, 0)), 0, 2, 'L');
            $loPdf->Cell($lnWidth, $lnHeight, utf8_decode('TDO2280  '.fxStringCenter($lcDesTit, 66).' '.$ldDate), 0, 2, 'L');
            $loPdf->Cell($lnWidth, $lnHeight, utf8_decode(fxStringCenter($lcFechas, 84)), 0, 2, 'L');
            $loPdf->SetFont('Courier', 'B' , 8);
            $loPdf->Cell($lnWidth, $lnHeight,             '------------------------------------------------------------------------------------------------------------', 0, 2, 'L');
            $loPdf->Cell($lnWidth, $lnHeight, utf8_decode(' #    DNI    NOMBRE                                    NRO.PAGO  F.GENERAC.  F.PAGO           COSTO '), 0, 2, 'L');
            $loPdf->Cell($lnWidth, $lnHeight,             '------------------------------------------------------------------------------------------------------------', 0, 2, 'L');
         }
         $loPdf->SetFont('Courier', '' , 8);
         if (substr($laFila[0], 0, 1) == '*') {
            $loPdf->SetFont('Courier', 'B' , 8);
         }
         $loPdf->Cell($lnWidth, $lnHeight, utf8_decode($laFila[0]), 0, 2, 'L');
         $lnRow++;
         $llTitulo = ($lnRow == 59) ? true : false;
      }
      $loPdf->Output('F', $this->pcFile, true);
      return true;
   }


   // ------------------------------------------------------------------------------
   // Mantenimiento Centros de Costo - Cargar relaciones de UniAcas y CenCos
   // 2019-03-07 XXXX Creacion
   // ------------------------------------------------------------------------------
   public function omCargarUniacaCencos() {
      $llOk = $this->mxValParamCargarUniacaCencos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarUniacaCencos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamCargarUniacaCencos() {
      if (!isset($this->paData['NPAGINA']) || !preg_match('(^[0-9]+$)', $this->paData['NPAGINA'])) {
         $this->pcError = "NUMERO DE PAGINA NO DEFINIDO O NO VALIDO";
         return false;
      } elseif (intval($this->paData['NPAGINA']) <= 0) {
         $this->pcError = "NUMERO DE PAGINA NO VALIDO ".intval($this->paData['NPAGINA']);
         return false;
      }
      return true;
   }

   protected function mxCargarUniacaCencos($p_oSql) {
      $lnLimit = 5;
      $lnPagina = intval($this->paData['NPAGINA']);
      if (isset($this->paData['CBUSQUE']) && !empty($this->paData['CBUSQUE'])) {
         $lcBusque = strtoupper(str_replace(' ', '%', $this->paData['CBUSQUE']));
         $lcSql = "SELECT A.cCenCos, A.cDescri, B.cUniAca, B.cNomUni, A.cEstado, (TO_CHAR(A.tModifi, 'YYYY-MM-DD HH24:MI')) FROM S01TCCO A INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca WHERE A.cDescri LIKE '%$lcBusque%' ORDER BY A.cDescri ASC";
      } else {
         $lcSql = "SELECT A.cCenCos, A.cDescri, B.cUniAca, B.cNomUni, A.cEstado, (TO_CHAR(A.tModifi, 'YYYY-MM-DD HH24:MI')) FROM S01TCCO A INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca ORDER BY A.cDescri ASC LIMIT $lnLimit OFFSET ".(($lnPagina - 1) * $lnLimit);
      }
      $R1 = $p_oSql->omExec($lcSql);
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCENCOS' => $laTmp[0], 'CDESCRI' => $laTmp[1], 'CUNIACA' => $laTmp[2], 'CNOMUNI' => $laTmp[3], 'CESTADO' => $laTmp[4], 'TMODIFI' => $laTmp[5]];
      }
      if (count($this->paDatos) == 0 && $lnPagina != 1) {
         $this->pcError = "SIN UNIDADES ACADEMICAS";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Mantenimiento de Centros de Costo - edita datos del centro de costo seleccionado
   // 2019-03-07 JABC Creacion
   // ------------------------------------------------------------------------------
   public function omActualizarCencos() {
      $llOk = $this->mxValParamActualizarCencos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxActualizarCencos($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamActualizarCencos() {
      if (!isset($this->paData['CCENCOS'])) {
         $this->pcError = "CODIGO DE CENTRO DE COSTO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CUNIACA'])) {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDA";
         return false;
      }
      if (!isset($this->paData['CESTADO'])) {
         $this->pcError = "ESTADO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxActualizarCencos($p_oSql) {
      $lcSql = "UPDATE S01TCCO
                  SET cUniAca = '{$this->paData['CUNIACA']}', CESTADO = '{$this->paData['CESTADO']}'
                  WHERE CCENCOS = '{$this->paData['CCENCOS']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR CENTRO DE COSTO';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Verifica Unidad Academica sea Escuela Profesional
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omVerificarUnidadAcademica() {
      $llOk = $this->mxValParamVerificarUnidadAcademica();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerificarUnidadAcademica($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamVerificarUnidadAcademica() {
      if (!isset($this->paData['CUNIACA'])) {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDA";
         return false;
      }
      if (in_array($this->paData['CUNIACA'], ['20', '21', '24', '10', '1I', '1J', '1P'])) {
         $this->pcError = "SELECCIONE SU PROGRAMA PROFESIONAL PARA GENERAR PAQUETE";
         return false;
      }
      return true;
   }

   protected function mxVerificarUnidadAcademica($p_oSql) {
      if ($this->paData['CUNIACA'] == '5C'){
         return true;
      }
      if ($this->paData['CCODIGO'] == 'M') {
        $lcSql = "SELECT count(cUniAca) FROM S01TUAC WHERE cNivel = '03' AND cUniAca = '{$this->paData['CUNIACA']}'";  
      } elseif ($this->paData['CCODIGO'] == 'D') {
        $lcSql = "SELECT count(cUniAca) FROM S01TUAC WHERE cNivel = '04' AND cUniAca = '{$this->paData['CUNIACA']}'";
      } else {
        $lcSql = "SELECT count(cUniAca) FROM S01TUAC WHERE cNivel = '01' AND cUniAca = '{$this->paData['CUNIACA']}'";
      }
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "ERROR EN VERIFICAR ACCESO";
         return false;
      }
      if ($laFila[0] == 0) {
         $this->pcError = "SELECCIONE SU PROGRAMA PROFESIONAL PARA GENERAR PAQUETE";
         return false;
      }
      return true;
   }

   public function omInitEstadoTramitesPaquetes() {
      $llOk = $this->mxValParamInitEstadoPaquetes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitEstadoPaquetes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitEstadoPaquetes() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO ALUMNO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitEstadoPaquetes($p_oSql) {
      if (!isset($this->paData['CCODIGO'])) {
         $lcCodigo = "B','T";
      } else {
         $lcCodigo = $this->paData['CCODIGO'];
      } 
      $lcSql = "SELECT cNroDni FROM A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "NO SE ENCONTRO DNI DEL ALUMNO";
         return false;  
      }
      $lcNroDni = $laTmp[0]; 
      //CARGAR ESTADOS DE LOS TRAMITES REALIZADOS SEGUN EL DNI DEL ALUMNO
      $lcSql = "SELECT DISTINCT ON (A.cCodTre) (TO_CHAR(A.tFecha, 'YYYY-MM-DD HH24:MI')), B.cNroExp, B.cCodAlu, C.cNroDni, 
                       E.cDescri, A.cEstado AS cEstMTr, F.cEstado AS cEstDTr, A.cIdCate, A.cIdLog, A.cCodTre, A.mDetall, 
                       E.cTipo, D.cNombre, A.cEtapa, A.cEstPro, A.cDocDig
                FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog  = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                INNER JOIN S01MPER D ON D.cNroDni = C.cNroDni
                LEFT JOIN B03TDOC E ON E.cIdCate = A.cIdCate
                LEFT JOIN B04DTRE F ON F.cCodTre = A.cCodTre
                WHERE C.cNroDni = '$lcNroDni' AND C.cPaquet IN ('$lcCodigo') AND A.cEstado NOT IN ('X','I') AND C.cEstado = 'C'
                ORDER BY A.cCodTre,
                   CASE F.cEstado
                WHEN 'B' THEN 0
                WHEN 'A' THEN 1
                WHEN 'C' THEN 2 END";
      $R1 = $p_oSql->omExec($lcSql);
      $i = 0;
      while ($laFila = $p_oSql->fetch($R1)) {
         $lcDocDig = '';
         if ($laFila[14] == 'R') {
            $lcPath = '/'.substr($laFila[15],64,-1).'f';
            $lcPath = '='.addslashes($lcPath);
            $lcDocDig = substr($laFila[15],0,64);
            $lcDocDig = str_replace('=',$lcPath,$lcDocDig);
         }
         $this->paDatos[] = ['TFECREC' => $laFila[0], 'CNROEXP' => $laFila[1], 'CCODALU' => $laFila[2], 'CNRODNI' => $laFila[3],
                             'CDESDOC' => $laFila[4], 'CESTMTR' => $laFila[5], 'CESTDTR' => $laFila[6], 'CIDCATE' => $laFila[7],
                             'CIDLOG'  => $laFila[8], 'CCODTRE' => $laFila[9], 'MDETALL' => json_decode($laFila[10],true), 
                             'CTIPO'   => $laFila[11],'CNOMBRE'=> $laFila[12], 'CETAPA'  => $laFila[13],'CESTPRO' => $laFila[14],
                             'CDOCDIG' => $lcDocDig];
                              //'CCODTRE' => $laFila[9], 'MDETALL' => 'ERROR', 'CTIPO'=> $laFila[11], 'CNOMBRE'=> $laFila[12], 'CETAPA'=> $laFila[13]];
         if ($laFila[9] != null || $laFila[10] != '') {
            $lmDatos = json_decode($laFila[10], true);
            if (isset($lmDatos['MOBSERV'])) {
               $this->paDatos[$i]['MOBSERV'] = $lmDatos['MOBSERV'];
            } else {
               $this->paDatos[$i]['MOBSERV'] = '';
            }
         } else {
            $this->paDatos[$i]['MOBSERV'] = '';
         }
         $i++; 
      }
      if ($lcCodigo == 'T') {
         $lcSql = "SELECT cCodAlu, cTipo, TO_CHAR(tModifi, 'YYYY-MM-DD HH24:MI'), cEstado FROM B04DFED WHERE cCodAlu = '{$this->paData['CCODALU']}' AND cEstado <> 'X'";
         $R1 = $p_oSql->omExec($lcSql);
         while ($laFila = $p_oSql->fetch($R1)) {
            if($laFila[1] == 'D'){
               $lcCodTre = '000002';
               $lcIdCate = 'PDADOC'; 
               $lcDesDoc = 'AUTENTICACION DIPLOMAS GRADOS Y TITULOS';
            }
            else {
               $lcCodTre = '000003';
               $lcIdCate = 'PDAOTR'; 
               $lcDesDoc = 'AUTENT.CERTIFICADOS CON BUSQUEDA';
            }
            if ($laFila[3] == 'A'){
               $lcCEtapa  = 'A';
            }
            if ($laFila[3] == 'E'){
               $lcCEtapa  = 'A';
            }
            if ($laFila[3] == 'B'){
               $lcCEtapa  = 'C';
            } 
            $this->paDatos[] = ['TFECREC' => $laFila[2], 'CCODTRE' => $lcCodTre, 
                                'CCODALU' => $laFila[0], 'CIDCATE' => $lcIdCate,
                                'CESTMTR' => $laFila[3], 'CDESDOC' => $lcDesDoc,
                                'CESTDTR' => null, 'CETAPA' => $lcCEtapa, 'CNRODNI' => $lcNroDni,
                                'CESTPRO' => 'N'];
            $i++; 
         }
      }
      if ($i == 0) {
         $this->pcError = "NO HAY TRAMITES PARA MOSTRAR";
         return false;
      }
      $lcNivel = '01';
      if ($lcCodigo == 'M') {
        $lcNivel = '03';
      } elseif ($lcCodigo == 'D') {
        $lcNivel = '04';
      }
      $lcSql = "SELECT C.cNombre, C.cNroDni, B.cNomUni, C.cNroCel, C.cEmail, A.cCodAlu
               FROM A01MALU A
               INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca AND B.cNivel = '$lcNivel'
               INNER JOIN S01MPER C ON C.cNroDni = A.cNroDni
               WHERE A.cCodAlu ='{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "NO SE ENCONTRARON DATOS DEL ALUMNO";
         return false;
      }
      $this->paData = ['CNOMBRE' => str_replace('/', ' ', $laTmp[0]), 'CNRODNI' => $laTmp[1],
                       'CNOMUNI' => $laTmp[2], 'CNROCEL' => $laTmp[3], 'CEMAIL' => $laTmp[4],
                       'CCODALU' => $laTmp[5]];
      return true;
   }

   public function omInitSeguimientoPaquetes() {
      $llOk = $this->mxValInitSeguimientoPaquetes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitSeguimientoPaquetes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInitSeguimientoPaquetes() {
      if (empty($this->paData['CCODTRE'])) {
         $this->pcError = "NUMERO DE TRAMITE NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitSeguimientoPaquetes($p_oSql) {
      $lcCodTre= $this->paData['CCODTRE'];
      $lcSql = "SELECT  nSerial, cCodTre, cIdCate, cDesDoc, mObserv, TO_CHAR(tRecibi, 'YYYY-MM-DD HH24:MI'), TO_CHAR(tUltima, 'YYYY-MM-DD HH24:MI'), cDesCco, cEstDtr, cNomPer,
                FROM V_B04DTRE
                WHERE cCodTre = '$lcCodTre'
                order by nserial";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['NSERIAL' => $laFila[0],'CCODTRE'=>$laFila[1],'CIDCATE' => $laFila[2], 'CDESDOC'=> $laFila[3],'TFECREC'=>$laFila[5],'TFECFIN'=>$laFila[6], 'CDESCCO'=> $laFila[7], 'CESTDTR'=> $laFila[8], 'CNOMPER'=> $laFila[9]];
         if ($laFila[8]!='C') {
            $lmObserv = json_decode($laFila[4], true);
            $this->paDatos[0]['MOBSERV'] = $lmObserv['MOBSERV'];
         }
         else {
            $this->paDatos[$i]['MOBSERV'] = '';
         }
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO SE ENCONTRARON DATOS";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Paquetes Completos (Listos para bachiller)
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omBandejaRevisionDocentes() {
      $llOk = $this->mxValParamBandejaRevisionDocentes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBandejaRevisionDocentes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBandejaRevisionDocentes() {
      if (!isset($this->paData['CCODUSU'])) {
         return false;
      }
      return true;
   }

   protected function mxBandejaRevisionDocentes($p_oSql) {
      //VERIFICA LOS PAQUETES QUE TENGAN TODOS LOS TRAMITES COMPLETADOS [ESTADO='B']
      $lcSql = "SELECT t_cCodAlu, t_cNombre, t_cUniAca, t_cNomUni, t_dRecepc, t_cNroDni
                FROM F_B04MTRE_5('{$this->paData['CCODUSU']}')";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CUNIACA' => $laFila[2], 'CNOMUNI' => $laFila[3],
                             'DRECEPC' => $laFila[4], 'CNRODNI' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Paquetes Completos (Listos para bachiller)
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omBandejaPaquetesCompletos() {
      $llOk = $this->mxValParamBandejaPaquetesCompletos();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBandejaPaquetesCompletos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBandejaPaquetesCompletos() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } 
      return true;
   }

   protected function mxBandejaPaquetesCompletos($p_oSql) {
      if (!isset($this->paData['CCODIGO'])) {
         $this->paData['CCODIGO'] = "B";
      }
      //VERIFICA LOS PAQUETES QUE TENGAN TODOS LOS TRAMITES COMPLETADOS [ESTADO='B']
      $lcSql = "SELECT t_cCodAlu, t_cNombre, t_cUniAca, t_cNomUni, t_dRecepc, t_cNroDni
                FROM F_B04MTRE_4('{$this->paData['CCODUSU']}','{$this->paData['CCODIGO']}')";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CUNIACA' => $laFila[2], 'CNOMUNI' => $laFila[3],
                             'DRECEPC' => $laFila[4], 'CNRODNI' => $laFila[5]];
      }

      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja de Firmas pendientes
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omBandejaFirmasPendientes() {
      $llOk = $this->mxValParamBandejaFirmasPendientes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBandejaFirmasPendientes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBandejaFirmasPendientes() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxBandejaFirmasPendientes($p_oSql) {
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcSql = "SELECT B.cCodAlu, H.cNombre, F.cUniAca, G.cNomUni, TO_CHAR(A.dRecepc, 'YYYY-MM-DD HH24:MI'), A.cNroDni, C.cCodTre, I.cDescri, (SELECT COUNT(1) FROM B04DAUT WHERE cCodTre = C.cCodTre AND cEstado = 'B')
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                INNER JOIN B04DAUT D ON D.cCodTre = C.cCodTre
                INNER JOIN B04MAUT E ON E.cCodAut = D.cCodAut
                INNER JOIN A01MALU F ON F.cCodAlu = B.cCodAlu
                INNER JOIN S01TUAC G ON G.cUniAca = F.cUniAca
                INNER JOIN S01MPER H ON H.cNroDni = A.cNroDni
                INNER JOIN B03TDOC I ON I.cIdCate = C.cIdCate
                WHERE cPaquet = 'S' AND E.cCodUsu = '{$this->paData['CCODUSU']}' AND D.cEstado = 'A'
                GROUP BY B.cCodAlu, H.cNombre, F.cUniAca, G.cNomUni, A.dRecepc, A.cNroDni, C.cCodTre, I.cDescri
                HAVING count(CASE WHEN C.cEstado IN ('B', 'S') THEN 1 END) = count(C.cCodTre)";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CUNIACA' => $laFila[2], 'CNOMUNI' => $laFila[3],
                             'DRECEPC' => $laFila[4], 'CNRODNI' => $laFila[5],
                             'CCODTRE' => $laFila[6], 'CDESCRI' => $laFila[7],
                             'NCANFIR' => $laFila[8]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Firmar certificado (3 firmas para aprobar documento)
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omFirmarCertificado() {
      $llOk = $this->mxValParamFirmarCertificado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxFirmarCertificado($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamFirmarCertificado() {

      if (!isset($this->paData['CCODTRE']) || !preg_match('(^[0-9]{6}$)', $this->paData['CCODTRE'])) {
         $this->pcError =  "TRAMITE NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError =  "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxFirmarCertificado($p_oSql) {
      $lcCodTre = $this->paData['CCODTRE'];
      $lcJson = json_encode([
         'CCODUSU' => $this->paData['CCODUSU'],
         'CCODTRE' => $this->paData['CCODTRE'],
      ]);
      $lcSql = "SELECT P_B04DAUT_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "ERROR AL AUTORIZAR";
         return false;
      }
      $laData = json_decode($laFila[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      return true;
   }

   /*public function omInitDetalleSeguimientoCompleto() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitDetalleSeguimientoCompleto($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxInitDetalleSeguimientoCompleto($p_oSql) {
      $lcSql = "SELECT B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc
               FROM B03MDEU A
                     INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                     INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                     INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
                     INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
                     INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni WHERE cPaquet = 'S'
               GROUP BY B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc
               HAVING count(CASE WHEN C.cEstado = 'B' THEN 1 END) = count(C.cCodTre)";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['TFECHA' => $laTmp[0], 'CCODTRE' => $laTmp[1], 'CNOMBRE' => $laTmp[2], 'CNRODNI' => $laTmp[3],'CNOMUNI' => $laTmp[4], 'CDESCRI'=>$laTmp[5], 'CESTADO' => $laTmp[6]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO EXISTEN TRAMITES DE BACHILLER COMPLETOS";
         return false;
      }
      return true;
   }*/

   public function omSoloDemo() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $lcSql = "SELECT cCodTre FROM B03DDEU A
               INNER JOIN B04MTRE B ON B.cIdLog = A.cIdLog
               WHERE A.cCodAlu = '2014202221' AND B.cIdCate = 'CCESTU'";
      $R1 = $loSql->omExec($lcSql);
      $laFila = $loSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "TRAMITE NO ENCONTRADO";
         return false;
      }
      $lcCodTre = $laFila[0];
      $lcSql = "UPDATE B04MTRE SET cEstado = 'S' WHERE cCodTre = '$lcCodTre'";
      if (!$loSql->omExec($lcSql)) {
         $this->pcError = "TRAMITE NO ACTUALIZADO";
         return false;
      }
      $loSql->omDisconnect();
      return true;
   }

   // --------------------------------------------------------------------------------
   // Init bandeja de solicitudes de descuento de bachiller para hijo de trabajadores
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omIniBandejaSolicitudes() {
      $llOk = $this->mxValParamInitBandejaSolicitudes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudes() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudes($p_oSql) {
      $lcSql = "SELECT A.cCodAlu, A.cCodUsu, A.cEstado, A.cTipAut, TO_CHAR(A.tModifi, 'YYYY-MM-DD HH24:MI'), E.cNombre, D.cDescri, A.nSerial, G.cNombre
                FROM B01DAUT A
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu
                LEFT JOIN S01TUSU C ON C.cCodUsu = A.cCodUsu
                LEFT JOIN B01TAUT D ON D.cTipAut = A.cTipAut
                LEFT JOIN S01MPER E ON E.cNroDni = B.cNroDni
                LEFT JOIN A01MDOC F ON F.cCodDoc = A.cUsuCod
                INNER JOIN  S01MPER G ON G.cNroDni = C.cNroDni
                WHERE A.cEstado = 'E' A.cTipAut = '005' ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CCODUSU' => $laFila[1],
                             'CESTADO' => $laFila[2], 'CTIPAUT' => $laFila[3],
                             'TMODIFI' => $laFila[4], 'CNOMBRE' => $laFila[5],
                             'CDESCRI' => $laFila[6], 'NSERIAL' => $laFila[7],'CNOMPAD' => $laFila[8]];
      }
      return true;
   }

   // --------------------------------------------------------------------------------
   // Init bandeja de solicitudes de descuento de bachiller para hijo de trabajadores
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omIniBandejaSolicitudesContabilidad() {
      $llOk = $this->mxValParamInitBandejaSolicitudesContabilidad();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudesContabilidad($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudesContabilidad() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudesContabilidad($p_oSql) {
      $lcSql = "SELECT A.cCodAlu, A.cCodUsu, A.cEstado, A.cTipAut, TO_CHAR(A.tModifi, 'YYYY-MM-DD HH24:MI'), E.cNombre, D.cDescri, A.nSerial, G.cNombre
                FROM B01DAUT A
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu
                LEFT JOIN S01TUSU C ON C.cCodUsu = A.cCodUsu
                LEFT JOIN B01TAUT D ON D.cTipAut = A.cTipAut
                LEFT JOIN S01MPER E ON E.cNroDni = B.cNroDni
                LEFT JOIN A01MDOC F ON F.cCodDoc = A.cUsuCod
                INNER JOIN  S01MPER G ON G.cNroDni = C.cNroDni
                WHERE A.cTipAut = '005' ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CCODUSU' => $laFila[1],
                             'CESTADO' => $laFila[2], 'CTIPAUT' => $laFila[3],
                             'TMODIFI' => $laFila[4], 'CNOMBRE' => $laFila[5],
                             'CDESCRI' => $laFila[6], 'NSERIAL' => $laFila[7],'CNOMPAD' => $laFila[8]];
      }
      return true;
   }
   public function omEditarSolicitud() {
      $llOk = $this->mxValParamEditarSolicitud();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEditarSolicitud($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEditarSolicitud() {
      if (!isset($this->paData['NSERIAL'])) {
         $this->pcError = "TRAMITE NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxEditarSolicitud($p_oSql) {
      $lcSql = "SELECT A.cCodAlu, A.cCodUsu, A.cEstado, A.cTipAut, TO_CHAR(A.tModifi, 'YYYY-MM-DD HH24:MI'), E.cNombre, D.cDescri, A.nSerial, G.cNombre
                FROM B01DAUT A
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu
                LEFT  JOIN S01TUSU C ON C.cCodUsu = A.cCodUsu
                LEFT  JOIN B01TAUT D ON D.cTipAut = A.cTipAut
                LEFT  JOIN S01MPER E ON E.cNroDni = B.cNroDni
                LEFT  JOIN A01MDOC F ON F.cCodDoc = A.cUsuCod
                INNER JOIN  S01MPER G ON G.cNroDni = C.cNroDni
                WHERE A.nSerial = '{$this->paData['NSERIAL']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $this->paDatos = ['CCODALU' => $laFila[0], 'CCODUSU' => $laFila[1],
               'CESTADO' => $laFila[2], 'CTIPAUT' => $laFila[3],
               'TMODIFI' => $laFila[4], 'CNOMBRE' => $laFila[5],
               'CDESCRI' => $laFila[6], 'NSERIAL' => $laFila[7], 'CNOMPAD' => $laFila[8]];
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Cambiar estado de solicitud a estado Aprobado
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omAprobarSolicitud() {
      $llOk = $this->mxValParamAprobarSolicitud();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitud($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAprobarSolicitud() {
      if (!isset($this->paData['CCODUSU']) OR (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO [CCODUSU] INVALIDO O NO DEFINIDO';
         return false;
      }  elseif (!isset($this->paData['NDESCUE']) OR (strlen($this->paData['NDESCUE']) > 3) OR !preg_match("(^[0-9]+$)", $this->paData['CCODUSU'])) {
         $this->pcError = 'PARAMETRO DE DESCUENTO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxGrabarSolicitud($p_oSql) {
      $lcDescu = $this->paData['NDESCUE']/100;
      $lcSql = "UPDATE B01DAUT SET cEstado = 'A', nDescue = '$lcDescu', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}'
                WHERE NSERIAL = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR SOLICTUD';
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Cambiar estado de solicitud a estado Denegado
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omGrabarSolicitudDenegada() {
      $llOk = $this->mxValParamGrabarSolicitudDenegada();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitudDenegada($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarSolicitudDenegada() {
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxGrabarSolicitudDenegada($p_oSql) {
      $lcSql = "UPDATE B01DAUT SET CESTADO = 'R', TMODIFI = NOW(), COBSERV = '', CUSUCOD = '{$this->paData['CCODUSU']}'
                   WHERE NSERIAL = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR SOLICTUD';
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Cambiar estado de solicitud a estado Denegado
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omAnularSolicitud() {
      $llOk = $this->mxValParamAnularSolicitud();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAnularSolicitud($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAnularSolicitud() {
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxAnularSolicitud($p_oSql) {
      $lcSql = "UPDATE B01DAUT SET CESTADO = 'X', TMODIFI = NOW(), CUSUCOD = '{$this->paData['CCODUSU']}'
                   WHERE NSERIAL = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL ACTUALIZAR SOLICTUD';
         return false;
      }
      return true;
   }


   // ------------------------------------------------------------------------------
   // Init Bandeja de Paquetes disponibles
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarBandejaMtoPaquetes() {
      $llOk = $this->mxValParamRecuperarBandejaMtoPaquetes();
      if (!$llOk) {
         return False;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarBandejaMtoPaquetes($loSql);
      return $llOk;
   }

   protected function mxValParamRecuperarBandejaMtoPaquetes() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO USUARIO INVALIDO O NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarBandejaMtoPaquetes($p_oSql) {
      $lcSql = "SELECT cIdPaqu, A.cDescri, A.cEstado, A.cUniAca, B.cNomUni
                FROM B03MPAQ A
                INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca
                INNER JOIN S01TCCO C ON C.cUniAca = A.cUniAca
                INNER JOIN B03PUSU D ON (D.cCenCos = C.cCenCos AND D.cEstado = 'A') OR (D.cCenCos = '098')
                WHERE B.cNivel = '01' AND A.cUniAca <> '00' AND D.cCodUsu = '{$this->paData['CCODUSU']}'
                ORDER BY cIdPaqu";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDPAQU' => $laFila[0], 'CDESCRI' => $laFila[1],
                             'CESTADO' => $laFila[2], 'CUNIACA' => $laFila[3],
                             'CNOMUNI' => $laFila[4]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Consigue codigo único para un paquete
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omConseguirNuevoCodigo() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxConseguirNuevoCodigo($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxConseguirNuevoCodigo($p_oSql) {
      $lcSql = "SELECT LPAD((cIdPaqu::INTEGER + 1)::TEXT, 4, '0') FROM B03MPAQ ORDER BY cIdPaqu DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $lcSql = "SELECT COUNT(*) FROM B03MPAQ";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         if (!isset($laFila[0])) {
            $this->pcError = "NO SE PUDO CONSEGUIR NUEVO CODIGO";
            return false;
         }
         $this->paData = ['CIDPAQU' => '0001'];
         return true;
      }
      $this->paData = ['CIDPAQU' => $laFila[0]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Agregar nuevo paquete a una Unidad Académica
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAgregarPaquete() {
      $llOk = $this->mxValParamAgregarPaquete();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAgregarPaquete($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAgregarPaquete() {
      if (!isset($this->paData['CDESCRI']) || !preg_match("(^[a-zA-Z0-9áéíóúñÁÉÍÓÚÑ \n\/\.,;_-]+$)", $this->paData['CDESCRI'])) {
         $this->pcError = "DESCRIPCCION DEL PAQUETE NO DEFINIDA";
         return false;
      } elseif (!isset($this->paData['CUNIACA']) || strlen($this->paData['CUNIACA']) != 2) {
         $this->pcError = "UNIDAD ACADEMICA NO VALIDA O NO DEFINIDA";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO VALIDA O NO DEFINIDA";
         return false;
      } elseif (!isset($this->paData['CCODIGO'])) {
         $this->pcError = "CODIGO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxAgregarPaquete($p_oSql) {
      $lcSql = "SELECT LPAD((cIdPaqu::INTEGER + 1)::TEXT, 4, '0') FROM B03MPAQ ORDER BY cIdPaqu DESC LIMIT 1";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $laFila[0] = '0001';
      }
      $this->paData['CIDPAQU'] = $laFila[0];
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_B03MPAQ_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "HA OCURRIDO UN PROBLEMA GUARDANDO PAQUETE";
         return false;
      }
      $laData = json_decode($laFila[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Recupera Información de un Paquete
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarInfoPaquete() {
      $llOk = $this->mxValParamRecuperarInfoPaquete();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarInfoPaquete($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarInfoPaquete() {
      if (!isset($this->paData['CIDPAQU']) || strlen($this->paData['CIDPAQU']) != 4) {
         $this->pcError = "CODIGO NO VALIDO O NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarInfoPaquete($p_oSql) {
      $lcSql = "SELECT cIdPaqu, A.cDescri, A.cEstado, A.cUniAca, B.cNomUni
                FROM B03MPAQ A
                INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca
                WHERE cIdPaqu = '{$this->paData['CIDPAQU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "PAQUETE NO ENCONTRADO";
         return false;
      }
      $this->paDatos['paPaquet'] = ['CIDPAQU' => $laFila[0], 'CDESCRI' => $laFila[1],
                                    'CESTADO' => $laFila[2], 'CUNIACA' => $laFila[3],
                                    'CNOMUNI' => $laFila[4]];
      $lcSql = "SELECT A.cIdCate, A.cEstado, B.cDescri, CASE WHEN A.cIdCate = 'CCESTU' THEN B.nMonto * A.nCanSem + (CASE WHEN A.cCurCom = 'S' THEN B.nMonto ELSE 0 END) ELSE B.nMonto END, C.nCosto AS nCosFor, A.nSerial
                FROM B03PPAQ A
                INNER JOIN B03TDOC B ON B.cIdCate = A.cIdCate
                INNER JOIN B03TFOR C ON C.cTipFor = B.cTipFor
                WHERE cIdPaqu = '{$this->paData['CIDPAQU']}'
                ORDER BY A.cIdCate";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paDocPaq'] = [];
      while( $laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paDocPaq'][] = ['CIDCATE' => $laFila[0], 'CESTADO' => $laFila[1],
                                         'CDESCRI' => $laFila[2], 'NMONTO'  => $laFila[3],
                                         'NCOSFOR' => $laFila[4], 'NSERIAL' => $laFila[5]];
      }
      //RECUPERAR LISTA DE DOCUMENTOS
      $lcSql = "SELECT cIdCate, cDescri FROM B03TDOC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paLisDoc'] = [];
      while( $laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paLisDoc'][] = ['CIDCATE' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      //RECUPERAR LISTA DE FORMATOS
      $lcSql = "SELECT cTipFor, cDescri, cEstado, nCosto FROM B03TFOR ORDER BY cTipFor";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paLisFor'] = [];
      while( $laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paLisFor'][] = ['CTIPFOR' => $laFila[0], 'CDESCRI' => $laFila[1],
                                         'CESTADO' => $laFila[2], 'XCOSTO'  => $laFila[3]];
      }
      //RECUPERAR LISTA DE PROYECTOS
      $lcSql = "SELECT cProyec, cDescri, cEstado FROM A02MPRY WHERE cUniAca = '00' ORDER BY cProyec DESC LIMIT 5";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paLisPry'] = [];
      while( $laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paLisPry'][] = ['CPROYEC' => $laFila[0], 'CDESCRI' => $laFila[1],
                                         'CESTADO' => $laFila[2]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Agregar documento a un Paquete (PQ)
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAgregarDocumento() {
      $llOk = $this->mxValParamAgregarDocumento();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAgregarDocumento($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAgregarDocumento() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO VALIDO O NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CIDPAQU']) || strlen($this->paData['CIDPAQU']) != 4) {
         $this->pcError = "PAQUETE NO VALIDO O NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CIDCATE']) || strlen($this->paData['CIDCATE']) != 6) {
         $this->pcError = "UNIDAD ACADEMICA NO VALIDA O NO DEFINIDA";
         return false;
      }
      if ($this->paData['CIDCATE'] == 'CCESTU') {
         if (!isset($this->paData['NCANSEM']) || !preg_match('/^[0-9]+$/', $this->paData['NCANSEM'])) {
            $this->pcError = "CANTIDAD DE SEMESTRES NO VALIDO O NO DEFINIDOS";
            return false;
         }
         if (!isset($this->paData['CCURCOM']) || !preg_match('/^[SN]{1}$/', $this->paData['CCURCOM'])) {
            $this->pcError = "COMPLEMENTACION CURRICULAR NO VALIDA O NO DEFINIDA";
            return false;
         }
      }
      return true;
   }

   protected function mxAgregarDocumento($p_oSql) {
      $lnCanSem = 0;
      $lcCurCom = 'N';
      if ($this->paData['CIDCATE'] == 'CCESTU') {
         $lnCanSem = $this->paData['NCANSEM'];
         $lcCurCom = $this->paData['CCURCOM'];
      }
      $lcJson = json_encode($this->paData);
      $lcSql =  "SELECT P_B03PPAQ_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE LOGRÓ INSERTAR DOCUMENTO";
         return false;
      }
      $loJson = json_decode($laFila[0], true);
      if (isset($loJson['ERROR'])) {
         $this->pcError = $loJson['ERROR'];
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualizar estado de documento de un paquete
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omEditarDocumento() {
      $llOk = $this->mxValParamEditarDocumento();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEditarDocumento($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEditarDocumento() {
      if (!isset($this->paData['NSERIAL']) || !preg_match('/^[0-9]+$/', $this->paData['NSERIAL'])) {
         $this->pcError = "DOCUMENTO NO VALIDO O NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CESTADO']) || !preg_match('/^[AI]{1}$/', $this->paData['CESTADO'])) {
         $this->pcError = "ESTADO NO VALIDO O NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxEditarDocumento($p_oSql) {
      $lcSql = "UPDATE B03PPAQ SET cEstado = '{$this->paData['CESTADO']}'
                WHERE nSerial = {$this->paData['NSERIAL']}";
      if (!$p_oSql->omExec($lcSql)) {
         $this->pcError = "OCURRIÓ UN PROBLEMA ACTUALIZANDO EL ESTADO DEL DOCUMENTO";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualizar estado de Paquetes
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omEditarPaquete() {
      $llOk = $this->mxValParamEditarPaquete();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxEditarPaquete($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamEditarPaquete() {
      if (!isset($this->paData['CIDPAQU']) || strlen($this->paData['CIDPAQU']) != 4) {
         $this->pcError = "PAQUETE NO VALIDO O NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CESTADO']) || !preg_match('/^[AI]{1}$/', $this->paData['CESTADO'])) {
         $this->pcError = "ESTADO NO VALIDO O NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CDESCRI']) || empty($this->paData['CDESCRI'])) {
         $this->pcError = "NOMBRE NO VALIDO O NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxEditarPaquete($p_oSql) {
      $lcSql = "UPDATE B03MPAQ SET cEstado = '{$this->paData['CESTADO']}', cDescri = '{$this->paData['CDESCRI']}'
                WHERE cIdPaqu = '{$this->paData['CIDPAQU']}'";
      if (!$p_oSql->omExec($lcSql)) {
         $this->pcError = "OCURRIÓ UN PROBLEMA ACTUALIZANDO EL ESTADO DEL PAQUETE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Agregar nuevo documento (PQ)
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAgregarDocumentoNuevo() {
      $llOk = $this->mxValParamAgregarDocumentoNuevo();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAgregarDocumentoNuevo($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAgregarDocumentoNuevo() {
      if (!isset($this->paData['CDESCRI'])) {
         $this->pcError = "NOMBRE DEL DOCUMENTO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CTIPFOR'])) {
         $this->pcError = "FORMATO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['DINICIO'])) {
         $this->pcError = "FECHA DE INICIO NO DEFINIDA";
         return false;
      }
      if (!isset($this->paData['DFINALI'])) {
         $this->pcError = "FECHA DE FINALIZACIÓN NO DEFINIDA";
         return false;
      }
      if (!isset($this->paData['CPERIOD'])) {
         $this->pcError = "PERIODO NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CCODUSU']) || !preg_match('(^[0-9]{4}$)', $this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO O INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxAgregarDocumentoNuevo($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcSql =  "SELECT P_B03TDOC_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE LOGRÓ INSERTAR DOCUMENTO";
         return false;
      }
      $loJson = json_decode($laFila[0], true);
      if (isset($loJson['ERROR'])) {
         $this->pcError = $loJson['ERROR'];
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Añadir Nuevos Alumnos al Curso de Liderazgo
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAgregarLiderazgo() {
      $llOk = $this->mxValParamAgregarLiderazgo();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAgregarLiderazgo($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAgregarLiderazgo() {
      if (!isset($this->paData['CNRODNI']) || !preg_match('(^[0-9]{8}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI DEL ALUMNO NO DEFINIDO O INVALIDO";
         return false;
      }
      if (!isset($this->paData['CCODUSU']) || !preg_match('(^[0-9]{4}$)', $this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO USUARIO NO DEFINIDO O INVALIDO";
         return false;
      }
      if (!isset($this->paData['CPROYEC'])) {
         $this->pcError = "PROYECTO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxAgregarLiderazgo($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcSql =  "SELECT P_B04DTCL_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE LOGRÓ INSCRIBIR ALUMNO";
         return false;
      }
      $loJson = json_decode($laFila[0], true);
      if (isset($loJson['ERROR'])) {
         $this->pcError = $loJson['ERROR'];
         return false;
      }
      return true;
   }
   //AQUI
   // --------------------------------------------------------------------------
   // Init bandeja de revision alumnos deudores de Coordinacion de Laboratorio
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omInitBandejaRevision() {
      $llOk = $this->mxValParamInitBandejaRevision();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaRevision($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaRevision() {
      if (!isset($this->paData['CCODUSU']) or strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaRevision($p_oSql) {
      $lcSql = "SELECT TO_CHAR(A.dGenera, 'yyyy-mm-dd hh24:mi'), A.cNroDni, B.cNombre, A.nSerial FROM B04DTCL A
               INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni WHERE A.cEstLab = 'D' AND A.cUnidad = 'CL' ORDER BY A.dGenera";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['DGENERA' => $laFila[0], 'CNRODNI' => $laFila[1], 'CNOMBRE' => $laFila[2], 'NSERIAL' => $laFila[3]];
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Consulta detalle de deuda de alumno
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omDetalleAlumno() {
      $llOk = $this->mxValParamDetalleAlumno();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDetalleAlumno($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDetalleAlumno() {
      if (!isset($this->paData['CCODUSU']) or (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO [CCODUSU] INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL'])) {
         $this->pcError = 'PARAMETRO CODIGO DE TRANSACCION [NSERIAL] INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxDetalleAlumno($p_oSql) {
      $lcNserial = $this->paData['NSERIAL'];
      $lcSql = "SELECT TO_CHAR(A.dGenera, 'yyyy-mm-dd hh24:mi'), A.cNroDni, B.cNombre, A.nSerial, A.mDescri FROM B04DTCL A
               INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni WHERE A.nSerial = '{$this->paData['NSERIAL']}' AND A.cEstLab = 'D' AND A.cUnidad = 'CL'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE ENCONTRO REGISTRO";
         return false;
      }
      $this->paData = ['DGENERA' => $laFila[0], 'CNRODNI' => $laFila[1], 'CNOMBRE' => $laFila[2], 'NSERIAL' => $laFila[3], 'MDESCRI' => $laFila[4]];
      return true;
   }

   // --------------------------------------------------------------------------
   // Remueve (inactiva) deuda de alumno en coordinacion de laboratorios
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omRemoverDeudaLaboratorio() {
      $llOk = $this->mxValParamRemoverDeudaLaboratorio();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRemoverDeudaLaboratorio($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRemoverDeudaLaboratorio() {
      $this->paData['NSERIAL'] = (int)$this->paData['NSERIAL'];
      if (!isset($this->paData['NSERIAL']) or $this->paData['NSERIAL'] <= 0) {
         $this->pcError = 'PARAMETRO ID. DE DEUDA INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxRemoverDeudaLaboratorio($p_oSql) {
      $lcSql = "SELECT cEstLab, cUnidad FROM B04DTCL WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE ENCONTRO DEUDA DE LABORATORIO";
         return false;
      } elseif ($laFila[0] == 'L') {
         $this->pcError = "DEUDA DE LABORATORIO NO ESTA PENDIENTE";
         return false;
      } elseif ($laFila[1] != 'CL') {
         $this->pcError = "DEUDA NO PERTENECE A COORDINACION DE LABORATORIO";
         return false;
      }
      $lcSql = "UPDATE B04DTCL SET cEstLab = 'L', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}', dLevant = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL REMOVER (INACTIVAR) ID. DE DEUDA';
         return false;
      }
      return true;
   }

   public function omVerificarAlumno() {
      $llOk = $this->mxValParamVerificarAlumno();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerificarAlumno($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamVerificarAlumno() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxVerificarAlumno($p_oSql) {
      $lcCodAlu = $this->paData['CCODALU'];
      $lcSql = "SELECT CNOMBRE, CNRODNI,CNOMUNI FROM V_A01MALU WHERE CCODALU  = '$lcCodAlu'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE ALUMNO ERRONEO";
         return false;
      }
      $this->paData = ['CNOMBRE' => $laFila[0], 'CNRODNI' => $laFila[1],'CNOMUNI' => $laFila[2]];
      return true;
   }

   // --------------------------------------------------------------------------
   // Crea nueva deuda en coordinacion de laboratorios
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omGrabarCoordinacionLaboratorios() {
      $llOk = $this->mxValParamGrabarCoordinacionLaboratorio();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerUsuario($loSql);
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }
      $llOk = $this->mxGrabarCoordinacionLaboratorio($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarCoordinacionLaboratorio() {
      if (!isset($this->paData['MDESCRI'])) {
         $this->pcError = 'PARAMETRO OBSERVACIONES [MOBSERV] INVALIDO O NO DEFINIDO';
         return false;
      } elseif (strlen($this->paData['MDESCRI']) >= 50) {
         $this->pcError = 'LAS OBSERVACIONES DEBEN TENER MAXIMO 50 CARACTERES';
         return false;
      } elseif (!preg_match("(^[a-zA-Z0-9áéíóúñÁÉÍÓÚÑ \n\/\.,;_-]+$)", $this->paData['MDESCRI'])) {
         $this->pcError = 'NO USAR CARACTERES ESPCIALES';
         return false;
      }
      return true;
   }

   protected function mxGrabarCoordinacionLaboratorio($p_oSql) {
      $lcParam = json_encode($this->paData);
      $lcSql = "SELECT P_B04DTCL_2('$lcParam')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!$laTmp[0]) {
         $this->pcError = "ERROR EN EJECUCION DE GRABACION DE TRANSACCION [P_B04DTCL_2]";
         return false;
      }
      $laData = json_decode($laTmp[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      return true;
   }
   //AQUI

   // ------------------------------------------------------------------------------
   // Init Listado de Unidades Academicas y Proyectos para Liderzgo
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitTutoria() {
      $loSql = new CSql();
      $llOk =  $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitTutoria($loSql);
      return $llOk;
   }

   protected function mxInitTutoria($p_oSql) {
      $lcSql = "SELECT cUniAca, cNomUni FROM S01TUAC WHERE cNivel = '01' AND cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paUniAca'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paUniAca'][] = ['CUNIACA' => $laFila[0], 'CNOMUNI' => $laFila[1]];
      }
      if (count($this->paDatos['paUniAca']) == 0) {
         $this->pcError = "SIN UNIDADES ACADEMICAS";
         return false;
      }
      $lcSql = "SELECT cProyec, cDescri, cEstado FROM A02MPRY WHERE cUniAca = 'TU' ORDER BY cEstado, cProyec";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paProyec'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paProyec'][] = ['CPROYEC' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      if (count($this->paDatos['paProyec']) == 0) {
         $this->pcError = "SIN PROYECTOS DISPONIBLES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Tutoria Universitaria
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarBandejaTutoria() {
      $llOk = $this->mxValParamRecuperarBandejaTutoria();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarBandejaTutoria($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarBandejaTutoria() {
      if (!isset($this->paData['CUNIACA'])) {
         $this->pcError = "UNIDADA ACADÉMICA NO DEFINIDA";
         return false;
      }
      if (!isset($this->paData['CPROYEC'])) {
         $this->pcError = "PROYECTO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarBandejaTutoria($p_oSql) {
      $lcSql = "SELECT A.nSerial, B.cCodAlu, A.cProyec, A.cEstTut, A.dGenera, D.cNombre, C.cNomUni, A.cNroDni, A.cEstTut
                FROM B04DTCL A
                INNER JOIN A01MALU B ON B.cNroDni = A.cNroDni
                INNER JOIN S01TUAC C ON C.cUniAca = B.cUniAca
                INNER JOIN S01MPER D ON D.cNroDni = B.cNroDni
                WHERE B.cUniAca = '{$this->paData['CUNIACA']}' AND A.cProyec = '{$this->paData['CPROYEC']}'
                ORDER BY A.dGenera";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['NSERIAL' => $laFila[0], 'CCODALU' => $laFila[1],
                             'CPROYEC' => $laFila[2], 'CESTTUT' => $laFila[3],
                             'DGENERA' => $laFila[4], 'CNOMBRE' => $laFila[5],
                             'CNOMUNI' => $laFila[6], 'CNRODNI' => $laFila[7],
                             'CESTTUT' => $laFila[8]];
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Recuperar Información de alumno inscrito en Liderazgo (Tutoria Universitaria)
   // 2019-02-06 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarInfoTutoria() {
      $llOk = $this->mxValParamRecuperarInfoTutoria();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarInfoTutoria($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarInfoTutoria() {
      if (!isset($this->paData['NSERIAL'])) {
         $this->pcError = "REGISTRO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarInfoTutoria($p_oSql) {
      $lcSql = "SELECT A.nSerial, A.cProyec, A.cEstTut, TO_CHAR(A.dGenera, 'yyyy-mm-dd hh24:mi'), B.cNombre, A.cNroDni
                FROM B04DTCL A
                INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni
                WHERE A.nSerial = {$this->paData['NSERIAL']}";
      //echo $lcSql; die;
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "REGISTRO NO ENCONTRADO";
         return false;
      }
      $this->paDatos = ['NSERIAL' => $laFila[0], 'CPROYEC' => $laFila[1],
                        'CESTTUT' => $laFila[2], 'DGENERA' => $laFila[3],
                        'CNOMBRE' => $laFila[4], 'CNRODNI' => $laFila[5]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Actualiza estado del curso de liderazgo moral
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omActualizarEstadoLiderazgo() {
      $llOk = $this->mxValParamActualizarEstadoLiderazgo();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxActualizarEstadoLiderazgo($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamActualizarEstadoLiderazgo() {
      if (!isset($this->paData['NSERIAL']) && !preg_match('^[0-9]+$', $this->paData['NSERIAL'])) {
         $this->pcError = "REGISTRO A ACTUALIZAR NO DEFINIDO O INVALIDO";
         return false;
      }
      if (!isset($this->paData['CESTADO']) && !preg_match('^[ADNX]{1}$', $this->paData['CESTADO'])) {
         $this->pcError = "ESTADO NO DEFINIDO O INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxActualizarEstadoLiderazgo($p_oSql) {
      $lcSql = "UPDATE B04DTCL SET cEstTut = '{$this->paData['CESTADO']}' WHERE nSerial = {$this->paData['NSERIAL']}";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR ACTUALIZANDO REGISTRO";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Curso liderazgo
   // 2019-02-27 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitCursoLiderazgo() {
      $llOk = $this->mxValParamInitCursoLiderazgo();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitCursoLiderazgo($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitCursoLiderazgo() {
      if (!isset($this->paData['CNRODNI']) || !preg_match('(^[0-9]{8}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitCursoLiderazgo($p_oSql) {
      $lcSql = "SELECT cEstTut, TO_CHAR(dGenera, 'YYYY-MM-DD HH24:MI'), mDescri FROM B04DTCL WHERE cNroDni = '{$this->paData['CNRODNI']}' AND cUnidad = 'TU' AND cEstTut IN ('A', 'I', 'P') ORDER BY cEstTut";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->paData = ['CESTTUT' => $laFila[0], 'DGENERA' => $laFila[1]];
         // SI TIENE CODIGO DE PAGO
         if (!empty($laFila[2])) {
            $lcIdDeud = json_decode($laFila[2], true)['CIDDEUD'];
            $lcSql = "SELECT cNroPag FROM B03MDEU WHERE cIdDeud = '$lcIdDeud'";
            $R1 = $p_oSql->omExec($lcSql);
            $laTmp = $p_oSql->fetch($R1);
            if (isset($laTmp[0])) {
               $this->paData['CNROPAG'] = $laTmp[0];
            }
         }
         return true;
      }
      $lcSql = "SELECT cConten, cDescri FROM S01TVAR WHERE cVariab = 'GDFECHTU'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "SIN CURSOS DISPONIBLES";
         return false;
      }
      $this->paData = ['DFECCUR' => $laFila[0]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Inscripcion en curso de liderazgo
   // 2019-02-012 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInscripcionLiderazgo() {
      $llOk = $this->mxValParamInscripcionLiderazgo();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInscripcionLiderazgo($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInscripcionLiderazgo() {
      if (!isset($this->paData['CNRODNI']) || !preg_match('(^[0-9]{8}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI DEL ALUMNO NO DEFINIDO O INVALIDO";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO USUARIO NO DEFINIDO O INVALIDO";
         return false;
      } elseif (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO ALUMNO NO DEFINIDO O INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxInscripcionLiderazgo($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcSql =  "SELECT P_B04DTCL_1('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE LOGRÓ INSERTAR DOCUMENTO";
         return false;
      }
      $loJson = json_decode($laFila[0], true);
      if (isset($loJson['ERROR'])) {
         $this->pcError = $loJson['ERROR'];
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Bandeja ORAA historial expedientes
   // 2019-02-012 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarHistorialExpedientes() {
      $llOk = $this->mxValParamRecuperarHistorialExpedientes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarHistorialExpedientes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarHistorialExpedientes() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarHistorialExpedientes($p_oSql) {
      $lcSql = "SELECT COUNT(*) FROM B03PUSU WHERE cCodUsu = '{$this->paData['CCODUSU']}' AND cCenCos = '031' AND cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0]) || $laTmp[0] == '0') {
         $this->pcError = "NO SE TIENEN PERMISOS PARA ACCEDER";
         return false;
      }
      if (!isset($this->paData['CIDCOLA'])){
         $lcTipCol = 'T'; 
      } else {
         $lcSql = "SELECT cTipCol FROM B05MCOL WHERE CIDCOLA = '{$this->paData['CIDCOLA']}'";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         $lcTipCol = $laFila[0];
      }
      $lcWhere = (isset($this->paData['CIDCOLA']) && $this->paData['CIDCOLA'] != 'T' ? "AND cIdCola = '{$this->paData['CIDCOLA']}'" : '');
      $lcSql = "SELECT B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, TO_CHAR(A.dRecepc, 'YYYY-MM-DD HH24:MI') as dRecepc, A.cNroDni, A.cIdDeud
               FROM B03MDEU A
               INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
               INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
               INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
               INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
               INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni
               INNER JOIN B05DUAC G ON G.cCodAlu = D.cCodAlu AND D.cEstado = 'A'
               INNER JOIN B05PUAC H ON H.cColUac = G.cColUac
               WHERE A.cPaquet = '$lcTipCol' AND A.cEstado = 'C' AND C.cEtapa <> 'E' AND E.cUniAca NOT IN ('20', '21') $lcWhere
               GROUP BY B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc, A.cNroDni, A.cIdDeud ORDER BY A.dRecepc DESC";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => str_replace('/', ' ', $laFila[1]),
                             'CUNIACA' => $laFila[2], 'CNOMUNI' => $laFila[3],
                             'DRECEPC' => $laFila[4], 'CNRODNI' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN EXPEDIENTES ENCONTRADOS";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja lista Paquetes Para seguimiento de Paquete de cada alumno
   // 2019-02-012 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omIniBandejaPaquetes() {
      $llOk = $this->mxInitValParamBandejaPaquetes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaPaquetes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxInitValParamBandejaPaquetes() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } 
      return true;
   }

   protected function mxInitBandejaPaquetes($p_oSql) {
      $lcCodigo = $this->paData['CCODIGO'];
      if ($lcCodigo == 'T'){
         $lcEtapa = 'C';
         if ($this->paData['CCODUSU'] == '2052' || $this->paData['CCODUSU'] == '3070') {
            $lcSql = "SELECT B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, TO_CHAR(A.dRecepc, 'YYYY-MM-DD HH24:MI') as dRecepc, A.cNroDni, A.cIdDeud, C.cEtapa
                   FROM B03MDEU A
                   INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                   INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
                   INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
                   INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni
                   WHERE A.cPaquet IN ('$lcCodigo') AND C.cEtapa <> '$lcEtapa' AND E.cUniAca NOT IN ('20','21','24')
                   GROUP BY B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc, A.cNroDni, A.cIdDeud, C.cEtapa ORDER BY A.dRecepc DESC";
         } else {
            $lcSql = "SELECT B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, TO_CHAR(A.dRecepc, 'YYYY-MM-DD HH24:MI') as dRecepc, A.cNroDni, A.cIdDeud, C.cEtapa
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
                INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
                INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni
                INNER JOIN B03PUSU G ON G.cCenCos = C.cCCoDes AND G.cEstado = 'A'
                WHERE A.cPaquet IN ('$lcCodigo') AND G.cCodUsu = '{$this->paData['CCODUSU']}' AND C.cEtapa <> '$lcEtapa' AND E.cUniAca NOT IN ('20','21','24')
                GROUP BY B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc, A.cNroDni, A.cIdDeud, C.cEtapa ORDER BY A.dRecepc DESC";
         }
      } else {
         $lcEtapa = 'D';
         $lcSql = "SELECT B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, TO_CHAR(A.dRecepc, 'YYYY-MM-DD HH24:MI') as dRecepc, A.cNroDni, A.cIdDeud, C.cEtapa
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
                INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
                INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni
                INNER JOIN B03PUSU G ON G.cCenCos = C.cCCoDes AND G.cEstado = 'A'
                WHERE A.cPaquet IN ('$lcCodigo') AND G.cCodUsu = '{$this->paData['CCODUSU']}' AND C.cEtapa <> '$lcEtapa' AND E.cUniAca NOT IN ('20','21','24')
                GROUP BY B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc, A.cNroDni, A.cIdDeud, C.cEtapa ORDER BY A.dRecepc DESC";   
      }
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => str_replace('/', ' ', $laFila[1]),
                             'CUNIACA' => $laFila[2], 'CNOMUNI' => $laFila[3],
                             'DRECEPC' => $laFila[4], 'CNRODNI' => $laFila[5],
                             'CIDDEUD' => $laFila[6], 'CETAPA' => $laFila[7]];
      }
      if (count($this->paDatos) == 0) {   
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Recuperar peticiones cert. conducta por escuela
   // 2019-05-02 LVA Creacion
   // --------------------------------------------------------------------------
   public function omRecuperarCertConductaPendientes() {
      $llOk = $this->mxValParamRecuperarCertConductaPendientes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarCertConductaPendientes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarCertConductaPendientes() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarCertConductaPendientes($p_oSql) {
      $lcSql = "SELECT C.cNroDni, B.cCodAlu, G.cNombre, F.cNomUni
                FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog  = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                INNER JOIN B03PUSU D ON D.cCenCos = A.cCCoDes
                INNER JOIN A01MALU E ON E.cCodAlu = B.cCodAlu
                INNER JOIN S01TUAC F ON F.cUniAca = E.cUniAca
                INNER JOIN S01MPER G ON G.cNroDni = C.cNroDni
                WHERE C.cPaquet = 'S' AND A.cIdCate = 'PQ0016' AND A.cEstado = 'A' AND D.cCodUsu = '{$this->paData['CCODUSU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CNRODNI' => $laFila[0], 'CCODALU' => $laFila[1],
                             'CNOMBRE' => $laFila[2], 'CNOMUNI' => $laFila[3]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN CERTIFICADOS PEDIENTES DE REVISION";
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Init bandeja de revision alumnos deudores de material didactico
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omInitBandejaRevisionMaterialDidactico() {
      $llOk = $this->mxValParamInitBandejaRevisionMaterialDidactico();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaRevisionMaterialDidactico($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaRevisionMaterialDidactico() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaRevisionMaterialDidactico($p_oSql) {
      $lcSql = "SELECT TO_CHAR(A.dGenera, 'yyyy-mm-dd hh24:mi'), A.cNroDni, B.cNombre, A.nSerial, D.cDescri FROM B04DTCL A
               INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni
               INNER JOIN B03PUSU C ON C.cCenCos = A.cCenCos AND C.cEstado = 'A'
               INNER JOIN S01TCCO D ON D.cCenCos = A.cCenCos
      WHERE A.cEstMat = 'D' AND A.cUnidad = 'MD' AND C.cCodUsu = '{$this->paData['CCODUSU']}' ORDER BY A.dGenera";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['DGENERA' => $laFila[0], 'CNRODNI' => $laFila[1], 'CNOMBRE' => $laFila[2], 'NSERIAL' => $laFila[3], 'CDESCRI' => $laFila[4]];
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Consulta detalle de deuda de alumno de material didactico
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omDetalleAlumnoMaterialDidactico() {
      $llOk = $this->mxValParamDetalleAlumnoMaterialDidactico();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDetalleAlumnoMaterialDidactico($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDetalleAlumnoMaterialDidactico() {
      if (!isset($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU']) != 4)) {
         $this->pcError = 'PARAMETRO CODIGO DE USUARIO [CCODUSU] INVALIDO O NO DEFINIDO';
         return false;
      } elseif (!isset($this->paData['NSERIAL'])) {
         $this->pcError = 'PARAMETRO CODIGO DE TRANSACCION [NSERIAL] INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxDetalleAlumnoMaterialDidactico($p_oSql) {
      $lcNserial = $this->paData['NSERIAL'];
      $lcSql = "SELECT TO_CHAR(A.dGenera, 'yyyy-mm-dd hh24:mi'), A.cNroDni, B.cNombre, A.nSerial, A.mDescri FROM B04DTCL A
               INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni WHERE A.nSerial = '{$this->paData['NSERIAL']}' AND A.cEstMat = 'D' AND A.cUnidad = 'MD'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE ENCONTRO REGISTRO";
         return false;
      }
      $this->paData = ['DGENERA' => $laFila[0], 'CNRODNI' => $laFila[1], 'CNOMBRE' => $laFila[2], 'NSERIAL' => $laFila[3], 'MDESCRI' => $laFila[4]];
      return true;
   }

   // --------------------------------------------------------------------------
   // Remueve (inactiva) deuda de alumno en coordinacion de laboratorios
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omRemoverDeudaMaterialDidactico() {
      $llOk = $this->mxValParamRemoverDeudaMaterialDidactico();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRemoverDeudaMaterialDidactico($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRemoverDeudaMaterialDidactico() {
      $this->paData['NSERIAL'] = (int)$this->paData['NSERIAL'];
      if (!isset($this->paData['NSERIAL']) || $this->paData['NSERIAL'] <= 0) {
         $this->pcError = 'PARAMETRO ID. DE DEUDA INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxRemoverDeudaMaterialDidactico($p_oSql) {
      $lcSql = "SELECT cEstLab, cUnidad FROM B04DTCL WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE ENCONTRO DEUDA DE MATERIAL DIDACTICO";
         return false;
      } elseif ($laFila[0] == 'L') {
         $this->pcError = "DEUDA DE MATERIAL DIDACTICO NO ESTA PENDIENTE";
         return false;
      } elseif ($laFila[1] != 'MD') {
         $this->pcError = "DEUDA NO PERTENECE A LA ESCUELA";
         return false;
      }
      $lcSql = "UPDATE B04DTCL SET cEstMat = 'L', tModifi = NOW(), cUsuCod = '{$this->paData['CCODUSU']}', dLevant = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL REMOVER (INACTIVAR) ID. DE DEUDA';
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Crea nueva deuda de material didactico
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omGrabarMaterialDidactico() {
      $llOk = $this->mxValParamGrabarMaterialDidactico();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerUsuario($loSql);
      if (!$llOk) {
         $loSql->omDisconnect();
         return false;
      }
      $llOk = $this->mxGrabarMaterialDidactico($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarMaterialDidactico() {
      if (!isset($this->paData['MDESCRI'])) {
         $this->pcError = 'PARAMETRO OBSERVACIONES [MOBSERV] INVALIDO O NO DEFINIDO';
         return false;
      } elseif (strlen($this->paData['MDESCRI']) >= 50) {
         $this->pcError = 'LAS OBSERVACIONES DEBEN TENER MAXIMO 50 CARACTERES';
         return false;
      } elseif (!preg_match("(^[a-zA-Z0-9áéíóúñÁÉÍÓÚÑ \n\/\.,;_-]+$)", $this->paData['MDESCRI'])) {
         $this->pcError = 'NO USAR CARACTERES ESPCIALES';
         return false;
      }
      return true;
   }

   protected function mxGrabarMaterialDidactico($p_oSql) {
      $lcParam = json_encode($this->paData);
      $lcSql = "SELECT P_B04DTCL_3('$lcParam')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!$laTmp[0]) {
         $this->pcError = "ERROR EN EJECUCION DE GRABACION DE TRANSACCION";
         return false;
      }
      $laData = json_decode($laTmp[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------
   // Cargar datos centros de costos del registrado
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------
   public function omCargarDatos() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarDatos($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxCargarDatos($p_oSql) {
      //CENTRO DE COSTOS
      $lcSql = "SELECT A.cCenCos, B.cDescri FROM B03PUSU A
                INNER JOIN S01TCCO B ON B.cCenCos = A.cCenCos WHERE A.cCodUsu = '{$this->paData['CCODUSU']}' AND A.cEstado = 'A' AND A.cCenCos <> '000'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paCenCos['pcCenCos'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paCenCos['pcCenCos'][] = ['CCENCOS' => $laFila[0], 'CDESCRI' => $laFila[1]];
      }
      if (count($this->paCenCos['pcCenCos']) == 0) {
         $this->pcError = "ERROR AL RECUPERAR CENTROS DE COSTO";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Verifica alumno por codigo ingresado
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omVerificarAlumnoxDni() {
      $llOk = $this->mxValParamVerificarAlumnoxDni();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerificarAlumnoxDni($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamVerificarAlumnoxDni() {   // OJOFPM FALTA VERIFICAR CODIGO DE ALUMNO... QUE DEBE PASAR A SER DNI
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CNRODNI']) || strlen($this->paData['CNRODNI']) != 8) {
         $this->pcError = 'EL NUMERO DE DNI INVALIDO';
         return false;
      }
      return true;
   }

   protected function mxVerificarAlumnoxDni($p_oSql) {
      $lcSql = "SELECT cNombre FROM V_A01MALU WHERE cNroDni = '{$this->paData['CNRODNI']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NUMERO DE DNI NO EXISTE";
         return false;
      }
      $this->paData = ['CNOMBRE' => $laFila[0]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Recuperar descripcción centro de costo
   // 2019-04-22 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarInformacionCentroCosto() {
      $llOk = $this->mxValParamRecuperarInformacionCentroCosto();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarInformacionCentroCosto($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarInformacionCentroCosto() {
      if (!isset($this->paData['CCENCOS']) || strlen($this->paData['CCENCOS']) != 3) {
         $this->pcError = "CENTRO DE COSTO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarInformacionCentroCosto($p_oSql) {
      $lcSql = "SELECT A.cCenCos, A.cDescri, A.cUniAca, B.cNomUni, A.cEstado
                FROM S01TCCO A
                INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca
                WHERE cCenCos = '{$this->paData['CCENCOS']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "CENTRO DE COSTO NO ENCONTRADO";
         return false;
      }
      $this->paData = ['CCENCOS' => $laTmp[0], 'CDESCRI' => $laTmp[1],
                       'CUNIACA' => $laTmp[2], 'CNOMUNI' => $laTmp[3],
                       'CESTADO' => $laTmp[4]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // Recuperar Unidades Academicas
   // 2019-02-22 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarUnidadesAcademicas() {
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarUnidadesAcademicas($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxRecuperarUnidadesAcademicas($p_oSql) {
      $lcSql = "SELECT cUniAca, cNomUni FROM S01TUAC WHERE cNivel = '01' AND cUniAca <> '00'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paUniAca'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paUniAca'][] = ['CUNIACA' => $laFila[0], 'CNOMUNI' => $laFila[1]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN UNIDADES ACADÉMICAS DISPONIBLES";
         return false;
      }
      $lcSql = "SELECT cDescri, cCodigo FROM S01TTAB WHERE cCodTab = '501' AND cTipReg <> '0'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paTipCol'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paTipCol'][] = ['CDESCRI' => $laFila[0], 'CCODIGO' => $laFila[1]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN UNIDADES ACADÉMICAS DISPONIBLES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja de documentos revisados para digitalizacion de imagen
   // 2019-02-08 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaAprobadosDigitalizacionImagen() {
      $llOk = $this->mxValParamInitBandejaAprobadosDigitalizacionImagen();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaAprobadosDigitalizacionImagen($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaAprobadosDigitalizacionImagen() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaAprobadosDigitalizacionImagen($p_oSql) {
      $lcSql = "SELECT A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni, C.cCodtre
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                INNER JOIN B03TDOC D ON D.cIdCate = C.cIdCate
                INNER JOIN S01MPER E ON E.cNroDni = A.cNroDni
                INNER JOIN A01MALU F ON F.cCodAlu = B.cCodAlu
                INNER JOIN S01TUAC G ON G.cUniAca = F.cUniAca
                INNER JOIN B03PUSU H ON H.cCenCos = C.cCCoDes AND H.cEstado = 'A'
                WHERE A.cEstado = 'C' AND D.cTipo = 'PQ' AND A.cPaquet IN ('T','B') AND C.cEstado = 'B' AND H.cCodUsu = '{$this->paData['CCODUSU']}' -- AND C.cIdCate = 'PQ0001'
                GROUP BY A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni, C.cCodtre
                HAVING COUNT(*) = COUNT(CASE WHEN C.cEstado = 'B' THEN 1 END)";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDDEUD' => $laFila[0], 'CNRODNI' => $laFila[1],
                             'CNOMBRE' => $laFila[2], 'CCODALU' => $laFila[3],
                             'CNOMUNI' => $laFila[4], 'CCODTRE' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja de documentos pendientes/Observados para digitalizacion de imagen
   // 2019-02-08 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaObservadosDigitalizacionImagen() {
      $llOk = $this->mxValParamInitBandejaObservadosDigitalizacionImagen();
      if (!$llOk) {
         return false;
      }      
      $loSql = new CSql();
      $llOk = $loSql->omConnect(); 
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaObservadosDigitalizacionImagen($loSql); 
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaObservadosDigitalizacionImagen() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }
   
   protected function mxInitBandejaObservadosDigitalizacionImagen($p_oSql) {
      $lcSql = "SELECT A.cIdDeud, A.cNroDni, E.cNombre, I.cDescri , B.cCodAlu, G.cNomUni, C.cCodtre, C.cEstado, C.cIdCate
                FROM B03MDEU A 
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud 
                INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog 
                INNER JOIN B03TDOC D ON D.cIdCate = C.cIdCate 
                INNER JOIN S01MPER E ON E.cNroDni = A.cNroDni 
                INNER JOIN A01MALU F ON F.cCodAlu = B.cCodAlu 
                INNER JOIN S01TUAC G ON G.cUniAca = F.cUniAca 
                INNER JOIN B03PUSU H ON H.cCenCos = C.cCCoDes 
                INNER JOIN B03TDOC I ON I.cIdCate = C.cIdCate 
                AND H.cEstado = 'A' 
                WHERE A.cEstado = 'C' AND D.cTipo = 'PQ' AND A.cPaquet IN ('B','T') AND C.cEstado = 'E' AND H.cCodUsu = '{$this->paData['CCODUSU']}' AND C.cIdCate = 'PQ0001'
                GROUP BY A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni, C.cCodtre, I.cDescri, C.cEstado, C.cIdCate 
                HAVING COUNT(*) = COUNT(CASE WHEN C.cEstado = 'E' THEN 1 END)";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CIDDEUD' => $laFila[0], 'CNRODNI' => $laFila[1],
                             'CNOMBRE' => $laFila[2], 'CDESCRI' => $laFila[3], 
                             'CCODALU' => $laFila[4], 'CNOMUNI' => $laFila[5], 
                             'CCODTRE' => $laFila[6], 'CESTADO' => $laFila[7], 
                             'CIDCATE' => $laFila[8]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Busca imagen de alumno por DNI ingresado
   // 2019-02-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omBuscarPersonaxDni() {
      $llOk = $this->mxValParamBuscarPersonaxDni();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBuscarPersonaxDni($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBuscarPersonaxDni() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CNRODNI']) AND strlen($this->paData['CNRODNI']) != 8) {
         $this->pcError = 'EL NUMERO DE DNI INVALIDO';
         return false;
      }
      return true;
   }

   protected function mxBuscarPersonaxDni($p_oSql) {
      $lcSql = "SELECT C.cCodTre, A.cNroDni, D.cNombre FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON B.cIdLog  = C.cIdLog AND C.cIdCate = 'PQ0001'
                INNER JOIN S01MPER D ON D.cNroDni = A.cNroDni
                WHERE A.cEstado = 'C' AND A.cPaquet = 'S' AND A.cNroDni = '{$this->paData['CNRODNI']}' AND C.cEstado = 'B'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "PERSONA Y FOTOGRAFIA NO ENCONTRADA";
         return false;
      }
      $this->paData = ['CCODTRE' => $laFila[0],'CNRODNI' => $laFila[1], 'CNOMBRE' => $laFila[2]];
      return true;
   }

   // ------------------------------------------------------------------------------
   // ws para informatica - firma de documentos
   // 2019-03-15 LVA - LVA Creacion
   // ------------------------------------------------------------------------------
   public function omFirmasAprobadas() {
      $llOk = $this->mxValParamFirmasAprobadas();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxFirmasAprobadas($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamFirmasAprobadas() {
      if (!isset($this->paData['CUSUCOD']) || !preg_match("(^[a-zA-Z0-9]+$)", $this->paData['CUSUCOD']) || strlen($this->paData['CUSUCOD']) != 4) {
         $this->pcError = "USUARIO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CIDCATE']) || !preg_match("(^[a-zA-Z0-9]+$)", $this->paData['CIDCATE']) || strlen($this->paData['CIDCATE']) != 6) {
         $this->pcError = "DOCUMENTO INVALIDO";
         return false;
      } elseif (!isset($this->paData['CCODALU'])|| !preg_match("(^[a-zA-Z0-9]+$)", $this->paData['CCODALU'])|| strlen($this->paData['CCODALU']) != 10) {
         $this->pcError = "CODIGO DE ALUMNO INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxFirmasAprobadas($p_oSql) {
      $lcParam = json_encode($this->paData);
      $lcSql = "SELECT P_B04DAUT_2('$lcParam')"; //$lcSql = "SELECT P_B04DAUT_2('$lcParam')";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!$laTmp[0]) {
         $this->pcError = "ERROR EN LA PETICION";
         return false;
      }
      $laData = json_decode($laTmp[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      $this->paData = $laData;
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init muestra las deudas de paquetes del interesado
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitRecepciondeAutenticacion() {
      $llOk = $this->mxValInittRecepciondeAutenticacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitRecepciondeAutenticacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInittRecepciondeAutenticacion() {
      if (!isset($this->paData['CCODUSU']) || !preg_match("(^[a-zA-Z0-9]+$)", $this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO INVALIDO";
         return false;
      } 
      return true;
   }

   protected function mxInitRecepciondeAutenticacion($p_oSql) {
      /*$lcSql = "SELECT A.cCodTre, (TO_CHAR(A.tFecha, 'YYYY-MM-DD HH24:MI')), E.cDescri, B.cCodAlu, D.cNombre, D.cNroDni, D.cNomUni FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                INNER JOIN V_A01MALU D ON D.cCodAlu = B.cCodAlu
                INNER JOIN B03TDOC E ON E.cIdCate = B.cIdCate 
                WHERE A.cIdCate IN ('PDACBU','PDADOC','PDAOTR') AND A.cEstado = 'B' AND C.cEstado = 'C'";*/
      $lcSql = "SELECT A.cCodAlu, A.cTipo, B.cNroDni, C.cNombre, D.cNomUni, A.nSerial FROM B04DFED A 
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu
                INNER JOIN S01MPER C ON C.cNroDni = B.cNroDni
                INNER JOIN S01TUAC D ON D.cUniAca = B.cUniAca
                INNER JOIN S01TCCO F ON F.cUniAca = D.cUniAca
                INNER JOIN B03PUSU E ON E.cCenCos = F.cCenCos AND E.cEstado = 'A'
                WHERE A.cEstado = 'A' AND E.cCodUsu = '{$this->paData['CCODUSU']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR DE BUSQUEDA';
         return false;
      }
      while($laFila = $p_oSql->fetch($llOk)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CTIPO' => $laFila[1] == 'D'? 'DIPLOMA' : 'CERTIFICADO DE ESTUDIOS',
                             'CNRODNI' => $laFila[2], 'CNOMBRE' => $laFila[3],
                             'CNOMUNI' => $laFila[4], 'NSERIAL' => $laFila[5]];
      } 
      if(count($this->paDatos) == 0){
         $this->pcError = 'SIN TRAMITES PENDIENTES';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init muestra las deudas de paquetes del interesado
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAprobarRecepcionAutenticacion() {
      $llOk = $this->mxValAprobarRecepcionAutenticacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAprobarRecepcionAutenticacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValAprobarRecepcionAutenticacion() {
      if (empty($this->paData['NSERIAL'])) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxAprobarRecepcionAutenticacion($p_oSql) {
      //$lcSql = "UPDATE B04MTRE SET cEstado = 'S', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $lcSql = "UPDATE B04DFED SET cEstado = 'B', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL RECEPCIONAR TRAMITE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Observa Atenticacion de Documentos para Titulacion
   // 2019-12-17 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omObservarRecepcionAutenticacion() {
      $llOk = $this->mxValObservarRecepcionAutenticacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxObservarRecepcionAutenticacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValObservarRecepcionAutenticacion() {
      if (empty($this->paData['NSERIAL'])) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxObservarRecepcionAutenticacion($p_oSql) {
      $lcSql = "UPDATE B04DFED SET cEstado = 'E', cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE nSerial = '{$this->paData['NSERIAL']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL RECEPCIONAR TRAMITE";
         return false;
      }
      return true;
   }


   // ------------------------------------------------------------------------------
   // ws para informatica
   // 2019-03-15 LVA - LVA Creacion
   // ------------------------------------------------------------------------------
   public function omVerficiarUsuarioFirmas() {
      $llOk = $this->mxValParamVerficiarUsuarioFirmas();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerficiarUsuarioFirmas($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamVerficiarUsuarioFirmas() {
      if (!isset($this->paData['CCODUSU']) || !preg_match('(^[0-9]{4}$)', $this->paData['CCODUSU'])) {
         $this->pcError = "DNI NO DEFINIDO Y NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxVerficiarUsuarioFirmas($p_oSql) {
      $lcSql = "SELECT COUNT(*) FROM B04MAUT WHERE cCodUsu = '{$this->paData['CCODUSU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "ERROR AL VERIFICAR";
         return false;
      }
      if ($laFila[0] == 0) {
         $this->pcError = "USUARIO NO AUTORIZADO";
         return false;
      }
      return true;
   }
   
   // ------------------------------------------------------------------------------
   // Verificar revisores del grupo de la colacion seleccionada
   // 2019-04-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omVerificarEstadoRevisores() {
      $llOk = $this->mxValParamVerificarEstadoRevisores();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerificarEstadoRevisores($loSql);
      $loSql->omDisconnect();

      return $llOk;
   }

   protected function mxValParamVerificarEstadoRevisores() {
      if (!isset($this->paData['CIDCOLA']) || !preg_match('(^[0-9]{5}$)', $this->paData['CIDCOLA'])) {
         $this->pcError = "COLACION NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDA O NO VALIDA";
         return false;
      }
      return true;
   }

   protected function mxVerificarEstadoRevisores($p_oSql) {
      $lcSql = "SELECT A.cColUac FROM B05PUAC A
                INNER JOIN S01TCCO B ON B.cUniAca = A.cUniAca
                INNER JOIN B03PUSU C ON C.cCenCos = B.cCenCos AND C.cEstado = 'A'
                WHERE A.cIdCola = '{$this->paData['CIDCOLA']}' AND A.cUniAca = '{$this->paData['CUNIACA']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = "USUARIO SIN COLACION ASIGNADA";
         return false;
      }
      $lcColUac = $laTmp[0];
      $lcSql = "SELECT A.cCodDoc, C.cNombre, A.cEstado FROM B05PREV A
                INNER JOIN A01MDOC B ON B.cCodDoc = A.cCodDoc
                INNER JOIN S01MPER C ON C.cNroDni = B.cNroDni 
                WHERE A.cColUac = '$lcColUac' ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql); 
      $this->paDatos['paRevisor'] = []; 
      while($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paRevisor'][] = ['CCODDOC' => $laFila[0], 'CNOMBRE' => str_replace('/', ' ', $laFila[1]), 'CESTADO' => $laFila[2]];
      }
      if (count($this->paDatos['paRevisor']) != 3) {
         $this->pcError = "NRO DE REVISORES INCOMPLETO";
         return false;
      }
      $lcSql = "SELECT B.cCodAlu, A.cNombre, A.cNroDni, (TO_CHAR(B.tModifi, 'YYYY-MM-DD HH24:MI')) FROM V_A01MALU A 
               INNER JOIN B05DUAC B ON B.cCodAlu = A.cCodAlu AND A.cEstado = 'A' 
               WHERE B.cColUac = '$lcColUac'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['paAlumno'] = [];
      while($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['paAlumno'][] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1], 'CNRODNI' => $laFila[2], 'TMODIFI' => $laFila[3]];
      }
      if (count($this->paDatos['paAlumno']) == 0) {
         $this->pcError = "ALUMNOS NO ASIGNADOS";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Grabar solicitud de descuento por investigacion para bachiller 
   // 2019-05-22 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarSolicitudDescuentoInvestigacion() {
      $llOk = $this->mxValParamGrabarSolicitudDescuentoInvestigacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitudDescuentoInvestigacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarSolicitudDescuentoInvestigacion() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }  elseif (!isset($this->paData['CCODALU']) || strlen($this->paData['CCODALU']) != 10) {
         $this->pcError = 'CODIGO INVALIDO';
         return false;
      }
      return true;
   }

   protected function mxGrabarSolicitudDescuentoInvestigacion($p_oSql) {
      $lcSql = "SELECT CNRODNI FROM A01MALU WHERE CCODALU = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE ALUMNO ERRONEO";
         return false;
      }
      $lcSql = "SELECT * FROM B01DAUT WHERE CCODUSU = '{$this->paData['CCODUSU']}' AND CCODALU = '{$this->paData['CCODALU']}' AND CESTADO = 'E'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "PRESENTA SOLICITUD PENDIENTE";
         return false;
      }
      $lcSql = "SELECT * FROM B01DAUT WHERE CCODALU = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "ESTE ALUMNO YA SOLICITUD DE DESCUENTO";
         return false;
      }
      $lcSql = "INSERT INTO B01DAUT (cCodAlu, cCodUsu, cEstado, cTipAut, cCodEmp, cUsuCod)
                  VALUES('{$this->paData['CCODALU']}', '{$this->paData['CCODUSU']}', 'E', '002', '1015', '9999')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL MANDAR SOLICITUD';
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------------
   // Init bandeja de solicitudes de descuento por investigacion de bachiller 
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omIniBandejaSolicitudesDescuentoInvestigacion() {
      $llOk = $this->mxValParamInitBandejaSolicitudes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudesDescuentoInvestigacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudesDescuentoInvestigacion() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudesDescuentoInvestigacion($p_oSql) {
      $lcSql = "SELECT A.cCodAlu, A.cCodUsu, A.cEstado, A.cTipAut, TO_CHAR(A.tModifi, 'YYYY-MM-DD HH24:MI'), E.cNombre, D.cDescri, A.nSerial, G.cNombre
                FROM B01DAUT A
                INNER JOIN A01MALU B ON B.cCodAlu = A.cCodAlu
                LEFT JOIN S01TUSU C ON C.cCodUsu = A.cCodUsu
                LEFT JOIN B01TAUT D ON D.cTipAut = A.cTipAut
                LEFT JOIN S01MPER E ON E.cNroDni = B.cNroDni
                LEFT JOIN A01MDOC F ON F.cCodDoc = A.cUsuCod
                INNER JOIN  S01MPER G ON G.cNroDni = C.cNroDni
                WHERE A.cTipAut = '002' ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CCODUSU' => $laFila[1],
                             'CESTADO' => $laFila[2], 'CTIPAUT' => $laFila[3],
                             'TMODIFI' => $laFila[4], 'CNOMBRE' => $laFila[5],
                             'CDESCRI' => $laFila[6], 'NSERIAL' => $laFila[7],'CNOMPAD' => $laFila[8]];
      }
      return true;
   }

   // --------------------------------------------------------------------------------
   // Verifica si código de alumno existe
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omVerificarAlumnoxCod() {
      $llOk = $this->mxValParamVerificarAlumnoxCod();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxVerificarAlumnoxCod($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamVerificarAlumnoxCod() {  
      if (!isset($this->paData['CCODALU']) || strlen($this->paData['CCODALU']) != 10) {
         $this->pcError = 'CODIGO INVALIDO';
         return false;
      } elseif (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      } 
      return true;
   }

   protected function mxVerificarAlumnoxCod($p_oSql) {
      $lcSql = "SELECT cNombre FROM V_A01MALU WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE ALUMNO NO EXISTE";
         return false;
      }
      $this->paData = ['CNOMBRE' => $laFila[0]];
      return true;
   }

   // --------------------------------------------------------------------------------
   // Recuperar 
   // 2019-02-05 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omRecuperarPendientesInternado() {
      $llOk = $this->mxValParamRecuperarPendientesInternado();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarPendientesInternado($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarPendientesInternado() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO O NO VALIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarPendientesInternado($p_oSql) {
      $lcSql = "SELECT A.cCodTre, A.tFecha, B.cCodAlu, D.cNombre
                FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                INNER JOIN S01MPER D ON D.cNroDni = C.cNroDni
                WHERE A.cIdCate = 'PQ0009' AND A.cEstado = 'F'";
      $R1 = $p_oSql->omExec($lcSql);
   }

   // --------------------------------------------------------------------------------
   //  Mostrar Certificado de conducta para bachiller 
   //  2019-06-17 LVA Creacion
   // --------------------------------------------------------------------------------

   public function omIniMostrarCertificadoDeConducta(){
      $llOk = $this->mxValParamMostrarCertificadoDeConducta();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if(!$llOk){
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxIniMostrarCertificadoDeConducta($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamMostrarCertificadoDeConducta() {
      if (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxIniMostrarCertificadoDeConducta($p_oSql) {
      $lcSql = "SELECT C.cCodTre, D.cDescri, C.tModifi, C.cEstado FROM B03MDEU A INNER JOIN B03DDEU B ON A.cIdDeud = B.cIdDeud
                       INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog 
                       INNER JOIN B03TDOC D ON D.cIdCate = C.cIdCate
                       INNER JOIN A01MALU E ON E.cNroDni = A.cNroDni
                       INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni
                       INNER JOIN S01TUAC G ON G.cUniAca = E.cUniAca
                       INNER JOIN S01TCCO H ON H.cUniAca = G.cUniAca
                       INNER JOIN B03PUSU I ON I.cCenCos = H.cCenCos
                       WHERE C.cIdCate = 'PQ0016' AND C.cEstado = 'F' AND I.cCodUsu = '{$this->paData['CCODUSU']}' AND I.cEstado = 'A'";
      $this->paDatos = [];                
      $R1 = $p_oSql->omExec($lcSql);
      while ($lafila = $p_oSql->fetch($R1)){
         $this->paDatos[] = ['CCODTRE' => $lafila[0], 'CDESCRI' => $lafila[1],
                            'TMODIFI' => $lafila[2], 'CESTADO' => $lafila[3]];
      }
      return true;
   }

   // --------------------------------------------------------------------------------
   //  Aprobar Certificado de conducta para bachiller 
   //  2019-06-17 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omAprobarCertificadoDeConducta(){
      $llOk = $this->mxValAprobarCertificadoDeConducta();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if(!$llOk){
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAprobarCertificadoDeConducta($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValAprobarCertificadoDeConducta() {
      if (empty($this->paData['CCODTRE'])|| (strlen($this->paData['CCODTRE']))>6) {
         $this->pcError = "NUMERO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxAprobarCertificadoDeConducta($p_oSql) {
      $lcSql = "UPDATE B04MTRE SET cEstado = 'B', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRE']}'";                
      $llOk = $p_oSql->omExec($lcSql);
      return true;
   }

   // --------------------------------------------------------------------------------
   //  Observar Certificado de conducta para bachiller 
   //  2019-06-17 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omObervarCertificadoDeConducta(){
      $llOk = $this->mxValObservarCertificadoDeConducta();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if(!$llOk){
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxObservarCertificadoDeConducta($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValObservarCertificadoDeConducta() {
      if (empty($this->paData['CCODTRE'])|| (strlen($this->paData['CCODTRE']))>6) {
         $this->pcError = "NUMERO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxObservarCertificadoDeConducta($p_oSql) {
      $lcSql = "UPDATE B04MTRE SET cEstado = 'E', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRE']}'";                
      $llOk = $p_oSql->omExec($lcSql);
		if (!$llOk) {
         $this->pcError = "ERROR AL OBSERVAR TRAMITE";
			return false;
		}
      return true;
   }

   // --------------------------------------------------------------------------------
   //  Carga vista inicial Alumnos ITL
   //  2019-06-18 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omCargaDeAlumnoITL(){
      $llOk = $this->mxValCargAluITL();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if(!$llOk){
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargaDeAlumnoITL($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValCargAluITL() {
      if (empty($this->paData['CCODUSU'])|| (strlen($this->paData['CCODUSU']))>4) {
         $this->pcError = "NUMERO SERIAL NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxCargaDeAlumnoITL($p_oSql) {
      $lcSql = "SELECT A.nSerial, A.cCodAlu, C.cNombre, A.nNota, A.cAproba, A.cNivel FROM A05DTFL A 
                INNER JOIN A01MALU B ON A.cCodAlu = B.cCodAlu 
                INNER JOIN S01MPER C ON C.cNroDni = B.cNroDni
                ORDER BY A.nSerial ASC";                
      $llOk = $p_oSql->omExec($lcSql);
		if (!$llOk) {
         $this->pcError = "ERROR AL CARGAR ALUMNOS";
			return false;
      }
      while ($lafila = $p_oSql->fetch($llOk)){
         $this->paDatos[] = ['NSERIAL' => $lafila[0], 'CCODALU' => $lafila[1], 
                              'CNOMBRE' => $lafila[2], 'NNOTA' => $lafila[3], 
                              'CAPROBA' => $lafila[4], 'CNIVEL'=> $lafila[5]];
      }
      return true;
   }
   // --------------------------------------------------------------------------------
   //  Carga vista de Detalle para modificar Alumnos ITL
   //  2019-06-20 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omDetalleAlumnoITLMod(){
      $llOk = $this->mxValDetalleAlumnoITLMod();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if(!$llOk){
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDetalleAlumnoITLMod($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValDetalleAlumnoITLMod() {
      if (empty($this->paData['NSERIAL'])|| (strlen($this->paData['NSERIAL']))<=0) {
         $this->pcError = "NUMERO DE SERIAL INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxDetalleAlumnoITLMod($p_oSql) {
      $lcSql = "SELECT nSerial, nNota, cAproba, cNivel FROM A05DTFL 
                WHERE nSerial = '{$this->paData['NSERIAL']}'"; 
      $llOk = $p_oSql->omExec($lcSql);
		if (!$llOk) {
         $this->pcError = "ERROR AL CARGAR DATOS";
			return false;
      }
      $lafila = $p_oSql->fetch($llOk);
      $this->paDatos[] = ['NSERIAL' => $lafila[0], 'NNOTA' => $lafila[1], 'CAPROBA' => $lafila[2],
                            'CNIVEL' => $lafila[3]];
      return true;
   }

   // --------------------------------------------------------------------------------
   //  Update de los datos cambiados en el detalle Alumnos ITL
   //  2019-06-21 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omDetalleAlumnoITLUpd(){
      $llOk = $this->mxValDetalleAlumnoITLUpd();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if(!$llOk){
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxDetalleAlumnoITLUpd($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValDetalleAlumnoITLUpd() {
      if (empty($this->paData['NSERIAL']) || (strlen($this->paData['NSERIAL']))<=0) {
         $this->pcError = "NUMERO DE SERIAL INCORRECTO";
         return false;
      }
      /*if(!preg_match('(^[0-9]{3}$)', $this->paData['NNOTA'])){
         $this->pcError = 'FORMATO INCORRECTO, SOLO NUMEROS Y DE 3 DIGITOS';
         return false;
      }
      if(!preg_match('(^[N|S]$)',$this->paData['CAPROBA'])){
         $this->pcError = 'FORMATO INCORRECTO, Aprobado (S), Desaprobado (N)';
         return false;
      }
      if(!preg_match('(^[B1|A1|A2|B2|C1]$)',$this->paData['CNIVEL'])){
         $this->pcError = 'FORMATO INCORRECTO, NIVELES (A1,A2,B1,B2,C1)';
         return false;
      }*/
      return true;
   }

   protected function mxDetalleAlumnoITLUpd($p_oSql) {
      $lcSql = "UPDATE A05DTFL SET nNota = '{$this->paData['NNOTA']}', 
                cAproba = '{$this->paData['CAPROBA']}', 
                cNivel = '{$this->paData['CNIVEL']}',
                cUsuCod = '{$this->paData['CCODUSU']}', tModifi = NOW() 
                WHERE nSerial = '{$this->paData['NSERIAL']}'";                 
      $llOk = $p_oSql->omExec($lcSql);
		if (!$llOk) {
         $this->pcError = "ERROR AL CAMBIAR LOS DATOS";
			return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Grabar solicitud de descuento por fallecimiento de padres para bachiller 
   // 2019-05-22 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarSolicitudDescuentoBachiller() {
      $llOk = $this->mxValParamGrabarSolicitudDescuentoBachiller();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitudDescuentoBachiller($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarSolicitudDescuentoBachiller() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO";
         return false;
      }  elseif (!isset($this->paData['CCODALU']) || strlen($this->paData['CCODALU']) != 10) {
         $this->pcError = 'CODIGO INVALIDO';
         return false;
      }  elseif (!isset($this->paData['NDESCUP']) OR (strlen($this->paData['NDESCUP']) > 3) OR !preg_match("(^[0-9]+$)", $this->paData['NDESCUP'])) {
         $this->pcError = 'PARAMETRO DE DESCUENTO INVALIDO O NO DEFINIDO';
         return false;
      }
      return true;
   }

   protected function mxGrabarSolicitudDescuentoBachiller($p_oSql) {
      $lcDescu = $this->paData['NDESCUP']/100;
      $lcSql = "SELECT CNRODNI FROM A01MALU WHERE CCODALU = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE ALUMNO ERRONEO";
         return false;
      }
      $lcSql = "SELECT * FROM B01DAUT WHERE CCODUSU = '{$this->paData['CCODUSU']}' AND CCODALU = '{$this->paData['CCODALU']}' AND CESTADO = 'E'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "PRESENTA SOLICITUD PENDIENTE";
         return false;
      }
      $lcSql = "SELECT * FROM B01DAUT WHERE CCODALU = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "ESTE ALUMNO YA SOLICITUD DE DESCUENTO";
         return false;
      }
      $lcSql = "INSERT INTO B01DAUT (cCodAlu, cCodUsu, cEstado, cTipAut, cCodEmp, nDescue, cUsuCod)
                  VALUES('{$this->paData['CCODALU']}', '{$this->paData['CCODUSU']}', 'A', '003', '1015', $lcDescu, '9999')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL MANDAR SOLICITUD';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar archivo 
   // 2019-06-18 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omCargarArchivoDetalle() {
      $llOk = $this->mxValCargarArchivoDetalle();
      if (!$llOk) {
         return False;
      }
      $llOk = $this->mxSubirArchivoTemporalCSV();
      if (!$llOk) {
         return False;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxCargarArchivoDetalle($loSql);
      if (!$llOk) {
         return False;
      }
      return $llOk;
   }

   protected function mxValCargarArchivoDetalle() {
      if (!isset($this->paData['CUSUCOD']) || strlen($this->paData['CUSUCOD']) != 4) {
         $this->pcError = 'CODIGO DE USUARIO INVÁLIDO';
         return false;
      } elseif (!isset($this->paData['CCENCOS']) || strlen($this->paData['CCENCOS']) != 3) {
         $this->pcError = 'CENTRO DE COSTO INVÁLIDO';
         return false;
      }
      return True;
   }

   protected function mxSubirArchivoTemporalCSV() {
      $laErrFil = array(
         1 => 'EL ARCHIVO NO FUE CARGADO',
      );
      $lcUsuCod = $this->paData['CUSUCOD'];
      $lcCenCos = $this->paData['CCENCOS'];
      if ($this->poFile['error'] != 0 && $this->poFile['error'] != 4) {
         $this->pcError = $laErrFil[$this->poFile['error']];
         return false;
      } elseif ($this->poFile['error'] == 1) {
         return true;
      }
      $llOk = fxSubirCSV($this->poFile, 'FILES', $lcUsuCod.$lcCenCos);
      if(!$llOk) { 
         $this->pcError = "HA OCURRIDO UN ERROR AL SUBIR ARCHIVO ".$this->poFile['name'];
         return false;
      }
      return true;
   }

   protected function mxCargarArchivoDetalle($p_oSql) {
      $lcUsuCod = $this->paData['CUSUCOD'];
      $lcCenCos = $this->paData['CCENCOS'];
      $this->paDatos = null;
      $loArchiv = fopen("FILES/".$lcUsuCod.$lcCenCos.".csv", "r");
      while (($laTmp = fgetcsv($loArchiv, 4, ',')) == true) {
         //print_r($laTmp); die;
         $lcSql = "SELECT A.cCodArt, A.cDescri, A.cUnidad, B.cDescri AS cDesUni FROM E01MART A 
                     LEFT OUTER JOIN V_S01TTAB B ON B.cCodTab = '074' AND B.cCodigo = A.cUnidad
                     WHERE A.cCodArt = '{$laTmp[0]}'";
         $RS = $p_oSql->omExec($lcSql);
         if (!$RS) {
            $this->pcError = 'ERROR DE EJECUCIÓN EN BASE DE DATOS';
            continue;
         }
         $laFila = $p_oSql->fetch($RS);
         $this->paDatos[] = ['CCODART' => $laTmp[0], 'CDESART' => (!isset($laFila[1]) || $laFila[0] == '00000000')? substr($laTmp[1], 0, 200) : $laFila[1], 
                             'CDESDET' => $laTmp[1], 'NCANTID' => $laTmp[2], 'NPREREF' => $laTmp[3], 'CUNIDAD' => (!isset($laFila[4]))? 'UNIDAD' : $laFila[4], 
                             'NSTOTAL' => $laTmp[2] * $laTmp[3]];
      }
      //Cerramos el archivo
      fclose($loArchiv);
      return True;
   }

   // ------------------------------------------------------------------------------
   // Cargar archivo 
   // 2019-06-18 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitAutenticacionDocumentos() {
      $llOk = $this->mxValAutenticacionDocumentos();
      if (!$llOk) {
         return False;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAutenticacionDocumentos($loSql);
      if (!$llOk) {
         return False;
      }
      return $llOk;
   }

   protected function mxValAutenticacionDocumentos() {
      if (!isset($this->paData['CNRODNI']) || strlen($this->paData['CNRODNI']) != 8) {
         $this->pcError = 'CODIGO DE USUARIO INVÁLIDO';
         return false;
      } elseif (!isset($this->paData['CCODALU']) || strlen($this->paData['CCODALU']) != 10) {
         $this->pcError = 'CENTRO DE COSTO INVÁLIDO';
         return false;
      }
      return True;
   }

   protected function mxAutenticacionDocumentos($p_oSql) {
      $lcSql = "SELECT A.cNroDni FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog AND C.cIdCate = 'CCESTU'
                WHERE A.cNroDni = '{$this->paData['CNRODNI']}' AND B.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado = 'C' AND A.cPaquet = 'B' AND C.cEtapa = 'D'";                
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) { //sin bachiller Online
         $lcSql = "SELECT cIdCate, cDescri, nMonto FROM B03TDOC WHERE CIDCATE IN ('PDACBU','PDADOC')";    
         $R1 = $p_oSql->omExec($lcSql);
         $this->paDatos['paDocAut'] = [];
         while ($laFila = $p_oSql->fetch($R1)){
            $this->paDatos['paDocAut'][] = ['CIDCATE' => $laFila[0], 'CDESCRI' => $laFila[1], 'NMONTO' => $laFila[2]];
         }
         if(count($this->paDatos['paDocAut']) == 0){
            $this->pcError = 'NO SE ENCONTRARON LOS CONCEPTOS DE PAGO';
            return false;
         }
      } elseif (isset($laFila[0])) { //con Bachiller Online
         $this->pcError = 'USTED REALIZO BACHILLER EN LINEA POR FAVOR ELIJA LA SEGUNDA OPCION';
         return false;
         //$lcSql = "SELECT cIdCate, cDescri, nMonto FROM B03TDOC WHERE CIDCATE IN ('PDAOTR','PDADOC')";
         //$R1 = $p_oSql->omExec($lcSql);
         //$this->paDatos['paDocAut'] = [];
         //while ($laFila = $p_oSql->fetch($R1)){
         //   $this->paDatos['paDocAut'][] = ['CIDCATE' => $laFila[0], 'CDESCRI' => $laFila[1], 'NMONTO' => $laFila[2]];
         //}
         //if(count($this->paDatos['paDocAut']) == 0){
         //   $this->pcError = 'NO SE ENCONTRARON LOS CONCEPTOS DE PAGO';
         //   return false;
         //}
      }
      $lcSql = "SELECT B.cNroCel, B.cEmail FROM A01MALU A INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni WHERE A.cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp)) {
         $this->pcError = "ERROR AL RECUPERAR INFORMACIÓN DEL ALUMNO";
         return false;
      }
      $laData['CNROCEL'] = $laTmp[0];
      $laData['CEMAIL'] = $laTmp[1];
      $this->paData = $laData;
      return true;
   }

   // ------------------------------------------------------------------------------
   // Grabar curso por jurado a alumno
   // 2019-02-13 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGrabarDeudaAutenticacion() {
      $llOk = $this->mxValParamGrabarDeudaAutenticacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarDeudaAutenticacion($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamGrabarDeudaAutenticacion() {
      if (!isset($this->paData['CCODALU']) || !preg_match('(^[0-9]{10}$)', $this->paData['CCODALU'])) {
         $this->pcError = "CODIGO DE ALUMNO NO DEFINIDO O NO VALIDO";
         return false;
      } if (!isset($this->paData['CIDCATE']) || count($this->paData['CIDCATE']) == 0) {
         $this->pcError = "SIN CURSOS SELECCIONADOS";         
         return false;
      }
      return true;
   }

   protected function mxGrabarDeudaAutenticacion($p_oSql) {
      $laData=[];
      foreach ($this->paData['CIDCATE'] as $laFila) {
         $laData = $laData + [$laFila => $laFila];  
      }
      $laData = $laData + ['CCODALU' => $this->paData['CCODALU'], 'CCODUSU' => $this->paData['CCODUSU']];
      $lcJson = json_encode($laData);
      $lcSql = "SELECT P_B03MDEU_14('$lcJson')";;
      $R1 = $p_oSql->omExec($lcSql);
      $laTmp = $p_oSql->fetch($R1);
      if (!isset($laTmp[0])) {
         $this->pcError = 'ERROR EN EJECUCION DE COMANDO SQL';
         return false;
      }
      $laData = json_decode($laTmp[0], true);
      if (isset($laData['CNROPAG'])) {
         $this->paData = $laData;
         return true;
      } elseif (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
      } else {
         $this->pcError = 'ERROR EN RETORNO DE COMANDO SQL';
      }
      return false;
   }

   // --------------------------------------------------------------------------------
   // Init bandeja de solicitudes de descuento de Curso por Jurado
   // 2019-06-25 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omIniBandejaSolicitudesDescuenCPJ() {
      $llOk = $this->mxValParamInitBandejaSolicitudesDescuentoCPJ();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaSolicitudesDescuenCPJ($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudesDescuentoCPJ() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaSolicitudesDescuenCPJ($p_oSql) {
      $lcSql = "SELECT A.cCodAlu, B.cNombre, C.cDescri, A.cEstado
                FROM B01DAUT A
                INNER JOIN V_A01MALU B ON A.cCodAlu = B.cCodAlu
                INNER JOIN B01TAUT C ON c.cTipAut = A.cTipAut
                WHERE A.cEstado = 'E' AND A.cTipAut = '004'
                ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CDESCRI' => $laFila[2], 'CESTADO' => $laFila[3]];
      }
      if (!$R1) {
         $this->pcError = 'ERROR DE EJECUCIÓN EN BASE DE DATOS';
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------------
   // Grabar solicitudes de descuento de Curso por Jurado
   // 2019-06-25 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omGrabarSolicitudDescuenCPJ() {
      $llOk = $this->mxValGrabarSolicitudDescuenCPJ();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitudDescuenCPJ($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValGrabarSolicitudDescuenCPJ() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxGrabarSolicitudDescuenCPJ($p_oSql) {
      
      $lcSql = "SELECT * FROM V_A01MALU WHERE CCODALU = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE ALUMNO ERRONEO";
         return false;
      }
      $lcSql = "SELECT * FROM B01DAUT WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "PRESENTA SOLICITUD PENDIENTE";
         return false;
      }
      $lcDescu = $this->paData['NDESCUE']/100;
      $lcSql = "INSERT INTO B01DAUT (cCodAlu, cCodUsu, cEstado, cTipAut, cCodEmp, tProces, cObserv, nDescue, cUsuCod, tModifi)
                  VALUES('{$this->paData['CCODALU']}', '{$this->paData['CCODUSU']}', 'E', '004', '1015', NULL,
                  NULL, {$lcDescu},'U666', NOW())";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL MANDAR SOLICITUD';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Solicitud de Clinica (Odontologia)
   // 2019-07-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaClinica() {
      $llOk = $this->mxValParamInitBandejaClinica();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaClinica($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaClinica() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaClinica($p_oSql) {
      $lcSql = "SELECT  C.cCodTre, A.cNroDni, B.cCodAlu, D.cNombre , F.cDescri, TO_CHAR(C.tFecha, 'yyyy-mm-dd hh24:mi') 
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog
                INNER JOIN S01MPER D ON D.cNroDni = A.cNroDni
                INNER JOIN A01MALU E ON E.cCodAlu = B.cCodAlu
                INNER JOIN B03TDOC F ON F.cIdCate = C.cIdCate
                WHERE C.cIdCate = 'CCCONC' AND C.cEstado IN ('F') ";
      $llOk = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($llOk)){
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'CNRODNI' => $laFila[1],
                             'CCODALU' => $laFila[2], 'CNOMBRE' => $laFila[3], 
                             'CDESCRI' => $laFila[4], 'TFECHA' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Aprobar Solicitud de Clinica (Odontologia)
   // 2019-07-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAprobarSolicitudCli() {
      $llOk = $this->mxValParamAprobarSolicitudCli();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAprobarSolicitudCli($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAprobarSolicitudCli() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CCODTRE']) || strlen($this->paData['CCODTRE'])!= 6) {
         $this->pcError = "TRAMITE NO DEFINIDO O INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxAprobarSolicitudCli($p_oSql) {
      $lcSql = "UPDATE B04MTRE SET cEstado ='R', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Observar Solicitud de Clinica (Odontologia)
   // 2019-07-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omObservSolicitudCli() {
      $llOk = $this->mxValParamObservSolicitudCli();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxObservSolicitudCli($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamDenegarSolicitudCli() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CCODTRE']) || strlen($this->paData['CCODTRE'])!= 6) {
         $this->pcError = "TRAMITE NO DEFINIDO O INVALIDO";
         return false;
      }
      if((preg_match_all("/\s/",$this->paData['MOBSERV'])) == strlen($this->paData['MOBSERV'])){
         $this->pcError = "ESCRÍBA ALGUNA OBSERVACIÓN";
         return false;
      }
      return true;
   }

   protected function mxObservSolicitudCli($p_oSql) {
      $lcFec = date("Y-m-d h:i:s");
      $lcObs = mb_strtoupper($this->paData['MOBSERV'], 'UTF-8');
      $lcSql = "UPDATE B04MTRE SET cEstado ='E', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW(), mObserv = '* {$lcFec} .{$this->paData['CCODUSU']}. - {$lcObs}' WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      return true;
   }
   
   // ------------------------------------------------------------------------------
   // Init Bandeja Solicitud de Clinica (Odontologia)
   // 2019-07-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaConstanciaClinicaEscuela() {
      $llOk = $this->mxValParamInitBandejaConstanciaClinicaEscuela();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaConstanciaClinicaEscuela($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaConstanciaClinicaEscuela() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaConstanciaClinicaEscuela($p_oSql) {
      $lcSql = "SELECT  C.cCodTre, A.cNroDni, B.cCodAlu, D.cNombre , F.cDescri, TO_CHAR(C.tFecha, 'yyyy-mm-dd hh24:mi') 
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog
                INNER JOIN S01MPER D ON D.cNroDni = A.cNroDni
                INNER JOIN A01MALU E ON E.cCodAlu = B.cCodAlu
                INNER JOIN B03TDOC F ON F.cIdCate = C.cIdCate
                WHERE C.cIdCate IN ('CCCONC','PQ0023') AND C.cEstado IN ('R')";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'CNRODNI' => $laFila[1],
                             'CCODALU' => $laFila[2], 'CNOMBRE' => $laFila[3], 
                             'CDESCRI' => $laFila[4], 'TFECHA' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Aprobar Solicitud de Clinica (Odontologia)
   // 2019-07-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAprobarConstanciaClinicaEscuela() {
      $llOk = $this->mxValParamAprobarConstanciaClinicaEscuela();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAprobarConstanciaClinicaEscuela($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      
      $llOk = $this->mxPrintReporteExpedienteConsClini();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamAprobarConstanciaClinicaEscuela() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU']) != 4) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CCODTRE']) || strlen($this->paData['CCODTRE'])!= 6) {
         $this->pcError = "TRAMITE NO DEFINIDO O INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxAprobarConstanciaClinicaEscuela($p_oSql) {
      $lcSql = "UPDATE B04MTRE SET cEstado = 'B', cCodUsu = {$this->paData['CCODUSU']}, tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRE']}'"; 
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL APROBAR CONSTANCIA DE CLINICA";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Generacion de constancia de clinica escuela Secr. (Obstetricia)
   // 2019-07-22 LVA Creacion
   // ------------------------------------------------------------------------------
   protected function mxPrintReporteExpedienteConsClini() {
      $fecha_actual = date ("Y-m-d");
      $lcCodTre = $this->paData['CCODTRE'];
      $lcNroDni = $this->paData['CNRODNI'];
      $loDate = new CDate();
      $lcPath = "./EXP/D$lcNroDni/P$lcCodTre.pdf"; 
      try {
         $fecha_actual = date ("Y-m-d");
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->SetFont('Courier','B',20);
         $pdf->Cell(186, 10, utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 0, 'C');
         $pdf->Ln(15);
         $pdf->SetFont('Courier','B',12);
         $pdf->Cell(186, 10, utf8_decode('EXPEDIENTE: E-' . $lcCodTre), 0, 0, 'L');
         $pdf->Ln(15);
         $pdf->SetFont('Courier','BU',15);
         $pdf->Cell(186, 10, utf8_decode('CONSTANCIA Nº'. $lcCodTre.'-FOyP-2019'), 0, 0, 'C');
         $pdf->Ln(15);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Cell(186, 10, utf8_decode('LA QUE SUSCRIBE, DECANA DE LA FACULTAD  DE  ODONTOLOGIA DE LA UNIVERSIDAD'), 0, 0, 'L');
         $pdf->Ln(10);
         $pdf->Cell(186, 10, utf8_decode('CATOLICA DE "SANTA MARÍA" DE AREQUIPA'), 0, 0, 'L');
         $pdf->Ln(15);
         $pdf->Cell(186, 10, utf8_decode('Hace constar que el(la)'), 0, 0, 'L');
         $pdf->SetFont('Courier', 'B', 14);
         $pdf->Ln(15);
         $pdf->SetX(23);
         $pdf->Cell(160,5,utf8_decode('Sr.(ta.)'.str_replace('/', ' ',$this->paData['CNOMBRE'])), 0, 0,'C');
         $pdf->Ln(10);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Cell(186, 10, utf8_decode('NO ADEUDA por ningún concepto de la Clinica Odontológica material u otros'), 0, 0, 'L');
         $pdf->Ln(10);
         $pdf->Cell(186, 10, utf8_decode('según informe del señor Director de la Clínica que obra en el expediente.'), 0, 0, 'L');
         $pdf->Ln(15);
         $pdf->Cell(186, 10, utf8_decode('Se expide la presente,  a  solicitud  expresa  de  la recurrente para los'), 0, 0, 'L');
         $pdf->Ln(10);
         $pdf->Cell(186, 10, utf8_decode('que estime por conveniente'), 0, 0, 'L');
         $pdf->SetFont('Courier', 'B', 11);
         $pdf->Ln(15);
         $pdf->Cell(67, 12, utf8_decode("Arequipa, ". $loDate->dateSimpleText($fecha_actual)), 0, 0, 'R');
         $pdf->Ln(15);
         $llOk = $pdf->Output($lcPath, "F");
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Solicitud de Clinica Lab Observados (Odontologia)
   // 2019-07-08 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaClinicaObser() {
      $llOk = $this->mxValParamInitBandejaClinicaObser();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaClinicaObser($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaClinicaObser() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaClinicaObser($p_oSql) {
      $lcSql = "SELECT  C.cCodTre, A.cNroDni, B.cCodAlu, D.cNombre , F.cDescri, TO_CHAR(C.tFecha, 'yyyy-mm-dd hh24:mi') 
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog
                INNER JOIN S01MPER D ON D.cNroDni = A.cNroDni
                INNER JOIN A01MALU E ON E.cCodAlu = B.cCodAlu
                INNER JOIN B03TDOC F ON F.cIdCate = C.cIdCate
                WHERE C.cIdCate = 'CCCONC' AND C.cEstado IN ('E') ";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'CNRODNI' => $laFila[1],
                            'CCODALU' => $laFila[2], 'CNOMBRE' => $laFila[3], 
                            'CDESCRI' => $laFila[4], 'TFECHA' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
  }

  // ------------------------------------------------------------------------------
   // Init Bandeja Solicitud de Clinica Lab para Observacion (Odontologia)
   // 2019-07-09 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omBandejaClinicaLevObser() {
      $llOk = $this->mxValParamBandejaClinicaLevObser();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxBandejaClinicaLevObser($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamBandejaClinicaLevObser() {
      if (!isset($this->paData['CCODUSU'])|| strlen($this->paData['CCODTRE'])!= 6) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxBandejaClinicaLevObser($p_oSql) {
      $lcSql = "UPDATE B04MTRE SET cEstado ='A',cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      return true;
  }
   
   // --------------------------------------------------------------------------------
   // Init bandeja de solicitudes de descuento bachiller por primeros puestos
   // 2019-07-08 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omIniBandejaSolicitudesDescuenPrimPues() {
      $llOk = $this->mxValParamInitBandejaSolicitudesDescuentoCPJ();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxIniBandejaSolicitudesDescuenPrimPues($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaSolicitudesDescuentoPrimPues() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxIniBandejaSolicitudesDescuenPrimPues($p_oSql) {
      $lcSql = "SELECT A.cCodAlu, B.cNombre, C.cDescri, A.cEstado
                FROM B01DAUT A
                INNER JOIN V_A01MALU B ON A.cCodAlu = B.cCodAlu
                INNER JOIN B01TAUT C ON c.cTipAut = A.cTipAut
                WHERE A.cEstado = 'E' AND A.cTipAut = '004'
                ORDER BY A.nSerial";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODALU' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CDESCRI' => $laFila[2], 'CESTADO' => $laFila[3]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN SOLICITUDES PENDIENTES";
         return true;
      }
      return true;
   }

   // --------------------------------------------------------------------------------
   // Grabar solicitudes de descuento bachiller por primeros puestos
   // 2019-07-08 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omGrabarSolicitudDescuenPrimPues() {
      $llOk = $this->mxValGrabarSolicitudDescuenPrimPues();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitudDescuenPrimPues($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValGrabarSolicitudDescuenPrimPues() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxGrabarSolicitudDescuenPrimPues($p_oSql) {
      $lcSql = "SELECT * FROM V_A01MALU WHERE CCODALU = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "CODIGO DE ALUMNO ERRONEO";
         return false;
      }
      $lcSql = "SELECT * FROM B01DAUT WHERE cCodAlu = '{$this->paData['CCODALU']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = "PRESENTA SOLICITUD PENDIENTE";
         return false;
      }
      $lcDescu = $this->paData['NDESCUE']/100;
      $lcSql = "INSERT INTO B01DAUT (cCodAlu, cCodUsu, cEstado, cTipAut, cCodEmp, tProces, cObserv, nDescue, cUsuCod, tModifi)
                  VALUES('{$this->paData['CCODALU']}', '{$this->paData['CCODUSU']}', 'E', '003', '1015', NULL,
                  NULL, {$lcDescu},'U666', NOW())";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL MANDAR SOLICITUD';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Solicitud de Constancia de Internado (Obstetricia)
   // 2019-07-15 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaConsInter() {
      $llOk = $this->mxValParamInitBandejaConsInter();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaConsInter($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaConsInter() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaConsInter($p_oSql) {
      $lcSql = "SELECT  C.cCodTre, A.cNroDni, B.cCodAlu, D.cNombre , F.cDescri, B.cRecibo
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog
                INNER JOIN S01MPER D ON D.cNroDni = A.cNroDni
                INNER JOIN A01MALU E ON E.cCodAlu = B.cCodAlu
                INNER JOIN B03TDOC F ON F.cIdCate = C.cIdCate
                WHERE C.cIdCate = 'PQ0009' AND C.cEstado = 'F' ";
      $llOk = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laFila = $p_oSql->fetch($llOk)){
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'CNRODNI' => $laFila[1],
                             'CCODALU' => $laFila[2], 'CNOMBRE' => $laFila[3], 
                             'CDESCRI' => $laFila[4], 'CRECIBO' => $laFila[5]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // --------------------------------------------------------------------------------
   // Grabar solicitudes de Constancia de Internado (Obstetricia)
   // 2019-07-15 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omGrabarSolicitudConstInter() {
      $llOk = $this->mxValGrabarSolicitudConstInter();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGrabarSolicitudConstInter($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $llOk = $this->mxPrintReporteExpedienteConsInter();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValGrabarSolicitudConstInter() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxGrabarSolicitudConstInter($p_oSql) {
      $lcDetall = '{';
      $cont = 0;
      while($cont < (sizeof($this->paData)-18)/4){
         $lcDetall = $lcDetall . '"CCURSO" :' . '"' .mb_strtoupper("{$this->paData["CASIGNA$cont"]}",'UTF-8') . '",' .
                     '"CNOTA" :' . '"' ."{$this->paData["CNOTA$cont"]}" . '",' .
                     '"CCAMCLI" :' . '"' .mb_strtoupper("{$this->paData["CCAMCLI$cont"]}",'UTF-8') . '",'.
                     '"CDURACI" :' . '"' ."{$this->paData["CDURACI$cont"]}" . '",';
         $this->paDatos[] = ['CCURSO' => mb_strtoupper(str_replace("\n",' ',$this->paData["CASIGNA$cont"]),'UTF-8'), 
                             'CNOTA' => $this->paData["CNOTA$cont"],
                             'CCAMCLI' => mb_strtoupper(str_replace("\n",' ',$this->paData["CCAMCLI$cont"]),'UTF-8'),
                             'CDURACI' => $this->paData["CDURACI$cont"]];
         $cont++;
      }
      $lcDetall = $lcDetall . '}';
      $lcSql = "UPDATE B04MTRE SET cCodUsu = {$this->paData['CCODUSU']},tModifi = NOW() , cEstado ='B', mDetall = '{$lcDetall}' WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL MANDAR SOLICITUD';
         return false;
      }
      return true;
   }
   protected function round_up($value, $places) 
   {
      $mult = pow(10, abs($places)); 
         return $places < 0 ?
      ceil($value / $mult) * $mult :
         ceil($value * $mult) / $mult;
   }

   protected function mxPrintReporteExpedienteConsInter() {
      $cont = 0;
      $loDate = new CDate();
      $lcCodTre = $this->paData['CCODTRE'];
      $lcNroDni = $this->paData['CDNIALU'];
      $lcPath= "./EXP/D$lcNroDni/P$lcCodTre.pdf";
      try {
         $fecha_actual = date ("Y-m-d");
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->SetFont('Courier','B',20);
         $pdf->Cell(186, 10, utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 0, 'C');
         $pdf->Ln(15);
         $pdf->SetFont('Courier','B',10);
         $pdf->Cell(186, 10, utf8_decode('EXPEDIENTE: E-' . $this->paData['CCODTRE']), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('RECIBO: '. $this->paData['CRECIBO']), 0, 0, 'L');
         $pdf->Ln(15);
         $pdf->SetFont('Courier','BU',17);
         $pdf->Cell(186, 10, utf8_decode('CONSTANCIA Nº'. $this->paData['CCODTRE'].'-FOyP-2019'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Cell(186, 10, utf8_decode('LA DECANA DE LA FACULTAD DE OBSTETRICIA Y PUERICULTURA DE LA UNIVERSIDAD'), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('CATÓLICA DE SANTA MARÍA DE AREQUIPA QUE SUSCRIBE, HACE CONSTAR QUE:'), 0, 0, 'L');
         $pdf->Ln(10);
         $pdf->SetFont('Courier', 'B', 12);
         $pdf->Ln(5);
         $pdf->SetX(23);
         $pdf->Multicell(160,5,utf8_decode(str_replace('/',' ',$this->paData['CNOMALU'])."\n".$this->paData['CCODALU']),1,'C');
         $pdf->Ln(5);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Cell(186, 10, utf8_decode('HA CONCLUÍDO Y APROBADO SATISFACTORIAMENTE SU INTERNADO'), 0, 0, 'L');
         $pdf->Ln(15);
         $pdf->SetFont('Courier', 'B', 12);
         $pdf->SetX(17);
         $pdf->Cell(62, 8, utf8_decode("ASIGNATURA"), 1, 0);
         $pdf->Cell(15, 8, utf8_decode("NOTA"), 1, 0);
         $pdf->Cell(69, 8, utf8_decode("CAMPO CLÍNICA"), 1, 0);
         $pdf->Cell(27, 8, utf8_decode("DURACIÓN"), 1, 0);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Ln(8);
         $EjeY = 143;
         foreach ($this->paDatos as $laFila) {            
            $lcCursoTam =strlen($laFila['CCURSO'])/23;
            $lcCamcliTam =strlen($laFila['CCAMCLI'])/26;
            $lcCellTam = 5;
            $lcPaddCur = $this->round_up($lcCursoTam, 0);
            $lcPaddCam = $this->round_up($lcCamcliTam, 0);
            print_r($lcCursoTam);
            $pdf->SetX(17);
            if(strlen($laFila['CCURSO']) > strlen($laFila['CCAMCLI']) and $lcCursoTam > 1){
               $pdf->Multicell(62, $lcCellTam, utf8_decode($laFila['CCURSO']), 1,'C');
               $pdf->SetXY(79,$EjeY);
               $pdf->Multicell(15,$lcCellTam * $lcPaddCur, utf8_decode($laFila['CNOTA']), 1,'C');
               $pdf->SetXY(94,$EjeY);
               if ( $lcCamcliTam <= 1 ){
                  $pdf->Multicell(69, $lcCellTam * $lcPaddCur, utf8_decode($laFila['CCAMCLI']), 1);
               } else {
                  $pdf->Multicell(69, $lcCellTam , utf8_decode($laFila['CCAMCLI']), 1, 'C');
               }
               $pdf->SetXY(163,$EjeY);
               $pdf->Multicell(27, $lcCellTam * $lcPaddCur, utf8_decode($laFila['CDURACI']. " MESES"), 1);
               $EjeY = $EjeY + ($lcCellTam * $lcPaddCur);
            }
            elseif(strlen($laFila['CCURSO']) < strlen($laFila['CCAMCLI']) and $lcCamcliTam > 1){
               if($lcCursoTam <= 1){
                  $pdf->Multicell(62, $lcCellTam * $lcPaddCam, utf8_decode($laFila['CCURSO']), 1,'C');
               } elseif ($lcPaddCur == ($lcPaddCam/2)) {
                  $pdf->Multicell(62, $lcCellTam * $lcPaddCur, utf8_decode($laFila['CCURSO']), 1,'C');
               } else {
                  $pdf->Multicell(62, $lcCellTam  , utf8_decode($laFila['CCURSO']), 1,'C');
               }
               $pdf->SetXY(79,$EjeY);
               $pdf->Multicell(15, $lcCellTam * $lcPaddCam, utf8_decode($laFila['CNOTA']), 1,'C');
               $pdf->SetXY(94,$EjeY);
               $pdf->Multicell(69, $lcCellTam , utf8_decode($laFila['CCAMCLI']), 1, 'C');
               $pdf->SetXY(163,$EjeY);
               $pdf->Multicell(27, $lcCellTam * $lcPaddCam, utf8_decode($laFila['CDURACI']. " MESES"), 1);
               $EjeY = $EjeY + ($lcCellTam * $lcPaddCam);
            } else {
               $pdf->Multicell(62, 8, utf8_decode($laFila['CCURSO']), 1,'C');
               $pdf->SetXY(79,$EjeY);
               $pdf->Multicell(15, 8, utf8_decode($laFila['CNOTA']), 1,'C');
               $pdf->SetXY(94,$EjeY);
               $pdf->Multicell(69, 8, utf8_decode($laFila['CCAMCLI']), 1);
               $pdf->SetXY(163,$EjeY);
               $pdf->Multicell(27, 8, utf8_decode($laFila['CDURACI']. " MESES"), 1);
               $EjeY = $EjeY + ($lcCellTam * $lcPaddCam);
            }
         }
         $pdf->Ln(15);
         $pdf->Cell(186, 10, utf8_decode('NOTAS QUE CONSTAN EN LAS FICHAS DE LA EVALUACIÓN DE CADA UNIDAD.'), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('SE EXPIDE LA PRESENTE CONSTANCIA A LA SOLICITUD DE LA INTERESADA PARA'), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('LOS FINES QUE CREA CONVENIENTES'), 0, 0, 'L');
         $pdf->Ln(10);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Cell(186, 10, utf8_decode('Arequipa, '.$loDate->dateSimpleText($fecha_actual)), 0, 0, 'L');
         $llOk = $pdf->Output($lcPath, "F");
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Observar Solicitud de Constancia de Internado (Obstetricia)
   // 2019-07-16 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omObservSolicitudConsInter() {
      $llOk = $this->mxValParamObservSolicitudConsInter();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxObservSolicitudConsInter($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamObservSolicitudConsInter() {
      if (!isset($this->paData['CCODUSU'])|| strlen($this->paData['CCODTRE'])!= 6) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      if((preg_match_all("/\s/",$this->paData['MOBSERV'])) == strlen($this->paData['MOBSERV'])){
         $this->pcError = "ESCRÍBA ALGUNA OBSERVACIÓN";
         return false;
      }
      return true;
   }

   protected function mxObservSolicitudConsInter($p_oSql) {
      $lcFec = date("Y-m-d h:i:s");
      $lcObs = mb_strtoupper($this->paData['MOBSERV'], 'UTF-8');
      $lcSql = "UPDATE B04MTRE SET cEstado ='E',cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW(), mObserv = '* {$lcFec} .{$this->paData['CCODUSU']}. - {$lcObs}' WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Generacion de constancia de desfile (Obstetricia)
   // 2019-07-22 LVA - LVA Creacion
   // ------------------------------------------------------------------------------
   protected function mxPrintConstanciaDesfile() {
      $cont = 0;
      $fecha_actual = date ("Y-m-d");
      $loDate = new CDate();
      $lcCodTre = $this->laData['CCODTRE'];
      $lcNroDni = $this->laData['CNRODNI'];
   
      $lcPath = "./EXP/D$lcNroDni/P$lcCodTre.pdf";
      $lcPathImg = "./EXP/D$lcNroDni/P$lcCodTre.jpg";
      try {
         $fecha_actual = date ("Y-m-d");
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->SetFont('Courier','B',20);
         $pdf->Cell(186, 10, utf8_decode('UNIVERSIDAD CATÓLICA DE SANTA MARÍA'), 0, 0, 'C');
         $pdf->Ln(15);
         $pdf->SetFont('Courier','B',12);
         $pdf->Cell(186, 10, utf8_decode('EXPEDIENTE: E-' . $lcCodTre), 0, 0, 'L');
         $pdf->Ln(15);
         $pdf->SetFont('Courier','BU',17);
         $pdf->Cell(186, 10, utf8_decode('CONSTANCIA Nº '. $lcCodTre.'-FOyP-2019'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Cell(186, 10, utf8_decode('LA QUE SUSCRIBE, DECANA DE LA FACULTAD DE OBSTETRICIA  Y PUERICULTURA DE'), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('LA  UNIVERSIDAD  CATÓLICA DE SANTA MARÍA DE  AREQUIPA QUE SUSCRIBE, HACE'), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('CONSTAR QUE EL SR(A):'), 0, 0, 'L');
         $pdf->Ln(15);
         $pdf->SetFont('Courier', 'B', 14);
         $pdf->SetX(23);
         $pdf->Multicell(160,5,utf8_decode(substr(str_replace('/', ' ',$this->laData['CNOMBRE']), 0, 300)."\n"."Código: ".$this->laData['CCODALU']),1,'C');
         $pdf->Ln(5);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Cell(186, 10, utf8_decode('EGRESADA  DE NUESTRA FACULTAD, QUIEN HA  PARTICIPADO EN  LAS ACTIVIDADES'), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('DE EXTENSION UNIVERSITARIA DE LA FACULTAD DE OBSTETRICIA Y  PUERICULTURA,'), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('EN EL DESFILE DEL DIA DE LA OBSTETRA.'), 0, 0, 'L');
         $pdf->Ln(10);
         $pdf->Cell(186, 10, utf8_decode('SE EXPIDE LA PRESENTE  CONSTANCIA A  SOLICITUD DE LA INTERESADA PARA LOS'), 0, 0, 'L');
         $pdf->Ln(5);
         $pdf->Cell(186, 10, utf8_decode('FINES QUE ESTIME CONVENIENTE.'), 0, 0, 'L');
         $pdf->Ln(35);
         $pdf->SetFont('Courier', '', 12);
         $pdf->Cell(186, 10, utf8_decode('Arequipa,'. $loDate->dateSimpleText($fecha_actual)), 0, 0, 'R');
         $pdf->Ln(15);
         $pdf->AddPage();
         $pdf->Cell(10, 40, $pdf->Image($lcPathImg, $pdf->GetX(), $pdf->GetY(), 190.00), 0, 0, 'L', false );
         $llOk = $pdf->Output($lcPath, "F");
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      rename($lcPathImg,"./EXP/D$lcNroDni/P$lcCodTre"."F.jpg");
      //unlink($lcPathImg);
      return true;
   }

   // --------------------------------------------------------------------------------
   //  Agregar Registro TOELF ITL
   //  2019-07-31 LVA Creacion
   // --------------------------------------------------------------------------------
   public function omAgregarNuevoRegTOELF(){
      $llOk = $this->valParamAgregarNuevoRegTOELF();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if(!$llOk){
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAgregarNuevoRegTOELF($loSql);
      if (!$llOk) {
         $loSql->rollback();
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function valParamAgregarNuevoRegTOELF() {
      if (empty($this->paData['CCODALU']) || (strlen($this->paData['CCODALU']))!=10) {
         $this->pcError = "CODIGO ERRONEO";
         return false;
      }
      return true;
   }

   protected function mxAgregarNuevoRegTOELF($p_oSql) {
      $lcPeriod = substr(str_replace('-','',$this->paData['CPERIOD']),0,-2);
      $lcSql = "INSERT INTO A05DTFL (cCodAlu, cPeriod, cOrigen, nNota, cAproba, cNivel,cUsuCod,tModifi)
               VALUES('{$this->paData['CCODALU']}', '$lcPeriod' , '{$this->paData['CORIGEN']}',
                     {$this->paData['CNOTA']},'{$this->paData['CAPROBA']}','{$this->paData['CNIVEL']}','{$this->paData['CCODUSU']}', NOW())";
      $llOk = $p_oSql->omExec($lcSql);
		if (!$llOk) {
         $this->pcError = "ERROR AL REGISTRAR DATOS";
			return false;
      }
      return true;
   }
   // ------------------------------------------------------------------------------
   // Agregar Registro de Notas TOELF
   // 2019-07-26 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAgregarRegistroNotasTOELF() {
      $llOk = $this->mxValParamAgregarRegistroTOELF();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAgregarRegistroNotasTOELF($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxValParamAgregarRegistroTOELF() {
      if (empty($this->paData['CCODUSU'])|| (strlen($this->paData['CCODUSU']))>4) {
         $this->pcError = "USUARIO NO IDENTIFICADO";
         return false;
      }
      return true;
   }
   protected function mxAgregarRegistroNotasTOELF(){
   $lcSql = "INSERT INTO B05PREV (cColUac, cCodDoc, cUsuCod, tModifi)
                     VALUES('$lcColUac', '{$this->paData['CCODDOC']}', '{$this->paData['CCODUSU']}', NOW())";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL REGISTRAR REVISOR';
         return false;
      } 
   }
   
   // ------------------------------------------------------------------------------
   // Busqueda de Alumno para Notas TOELF
   // 2019-07-26 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAlumnoITLBusqueda() {
      $llOk = $this->mxValParamBusquedaTOELF();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxAlumnoITLBusqueda($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxValParamBusquedaTOELF() {
      if (empty($this->paData['CCODUSU'])|| (strlen($this->paData['CCODUSU']))>4) {
         $this->pcError = "USUARIO NO IDENTIFICADO";
         return false;
      }
      if(strlen($this->paData['CNRODNI'])>8){
         $this->pcError = "DNI INCORRECTO";
         return false;
      }
      return true;
   }
   protected function mxAlumnoITLBusqueda($p_oSql){
      if(isset($this->paData['CNOMUNI'])){
         $lcSql = "SELECT cNombre, cCodAlu, cNomUni FROM V_A01MALU WHERE cNroDni  = '{$this->paData['CNRODNI']}' AND cNivel = '01'";
      }
      else {
         $lcSql = "SELECT cNombre, cCodAlu, cNomUni FROM V_A01MALU WHERE cNroDni  = '{$this->paData['CNRODNI']}'";
      }
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'DNI NO ENCONTRADO';
         return false;
      }
      while ($laFila = $p_oSql->fetch($llOk)){
         if (!$laFila) {         
            $this->pcError = "ERROR AL BUSCAR ALUMNO";
            return false;
         }
         $resp[] = ["Nombre"=>$laFila[0],"Codigo"=>$laFila[1],"CNomUni"=>$laFila[2],'OK'];
      }
      echo json_encode($resp);
      return true;
   }
   
   // ------------------------------------------------------------------------------
   // Init Registro de Notas Alumno TOELF ITL
   // 2019-08-02 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitLeerArchivoTOELF() {
      $llOk = $this->mxValParamLeerArchivoTOELF();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitLeerArchivoTOELF($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamLeerArchivoTOELF() {
      if (!isset($this->paData['CCODUSU']) || strlen($this->paData['CCODUSU'])!= 4) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      }
      return true;
   }

   protected function mxInitLeerArchivoTOELF($p_oSql) {
      $loArchiv = fopen("toelf.csv", "r");
      $i=0;
      if(empty($loArchiv)){
         $this->pcError="ERROR AL ABRIR ARCHIVO";
         return false;
      }
      $lcSql = 'INSERT INTO A05DTFL VALUES(';
      while (($laTmp = fgetcsv($loArchiv, ',')) == true) {
         if(!empty($laTmp[2])){
            if($i>0){
               $lcSql = $lcSql."'{$laTmp[2]}','{$laTmp[5]}','','{$laTmp[6]}','{$laTmp[7]},'{$laTmp[8]}','{$this->paData['CCODUSU']}',NOW())";   
               $lcSql = $lcSql. ",(";
               $this->paDatos[] = ['CCODALU' => $laTmp[2],'CNOMBRE' => $laTmp[4],
                                   'CPERIOD' => $laTmp[5],'CNOTA' => $laTmp[6],
                                   'NAPROBA' => $laTmp[7],'CNIVEL' => $laTmp[8]];
            }
            $i++;
         }
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "ERROR AL CARGAR DATA";
         return false;
      }
      $lcSql = substr($lcSql,0,-2); 
      fclose($loArchiv);
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init bandeja de solicitud certificado de turniting para titulacion online
   // 2019-10-10 LVA - LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaBibliotecaTOturniting() {
      $llOk = $this->mxValInitBandejaBibliotecaTOturniting();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaBibliotecaTOturniting($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInitBandejaBibliotecaTOturniting() {
      return true;
   }

   protected function mxInitBandejaBibliotecaTOturniting($p_oSql) {
      $lcSql = "SELECT t_cCodTre, t_tFecha, t_cNroDni, t_cCodAlu, t_cNombre, t_cNomUni, t_cIdDeud, t_cCodtra FROM F_B04MTRE_7('0672')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR DE BUSQUEDA';
         return false;
      }
      $this->paDatos = [];
      while($laFila = $p_oSql->fetch($llOk)) {
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'TFECHA' => $laFila[1],
                             'CNRODNI' => $laFila[2], 'CCODALU' => $laFila[3],
                             'CNOMBRE' => $laFila[4], 'CNOMUNI' => $laFila[5], 
                             'CIDDEUD' => $laFila[6], 'CCODTRA' => $laFila[7]];
      }
      if(count($this->paDatos) == 0){
         $this->pcError = 'SIN TRAMITES PENDIENTES';
         return true;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init aprobar las constancias turniting para titulacion online
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAprobarConformidadBibliotecaTurniting() {
      $llOk = $this->mxValAprobarConformidadBibliotecaTurniting();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError; 
         return false;
      }
      $llOk = $this->mxAprobarConformidadBibliotecaTurniting($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValAprobarConformidadBibliotecaTurniting() {
      if (empty($this->paData['CCODTRE']) || (strlen($this->paData['CCODTRE'])) > 6) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CIDREVI'])) {
         $this->pcError = "CODIGO DE REVISION NO INGRESADO";
         return false;
      }
      return true;
   }

   protected function mxAprobarConformidadBibliotecaTurniting($p_oSql) {
      $lcFec = date("Y-m-d h:i:s");
      $lcCodUsu = $this->paData['CCODUSU'];
      $lcIdRevi = $this->paData['CIDANOR'].'-'.$this->paData['CIDREVI'];
      $lcUrlPdf = "http://cib.ucsm.edu.pe/api-rest-biblio/verconstancia-turnitin?idrevision=".$lcIdRevi."&format=pdf";
      $lcDetall = ['CIDREVI' => $lcIdRevi, 'CCODUSU' => $lcCodUsu, 
                   'TFECEXP' => $lcFec, 'CURLPDF' => $lcUrlPdf];
      $lcDetall = json_encode($lcDetall,JSON_UNESCAPED_SLASHES);     
      $lcSql = "UPDATE B04MTRE SET cEstado = 'B', mDetall = '$lcDetall', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL APROBAR TRAMITE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init aprobar los fedateos para titulacion online
   // 2019-11-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omObservarConformidadBibliotecaTurniting() {
      $llOk = $this->mxValObservarConformidadBibliotecaTurniting();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError; 
         return false;
      }
      $llOk = $this->mxObservarConformidadBibliotecaTurniting($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValObservarConformidadBibliotecaTurniting() {
      if (empty($this->paData['CCODTRE']) || (strlen($this->paData['CCODTRE'])) > 6) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (preg_match_all("/\s/",$this->paData['MOBSERV'])) {
         $this->pcError = "INGRESE ALGUNA OBSERVACION";
         return false;
      }
      return true;
   }

   protected function mxObservarConformidadBibliotecaTurniting($p_oSql) {
      $lcFec = date("Y-m-d h:i:s");
      $lcObs = mb_strtoupper($this->paData['MOBSERV'], 'UTF-8');
      $lcSql = "UPDATE B04MTRE SET cEstado ='E', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW(), mObserv = '* {$lcFec} .{$this->paData['CCODUSU']}. - {$lcObs}' WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL APROBAR TRAMITE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init bandeja de solicitudes observadas constancia de turniting para titulacion online
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaLevObservadosTurniting() {
      $llOk = $this->mxValInitBandejaObservadosTurniting();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaObservadosTurniting($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInitBandejaObservadosTurniting() {
      if (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaObservadosTurniting($p_oSql) {
      $lcSql = "SELECT A.cCodTre, TO_CHAR(A.tFecha, 'YYYY-MM-DD HH24:MI'), C.cNroDni, B.cCodAlu, D.cNombre, A.mObserv FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                INNER JOIN S01MPER D ON D.cNroDni = C.cNroDni
                WHERE A.cIdCate  = 'PQ0034' AND C.cEstado = 'C' AND A.cEstado = 'E'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR DE BUSQUEDA';
         return false;
      }
      while($laFila = $p_oSql->fetch($llOk)) {
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'TFECHA' => $laFila[1],
                             'CNRODNI' => $laFila[2], 'CCODALU' => $laFila[3],
                             'CNOMBRE' => $laFila[4], 'MOBSERV' => $laFila[5]];
      }
      if(count($this->paDatos) == 0){
         $this->pcError = 'SIN TRAMITES PENDIENTES';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Levantamiento de Observaciones Constancia Turniting
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omLevObservadosTurniting() {
      $llOk = $this->mxValLevObservadosTurniting();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxLevObservadosTurniting($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValLevObservadosTurniting() {
      if (empty($this->paData['CCODTRE']) || (strlen($this->paData['CCODTRE'])) > 6) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      } 
      return true;
   }

   protected function mxLevObservadosTurniting($p_oSql) {
      $lcSql = "UPDATE B04MTRE SET cEstado = 'F', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE CCODTRE = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL LEVANTAR OBSERVACIÓN';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Pantalla de subida de los documentos autenticados para titulacion
   // 2019-10-23 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitSubidaDeAutenticacion() {
      $llOk = $this->mxValSubidaDeAutenticacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitSubidaDeAutenticacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValSubidaDeAutenticacion() {
      if (empty($this->paData['CCODALU']) || (strlen($this->paData['CCODALU'])) != 10) {
         $this->pcError = "CODIGO DE ALUMNO NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxInitSubidaDeAutenticacion($p_oSql) {
      $lcSql = "SELECT A.cNroDni FROM B03MDEU A
                  INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                  INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog AND C.cIdCate = 'CCESTU'
                  WHERE A.cNroDni = '{$this->paData['CNRODNI']}' AND B.cCodAlu = '{$this->paData['CCODALU']}' AND A.cEstado = 'C' AND A.cPaquet = 'B' AND C.cEtapa IN ('A','B','C','D')";                
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (isset($laFila[0])) {
         $this->pcError = 'USTED REALIZO BACHILLER EN LINEA, PROXIMAMENTE USTED PODRA REALIZAR ESTE TRAMITE';
         return false;
      }
      $lcSql = "SELECT cTipo, cEstado FROM B04DFED WHERE CCODALU = '{$this->paData['CCODALU']}' AND (cEstado = 'A' OR cEstado = 'B')";
      $llOk = $p_oSql->omExec($lcSql);
      while($laFila = $p_oSql->fetch($llOk)){
         $this->paDatos[]= ['CESTADO' => $laFila[1], 'CTIPO' => $laFila[0]];
      }
      if(count($this->paDatos) < 2){
         if(count($this->paDatos) == 1){
            if ($this->paDatos[0]['CTIPO'] == 'C') { 
               $this->paDatos[] = ['CESTADO' => 'X', 'CTIPO' => 'D'];      
            }
            else{
               $this->paDatos[] = ['CESTADO' => 'X', 'CTIPO' => 'C'];      
            }
         }
         else {
            $this->paDatos[] = ['CESTADO' => 'X', 'CTIPO' => 'C'];
            $this->paDatos[] = ['CESTADO' => 'X', 'CTIPO' => 'D'];
         }
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Registro de los documentos autenticados en el B04DFED
   // 2019-10-23 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitRegistroDeAutenticacion() {
      $llOk = $this->mxValRegistroDeAutenticacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitRegistroDeAutenticacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValRegistroDeAutenticacion() {
      if (empty($this->paData['CCODALU']) || (strlen($this->paData['CCODALU'])) != 10) {
         $this->pcError = "CODIGO DE ALUMNO NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxInitRegistroDeAutenticacion($p_oSql) {
      $lcSql = "SELECT nSerial, cTipo, cEstado FROM B04DFED WHERE CCODALU = '{$this->paData['CCODALU']}' AND CTIPO = '{$this->paData['CTIPDOC']}'";
      $llOk = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($llOk);
      if($laFila[2] == 'E'){
         $lcSql = "UPDATE B04DFED SET cEstado = 'A' WHERE nSerial = '{$laFila[0]}'";
         $llOk = $p_oSql->omExec($lcSql);
      } else {
         $lcSql = "INSERT INTO B04DFED VALUES (DEFAULT, '{$this->paData['CCODALU']}', 'A', '{$this->paData['CTIPDOC']}', 'U666', DEFAULT)";
      }
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $loSql->pcError = 'NO SE PUDO REGISTRAR LOS DOCUMENTOS';
         return false;
      }
      return true;
   }


   public function omMover(){
      $directorio = opendir("./EXP/Bachiller");
      while ( $archivo = readdir($directorio)) {
         if (!is_dir($archivo)) {
            
            $nuevoNombre = str_replace('D015_', 'D', $archivo);
            $nuevoNombre = str_replace('_B','',$nuevoNombre);
            print_r($nuevoNombre);
            $rutaArchivo1 = "./EXP/Bachiller/".$archivo;
            $rutaArchivo2 = "./EXP/Bachiller/".$nuevoNombre;
            //$rutaArchivo2 = "./EXP/$nuevoNombre/".'P000001.pdf';
            if (rename ($rutaArchivo1, $rutaArchivo2)) {
               echo ("El archivo ".$rutaArchivo1." se ha renombrado a ".$rutaArchivo2);

            } else {
               echo ("El archivo ".$rutaArchivo1." no se ha renombrado correctamente");
            }
         }
      }
      die;
   }

   // ------------------------------------------------------------------------------
   // Init bandeja de solicitud certificado de turniting para titulacion online
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaEmpastadoCd() {
      $llOk = $this->mxValInitBandejaEmpastadoCd();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaEmpastadoCd($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInitBandejaEmpastadoCd() {
      if (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxInitBandejaEmpastadoCd($p_oSql) {
      $lcSql = "SELECT A.cCodTre, TO_CHAR(A.tFecha, 'YYYY-MM-DD HH24:MI'), C.cNroDni, B.cCodAlu, D.cNombre, F.cNomUni, A.cEstado 
                FROM B04MTRE A 
                INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog 
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud 
                INNER JOIN S01MPER D ON D.cNroDni = C.cNroDni 
                INNER JOIN A01MALU E ON E.cCodAlu = B.cCodAlu 
                INNER JOIN S01TUAC F ON F.cUniAca = E.cUniAca 
                WHERE A.cIdCate = 'PQ0033' AND C.cEstado = 'C' AND (A.cEstado = 'F' OR A.cEstado = 'R')";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR DE BUSQUEDA';
         return false;
      }
      while($laFila = $p_oSql->fetch($llOk)) {
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'TFECHA' => $laFila[1],
                             'CNRODNI' => $laFila[2], 'CCODALU' => $laFila[3],
                             'CNOMBRE' => $laFila[4], 'CNOMUNI' => $laFila[5],
                             'CESTADO' => $laFila[6]];
      }
      if(count($this->paDatos) == 0){
         $this->pcError = 'SIN TRAMITES PENDIENTES';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init aprobar los fedateos para titulacion online
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omAprobarEmpastadoCd() {
      $llOk = $this->mxValAprobarEmpastadoCd();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError; 
         return false;
      }
      $llOk = $this->mxAprobarEmpastadoCd($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValAprobarEmpastadoCd() {
      if (empty($this->paData['CCODTRA']) || (strlen($this->paData['CCODTRA'])) > 6) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      } 
      return true;
   }

   protected function mxAprobarEmpastadoCd($p_oSql) {
      if($this->paData['CESTADO'] == 'F'){
         $lcSql = "UPDATE B04MTRE SET cEstado = 'A', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRA']}'";
      } else {
         $lcSql = "UPDATE B04MTRE SET cEstado = 'B', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE cCodTre = '{$this->paData['CCODTRA']}'";
      }
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL APROBAR TRAMITE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init aprobar los fedateos para titulacion online
   // 2019-11-05 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omObservarEmpastadoCd() {
      $llOk = $this->mxValObservarEmpastadoCd();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError; 
         return false;
      }
      $llOk = $this->mxObservarEmpastadoCd($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValObservarEmpastadoCd() {
      if (empty($this->paData['CCODTRE']) || (strlen($this->paData['CCODTRE'])) > 6) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (preg_match_all("/\s/",$this->paData['MOBSERV'])) {
         $this->pcError = "INGRESE ALGUNA OBSERVACION";
         return false;
      }
      return true;
   }

   protected function mxObservarEmpastadoCd($p_oSql) {
      $lcFec = date("Y-m-d h:i:s");
      $lcObs = mb_strtoupper($this->paData['MOBSERV'], 'UTF-8');
      $lcSql = "UPDATE B04MTRE SET cEstado ='E', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW(), mObserv = '* {$lcFec} .{$this->paData['CCODUSU']}. - {$lcObs}' WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = "ERROR AL APROBAR TRAMITE";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init bandeja de solicitudes observadas constancia de turniting para titulacion online
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaLevObservadosEmpastadoCd() {
      $llOk = $this->mxValInitBandejaObservadosEmpastadoCd();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaObservadosEmpastadoCd($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInitBandejaObservadosEmpastadoCd() {
      if (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      } 
      return true;
   }

   protected function mxInitBandejaObservadosEmpastadoCd($p_oSql) {
      $lcSql = "SELECT A.cCodTre, TO_CHAR(A.tFecha, 'YYYY-MM-DD HH24:MI'), C.cNroDni, B.cCodAlu, D.cNombre, A.mObserv, F.cNomUni  FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                INNER JOIN S01MPER D ON D.cNroDni = C.cNroDni
                INNER JOIN A01MALU E ON E.cCodAlu = B.cCodAlu 
                INNER JOIN S01TUAC F ON F.cUniAca = E.cUniAca 
                WHERE A.cIdCate  = 'PQ0033' AND C.cEstado = 'C' AND A.cEstado = 'E'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR DE BUSQUEDA';
         return false;
      }
      while($laFila = $p_oSql->fetch($llOk)) {
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'TFECHA' => $laFila[1],
                             'CNRODNI' => $laFila[2], 'CCODALU' => $laFila[3],
                             'CNOMBRE' => $laFila[4], 'MOBSERV' => $laFila[5]];
      }
      if(count($this->paDatos) == 0){
         $this->pcError = 'SIN TRAMITES PENDIENTES';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Levantamiento de Observaciones Constancia Turniting
   // 2019-10-10 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omLevObservadosEmpastadoCd() {
      $llOk = $this->mxValLevObservadosEmpastadoCd();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxLevObservadosEmpastadoCd($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValLevObservadosEmpastadoCd() {
      if (empty($this->paData['CCODTRE']) || (strlen($this->paData['CCODTRE'])) > 6) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      } 
      return true;
   }

   protected function mxLevObservadosEmpastadoCd($p_oSql) {
      $lcSql = "UPDATE B04MTRE SET cEstado = 'F', cCodUsu = '{$this->paData['CCODUSU']}', tModifi = NOW() WHERE CCODTRE = '{$this->paData['CCODTRE']}'";
      $llOk = $p_oSql->omExec($lcSql);
      if (!$llOk) {
         $this->pcError = 'ERROR AL LEVANTAR OBSERVACIÓN';
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Revision Documentos (Por escuela profesional)
   // 2019-11-25 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaRevPreviaSustentacionTitulacion() {
      $llOk = $this->mxValParamInitBandejaRevPreviaSustentacionTitulacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejaRevPreviaSustentacionTitulacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaRevPreviaSustentacionTitulacion() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CCODIGO']) or (strlen($this->paData['CCODIGO']) != 1)) {
         $this->pcError = 'PARAMETRO CODIGO DE TRAMITE NO ESPECIFICADO';
         return false;
      }
      return true;
   }

   protected function mxInitBandejaRevPreviaSustentacionTitulacion($p_oSql) {
      $lcSql = "SELECT DISTINCT C.cUniAca, C.cNomUni FROM B03PUSU A
      INNER JOIN S01TCCO B ON B.cCenCos = A.cCenCos 
      INNER JOIN S01TUAC C ON C.cUniAca = B.cUniAca
      WHERE A.cCodUsu = '{$this->paData['CCODUSU']}' AND A.cEstado = 'A' OR A.cCenCos = '031'";
      $R1 = $p_oSql->omExec($lcSql);
      $lcInSql = '';
      while ($laFila = $p_oSql->fetch($R1)) {
         $lcInSql = $lcInSql . "'$laFila[0]',";
      }
      $lcInSql = substr($lcInSql,0,-1);
      $lcSql = "SELECT A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni
            FROM B03MDEU A
            INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
            INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
            INNER JOIN B03TDOC D ON D.cIdCate = C.cIdCate
            INNER JOIN S01MPER E ON E.cNroDni = A.cNroDni
            INNER JOIN A01MALU F ON F.cCodAlu = B.cCodAlu
            INNER JOIN S01TUAC G ON G.cUniAca = F.cUniAca
            WHERE A.cEstado = 'C' AND D.cTipo IN ('PQ','CC') AND A.cPaquet = 'T' AND C.cEtapa <> 'C' AND G.cUniAca IN ($lcInSql) AND C.cIdCate NOT IN ('PQ0009','PQ0033','PQ0032')
            GROUP BY A.cIdDeud, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni
            HAVING COUNT(*) = COUNT(CASE WHEN C.cEstado IN ('B') THEN 1 END)";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
      $this->paDatos[] = ['CIDDEUD' => $laFila[0], 'CNRODNI' => $laFila[1],
                         'CNOMBRE' => $laFila[2], 'CCODALU' => $laFila[3],
                         'CNOMUNI' => $laFila[4]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN PAQUETES PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Bandeja Revision Documentos (Por alumno)
   // 2019-11-26 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejaRevisionDocumentosExpSustentacion() {
      $llOk = $this->mxValParamInitBandejaRevisionDocumentosExpSustentacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->omxInitBandejaRevisionDocumentosExpSustentacion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejaRevisionDocumentosExpSustentacion() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } elseif (!isset($this->paData['CIDDEUD'])) {
         $this->pcError = "REGISTRO NO DEFINIDO";
         return false;
      } 
      return true;
   }

   protected function omxInitBandejaRevisionDocumentosExpSustentacion($p_oSql) {
      $lcSql = "SELECT  (TO_CHAR(C.dRecepc, 'YYYY-MM-DD HH24:MI')),
                                   D.cNombre, C.cIdDeud, E.cDescri, A.cCodTre, C.cNroDni,
                                   B.cCodAlu, A.cEstado, A.cIdCate, A.mDetall
                FROM B04MTRE A
                INNER JOIN B03DDEU B ON B.cIdLog  = A.cIdLog
                INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud
                INNER JOIN S01MPER D ON D.cNroDni = C.cNroDni
                INNER JOIN B03TDOC E ON E.cIdCate = A.cIdCate
                WHERE C.cIdDeud = '{$this->paData['CIDDEUD']}' AND C.cPaquet IN ('T') AND A.cEstado = 'B' AND E.cTipo IN ('PQ','CC')";              
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['DRECEPC' => $laFila[0], 'CNOMBRE' => $laFila[1],
                             'CIDDEUD' => $laFila[2], 'CDESCRI' => $laFila[3],
                             'CCODTRE' => $laFila[4], 'CNRODNI' => $laFila[5],
                             'CCODALU' => $laFila[6], 'CESTMTR' => $laFila[7],
                             'CIDCATE' => $laFila[8], 'MDETALL' => json_decode($laFila[9],true)];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN DOCUMENTOS PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Generar constancia de expediente completo - escuela
   // 2019-03-12 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omGenerarConstanciaTitulacionPrevia() {
      $llOk = $this->mxValGenerarConstanciaTitulacionPrevia();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $loSqlErp = new CSql();
      $llOk = $loSqlErp->omConnect(1);
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxGenerarConstanciaTitulacionPrevia($loSql, $loSqlErp);
      $loSql->omDisconnect();
      $loSqlErp->omDisconnect();
      return $llOk;
   }

   protected function mxValGenerarConstanciaTitulacionPrevia(){
      if (empty($this->paData['CCODUSU']) || (strlen($this->paData['CCODUSU'])) > 4) {
         $this->pcError = "CODIGO DE USUARIO NO DEFINIDO O INCORRECTO";
         return false;
      } elseif (empty($this->paData['CCODALU']) || (strlen($this->paData['CCODALU'])) != 10) {
         $this->pcError = "CODIGO DE ALUMNO NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }
   
   protected function mxGenerarConstanciaTitulacionPrevia($p_oSql, $p_oSqlErp){
      $lcCodAlu = $this->paData['CCODALU'];
      $lcSql = "SELECT A.cCodAlu, A.cNroDni, B.cNombre, A.cUniAca, C.cNomUni, TO_CHAR(NOW(), 'yyyy-mm-dd')
                  FROM A01MALU A
                  INNER JOIN S01MPER B ON B.cNroDni = A.cNroDni
                  INNER JOIN S01TUAC C ON C.cUniAca = A.cUniAca AND CNIVEL = '01'
                  WHERE A.cCodAlu = '$lcCodAlu'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
        $this->pcError = "NO SE OBTUVIERON DATOS DE ALUMNO";
        return false;
      }
      $this->paDatos = ['CCODALU' => $laFila[0],'CNRODNI' => $laFila[1], 'CNOMBRE' => str_replace('/', ' ', $laFila[2]), 'CUNIACA' => $laFila[3], 'CNOMUNI' => $laFila[4], 'DFECHA' => $laFila[5]];
      $lcNroDni = $laFila[1];
      $lcSql = "SELECT B.cCodtre FROM B03DDEU A
                INNER JOIN B04MTRE B ON B.cIdLog =  A.cIdLog
                WHERE A.cIdCate = 'PQ0004' AND A.cIdDeud = '{$this->paData['CIDDEUD']}'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "NO SE OBTUVIERON DATOS DEL TRAMITE";
         return false;
      }
      $this->paDatos['CCODTRE'] = $laFila[0];
      $lcSql = "SELECT B.cCargo, E.cNombre, C.cEstPro FROM T01DALU A
                INNER JOIN T01DDIC B ON B.cIdTesi = A.cIdTesi
                INNER JOIN T01MTES C ON C.cIdTesi = A.cIdTesi
                INNER JOIN S01TUSU D ON D.cCodUsu = B.cCodDoc
                INNER JOIN S01MPER E ON E.cNroDni = D.cNroDni
                WHERE A.CCODALU = '$lcCodAlu' AND cEstPro IN ('G','H')";
      $R1 = $p_oSqlErp->omExec($lcSql);
      $this->paDatos['CCONFOR'] = 'N';
      while ($laFila = $p_oSqlErp->fetch($R1)){
         $this->paDatos['CCONFOR'] = 'S';
         if($laFila[0] == 'P') {
            $this->paDatos['CPRESI'] = $laFila[1];
         } elseif($laFila[0] == 'V') {
            $this->paDatos['CVOCAL'] = $laFila[1];
         } else {
            $this->paDatos['CSECRE'] = $laFila[1];
         }
      }
      $lcPath= "./EXP/D$lcNroDni/P000005.pdf";
      try {
         $pdf = new PDF_Code128('P', 'mm');
         $pdf->AddPage();
         $pdf->Ln(30);
         $pdf->Cell(85);
         $pdf->SetFont('Courier', 'B', 15);
         $pdf-> Cell(20, 10, utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'), 0, 0, 'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf-> Cell(20,10,utf8_decode('ESCUELA PROFESIONAL DE '.$this->paDatos['CNOMUNI']),0,0,'C');
         $pdf->Ln(10);
         $pdf->Cell(85);
         $pdf->SetFont('Courier', 'BU', 15);
         $pdf->Cell(20, 10, utf8_decode('CONSTANCIA DE EXPEDIENTE COMPLETO PARA SUSTENTACIÓN'), 0, 0,'C');
         $pdf->Image("./Images/ucsm-02.png" , 80, 80, 50, 50);
         $pdf->Ln(80);
         $pdf->SetFont('Courier', '', 10);
         $pdf->MultiCell(186, 8, str_repeat(' ', 20).utf8_decode("Por la presente se hace constar que el alumno {$this->paDatos['CNOMBRE']} con código de alumno {$this->paDatos['CCODALU']} bachiller de la Escuela Profesional de {$this->paDatos['CNOMUNI']}, con número de expediente {$this->paDatos['CCODTRE']} presenta su expediente completo para su sustentación. "), 0);
         $pdf->Ln(5);
         if ($this->paDatos['CCONFOR'] == 'S') {
            $pdf-> Cell(20,10,utf8_decode('Siendo los Jurados:'));
            $pdf->Ln(10);
            $pdf->Cell(93, 5, utf8_decode('Presidente                                                            '), 0, 0, 'L');
            $pdf->Cell(93, 5, utf8_decode(':  '.substr(str_replace('/', ' ',$this->paDatos['CPRESI']), 0, 300)), 0, 0,'L');               
            $pdf->Ln(7);               
            $pdf->Cell(93, 5, utf8_decode('Vocal                       '), 0, 0, 'L');
            $pdf->Cell(93, 5, utf8_decode(':  '.substr(str_replace('/', ' ',$this->paDatos['CVOCAL']), 0, 300)), 0, 0,'L');               
            $pdf->Ln(7);               
            $pdf->Cell(93, 5, utf8_decode('Secretario                                 '), 0, 0, 'L');
            $pdf->Cell(93, 5, utf8_decode(':  '.substr(str_replace('/', ' ',$this->paDatos['CSECRE']), 0, 300)), 0, 0,'L');                           
            $pdf->Ln(20);
         }
         $pdf->Cell(68);
         $pdf->Cell(20, 5, utf8_decode('Se expide la presente a solicitud del interesado para'), 0, 0,'L');
         $pdf->Ln(8);
         $pdf->Cell(20, 5, str_repeat(' ', 53).utf8_decode('los fines que estime conveniente.'), 0, 0,'J');
         $pdf->Ln(20);
         $pdf->Cell(106);
         $pdf->SetFont('Courier', '', 10);
         $pdf->Cell(20,6,utf8_decode('UNIVERSIDAD CATOLICA DE SANTA MARIA'));
         $pdf->Ln(10);
         $pdf->Cell(144);
         $pdf->SetFont('Courier','',9);
         $pdf->Cell(30, 5, utf8_decode('AREQUIPA,'.$this->paDatos['DFECHA']), 0, 0,'R');
         $llOk = $pdf->Output($lcPath, "F");
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }
   
   // ------------------------------------------------------------------------------
   // Init Bandeja Seguimiento de Empastados para Biblioteca
   // 2019-12-18 LVA Creacion
   // ------------------------------------------------------------------------------
   public function omInitBandejSeguimientoEmpastadosBiblioteca() {
      $llOk = $this->mxValParamInitBandejSeguimientoEmpastadosBiblioteca();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitBandejSeguimientoEmpastadosBiblioteca($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamInitBandejSeguimientoEmpastadosBiblioteca() {
      if (!isset($this->paData['CCODUSU'])) {
         $this->pcError = "USUARIO NO DEFINIDO";
         return false;
      } 
      return true;
   }

   protected function mxInitBandejSeguimientoEmpastadosBiblioteca($p_oSql) {
      $lcSql = "SELECT C.cCodTre, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni, C.cEstado, TO_CHAR(C.tFecha, 'yyyy-mm-dd')
                FROM B03MDEU A
                INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                INNER JOIN B03TDOC D ON D.cIdCate = C.cIdCate
                INNER JOIN S01MPER E ON E.cNroDni = A.cNroDni
                INNER JOIN A01MALU F ON F.cCodAlu = B.cCodAlu
                INNER JOIN S01TUAC G ON G.cUniAca = F.cUniAca
                WHERE A.cEstado = 'C' AND D.cTipo = 'PQ' AND C.cEtapa <> 'C' AND A.cPaquet IN ('T') AND C.cIdCate IN ('PQ0033')
                GROUP BY C.cCodTre, A.cNroDni, E.cNombre, B.cCodAlu, G.cNomUni, C.cEstado";              
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CCODTRE' => $laFila[0], 'CNRODNI' => $laFila[1],
                             'CNOMBRE' => $laFila[2], 'CCODALU' => $laFila[3],
                             'CNOMUNI' => $laFila[4], 'CESTADO' => $laFila[5],
                             'TFECHA' => $laFila[6]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "SIN DOCUMENTOS PENDIENTES";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Cargar Constancias de Conducta pendientes por subir
   // 2020-01-22 APR Creacion
   // ------------------------------------------------------------------------------
   public function omRecuperarCertificadosConductaPorSubir() {
      $llOk = $this->mxValParamRecuperarCertificadosConductaPorSubir();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarCertificadosConductaPorSubir($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValParamRecuperarCertificadosConductaPorSubir() {
      if (!isset($this->paData['CIDCATE']) || strlen($this->paData['CIDCATE']) != 6) {
         $this->pcError = "CERTIFICADO NO DEFINIDO O NO VALIDO";
      }
      return true;
   }

   protected function mxRecuperarCertificadosConductaPorSubir($p_oSql) {

      $lcSql = "SELECT TO_CHAR(A.tFecha, 'YYYY-MM-DD HH24:MI'), A.cCodTre, E.cNombre, E.cNomUni, D.cDescri, A.cEstado, C.cNroDni
                  FROM B04MTRE A 
                  INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog 
                  INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud 
                  INNER JOIN B03TDOC D ON D.cIdCate = A.cIdCate 
                  LEFT OUTER JOIN V_A01MALU E ON E.cCodAlu = B.cCodAlu 
                  WHERE C.cEstado = 'C' AND D.cTipo = 'CT' AND A.cEstado = 'R' AND A.cIdCate = '000017' AND C.cPaquet = 'B' ORDER BY A.tFecha DESC;";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['TFECHA'  => $laTmp[0], 'CCODTRE' => $laTmp[1], 'CNOMBRE' => $laTmp[2], 'CNOMUNI' => $laTmp[3], 
                             'CDESCRI' => $laTmp[4], 'CESTADO' => $laTmp[5], 'CNRODNI' => $laTmp[6]];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO HAY CONSTANCIAS POR SUBIR";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Pantalla de subida de los documentos solicitudes mesa de partes
   // ------------------------------------------------------------------------------
   public function omActualizarTramiteMesaDePartes() {
      $llOk = $this->mxValActualizarTramiteMesaDePartes();
      if (!$llOk) {
         return false;
      }
       $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxActualizarTramiteMesaDePartes($loSql);
      if (!$llOk) {
         $loSql->rollback();
         $loSql->omDisconnect();
         return false;
      }
      if ($this->paFile['error'] != '4') {
        $llOk = $this->mxSubirArchivoMesaPartes($loSql);
        if (!$llOk) {
           $loSql->rollback();
        }
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValActualizarTramiteMesaDePartes() {
      if (empty($this->paData['CCODTRE']) || ((strlen($this->paData['CCODTRE'])) != 6 && $this->paData['CCODTRE'] != '*')) {
         $this->pcError = "CODIGO DE TRAMITE NO DEFINIDO O INCORRECTO";
         return false;
      } elseif ((preg_match_all("/\s/",$this->paData['CDESCRI'])) == strlen($this->paData['CDESCRI'])){
         $this->pcError = "ESCRÍBA ALGUNA DESCRIPCION BREVE";
         return false;
      } elseif ($this->paData['CASUNTO'] == '000     ') {
          if ((preg_match_all("/\s/",$this->paData['CDESCOR'])) == strlen($this->paData['CDESCOR'])){
            $this->pcError = "ESCRÍBA LA DESCRIPCION DEL TRAMITE";
            return false;
        }
      } elseif (!isset($this->paData['CNRODNI']) || !preg_match('(^[E0-9]{1}[0-9]{7}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO";
         return false;
      } elseif ($this->paFile['error'] != '4') {
          if ($this->paFile['error'] != '0' || $this->paFile['size'] > 5242880 || $this->paFile['type'] != 'application/pdf') {
            $this->pcError = "ARCHIVO NO VALIDO";
            return false;
          }
      } elseif (!isset($this->paData['CCODALU']) || strlen(trim($this->paData['CCODALU'])) != 10) {
         $this->pcError = "CODIGO DE ALUMNO INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxValActualizarTramiteMesaDePartesTerceros() {
      if ((preg_match_all("/\s/",$this->paData['CDESCRI'])) == strlen($this->paData['CDESCRI'])){
         $this->pcError = "ESCRÍBA ALGUNA DESCRIPCION BREVE";
         return false;
      } elseif ($this->paData['CASUNTO'] == ' ') {
         $this->pcError = "ESCRÍBA LA DESCRIPCION DEL TRAMITE";
         return false;
      } elseif (!isset($this->paData['CNRODNI']) || !preg_match('(^[E0-9]{1}[0-9]{7}$)', $this->paData['CNRODNI'])) {
         $this->pcError = "DNI NO DEFINIDO";
         return false;
      } elseif ($this->paFile['error'] != '4') {
         if ($this->paFile['error'] != '0' || $this->paFile['size'] > 5242880 || $this->paFile['type'] != 'application/pdf') {
            $this->pcError = "ARCHIVO NO VALIDO";
            return false;
         }
      }
      return true;
   }

   protected function mxActualizarTramiteMesaDePartes($p_oSql) {
      if ($this->paData['CCODTRE'] == '*') {
         $lcNroDni = $this->paData['CNRODNI'];
         $lcCodAlu = $this->paData['CCODALU'];
         $lnDuraci = 7;
         //valida tiempo entre solicitudes
         $lcSql = "SELECT A.cIdDeud FROM B03MDEU A INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   WHERE A.cNroDni = '$lcNroDni' AND A.cEstado = 'C' AND B.cIdCate = '000108' AND (A.dFecha + INTERVAL '5 MINUTE') > NOW()";
         $R1 = $p_oSql->omExec($lcSql);
         if ($R1 == false || $p_oSql->pnNumRow > 0) {
            $this->pcError = "NO PUEDE HACER MÁS DE UNA SOLICITUD EN MENOS DE 5 MINUTOS";
            return false;
         }
         //Id de deuda
         $lcSql = "SELECT MAX(cIdDeud) FROM B03MDEU";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         $lcIdDeud = (empty($laFila[0])) ? '000000' : $laFila[0];
         $i = (int)$lcIdDeud + 1;
         $lcIdDeud = sprintf('%06d', $i);
         $lcSql = "INSERT INTO B03MDEU (cIdDeud, cNroPag, cNroDni, cEstado, nMonto, cEnvio, cCodUsu) VALUES ('$lcIdDeud', '000000000', '$lcNroDni', 'C', 6.00, 'S', '9999')";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL GRABAR MAESTRO DE DEUDAS";
            return false;
         }
         //DATOS DEL TRAMITE
         $lcSql = "SELECT cIdCate, nMonto FROM B03TDOC WHERE cIdCate = '000108'";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         $lcIdCate = $laFila[0];
         $lnCosto = $laFila[1];
         $lcSql = "SELECT MAX(cIdLog) FROM B03DDEU";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         $lcIdLog = (empty($laTmp[0])) ? '000000' : $laTmp[0];
         $i = (int)$lcIdLog + 1;
         $lcIdLog = sprintf('%06d', $i);
         //DATOS DEL DETALLE
         $lcSql = "SELECT cPeriod, nCanPer FROM B03TDOC WHERE cIdCate = '$lcIdCate'";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         $lcPeriod = $laTmp[0];
         $lnCantid = $laTmp[1];
         $lcSql = "INSERT INTO B03DDEU (cIdLog, cRecibo, cNroExp, cIdDeud, cIdCate, nCosto, nCosFor, cPeriod, cCodAlu, cCodUsu, cCurCom) VALUES
                  ('$lcIdLog', '' , '' , '$lcIdDeud', '$lcIdCate','$lnCosto', 0.00, '$lcPeriod','$lcCodAlu' , '9999','N')";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL GRABAR DETALLE DE DEUDAS";
            return false;
         }
         //GENERA TRAMITE B04MTRE
         $lcSql = "SELECT MAX(cCodTre) FROM B04MTRE";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         $lcCodTre = (empty($laTmp[0])) ? '000000' : $laTmp[0];
         $i = (int)$lcCodTre + 1;
         $lcCodTre = sprintf('%06d', $i);
         $this->paData['CCODTRE'] = $lcCodTre;
         $lcSql = "INSERT INTO B04MTRE (cCodTre, cIdCate, cIdLog, tFecha, mDetall, cEstado, cCcoDes, cNroExp, mObserv, cEstPro, cDocDig, cClave, cEtapa, cCodUsu, tModifi) VALUES
                  ('$lcCodTre', '$lcIdCate' , '$lcIdLog' , NOW(), '','A','506','000000000', '', 'P', '', '', 'A', '9999', NOW())";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL GRABAR EN MAESTRO DE TRAMITES";
            return false;
         }
      }
      $this->paData['CDESCRI'] = str_replace("'", "''", $this->paData['CDESCRI']);
      $this->paData['CDESCOR'] = str_replace("'", "''", $this->paData['CDESCOR']);
      $lcDescri = mb_strtoupper($this->paData['CDESCRI'], 'UTF-8');
      if ($this->paData['CASUNTO'] == '000     '){
         $lcDesCor = mb_strtoupper($this->paData['CDESCOR'], 'UTF-8');
      } else {
         $lcDesCor = $this->paData['CASUNTO'];
      }
      $lcSql = "UPDATE T05DDOC SET cDesCor = '$lcDesCor',mDescri = '$lcDescri', cEstado = 'A' WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if (!$R1){
         $this->pcError = 'NO SE ACTUALIZO EL DETALLE DEL DOCUMENTO EN TRAMITE';
         return false;
      }
      return true;
   }

   protected function mxSubirArchivoMesaPartes($p_oSql) {
      $lcCodTre = $this->paData['CCODTRE'];
      $lcNroDni = $this->paData['CNRODNI'];
      $lcFolder = "EXP/D$lcNroDni/";
      if (!is_dir($lcFolder)) {
         $perm = "0777";             
         $modo = intval( $perm, 8 ); 
         mkdir( $lcFolder, $modo ); 
         chmod( $lcFolder, $modo);
      }
      $lcFilePath = $lcFolder."P$lcCodTre.".'pdf';
      if (!move_uploaded_file($this->paFile['tmp_name'], $lcFilePath)) {
         $this->pcError = "NO SE PUDO SUBIR EL ARCHIVO";
         return false;
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Init Pantalla de subida de los documentos solicitudes mesa de partes
   // ------------------------------------------------------------------------------
   public function omInitSubidaDeSolicitudMesaDePartes() {
      $llOk = $this->mxValSubidaDeSolicitudMesaDePartes();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxInitSubidaDeSolicitudMesaDePartes($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValSubidaDeSolicitudMesaDePartes() {
      if (empty($this->paData['CNRODNI']) || (strlen($this->paData['CNRODNI'])) != 10) {
         $this->pcError = "NUMERO DE DNI NO DEFINIDO O INCORRECTO";
         return false;
      }
      return true;
   }

   protected function mxInitSubidaDeSolicitudMesaDePartes($p_oSql) {
      $lcSql = "SELECT C.cCodTre, D.cDescri FROM B03MDEU A
               INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
               INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog 
               INNER JOIN B03TDOC D ON D.cIdCate = C.cIdCate 
               WHERE A.cNroDni = '{$this->paData['CNRODNI']}' 
               AND A.cEstado = 'C' AND C.cEstado IN ('F') AND C.cIdCate = '000108'";                
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $this->paDatos = ['CCODTRE' => $laFila[0], 'CDESCRI' => $laFila[1]];
      if (count($this->paDatos) == 0){
      $this->pcError = 'SIN SOLICITUDES PENDIENTES';
      return false; 
      }
      return true;
   }

   // ------------------------------------------------------------------------------
   // Tdo2770 Init Pantalla de subida de los documentos solicitudes mesa de partes para Terceros
   // 2020-04-30 FLC
   // ------------------------------------------------------------------------------
   public function omActualizarTramiteMesaDePartesTerceros() {
      $llOk = $this->mxValActualizarTramiteMesaDePartesTerceros();
      if (!$llOk) {
         return false;
      }
       $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxActualizarTramiteMesaDePartesTerceros($loSql);
      if (!$llOk) {
         $loSql->rollback();
         $loSql->omDisconnect();
         return false;
      }
      if ($this->paFile['error'] != '4') {
        $llOk = $this->mxSubirArchivoMesaPartes($loSql);
        if (!$llOk) {
           $loSql->rollback();
        }
      }
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxActualizarTramiteMesaDePartesTerceros($p_oSql) {
      if ($this->paData['CCODTRE'] == '*') {
         $lcNroDni = $this->paData['CNRODNI'];
         $lcCodAlu = $this->paData['CCODALU'];
         $lnDuraci = 7;
         //valida tiempo entre solicitudes
         $lcSql = "SELECT A.cIdDeud FROM B03MDEU A INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   WHERE A.cNroDni = '$lcNroDni' AND A.cEstado = 'C' AND B.cIdCate = '000108' AND (A.dFecha + INTERVAL '5 MINUTE') > NOW()";
         $R1 = $p_oSql->omExec($lcSql);
         if ($R1 == false || $p_oSql->pnNumRow > 0) {
            $this->pcError = "NO PUEDE HACER MÁS DE UNA SOLICITUD EN MENOS DE 5 MINUTOS";
            return false;
         }
         //Id de deuda
         $lcSql = "SELECT MAX(cIdDeud) FROM B03MDEU";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         $lcIdDeud = (empty($laFila[0])) ? '000000' : $laFila[0];
         $i = (int)$lcIdDeud + 1;
         $lcIdDeud = sprintf('%06d', $i);
         $lcSql = "INSERT INTO B03MDEU (cIdDeud, cNroPag, cNroDni, cEstado, nMonto, cEnvio, cCodUsu) VALUES ('$lcIdDeud', '000000000', '$lcNroDni', 'C', 6.00, 'S', '9999')";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL GRABAR MAESTRO DE DEUDAS";
            return false;
         }
         //DATOS DEL TRAMITE
         $lcSql = "SELECT cIdCate, nMonto FROM B03TDOC WHERE cIdCate = '000108'";
         $R1 = $p_oSql->omExec($lcSql);
         $laFila = $p_oSql->fetch($R1);
         $lcIdCate = $laFila[0];
         $lnCosto = $laFila[1];
         $lcSql = "SELECT MAX(cIdLog) FROM B03DDEU";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         $lcIdLog = (empty($laTmp[0])) ? '000000' : $laTmp[0];
         $i = (int)$lcIdLog + 1;
         $lcIdLog = sprintf('%06d', $i);
         //DATOS DEL DETALLE
         $lcSql = "SELECT cPeriod, nCanPer FROM B03TDOC WHERE cIdCate = '$lcIdCate'";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         $lcPeriod = $laTmp[0];
         $lnCantid = $laTmp[1];
         $lcSql = "INSERT INTO B03DDEU (cIdLog, cRecibo, cNroExp, cIdDeud, cIdCate, nCosto, nCosFor, cPeriod, cCodAlu, cCodUsu, cCurCom) VALUES
                  ('$lcIdLog', '' , '' , '$lcIdDeud', '$lcIdCate','$lnCosto', 0.00, '$lcPeriod','$lcCodAlu' , '9999','N')";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL GRABAR DETALLE DE DEUDAS";
            return false;
         }
         //GENERA TRAMITE B04MTRE
         $lcSql = "SELECT MAX(cCodTre) FROM B04MTRE";
         $R1 = $p_oSql->omExec($lcSql);
         $laTmp = $p_oSql->fetch($R1);
         $lcCodTre = (empty($laTmp[0])) ? '000000' : $laTmp[0];
         $i = (int)$lcCodTre + 1;
         $lcCodTre = sprintf('%06d', $i);
         $this->paData['CCODTRE'] = $lcCodTre;
         $lcSql = "INSERT INTO B04MTRE (cCodTre, cIdCate, cIdLog, tFecha, mDetall, cEstado, cCcoDes, cNroExp, mObserv, cEstPro, cDocDig, cClave, cEtapa, cCodUsu, tModifi) VALUES
                  ('$lcCodTre', '$lcIdCate' , '$lcIdLog' , NOW(), '','A','506','000000000', '', 'P', '', '', 'A', '9999', NOW())";
         $llOk = $p_oSql->omExec($lcSql);
         if (!$llOk) {
            $this->pcError = "ERROR AL GRABAR EN MAESTRO DE TRAMITES";
            return false;
         }
      }
      $this->paData['CDESCRI'] = str_replace("'", "''", $this->paData['CDESCRI']);
      $this->paData['CDESCOR'] = str_replace("'", "''", $this->paData['CDESCOR']);
      $lcDescri = mb_strtoupper($this->paData['CDESCRI'], 'UTF-8');
      $lcDesCor = mb_strtoupper($this->paData['CDESCOR'], 'UTF-8');
      $lcSql = "UPDATE T05DDOC SET cDesCor = '$lcDesCor',mDescri = '$lcDescri', cEstado = 'A' WHERE cCodTre = '{$this->paData['CCODTRE']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if (!$R1){
         $this->pcError = 'NO SE ACTUALIZO EL DETALLE DEL DOCUMENTO EN TRAMITE';
         return false;
      }
      return true;
   }

   //EXTRACCION DE UNIDAD ACADEMICA PARA CREACION DE ACTAS BACHILLER

   public function omRecuperarUnidadesAcademicasActas(){
      $llOk = $this->mxValRecuperarUnidadesAcademicasActas();
      if (!$llOk) {
         return false;
      }
       $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarUnidadesAcademicasActas($loSql);
      if (!$llOk) {
         return false;
      }
      return true;
   }

   protected function mxValRecuperarUnidadesAcademicasActas(){
      if (!isset($this->paData['CESTPRE']) || strlen($this->paData['CESTPRE']) != 2) {
         $this->pcError = "PARAMETRO DE BUSQUEDA DE UNIDAD INVALIDO";
         return false;
      }
      return true;
   }

   protected function mxRecuperarUnidadesAcademicasActas($p_oSql){
      $lcSql = "SELECT A.cNomUni, A.cUniAca FROM S01TUAC A 
                INNER JOIN S01TCCO B ON B.cUniAca = A.cUniAca AND A.cUniAca <> '00'
                WHERE B.cEstPre = '{$this->paData['CESTPRE']}' AND A.cNivel = '01'";
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $this->paDatos[] = ['CNOMUNI' => $laTmp[0], 'CUNIACA' => $laTmp[1]];
      }
      if ($this->paData['CESTPRE'] == '17'){
         $this->paDatos[] = ['CNOMUNI' => 'COMPLEMENTACION ACADEMICA EN EDUCACION MODALIDAD A DISTANCIA', 'CUNIACA' => '5C'];
      }
      if (count($this->paDatos) == 0) {
         $this->pcError = "NO SE ENCONTRARON UNIDADES ACADEMICAS RELACIONADAS";
         return false;
      }
      return true;
   }

   // ---------------------------------------------------------------------------
   // Reporte cargo colacion
   // 2019-06-03 LVA Creacion
   // ---------------------------------------------------------------------------
   public function omReporteActasColacion() {
      $llOk = $this->mxValParamReporteActasColacion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      //conecta al ERP
      $lcQErp = new CSql();
      $llOk = $lcQErp->omConnect(2);
      if (!$llOk) {
         $this->pcError = $lcQErp->pcError;
         return false;
      }
      $llOk = $this->mxReporteActasColacion($loSql);
      if ($llOk && $this->pcFlag == '1'){
        return true;
      }
      if (!$llOk) {
         return false;
      }
      $llOk = $this->mxPrintReporteActasColacion($loSql, $lcQErp);
      if (!$llOk){
        $loSql->rollback();
        $lcQErp->rollback();
        return false;
      }
      $llOk = $this->mxActualizarAlumnosExcluidosColacion($loSql);
      if (!$llOk){
        $loSql->rollback();
        return false;
      }
      $loSql->omDisconnect();
      $lcQErp->omDisconnect();
      return $llOk;
   }
  
   protected function mxValParamReporteActasColacion() {
      if (!isset($this->paData['CIDCOLA']) || strlen($this->paData['CIDCOLA']) != 5) {
         $this->pcError = "COLACION NO DEFINIDA O NO VALIDA";
         return false;
      } elseif (!isset($this->paData['CUNIACA']) || strlen($this->paData['CUNIACA']) != 2 || $this->paData['CUNIACA'] == '00') {
         $this->pcError = "UNIDAD ACADEMICA NO DEFINIDA O NO VALIDA";
         return false;  
      }
      return true;
   }
  
   protected function mxReporteActasColacion($p_oSql) {
      $lcSql = "SELECT cColUac FROM B05PUAC WHERE cUniAca = '{$this->paData['CUNIACA']}' AND cIdCola = '{$this->paData['CIDCOLA']}' AND cEstado = 'A'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $this->paDatos = [];
      $this->paDatos = ['CCOLUAC' => $laFila[0]];
      $llOk = $this->mxVerificarArchivoActa($this->paDatos['CCOLUAC']);
      if ($llOk){
        $this->pcFlag = '1';
        return true;
      }
      $this->pcFlag = '0';
      $lcSql = "SELECT cDescri FROM S01TCCO WHERE cEstPre = '{$this->paData['CESTPRE']}' AND cDescri LIKE '%FACULT%'";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      $lcFacult = $laFila[0];
      if ($this->paData['CUNIACA'] == '5C' || $this->paData['CUNIACA'] == '5M' || $this->paData['CUNIACA'] == '5D' || $this->paData['CUNIACA'] == '5B'){
        $lcNomUni = 'CIENCIAS DE LA EDUCACION';
      } else {
        $lcSql = "SELECT cNomUni FROM S01TUAC WHERE cUniAca = '{$this->paData['CUNIACA']}'";
        $R1 = $p_oSql->omExec($lcSql);
        $laFila = $p_oSql->fetch($R1);
        $lcNomUni = $laFila[0];
      }
      $this->paDatos += ['CFACULT' => $lcFacult, 'CNOMUNI' => $lcNomUni];
      $lcSql = "SELECT A.cCodDoc, D.cNombre, A.tModifi, B.cColUac FROM B05PREV A 
                INNER JOIN B05PUAC B ON B.cColUac = A.cColUac
                INNER JOIN A01MDOC C ON C.cCodDoc = A.cCodDoc
                INNER JOIN S01MPER D ON D.cNroDni = C.cNroDni 
                WHERE B.cUniAca = '{$this->paData['CUNIACA']}' AND B.cIdCola = '{$this->paData['CIDCOLA']}'
                ORDER BY A.nSerial ASC";
      $R1 = $p_oSql->omExec($lcSql);
      while ($laFila = $p_oSql->fetch($R1)) {
        $lcFile = 'FILES/R'.rand().'.png';
        $this->paDatos['pcJurado'][] = ['CCODDOC' => $laFila[0], 'CNOMBRE' => str_replace('/',' ',$laFila[1]), 'TFIRMA' => $laFila[2], 'CFILE' => $lcFile];
        $lcDataQr = utf8_decode($laFila[0].' - '.$laFila[1].'  '.$laFila[2]);
        QRcode::png(utf8_encode($lcDataQr), $lcFile, QR_ECLEVEL_L, 4, 0, false);
      }
      if($this->paData['CCODIGO'] == 'B') {
         $lcWhere = 'D';
      } elseif ($this->paData['CCODIGO'] == 'T'){
         $lcWhere = 'C';
      }
      if($this->paData['CCODIGO'] == 'T'){
         $lcSql = "SELECT B.cCodAlu, F.cNombre, E.cNomUni, TO_CHAR(A.dRecepc, 'YYYY-MM-DD HH24:MI') as dRecepc, A.cNroDni
                   FROM B03MDEU A
                   INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud
                   INNER JOIN B04MTRE C ON C.cIdLog  = B.cIdLog
                   INNER JOIN A01MALU D ON D.cCodAlu = B.cCodAlu
                   INNER JOIN S01TUAC E ON E.cUniAca = D.cUniAca
                   INNER JOIN S01MPER F ON F.cNroDni = A.cNroDni
                   INNER JOIN B05DUAC G ON G.cCodAlu = D.cCodAlu
                   INNER JOIN B05PUAC H ON H.cColUac = G.cColUac
                   WHERE A.cPaquet = '{$this->paData['CCODIGO']}' AND C.cEtapa = '$lcWhere' AND E.cUniAca NOT IN ('20', '21') AND cIdCola = '{$this->paData['CIDCOLA']}' AND C.cEstado IN ('B', 'M', 'S')
                   GROUP BY B.cCodAlu, F.cNombre, D.cUniAca, E.cNomUni, A.dRecepc, A.cNroDni, A.cIdDeud ORDER BY D.cUniAca, F.cNombre";
      } else {
         $lcSql = "SELECT D.cNombre,array_to_json(array[MDETALL::json->'CNRODIP'])->>0 as cNroDip, B.cCodtre, C.cCodAlu FROM B03DDEU A 
                   INNER JOIN B04MTRE B ON B.cIdLog = A.cIdLog AND B.cEstado = 'B'
                   INNER JOIN A01MALU C ON C.cCodAlu = A.cCodAlu
                   INNER JOIN S01MPER D ON C.cNroDni = D.cNroDni
                   INNER JOIN B05DUAC E ON E.cCodAlu = C.cCodAlu AND E.cEstado = 'I'
                   INNER JOIN B05PUAC F ON F.cColUac = E.cColUac 
                   INNER JOIN B03MDEU G ON G.cIdDeud = A.cIdDeud
                   WHERE F.cEstado = 'A' AND B.cEtapa = '$lcWhere' AND G.cPaquet = '{$this->paData['CCODIGO']}' AND F.cUniAca = '{$this->paData['CUNIACA']}' AND F.cIdCola = '{$this->paData['CIDCOLA']}' AND A.cIdCate = 'PDDEBA'
                   ORDER BY cNroDip ASC";
      }
      $R1 = $p_oSql->omExec($lcSql);
      $this->paDatos['pcAlumnos'] = [];
      while ($laFila = $p_oSql->fetch($R1)) {
         $this->paDatos['pcAlumnos'][] = ['CNOMBRE' => str_replace('/',' ',$laFila[0]), 'CNRODIP' => $laFila[1], 'CCODTRE' => $laFila[2], 'CCODALU' => $laFila[3]];
      }
      if (count($this->paDatos['pcAlumnos']) == 0) {
         $this->pcError = "SIN ALUMNOS EN LA COLACION SELECCIONADA";
         return false;
      }
      return true;
   }
  
   protected function mxPrintReporteActasColacion($p_oSql, $p_oSqlErp) {
      $lcCanExp = count($this->paDatos['pcAlumnos']);
      $fechaEntera = strtotime($this->paDatos['pcJurado'][0]['TFIRMA']);
      $hora = date("H:i", $fechaEntera);
      $horaIni = date("H:i", strtotime($hora)- (rand(15,24)*68));
      $dia = date("d",$fechaEntera);
      $MesAño = date("m:Y",$fechaEntera);
      $fecha = new CDate();
      $MesAño = $fecha->dateSimpleText($this->paDatos['pcJurado'][0]['TFIRMA']);
      $lcNroExp = 0;
      $lcColUac = $this->paDatos['CCOLUAC'];
      $lcPath= "./EXP/ACTAS/$lcColUac.pdf";
      $pdf = new PDF_Code128('P', 'mm');
      try {
         $pdf->AddPage('P','A4');
         $pdf->SetAutoPageBreak(true, 20);
         for ($j = 0; $j< ($lcCanExp /10); $j++) {
           //REGISTRO DEL LIBRO POR FOLIO - ERP
           $lcErp = "SELECT MAX(cIdLibr) FROM T02MLIB";
           $RS = $p_oSqlErp->omExec($lcErp);
           $laTmp = $p_oSqlErp->fetch($RS);
           $lcNroLib = (empty($laTmp[0])) ? '000000' : $laTmp[0];
           $lcNroLib = intval($laTmp[0]) + 1;
           $lcErp = "SELECT MAX(cFolio) FROM T02MLIB WHERE cUniAca = '{$this->paData['CUNIACA']}' AND cPrefij = 'B'";
           $RS = $p_oSqlErp->omExec($lcErp);
           $laTmp = $p_oSqlErp->fetch($RS);
           $lcNroFol = (empty($laTmp[0])) ? '000000' : $laTmp[0];
           $lcNroFol = intval($laTmp[0]) + 1;
           $lcNroFol = substr('000000', 0, 6 - strlen($lcNroFol)).$lcNroFol;  
           $lcNroLib = substr('000000', 0, 6 - strlen($lcNroLib)).$lcNroLib;
           $lcErp = "INSERT INTO T02MLIB VALUES ('$lcNroLib','$lcNroFol','000000', '{$this->paData['CUNIACA']}',null,null,'A','*','B','{$this->paData['CCODUSU']}',NOW(),'{$this->paDatos['CCOLUAC']}')";
           $RS = $p_oSqlErp->omExec($lcErp);
           if (!$RS){
             $this->pcError = 'ERROR AL INSERTAR EN EL MAESTRO DE LIBROS PARA BACHILLER';
             return false;
           }
           //----------------------------------
           if (($lcCanExp - $lcNroExp) > 10){
             $lcNroRev = 10;
           } else {
            $lcNroRev = ($lcCanExp - $lcNroExp);
           }
           $pdf->SetFont('Courier','B',15);
           $pdf->Cell(190, 5, intval($lcNroFol), 0, 0, 'R');
           $pdf->Ln(1);
           $pdf->Cell(186, 10, utf8_decode('ACTA DE GRADO DE BACHILLER'), 0, 0, 'C');
           $pdf->Ln(12);
           $pdf->SetFont('Courier','',11);
           $pdf->MultiCell(186, 5, utf8_decode("En la ciudad de Arequipa, en la Escuela Profesional de ".$this->paDatos['CNOMUNI']." de la ".
                                               $this->paDatos['CFACULT']." de la Universidad Católica de Santa Maria, siendo las ".$horaIni." horas del día ".$MesAño." se reunió la Comisión Evaluadora para el otorgamiento del GRADO ACADÉMICO DE BACHILLER, constituida por:"), 0);
           $pdf->Ln(4);
           $pdf->Cell(90, 5, utf8_decode("PRESIDENTE : ".$this->paDatos['pcJurado'][0]['CNOMBRE']), 0, 0);
           $pdf->Ln(5);
           $pdf->Cell(90, 5, utf8_decode("VOCAL      : ".$this->paDatos['pcJurado'][1]['CNOMBRE']), 0, 0);
           $pdf->Ln(5);
           $pdf->Cell(90, 5, utf8_decode("SECRETARIO : ".$this->paDatos['pcJurado'][2]['CNOMBRE']), 0, 0);
           $pdf->Ln(9);
           $pdf->SetFont('Courier','',11);
           $pdf->MultiCell(186, 5, utf8_decode("Habiéndose procedido a revisar ".$lcNroRev." expedientes de la Escuela Profesional de ".$this->paDatos['CNOMUNI']." de la ".
                                                $this->paDatos['CFACULT']."."."\n"."En cuanto que, reúnen los requisitos señalados en el Reglamento de Grados y Titulos de la Universidad, en concordancia con el Decreto Legislativo N°739, declarándose que los siguientes ex-alumnos están aptos".
                                                " para recepcionar el Diploma correspondiente al GRADO ACADÉMICO en ".$this->paDatos['CNOMUNI']."."), 0);
           $pdf->Ln(5);
           $pdf->SetFont('Courier', 'BU', 10);
           $pdf->SetX(19);
           $pdf->Cell(20, 8, utf8_decode("N° Ord."), 0, 0,'C');
           $pdf->Cell(100, 8, utf8_decode("Apellidos y Nombres"), 0, 0,'C');
           $pdf->Cell(20, 8, utf8_decode("Exp N°"), 0, 0,'C');
           $pdf->Cell(40, 8, utf8_decode("N° Diploma"), 0, 1,'C');
           $i = 1;
           for($k = $lcNroExp; $k < $lcCanExp ; $k++) {
              $lcSql = "UPDATE B05DUAC SET cIdLibr = '$lcNroLib', cEstado = 'C' WHERE cCodAlu = '{$this->paDatos['pcAlumnos'][$k]['CCODALU']}' AND cColUac = '{$this->paDatos['CCOLUAC']}'";
              $R1 = $p_oSql->omExec($lcSql);
              if (!$R1){
                $this->pcError = "ERROR AL ACTUALIZAR ESTADO DE COLACION, ALUMNO";
                return false;
              }
              $pdf->SetFont('Courier','', 10);
              $pdf->SetX(19);
              if ($i<10){
                $pdf->Cell(20, 8, utf8_decode('0'.$i.'.'), 0, 0, 'C');
              } else {
                $pdf->Cell(20, 8, utf8_decode($i.'.'), 0, 0, 'C');
              }
              $pdf->SetFont('Courier', 'U', 10);
              $pdf->Cell(100, 8, utf8_decode($this->paDatos['pcAlumnos'][$k]['CNOMBRE']), 0, 0);
              $pdf->Cell(20, 8, utf8_decode($this->paDatos['pcAlumnos'][$k]['CCODTRE']), 0, 0,'C');
              $pdf->Cell(40, 8, utf8_decode($this->paDatos['pcAlumnos'][$k]['CNRODIP']), 0, 1,'C');
              if($i == 10){
                break;
              }
              ++$i;
           }
           $lcNroExp = $k + 1;
           $pdf->SetFont('Courier','',11);
           $pdf->Ln(7);
           $pdf->MultiCell(186, 5, utf8_decode("Siendo las ".$hora." horas, se dio por concluido el acto, procediendo a la firma de la presente acta"), 0);
           $pdf->Ln(5);
           //FIRMAS QR
           $pdf->Cell(160, 5, utf8_decode("PRESIDENTE : ".$this->paDatos['pcJurado'][0]['CCODDOC']."-".$this->paDatos['pcJurado'][0]['CNOMBRE']), 0, 0);
           $pdf->Image($this->paDatos['pcJurado'][0]['CFILE'],null,null,12,12,'PNG');
           $pdf->Ln(5);
           $pdf->Cell(160, 5, utf8_decode("VOCAL      : ".$this->paDatos['pcJurado'][1]['CCODDOC']."-".$this->paDatos['pcJurado'][1]['CNOMBRE']), 0, 0);
           $pdf->Image($this->paDatos['pcJurado'][1]['CFILE'],null,null,12,12,'PNG');
           $pdf->Ln(5);
           $pdf->Cell(160, 5, utf8_decode("SECRETARIO : ".$this->paDatos['pcJurado'][2]['CCODDOC']."-".$this->paDatos['pcJurado'][2]['CNOMBRE']), 0, 0);
           $pdf->Image($this->paDatos['pcJurado'][2]['CFILE'],null,null,12,12,'PNG');
           $pdf->SetY(297);
         }
         $llOk = $pdf->Output($lcPath, "F");
      } catch (Exception $e) {
         $this->pcError = 'ERROR AL GENERAR PDF';
         return false;
      }
      return true;
   }

   protected function mxVerificarArchivoActa($p_cColUac) {
      if (file_exists("EXP/ACTAS/$p_cColUac.pdf")) {
         return true;
      }
      return false;
   }

   protected function mxActualizarAlumnosExcluidosColacion($p_oSql){
     $lcSql = "SELECT cNroDni FROM V_A01MALU A 
               INNER JOIN B05DUAC B ON B.cCodAlu = A.cCodAlu AND B.cEstado <> 'C'
               WHERE B.cColUac = '{$this->paDatos['CCOLUAC']}'";
     $R1 = $p_oSql->omExec($lcSql);
     if ($R1 == false) {
        $this->pcError = "ERROR AL BUSCAR ALUMNOS EXCLUIDOS";
        return false;
     } elseif ($p_oSql->pnNumRow == 0) {
        return true;
     }
     while ($laFila = $p_oSql->fetch($R1)) {
       $lcSql = "UPDATE B04MTRE SET cEtapa = 'B', tModifi = NOW() WHERE cEtapa != 'D' AND 
                    cIdLog IN (SELECT cIdLog FROM B03DDEU A INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud
                 WHERE B.cNroDni = '$laFila[0]' AND B.cPaquet = 'B' AND B.cEstado = 'C')";
       $RS = $p_oSql->omExec($lcSql);
       if ($R1 == false) {
          $this->pcError = "ERROR AL ACTUALIZAR PAQUETES DE ALUMNOS EXCLUIDOS";
          return false;
       }
     }
     $lcSql = "UPDATE B05DUAC SET cEstado = 'X' WHERE cColUac = '{$this->paDatos['CCOLUAC']}' AND cEstado <> 'C'";
     $R1 = $p_oSql->omExec($lcSql);
     if (!$R1){
       $this->pcError = "ERROR AL ACTUALIZAR ESTADO DE COLACION, ALUMNOS EXCLUIDOS";
       return false;
     }
     $lcSql = "UPDATE B05PUAC SET cEstado = 'I' WHERE cColUac = '{$this->paDatos['CCOLUAC']}'";
     $R1 = $p_oSql->omExec($lcSql);
     if (!$R1){
       $this->pcError = "ERROR AL INACTIVAR ESTADO DE COLACION";
       return false;
     }
     return true;
   }

   //RECUPERA COLACIONES PARA ESCUELAS
   public function omRecuperarGruposColacionXUni(){
      $llOk = $this->mxValRecuperarGruposColacionXUni();
      if (!$llOk) {
         return false;
      }
       $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRecuperarGruposColacionXUni($loSql);
      if (!$llOk) {
         return false;
      }
      return true;
   }
  
   protected function mxValRecuperarGruposColacionXUni(){
      if (!isset($this->paData['CIDCOLA']) || strlen($this->paData['CIDCOLA']) != 5) {
         $this->paDatos['ERROR'] = "PARAMETRO DE BUSQUEDA DE ID COLACION INVALIDO";
         return false;
      } elseif (!isset($this->paData['CUNIACA']) || strlen($this->paData['CUNIACA']) != 2 || $this->paData['CUNIACA'] == '00') {
         $this->paDatos['ERROR'] = "PARAMETRO DE BUSQUEDA DE UNIDAD ACADEMICA INVALIDO";
         return false;
      } 
      return true;
   }
  
   protected function mxRecuperarGruposColacionXUni($p_oSql){
   $lcSql = "SELECT cColUac, TO_CHAR(tModifi, 'yyyy-mm-dd hh24:mi') FROM B05PUAC WHERE cUniAca = '{$this->paData['CUNIACA']}' AND cIdCola = '{$this->paData['CIDCOLA']}'";
      $R1 = $p_oSql->omExec($lcSql);
      if ($R1 == false) {
         $this->paDatos['ERROR'] = "ERROR AL BUSCAR COLACIONES";
         return false;
      } elseif ($p_oSql->pnNumRow == 0) {
         $this->paDatos['ERROR'] = "SIN GRUPOS PARA ESTA COLACIÓN ENCONTRADAS";
         return false;
      }
      $this->paDatos = [];
      while ($laTmp = $p_oSql->fetch($R1)) {
         $llOk = $this->mxVerificarArchivoActa($laTmp[0]);
         if ($llOk) {
            $this->paDatos[] = ['CCOLUAC' => $laTmp[0], 'TMODIFI' => $laTmp[1]];
         }    
      }
      if (count($this->paDatos) == 0) {
         $this->paDatos['ERROR'] = "SIN GRUPOS PARA ESTA COLACIÓN ENCONTRADAS";
         return false;
      }
      return true;
   }

}
?>