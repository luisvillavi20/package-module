<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) {
      return;
   } elseif (@$_REQUEST['Id'] == 'cargarSolicitudes') {
      fxAxCargarSolicitudes();
   } elseif(@$_REQUEST['Boton'] == 'Activar') {
      fxActivar();
   } elseif(@$_REQUEST['Boton'] == 'AsignarJurado') {
      fxAsignarJurado();
   } elseif(@$_GET['Id'] == 'BuscarDocente') {
      fxAxBuscarDocente();
   } else {
      fxInit();
   }

   function fxInit() {
      $_SESSION['paDatos']  = null;
      $_SESSION['paData']   = null;
      $_SESSION['paSolJur'] = null;
      fxScreen(0);
   }

   function fxActivar() {
      $lcIdenti = $_REQUEST['paData']['CIDENTI'];
      foreach ($_SESSION['paSolJur'] as $laFila) {
         if ($laFila['CIDENTI'] == $lcIdenti) {
            $laData = $laFila;
            break;
         }
      }
      $lo = new CMatricula();
      $lo->paData = array_merge($laData, ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']]);
      $llOk = $lo->omCursosParaAsignarDocentes();
      if (!$llOk) {
         fxHeader('Tdo5020.php', $lo->pcError);
      }
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxAsignarJurado() {
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAsignarJurado();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(1);
         return;
      }
      $lo->paData = array_merge($_SESSION['paData'], ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']]);
      $llOk = $lo->omCursosParaAsignarDocentes();
      if (!$llOk) {
         fxHeader('Tdo5020.php', $lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxAxCargarSolicitudes() {
      $lo = new CMatricula();
      $lo->paData = array_merge($_REQUEST['paData'],['CUSUCOD' => $_SESSION['GADATA']['CCODUSU']]);
      $llOk = $lo->omAlumnosParaAsignarDocentes();
      if(!$llOk) {
         echo json_encode(['ERROR' => $lo->pcError]);
         return;
      }
      $_SESSION['paSolJur'] = $lo->paSolJur;
      AxPrintSolicitudes();
   }

   function fxAxBuscarDocente() {
      $lo = new CMatricula();
      $lo->paData = ['CBUSQUE' => $_REQUEST['paData']['CBUSQUE']];
      $llOk = $lo->omBuscarDocente();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      global $loSmarty;
      $loSmarty->assign('saDatos', $lo->paDatos);
      $loSmarty->display('Plantillas/Tdo5021.tpl');
   }  

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('scNombre', $_SESSION['GADATA']['CNOMBRE']);
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5020.tpl');
   }

   function AxPrintSolicitudes() {
      global $loSmarty;
      $loSmarty->assign('saSolJur', $_SESSION['paSolJur']);
      $loSmarty->display('Plantillas/Tdo5022.tpl');
   }
?>