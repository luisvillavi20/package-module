<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_GET['Id'] == 'CargarGrupos') {
      fxCargarGrupos();
   }
   /*elseif (@$_GET['Id'] == 'CargarColacion') {
      fxCargarColacion();
   }*/ elseif (@$_GET['Id'] == 'CargarAlumnos') {
      fxCargarAlumnos();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'];
      $llOk = $lo->omCargarArrayColaciones();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paArrays'] = $lo->paArrays;
      fxScreen(0);
   }

   function fxCargarGrupos() {
      $lo = new CPaquetes();      
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omCargarColacionesPorUsuario();
      $laData = $_SESSION['GADATA'] + $lo->paData;
      global $loSmarty;  
      $loSmarty->assign('saData', $laData);
      $loSmarty->assign('saDatos', $lo->paDatos);
      $loSmarty->display('Plantillas/Paq2071.tpl');
   }

   function fxCargarColacion() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRecuperarUltimaColacion();
      if (!$llOk) {
         echo '{"ERROR":"'.$lo->pcError.'"}';
      } else {
         echo json_encode($lo->paData);
      }
   }

   function fxCargarAlumnos() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omCargarAlumnosExpComp();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $laData = $_SESSION['GADATA'] + $lo->paData;
      
      global $loSmarty;  
      $loSmarty->assign('saData', $laData);
      $loSmarty->assign('saDatos', $lo->paDatos);
      $loSmarty->display('Plantillas/Paq2072.tpl');
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('saArrays',   $_SESSION['paArrays']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq2070.tpl');
   }
?>