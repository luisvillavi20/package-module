<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   require_once 'Clases/CDeudas.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Revisar') {
      fxRevisar();
   } elseif (@$_REQUEST['Boton'] == 'Aprobar') {
      fxGrabarAprobar();
   } elseif (@$_REQUEST['Boton'] == 'Denegar') {
      fxGrabarDenegar();
   } elseif (@$_REQUEST['Boton'] == 'Detener') {
      fxGrabarDetener();
   } elseif (@$_REQUEST['Id'] == 'Opcion') {     
      fxAxOpcion();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CMatricula();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omInitBandejaSolicitudesCursosJuradoAc(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxRevisar() {
      $lo = new CMatricula();
      $lo->paData = ['CIDENTI' => $_REQUEST['pcCidenti']];
      $llOk = $lo->omRevisarSolicitudAc();
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
         return;
      }
      $_SESSION['paData'] = $_SESSION['GADATA'] + $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos + $_SESSION['paDatos'];
      fxScreen(1);  
   }

   function fxGrabarAprobar() {
      $loD = new CDeudas(); 
      $loD->paData = $_REQUEST['paData'];
      $llOk = $loD->omComprobarDeudas();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxHeader('Tdo5050.php');
         return;
      }
      $lo = new CMatricula();
      $laData = $_REQUEST['paData'] + ['NSERIAL' => $_REQUEST['nSerial'], 'CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omAprobarSolicitudCursoJuradoAc(); 
      if (!$llOk) {
         fxHeader('Tdo5050.php', $lo->pcError);
      }
      $lo->paData = $_REQUEST['paData'] + $_SESSION['paData'];
      $llOk = $lo->omRevisarSolicitudAc();
      if (!$llOk) {        
         fxInit(0);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData; 
      fxScreen(1);
   }
   
   function fxGrabarDenegar() {
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['paData'];
      $llOk = $lo->omDenegarSolicitudCursoJuradoAc(); 
      if (!$llOk) {
         fxHeader('Tdo5050.php', $lo->pcError);
      }
      $llOk = $lo->omRevisarSolicitudAc();
      if (!$llOk) {        
         fxInit(0);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData; 
      fxScreen(1);
   }

   function fxGrabarDetener() {
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['paData'];
      $llOk = $lo->omDetenerSolicitudCursoJuradoAc(); 
      if (!$llOk) {
         fxHeader('Tdo5050.php', $lo->pcError);
      }
      $llOk = $lo->omInitBandejaSolicitudesCursosJuradoAc();
      if (!$llOk) {        
         fxHeader('Mnu1000.php', $lo->pcError);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData; 
      fxScreen(0);
   }

   function fxAxOpcion() { 
		$laData = $_REQUEST['paData'];
		$lo = new CMatricula();
		$lo->paData = $laData + $_SESSION['GADATA'];
      $llOk = $lo->omCargarOpcion();
      if (!$llOk) {
         echo json_encode(['ERROR' => $lo->pcError]);
         return;
		}
      $_SESSION['paDatos'] = $lo->paDatos;
      fxPrintOpcion(0);
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5050.tpl');
   }

   function fxPrintOpcion() {
      global $loSmarty;
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->display('Plantillas/Tdo5051.tpl');
   }
?>