<?php
   //BANDEJA TRAMITES PENDIENTES
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CWebService.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   }
   else if (@$_REQUEST['Boton'] == 'Firmar') {
      fxFirmar();
   }
   else {
      fxInit();
	}

	function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'];
      $llOk = $lo->omBandejaFirmasPendientes();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxFirmar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omFirmarCertificado();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      /*$llOk = $lo->omSoloDemo();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }*/
      fxInit();
   }

	function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1040.tpl');
   }
?>