<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CTramites.php';
   require_once 'Clases/CEmail.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Aprobar') {
      fxAprobarCertificado();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CTramites();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omInitBandejaSubirCertificadosDigitales(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxAprobarCertificado() {
      $lo = new CTramites();
      $laData = ['CCODTRE'=>$_REQUEST['pcCodTre']]+['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      print_r($laData);
      $lo->paData = $laData;
      $lo->paFile = $_FILES['fTraduccion'];
      $llOk = $lo->omSubirCertificadosDigitales(); 
      if (!$llOk) {
         fxHeader('Tdo5150.php', $lo->pcError);
      }
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5150.tpl');
   }
?>