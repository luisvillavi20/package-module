<?php
   // ----------------------------------------------------------------------------
   // WS confirmacion de firmas completas de certificados
   // 2019-02-05 LVA Creacion
   // ----------------------------------------------------------------------------
   session_start();
   require_once 'Clases/CPaquetes.php';
   try {
      $REQUEST = json_decode(file_get_contents("php://input"), true);
      if (is_null($REQUEST)) {
         echo '{"ERROR":"PARAMETROS INCORRECTOS"}';
         return;
      }
   } catch (Exception $e) {
      echo json_encode(["ERROR" => $e->getMessage()]);
   }
   fxVerificarFirmaDocumento();
   
   function fxVerificarFirmaDocumento() {
      global $REQUEST;
      $lo = new CPaquetes();
      $lo->paData = $REQUEST;
      $llOk = $lo->omFirmasAprobadas();
      if(!$llOk) {
         echo json_encode(['ERROR'=>$lo->pcError]);
      }
      else {
         echo json_encode($lo->paData);
      }
   }   
?>