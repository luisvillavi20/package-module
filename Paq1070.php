<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;   
   if (!fxSoloAlumnos()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'cerrarsesion') {
      fxCerrarSesion();
   } elseif (@$_REQUEST['Boton'] == 'Detalles') {
      fxDetalles();
   } elseif (@$_REQUEST['Boton'] == 'Subir') {
      fxSubirArchivo();
   } else {      
      fxInit();
   }    
   function fxCerrarSesion() {      
      session_destroy();
      fxheader("index.php");
   } 
   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ["CCODALU" => $_SESSION['GADATA']['CCODALU']] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omRecuperarDocumentosCarpeta();
      if (!$llOk) {
         fxHeader('Mnu2000.php', $lo->pcError);
         return;            
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'] + ['LLMODAL' => isset($_SESSION['llModal'])] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      fxScreen(0);
   }

   function fxSubirArchivo () {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $lo->paFile = $_FILES['pfDocBac'];
      $llOk = $lo->omSubirArchivo();
      if (!$llOk) {
         fxScreen(0);
         fxAlert($lo->pcError);
         return;
      }
      $_SESSION['llModal'] = true;
      fxInit();
   }

   function fxScreen($p_nBehavior) {
      global $loSmarty;      
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq1070.tpl');
   }
?>