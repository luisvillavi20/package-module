<?php
   error_reporting(E_ALL);
   ini_set('display_errors',1);
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;   
   if (!fxSoloAdministrativo()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif(@$_GET['Id'] == 'Verificar') {
      fxVerificar();
   } else {      
      fxInit();
   }    
   function fxInit() {
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $lo = new CPaquetes();
      $llOk = $lo->omCargarSolicitudesBachiller();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;            
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxGrabar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omGrabarSolicitudBachiller();
      if (!$llOk) {
         fxHeader('Paq1140.php', $lo->pcError);
         return;            
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxVerificar() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODALU' => $_REQUEST['CCODALU']] + $_SESSION['GADATA'];
      $llOk = $lo->omVerificarAlumnoSolicitud();
      echo json_encode([
         'success' => $llOk,
         'data' => $llOk ? $lo->paData: $lo->pcError
      ]);
   }

   function fxScreen($p_nBehavior) {
      global $loSmarty;      
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq1140.tpl');
   }
?>