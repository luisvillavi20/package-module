<?php
   // -------------------------------------------------------
   // Permite modificar el creditaje de matricula del alumno
   // 2019-02-14 LVA Creacion
   // -------------------------------------------------------
   //error_reporting(E_ALL);ini_set('display_errors', 1);
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   require_once 'Clases/CDeudas.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['ID'] == 'ConsultaUniAcaCosto') {
      fxConsultaUniAcaCosto();
   } elseif (@$_REQUEST['Boton'] == 'Anular') {
      fxAnular();
   } elseif (@$_REQUEST['Boton'] == 'Nuevo') {
      fxScreen(0);
   } else {
      fxInit();
   }
   function fxInit() {   
      $laData  = $_SESSION['GADATA'];
      $lo = new CMatricula();
      $lo->paData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $llOk = $lo->omInitMatriculaxCreditosConsulta();
      $_SESSION['paDatos'] = [];
      if (!$llOk) {
         fxHeader('Mnu0000.php', $lo->pcError);
      } //[P: Pendiente, U: Utilizado, X: Anulado]       
      $_SESSION['paData']  = $lo->paData + $_SESSION['GADATA']; //RECUPERA EL COSTO DEL CREDITO
      $_SESSION['paDatos'] = $lo->paDatos;
      if ($lo->paData['NSERIAL'] == -1) {
         fxScreen(0); 
         return;
      } elseif ($lo->paData['CESTADO'] == 'P') {
         fxHeader('Tdo5010.php', "");
         //fxScreen(1); 
      } elseif ($lo->paData['CESTADO'] == 'X') {
         // Cambio de creditaje anulado
         //fxScreen(0);
         fxHeader('Tdo5010.php', "");
      } elseif ($lo->paData['CESTADO'] == 'U') {
         fxHeader('Tdo5010.php', "");
      }
      fxScreen(0); 
   }

   function fxConsultaUniAcaCosto(){
      $lo = new CMatricula();
      $laData = $_REQUEST['paData'];
      $lo->paData = ['CCODALU' => $laData['CCODALU']];
      $llOk = $lo->omConsultaUniAcaCosto();
      $_SESSION['paData']  = $_SESSION['GADATA'] + $lo->paData; //RECUPERA EL COSTO DEL CREDITO
      echo json_encode($lo->paData);
   }

   function fxGrabar() {
      $loD = new CDeudas(); 
      $loD->paData = $_SESSION['paData'];
      $llOk = $loD->omComprobarSuspensiones();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxScreen(0);
         return;
      }
      $llOk = $loD->omRevisarDeudasAlumnoMatriculaPorCredito();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxScreen(0);
         return;
      }
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + ['CCODALU' => $_SESSION['GADATA']['CCODALU']];
      $llOk = $lo->omRegistrarMatricula();
      if (!$llOk) {
         fxHeader('Tdo5010.php', $lo->pcError); 
      }
      fxHeader('Tdo5010.php', 'GRABACION DE PENSIÓN POR CRÉDITOS CONFORME'); // 'CREDITOS REGISTRADOS'
   }
   
   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('saUniAca', $_SESSION['paUniAca']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5160.tpl');
      return;
   }
?>