<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Revisar') {
      fxRevisar();
   } elseif (@$_REQUEST['Boton'] == 'Levantar') {
      fxGrabarLevantarDenegacion();
   } elseif (@$_REQUEST['Boton'] == 'Reporte') {
      fxReporte();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CMatricula();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omInitBandejaSolicitudesCursosJuradoDenegadas(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxRevisar() {
      $lo = new CMatricula();
      $lo->paData = ['CIDENTI' => $_REQUEST['pcCidenti'],'CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omRevisarSolicitudDenegada();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit(0);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData;
      fxScreen(1);  
   }

   function fxGrabarLevantarDenegacion(){
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['paData'];
      $llOk = $lo->omLevantarDenegacionSolicitudCursoJurado(); 
      if (!$llOk) {
         fxHeader('Tdo5080.php', $lo->pcError);
      }
      $llOk = $lo->omRevisarSolicitudDenegada();
      if (!$llOk) {        
         fxInit(0);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['paData'] + $lo->paData; 
      fxScreen(1);
   }

   function fxReporte(){
      $lo = new CMatricula();
      $laData = $_REQUEST['pcCidenti'];
      $lo->paData = ['CIDENTI' => $laData];
      $llOk = $lo->omGenerarDecretoCpjDenegado();
      if (!$llOk) {
         fxHeader('Tdo5080.php', $lo->pcError);
      }
      fxDocumento($lo->pcFile);
		fxScreen(0);
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5080.tpl');
   }
?>