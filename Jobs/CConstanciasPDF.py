#generar pdf
from reportlab.lib.pagesizes import A4, letter, landscape
from reportlab.pdfgen import canvas
from reportlab.lib.utils import ImageReader
import os
import shutil
import stat
from CBase import *
class CConstanciasPDF(CBase):
   def __init__(self):
      self.paData  = None
      #self.paDatos = None
      #self.laData  = None
      self.loSql   = None

   # ------------------------------------------------------------------------------
   # Generacion de PDFS de las constancias del paquete bachiller
   # 2019-02-15 LVA Creacion
   # ------------------------------------------------------------------------------
   def omGenerarConstancia(self):
      llOk = self.mxValParamGenerarConstancia()
      if not llOk:
         return False
      self.loSql = CSql()
      llOk = self.loSql.omConnect()
      if not llOk:
         self.pcError = self.loSql.pcError
         return False
      llOk = self.mxRecuperarDatosConstancia()
      if not llOk:
         return False
      llOk = self.mxGenerarPdf()
      self.loSql.omDisconnect()
      return llOk

   # ------------------------------------------------------------------------------
   # Generacion de PDFS de las constancias del paquete bachiller al pagar
   # 2019-02-15 LVA Creacion
   # ------------------------------------------------------------------------------ 
   def omGenerarConstanciaAlPagar(self):
      llOk = self.mxValParamGenerarConstancia()
      if not llOk:
         return False
      if self.loSql is None:
         self.pcError = "CONEXION CON BASE DE DATOS NO DEFINIDA"
         return False
      llOk = self.mxRecuperarDatosConstancia()
      if not llOk:
         return False
      llOk = self.mxGenerarPdf()
      return llOk

   def mxValParamGenerarConstancia(self):
      if not self.paData['CCODTRE'] and len(self.paData['CCODTRE']) != 8:
         self.pcError = "CODIGO DE TRAMITE NO DEFINIDO O INVALIDO"
         return False
      return True

   def mxRecuperarDatosConstancia(self):
      lcSql = "SELECT A.cIdCate, E.cDescri, D.cNomUni, TO_CHAR(A.tModifi, 'yyyy-mm-dd hh24:mi'), B.cCodAlu, F.cNombre, C.cNroDni FROM B04MTRE A INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog INNER JOIN A01MALU C ON C.cCodAlu = B.cCodAlu INNER JOIN S01TUAC D ON D.cUniAca = C.cUniAca INNER JOIN B03TDOC E ON E.cIdCate = B.cIdCate INNER JOIN S01MPER F ON F.CNRODNI = C.CNRODNI WHERE CCODTRE = '%s'"%(self.paData['CCODTRE'])
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:
         self.pcError = "NO SE ENCUENTRAN DATOS DEL TRAMITE: %s "%(lcSql)
         return False       
      self.paDatos = {'CIDCATE': R1[0][0], 'CDESCRI': R1[0][1], 'CNOMUNI': R1[0][2], 'TMODIFI': R1[0][3], 'CCODALU': R1[0][4], 'CNOMBRE': R1[0][5], 'CNRODNI': R1[0][6]}
      return True

   def mxGenerarPdf(self):
      ancho, alto = A4
      cabecera = ImageReader("../Images/ucsm-03.png")
      logo = ImageReader("../Images/ucsm-02.png")
      lcDir = "../EXP/D%s"%(self.paDatos['CNRODNI'])
      lcPath = "%s/P%s.pdf"%(lcDir, self.paData['CCODTRE'])
      '''if not os.path.isdir(lcDir):
         os.mkdir(lcDir)
         # S_IRWXU: READ/WRITE/EXECUTE - USER
         # S_IRWXG: READ/WRITE/EXECUTE - GROUP
         # S_IRWXO: READ/WRITE/EXECUTE - OTHERS
         os.chmod(lcDir, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)'''
      c = canvas.Canvas(lcPath, pagesize=landscape(A4))
      c.setLineWidth(.3)
      c.drawImage(cabecera, 20, alto - 375, width=400, height=85, mask='auto')
      c.line(75,alto-375,765,alto-375)
      c.drawImage(logo, 600, alto - 800, width=175, height=175, mask='auto')
      titulo = c.beginText(75, alto - 400)
      titulo.setFont("Courier-Bold", 20)
      titulo.textLine(self.paDatos['CDESCRI'])
      c.line(75,alto-415,600,alto-415)
      c.drawText(titulo)
      text = c.beginText(75, alto - 450)
      text.setFont("Courier", 17)
      text.textLine("Escuela Profesional: " + self.paDatos['CNOMUNI'])
      text.textLine("")
      text.textLine("Fecha: " + self.paDatos['TMODIFI'])
      text.textLine("")
      text.textLine("Codigo del Alumno: " + self.paDatos['CCODALU'])
      text.textLine("")
      text.textLine("Nombre del Alumno: " + self.paDatos['CNOMBRE'])
      c.drawText(text)
      foot = c.beginText(75, alto - 700)
      foot.setFont("Courier", 15)
      foot.textLine("CONSTANCIA GENERADA Y VALIDADA POR LA")
      if self.paDatos['CIDCATE'] == 'PQ0002':
         foot.textLine('OFICINA DE TUTORIA UNIVERSITARIA')
      elif self.paDatos['CIDCATE'] == 'CCCONL':
         foot.textLine('COORDINACION DE LABORATORIOS Y GABINETES')
      else:
         foot.textLine("ESCUELA PROFESIONAL DE " + self.paDatos['CNOMUNI'])
      c.drawText(foot)
      c.showPage()
      c.save()
      return True

   # ------------------------------------------------------------------------------
   # Generacion de PDFS de las constancias del paquete bachiller al pagar
   # 2019-02-15 LVA Creacion
   # ------------------------------------------------------------------------------ 
   def omEncontrarCertificado(self):
      llOk = self.mxValParamEncontrarCertificado()
      if not llOk:
         return False
      if self.loSql is None:
         self.pcError = "CONEXION CON BASE DE DATOS NO DEFINIDA"
         return False
      llOk = self.mxEncontrarNombreCertificado()
      if not llOk:
         return False
      llOk = self.mxEncontrarCertificado()
      return llOk

   def mxValParamEncontrarCertificado(self):
      if not self.paData['CCODTRE'] and len(self.paData['CCODTRE']) != 8:
         self.pcError = "CODIGO DE TRAMITE NO DEFINIDO O INVALIDO"
         return False
      return True

   def mxEncontrarNombreCertificado(self):
      lcSql = "SELECT A.cNroDni FROM B03MDEU A INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog WHERE C.cCodtre = '%s'"%(self.paData['CCODTRE'])  
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:
         self.pcError = "NO SE ENCUENTRAN DATOS DEL TRAMITE: %s "%(lcSql)
         return False       
      self.laData = {'CNRODNI': R1[0][0]}
      lcSql = "SELECT C.cCodTre, C.cEstPro, C.cDocDig FROM B03MDEU A INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog AND C.cIdCate = 'CCESTU' WHERE A.cEstado = 'C' AND A.cPaquet = 'B' AND C.cEtapa = 'D' AND A.cNroDni = '%s'"%(self.laData['CNRODNI'])  
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:
         self.pcError = "NO SE ENCUENTRAN DATOS DEL TRAMITE: %s "%(lcSql)
         return False       
      self.paDatos = {'CCODTRE': R1[0][0], 'CESTPRO': R1[0][1], 'CDOCDIG': R1[0][2]}
      return True

   def mxEncontrarCertificado(self):
      if self.paDatos['CESTPRO'] == 'R':
         lcSql = "UPDATE B04MTRE SET cEstPro = 'R', cDocDig = '%s' WHERE cCodTre = '%s'"%(self.paDatos['CDOCDIG'], self.paData['CCODTRE'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print lcSql
            self.pcError = "NO SE PUEDE ACTULIZAR CERTIFICADO DIGITAL DEL TRAMITE: [%s] "%(self.paData['CCODTRE'])
            return False
      else:
         lcDirSal = "../EXP/D%s/P%s.pdf"%(self.laData['CNRODNI'],self.paDatos['CCODTRE'])
         lcDirDes = "../EXP/D%s/P%s.pdf"%(self.laData['CNRODNI'],self.paData['CCODTRE'])
         shutil.copy(lcDirSal,lcDirDes)
      return True

   # ------------------------------------------------------------------------------
   # Generacion de PDFS de las constancias del paquete bachiller al pagar
   # 2019-02-15 LVA Creacion
   # ------------------------------------------------------------------------------ 
   def omEncontrarDiploma(self):
      llOk = self.mxValParamEncontrarDiploma()
      if not llOk:
         return False
      if self.loSql is None:
         self.pcError = "CONEXION CON BASE DE DATOS NO DEFINIDA"
         return False
      llOk = self.mxEncontrarNombreDiploma()
      if not llOk:
         return False
      llOk = self.mxEncontrarDiploma()
      return llOk

   def mxValParamEncontrarDiploma(self):
      if not self.paData['CCODTRE'] and len(self.paData['CCODTRE']) != 6:
         self.pcError = "CODIGO DE TRAMITE NO DEFINIDO O INVALIDO"
         return False
      return True

   def mxEncontrarNombreDiploma(self):
      lcSql = "SELECT A.cNroDni FROM B03MDEU A INNER JOIN B03DDEU B ON B.cIdDeud = A.cIdDeud INNER JOIN B04MTRE C ON C.cIdLog = B.cIdLog WHERE C.cCodtre = '%s'"%(self.paData['CCODTRE'])  
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:
         self.pcError = "NO SE ENCUENTRAN DATOS DEL TRAMITE: %s "%(lcSql)
         return False       
      self.laData = {'CNRODNI': R1[0][0]}
      return True

   def mxEncontrarDiploma(self):
      lcDirSal = "../EXP/D%s/P000002.pdf"%(self.laData['CNRODNI'])
      lcDirDes = "../EXP/D%s/P%s.pdf"%(self.laData['CNRODNI'],self.paData['CCODTRE'])
      shutil.copy(lcDirSal,lcDirDes)
      return True