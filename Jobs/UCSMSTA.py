#-*- coding: utf-8 -*-
import json
import time
from decimal import *
from CBase import *
from CTramitesAutomaticos import *

# ------------------------------------------------------
# Clase para tramite de pagos 
# ------------------------------------------------------
class CTramitePagos(CBase):
   
   def __init__(self):
      self.lcProyec = '2000-0'
      self.lcCodUsu = '0666'
      self.loSql1   = None   # UCSMINS
      self.loSql2   = None   # UCSMListener
      self.lcNroPag = None
      self.llTesis  = None
      self.llCpjur  = None
      self.llTitul  = None
      self.lcData   = ''
   
   # Valida parametro de retorno de procedimientos almacenados
   # 2020-06-02 JLF Creacion
   def mxValParamRetorno(self):
       try:
          laTmp = json.loads(self.lcData)
       except:
          print self.lcData
          self.pcError = 'VALOR DE RETORNO DE PROCEDIMIENTO INVALIDO'
          return False
       if 'ERROR' in laTmp:
          self.pcError = laTmp['ERROR']
          return False
       return True

   # Recupera los pagos realizados para activar tramites
   # 2018-12-19 LVA Revision
   def omInterfazTramitePagos(self):
      print 'Conecta DB Tramite Pagos..'
      self.loSql1 = CSql()
      llOk = self.loSql1.omConnect()
      if not llOk:
         self.pcError = self.loSql1.pcError
         return False
      print 'Recuperar pagos realizados ...'
      llOk = self.mxRecuperarPagosRealizados()
      self.loSql1.omDisconnect()
      return llOk

   # Recupera los pagos realizados
   def mxRecuperarPagosRealizados(self):
      lcSql = "SELECT cNroPag, cIdDeud FROM B03MDEU WHERE cEstado = 'B'" 
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:
         print 'NO HAY PAGOS A RECUPERAR'
         return True
      for laFila in R1:
         # Activa tramites
         self.llTesis = False
         self.llCpjur = False
         self.llTitul = False
         llOk = self.mxActivarTramites(laFila)
         if not llOk:
            print 'ERROR: [%s] EN DEUDA [%s] CON NRO PAGO [%s]'%(self.pcError, laFila[1], laFila[0])
            continue
            #return False
         # Actualiza pagos realizados
         if not self.llTesis or self.llCpjur or self.llTitul:
            lnOk = self.mxActualizarPago(laFila)
            if lnOk == 0:
               # Error
               return False
            elif lnOk == 1:
               # Ya se realizo el pago
               continue
      self.loSql1.omCommit()
      return True

   def mxActivarTramites(self, p_aFila):
      lcSql = """SELECT A.cIdLog, A.cIdCate, D.cFlujo, D.cUniAca, D.cNomUni, A.cCodAlu, E.cTipo, B.cNroDni, B.cPaquet
                        FROM B03DDEU A 
                        INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud 
                        INNER JOIN A01MALU C ON A.cCodAlu = C.cCodAlu 
                        INNER JOIN S01TUAC D ON D.cUniAca = C.cUniAca 
                        INNER JOIN B03TDOC E ON E.cIdCate = A.cIdCate
                        WHERE B.cNroPag = '%s'"""%(p_aFila[0])
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:
         print '*** ADVERTENCIA ***'
         print 'NUMERO DE PAGO [CNROPAG: %s] NO PUDO ACTIVAR TRAMITE'%(p_aFila[0])
         return True
      # Tramites - Flujos
      for laTmp in R1:
         lcSql = """SELECT cCodTre FROM B04MTRE WHERE cIdLog = '%s'"""%(laTmp[0])
         R2 = self.loSql1.omExecRS(lcSql)
         if self.loSql1.plOk and R2 and laTmp[6] != 'CT':
            print 'NUMERO DE PAGO [CNROPAG: %s] CON [CIDLOG %s] Y [CIDCATE %s] YA TIENE TRAMITE DIFINIDO'%(p_aFila[0], laTmp[0], laTmp[1])
            continue
         elif self.loSql1.plOk and R2 and laTmp[6] == 'CT' and (laTmp[1] in ['000001','000017','000018','000020','000029','000062','000065']):
            print 'NUMERO DE PAGO [CNROPAG: %s] CON [CIDLOG %s] Y [CIDCATE %s] YA TIENE TRAMITE DIFINIDO'%(p_aFila[0], laTmp[0], laTmp[1])
            continue
         laParam = {'CIDLOG': laTmp[0], 'CIDCATE': laTmp[1], 'CCODALU': laTmp[5], 'CCENCOS': '*', 'CNRODNI': laTmp[7], 'CPAQUET': laTmp[8]}
         llOk = True
         if laTmp[1] == 'CCCOND':
            if laTmp[8] == 'M':
               laParam['CCENCOS'] = '310'
               llOk = self.mxGrabarB04MTRE(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA ESCUELA - MATERIAL DIDACTICO'
            else:
               llOk = self.mxEscuelaProfesional(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA ESCUELA - MATERIAL DIDACTICO'
               #self.loSql1.omCommit()
               llOk = self.mxVerificarB04DTCL(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL VERIFICAR LIDERAZGO MORAL Y EMPRESARIAL'
         elif laTmp[1] == 'CCCONB':
            # Material Bibliografico
            laParam['CCENCOS'] = '034'
            llOk = self.mxGrabarB04MTRE(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA MATERIAL BIBLIOGRAFICO'
            #self.loSql1.omCommit()
            if laTmp[8] != 'M' and laTmp[8] != 'D':
               llOk = self.mxVerificarB04DTCL(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL VERIFICAR LIDERAZGO MORAL Y EMPRESARIAL'
         elif laTmp[1] == 'CCCONC':
            llOk = self.mxEscuelaProfesional(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA CLINICA ODONTOLOGICA'
            #self.loSql1.omCommit()
         elif laTmp[1] == 'PDAOTR':
            # AUTENTICACION OTROS DOCUMENTOS
            laParam['CCENCOS'] = '012'
            if laTmp[8] == 'T':
               llOk = self.mxGrabarB04MTRETitula(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA AUTENTICACION DE DOCUMENTOS TO'
               #self.loSql1.omCommit()
               llOk = self.mxVerificarB04DTCL(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL VERIFICAR LIDERAZGO MORAL Y EMPRESARIAL'
            else:
               llOk = self.mxGrabarB04MTRE(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA AUTENTICACION DE DOCUMENTOS'   
         elif laTmp[1] == 'PDACBU':
            # AUTENTICACION CERTIFICADOS CON BUSQUEDA
            laParam['CCENCOS'] = '012'
            if laTmp[8] == 'T':
               llOk = self.mxGrabarB04MTRETitula(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA AUTENTICACION DE DOCUMENTOS TO'
               #self.loSql1.omCommit()
               llOk = self.mxVerificarB04DTCL(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL VERIFICAR LIDERAZGO MORAL Y EMPRESARIAL'
            else:
               llOk = self.mxGrabarB04MTRE(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA AUTENTICACION DE DOCUMENTOS'
         elif laTmp[1] == '000113':
            # AUTENTICACION DE CERTIFICADOS CON BUSQUEDA PARA TITULACION
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'A', 'CCENCOS': '012'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_11('%s')"%(lcJson)
            R2 = self.loSql1.omExecRS(lcSql)                                
            if not R2:
               print lcSql
               self.pcError = self.loSql1.pcError
               return False
            self.lcData = R2[0][0]
            llOk = self.mxValParamRetorno()
            if not llOk:
               return False
         elif laTmp[1] == 'PDADOC':
            # AUTENTICACION DIPLOMAS GRADOS Y TITULOS
            laParam['CCENCOS'] = '012'
            if laTmp[8] == 'T':
               llOk = self.mxGrabarB04MTRETitula(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA AUTENTICACION DE DOCUMENTOS TO'
               #self.loSql1.omCommit()
               llOk = self.mxVerificarB04DTCL(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL VERIFICAR LIDERAZGO MORAL Y EMPRESARIAL'
            else:
               llOk = self.mxGrabarB04MTRE(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA AUTENTICACION DE DOCUMENTOS'
         elif laTmp[1] == 'PDACRG':
            # AUTENTICACION RESOLUCIONES Y GRADOS
            laParam['CCENCOS'] = '012'
            llOk = self.mxGrabarB04MTRE(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE PARA AUTENTICACION DE RESOLUCIONES Y GRADOS'
               return False
         elif laTmp[1] == 'CCCSYL':
            # TRAMITE DE COPIA DE SILABO CERTIFICADA
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'A'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_13('%s')"%(lcJson)
            R2 = self.loSql1.omExecRS(lcSql)                                
            if not R2:
               print lcSql
               self.pcError = self.loSql1.pcError
               return False
            self.lcData = R2[0][0]
            llOk = self.mxValParamRetorno()
            if not llOk:
               return False
         elif laTmp[1] == '000108':
            #TRAMITE DOCUMENTARIO PARA MESA DE PARTES VIRTUAL
            laParam['CCENCOS'] = '506'
            llOk = self.mxGrabarB04MTRE(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA PARA TRAMITE DOCUMENTARIO MPV'
         elif laTmp[1] == 'PQ0034':
            # AUTENTICACION OTROS DOCUMENTOS
            laParam['CCENCOS'] = '034'
            llOk = self.mxGrabarB04MTRE(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA CONSTANCIA DE TURNITIN'
            self.loSql1.omCommit()
            llOk = self.mxVerificarB04DTCL(laParam)
            if not llOk:
               self.pcError = 'ERROR AL VERIFICAR RESULTADO DE TURNITIN ERP'
         elif laTmp[1] == 'PQ0001':
            # Fotografia
            laParam['CCENCOS'] = '552'
            llOk = self.mxGrabarB04MTRE(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA DIGITALIZACION DE IMAGENES'
         elif laTmp[1] == 'PQ0002':
            # Liderazgo (revisa la escuela)
            laParam['CCENCOS'] = '032'
            llOk = self.mxGrabarB04MTRE(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE LIDERAZGO (ESCUELA)'
            #self.loSql1.omCommit()
            llOk = self.mxVerificarB04DTCL(laParam)
            if not llOk:
               self.pcError = 'ERROR AL VERIFICAR LIDERAZGO MORAL Y EMPRESARIAL'
         elif laTmp[1] == 'PQ0063' or laTmp[1] == 'PQ0064'or laTmp[1] == 'PQ0060'or laTmp[1] == 'PQ0061':
            # Liderazgo (revisa la escuela)
            laParam['CCENCOS'] = '310'
            llOk = self.mxGrabarB04MTRE(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DOCUMENTOS (ORAA)'
         elif laTmp[1] == 'PQ0003':
            # Partida de nacimiento
            if laTmp[8] == 'M' or laTmp[8] == 'D':
               laParam['CCENCOS'] = '310'
               llOk = self.mxGrabarB04MTRE(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE PARTIDA DE NACIMIENTO (ESCUELA)'
            else:
               llOk = self.mxEscuelaProfesional(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE PARTIDA DE NACIMIENTO (ESCUELA)'
         elif laTmp[1] == 'PQ0004':
            # DNI
            if laTmp[8] == 'M' or laTmp[8] == 'D':
               laParam['CCENCOS'] = '310'
               llOk = self.mxGrabarB04MTRE(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE DNI (ESCUELA)'
            else:
               llOk = self.mxEscuelaProfesional(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE DNI (ESCUELA)'
         elif laTmp[1] == 'PQ0005':
            # Declaracion jurada
            if laTmp[8] == 'M' or laTmp[8] == 'D':
               laParam['CCENCOS'] = '310'
               llOk = self.mxGrabarB04MTRE(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE DECLARACION JURADO (ESCUELA)'
            else:
               llOk = self.mxEscuelaProfesional(laParam)
               if not llOk:
                  self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE DECLARACION JURADA (ESCUELA)'
         elif laTmp[1] == 'PQ0006':
            # Certificados CISCO (EPIS)
            llOk = self.mxEscuelaProfesional(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE CERTIFICADOS CISCO (ESCUELA EPIS)'
         elif laTmp[1] == 'PQ0007':
            # Certificados JINIS (EPIS)
            llOk = self.mxEscuelaProfesional(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE CERTIFICADOS JINIS (ESCUELA EPIS)'
         elif laTmp[1] == 'CCCONL':
            #llOk = self.mxLaboratorio(laTmp)
            laParam['CCENCOS'] = '035'
            llOk = self.mxGrabarB04MTRE(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA CONSTANCIA DE LABORATORIOS'
            #self.loSql1.omCommit()
            llOk = self.mxVerificarB04DTCL(laParam)
            if not llOk:
               self.pcError = 'ERROR AL VERIFICAR CONSTANCIA DE LABORATORIOS'
         elif laTmp[1] == 'CCCSUC':
            # Certificado de Informatica
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_2('%s')"%(lcJson)
            R2 = self.loSql1.omExecRS(lcSql)                                        
            if not R2:
               print lcSql
               self.pcError = self.loSql1.pcError   # OJOLVA ESTA DEVOLVIENDO EL ERROR EN JSON???
               return False 
         elif laTmp[1] == 'PDAUSP':
            '''
            # DERECHO A TRAMITE DE AUSPICIO
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_2('%s')"%(lcJson)
            print lcSql
            R2 = self.loSql1.omExecRS(lcSql)                                        
            if not R2:
               self.pcError = self.loSql1.pcError
               return False
            '''   
         elif laTmp[1] == 'CCESTU' or laTmp[1] == 'CCEQUI':
            # Certificado Estudios
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_2('%s')"%(lcJson)
            R2 = self.loSql1.omExecRS(lcSql)                                
            if not R2:
               print lcSql
               self.pcError = self.loSql1.pcError
               return False
         elif laTmp[1] == 'CCCSID' or laTmp[1] == '000084':
            # Certificado Instituto Idiomas / Instituto Confucio
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_2('%s')"%(lcJson)
            R2 = self.loSql1.omExecRS(lcSql)
            if not R2:
               print lcSql
               self.pcError = self.loSql1.pcError
               return False
         elif laTmp[1] == 'CCLIDE':
            # Inscripcion Liderazgo
            lcSql = "UPDATE B04DTCL SET cEstTut = 'P' WHERE cNroDni = '%s' AND cUnidad = 'TU' AND cEstTut = 'I'"%(laParam['CNRODNI'])
            llOk = self.loSql1.omExec(lcSql)
            if not llOk:
               print lcSql
               self.pcError = self.loSql1.pcError
               return False
         elif laTmp[1] == '000001' or laTmp[1] == '000017' or laTmp[1] == '000018' or laTmp[1] == '000020' or laTmp[1] == '000029' or laTmp[1] == '000061' or laTmp[1] == '000062' or laTmp[1] == '000065':
            # Constancia de conducta # CTCOND 
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F', 'CCENCOS': '031'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_11('%s')"%(lcJson)
            R2 = self.loSql1.omExecRS(lcSql)                                
            if not R2:
               print lcSql
               self.pcError = self.loSql1.pcError
               return False
            self.lcData = R2[0][0]
            llOk = self.mxValParamRetorno()
            if not llOk:
               return False
         elif laTmp[6] == 'CT':
            # Constancias
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0]}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_9('%s')"%(lcJson)
            R2 = self.loSql1.omExecRS(lcSql)
            if not R2:
               print lcSql
               self.pcError = self.loSql1.pcError
               return False
         elif laTmp[6] == 'PQ':
            llOk = self.mxEscuelaProfesional(laParam)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR CABECERA DE TRANSACCION PARA REVISION DE DOCUMENTOS (CREADOS POR ESCUELA)'
         elif laTmp[1] == 'PDCJUR':
            self.llCpjur = True
         elif laTmp[1] == 'PDCJUC':
            self.llCpjur = True   
         elif laTmp[1] == 'PDDTRA':
            self.llTesis = True
         elif laTmp[1] == 'GTGTPR':
            self.llTitul = True
         else:
            # Otro concepto que no es material didactivo, laboratorio y biblioteca
            print 'CATEGORIA DOCUMENTOS [%s]'%(laTmp[1])
         if not llOk:
            return False 
      return True

   # Siguiente CCODTRE
   def mxStecCodTre(self):
      lcSql = "SELECT MAX(cCodTre) FROM B04MTRE"
      R1 = self.loSql1.omExecRS(lcSql)
      lcCodTre = '000000' if not R1 else R1[0][0]                           
      lnCodTre = int(lcCodTre) + 1
      lcCodTre = '00000' + str(lnCodTre)
      lcCodTre = lcCodTre[-6:]
      return lcCodTre

   def mxGrabarB04MTRE(self, p_aParam):
      # Siguiente CCODTRE
      lcCodTre = self.mxStecCodTre()
      # Inserta en maestro de tramites
      lcSql = """INSERT INTO B04MTRE (cCodTre, cIdCate, cIdLog, tFecha, mDetall, cEstado, cCcoDes, cCodUsu) VALUES
               ('%s', '%s', '%s', NOW(), '', 'F', '%s', 'U666')"""%(lcCodTre, p_aParam['CIDCATE'], p_aParam['CIDLOG'], p_aParam['CCENCOS'])
      llOk = self.loSql1.omExec(lcSql)
      if not llOk:
         print lcSql
      return llOk 

   def mxGrabarB04MTRETitula(self, p_aParam):
          # Siguiente CCODTRE
      lcCodTre = self.mxStecCodTre()
      # Inserta en maestro de tramites
      lcSql = """INSERT INTO B04MTRE (cCodTre, cIdCate, cIdLog, tFecha, mDetall, cEstado, cCcoDes, cCodUsu) VALUES
               ('%s', '%s', '%s', NOW(), '', 'S', '%s', 'U666')"""%(lcCodTre, p_aParam['CIDCATE'], p_aParam['CIDLOG'], p_aParam['CCENCOS'])
      llOk = self.loSql1.omExec(lcSql)
      if not llOk:
         print lcSql
      return llOk 

   def mxEscuelaProfesional(self, p_aParam):
      if p_aParam['CPAQUET'] == 'M' or p_aParam['CPAQUET'] == 'D':
         p_aParam['CCENCOS'] = '310'
      # Centro de costo
      else: 
         lcSql = """SELECT B.cCenCos FROM A01MALU A
                           INNER JOIN S01TCCO B ON B.cUniAca = A.cUniAca
                           WHERE A.cCodAlu = '%s'"""%(p_aParam['CCODALU'])
         R1 = self.loSql1.omExecRS(lcSql)
         if not R1:
            self.pcError = '*** ERROR *** NO EXISTE RELACION UNIDAD ACADEMICA - CENTRO DE COSTO'   # OJOLVA VALE ESTE PCERROR?
            return False
         elif R1[0][0] == '000':
            self.pcError = '*** ERROR *** NO HAY CENTRO DE COSTO DEFINIDO (000)'
            return False
         p_aParam['CCENCOS'] = R1[0][0]
      llOk = self.mxGrabarB04MTRE(p_aParam)
      return llOk 

   def mxVerificarB04DTCL(self, p_aParam):
      #RECUPERAR CCODTRE
      lcSql = "SELECT cCodTre FROM B04MTRE WHERE cIdLog = '%s'"%(p_aParam['CIDLOG'])
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:
         self.pcError = "TRAMITE NO ENCONTRADO"
         return False
      p_aParam['CCODTRE'] = R1[0][0]
      #PROCEMIENTO PARA ACTUALIZAR ESTADO Y GENERAR CONSTANCIAS
      lo = CTramitesAutomaticos()
      lo.paDatos = [p_aParam]
      lo.loSql = self.loSql1
      llOk = lo.mxActivarConstancia()
      return llOk

   '''def mxAutenticacionDocumento(self, p_aParam):
      #RECUPERAR CCODTRE
      lcSql = "SELECT cCodTre FROM B04MTRE WHERE cIdLog = '%s'"%(p_aParam['CIDLOG'])
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:
         self.pcError = "TRAMITE NO ENCONTRADO"
         return False
      p_aParam['CCODTRE'] = R1[0][0]
      #PROCEMIENTO PARA ACTUALIZAR ESTADO Y GENERAR CONSTANCIAS
      lo = CTramitesAutomaticos()
      lo.paDatos = [p_aParam]
      lo.loSql = self.loSql1
      llOk = lo.mxActivarConstancia()
      return llOk'''

   '''def mxAutenticacionDiploma(self, p_aParam):
      #RECUPERAR CCODTRE
      lcSql = "SELECT cCodTre FROM B04MTRE WHERE cIdLog = '%s'"%(p_aParam['CIDLOG'])
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:
         self.pcError = "TRAMITE NO ENCONTRADO"
         return False
      p_aParam['CCODTRE'] = R1[0][0]
      #PROCEMIENTO PARA ACTUALIZAR ESTADO Y GENERAR CONSTANCIAS
      lo = CTramitesAutomaticos()
      lo.paDatos = [p_aParam]
      lo.loSql = self.loSql1
      llOk = lo.mxActivarConstancia()
      return llOk'''

   def mxActualizarPago(self, p_aFila):   
      # Verifica si ya se recupero pago
      lcSql = "SELECT cEstado FROM B03MDEU WHERE cNroPag = '%s'"%(p_aFila[0])
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:
         print 'NUMERO DE PAGO [CNROPAG: %s] NO EXISTE EN B03MDEU'%(p_aFila[0])
         return 1
      elif R1[0][0] != 'B':
         return 1
      # Actualiza pago
      lcSql = "UPDATE B03MDEU SET cEstado = 'C', dRecepc = NOW() WHERE cNroPag = '%s'"%(p_aFila[0])
      llOk = self.loSql1.omExec(lcSql)
      if not llOk:
         print lcSql
         self.pcError = 'ERROR AL ACTUALIZAR ESTADO DE CABECERA DE DEUDAS'
         return 0          
      # Iterar todos los Tramites
      lcSql = """SELECT A.cIdLog FROM B03DDEU A 
                        INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud 
                        WHERE cNroPag = '%s'"""%(p_aFila[0])
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:
         print '*** ADVERTENCIA ***'
         print 'NUMERO DE PAGO [CNROPAG: %s] NO PUDO ACTIVAR TRAMITE'%(p_aFila[0])
         return True
      # Grabar Recibos
      i = 0
      for laTmp in R1:
         i += 1  
         lcRecibo = p_aFila[1] + '-' + str(i)
         lcSql = "UPDATE B03DDEU SET cRecibo = '%s' WHERE cIdLog = '%s'"%(lcRecibo, laTmp[0]) 
         llOk = self.loSql1.omExec(lcSql)
         if not llOk:
            print lcSql                    
            self.pcError = 'ERROR AL GRABAR RECIBO'
            return 0      
      return 2

   def mxActivarTramites_old(self, p_aFila):
      lcSql = """SELECT A.cIdLog, A.cIdCate, D.cFlujo, D.cUniAca, D.cNomUni 
                        FROM B03DDEU A 
                        INNER JOIN B03MDEU B ON B.cIdDeud = A.cIdDeud 
                        INNER JOIN A01MALU C ON A.cCodAlu = C.cCodAlu 
                        INNER JOIN S01TUAC D ON D.cUniAca = C.cUniAca 
                        WHERE B.cNroPag = '%s'"""%(p_aFila[0])
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:
         print '*** ADVERTENCIA ***'
         print 'NUMERO DE PAGO [CNROPAG: %s] NO PUDO ACTIVAR TRAMITE'%(p_aFila[0])
         return True
      # Flujos
      for laTmp in R1:
         if laTmp[1] == 'CCCOND':
            # Material didactico
            lcSql = "SELECT A.cCodUsu FROM B03DUSU A INNER JOIN S01TUSU B on B.cCodUsu = A.cCodUsu \
                     WHERE A.cIdCate = '%s' AND A.cUniAca = '%s' AND A.cNivel = '1' AND A.cEstado = 'A'"%(laTmp[1], laTmp[3])            
            R2 = self.loSql1.omExecRS(lcSql)                           
            if not R2:
               self.pcError = 'NO HAY SECRETARIA'
               return False         
            lcCodUsu = R2[0][0]
            lcSql = "INSERT INTO B03DLOG (cIdLog, mObserv, cNivel, cAproba, cCodUsu) \
                     VALUES ('%s', '', '1','N', '%s')"%(laTmp[0],lcCodUsu)
            llOk = self.loSql1.omExec(lcSql)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR EL SEGUIMIENTO DE ESCUELA PROFESIONAL'
               return False 
         elif laTmp[1] == 'CCCONL':
            # Laboratorio
            lcSql = "SELECT A.cCodUsu FROM B03DUSU A INNER JOIN S01TUSU B on B.cCodUsu = A.cCodUsu  \
                     WHERE A.cIdCate = '%s' AND A.cUniAca='%s' AND A.cNivel = '4' AND A.cEstado = 'A'"%(laTmp[1],laTmp[3])            
            R2 = self.loSql1.omExecRS(lcSql)                                        
            if not R2:
               self.pcError = 'NO HAY COORDINADOR DE LABORATORIOS'
               return False             
            lcCodUsu = R2[0][0]
            lcSql = "INSERT INTO B03DLOG (cIdLog, mObserv, cNivel, cAproba, cCodUsu) \
                     VALUES ('%s', '', '4','N', '%s')"%(laTmp[0],lcCodUsu)
            llOk = self.loSql1.omExec(lcSql)
            if not llOk:
               self.pcError = 'ERROR AL GENERAR EL SEGUIMIENTO DE LABORATORIOS'
               return False 
         elif laTmp[1] == 'CCCSUC':
            # Certificado de Informatica
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_2('%s')"%(lcJson)
            print lcSql
            R2 = self.loSql1.omExecRS(lcSql)                                        
            if not R2:
               self.pcError = self.loSql1.pcError
               return False 
         elif laTmp[1] == 'PDAUSP':
            # DERECHO A TRAMITE DE AUSPICIO
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_2('%s')"%(lcJson)
            print lcSql
            R2 = self.loSql1.omExecRS(lcSql)                                        
            if not R2:
               self.pcError = self.loSql1.pcError
               return False   
         elif laTmp[1] == 'CCESTU':
            # CERTIFICADO DE ESTUDIOS
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_2('%s')"%(lcJson)
            print lcSql
            R2 = self.loSql1.omExecRS(lcSql)                                        
            if not R2:
               self.pcError = self.loSql1.pcError
               return False
         elif laTmp[1] == 'CCCSID':
            # CERTIFICADO DEL INSTITUTO DE IDIOMAS
            laTmp = {'CIDCATE': laTmp[1], 'CIDLOG': laTmp[0], 'CESTADO': 'F'}
            lcJson = json.dumps(laTmp, sort_keys=True)
            lcSql = "SELECT P_B04MTRE_2('%s')"%(lcJson)
            print lcSql
            R2 = self.loSql1.omExecRS(lcSql)                                        
            if not R2:
               self.pcError = self.loSql1.pcError
               return False
         else:
            # Otro concepto que no es material didactivo, laboratorio y biblioteca
            print 'CATEGORIA DOCUMENTOSS [%s]'%(laTmp[1])
      return True

def main():
   lo = CTramitePagos()
   llOk = lo.omInterfazTramitePagos()
   if not llOk:
      print '*** ERROR ***'
      print lo.pcError
      return
   print '*** CONFORME ***'
    
main()    
