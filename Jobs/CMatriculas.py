# -*- coding: utf-8 -*-
import time
from CBase import *

# ------------------------------------------------------
# Clase para las Matriculas x Creditos de Alumnos
# ------------------------------------------------------
class CMatriculas(CBase):
   
   def __init__(self):
       self.loSqlP   = CSql()   # UCSMINS
       self.loSqlS   = CSqlServer()   # SQL SERVER
       self.loSqlL   = CSql()   # LISTENER
       self.pcError  = ''
       self.laDatos  = []
       self.laSerial = []

   # Conecta con SQL SERVER Y UCSMINS
   def mxConnectDB(self):
       llOk = self.loSqlS.omConnect()
       if not llOk:
          self.pcError = self.loSqlS.pcError
          return False
       llOk = self.loSqlP.omConnect()
       if not llOk:
          self.loSqlS.omDisconnect()
          self.pcError = self.loSqlP.pcError
          return False
       return True

   # Recupera los creditos pagados por alumnos
   # 2019-02-21 JLF y LVA
   def omCargarCreditosPagados(self, p_nFlag):
       if not p_nFlag in [1,2]:
          self.pcError = 'OPCION DE CARGA DE CREDITOS NO DISPONIBLE'
          return False
       print 'Conecta DB UCSMINS..'
       llOk = self.loSqlP.omConnect()
       if not llOk:
          self.pcError = self.loSqlP.pcError
          return False
       print 'Recuperar Pagos de Creditos...'
       if p_nFlag == 1:
          llOk = self.mxRecuperarCreditosPagados()
       else:
          llOk = self.mxRecuperarTodoCreditajePagado()
       if not llOk:
          self.loSqlP.omDisconnect()
          return False
       elif len(self.laDatos) == 0:
          self.loSqlP.omDisconnect()
          return True
       print 'Conecta DB SQL Server..'
       llOk = self.loSqlS.omConnect()
       if not llOk:
          self.pcError = self.loSqlS.pcError
          return False
       print 'Envia el creditaje pagado al Sql-Server ...'
       llOk = self.mxEnviarCreditajePagado()
       if not llOk:
          self.loSqlP.omDisconnect()
          self.loSqlS.omDisconnect()
          return False
       print 'Actualiza el estado del creditaje pagado ...'
       llOk = self.mxAcualizarEstadoCreditajeEnviado()
       self.loSqlP.omDisconnect()
       self.loSqlS.omDisconnect()
       return llOk

   # Recupera los pagos realizados
   def mxRecuperarCreditosPagados(self):
       # Carga periodo a procesar
       lcSql = "SELECT TRIM(cConten) FROM S01TVAR WHERE cVariab = 'PCPERCRE'" 
       R1 = self.loSqlP.omExecRS(lcSql)
       if not self.loSqlP.plOk:
          print lcSql
          self.pcError = self.loSqlP.pcError
          return False
       elif not R1 or not R1[0][0]:
          print 'PERIODO DE MATRICULAS POR CREDITAJE NO EXISTE'
          return False
       lcPeriod = R1[0][0]
       lcSql = "SELECT nSerial, cCodAlu FROM B05DMPC WHERE cTipo IN ('N','R') AND cEstado = 'U' AND cPeriod = '%s' AND cEstInf = 'A' ORDER BY cCodAlu"%(lcPeriod) 
       R1 = self.loSqlP.omExecRS(lcSql)
       if not self.loSqlP.plOk:
          print lcSql
          self.pcError = self.loSqlP.pcError
          return False
       elif not R1:
          print 'NO HAY PAGOS PARA MATRICULA POR CREDITAJE'
          return True
       for laFila in R1:
           lcSql = "SELECT cCodAlu, cPeriod, SUM(nCredit) FROM B05DMPC WHERE cTipo IN ('N','R') AND cEstado = 'U' AND cPeriod = '%s' AND cCodAlu = '%s' GROUP BY cCodAlu, cPeriod"%(lcPeriod, laFila[1])
           R2 = self.loSqlP.omExecRS(lcSql)
           if not self.loSqlP.plOk:
              print lcSql
              print self.loSqlP.pcError
              continue
           elif not R2:
              print lcSql
              print 'ALUMNO [%s] NO TIENE PAGOS'%(laFila[1])
              continue
           self.laDatos.append({'NSERIAL' : laFila[0], 'CCODALU' : laFila[1], 'CPERIOD' : R2[0][1], 'NCREDIT' : R2[0][2]})
           self.laSerial.append([laFila[1], laFila[0], 1])
       return True
   
   # Recupera los pagos realizados
   def mxRecuperarTodoCreditajePagado(self):
       # Carga periodo a procesar
       lcSql = "SELECT TRIM(cConten) FROM S01TVAR WHERE cVariab = 'PCPERCRE'" 
       R1 = self.loSqlP.omExecRS(lcSql)
       if not self.loSqlP.plOk:
          print lcSql
          self.pcError = self.loSqlP.pcError
          return False
       elif not R1 or not R1[0][0]:
          print 'PERIODO DE MATRICULAS POR CREDITAJE NO EXISTE'
          return True
       lcPeriod = R1[0][0]
       # Carga todos los pagos de creditaje realizados
       lcSql = "SELECT nSerial, cCodAlu, nCredit FROM B05DMPC WHERE cTipo IN ('N','R') AND cPeriod = '%s' AND cEstado = 'U' ORDER BY cCodAlu"%(lcPeriod)
       R1 = self.loSqlP.omExecRS(lcSql)
       if not self.loSqlP.plOk:
          print lcSql
          self.pcError = self.loSqlP.pcError
          return False
       elif not R1:
          return True
       lcCodAlu = '*'
       llFirst = True
       lnCredit = 0
       # Genera dos arreglos, self.laDatos con el creditaje total por alumno y self.laSerial con el detalle
       for laTmp in R1:
           if lcCodAlu != laTmp[1] and not llFirst:
              laData = {'CCODALU' : lcCodAlu, 'CPERIOD' : lcPeriod, 'NCREDIT' : lnCredit}
              self.laDatos.append(laData)
              lnCredit = 0
           lnCredit += laTmp[2]
           llFirst = False
           lcCodAlu = laTmp[1]
           self.laSerial.append([lcCodAlu, laTmp[0], 1])
       laData = {'CCODALU' : lcCodAlu, 'CPERIOD' : lcPeriod, 'NCREDIT' : lnCredit}
       self.laDatos.append(laData)
       return True


   # Envia el creditaje total pagado al Sql-Server, marca aquellos que no son actualizados
   def mxEnviarCreditajePagado(self):
       i = 0
       for laTmp in self.laDatos:
           lcSql = "EXEC dbo.uspCreditosPago @cCodAlm = %s, @cAñoPro = %s, @cSemPro = %s, @iNroCre = %s"%(laTmp['CCODALU'], laTmp['CPERIOD'][:4], laTmp['CPERIOD'][5], laTmp['NCREDIT'])
           llOk = self.loSqlS.omExec(lcSql)
           if not llOk:
              for j in range(0, len(self.laSerial)):
                  if self.laSerial[j][0] == laTmp['CCODALU']:
                     self.laSerial[j][2] = 0 
              print lcSql
              print "ERROR AL ACTUALIZAR CREDITAJE DE ALUMNO [%s]"%(laTmp['CCODALU'])
              continue
           i += 1
       print 'Leidos (PG): ', len(self.laDatos), ', enviados (SQL-SERVER):', i
       self.loSqlS.omCommit()
       return True

   # Actualiza el estado del detalle del creditaje pagado, solo aquellos que se enviaron conforme al Sql-Server
   def mxAcualizarEstadoCreditajeEnviado(self):
       j = 0
       for laTmp in self.laSerial:
           if laTmp[2] == 0:
              continue
           lcSql = "UPDATE B05DMPC SET cEstInf = 'E' WHERE nSerial = %s"%(laTmp[1])
           llOk = self.loSqlP.omExec(lcSql)
           if not llOk:
              print lcSql
              print "ERROR AL ACTUALIZAR ESTADO DE PAGO [%s] DE ALUMNO [%s]"%(laTmp[1], laTmp[0])
              continue
           j += 1
       print 'Estados actualizados ', j, ', de', len(self.laSerial)
       self.loSqlP.omCommit()
       return True
   
   # Recupera los creditos pagados por alumnos
   # 2019-03-05 JLF
   def omCargarPagosMatriculaxCreditos(self):
       print 'Conecta DB UCSMINS..'
       llOk = self.loSqlP.omConnect()
       if not llOk:
          self.pcError = self.loSqlP.pcError
          return False
       print 'Recuperar Deudas Pendientes de Pago...'
       llOk = self.mxRecuperarDeudasPendientesCreditaje()
       if not llOk:
          self.loSqlP.omDisconnect()
          return False
       elif len(self.laDatos) == 0:
          self.loSqlP.omDisconnect()
          return True
       print 'Conecta DB LISTENER..'
       llOk = self.loSqlL.omConnect(1)
       if not llOk:
          self.loSqlP.omDisconnect()
          self.pcError = self.loSqlL.pcError
          return False
       print 'Revisa las deudas con pagos en Contabilidad'
       llOk = self.mxRevisarPagosContabilidad()
       if not llOk:
          self.loSqlP.omDisconnect()
          self.loSqlL.omDisconnect()
          return False
       print 'Actualiza el estado del creditaje pagado ...'
       llOk = self.mxAcualizarEstadoCreditajePagado()
       self.loSqlP.omDisconnect()
       self.loSqlL.omDisconnect()
       return llOk
   
   # Recuperar Deudas Pendientes de Pago
   def mxRecuperarDeudasPendientesCreditaje(self):
       # Carga periodo a procesar
       lcSql = "SELECT TRIM(cConten) FROM S01TVAR WHERE cVariab = 'PCPERCRE'" 
       R1 = self.loSqlP.omExecRS(lcSql)
       if not self.loSqlP.plOk:
          print lcSql
          self.pcError = self.loSqlP.pcError
          return False
       elif not R1 or not R1[0][0]:
          print 'PERIODO DE MATRICULAS POR CREDITAJE NO EXISTE'
          return False
       lcPeriod = R1[0][0]
       lcSql = "SELECT nSerial, cCodAlu FROM B05DMPC WHERE cTipo IN ('N','R') AND cEstado = 'P' AND cPeriod = '%s' AND cEstPro = 'P' ORDER BY cCodAlu"%(lcPeriod) 
       R1 = self.loSqlP.omExecRS(lcSql)
       if not self.loSqlP.plOk:
          print lcSql
          self.pcError = self.loSqlP.pcError
          return False
       elif not R1:
          print 'NO HAY DEUDAS PENDIENTES PARA MATRICULA POR CREDITAJE'
          return True
       for laFila in R1:
           lcSql = "SELECT cCodAlu, cPeriod, SUM(nMonto) FROM B05DMPC WHERE cTipo IN ('N','R') AND cEstado IN ('U','P') AND cPeriod = '%s' AND cCodAlu = '%s' AND cEstPro = 'P' GROUP BY cCodAlu, cPeriod"%(lcPeriod, laFila[1])
           R2 = self.loSqlP.omExecRS(lcSql)
           if not self.loSqlP.plOk:
              print lcSql
              print self.loSqlP.pcError
              continue
           elif not R2:
              print lcSql
              print 'ALUMNO [%s] NO TIENE PAGOS'%(laFila[1])
              continue
           self.laDatos.append({'NSERIAL' : laFila[0], 'CCODALU' : laFila[1], 'CPERIOD' : R2[0][1], 'NMONTO' : R2[0][2]})
           self.laSerial.append([laFila[1], laFila[0], 0])
       return True
   
   # Revisa el monto total pagado, marca aquellos que no son pagados
   def mxRevisarPagosContabilidad(self):
       i = 0
       for laTmp in self.laDatos:
           lcSql = "SELECT cCodAlu, SUM(nMonto) FROM B09DPAG WHERE cProyec = '%s-%s' AND cNroCuo = '01' AND cCodAlu = '%s' GROUP BY cCodAlu"%(laTmp['CPERIOD'][:4], laTmp['CPERIOD'][5], laTmp['CCODALU'])
           R1 = self.loSqlL.omExecRS(lcSql)
           if not self.loSqlL.plOk:
              print lcSql
              print "ERROR AL CONSULTAR PAGOS DE ALUMNO [%s]"%(laTmp['CCODALU'])
              continue
           elif not R1:
              #print "ALUMNO [%s] NO TIENE PAGOS REGISTRADOS"%(laTmp['CCODALU'])
              continue
           elif R1[0][1] < laTmp['NMONTO']:
              # ALUMNO NO PAGO EN SU TOTALIDAD SUS CREDITOS
              continue
           for j in range(0, len(self.laSerial)):
               if self.laSerial[j][0] == laTmp['CCODALU']:
                  self.laSerial[j][2] = 1
           i += 1
       print 'Leidos (UCSMINS): ', len(self.laDatos), ', pagados (LISTENER):', i
       return True

   # Actualiza el estado del creditaje pagado, de acuerdo a lo recuperado del LISTENER
   def mxAcualizarEstadoCreditajePagado(self):
       j = 0
       for laTmp in self.laSerial:
           if laTmp[2] == 0:
              continue
           lcSql = "UPDATE B05DMPC SET cEstado = 'U', cUsuCod = 'U666', tModifi = NOW() WHERE nSerial = %s"%(laTmp[1])
           llOk = self.loSqlP.omExec(lcSql)
           if not llOk:
              print lcSql
              print "ERROR AL ACTUALIZAR ESTADO DE PAGO [%s] DE ALUMNO [%s]"%(laTmp[1], laTmp[0])
              continue
           j += 1
       print 'Estados actualizados ', j, ', de', len(self.laSerial)
       self.loSqlP.omCommit()
       return True
   
   # Recupera los pagos de inscripción precatolica
   # 2020-04-23 JLF
   def omCargarPagosInscripcionPrecatolica(self):
       print 'Conecta DB UCSMINS..'
       llOk = self.loSqlP.omConnect()
       if not llOk:
          self.pcError = self.loSqlP.pcError
          return False
       print 'Recuperar Deudas Pendientes de Pago...'
       llOk = self.mxRecuperarDeudasPendientesPrecatolica()
       if not llOk:
          self.loSqlP.omDisconnect()
          return False
       elif len(self.laDatos) == 0:
          self.loSqlP.omDisconnect()
          return True
       print 'Conecta DB LISTENER..'
       llOk = self.loSqlL.omConnect(1)
       if not llOk:
          self.loSqlP.omDisconnect()
          self.pcError = self.loSqlL.pcError
          return False
       print 'Revisa las deudas con pagos en Contabilidad'
       llOk = self.mxRevisarPagosPrecatolica()
       if not llOk:
          self.loSqlP.omDisconnect()
          self.loSqlL.omDisconnect()
          return False
       print 'Actualiza el estado del creditaje pagado ...'
       llOk = self.mxAcualizarPagoInscripcionPrecatolica()
       self.loSqlP.omDisconnect()
       self.loSqlL.omDisconnect()
       return llOk
   
   # Recuperar Deudas Pendientes de Pago
   def mxRecuperarDeudasPendientesPrecatolica(self):
       # Carga periodo a procesar
       lcSql = "SELECT TRIM(cConten) FROM S01TVAR WHERE cVariab = 'PCPERPRE'" 
       R1 = self.loSqlP.omExecRS(lcSql)
       if not self.loSqlP.plOk:
          print lcSql
          self.pcError = self.loSqlP.pcError
          return False
       elif not R1 or not R1[0][0]:
          print 'PERIODO DE MATRICULAS POR CREDITAJE NO EXISTE'
          return False
       lcPeriod = R1[0][0]
       lcSql = "SELECT nSerial, cCodAlu, cPeriod, nMonto FROM B05DMPC WHERE cTipo IN ('P') AND cEstado = 'P' AND cPeriod = '%s' AND cEstPro = 'P' ORDER BY cCodAlu"%(lcPeriod) 
       R1 = self.loSqlP.omExecRS(lcSql)
       if not self.loSqlP.plOk:
          print lcSql
          self.pcError = self.loSqlP.pcError
          return False
       elif not R1:
          print lcSql
          print 'NO HAY DEUDAS PENDIENTES PARA INSCRIPCION PRECATOLICA'
          return True
       for laFila in R1:
           self.laDatos.append({'NSERIAL' : laFila[0], 'CCODALU' : laFila[1], 'CPERIOD' : laFila[2], 'NMONTO' : laFila[3]})
       return True
   
   # Revisa el monto pagado
   def mxRevisarPagosPrecatolica(self):
       i = 0
       for laTmp in self.laDatos:
           lcSql = "SELECT cCodAlu, SUM(nMonto) FROM B09DPAG WHERE cProyec = '%s' AND cNroCuo = '01' AND cCodAlu = '%s' GROUP BY cCodAlu"%(laTmp['CPERIOD'], laTmp['CCODALU'])
           R1 = self.loSqlL.omExecRS(lcSql)
           if not self.loSqlL.plOk:
              print lcSql
              print "ERROR AL CONSULTAR PAGOS DE ALUMNO [%s]"%(laTmp['CCODALU'])
              continue
           elif not R1:
              #print "ALUMNO [%s] NO TIENE PAGOS REGISTRADOS"%(laTmp['CCODALU'])
              continue
           elif R1[0][1] < laTmp['NMONTO']:
              # ALUMNO NO PAGO EN SU TOTALIDAD SUS CREDITOS
              continue
           self.laSerial.append([laTmp['NSERIAL']])
           i += 1
       print 'Leidos (UCSMINS): ', len(self.laDatos), ', pagados (LISTENER):', i
       return True

   # Actualiza el estado del creditaje pagado, de acuerdo a lo recuperado del LISTENER
   def mxAcualizarPagoInscripcionPrecatolica(self):
       j = 0
       for laTmp in self.laSerial:
           lcSql = "UPDATE B05DMPC SET cEstado = 'U', cUsuCod = 'U666', tModifi = NOW() WHERE nSerial = %s"%(laTmp[0])
           llOk = self.loSqlP.omExec(lcSql)
           if not llOk:
              print lcSql
              print "ERROR AL ACTUALIZAR ESTADO DE INSCRIPCION [%s]"%(laTmp[0])
              continue
           j += 1
       print 'Estados actualizados ', j, ', de', len(self.laSerial)
       self.loSqlP.omCommit()
       return True