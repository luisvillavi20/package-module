import json
import time
import requests
#import urllib2
#from decimal import *
from CBase import *
from CMaterialBibliografico import *
from CConstanciasPDF import *
#######################################################
## Clase para Aprobacion Automatica de Tramites
#######################################################
class CTramitesAutomaticos(CBase):
   def __init__(self):
      self.lcProyec = '2000-0'
      self.lcCodUsu = '0666'
      self.pcError  = ''
      self.loSql    = None
      self.paDatos  = []

   # --------------------------------------------------------------------
   # Carga tramites pendientes de aprobacion
   # 2019-02-13 LVA Creacion
   # -------------------------------------------------------------------
   def omCargarPendientes(self):
      print 'Conecta DB Tramite...'
      self.loSql = CSql()
      llOk = self.loSql.omConnect()
      if not llOk:
         self.pcError = self.loSql.pcError
         return False
      print 'Recuperar tramites pendientes ...'
      llOk = self.mxRecuperarEstadoConstancia()
      if not llOk:
         self.loSql.omDisconnect()
         return False
      llOk = self.mxActivarConstancia()
      self.loSql.omCommit()
      self.loSql.omDisconnect()
      return llOk

   def mxRecuperarEstadoConstancia(self):
      lcSql = "SELECT C.cNroDni, A.cIdLog, A.cIdCate, A.cCodTre FROM B04MTRE A \
					INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog \
               INNER JOIN B03MDEU C ON C.cIdDeud = B.cIdDeud \
					WHERE B.cIdCate IN ('CCCONB', 'PQ0034', 'PQ0002', 'CCCOND', 'CCCONL') AND A.cEstado IN ('A', 'E', 'F')"
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:
         print 'NO HAY PENDIENTES A RECUPERAR'
         return True
      for laFila in R1:
         laTmp = {'CNRODNI': laFila[0], 'CIDLOG': laFila[1], 'CIDCATE': laFila[2], 'CCODTRE': laFila[3]}
         self.paDatos.append(laTmp)
      return True

   def mxActivarConstancia(self):      
      for laFila in self.paDatos:
         # CREAR CARPETA SI SE REQUIERE
         lcDir = "../EXP/D%s"%(laFila['CNRODNI'])
         lcPath = "%s/P%s.pdf"%(lcDir, laFila['CCODTRE'])
         if not os.path.isdir(lcDir):
            os.mkdir(lcDir)
            # S_IRWXU: READ/WRITE/EXECUTE - USER
            # S_IRWXG: READ/WRITE/EXECUTE - GROUP
            # S_IRWXO: READ/WRITE/EXECUTE - OTHERS
            os.chmod(lcDir, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
         # FIN CREACION CARPETA      
         if laFila['CIDCATE'] == 'PQ0002': # LIDERAZGO
            self.mxLiderazgo(laFila)
         elif laFila['CIDCATE'] == 'CCCOND': # CONSTANCIA MATERIAL DIDACTICO
            self.mxMaterialDidactico(laFila)
         elif laFila['CIDCATE'] == 'CCCONL': # CONSTANCIA MATERIAL DE LABORATORIOS
            self.mxCoordLaboratorio(laFila)
         elif laFila['CIDCATE'] == 'CCCONB': # CONSTANCIA MATERIAL BIBLIOGRAFICO
            self.mxMatBibliografico(laFila)
         elif laFila['CIDCATE'] == 'PDAOTR': # AUTENTICACION OTROS DOCUMENTOS
            self.mxAutenticacionDocumentos(laFila)
         elif laFila['CIDCATE'] == 'PDADOC': # AUTENTICACION DIPLOMAS GRADOS Y TITULOS 
            self.mxAutenticacionDiplomas(laFila)
         elif laFila['CIDCATE'] == 'PQ0034': # TURNITIN
            self.mxConTurnitin(laFila)
      #self.loSql.omCommit()
      return True

   def mxLiderazgo(self, p_aFila):
      lcSql = """SELECT cEstTut FROM B04DTCL WHERE cUnidad = 'TU' AND cNroDni = '%s'"""%(p_aFila['CNRODNI'])         
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:
         lcSql = "UPDATE B04MTRE SET cEstado = 'E', mObserv = 'NO REALIZO EL CURSO DE LIDERAZGO MORAL Y EMPRESARIAL' WHERE cIdLog = '%s'"%(p_aFila['CIDLOG'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL ACTUALIZAR TRAMITE(OBSERVAR): %s'%(lcSql)
      elif R1[0][0] == 'A': # APROBADO
         lcSql = "UPDATE B04MTRE SET cEstado = 'B' WHERE cIdLog = '%s'"%(p_aFila['CIDLOG'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL ACTUALIZAR TRAMITE(APROBAR): %s'%(lcSql)
         #GENERAR CONSTANCIA
         lo = CConstanciasPDF()
         lo.paData = p_aFila
         lo.loSql = self.loSql
         llOk = lo.omGenerarConstanciaAlPagar()
         if not llOk:
            print lo.pcError
      elif R1 == 'D': # DESAPROBADO
         lcSql = "UPDATE B04MTRE SET cEstado = 'E', mObserv = 'NO APROBO EL CURSO DE LIDERAZGO MORAL Y EMPRESARIAL' WHERE cIdLog = '%s'"%(p_aFila['CIDLOG'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL ACTUALIZAR TRAMITE(OBSERVAR): %s'%(lcSql)
      else:
         lcSql = "UPDATE B04MTRE SET cEstado = 'E', mObserv = 'NO REALIZO EL CURSO DE LIDERAZGO MORAL Y EMPRESARIAL' WHERE cIdLog = '%s'"%(p_aFila['CIDLOG'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL ACTUALIZAR TRAMITE(OBSERVAR): %s'%(lcSql)

   def mxAutenticacionDocumentos(self, p_aFila):
         #GENERAR CONSTANCIA
         lo = CConstanciasPDF()
         lo.paData = p_aFila
         lo.loSql = self.loSql
         llOk = lo.omEncontrarCertificado()
         if not llOk:
            print lo.pcError
            
   def mxAutenticacionDiplomas(self, p_aFila):
         #GENERAR CONSTANCIA
         lo = CConstanciasPDF()
         lo.paData = p_aFila
         lo.loSql = self.loSql
         llOk = lo.omEncontrarDiploma()
         if not llOk:
            print lo.pcError

   def mxMaterialDidactico(self, p_aFila):
      lcSql = """SELECT mDescri as mDescri FROM B04DTCL WHERE cUnidad = 'MD' AND cEstmat = 'D' AND cNroDni = '%s'"""%(p_aFila['CNRODNI'])
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:              # NO TIENE DEUDA DE MATERIAL DIDACTICO
         lcSql = "UPDATE B04MTRE SET cEstado = 'B' WHERE cIdLog = '%s'"%(p_aFila['CIDLOG'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL ACTUALIZAR TRAMITE(APROBAR): %s'%(lcSql)
         #GENERAR CONSTANCIA
         lo = CConstanciasPDF()
         lo.paData = p_aFila
         lo.loSql = self.loSql
         llOk = lo.omGenerarConstanciaAlPagar()
         if not llOk:
            print lo.pcError
      else:
         lcSql = "UPDATE B04MTRE SET cEstado = 'E', mObserv = '%s' WHERE cIdLog = '%s'"%(R1[0][0], p_aFila['CIDLOG'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL ACTUALIZAR TRAMITE(OBSERVAR): %s'%(lcSql)

   def mxCoordLaboratorio(self, p_aFila):
      lcSql = """SELECT mDescri as mDescri FROM B04DTCL WHERE cUnidad = 'CL' AND cEstLab = 'D' AND cNroDni = '%s'"""%(p_aFila['CNRODNI'])
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:              # NO TIENE DEUDA DE LABORATORIO
         lcSql = "UPDATE B04MTRE SET cEstado = 'B' WHERE cIdLog = '%s'"%(p_aFila['CIDLOG'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL ACTUALIZAR TRAMITE(APROBAR): %s'%(lcSql)
         #GENERAR CONSTANCIA
         lo = CConstanciasPDF()
         lo.paData = p_aFila
         lo.loSql = self.loSql
         llOk = lo.omGenerarConstanciaAlPagar()
         if not llOk:
            print lo.pcError
      else:
         lcSql = "UPDATE B04MTRE SET cEstado = 'E', mObserv = '%s' WHERE cIdLog = '%s'"%(R1[0][0], p_aFila['CIDLOG'])
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL ACTUALIZAR TRAMITE(OBSERVAR): %s'%(lcSql)
   
   def mxMatBibliografico(self, p_aFila):
      lo = CMaterialBibliografico()
      lcSql = "SELECT A.cCodTre, B.cCodAlu, B.cIdDeud, A.cEstado FROM B04MTRE A INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog WHERE A.cIdLog = '%s'"%(p_aFila['CIDLOG'])
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:                          # DATOS DEL TRAMITE NO ENCONTRADO
         print "DATOS DEL TRAMITE NO ENCONTRADO: %s"%(lcSql)
      #laData = {'CCODTRE': R1[0][0], 'CCODALU': R1[0][1], 'CIDDEUD': R1[0][2], 'CESTADO': R1[0][3]}
      lo.loSql = self.loSql
      lo.paData = {'CCODTRE': R1[0][0], 'CCODALU': R1[0][1], 'CIDDEUD': R1[0][2], 'CESTADO': R1[0][3]}
      llOk =  lo.mxActivarConstancia()
      if not llOk:
         print lo.pcError
   
   def mxConTurnitin(self, p_aFila):
      lo = CMaterialBibliografico()
      lcSql = "SELECT A.cCodTre, B.cCodAlu, B.cIdDeud, A.cEstado FROM B04MTRE A INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog WHERE A.cIdLog = '%s'"%(p_aFila['CIDLOG'])
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:                          # DATOS DEL TRAMITE NO ENCONTRADO
         print "DATOS DEL TRAMITE NO ENCONTRADO: %s"%(lcSql)
      laData = {'CCODTRE': R1[0][0], 'CCODALU': R1[0][1], 'CIDDEUD': R1[0][2], 'CESTADO': R1[0][3]}
      lo.loSql = self.loSql
      lo.paDatos = {'CCODTRE': R1[0][0], 'CCODALU': R1[0][1], 'CIDDEUD': R1[0][2], 'CESTADO': R1[0][3]}
      llOk =  lo.mxActivarConstanciaTurnitin()
      if not llOk:
         print lo.pcError

def main():
	lo = CTramitesAutomaticos()
	llOk = lo.omCargarPendientes()
	if not llOk:
		print '*** ERROR ***'
		print lo.pcError
		return
	print '*** CONFORME ***'
    
#main()

def main():
	lo = CTramitesAutomaticos()
	llOk = lo.omCargarPendientes()
	if not llOk:
		print '*** ERROR ***'
		print lo.pcError
		return
	print '*** CONFORME ***'
#for laFila in lista:
#   lcRequest = requests.get('http://cib.ucsm.edu.pe/api-rest-biblio/revalidar-constancia?idexpediente=%s'%(laFila)) 
#   laJson = lcRequest.json()
#main()