#-*- coding: utf-8 -*-
import json
import time
from decimal import *
from CBase import *

#########################################################
## Clase para tramite de pagos 
#########################################################
class CTramitePagos(CBase):
   
   def __init__(self):
       self.lcProyec = '2000-0'
       self.lcCodUsu = '0666'
       self.pcError  = ''
       self.loSql1   = None   # UCSMINS
       self.loSql2   = None   # UCSMListener
       self.lcNroPag = None

   def omInterfazTramitePagos(self):
       print 'Conecta DB Tramite Pagos..'
       self.loSql1 = CSql()
       llOk = self.loSql1.omConnect()
       if not llOk:
          self.pcError = self.loSql1.pcError
          return False
       print 'Recuperar pagos realizados ...'
       llOk = self.mxRecuperarPagos()
       self.loSql1.omDisconnect()
       return llOk

   # Recupera pagos Realizados
   def mxRecuperarPagos(self):
       lcSql = "SELECT cNroPag, cIdDeud, cIdCate FROM V_B03DDEU_1 WHERE cEstado = 'A'" 
       R1 = self.loSql1.omExecRS(lcSql)
       if not R1:
          print 'NO HAY PAGOS A RECUPERAR'
          return True
       for laFila in R1:
           # Anula los Pagos por Inactividad
           lnOk = self.mxAnularPagos(laFila)
           if lnOk == 0:
              # Error
              return False
           elif lnOk == 1:
              # Ya se realizo el pago
              continue
       self.loSql1.omCommit()
       return True

   def mxAnularPagos(self, p_aFila):

       lcIdCate = p_aFila[2];  
       lcIdDeud = p_aFila[1];  
       lcNroPag = p_aFila[0];
       # Verifica si ya se recupero pago y fecha de vencimiento
       lcSql = "SELECT cEstado, TO_CHAR(tVencim, 'YYYY-MM-DD'), TO_CHAR(NOW(), 'YYYY-MM-DD') FROM B03MDEU WHERE cNroPag = '%s'"%(lcNroPag)
       R1 = self.loSql1.omExecRS(lcSql)
       if not R1:
          print 'CNROPAG [%s] No existe en B03MDEU'%(lcNroPag)
          return 1
       print '*** OKOKOKOKOK ***'
       # Comprueba pagos a anular
       ltVencim = str(R1[0][1]);
       ltFecAct = str(R1[0][2]);
       print 'tVencim [%s] '%(ltVencim)
       print 'tFecAct [%s] '%(ltFecAct)
       #Verifica que la fecha actual sea menor a la fecha de pago limite
       if ltVencim <= ltFecAct :
          print '*** OKOKOKOKOK ***'
          lcSql = "SELECT cIdDeud FROM V_B03DDEU_1 WHERE cEstado = 'P' AND cIdCate = '%s' ORDER BY tModifi ASC LIMIT 1"%(lcIdCate)
          R1 = self.loSql1.omExecRS(lcSql)
          if not R1:
             print 'CIDDEUD [%s] No existe en B03MDEU'%(lcIdDeud)
             return 1
          lcIdDeud = R1[0][0];
	       # Actualiza pagos pendiente
          lcSql = "UPDATE B03MDEU SET cEstado = 'A', tModifi = NOW(), dFecha = NOW() WHERE cIdDeud = '%s'"%(lcIdDeud)
          llOk = self.loSql1.omExec(lcSql)
          if not llOk:
             self.pcError = 'ERROR AL MAESTRO DE DEUDAS'
             return 0           
       return 2

def main():
    lo = CTramitePagos()
    llOk = lo.omInterfazTramitePagos()
    if not llOk:
       print '*** ERROR ***'
       print lo.pcError
       return
    print '*** CONFORME ***'
    
main()    
           
