import json
import time
import requests
#import urllib2
#from decimal import *
from CBase import *
#########################################################
## Clase para Constancias de Material Bibliografico
#########################################################
class CMaterialBibliografico(CBase):
   def __init__(self):
      self.lcProyec = '2000-0'
      self.lcCodUsu = '0666'
      self.pcError  = ''
      self.loSql    = None
      self.paDatos  = []
      

   # --------------------------------------------------------------------
   # Carga tramites pendientes de constancia de material bibliografico
   # 2019-02-12 LVA Creacion
   # -------------------------------------------------------------------
   def omCargarPendientes(self):
      print 'Conecta DB Tramite...'
      self.loSql = CSql()
      llOk = self.loSql.omConnect()
      if not llOk:
         self.pcError = self.loSql.pcError
         return False
      print 'Recuperar constancias de material bibliografico pendientes ...'
      llOk = self.mxRecuperarEstadoConstancia()
      if not llOk:
         self.loSql.omDisconnect()
         return
      llOk = self.mxActivarConstanciasBibTur()
      #llOk = self.mxActivarConstancia()      
      self.loSql.omDisconnect()
      return llOk

   def mxRecuperarEstadoConstancia(self):
      lcSql = "SELECT A.cCodTre, B.cCodAlu, A.cEstado, B.cIdDeud, A.cIdCate FROM B04MTRE A \
					INNER JOIN B03DDEU B ON B.cIdLog = A.cIdLog \
					WHERE B.cIdCate IN ('CCCONB','PQ0034') AND A.cEstado IN ('A', 'E', 'F')"      
      R1 = self.loSql.omExecRS(lcSql)
      if not R1:
         print 'NO HAY PENDIENTES A RECUPERAR'
         return True
      for laFila in R1:
         laTmp = {'CCODTRE': laFila[0], 'CCODALU': laFila[1], 'CESTADO': laFila[2], 'CIDDEUD': laFila[3], 'CIDCATE': laFila[4]}
         self.paDatos.append(laTmp)
      return True

   def mxActivarConstanciasBibTur(self):      
      for laFila in self.paDatos:
         if laFila['CIDCATE'] == 'CCCONB': # MATERIAL BIBLIOGRAFICO
            self.mxActivarConstancia(laFila)
         elif laFila['CIDCATE'] == 'PQ0034': # TURNITING
            self.mxActivarConstanciaTurnitin(laFila)
      #self.loSql.omCommit()
      return True

   def mxActivarConstancia(self):
      try:
         lcRequest = requests.get('codigo'%(self.paData['CCODALU']), timeout=5)
         laJson = lcRequest.json()
      except:
         print 'ERROR EN JSON(1) DE RETORNO DE API BIBLIOTECA'
         self.pcError = 'ERROR EN JSON(1) DE RETORNO DE API BIBLIOTECA'
         return False
      if laJson['result'] == 'ok':
         llOk = self.mxAprobarConstancia(self.paData)
         if not llOk:
            return True
      elif laJson['result'] == 'advertencia' and self.paData['CESTADO'] != 'O':
         laData = {'MOBSERV': laJson['mensaje'], 'CCODTRE': self.paData['CCODTRE']}
         llOk = self.mxObservarConstancia(laData)
         if not llOk:                   
            return True
      else:
         #print laJson['mensaje']
         self.pcError = laJson['mensaje']         
         #return False
      self.loSql.omCommit()
      return True
   
   def mxActivarConstanciaTurnitin(self,p_aFila):
      lcCodAlu = p_aFila['CCODALU']
      #lcCodAlu = self.paData['CCODALU']
      parametros = {'titulo':'','autores':[]}
      #trae parametros, TITULO Y AUTORES de la tesis
      self.loSql1 = CSql()
      llOk = self.loSql1.omConnect(2)
      if not llOk:
         self.pcError = self.loSql.pcError
         return False
      lcSql = "SELECT B.cIdTesi, B.mTitulo FROM T01DALU A INNER JOIN T01MTES B ON B.cIdTesi = A.cIdTesi WHERE A.cCodAlu = '%s'"%(lcCodAlu)
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:                          
         print "DATOS DEL TRAMITE NO ENCONTRADO: %s"%(lcSql)
         return False
      lcIdTesi = R1[0][0]
      lcTitulo = R1[0][1]
      lcSql = "SELECT cCodAlu FROM T01DALU WHERE cIdTesi = '%s'"%(lcIdTesi)
      R1 = self.loSql1.omExecRS(lcSql)
      if not R1:                          
         print "DATOS DEL TRAMITE NO ENCONTRADO: %s"%(lcSql)
         return False
      self.loSql1.omDisconnect()
      parametros['titulo']= lcTitulo
      for laFila in R1:
         laTmp1 = {'codigo':laFila[0]}
         parametros['autores'].append(laTmp1)
      try:
         lcRequest = requests.get('codigo', params=parametros,timeout=5)
         laJson = lcRequest.json()
      except:
         print 'ERROR EN JSON(1) DE RETORNO DE API BIBLIOTECA'
         self.pcError = 'ERROR EN JSON(1) DE RETORNO DE API BIBLIOTECA'
         return False
      if laJson['estado'] == '2':
         self.paData.append({'URL':laJson['url_pdf']})
         llOk = self.mxAprobarConstanciaTurnitin(self.paData)
         if not llOk:
            return True
      else:
         print laJson['mensaje']
         self.pcError = laJson['mensaje']         
         #return False
      self.loSql.omCommit()
      return True
			
	#Aprueba la constancia
   def mxAprobarConstancia(self, p_aFila):
      lcCodTre = p_aFila['CCODTRE']
      lcCodAlu = p_aFila['CCODALU']
      lcIdDeud = p_aFila['CIDDEUD']
      #Recuperar expediente de constancia
      try:
         lcRequest = requests.get('codigo'%(lcCodAlu, lcIdDeud), timeout=5)
         laJson = lcRequest.json()
      except:
         self.pcError = 'ERROR EN JSON DE RETORNO DE API(2) BIBLIOTECA'
         return False
      if laJson['result'] == 'ok':
         try:
            laExpediente = json.loads(laJson['expediente'])
         except:
            self.pcError = 'ERROR EN JSON DE RETORNO DE API(3) BIBLIOTECA'
            return False
         lcUrlPdf = laJson['url_pdf']
         ldVencim = datetime.datetime.fromtimestamp(laExpediente['fechaCaducidad']/1000.0).strftime("%Y-%m-%d")
         ltFecExp = datetime.datetime.fromtimestamp(laExpediente['fechaExpedicion']/1000.0).strftime("%Y-%m-%d %H:%M")
         lcCodInt = laExpediente['idExpediente']
         lcDetall = json.dumps({'CCODUSU':self.lcCodUsu,'DVENCIM':ldVencim,'TFECEXP':ltFecExp,"CURLPDF":lcUrlPdf,"CCODINT":lcCodInt})
         lcSql = "UPDATE B04MTRE SET cEstado = 'B', mDetall = '%s' WHERE cCodTre = '%s'"%(lcDetall, lcCodTre)
         llOk = self.loSql.omExec(lcSql)
         if not llOk:
            print 'ERROR AL APROBAR: %s'%(lcSql)
            self.pcError = 'ERROR AL ACTUALIZAR TRAMITE'
            return False
      else:
         print laJson['mensaje']
         self.pcError = laJson['mensaje']
         return False
      return True
	
   #Aprueba la constancia
   def mxAprobarConstanciaTurnitin(self, p_aFila):
      lcCodTre = p_aFila['CCODTRE']
      lcCodAlu = p_aFila['CCODALU']
      lcIdDeud = p_aFila['CIDDEUD']
      lcUrlPdf = p_aFila['URL']
      lcDetall = json.dumps({'CCODUSU':self.lcCodUsu,"CURLPDF":lcUrlPdf})
      lcSql = "UPDATE B04MTRE SET cEstado = 'B', mDetall = '%s' WHERE cCodTre = '%s'"%(lcDetall, lcCodTre)
      llOk = self.loSql.omExec(lcSql)
      if not llOk:
         print 'ERROR AL APROBAR: %s'%(lcSql)
         self.pcError = 'ERROR AL ACTUALIZAR TRAMITE'
         return False
      return True

   def mxObservarConstancia(self, p_aFila):
      lcCodTre = p_aFila['CCODTRE']
      lmObserv = p_aFila['MOBSERV']
      lcObserv = datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + lmObserv
      lcSql = "UPDATE B04MTRE SET mObserv = '%s' WHERE cCodTre = '%s'"%(lcObserv, lcCodTre)
      llOk = self.loSql.omExec(lcSql)
      if not llOk:
         print 'ERROR AL OBSERVAR: %s'%(lcSql)
         self.pcError = 'ERROR AL ACTUALIZAR TRAMITE (OBSERVACION)'
         return False
      return True

   def mxObservarConstanciaTurnitin(self, p_aFila):
      lcCodTre = p_aFila['CCODTRE']
      lmObserv = p_aFila['MOBSERV']
      lcObserv = datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + lmObserv
      lcSql = "UPDATE B04MTRE SET mObserv = '%s' WHERE cCodTre = '%s'"%(lcObserv, lcCodTre)
      llOk = self.loSql.omExec(lcSql)
      if not llOk:
         print 'ERROR AL OBSERVAR: %s'%(lcSql)
         self.pcError = 'ERROR AL ACTUALIZAR TRAMITE (OBSERVACION)'
         return False
      return True

def main():
	lo = CMaterialBibliografico()
	llOk = lo.omCargarPendientes()
	if not llOk:
		print '*** ERROR ***'
		print lo.pcError
		return
	print '*** CONFORME ***'
    
#main()