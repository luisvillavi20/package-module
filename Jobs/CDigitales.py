#!/usr/bin/env python
#-*- coding: utf-8 -*-
import sys
import json
import time
import random
import os
from decimal import *
from CBase import *
from datetime import datetime
reload(sys)
sys.setdefaultencoding('utf-8')

# ----------------------------------------------
# Interfaz con documentos digitales
# 2019-11-14 LVA Creacion
# ----------------------------------------------
class CDigitales(CBase):

   def __init__(self):
       self.paData   = []
       self.paDatos  = []
       self.laData   = []
       self.laDatos  = []
       self.loSql    = CSql()
       self.loSqlS   = CSqlServer()

   def mxValParam(self):
       if not 'CUSUCOD' in self.paData or len(self.paData['CUSUCOD']) != 4:
          self.pcError = 'CODIGO DE USUARIO NO DEFINIDO O INVALIDO'
          return False
       elif not 'CCENCOS' in self.paData or len(self.paData['CCENCOS']) != 3:
          self.pcError = 'CENTRO DE COSTO NO DEFINIDO O INVALIDO'
          return False
       return True
       
   # ------------------------------------------------------------
   # Registro de documentos ingresados a centro de costo
   # 2019-11-13 LVA Creacion
   # ------------------------------------------------------------
   def omProcesarDigitales(self):
       llOk = self.mxConectarDB()
       if not llOk:
          return False
       llOk = self.mxEnviarDocumentos()
       if not llOk:
          self.loSql.omDisconnect()
          self.loSqlS.omDisconnect()
          return False
       print '----------------'
       llOk = self.mxRecuperarDocumentos()
       self.loSql.omDisconnect()
       self.loSqlS.omDisconnect()
       return True

   def mxConectarDB(self):
       llOk = self.loSql.omConnect(2)
       if not llOk:
          self.pcError = self.loSql.pcError
          return False
       llOk = self.loSqlS.omConnect()
       if not llOk:
          self.pcError = self.loSqlS.pcError
          return False
       return True

   def mxEnviarDocumentos(self):
       lcSql = "(SELECT A.cCodTre, A.cIdCate, D.cCodAlu, D.cEmail FROM B04MTRE A \
                 INNER JOIN B04DTRE B ON B.cCodTre = A.cCodTre \
                 INNER JOIN B03DDEU C ON C.cIdLog = A.cIdlog \
                 INNER JOIN V_A01MALU D ON D.cCodAlu = C.cCodAlu \
                 INNER JOIN B03TDOC E ON E.cIdCate = A.cIdCate \
                 WHERE (E.cTipo = 'CC' OR A.cIdCate = '000062') AND A.cEstPro = 'N' AND B.cEstado = 'D') \
                UNION \
                (SELECT A.cCodTre, A.cIdCate, C.cCodAlu, C.cEmail FROM B04MTRE A \
                 INNER JOIN B03DDEU B ON B.cIdLog = A.cIdlog \
                 INNER JOIN V_A01MALU C ON C.cCodAlu = B.cCodAlu \
                 INNER JOIN B03TDOC D ON D.cIdCate = A.cIdCate \
                 WHERE D.cTipo = 'CT' AND A.cEstPro = 'N' AND A.cEstado = 'D') \
                ORDER BY cCodTre"
       R1 = self.loSql.omExecRS(lcSql)
       if not self.loSql.plOk:
          print lcSql
          self.pcError = '* ERROR: NO SE PUEDE CARGAR TRAMITES PENDIENTES DE ENVIO A BANDEJA DIGITAL'
          return False
       for r in R1:
           llOk = False
           lcSql = "EXEC dbo.uspT_Tramites_S @CodTre = '%s'"%(r[0])
           #print lcSql
           R1 = self.loSqlS.omExecRS(lcSql)
           if not self.loSqlS.plOk:
              print lcSql
              print 'ERROR AL VALIDAR TRAMITE [%s] EN SQL-SERVER'%(r[0])
              continue
           elif not R1:
              ltEnvio = datetime.today().strftime('%Y%m%d %H:%M')
              lcSql = """EXEC dbo.uspT_Tramites_I @CodTre = '%s', @Estado = 'A', @Envio = '%s', @IdCate = '%s', @Recep = '19000101 00:00', 
                         @Termin = '19000101 00:00', @Recupe = '19000101 00:00', @NroCer = '*', @CodAlm = '%s', @Email = '%s'"""%(r[0], ltEnvio, r[1], r[2], r[3])
              llOk = self.loSqlS.omExec(lcSql)
              if not llOk:
                 print lcSql
                 print '* ERROR: NO SE PUDO INSERTAR TRAMITE EN SQL-SERVER'
                 continue
           elif R1[0][1] in ['A','D']:
              ltEnvio = datetime.today().strftime('%Y%m%d %H:%M')
              lcSql = """EXEC dbo.uspT_Tramites_U @CodTre = '%s', @Estado = 'A', @Envio = '%s', @IdCate = '%s', @CodAlm = '%s', @Email = '%s'"""%(r[0], ltEnvio, r[1], r[2], r[3])
              llOk = self.loSqlS.omExec(lcSql)
              if not llOk:
                 print lcSql
                 print '* ERROR: NO SE PUDO ACTUALIZAR TRAMITE EN SQL-SERVER'
                 continue
           elif R1[0][1] in ['C']:
               print lcSql
               print '* ATENCION: CODIGO [%s] YA ESTA FINALIZADO EN SQL-SERVER'%(r[0])
               continue
           lcSql = "UPDATE B04MTRE SET cEstPro = 'S' WHERE cCodTre = '%s'"%(r[0])
           llOk = self.loSql.omExec(lcSql)
           if not llOk:
              print lcSql
              self.pcError = '* ERROR: NO SE PUDO ACTUALIZAR B04MTRE'
              return False
       self.loSqlS.omCommit()
       self.loSql.omCommit()
       return True

   def mxRecuperarDocumentos(self):
       lcSql = "SELECT A.cCodTre FROM B04MTRE A INNER JOIN B03TDOC B ON B.cIdCate = A.cIdCate \
                WHERE B.cTipo IN ('CC','CT','PD') AND A.cEstPro = 'S' ORDER BY A.cCodTre"
       R1 = self.loSql.omExecRS(lcSql)
       for r in R1:
           llOk = False
           lcSql = "EXEC dbo.uspT_Tramites_S @CodTre = '%s'"%(r[0])
           R1 = self.loSqlS.omExecRS(lcSql)
           if not R1:
              print '* NO SE ENCONTRARON CERTIFICADOS FIRMADOS'
              return True
           r = R1[0]
           #for r in R1:
           if r[1] == 'C' and r[7] == None:
              print lcSql
              print '* FIRMAS TERMNADAS SIN RUTA DE DOCUMENTO'
              continue
           elif r[1] == 'C':
              lcDocDig = r[7].replace("'", "''")
              lcSql = "UPDATE B04MTRE SET cEstado = 'S', cEstPro = 'R', cDocDig = '%s', cClave = '%s' WHERE cCodTre = '%s'"%(lcDocDig, r[8], r[0])
              llOk = self.loSql.omExec(lcSql)
              if not llOk:
                 print '* ERROR: NO SE PUDO ACTUALIZAR B04MTRE'
       self.loSql.omCommit()
       return True

   def omConsultar(self):
       llOk = self.loSqlS.omConnect()
       if not llOk:
          self.pcError = self.loSqlS.pcError
          return False
       lcSql = "EXEC dbo.uspT_Tramites_S"
       #lcSql = "EXEC dbo.uspT_Tramites_S @CodTre = '000001'"
       R1 = self.loSqlS.omExecRS(lcSql)
       for r in R1:
           print r[0], r[1]
       self.loSqlS.omDisconnect()
       return True

def main():
    lo = CDigitales()
    llOk = lo.omProcesarDigitales()    
    #llOk = lo.omConsultar()
    if not llOk:
       print lo.pcError
    else:
       print '*** CONFORME ***'

main()