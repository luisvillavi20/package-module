#! /bin/bash
while true
do
   lcHora=$(date +%H)
   lcDia=$(date +%-u)
   echo 'INICIO UCSMSTA ' >> LogTime.txt
   date >> LogTime.txt
   python UCSMSTA.py
   echo 'FIN UCSMSTA ' >> LogTime.txt
   date >> LogTime.txt
   echo 'INICIO TESIS ' >> LogTime.txt
   date >> LogTime.txt
   python MenuTramites.py 3
   echo 'FIN TESIS ' >> LogTime.txt
   date >> LogTime.txt
   echo 'INICIO PAGOS MATRICULA POR CREDITOS ' >> LogTime.txt
   date >> LogTime.txt
   python MenuTramites.py 4
   echo 'FIN PAGOS MATRICULA POR CREDITOS ' >> LogTime.txt
   date >> LogTime.txt
   echo 'INICIO ACTUALIZACION ALUMNOS ' >> LogTime.txt
   date >> LogTime.txt
   python MenuTramites.py 7
   echo 'FIN ACTUALIZACION ALUMNOS ' >> LogTime.txt
   date >> LogTime.txt
   echo 'INICIO ENVIO CREDITAJE PAGADO ' >> LogTime.txt
   date >> LogTime.txt
   python MenuTramites.py 1
   echo 'FIN ENVIO CREDITAJE PAGADO ' >> LogTime.txt
   date >> LogTime.txt
   python CDigitales.py
   echo 'DIGITALIZACION DE CERTIFICADOS ' >> LogTime.txt
   date >> LogTime.txt
   echo 'INICIO PAGOS INSCRIPCION PRECATOLICA ' >> LogTime.txt
   date >> LogTime.txt
   python MenuTramites.py 11
   echo 'FIN PAGOS INSCRIPCION PRECATOLICA ' >> LogTime.txt
   date >> LogTime.txt
   if [ "$lcHora" -eq "00" ]; then
      echo 'INICIO ACTUALIZACION UNIDADES ACADEMICAS ' >> LogTime.txt
      date >> LogTime.txt
      python MenuTramites.py 9
      echo 'FIN ACTUALIZACION UNIDADES ACADEMICAS ' >> LogTime.txt
      date >> LogTime.txt
      echo 'INICIO ACTUALIZACION CURSOS ' >> LogTime.txt
      date >> LogTime.txt
      python MenuTramites.py 5
      echo 'FIN ACTUALIZACION CURSOS ' >> LogTime.txt
      date >> LogTime.txt
      echo 'INICIO ACTUALIZACION TOTAL ALUMNOS ' >> LogTime.txt
      date >> LogTime.txt
      python MenuTramites.py 6
      echo 'FIN ACTUALIZACION TOTAL ALUMNOS ' >> LogTime.txt
      date >> LogTime.txt
      echo 'INICIO ENVIO CREDITAJE TOTAL PAGADO ' >> LogTime.txt
      date >> LogTime.txt
      python MenuTramites.py 2
      echo 'FIN ENVIO CREDITAJE TOTAL PAGADO ' >> LogTime.txt
      date >> LogTime.txt
      echo 'INICIO ACTUALIZACION DOCENTES ACTIVOS ' >> LogTime.txt
      date >> LogTime.txt
      python MenuTramites.py 12
      echo 'FIN ACTUALIZACION  DOCENTES ACTIVOS ' >> LogTime.txt
      date >> LogTime.txt
      if [ "$lcDia" -eq "01" ]; then
         echo 'INICIO ACTUALIZACION ALUMNOS INACTIVOS ' >> LogTime.txt
         date >> LogTime.txt
         python MenuTramites.py 10
         echo 'FIN ACTUALIZACION ALUMNOS INACTIVOS ' >> LogTime.txt
         date >> LogTime.txt
      fi
      sleep 3600
   elif [ "$lcDia" -ge "1" ] && [ "$lcDia" -le "5" ]; then
      if [ "$lcHora" -ge "08" ] && [ "$lcHora" -le "19" ]; then
         sleep 120
      else
         sleep 600
      fi
   else
      sleep 1800
   fi
done
