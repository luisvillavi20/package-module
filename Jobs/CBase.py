import psycopg2
import pymssql
import datetime
import hashlib
import time
from datetime import date, timedelta 

class CBase:
    def __init__(self):
        self.pcError   = None

class CSql(CBase):
    def __init__(self):
        self.h    = None
        self.plOk = True

    def omConnect(self, p_nFlag = 0):
        self.plOk = True
        if p_nFlag == 0:
           lcConnect = "host=localhost dbname=DB1 user=postgres password=postgres port=port"
        elif p_nFlag == 1:
           lcConnect = "host=localhost dbname=DB2 user=postgres password=postgres port=port"
        elif p_nFlag == 2:
           lcConnect = "host=localhost dbname=DB3 user=postgres password=postgres port=port"
        try:
           self.h = psycopg2.connect(lcConnect) 
        except psycopg2.DatabaseError:
           print 'Error conectando ...'
           self.plOk = False
           self.pcError = 'ERROR AL CONECTAR CON LA BASE DE DATOS'
        return self.plOk

    def omExecRS(self, p_cSql):
        #print p_cSql
        self.plOk = True
        lcCursor = self.h.cursor()
        try:
           lcCursor.execute(p_cSql)
           RS = lcCursor.fetchall()
        except psycopg2.DatabaseError as e:
           self.plOk = False
           print e.message
           self.pcError = 'ERROR AL EJECUTAR COMANDO SELECT'
           RS = None
        return RS

    def omExec(self, p_cSql):
        #print p_cSql
        self.plOk = True
        lcCursor = self.h.cursor()
        try:
           lcCursor.execute(p_cSql)
        except psycopg2.DatabaseError as e:
           self.plOk = False
           print e.message
           self.pcError = 'ERROR AL ACTUALIZAR LA BASE DE DATOS'
        return self.plOk

    def omDisconnect(self):
        self.h.close()

    def omCommit(self):
        self.h.commit()

class CSqlServer(CBase):
   def __init__(self):
       self.h    = None
       self.plOk = True

   def omConnect(self, p_nFlag = 0):
       i = 0
       while i < 10:
       	  i = i + 1
       	  llOk = self.mxConectar(p_nFlag)
       	  if llOk:
       	  	 break
       	  time.sleep(0.5)
       return llOk

   def mxConectar(self, p_nFlag):
       self.plOk = True
       try:
          # DB produccion
          self.h = pymssql.connect("DB", "UDB", "DB", "DB")
          #self.omExec("BEGIN TRANSACTION")
       except pymssql.DatabaseError as e:
          print e.message
          self.plOk = False
          self.pcError = 'ERROR AL CONECTAR CON SQL-SERVER'
       return self.plOk

   def omExecRS(self, p_cSql):
       #print p_cSql
       self.plOk = True
       lcCursor = self.h.cursor()
       try:
          lcCursor.execute(p_cSql)
          RS = lcCursor.fetchall()
       except pymssql.DatabaseError as e:
          print e.message
          self.plOk = False
          self.pcError = 'ERROR DE EJECUCION EN BASE DE DATOS'
          return None
       return RS

   def omExec(self, p_cSql):
       self.plOk = True
       lcCursor = self.h.cursor()
       try:
          lcCursor.execute(p_cSql)
       except pymssql.DatabaseError as e:
          self.plOk = False
          self.pcError = e.message
          print e.message
       return self.plOk

   def omDisconnect(self):
       self.h.close()

   def omCommit(self):
       self.h.commit()

class CDate(CBase):
   pcClave = None
   
   def mxValDate(self, p_cFecha):
       try:
          ldFecha = datetime.datetime.strptime(p_cFecha, "%Y-%m-%d").date()
       except:
          ldFecha = None
       return ldFecha

   def valDate(self, p_cFecha):
       llOk = True
       try:
          ldFecha = datetime.datetime.strptime(p_cFecha, "%Y-%m-%d").date()
       except:
          llOk = False
       return llOk
  
   def add(self, p_cFecha, p_nDias):
       llOk = self.valDate(p_cFecha)
       if not llOk:
          return None
       ldFecha = self.mxValDate(p_cFecha)
       ldFecha = ldFecha + timedelta(days = p_nDias)
       return ldFecha.strftime('%Y-%m-%d')
      
   def diff(self, p_cFecha1, p_cFecha2):
       llOk = self.valDate(p_cFecha1)
       if not llOk:
          return None
       llOk = self.valDate(p_cFecha2)
       if not llOk:
          return None
       ldFecha1 = self.mxValDate(p_cFecha1)
       ldFecha2 = self.mxValDate(p_cFecha2)
       d = ldFecha1 - ldFecha2
       return d.days

   def dow(self, p_cFecha):
       llOk = self.valDate(p_cFecha)
       if not llOk:
          return None
       ldFecha = self.mxValDate(p_cFecha)
       return ldFecha.weekday()

   def day(self, p_cFecha):
       llOk = self.valDate(p_cFecha)
       if not llOk:
          return None
       ldFecha = self.mxValDate(p_cFecha)
       return int(ldFecha.strftime('%d'))

   def month(self, p_cFecha):
       llOk = self.valDate(p_cFecha)
       if not llOk:
          return None
       ldFecha = self.mxValDate(p_cFecha)
       return int(ldFecha.strftime('%m'))

def fxElapsedTime(p_nTime):
    if p_nTime <= 0:
       return '0h00m00s'
    lnHours = int(p_nTime / 3600)
    lcTime = str(lnHours) + 'h'
    lnTime = p_nTime - lnHours * 3600
    lnMinute = int(lnTime / 60)
    lcMinute = '0' + str(lnMinute)
    lcTime = lcTime + lcMinute[:2] + 'm'
    lnTime = lnTime - lnMinute * 60
    lcSecond = '0' + str(lnTime)
    lcTime = lcTime + lcSecond[:5] + 's'
    return lcTime