<?php
   error_reporting(E_ALL);
   ini_set('display_errors',1);
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CConstancias.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'DetalleAlumno') {
      fxDetalleAlumno();
   } elseif (@$_REQUEST['Boton'] == 'Activar') {
      fxActivar();
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Reporte') {
      fxReporte();
   } else {
      fxInit();
   }   

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      if ($_SESSION['GADATA']['CNIVEL'] == 'D') {
         $lo->paData['CCODIGO'] = "B','T";
      }
      $llOk = $lo->omInitBandejaRevisionDocumentos();
      if (!$llOk) {
         fxHeader('Mnu1000.php', 'SIN PAQUETES PENDIENTES');
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxDetalleAlumno() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      if($_SESSION['GADATA']['CNIVEL'] == 'D') {
         $lo->paData['CCODIGO'] = "B','T";
      } 
      $llOk = $lo->omInitBandejaRevisionDocumentosAlumno();
      if (!$llOk) {
         fxAlert($lo->pcError);
         return fxScreen(0);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxActivar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omEditarTransaccion();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
         return;
      }

      $_SESSION['paData'] = $_SESSION['GADATA'];
      if ($lo->paDatos['CIDCATE'] != 'CCCOND' AND $lo->paDatos['CIDCATE'] != 'PQ0033') {
         $llOk = $lo->omDetalleRevisionDocumentos();
         if (!$llOk) {
            fxAlert($lo->pcError);
            fxScreen(1);
            return;
         }
         $_SESSION['paDatos'] = $lo->paDatos;
         fxScreen(2);
      } elseif ($lo->paDatos['CIDCATE'] == 'PQ0033'){
         $_SESSION['paData'] = $lo->paData;
         fxScreen(4);
         return;
      }
      else {
         $_SESSION['paDatos'] = $lo->paDatos;
         fxScreen(3);
      }      
   }

   function fxGrabar() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      if (!isset($laData['DVENCIM'])) {
         //UN AÑO DE VENCIMIENTO
         $laData['DVENCIM'] = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
      }
      $lo->paData = $laData;
      $llOk = $lo->omGrabarTransaccion();
      if (!$llOk) {
         fxHeader('Paq1060.php', $lo->pcError);
      }
      fxDetalleAlumno();
   }

   function fxReporte(){
      $lo = new CConstancias();
      $laData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omConstanciaEmpastadosTitulacion();
      if (!$llOk) {
         fxHeader('Paq1060.php', $lo->pcError);
      }
      fxDocumento("DocumentoPaquete.php?CNRODNI={$lo->paData['CNRODNI']}&CCODTRE={$lo->paData['CCODTRE']}");
      fxInit();
   }

   function fxSubirDocumento(){
      $lo = new CPaquetes();
      $lcTipDoc = $_REQUEST['paData']['CTIPDOC'];
      if($lcTipDoc == 'D') {
         $lcCodTre = '000002';
      }
      else {
         $lcCodTre = '000003';
      }
      $laData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']] + ['CCODALU' => $_SESSION['GADATA']['CCODALU']] +
                ['CCODTRE' => $lcCodTre] + ['CTIPDOC' => $lcTipDoc];
      $lo->paData = $laData;
      $lo->paFile = $_FILES['fAutenticacion'];
      $llOk = $lo->omSubirArchivo();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
      }
      else {
         $llOk = $lo->omInitRegistroDeAutenticacion();
         if (!$llOk) {
            fxAlert($lo->pcError);
         }
         header("HTTP/1.1 303 See Other");
         header("Location: http://$_SERVER[HTTP_HOST]/UCSMMTA/Paq1490.php");
      }
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1060.tpl');
   }
?>