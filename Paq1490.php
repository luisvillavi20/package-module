<?php
   //BANDEJA TRAMITES PENDIENTES
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CTramites.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Confirmar') {
      fxConfirmar();
   } elseif (@$_REQUEST['Boton'] == 'SubirDocumento') {
      fxSubirDocumento();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI'], 'CCODUSU' => $_SESSION['GADATA']['CCODUSU']
                    ,'CCODALU' => $_SESSION['GADATA']['CCODALU']];
      $llOk = $lo->omInitSubidaDeAutenticacion();
      if (!$llOk) {
         fxHeader('Mnu2000.php',$lo->pcError);
      } 
      $laData['CNOMBRE'] = $_SESSION['GADATA']['CNOMBRE'];
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['GADATA'] = $_SESSION['GADATA']; 
      fxScreen(0);
   }

   function fxSubirDocumento(){
      $lo = new CPaquetes();
      $lcTipDoc = $_REQUEST['paData']['CTIPDOC'];
      if($lcTipDoc == 'D') {
         $lcCodTre = '000002';
         $lo->paFile = $_FILES['fAutenticacion1'];
      }
      else {
         $lcCodTre = '000003';
         $lo->paFile = $_FILES['fAutenticacion2'];
      }
      $laData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']] + ['CCODALU' => $_SESSION['GADATA']['CCODALU']] +
                ['CCODTRE' => $lcCodTre] + ['CTIPDOC' => $lcTipDoc];
      $lo->paData = $laData;
      
      $llOk = $lo->omSubirArchivo();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
      }
      else {
         $llOk = $lo->omInitRegistroDeAutenticacion();
         if (!$llOk) {
            fxAlert($lo->pcError);
         }
         header("HTTP/1.1 303 See Other");
         header("Location: http://$_SERVER[HTTP_HOST]/UCSMMTA/Paq1490.php");
      }
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['GADATA']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1490.tpl');
   }
?>