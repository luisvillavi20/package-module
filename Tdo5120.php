<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CConstancias.php';
   require_once 'Clases/CEmail.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   }
   elseif (@$_REQUEST['Boton'] == 'Activar') {
      fxActivar();
   } elseif (@$_REQUEST['Boton'] == 'Aprobar') {
      fxAprobarCertificado();
   } elseif (@$_REQUEST['Id'] == 'Recepcion') {
      fxRecepcionar();
   } elseif (@$_REQUEST['Boton'] == 'Observar') {
      fxObsevarCertificado();
   } elseif(@$_REQUEST['Id'] == 'Opcion') {
      fxObservados();
   } elseif(@$_REQUEST['Boton'] == 'LevObserv') {
      fxLevantarObservacion();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CConstancias();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omIniBandejaTraduccionesConstanciasEspeciales(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxAprobarCertificado() {
      $lo = new CConstancias();
      $laData = ['CCODTRE'=>$_REQUEST['pcCodTre']]+['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $lo->paFile = $_FILES['fTraduccion'];
      $llOk = $lo->omAprobarCostanciaEspecialTraduccion(); 
      if (!$llOk) {
         fxHeader('Tdo5120.php', $lo->pcError);
      }
      fxInit();
   }

   function fxObsevarCertificado(){
      $lo = new CConstancias();
      $laData = $_REQUEST['paData'];
      $lo->paData = $laData + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omObervarCertificadoDeConductaTraduccion(); 
      if (!$llOk) {
         fxHeader('Tdo5120.php', $lo->pcError);
      }
      fxInit();
   }
   function fxLevantarObservacion(){
      $lo = new CConstancias();
      $lo->paData = ['CCODTRE'=>$_REQUEST['pcCodTre2']] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omBandejaLevConstaciaEspecialObserTraducciones();
      if (!$llOk) {
         fxHeader('Tdo5120.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      fxInit();
   }
   function fxObservados(){
      $lo = new CConstancias();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      if ($lo->paData['COPCION'] != 'P') {
         $llOk = $lo->omInitBandejaConductaObservadosTraduccion();
         $_SESSION['paDatos'] = $lo->paDatos;
         fxPrintOpcion(0);
      }
      else {
         fxScreen(0);
      }
      if (!$llOk) {
         fxHeader('Tdo5120.php', $lo->pcError);
      }
   }

   function fxRecepcionar() {
      $lo = new CConstancias();
      $lcCodTre = $_REQUEST['p_cCodTre'];
      $laData = ['CCODTRE' => $lcCodTre] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omRecepcionarConstanciaTraduccion();
      if (!$llOk) {
         fxHeader('Tdo5120.php', $lo->pcError);
      }
      fxInit();
   }

   function myFunctionShowObs(){
      $lo = new CConstancias();
      
      $laData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omBandejaLevConstaciaEspecialObserTraducciones(); 
      if (!$llOk) {
         fxHeader('Tdo5120.php', $lo->pcError);
      }
      fxInit();
   }

   function fxPrintOpcion() {
      global $loSmarty;
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->display('Plantillas/Tdo5121.tpl');
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5120.tpl');
   }
?>