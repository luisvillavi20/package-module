<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Activar') {
      fxActivar();
   } elseif (@$_REQUEST['Boton'] == 'Aprobar') {
      fxGrabarAprobar();
   } elseif (@$_REQUEST['Boton'] == 'Denegar') {
      fxGrabarDenegar();
   } elseif (@$_REQUEST['Boton'] == 'Solicitar') {
      fxGrabarDescuentoBachiller();
   } elseif(@$_GET['Id'] == 'Verificar') {
      fxVerificar();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omIniBandejaSolicitudes(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxActivar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omEditarSolicitud();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
         return;
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);  
   }

   function fxGrabarAprobar() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $lo->paData = $laData;
      $llOk = $lo->omAprobarSolicitud(); 
      if (!$llOk) {
         fxHeader('Paq1130.php', $lo->pcError);
      }
      fxInit();
   }
   
   function fxGrabarDenegar() {
      $lo = new CPaquetes();
      $laData = ['NSERIAL' => $_REQUEST['nSerial'], 'CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omGrabarSolicitudDenegada(); 
      if (!$llOk) {
         fxHeader('Paq1130.php', $lo->pcError);
      }
      fxInit();
   }

   function fxGrabarDescuentoBachiller() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $lo->paData = $laData;
      $llOk = $lo->omGrabarSolicitudDescuentoBachiller(); 
      if (!$llOk) {
         fxHeader('Paq1130.php', $lo->pcError);
      }
      fxInit();
   }

   function fxVerificar() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODALU' => $_REQUEST['CCODALU']] + $_SESSION['GADATA'];
      $llOk = $lo->omVerificarAlumnoxCod();
      if (!$llOk) {
         echo json_encode(["ERROR" => $lo->pcError]);
      } else {
         echo json_encode($lo->paData);
      }
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1130.tpl');
   }
?>