function f_Init() {
   $("#footer").load("Plantillas/footer.html");
}

function f_InitDAlu() {
   $.get( "Tdo2601.php?Id=DatosAlumno" ,
      function( data ) {
         $('#datosA').html(data);
   });
}

function f_InitDInf() {
   $.get( "Tdo2601.php?Id=DetalleInsInf" ,
      function( data ) {
         $('#datosInf').html(data);
   });
}

function f_InitDIdi() {
   $.get( "Tdo2601.php?Id=DetalleInsIdi" ,
      function( data ) {
         $('#datosIdi').html(data);
   });
}

function f_InitDCerEst() {
   $.get( "Tdo2601.php?Id=DetalleCerEst" ,
      function( data ) {
         $('#datosCer').html(data);
   });
}

function UpperCaseF(a) {
   a.value = a.value.toUpperCase();
}

function isNumber(n) {
   return !isNaN(parseFloat(n)) && isFinite(n);
}

function f_validateNumber(e, decimals) {
   var num = e.value;
   if (!isNaN(parseFloat(num)) && isFinite(num)) {
      e.value = parseFloat(Math.round(num * 100) / 100).toFixed(decimals);
   } else {
      e.value = 0;
   }
}

function isJson(str) {
   try {
      JSON.parse(str);
   } catch (e) {
      return false;
   }
   return true;
}