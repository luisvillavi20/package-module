<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CDeudas.php';
   require_once 'Clases/CBase.php';
   require_once 'Clases/CFormatos.php';
   require_once 'Clases/CPersona.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;   
   if (!fxSoloAlumnos()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'Cerrar Sesion') {
      fxCerrarSesion();
   } elseif (@$_REQUEST['Id'] == 'Cambiar') {
      fxCambiar();
   } elseif (@$_REQUEST['Id'] == 'Agregar') {
      fxAgregarPaqSesion();
   } else {      
      fxInit();
   }

   function fxCerrarSesion() {      
      session_destroy();
      fxheader("index.php");
   } 

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'];
      $lo->paData['CCODIGO'] = '';
      $lo->paData['CCODIGO'] = $_SESSION['paqDat']['CCODIGO'];
      $llOk = $lo->omVerificarUnidadAcademica();
      if (!$llOk) {
         fxHeader('Mnu0000.php', $lo->pcError);
      }
      $lo = new CFormatos();     
      $lo->paData = ['CNRODNI' =>$_SESSION['GADATA']['CNRODNI']];
      $_SESSION['GADATA'] = $_SESSION['GADATA'] + ['CCODIGO'=>'']; 
      $_SESSION['GADATA']['CCODIGO'] = $_SESSION['paqDat']['CCODIGO']; 
      $llOk = $lo->omGenerarFormato();
      if (!$llOk) {
         fxHeader('Mnu0000.php', $lo->pcError);
      }
      fxScreen();
   }

   function fxCambiar(){
      $lo = new CPersona();      
      $lo->paData = $_SESSION['GADATA'];
      $lo->lcCodAlu = $_REQUEST['pcCodAlu'];
      $llOk = $lo->omCambiarCodigo();      
      if (!$llOk) {
         fxHeader('Mnu0000.php', $lo->pcError);
      }
      $_SESSION['GADATA']['CCODALU'] = $lo->paData['CCODALU'];
      $_SESSION['GADATA']['CUNIACA'] = $lo->paData['CUNIACA'];
      $_SESSION['GADATA']['CNOMUNI'] = $lo->paData['CNOMUNI'];
      fxScreen();
   }
   
   function fxScreen() {
      global $loSmarty;      
      $loSmarty->assign('saData', $_SESSION['GADATA']);
      $loSmarty->display('Plantillas/Mnu2000.tpl');
   }   
    
?>
