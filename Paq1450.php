<?php
   //BANDEJA TRAMITES PENDIENTES
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CTramites.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Confirmar') {
      fxConfirmar();
   } elseif (@$_REQUEST['Boton'] == 'Observar') {
      fxObservar();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $llOk = $lo->omInitBandejaBibliotecaTOturniting();
      if (!$llOk) {
         fxHeader('Mnu1000.php',$lo->pcError);
      } 
      $_SESSION['paDatos'] = $lo->paDatos;
      $laData['CNOMBRE'] = $_SESSION['GADATA']['CNOMBRE'];
      $_SESSION['paData'] = $laData ; 
      fxScreen(0);
   }

   function fxConfirmar(){
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omAprobarConformidadBibliotecaTurniting();
      if (!$llOk) {
         fxHeader('Paq1450.php', $lo->pcError);
      }
      else {
         fxAlert('TRAMITE APROBADO CORRECTAMENTE');
      } 
      fxInit();
   }

   function fxObservar(){
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omObservarConformidadBibliotecaTurniting();
      if (!$llOk) {
         fxHeader('Paq1450.php', $lo->pcError);
      } 
      else {
         fxAlert('TRAMITE OBSERVADO CORRECTAMENTE');
      } 
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1450.tpl');
   }
?>