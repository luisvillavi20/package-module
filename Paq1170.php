<?php
   // Mantenimiento de Usuarios
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CUsuarios.php';
   require_once 'Clases/CBase.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   
   if (!fxSoloAdministrativo()) {  
      return;
   } /*elseif (!fxSoloAdministrador()) { 
      return;  
   } */elseif(@$_REQUEST['Boton'] == 'Reestablecer') {     
      fxReestablecerContraseña();
   } elseif (@$_REQUEST['Boton'] == 'Editar') {
      fxEditarEstadoNivel();
   } elseif (@$_REQUEST['Boton'] == 'Buscar') {
      fxBuscarxCargo();   
   } elseif (@$_REQUEST['Boton'] == 'Detalle') {
      fxDetalleUsuario();
   } elseif (@$_REQUEST['Boton1'] == 'Grabar') {
      fxGrabarPermisosUsuario();
   } elseif (@$_REQUEST['Boton2'] == 'Grabar') {
      fxGrabarNuevoNivelUsuario();
   } elseif (@$_REQUEST['Boton1'] == 'Actualizar') {
      fxActualizarEstadoCentroCosto(); //actualizar estado centro de costo
   } elseif (@$_REQUEST['Boton2'] == 'Actualizar') {
      fxActualizarEstadoNivel(); //actualizar estado nivel
   }/*elseif (@$_REQUEST['Boton'] == 'Actualizar') {
      fxActualizarUsuario();   
   }*/elseif (@$_REQUEST['Boton'] == 'Nuevo') {
      fxCargarDatos();    //new
   } elseif (@$_REQUEST['Boton'] == 'GrabarPersona') {
      fxGrabarPersona();   //new ---
   } elseif (@$_REQUEST['Boton'] == 'Asignar') {
      fxGrabarNivel();   //new
   } elseif (@$_REQUEST['Boton'] == 'GrabarNivelUsuario') {
      fxGrabarNivelUsuario();
   } elseif (@$_GET['Id'] == 'VerificarUsu') {
      fxVerificarUsu();
   } elseif (@$_GET['Id'] == 'Verificar') {
      fxVerificar();
   } else {
      fxInit();
   } 
   
   function fxReestablecerContraseña() {
      $lo = new CUsuarios(); 
      $lo->paData = ['CNRODNI' =>$_REQUEST['pcNroDni']];       
      $llOk = $lo->omReestablecerConstraseña();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      fxHeader('Paq1170.php','CLAVE REESTABLECIDA'); 
      fxInit();
   }   
   
   function fxBuscarxCargo() {   
      $lo = new CUsuarios();
      $lo->paData = ['CNIVEL' => $_REQUEST['pcNivel*']];
      $llOk = $lo->omBuscarxCargoUsuarios();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }  
      $laData = $lo->paData;
      $laData['CNOMBRE'] = $_SESSION['GADATA']['CNOMBRE'];
      $_SESSION['paData'] = $laData;     
      $_SESSION['paDatos']  = $lo->paDatos;
      fxScreen(0);
   }      
   //Inicio de Mantenimiento Usuarios
   function fxInit() {   
      $laData['CCODUSU'] = $_SESSION['GADATA']['CCODUSU'];
      $lo = new CUsuarios();
      $lo->paData = $laData;
      $llOk = $lo->omCargarDatos();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paUniAca'] = $lo->paUniAca;
      $_SESSION['paIdCate'] = $lo->paIdCate;
      $_SESSION['paEstado'] = $lo->paEstado;
      $_SESSION['paCargos'] = $lo->paCargos;
      $_SESSION['paCenCos'] = $lo->paCenCos;             
      fxInitUsuarios();
   }      
   function fxInitUsuarios() {      
      $laData['CCODUSU'] = $_SESSION['GADATA']['CCODUSU'];
      $lo = new CUsuarios();
      $lo->paData = $laData;
      $llOk = $lo->omInitUsuarios();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $laData = $lo->paData;
      $laData['CNOMBRE'] = $_SESSION['GADATA']['CNOMBRE'];
      $_SESSION['paData'] = $laData;       
      $_SESSION['paDatos']  = $lo->paDatos;
      fxScreen(0);
   }     
   
   // Iniciar Detalle para Añadir Permisos
   function fxDetalleUsuario() {       
      if (!isset($_POST['pcCodUsu'])) {
         fxAlert("SELECCIONE A UN USUARIO PARA VER SU DETALLE");
         fxInit();
         return;
      }   
      $lo = new CUsuarios();
      $lo->paData = ['CCODUSU' => $_REQUEST['pcCodUsu'],'CUSUCOD' => $_SESSION['GADATA']['CCODUSU']]; 
      $llOk = $lo->omDetalleUsuarioN();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }   
      $_SESSION['paData']  = $lo->paData;
      $_SESSION['paDatos']  = $lo->paDatos;
      fxScreen(1);
   } 
   
   function fxGrabarPermisosUsuario() {     
      $lo = new CUsuarios();
      $lo->paData = ['CCODUSU' => $_REQUEST['pcCodUsu'], 'CCENCOS' => $_REQUEST['pcCenCos'], 'CUSUCOD' => $_SESSION['GADATA']['CCODUSU']];    
      $llOk = $lo->omAsignarCentroCosto();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }
      $llOk = $lo->omDetalleUsuarioN();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }
      $_SESSION['paDatos']  = $lo->paDatos;
      fxScreen(1);
   } 

   function fxGrabarNuevoNivelUsuario() {     
      $lo = new CUsuarios();
      $lo->paData = ['CCODUSU' => $_REQUEST['pcCodUsu'], 'CUNIACA' => $_REQUEST['pcUniAca'], 'CNIVEL' => $_REQUEST['pcNivel'], 'CUSUCOD' => $_SESSION['GADATA']['CCODUSU']];    
      $llOk = $lo->omAsignarPermisosUsuario();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }
      $llOk = $lo->omDetalleUsuarioN();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }
      $_SESSION['paDatos']  = $lo->paDatos;
      fxScreen(1);
   }

   function fxActualizarEstadoCentroCosto() {    //aqui
      $lo = new CUsuarios();
      $lo->paData = ['CCODUSU' =>$_REQUEST['pcCodUsu'], 'CUSUCOD' => $_SESSION['GADATA']['CCODUSU']];
      //$lo->paData = ['NSERIAL' =>$_REQUEST['pnNserial'],'CUSUCOD' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paEstado = $_REQUEST['pcEstadoCenCos'];    
      $llOk = $lo->omActualizarCentroCostoUsuario();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }  
      $llOk = $lo->omDetalleUsuarioN();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }
      $_SESSION['paDatos']  = $lo->paDatos;
      fxScreen(1);      
   }  
   // Actualizar el Detalle del Usuario
   function fxActualizarEstadoNivel() {    //aqui  
      $lo = new CUsuarios();
      $lo->paData = ['CCODUSU' =>$_REQUEST['pcCodUsu'],'CUSUCOD' => $_SESSION['GADATA']['CCODUSU']];
      //$lo->paData = ['NSERIAL' =>$_REQUEST['pnNserial'],'CUSUCOD' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paEstado = $_REQUEST['pcEstado'];  
      $llOk = $lo->omActualizarPermisoUsuario();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }  
      $llOk = $lo->omDetalleUsuarioN();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }
      $_SESSION['paDatos']  = $lo->paDatos;
      fxScreen(1);      
   }   

    // Iniciar para Editar Cabecera del Usuario
   function fxEditarEstadoNivel() {     
      if (!isset($_POST['pnNserial'])) {
         fxInit();
         return;
      }   
      $lo = new CUsuarios();
      $lo->paData = ['NSERIAL' => $_REQUEST['pnNserial']]; 
      $llOk = $lo->omEditarEstadoUsuario();
      if (!$llOk) {
         fxAlert($lo->pcError);         
      }   
      $laData = $lo->paData;      
      $_SESSION['paData'] = $laData;       
      $_SESSION['paDatos']  = $lo->paDatos;
      fxScreen(2);
   } 

   function fxCargarDatos() {     
      $laData['CCODUSU'] = $_SESSION['GADATA']['CCODUSU'];
      $lo = new CUsuarios();
      $lo->paData = $laData;
      $llOk = $lo->omCargarDatos();  //omInsertarNuevaPersona
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paUniAca'] = $lo->paUniAca;
      $_SESSION['paIdCate'] = $lo->paIdCate;
      $_SESSION['paEstado'] = $lo->paEstado;
      $_SESSION['paCargos'] = $lo->paCargos;
      $_SESSION['paCenCos'] = $lo->paCenCos;             
      $_SESSION['paSexo'] = $lo->paSexo; 
      fxScreen(3);
   } 

   function fxGrabarPersona() {
      $lo = new CUsuarios();
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $lo->paData = $laData;
      $lcApePat = strtoupper($laData['CAPEPAT']);
      $lcApeMat = strtoupper($laData['CAPEMAT']);
      $lcNombre = strtoupper($laData['CNOMUSU']);
      $lo->paData = $laData+['CNOMNUE' => $lcApePat.'/'.$lcApeMat.'/'.$lcNombre];
      $llOk = $lo->omGrabarNuevaPersona();
      if (!$llOk) {
         fxHeader('Paq1170.php', $lo->pcError);
      }
      fxInit();
   }

   function fxGrabarNivel() {
      $laData['CUSUCOD'] = $_SESSION['GADATA']['CCODUSU'];
      $lo = new CUsuarios();
      $lo->paData = $laData;
      $llOk = $lo->omCargarDatos();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paUniAca'] = $lo->paUniAca;
      $_SESSION['paIdCate'] = $lo->paIdCate;
      $_SESSION['paEstado'] = $lo->paEstado;
      $_SESSION['paCargos'] = $lo->paCargos;
      $_SESSION['paCenCos'] = $lo->paCenCos;             
      fxScreen(4);
   } 

   function fxGrabarNivelUsuario() {
      $lo = new CUsuarios();
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $lo->paData = $laData;
      $llOk = $lo->omGrabarNuevoNivelUsuario();
      if (!$llOk) {
         fxHeader('Paq1170.php', $lo->pcError);
      }
      fxInit();
   }

   function fxVerificar() {
      $lo = new CUsuarios();
      $lo->paData = ['CNRODNI' => $_REQUEST['CNRODNI']] + $_SESSION['GADATA'];
      $llOk = $lo->omVerificarPersonaxDni();
      if (!$llOk) {
         echo json_encode(["ERROR" => $lo->pcError]);
      } else {
         echo json_encode($lo->paData);
      }
   }
   function fxVerificarUsu() {
      $lo = new CUsuarios();
      $lo->paData = ['CNRODNI' => $_REQUEST['CNRODNI']] + $_SESSION['GADATA'];
      $llOk = $lo->omVerificarUsuarioxDni();
      if (!$llOk) {
         echo json_encode(["ERROR" => $lo->pcError]);
      } else {
         echo json_encode($lo->paData);
      }
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('saEstado', $_SESSION['paEstado']);
      $loSmarty->assign('saCargos', $_SESSION['paCargos']);
      $loSmarty->assign('saUniAca', $_SESSION['paUniAca']);      
      $loSmarty->assign('saIdCate', $_SESSION['paIdCate']);  
      $loSmarty->assign('saCencos', $_SESSION['paCenCos']);
      $loSmarty->assign('saSexo',  $_SESSION['paSexo']);  
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1170.tpl');
   }
?>