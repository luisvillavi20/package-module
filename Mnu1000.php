<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CLogin.php';
   require_once 'Clases/CTramites.php';
   require_once 'Clases/CUsuarios.php';   
   session_start();
   $loSmarty = new Smarty;
   if (@$_REQUEST['Id']=='ERP') {
		fxInicioSesionERP();
	}
   if (!fxSoloAdministrativo()) { 
   	return;  
	} elseif (@$_REQUEST['Boton'] == 'Cerrar Sesion') {
		fxCerrarSesion();    
	} elseif (@$_REQUEST['Id']=='Agregar') {
		fxAgregarPaqSesion();
	} elseif (@$_REQUEST['OPTION']=='OPTION') {
		fxCambiarNivel();
	} else {
		fxInit();
	}

    function fxCambiarNivel() {    
        $_SESSION['GADATA']['CNIVEL'] = $_REQUEST['paData']['CNIVEL'];
        fxScreen();
    }

    function fxInicioSesionERP() {    
        $lo = new CLogin();  
        $lo->paData = $_SESSION['GADATA'];  
        $llOk = $lo->omIniciarSesionERP();     
	    if (!$llOk) {      
		   //fxHeader('index1.php', $lo->pcError);         
		   return;
	    } 
	    $_SESSION['GADATA'] = $_SESSION['GADATA'] + $lo->paData;
	    $_SESSION['GAOPTION'] = $lo->paDatos;
    }

   function fxCerrarSesion() {      
      session_destroy();
      fxheader("index1.php");  
   }
	function fxInit() { 
		//TESORERIA  -  ADMINSTRADOR - ACADEMICO - CONTABILIDAD (no TIENEN UNIDADES ACADEMICAS)
		if ($_SESSION['GADATA']['CNIVEL'] == '6' || $_SESSION['GADATA']['CNIVEL'] == '0' || $_SESSION['GADATA']['CNIVEL'] == '7' || $_SESSION['GADATA']['CNIVEL'] == '8'){
			fxScreen();
		}
		else {
			$lo = new CTramites();  
			$lo->paData = $_SESSION['GADATA'];  
			$llOk = $lo->omInitTramites();     
			if (!$llOk) {
				session_destroy();        
				fxHeader('index1.php', $lo->pcError);         
				return;
			}  
			$laData = $_SESSION['GADATA'];      
			$laData['CUNIACA'] = $lo->paData['CUNIACA'];            
			$laData['CNOMUNI'] = $lo->paData['CNOMUNI'];   
			$laData['NNROOBS'] = $lo->paData['NNROOBS'];
            $laData['NNROPEN'] = $lo->paData['NNROPEN'];
            $laData['NNROPAQ'] = $lo->paData['NNROPAQ'];
			$laData['NNROACT'] = $lo->paData['NNROACT'];
			$laData['NNROPAT'] = $lo->paData['NNROPAT'];
			$_SESSION['GADATA'] = $laData;    
			fxScreen();
		}     
	}
	
	function fxAgregarPaqSesion(){
		$lcModPaq = $_REQUEST['p_cCodigo'];
		$_SESSION['paqDat'] = ['CCODIGO' => $lcModPaq];
	}

	function fxScreen() {
		global $loSmarty;        
		$loSmarty->assign('saData', $_SESSION['GADATA']);
		$loSmarty->assign('saqDat', $_SESSION['paqDat']);
		$loSmarty->assign('saOption', $_SESSION['GAOPTION']);
		$loSmarty->display('Plantillas/Mnu1000.tpl');
	}  
?>