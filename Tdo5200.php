<?php
   //phpinfo();
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CDeudas.php';
   //header('Location: http://apps.ucsm.edu.pe/UCSMMTA/');
   //error_reporting(E_ALL);ini_set('display_errors', 1);
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'Aceptar') {
      fxConsultarUsuario();
   } else {
      fxInit();
   }

   function fxInit(){
      //fxHeader('index.php');
      fxScreen(0);
   }

   function fxConsultarUsuario(){
      $lo = new CDeudas();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omConsultarUsuario();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
         return;
      } 
      $_SESSION['GADATA'] = $lo->paData;
      if($_SESSION['GADATA']['CESTADO'] == 'C'){
         fxScreen(2);
      } else {
         fxScreen(1);
      }
   }

   function fxScreen($p_nBehavior){
      global $loSmarty;     
      $loSmarty->assign('saData'  , $_SESSION['GADATA']);
      $loSmarty->assign('saDeudas'  , $_SESSION['paDeudas']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Tdo5200.tpl');
   }
?>
