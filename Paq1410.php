<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   }
   elseif (@$_REQUEST['Boton'] == 'Observar') {
      fxLevObservarSol();
   } else {
      fxInit();
   }   

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omInitBandejaClinicaObser();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxLevObservarSol() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omBandejaClinicaLevObser();
      if (!$llOk) {
         fxHeader('Paq1410.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1410.tpl');
   }
?>