<?php
   // -------------------------------------------------------
   // Permite modificar el creditaje de matricula del alumno
   // 2019-02-14 LVA Creacion
   // -------------------------------------------------------
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   require_once 'Clases/CDeudas.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Actualizar') {
      fxActualizar();
   } elseif (@$_REQUEST['Boton'] == 'Anular') {
      fxAnular();
   } elseif (@$_REQUEST['Boton'] == 'Nuevo') {
      fxScreen(0);
   } else {
      fxInit();
   }
   
   function fxInit() {   
      $laData  = $_SESSION['GADATA'];
      $lo = new CMatricula();
      $lo->paData = ['CCODALU' => $_SESSION['GADATA']['CCODALU']];
      $llOk = $lo->omInitMatriculaxCreditos();
      $_SESSION['paDatos'] = [];
      if (!$llOk) {
         fxHeader('Mnu0000.php', $lo->pcError);
      } //[P: Pendiente, U: Utilizado, X: Anulado] 
      
      $_SESSION['paData']  = $lo->paData + $_SESSION['GADATA']; //RECUPERA EL COSTO DEL CREDITO
      if ($lo->paData['NSERIAL'] == -1) {
         fxScreen(0); 
      } elseif ($lo->paData['CESTADO'] == 'P') {
         // Cambio de creditaje pendiente de pago
         fxScreen(1); 
      } elseif ($lo->paData['CESTADO'] == 'X') {
         // Cambio de creditaje anulado
         fxScreen(0);
      } /*elseif ($lo->paData['CESTADO'] == 'U' AND $lo->paData['CESTINF'] == 'A' ) {
         // Cambio de creditaje usado (alumno ya pago)
         fxDetalle();
         //fxScreen(2);
      } elseif ($lo->paData['CESTADO'] == 'U' AND $lo->paData['CESTINF'] == 'M' ) {
         // Cambio de creditaje usado (alumno ya pago)
         echo 'lplplp22';
         fxDetalle();
         //fxScreen(3);
         //fxScreen(2);
      }*/ elseif ($lo->paData['CESTADO'] == 'U') {
         // Cambio de creditaje usado (alumno ya pago)
         fxDetalle();
         //fxScreen(2);
      }
   }

   function fxDetalle() {
      $lo = new CMatricula();
      $lo->paData = $_SESSION['GADATA'];
      //print_r($_SESSION['GADATA']); 
      $llOk = $lo->omDetalleDeuda();
      if (!$llOk) {
         fxHeader('Tdo5010.php', $lo->pcError); 
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      //print_r($_SESSION['paDatos']);
      fxScreen(2);
      
   }

   function fxGrabar() {
      $loD = new CDeudas(); 
      $loD->paData = $_SESSION['paData'];
      $llOk = $loD->omComprobarSuspensiones();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxScreen(0);
         return;
      }
      $llOk = $loD->omRevisarDeudasAlumnoMatriculaPorCredito();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxScreen(0);
         return;
      }
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + ['CCODALU' => $_SESSION['GADATA']['CCODALU']];
      $llOk = $lo->omRegistrarMatricula();
      if (!$llOk) {
         fxHeader('Tdo5010.php', $lo->pcError); 
      }
      fxHeader('Tdo5010.php', 'GRABACION DE PENSIÓN POR CRÉDITOS CONFORME'); // 'CREDITOS REGISTRADOS'
   }
    
   function fxActualizar() {
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omActualizarMatricula();
      if (!$llOk) {
         fxHeader('Tdo5010.php', $lo->pcError);
      }
      fxHeader('Tdo5010.php', 'ACTUALIZACIÓN DE PENSIÓN POR CRÉDITOS CONFORME'); // 'CREDITOS REGISTRADOS'
   }
   
   function fxAnular() {
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omAnularMatricula();
      if (!$llOk) {
         fxHeader('Tdo5010.php', $lo->pcError);
      }
      fxHeader('Mnu0000.php', 'ANULACIÓN DE PENSIÓN POR CRÉDITOS CONFORME'); // 'CREDITOS REGISTRADOS'
   }
   
   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5010.tpl');
      return;
   }
?>