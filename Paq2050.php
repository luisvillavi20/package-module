<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_GET['Id'] == 'CargarColacion') {
      fxCargarColacion();
   } elseif (@$_REQUEST['Boton'] == 'VerificarRevisores') {
      fxVerificarRevisores();
   } elseif (@$_REQUEST['Boton'] == 'AsignarRevisor') {
      fxAsignarRevisor();
   } elseif (@$_REQUEST['Boton'] == 'VerificarGrupo') {
      fxVerificarGrupo();
   } elseif (@$_REQUEST['Boton'] == 'AsignarAlumno') {
      fxAsignarAlumno();
   } else {
      fxInit();
	}

	function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omRecuperarTipoDeColaciones();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paArrays'] = [];
      $_SESSION['paData'] = $_SESSION['GADATA'] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $_SESSION['paDatos'] = null;
      $_SESSION['paTipCol'] = $lo->paTipCol;
      $_SESSION['paEscuel'] = $lo->paEscuel;
      $_SESSION['paEspMec'] = $lo->paEspMec;
      fxScreen(0);
	}

   function fxVerificarRevisores() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omVerificarRevisores();
      if (!$llOk) {
         fxHeader('Paq2050.php', $lo->pcError);
         return;            
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $llOk = $lo->omCargarArrayDocentes();
      if (!$llOk) {
         fxHeader('Paq2050.php', $lo->pcError);
      }
      $lcUniAca = (in_array($_REQUEST['paData']['CUNIACA'], ['4E','4A']) && $_REQUEST['paData']['CTIPCOL'] == 'B')? $_REQUEST['paData']['CUNIESP'] : $_REQUEST['paData']['CUNIACA'];
      foreach($_SESSION['paTipCol'] as $laTmp) {
         if ($laTmp['CCODIGO'] == $_REQUEST['paData']['CTIPCOL']) $lcDesCol = $laTmp['CDESCRI'];
      }
      foreach($_SESSION['paEscuel'] as $laTmp) {
         if ($laTmp['CUNIACA'] == $lcUniAca) $lcNomUni = $laTmp['CNOMUNI'];
      }
      $_SESSION['paArrays'] = $lo->paArrays;
      $_SESSION['paData'] = ['CNOMBRE' => $_SESSION['GADATA']['CNOMBRE'], 'CIDCOLA' => $_REQUEST['paData']['CIDCOLA'],
                             'CTIPCOL' => $_REQUEST['paData']['CTIPCOL'], 'CDESCOL' => $lcDesCol, 'CUNIACA' => $lcUniAca, 'CNOMUNI' => $lcNomUni];
      fxScreen(2);
   }

   function fxAsignarRevisor() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAsignarRevisor();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paData'] = array_merge($_SESSION['paData'], $lo->paData);
      $_SESSION['paDatos'] = $lo->paDatos;
      fxVerificarRevisores();
   }

   function fxVerificarGrupo() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omCargarAlumnosParaRevision(); // TEMPORALMENTE S CAMBIO A B
      if (!$llOk) {
         fxHeader('Paq2050.php', $lo->pcError);
      }
      $lcUniAca = (in_array($_REQUEST['paData']['CUNIACA'], ['4E','4A']) && $_REQUEST['paData']['CTIPCOL'] == 'B')? $_REQUEST['paData']['CUNIESP'] : $_REQUEST['paData']['CUNIACA'];
      foreach($_SESSION['paTipCol'] as $laTmp) {
         if ($laTmp['CCODIGO'] == $_REQUEST['paData']['CTIPCOL']) $lcDesCol = $laTmp['CDESCRI'];
      }
      foreach($_SESSION['paEscuel'] as $laTmp) {
         if ($laTmp['CUNIACA'] == $lcUniAca) $lcNomUni = $laTmp['CNOMUNI'];
      }
      $_SESSION['paData'] = ['CNOMBRE' => $_SESSION['GADATA']['CNOMBRE'], 'CIDCOLA' => $_REQUEST['paData']['CIDCOLA'],
                             'CTIPCOL' => $_REQUEST['paData']['CTIPCOL'], 'CDESCOL' => $lcDesCol, 'CUNIACA' => $lcUniAca, 'CNOMUNI' => $lcNomUni];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(3);
   }

   function fxAsignarAlumno() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAsignarAlumno();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxHeader('Paq2050.php', $lo->pcError);
         return;            
      }
      $_SESSION['paData'] = $lo->paData;
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxCargarColacion() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omRecuperarUltimaColacion();
      if (!$llOk) {
         echo '{"ERROR":"'.$lo->pcError.'"}';
      } else {
         echo json_encode($lo->paData);
      }
   }

	function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('saTipCol', $_SESSION['paTipCol']);
      $loSmarty->assign('saEscuel', $_SESSION['paEscuel']);
      $loSmarty->assign('saEspMec', $_SESSION['paEspMec']);
      $loSmarty->assign('saArrays',   $_SESSION['paArrays']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq2050.tpl');
   }
?>
