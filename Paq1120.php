<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CWebService.php';
   session_start();
   $loSmarty = new Smarty;
   if (@$_REQUEST['Boton'] == 'Verificar') {
      fxVerificar();
   } else if (@$_GET['Id'] == 'BuscarPaquete') {
      fxBuscarPaquete();
   } else {
      fxInit();
   }
   function fxVerificar() {
      if (!isset($_FILES['fileToUpload'])) {
         fxAlert('ARCHIVO NO SUBIDO');
         fxInit();
         return;
      }
      $lcFolder = "EXP/VERIFY/";
      $lcFileName = mxRandomString(6).".pdf";
      $lcFilePath = $lcFolder.$lcFileName;
      if (!move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $lcFilePath)) {
         fxAlert("NO SE PUDO SUBIR EL ARCHIVO");
         fxInit();
         return;
      }
      chmod($lcFilePath, 0666);
      $lo = new CWebService();
      $lo->paData = ['CFILENAME' => $lcFileName];
      $llOk = $lo->omVerificarDocumento();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
         return;
      }
      if (!unlink($lcFilePath)) {
         fxAlert("-");
      }
      $_SESSION['paData'] = $lo->paData;
      fxScreen(1);
   }
   function fxInit() {
      fxScreen(0);
   }
   function fxScreen($p_nFlag) {
      global $loSmarty;
      if (isset($_SESSION['paData'])) {
         $loSmarty->assign('saData',   $_SESSION['paData']);
      }
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1120.tpl');
   }
   function mxRandomString($length) {
      $key = '';
      $keys = array_merge(range(0, 9), range('a', 'z'));
  
      for ($i = 0; $i < $length; $i++) {
          $key .= $keys[array_rand($keys)];
      }
  
      return $key;
  }
?>