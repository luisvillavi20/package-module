<?php
   //BANDEJA TRAMITES PENDIENTES
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CTramites.php';
   require_once 'Clases/CDocumentos.php';
   require_once 'Clases/CDeudas.php';
   require_once 'Styles/phpqrcode/qrlib.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) { 
      return;  
   }
   elseif (@$_REQUEST['Boton'] == 'Enviar') {
      fxEnviar();
   } 
   else {
      fxInit();
   }
   
   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $lo->paData['CESTADO'] = 'F';
      $llOk = $lo->omInitLlenadoFormulario();
      if (!$llOk) {
         fxHeader('Mnu2000.php', $lo->pcError);
         $lo->pcError;
      }
      $laData = $lo->paData;
      if (isset($lo->paDatos['CCESTU'])) {
         $laData['NCANSEM'] = $lo->paDatos['CCESTU']['NCANSEM'];
      }
      $_SESSION['paData']  = $laData;
      $_SESSION['paDatos'] = $lo->paDatos;
      if (isset($lo->paCurso)) {
         $_SESSION['paCurso'] = $lo->paCurso;  
      }
      fxScreen(0);
   }

   function fxEnviar() {
      $laData = $_SESSION['GADATA'];
      $laData['CCODUSU'] = $_SESSION['GCCODUSU'];
      $loD = new CDeudas(); 
      $loD->paData = $_SESSION['paData'];
      /*$llOk = $loD->omComprobarDeudas();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxScreen(0);
         return;
      }*/
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omGuardarDocumento();
      if (!$llOk) {
         fxHeader('Paq1030.php', $lo->pcError);
         return;
      }
      fxHeader('Mnu2000.php', 'FORMATO COMPLETADO');
   }
   
   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saCurso', $_SESSION['paCurso']);
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1030.tpl');
      return;
   }
?>