<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CConstancias.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) {
      return;
   } elseif (@$_REQUEST['Boton'] == 'Aprueba') {
      fxAprobarCertificado();
   } elseif (@$_REQUEST['Boton'] == 'Observar') {
      fxObsevarCertificado();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CConstancias();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omIniBandejaConstanciasEspecialesEscuela();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxAprobarCertificado() {
      $lo = new CConstancias();
      if (isset($_REQUEST['pnIndice'])) {
         $laData = $_SESSION['paDatos'][$_REQUEST['pnIndice']];
         $_SESSION['paData'] += ['CCODTRE' => $laData['CCODTRE']];
      } else {
         $laData = $_REQUEST['paData'];
         $_SESSION['paData'] += ['CCODTRE' => $_REQUEST['paData']['CCODTRE']];
      }
      $lo->paData = $laData + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omIniDetalleBandejaConstanciasEspecialesEscuela();
      if (!$llOk) {
         fxHeader('Tdo5100.php', $lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      if(isset($laData['CIDCATE']) && $laData['CIDCATE'] == '000020') {
         return fxScreen(1);
      } elseif (isset($laData['CIDCATE']) && $laData['CIDCATE'] == '000001') {
         return fxScreen(2);
      } elseif (isset($laData['CIDCATE']) && $laData['CIDCATE'] == '000018') {
         return fxScreen(3);
      } elseif (isset($laData['CIDCATE']) && $laData['CIDCATE'] == '000061') {
         return fxScreen(4);
      } elseif (isset($laData['CIDCATE']) && !in_array($laData['CIDCATE'], ['000001', '000018', '000020', '000061'])) {
         fxHeader('Tdo5100.php', 'TIPO DE TRAMITE NO DEFINIDO');
      }
      $lo->paFile = $_FILES['poFile'];
      $lo->paData += ['CIDCATE' => $_SESSION['paDatos']['CIDCATE']]+['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAprobarCostanciaEspecial(); 
      if (!$llOk) {
         fxAlert($lo->pcError);
         if ($_SESSION['paDatos']['CIDCATE'] == '000020') {
            return fxScreen(1);   
         } elseif($_SESSION['paDatos']['CIDCATE'] == '000001') {
            return fxScreen(2);
         } elseif ($_SESSION['paDatos']['CIDCATE'] == '000018') {
            return fxScreen(3);
         } elseif($_SESSION['paDatos']['CIDCATE'] == '000061') {
            return fxScreen(4);
         }
      }
      fxHeader('Tdo5100.php', 'CONSTANCIA APROBADA CORRECTAMENTE');
   }

   function fxObsevarCertificado(){
      $lo = new CConstancias();
      $laData = $_REQUEST['paData'];
      $lo->paData = $laData + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omObervarCertificadoDeConducta(); 
      if (!$llOk) {
         fxHeader('Tdo5100.php', $lo->pcError);
      }
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5100.tpl');
   }
?>