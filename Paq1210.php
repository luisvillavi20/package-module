<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;
   // ----------------------------------------------------------
   // Mantenimiento de paquetes de requisitos para bachillerato
   // 2019-03-01LVACreacion
   // ----------------------------------------------------------
   if (!fxSoloAdministrativo()) {
      return;
   } elseif(@$_REQUEST['Boton'] == 'AgregarPaquete') {
      fxAgregarPaquete();
   } elseif(@$_REQUEST['Boton'] == 'InfoPaquete') {
      fxInfoPaquete();
   } elseif(@$_REQUEST['Boton'] == 'EditarPaquete') {
      fxEditarPaquete();
   } elseif(@$_REQUEST['Boton'] == 'EditarDocumento') {
      fxEditarDocumento();
   } elseif(@$_REQUEST['Boton'] == 'AgregarDocumento') {
      fxAgregarDocumento();
   } elseif(@$_REQUEST['Boton'] == 'NuevoPaquete') {
      fxNuevoPaquete();
   } elseif(@$_REQUEST['Boton'] == 'AgregarDocumentoNuevo') {
      fxAgregarDocumentoNuevo();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omRecuperarBandejaMtoPaquetes();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;
      }
      $laData = $lo->paData;      
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] =  $_SESSION['GADATA'];
      fxScreen(0);
   }
   
   function fxAgregarPaquete() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAgregarPaquete();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxInit();
   }

   function fxInfoPaquete() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRecuperarInfoPaquete();
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'];
      fxScreen(1);
   }

   function fxAgregarDocumento() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAgregarDocumento();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxInfoPaquete();
   }

   function fxEditarDocumento() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omEditarDocumento();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxInfoPaquete();
   }

   function fxEditarPaquete() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omEditarPaquete();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxInfoPaquete();
   }

   function fxAgregarDocumentoNuevo() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAgregarDocumentoNuevo();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxInfoPaquete();
   }

   function fxNuevoPaquete() {
      $lo = new CPaquetes();
      $llOk = $lo->omRecuperarUnidadesAcademicas();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'];
      fxScreen(3);
   }

   function fxScreen($p_nBehavior) {
      global $loSmarty;      
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq1210.tpl');
   }
?>