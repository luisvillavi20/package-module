<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Id'] == 'ObtenerActas') {
      fxCargarActas();
   } else {
      fxInit();
	}

	function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omCargarListaColaciones();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paArrays'] = [];
      $_SESSION['paData'] = $_SESSION['GADATA'] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
      $_SESSION['paDatos'] = null;
      $_SESSION['paHisCol'] = $lo->paHisCol;
      $_SESSION['paEscuel'] = $lo->paEscuel;
      fxScreen(0);
	}

      function fxCargarActas() {
         $lo = new CPaquetes();
         $lo->paData = $_REQUEST['paData'] + ['CCODIGO' => $_SESSION['paqDat']['CCODIGO']];
         $llOk = $lo->omRecuperarGruposColacionXUni();
         if (!$llOk) {
            echo json_encode($lo->paDatos);
            return;
         }
         $_SESSION['paGrpCol'] = $lo->paDatos;
         AxPrintCursos();
      }
	function fxScreen($p_nFlag) {
         global $loSmarty;  
         $loSmarty->assign('saData', $_SESSION['paData']);
         $loSmarty->assign('saDatos', $_SESSION['paDatos']);
         $loSmarty->assign('saHisCol', $_SESSION['paHisCol']);
         $loSmarty->assign('saEscuel', $_SESSION['paEscuel']);
         $loSmarty->assign('saArrays',   $_SESSION['paArrays']);
         $loSmarty->assign('snBehavior', $p_nFlag);
         $loSmarty->display('Plantillas/Paq2610.tpl');
      }

      function AxPrintCursos() {
         global $loSmarty;
         $loSmarty->assign('saGrpCol',  $_SESSION['paGrpCol']);
         $loSmarty->display('Plantillas/Paq2611.tpl');
      }
?>
