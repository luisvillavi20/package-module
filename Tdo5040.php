   <?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   require_once 'Clases/CEmail.php';
   require_once 'Clases/CDeudas.php';
   session_start();
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) {
      return;
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Seguimiento') {
      fxSeguimiento();
   } elseif (@$_REQUEST['Boton'] == 'Historial') {
      fxDetalleHistorial();
   } else {
      fxInit();
   }

   function fxInit() {   
      $lo = new CMatricula();
      $lo->paData = ['CCODALU' => $_SESSION['GADATA']['CCODALU']];
      $llOk = $lo->omLimitadorDeEscuela();
      if (!$llOk) {
         fxHeader('Mnu0000.php', $lo->pcError);
      }
      $lo = new CMatricula();
      $lo->paData = ['CCODALU' => $_SESSION['GADATA']['CCODALU'], 'CUSUCOD' => '9999'];
      $llOk = $lo->omInitCursosPorJurado();
      if (!$llOk) {
         fxHeader('Mnu0000.php', $lo->pcError);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'] + $lo->paData;
      fxScreen(0);
   }

   function fxGrabar() {
      $laData = $_SESSION['GADATA'];
      $laData['CCODUSU'] = $_SESSION['GCCODUSU'];
      $loD = new CDeudas(); 
      $loD->paData = $_SESSION['paData'];
      $llOk = $loD->omComprobarSuspensiones();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxScreen(0);
         return;
      }
      $llOk = $loD->omComprobarDeudas();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxScreen(0);
         return;
      }
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + ['CCODALU' => $_SESSION['GADATA']['CCODALU'], 'CCODUSU' => 'U666'];
      $llOk = $lo->omGrabarCursoPorJurado();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $llOk = $lo->omActualizarDatosAlumno();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxInit();
   }

   function fxSeguimiento(){
      $lo = new CMatricula();
      $lo->paData = $_REQUEST['paData'] + ['CCODALU' => $_SESSION['GADATA']['CCODALU'], 'CCODUSU' => 'U666'];
      $llOk = $lo->omSeguimientoCursoPorJurado();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxDetalleHistorial(){
      $lo = new CMatricula();
      $lo->paData = ['CCODALU' => $_SESSION['GADATA']['CCODALU']];
      $llOk = $lo->omHistorialDeCPJ();
      if(!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(3);
   }
   
   function fxScreen($p_nBehavior) {
      global $loSmarty;
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Tdo5040.tpl');
   }
?>