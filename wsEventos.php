<?php
//GENERAR DOCUMENTO DE EVENTO
//MMH 2018-08-02
require_once 'Clases/CEventos.php';
session_start();
if (@$_REQUEST['Id'] == 'RegistrarParticipanteTramites') {
   fxAxRegistrarParticipanteTramites();
} elseif (@$_REQUEST['Id'] == 'RegistrarParticipanteEventosEspeciales') {
   fxAxRegistrarParticipanteEventosEspeciales();
} elseif (@$_REQUEST['Id'] == 'BuscarDatosParticipante') {
   fxAxBuscarDatosParticipante();
} elseif (@$_REQUEST['Id'] == 'RegistrarParticipanteAdmision') {
   fxAxRegistrarParticipanteAdmision();
} else {
   echo json_encode(['ERROR'=>'OPCION INVALIDA']);
}

function fxAxRegistrarParticipanteTramites(){
   $lo = new CEventos();
   $lo->paData = $_REQUEST['paData'];
   $llOk = $lo->omRegistrarParticipanteTramites();
   if (!$llOk) {
      echo json_encode(['ERROR'=>$lo->pcError]);
   } else{
      echo json_encode($lo->paDatos);
   }
}

function fxAxRegistrarParticipanteEventosEspeciales(){
   $lo = new CEventos();
   $lo->paData = $_REQUEST['paData'];
   $llOk = $lo->omRegistrarParticipanteEventosEspeciales();
   if (!$llOk) {
      echo json_encode(['ERROR'=>$lo->pcError]);
   } else{
      echo json_encode($lo->paDatos);
   }
}

function fxAxBuscarDatosParticipante(){
   $lo = new CEventos();
   $lo->paData = $_REQUEST['paData'];
   $llOk = $lo->omBuscarDatosParticipante();
   if (!$llOk) {
      echo json_encode(['ERROR'=>$lo->pcError]);
   } else{
      $_SESSION['pcNuevo'] = ($lo->paData['CNRODNI'] == '')? 'S' : 'N';
      echo json_encode($lo->paData);
   }
}

function fxAxRegistrarParticipanteAdmision(){
   $lo = new CEventos();
   $lo->paData = $_REQUEST['paData'];
   $llOk = $lo->omRegistrarParticipanteAdmision();
   if (!$llOk) {
      echo json_encode(['ERROR'=>$lo->pcError]);
   } else{
      echo json_encode($lo->paDatos);
   }
}
?>