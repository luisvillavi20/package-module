<?php
   //BANDEJA TRAMITES PENDIENTES
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CDeudas.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) { 
      return;  
   }
   elseif (@$_REQUEST['Boton'] == 'Aplicar') {
      fxAplicarPaquete();
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_GET['Id']=='BuscarDNI'){
      fxBuscarDNI();
   } else {
      fxInit();
	}

	function fxInit() {
      $lcCodAlu = $_SESSION['GADATA']['CCODALU'];
      $laData = ['CCODALU' => $lcCodAlu, 'CCODUSU' => 'U666'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']]+ ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];   // $_SESSION['GCCODUSU']
      $lo = new CPaquetes();
      $lo->paData = $laData;
      $llOk = $lo->omInitPaquetes();      
      if (!$llOk) {
         fxHeader('Mnu2000.php', $lo->pcError);
      }
      //$_SESSION['paData'] = $_SESSION['GADATA'];     
      $_SESSION['paData'] = $lo->paData + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }
   
   function fxAplicarPaquete() {
      $laData = $_REQUEST['paData'] + ['CCODUSU' => 'U666'] + $_SESSION['GADATA'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];   // $_SESSION['GCCODUSU']
      $lo = new CPaquetes();
      $lo->paData = $laData;
      $llOk = $lo->omDetallePaquete();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxInit();
         return;
      }
      $_SESSION['paData'] = $lo->paData + $_SESSION['GADATA'] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']]+ ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxGrabar() {
      $loD = new CDeudas(); 
      $loD->paData = $_SESSION['paData'];
      /*$llOk = $loD->omComprobarSuspensiones();
      if (!$llOk) {
         fxHeader('Mnu2000.php', $loD->pcError);
         return;
      }
      $llOk = $loD->omComprobarDeudas();
      if (!$llOk) {
         fxHeader('Mnu2000.php', $loD->pcError);
         return;
      }*/
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $llOk = $lo->omActualizarDatosAlumno();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => 'U666'] + ['CUNIACA' => $_SESSION['GADATA']['CUNIACA']];
      if (!isset($lo->paData['CCODINF'])){
         $lo->paData['CCODINF'] = 'N';
      }
      if (!isset($lo->paData['CCODIDM'])){
         $lo->paData['CCODIDM'] = 'N';
      }
      $llOk = $lo->omGrabarPaquete();
      if (!$llOk) {
         fxHeader('Paq1010.php', $lo->pcError);
         return;
      }
      $lcNroPag = $lo->paData['CNROPAG'];
      $lcNroPag = substr($lcNroPag, 0, 1).' '.substr($lcNroPag, 1, 3).' '.substr($lcNroPag, 4, 3).' '.substr($lcNroPag, 7, 3);
      $lo->paData['CNROPAG'] = $lcNroPag;
      $_SESSION['paData'] = $lo->paData;
      fxScreen(2);
   }

   function fxBuscarDNI(){
      $lcNroDNI = $_GET['DNI'];
      $lcNomUni = $_GET['CNOMUNI'];
      $lo = new CPaquetes();
      $lo->paData =  [ 'CNOMUNI'=> $lcNomUni, 'CNRODNI' => $lcNroDNI, 'CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAlumnoITLBusqueda();
      if (!$llOk) {
         echo($lo->pcError); 
         return;
      }
   }
	function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1010.tpl');
   }
?>