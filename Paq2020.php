<?php
   error_reporting(E_ALL);
   ini_set('display_errors',1);
   require_once 'Libs/Smarty.class.php';   
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'Editar') {
      fxEditar();
   } elseif (@$_REQUEST['Boton'] == 'Actualizar') {
      fxActualizar();
   } elseif(@$_REQUEST['Boton'] == 'PaginaSiguiente') {
      fxPaginaSiguiente();
   } elseif(@$_REQUEST['Boton'] == 'PaginaAnterior') {
      fxPaginaAnterior();
   } elseif(@$_REQUEST['Boton'] == 'Buscar') {
      fxBuscar();
   } else {
      fxInit();
   }

   function fxInit() {         
      $lo = new CPaquetes();
      $lo->paData = ['NPAGINA' => '1'];
      $llOk = $lo->omCargarUniacaCencos();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;            
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'] + $lo->paData;
      fxScreen(0);
   }

   function fxPaginaSiguiente() {
      $lo = new CPaquetes();
      $lnPagina = intval($_REQUEST['paData']['NPAGINA']);
      $lo->paData = ['NPAGINA' => $lnPagina + 1];
      $llOk = $lo->omCargarUniacaCencos();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'] + $lo->paData;
      fxScreen(0);
   }

   function fxPaginaAnterior() {
      $lo = new CPaquetes();
      $lnPagina = intval($_REQUEST['paData']['NPAGINA']);
      $lo->paData = ['NPAGINA' => $lnPagina - 1];
      $llOk = $lo->omCargarUniacaCencos();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'] + $lo->paData;
      fxScreen(0);
   }

   function fxBuscar() {
      $lo = new CPaquetes();
      $lo->paData = ['CBUSQUE' => $_REQUEST['paData']['CBUSQUE'], 'NPAGINA' => 1];
      $llOk = $lo->omCargarUniacaCencos();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'] + $lo->paData;
      fxScreen(0);
   }

   function fxEditar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omRecuperarUnidadesAcademicas();
      if (!$llOk) {
         fxHeader('Paq2020.php', $lo->pcError);
         return;   
      }
      $llOk = $lo->omRecuperarInformacionCentroCosto();
      if (!$llOk) {
         fxHeader('Paq2020.php', $lo->pcError);
         return;   
      }
      $_SESSION['paData'] = $lo->paData + ['CNOMBRE' => $_SESSION['GADATA']['CNOMBRE']];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxActualizar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omActualizarCencos();
      if (!$llOk) {
         fxHeader('Paq2020.php', $lo->pcError);
         return;
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxScreen($p_nBehavior) {
      global $loSmarty;      
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      //$loSmarty->assign('saArrays',   $_SESSION['paArrays']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq2020.tpl');
   }
?>