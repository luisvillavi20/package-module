<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'DetalleAlumno') {
      fxDetalleAlumno();
   } elseif (@$_REQUEST['Boton'] == 'Activar') {
      fxActivar();
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_GET['Id'] == 'Verificar') {
      fxVerificar();
   } else {
      fxInit();
   }   
   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      //$llOk = $lo->omInitBandejaAprobadosDigitalizacionImagen();
      $llOk = $lo->omInitBandejaAprobadosDigitalizacionImagen();
      if (!$llOk) {
         fxHeader('Mnu1000.php', 'SIN PAQUETES PENDIENTES');
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }
   function fxActivar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omEditarTransaccion();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
         return;
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      if ($lo->paDatos['CIDCATE'] != 'CCCOND') {
         $llOk = $lo->omDetalleRevisionDocumentos();
         if (!$llOk) {
            fxAlert($lo->pcError);
            fxScreen(1);
            return;
         }
         $_SESSION['paDatos'] = $lo->paDatos;
         fxScreen(2);
      }
      else {
         $_SESSION['paDatos'] = $lo->paDatos;
         fxScreen(3);
      }      
   }

   function fxVerificar() {
      $lo = new CPaquetes();
      $lo->paData = ['CNRODNI' => $_REQUEST['CNRODNI']] + $_SESSION['GADATA'];
      $llOk = $lo->omBuscarPersonaxDni();
      if (!$llOk) {
         echo json_encode(["ERROR" => $lo->pcError]);
      } else {
         echo json_encode($lo->paData);
      }
   }


   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1240.tpl');
   }
?>