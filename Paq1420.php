<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   }
     elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Llenar') {
      fxLlenar();
   } elseif (@$_REQUEST['Boton'] == 'Observar') {
      fxObservar();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];  
      $llOk = $lo->omInitBandejaConsInter();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'] +
                           ['CNOMALU' => $lo->paDatos[0]['CNOMBRE']] +
                           ['CCODALU' => $lo->paDatos[0]['CCODALU']] +
                           ['CRECIBO' => $lo->paDatos[0]['CRECIBO']] +
                           ['CCODTRE' => $lo->paDatos[0]['CCODTRE']] +
                           ['CDNIALU' => $lo->paDatos[0]['CNRODNI']];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxLlenar() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'];
      $lo->paData = $laData;
      $_SESSION['paData'] += $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paData;
      fxScreen(1);
   }

   function fxGrabar() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + $_SESSION['paDatos'] + $_SESSION['paData'];
      $_SESSION['paDataC'] = $_REQUEST['paData'];
      $laDataC = $_SESSION['paDataC'];
      $lo->paData = $laData;
      $llOk = $lo->omGrabarSolicitudConstInter();
      if (!$llOk) {
         fxHeader('Paq1420.php', $lo->pcError);
      }
      fxDocumento("DocumentoPaquete.php?CNRODNI=".$laData['CDNIALU']."&CCODTRE=".$laData['CCODTRE']);
      fxInit();
   }

   function fxObservar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omObservSolicitudConsInter();
      if (!$llOk) {
         fxHeader('Paq1420.php', $lo->pcError);
      }
      $_SESSION['paData'] += $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1420.tpl');
   }
?>
   

