<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'DetalleAlumno') {
      fxDetalleAlumno();
   } elseif (@$_REQUEST['Boton'] == 'Activar') {
      fxActivar();
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Subir') {
      fxSubirArchivo();
   } elseif (@$_GET['Id'] == 'Verificar') {
      fxVerificar();
   } else {
      fxInit();
   }  

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omInitBandejaObservadosDigitalizacionImagen();
      if (!$llOk) {
         fxHeader('Mnu1000.php', 'SIN PAQUETES PENDIENTES');
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      //print_r($_SESSION['paDatos']);
      fxScreen(0);
   }

   function fxSubirArchivo () {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $lo->paFile = $_FILES['pfDocBac'];
      $llOk = $lo->omSubirArchivo();
      if (!$llOk) {
         fxScreen(0);
         fxAlert($lo->pcError);
         return;
      }
      fxInit();
   }


   function fxScreen($p_nBehavior) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq1290.tpl');
   }
?>