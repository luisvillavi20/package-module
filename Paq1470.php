<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CEmail.php';
   require_once 'Clases/CDeudas.php';
   session_start();
   $loSmarty = new Smarty;
   if (!fxSoloAlumnos()) {
      return;
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Seguimiento') {
      fxSeguimiento();
   } elseif (@$_REQUEST['Boton'] == 'Historial') {
      fxDetalleHistorial();
   } else {
      fxInit();
   }

   function fxInit() {   
      $lo = new CPaquetes();
      $lo->paData = ['CCODALU' => $_SESSION['GADATA']['CCODALU'], 'CNRODNI' => $_SESSION['GADATA']['CNRODNI'], 'CUSUCOD' => '9999'];
      $llOk = $lo->omInitAutenticacionDocumentos();
      if (!$llOk) {
         fxHeader('Mnu2000.php', $lo->pcError);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'] + $lo->paData;
      fxScreen(0);
   }

   function fxGrabar() {
      /*$laData = $_SESSION['GADATA'];
      $laData['CCODUSU'] = $_SESSION['GCCODUSU'];
      $loD = new CDeudas(); 
      $loD->paData = $_SESSION['paData'];
      $llOk = $loD->omComprobarDeudas();
      if (!$llOk) {
         fxAlert($loD->pcError);
         fxScreen(0);
         return;
      }*/
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODALU' => $_SESSION['GADATA']['CCODALU'], 'CCODUSU' => 'U666'];
      $llOk = $lo->omGrabarDeudaAutenticacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $llOk = $lo->omActualizarDatosAlumno();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }
      fxInit();
   }
   
   function fxScreen($p_nBehavior) {
      global $loSmarty;
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq1470.tpl');
   }
?>