<?php
   error_reporting(E_ALL);
   ini_set('display_errors',1);
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Generar') {
      fxGenerar();   
   } else{
      fxInit();
   }   

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'];
      $llOk = $lo->omCargarUltimaColacion();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;
      }
      $laData = $lo->paData;
      $lo->paData = $lo->paData + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omRecuperarTipoDeColaciones();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'] + $laData;
      fxScreen(0);
   }  
   
   function fxGenerar(){
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omGenerarColacion();
      if (!$llOk) {
         fxHeader('Paq2300.php', $lo->pcError);
         return;            
      }
      fxAlert('COLACIÓN CREADA CORRECTAMENTE');
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }     
    
   function fxScreen($p_nBehavior) {
      global $loSmarty;      
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq2300.tpl');
   }
?>