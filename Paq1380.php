<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   }
   elseif (@$_REQUEST['Boton'] == 'AprobarSol') {
      fxAprobarSol();
   } elseif (@$_REQUEST['Boton'] == 'DenegarSol') {
      fxDenegarSol();
   } else {
      fxInit();
   }   

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omInitBandejaConstanciaClinicaEscuela();
      if (!$llOk) {
         fxHeader('Mnu1000.php', 'SIN PAQUETES PENDIENTES');
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxAprobarSol() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData']+['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      
      $lo->paData = $laData;   
      $llOk = $lo->omAprobarConstanciaClinicaEscuela();
      if (!$llOk) {
         fxAlert($lo->pcError);
         return;
      }
      fxDocumento("DocumentoPaquete.php?CNRODNI=".$lo->paData['CNRODNI']."&CCODTRE=".$lo->paData['CCODTRE']);
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1380.tpl');
   }
?>