<?php
   error_reporting(E_ALL);
   ini_set('display_errors',1);
   require_once 'Libs/Smarty.class.php';   
   require_once 'Clases/CPaquetes.php';
   session_start();
   $loSmarty = new Smarty;
   
   if (!fxSoloAdministrativo()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Editar') {
      fxEditar();
   } elseif (@$_REQUEST['Boton'] == 'Actualizar') {
      fxActualizar();
   } else {
      fxInit();
   }

   function fxInit() {         
      $lo = new CPaquetes();
      $lo->paData = $_SESSION['GADATA'];
      $llOk = $lo->omCargarAutorizaciones();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;            
      }
      $_SESSION['paDocs'] = $lo->paDocs;
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = $_SESSION['GADATA'];
      fxScreen(0);
   }

   function fxGrabar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omGrabarAutorizacion();
      if (!$llOk) {
         fxHeader('Paq2010.php', $lo->pcError);
         return;            
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxEditar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omEditarAutorizacion();
      if (!$llOk) {
         fxHeader('Paq2010.php', $lo->pcError);
         return;            
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }

   function fxActualizar() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omActualizarAutorizacion();
      if (!$llOk) {
         fxHeader('Paq2010.php', $lo->pcError);
         return;            
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxInit();
   }

   function fxScreen($p_nBehavior) {
      global $loSmarty;      
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('saDocs',   $_SESSION['paDocs']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq2010.tpl');
   }
?>