<?php
   //BANDEJA TRAMITES PENDIENTES
   error_reporting(E_ALL);
   ini_set('display_errors',1);
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CWebService.php';
   require_once 'Styles/phpqrcode/qrlib.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'Subir') {
      fxSubirArchivoORAA();
   } elseif (@$_REQUEST['Boton'] == 'Salir') {     
      fxScreen3();
   } else {
      fxInit();
   }
   
   function fxInit() {        
      $lo = new CPaquetes();
      $lo->paData = ['CIDCATE' => 'CCCSID'];
      $llOk = $lo->omRecuperarCertificadosPorSubir();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;            
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

    function fxSubirArchivoORAA() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paFile = $_FILES['pfDocBac'];
      $llOk = $lo->omSubirArchivoORAA();
      if (!$llOk) {
         fxScreen(0);
         fxAlert($lo->pcError);
         return;
      }
      /*$lo = new CWebService();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omFirmarDocumento();
      if (!$llOk) {
         fxAlert($lo->pcError);
      }*/
      fxHeader('Paq1090.php', "ACTUALIZACION DE ARCHIVO REALIZADA CORRECTAMENTE");
   }
   
   function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1090.tpl');
   }
   function fxScreen3() {
      //header("location:Mnu2000.php"); 
      fxHeader("Mnu1000.php"); 
   } 
?>