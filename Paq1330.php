<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Subir') {
      fxSubirArchivo();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omIniBandejaSolicitudesDescuentoInvestigacion(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxCargarArchivo() {
      print($_SESSION['paData']);
      $laData = $_REQUEST['paData'] + ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      print_r($laData);
      $lo = new CPaquetes();
      $lo->poFile = $_FILES['poFile'];
      $llOk = $lo->omCargarDetalleArchivo();
   }
   
   function fxSubirArchivo () {
      $lo = new CPaquetes();
      $lo->paData = ['CNRODNI' => $_SESSION['GADATA']['CNRODNI']];
      $lo->paFile = $_FILES['pfDocBac'];
      print_r($lo->paData);
      print_r($lo->paFile);
      die;
      $llOk = $lo->omCargarDetalleArchivo();
      if (!$llOk) {
         fxScreen(0);
         fxAlert($lo->pcError);
         return;
      }
      $_SESSION['llModal'] = true;
      fxInit();
   }

   /*function fxAxCargarArchivoDetalle() {
      $laData = $_REQUEST['paData'];
      $laData['CIDREQU'] = (!empty($laData['CIDREQU'])) ? $laData['CIDREQU'] : '*';
      $laData['CCOMDIR'] = (isset($laData['CCOMDIR'])) ? $laData['CCOMDIR'] : 'N';
      $laData['CDESTIN'] = (isset($laData['CDESTIN'])) ? $laData['CDESTIN'] : 'I';
      $_SESSION['paData'] = $laData;
      $lo = new CLogistica();
      $lo->poFile = $_FILES['poFile'];
      $lo->paData = ['CUSUCOD' => $_SESSION['GCCODUSU'], 'CCENCOS' => $_SESSION['GCCENCOS']];
      $llOk = $lo->omCargarArchivoDetalle();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(1);
         return;   
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }*/
   
   function fxGrabarSolicitar() {
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $lo->paData = $laData;
      $llOk = $lo->omGrabarSolicitudDescuentoInvestigacion(); 
      if (!$llOk) {
         fxHeader('Paq1330.php', $lo->pcError);
      }
      fxInit();
   }

   function fxVerificar() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODALU' => $_REQUEST['CCODALU']] + $_SESSION['GADATA'];
      $llOk = $lo->omVerificarAlumnoxCod();
      if (!$llOk) {
         echo json_encode(["ERROR" => $lo->pcError]);
      } else {
         echo json_encode($lo->paData);
      }
   }
   
   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1330.tpl');
   }
?>