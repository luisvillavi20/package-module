<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CMatricula.php';
   require_once 'Clases/CReportes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'Seguir') {
      fxSeguimiento();
   } elseif (@$_REQUEST['Boton'] == 'PrintDecreto') {
      fxPrintDecreto();
   } elseif(@$_REQUEST['Boton'] == 'Entregar') {
      fxEntregar();
   } elseif(@$_REQUEST['Boton'] == 'Reporte') {
      fxReporteGrupo();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CMatricula();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omIniBandejaSolicitudesCPJ(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxSeguimiento() {
      $lo = new CMatricula();
      $laData = $_REQUEST['paData'];
      $lo->paData = $laData;
      $llOk = $lo->omSeguimientoSolicitudCPJ();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
         return;
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);  
   }

   function fxPrintDecreto() {
      $paData1 = $_SESSION['paData'];
      $paDatos1 = $_SESSION['paDatos'];
      $lo = new CMatricula();
      $laData = $_REQUEST['paData'] + ['SEMESTR' =>  $_REQUEST['paDataSem'], 'INCISO' =>  $_REQUEST['paDataInc']];
      $lo->paData = $laData;
      $llOk = $lo->omGenerarDecretoCpj();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(1);
         return;
      }
      //$_SESSION['paData'] = $paData1;
      //$_SESSION['paDatos'] = $paDatos1;
      fxDocumento($lo->pcFile);
      fxSeguimiento(); 
   }

   function fxEntregar(){
      $lo = new CMatricula();
      $lo->paData = $_REQUEST + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omEntregarTramiteCpj();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxScreen(0);
         return;
      }
      fxInit();
   }

   /*function fxReporteGrupo() {
      //print_r($_REQUEST);
      $lo = new CReportes();
      print_r($_REQUEST['paCodTre']);die;
      $lo->paData = ['paCodTre' => $_REQUEST['paCIdenti'], 'CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omGenerarReporteConstanciasEnGrupo();
      if (!$llOk) {
         fxHeader('Tdo5060.php', $lo->pcError);
         return;
      } else {
         fxDocument($lo->pcFile);
      }
      fxHeader('Tdo5060.php', 'DOCUMENTOS REVISADOS CORRECTAMENTE');
   }*/
   
   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5060.tpl');
   }
?>