<?php
   //phpinfo();
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CLogin.php';
   //header('Location: http://apps.ucsm.edu.pe/UCSMMTA/');
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (@$_REQUEST['Boton'] == 'Iniciar') {
      fxIniciarSesion();
   } else {
      fxInit();
   }  
   //Inicio de Mantenimiento Usuarios
   function fxInit() {
      //if (isset($_REQUEST['d'])) {
      //   $_SESSION['paIdCate'] = $_REQUEST['d'];
      //} elseif (isset($_REQUEST['a'])) {
      //   $_SESSION['pcAction'] = $_REQUEST['a'];
      //}
      $_SESSION = [];
      if (isset($_REQUEST['id'])) {
      //$_SESSION['PCIDCATE'] = $_REQUEST['id']]
         $_SESSION['CIDCATE'] = $_REQUEST['id'];
      }
      fxScreen();
   }
   
   function fxScreen() {
      global $loSmarty;
      $loSmarty->display('Plantillas/Index.tpl');
   } 
   
   function fxIniciarSesion() {
      if ($_REQUEST['pcCaptcha'] != $_SESSION['pcCaptcha']) {
         fxScreen();
         fxAlert('ERROR EN CAPTCHA. VUELVA A INTENTAR');
         return;
      }   
      $lo = new CLogin();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omIniciarSesion();      
      if (!$llOk) {
         fxHeader('index.php', $lo->pcError);
         return;
      }
      $_SESSION['GADATA'] = $lo->paData;
      $_SESSION['GCCODUSU'] = '9999';
      if (isset($_SESSION['paIdCate'])) {
         fxHeader('Tdo1150.php');
         unset($_SESSION['paIdCate']);
         return;
      } elseif ($_SESSION['CIDCATE'] == 'MPV') {// Mesa de Partes Virtual
         fxHeader('Tdo2750.php');
         return;
      } elseif ($_SESSION['CIDCATE'] == 'SIBEC') {// CONSTANCIAS PARA PRONABEC
         fxHeader('Tdo1270.php');
         return;
      } elseif ($_SESSION['CIDCATE'] == 'MATCRE') {// matricula por credito
         fxHeader('Tdo5160.php');
         return;
      } elseif ($_SESSION['CIDCATE'] == 'AUTENT') {// Acesso autentificacion
         fxHeader('Tdo5200.php');
         return;
      } elseif (isset($_SESSION['CIDCATE'])) {
         fxHeader('Tdo1260.php');
         return;
      } elseif (isset($_SESSION['pcAction'])) {
         $pcAction = $_SESSION['pcAction'].".php";
         if (file_exists($pcAction)) {
            fxHeader($pcAction);
            unset($_SESSION['pcAction']);
            return;
         } else {
            fxHeader('index.php', 'ACCION NO PERMITIDA');
         }
         return;
      }
      fxHeader("Mnu0000.php");
   }
?>