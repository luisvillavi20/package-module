<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CConstancias.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   }
   elseif (@$_REQUEST['Boton'] == 'Aprueba') {
      fxAprobarCertificado();
   } elseif (@$_REQUEST['Boton'] == 'Observar') {
      fxObsevarCertificado();
   } elseif (@$_REQUEST['Boton'] == 'Actualizar') {
      fxActualizar();
   } else {
      fxInit();
   }

   function fxInit() {
      $lo = new CConstancias();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];     
      $llOk = $lo->omIniBandejaConstanciasEspecialesPostGrado(); 
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxAprobarCertificado() {
      $lo = new CConstancias();
      $laData = $_REQUEST['paData'];
      $_SESSION['paData'] += ['CCODTRE' => $_REQUEST['paData']['CCODTRE']];
      $lo->paData = $laData + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omIniDetalleBandejaConstanciasEspecialesEscuela(); 
      if (!$llOk) {
         fxHeader('Tdo5140.php', $lo->pcError);
      }
      $llOk = $lo->omCargarDetalleAdmision(); 
      if (!$llOk) {
         fxHeader('Tdo5140.php', $lo->pcError);
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      if (isset($laData['CIDCATE']) && $laData['CIDCATE'] == '000001') {
         return fxScreen(1);
      } 
      $lo->paData += ['CIDCATE' => $_SESSION['paDatos']['CIDCATE']]+['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omAprobarCostanciaEspecial(); 
      if (!$llOk) {
         echo "'<script>alert('$lo->pcError');</script>'";
         fxScreen(1);
      }
      else {
         fxInit();
      }
   }

   function fxObsevarCertificado(){
      $lo = new CConstancias();
      $laData = $_REQUEST['paData'];
      $lo->paData = $laData + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];      
      $llOk = $lo->omObervarCertificadoDeConducta(); 
      if (!$llOk) {
         fxHeader('Tdo5140.php', $lo->pcError);
      }
      fxInit();
   }

   function fxActualizar() {
      $lo = new CConstancias();
      $laData = $_REQUEST['paData'];
      $lo->paData = $laData;
      $llOk = $lo->omIniMostrarCertificadoDeConducta(); 
      if (!$llOk) {
         fxHeader('Tdo5140.php', $lo->pcError);
      }
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Tdo5140.tpl');
   }
?>