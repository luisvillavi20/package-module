<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CBase.php';
   require_once 'Styles/phpqrcode/qrlib.php';
   session_start();
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;
   } elseif (@$_REQUEST['Boton'] == 'Generar') {
      fxGenerar();
   } else{
      fxInit();
   }
   
   function fxInit() {
      /*
      $lo = new CPaquetes();
      $llOk = $lo->omCargarTramitesFinalizados();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
         return;            
      }*/
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }      
   
   function fxGenerar(){
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + $_SESSION['GADATA'];
      $llOk = $lo->omGenerarRepTramitesFinalizados();
      if (!$llOk) {
         fxHeader('Paq2600.php', $lo->pcError);
         return;            
      }
      $laData = $_SESSION['GADATA'];
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxDocumento($lo->pcFile);
      fxScreen(0);
   }
   
   function fxScreen($p_nFlag) {
      global $loSmarty;      
      $loSmarty->assign('saDatos',  $_SESSION['paDatos']);      
      $loSmarty->assign('saData',   $_SESSION['paData']);
      $loSmarty->assign('snBehavior', $p_nBehavior);
      $loSmarty->display('Plantillas/Paq2600.tpl');
   }
?>