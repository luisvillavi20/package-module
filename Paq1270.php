<?php
   error_reporting(E_ALL);
   ini_set('display_errors',1);
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_GET['Id'] == 'CargarColacion') {
      fxCargarColacion();
   } elseif (@$_REQUEST['Boton'] == 'VerificarEstadoRevisores') {
      fxVerificarEstadoRevisores();
   } else {
      fxInit();
	}

	function fxInit() {
      $lo = new CPaquetes();
      $lo->paData =['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omRecuperarTipoDeColaciones();
      if (!$llOk) {
         fxHeader('Mnu1000.php', $lo->pcError);
      }
      $_SESSION['paArrays'] = [];
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paTipCol'] = $lo->paTipCol;
      $_SESSION['paEscuel'] = $lo->paEscuel;
      $_SESSION['paEspMec'] = $lo->paEspMec;
      $_SESSION['paDatos'] = null;
      fxScreen(0);
	}

   function fxVerificarEstadoRevisores() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $llOk = $lo->omVerificarEstadoRevisores();
      if (!$llOk) {
         fxHeader('Paq1270.php', $lo->pcError);
         return;            
      }
      $_SESSION['paDatos'] = $lo->paDatos;
      $_SESSION['paData'] = ['CNOMBRE' => $_SESSION['GADATA']['CNOMBRE'], 'CIDCOLA' => $_REQUEST['paData']['CIDCOLA'], 'CTIPCOL' => $_REQUEST['paData']['CTIPCOL']];
      fxScreen(2);
   }

   function fxCargarColacion() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omRecuperarUltimaColacion();
      if (!$llOk) {
         echo '{"ERROR":"'.$lo->pcError.'"}';
      } else {
         echo json_encode($lo->paData);
      }
   }

	function fxScreen($p_nFlag) {
      global $loSmarty;  
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('saTipCol', $_SESSION['paTipCol']);
      $loSmarty->assign('saEscuel', $_SESSION['paEscuel']);
      $loSmarty->assign('saEspMec', $_SESSION['paEspMec']);
      $loSmarty->assign('saArrays',   $_SESSION['paArrays']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1270.tpl');
   }
?>
