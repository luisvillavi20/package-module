<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CPaquetes.php';
   require_once 'Clases/CConstancias.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxSoloAdministrativo()) { 
      return;  
   } elseif (@$_REQUEST['Boton'] == 'DetalleAlumno') {
      fxDetalleAlumno();
   } elseif (@$_REQUEST['Boton'] == 'Activar') {
      fxActivar();
   } elseif (@$_REQUEST['Boton'] == 'Grabar') {
      fxGrabar();
   } elseif (@$_REQUEST['Boton'] == 'Reporte') {
      fxReporte();
   } else {
      fxInit();
   }   
   function fxInit() {
      $lo = new CPaquetes();
      $lo->paData = ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omInitBandejaRevPreviaSustentacionTitulacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
         fxHeader("Mnu1000.php");
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(0);
   }

   function fxDetalleAlumno() {
      $lo = new CPaquetes();
      $lo->paData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']] + ['CCODIGO'=>$_SESSION['paqDat']['CCODIGO']];
      $llOk = $lo->omInitBandejaRevisionDocumentosExpSustentacion();
      if (!$llOk) {
         fxAlert($lo->pcError);
         return fxScreen(0);
      }
      $_SESSION['paData'] = $_SESSION['GADATA'];
      $_SESSION['paDatos'] = $lo->paDatos;
      fxScreen(1);
   }
   
   function fxReporte(){
      $lo = new CPaquetes();
      $laData = $_REQUEST['paData'] + ['CCODUSU' => $_SESSION['GADATA']['CCODUSU']];
      $lo->paData = $laData;
      $llOk = $lo->omGenerarConstanciaTitulacionPrevia();
      if (!$llOk) {
         fxHeader('Paq1530.php', $lo->pcError);
      }
      fxDocumento("DocumentoPaquete.php?CNRODNI={$lo->paDatos['CNRODNI']}&CCODTRE=000005");
      fxInit();
   }

   function fxScreen($p_nFlag) {
      global $loSmarty;
      $loSmarty->assign('saData', $_SESSION['paData']);
      $loSmarty->assign('saDatos', $_SESSION['paDatos']);
      $loSmarty->assign('snBehavior', $p_nFlag);
      $loSmarty->display('Plantillas/Paq1530.tpl');
   }
?>